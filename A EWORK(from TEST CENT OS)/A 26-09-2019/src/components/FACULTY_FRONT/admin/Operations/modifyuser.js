import React, { Component,Fragment } from 'react'
import axios from 'axios'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'


export default class ModifyUser extends Component{
  constructor(){
      super();
      this.initialState={
        done:'',
        cancel_day:'',
        cancel_month:'',
        cancel_year:'',
        holiday_day:'',
        holiday_month:'',
        holiday_year:'',
          dayorder: '',
          day: '',
          month: '',
          year: '',
          sday: '',
          smonth: '',
          syear: '',
          eday: '',
          emonth: '',
          eyear: ''
      }
      this.state = this.initialState;
  }
  handleChange = (e) =>{
    this.setState({
      [e.target.name]: e.target.value,
    })
  }
  handleCancel_Holiday = (e) =>{
    this.setState(this.initialState)
    e.preventDefault();
    axios.post('/user/declare_cancel_or_holiday',this.state)
    .then(res=>{
      this.setState({done:res.data})
    })
    .catch(error =>{
      window.M.toast({html: 'Error!!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    })
  }

Declare_Advance_Holiday = (e) =>{
e.preventDefault()
}

  handleDayorder = (e) =>{
      this.setState({
          dayorder: e.target.value
      })
  }
  handleDay = (e) =>{
      this.setState({
          day: e.target.value
      })
  }
  handleMonth = (e) =>{
      this.setState({
          month: e.target.value
      })
  }
  handleYear = (e) =>{
      this.setState({
          year: e.target.value
      })
  }
  handleSday = (e) =>{
      this.setState({
          sday: e.target.value
      })
  }
  handleSmonth = (e) =>{
      this.setState({
          smonth: e.target.value
      })
  }
  handleSyear = (e) =>{
      this.setState({
          syear: e.target.value
      })
  }
  handleEday = (e) =>{
      this.setState({
          eday: e.target.value
      })
  }
  handleEmonth = (e) =>{
      this.setState({
          emonth: e.target.value
      })
  }
  handlEyear = (e) =>{
      this.setState({
          eyear: e.target.value
      })
  }
  handleSubmit(event){

  }
  handleHoliday(event){

  }
  handleStart(event){

  }
  handleEnd(event){

  }

  render(){
      if(this.props.select === 'start-semester'){
          return(
              <div className="center">

                              <div className="input-field col s4">
              <input id="sday" type="text" className="validate" value={this.state.sday} onChange={this.handleSday} required />
              <label htmlFor="sday">Day (dd)</label>
              </div>
                              <div className="input-field col s4">
              <input id="smonth" type="text" className="validate" value={this.state.smonth} onChange={this.handleSmonth} required />
              <label htmlFor="smonth">Month (mm)</label>
              </div>
                              <div className="input-field col s4">
              <input id="syear" type="text" className="validate" value={this.state.syear} onChange={this.handleSyear} required />
              <label htmlFor="syear">Year (yyyy)</label>
              </div>
              <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleStart}>Start Semester</Link>
              </div>

          )
      }
      else if(this.props.select === 'end-semester'){
          return(
              <div className="center">

                              <div className="input-field col s4">
              <input id="eday" type="text" className="validate" value={this.state.eday} onChange={this.handleEday} required />
              <label htmlFor="eday">Day (dd)</label>
              </div>
                              <div className="input-field col s4">
              <input id="emonth" type="text" className="validate" value={this.state.emonth} onChange={this.handleEmonth} required />
              <label htmlFor="emonth">Month (mm)</label>
              </div>
                              <div className="input-field col s4">
              <input id="eyear" type="text" className="validate" value={this.state.eyear} onChange={this.handleEyear} required />
              <label htmlFor="eyear">Year (yyyy)</label>
              </div>
              <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleEnd}>End Semester</Link>
              </div>

          )
      }
      else if(this.props.select === 'modifytoday'){
          return(
              <div className="center">

                              <div className="input-field col s12">
              <input id="dayorder" type="text" className="validate" value={this.state.dayorder} onChange={this.handleDayorder} required />
              <label htmlFor="dayorder">Reset Today's Day Order to:</label>
              </div>
              <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Reset Day Order</Link>
              </div>

          )
      }
      else if(this.props.select === 'cancel_or_declare_today_as_a_holiday'){
          return(
            <div className="form-signup">
                <div className="row">
                <div className="input-field col s4">
                <input id="cday" type="Number" name="cancel_day" className="validate" value={this.state.cancel_day} onChange={this.handleChange} required />
                <label htmlFor="cday">Day (dd)</label>
                </div>
                <div className="input-field col s4">
                <input id="cmonth" type="Number" name="cancel_month" className="validate" value={this.state.cancel_month} onChange={this.handleChange} required />
                <label htmlFor="cmonth">Month (mm)</label>
                </div>
                <div className="input-field col s4">
                <input id="cyear" type="Number" name="cancel_year" className="validate" value={this.state.cancel_year} onChange={this.handleChange} required />
                <label htmlFor="cyear">Year (yyyy)</label>
                </div>
                <div className="col l3">{this.state.done}</div>
                <button className="waves-effect right btn #37474f blue-grey darken-3 col s3 offset-s3" onClick={this.handleCancel_Holiday}>SUBMIT</button>
                </div>
          </div>
          )
      }
      else if(this.props.select === 'holiday'){
          return(
            <div className="form-signup">
                <div className="row">
                <div className="input-field col s4">
                <input id="hday" type="Number" name="holiday_day" className="validate" value={this.state.holiday_day} onChange={this.handleChange} required />
                <label htmlFor="hday">Day (dd)</label>
                </div>
                <div className="input-field col s4">
                <input id="hmonth" type="Number" name="holiday_month" className="validate" value={this.state.holiday_month} onChange={this.handleChange} required />
                <label htmlFor="hmonth">Month (mm)</label>
                </div>
                <div className="input-field col s4">
                <input id="hyear" type="Number" name="holiday_year" className="validate" value={this.state.holiday_year} onChange={this.handleChange} required />
                <label htmlFor="hyear">Year (yyyy)</label>
                </div>
                <div className="col l3">{this.state.done}</div>
                <button className="waves-effect right btn #37474f blue-grey darken-3 col s3 offset-s3" onClick={this.Declare_Advance_Holiday}>SUBMIT</button>
                </div>
          </div>
          )
      }
      else{
          return(
              <div></div>
          )
      }
  }
}
