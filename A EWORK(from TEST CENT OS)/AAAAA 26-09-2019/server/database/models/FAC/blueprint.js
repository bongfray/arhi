const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const blueprint = new Schema({
  username: {type: String , unique: false, required: false},
  alloted_slots: { type: String, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  timing: {type: String, unique: false , required: false},
  complete_slots_status: {type: String, unique: false , required: false},
  sum_submit: {type: Number, unique: false , required: false},
})


const BluePrint = mongoose.model('blue_print', blueprint)
module.exports = BluePrint
 
