## EWork




# About
It's About Official Toll

## Built With

Mongo, Express, ReactJS, Node.js, Javascript.

## Getting Started

Clone the repo and follow the instructions.  You can view each step by running these commands from the terminal:

```
git checkout step-0
git checkout step-1
...
```

### Prerequisites

- [Node.js](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)
- [Nodemon](https://github.com/remy/nodemon)
- [create-react-app](https://github.com/facebook/create-react-app)

### Installing

Run these commands in the terminal:

```
cd simple-mern-passport
npm install
npm run dev
```


### Next Steps
- [ ] Add redux
- [ ] Add SCSS

## Author
Arijit Nayak
