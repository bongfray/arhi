import React, { Component } from 'react';
import { render } from "react-dom";
import { css } from '@emotion/core';
import { RingLoader } from 'react-spinners';
import './loader.css'
const override = css`
    display: block;
    margin: 0 auto;
    top:0;
    left: 0;
    border-color: red;
    z-index: 1;
`;
class Loader2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

render(){
  return (
    <div className="Load2">
    <RingLoader
              css={override}
              sizeUnit={"0px"}
              size={20}
              color={'#123abc'}
              loading={this.state.loading}
            />
    </div>)
  }
}
export default Loader2;
