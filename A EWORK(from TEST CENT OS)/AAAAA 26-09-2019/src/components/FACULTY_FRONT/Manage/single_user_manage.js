import React, { Component,Fragment } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'
import ReqStat from './approve_stat'


export default class ManageOne extends Component {
  constructor(props)
  {
    super(props)
    this.initialState ={
      action:'false',
      username:props.username,
      day:'',
      month:'',
      year:'',
      req_reason:'',
      day_order:'',
      request_props:[],
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    M.AutoInit()
  }
  handleDatas=(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleRequest =(e) =>{
    e.preventDefault()
    this.initialState.request_props = this.state;
    axios.post('/user/request_for_entry',this.state).then(response=>{
      if(response.data.handled)
      {
         window.M.toast({html: response.data.handled,outDuration:'1000', classes:'rounded red'});
      }
      else if(response.data.succ)
      {
         window.M.toast({html: response.data.succ,outDuration:'1000', classes:'rounded green'});
      }
      else if(response.data.noday){
           window.M.toast({html: response.data.noday,outDuration:'1000', classes:'rounded green'});
      }
      this.setState(this.initialState);
    })
  }
  render()
  {
        console.log(this.props.username)
    return(
      <Fragment>
      <div className="row">
      <div className="col l1" />
        <div className="col l10 form-signup">
          <h6 className="center">REQUEST FOR AN ENTRY</h6><br />
          <div className="row">
            <div className="col l2">
            <b className="center">Enter DayOrder</b>
              <div className="input-field">
               <input className="" type="text" name="day_order" id="Request-dayorder" value={this.state.day_order} onChange={this.handleDatas}/>
               <label htmlFor="Request-dayorder">Enter the DayOrder</label>
              </div>
            </div>
            <div className="col l5">
              <b className="center">Enter Date</b>
              <div className="row">
               <div className="col l4">
               <div className="input-field">
                <input className="" name="day" type="text" id="day-req" value={this.state.day} onChange={this.handleDatas}/>
                <label htmlFor="day-req">Day(dd)</label>
               </div>
               </div>
               <div className="col l4">
               <div className="input-field">
                <input className="" type="text" name="month" value={this.state.month} id="month-req" onChange={this.handleDatas}/>
                <label htmlFor="month-req">Month(mm)</label>
               </div>
               </div>
               <div className="col l4">
               <div className="input-field">
                <input className="" type="text" name="year" value={this.state.year} id="year-req" onChange={this.handleDatas}/>
                <label htmlFor="year-req">Year(YYYY)</label>
               </div>
               </div>
              </div>
            </div>
            <div className="col l5">
              <b className="center">Kindly Give Some Valid Reason</b>
              <label className="pure-material-textfield-outlined alignfull">
                <textarea
                  className=""
                  type="text"
                  placeholder=" "
                  min="10"
                  max="60"
                  name="req_reason"
                  value={this.state.req_reason}
                  onChange={this.handleDatas}
                />
                <span>Enter the Valid Reason</span>
              </label>
            </div>
          </div>
          <button className="btn right blue-grey darken-2 sup" onClick={this.handleRequest} style={{marginBottom:'8px'}}>Make A Request</button>
        </div>
        <div className="col l1" />
      </div>
      <div className="row form-signup">
        <ReqStat request_props={this.initialState.request_props}/>
      </div>
      </Fragment>
    )
  }
}
