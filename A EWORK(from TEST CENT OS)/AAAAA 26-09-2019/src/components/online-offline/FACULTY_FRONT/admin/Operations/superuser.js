import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'

export default class SuperUser extends Component{
    constructor(){
        super();
        this.state={
          isChecked:false,
          isCheckedS:false,
          history:'',
            susername:'',
            username:'',
            index_id:'',
            request:[],
            viewdata:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    handleComp= (e)=>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
      axios.post('/user/handleFaculty_Registration',{
        checked: !this.state.isChecked,
        history: e.target.value,
      })
      .then(res=>{

      })
    }

    handleCompS=(e)=>{
      this.setState({
        isCheckedS: !this.state.isCheckedS,
        history: e.target.value,
      })

      axios.post('/user2/handleStudent_Registration',{
        checked: !this.state.isCheckedS,
        history: e.target.value,
      })
      .then(res=>{

      })
    }

    getRequest =() =>{
      axios.get('/user/getrequest')
          .then(response => {
            this.setState({request: response.data})
          })
    }
    view_Status_of_Request = (content,index) =>{
      this.setState({
        username:content.username,
        index_id:index,
      })
      axios.post('/user/approve_request',content)
          .then(response => {
            if(response.data)
            {
              this.setState({viewdata: response.data})
            }
          })
    }
    componentDidMount()
    {
      this.getRequest();
      this.fetchFaculty_Registration_Status();
      this.fetchStudent_Registration_Status();
    }
    fetchFaculty_Registration_Status()
    {
      axios.get('/user/fac_reg_status')
          .then(response => {
            if(response.data)
            {
              this.setState({isChecked: response.data})
            }
          })
    }
    fetchStudent_Registration_Status()
    {
      axios.get('/user2/stud_reg_status')
          .then(response => {
            if(response.data)
            {
              this.setState({isCheckedS: response.data})
            }
          })
    }




    handleRequestModify =(object) =>{
      this.setState(object)
    }
    render(){
      console.log(this.state.history)
      var viewbutton;
        if(this.props.select === 'register'){
            return(
                <div className="row" style={{padding:'15px'}}>
                <div className=" col s12" >

                <div className="row">
                <div className="col l8">
                   Faculty Registration
                </div>
                <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.isChecked} value="fac" onChange={this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
                </div>
                </div>

                <div className="row">
                <div className="col l8">
                   Student Registration
                </div>
                <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.isCheckedS} value="stud" onChange={this.handleCompS} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
                </div>
                </div>
                </div>
                </div>
            )
        }
        else if(this.props.select === 'facprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Studnet Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'facreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Student Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === "approve_single_faculty_req")
        {
          viewbutton =
          <React.Fragment>
          <div className="row">
            <div className="col l1 center"><b>Serial No.</b></div>
            <div className="col l1 center"><b>Day Order</b></div>
            <div className="col l2 center"><b>Official ID</b></div>
            <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
            <div className="col l3 center"><b>Reason</b></div>
            <div className="col l2 center"><b>Action</b></div>
          </div><hr />
          {this.state.request.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 center">{index+1}</div>
            <div className=" col l1 center">{content.day_order}</div>
            <div className=" col l2 center">{content.username}</div>
            <div className=" col l3 center">{content.day}/{content.month}/{content.year}</div>
            <div className=" col l3 center">{content.req_reason}</div>
            </div>
            <button className="btn col l2 blue-grey darken-2 sup" onClick={() => this.view_Status_of_Request(content,content.serial)}>View Status</button>
            </div>
            </React.Fragment>
          ))}
          <ViewApprove handleRequestModify={this.handleRequestModify} request={this.state.request} view_details={this.state.viewdata} username={this.state.username} index_id={this.state.index_id}/>
          </React.Fragment>
        }
        else{
          viewbutton =
          <div></div>
        }
        return(
            <div className="center">
            {viewbutton}
            </div>
        )
    }
}

class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleDeny =(id,username) =>{
    axios.post('/user/denyrequest',{
      serial: id,
      username:username,
    }).then(res=>{
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  handleApprove = (id,username) =>{
    axios.post('/user/approverequest',{
      serial: id,
      username:username,
    }).then(res=>{
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  render()
  {
    var dat;
    if(this.props.view_details ==="yes")
    {
      dat= <React.Fragment>
      <h6>Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!</h6>
      <div className="row">
      <button className="btn col l2 right red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      </div>
      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat= <React.Fragment>
      <h6>We can't able to find any data for this request !! Kindly take decison manually !!</h6>
      <div className="row">
      <div className="col l7" />
      <div className="col l5">
      <div className="row">
      <div className="col l5" />
      <button className="btn col l3 sup red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      <div className="col l1" />
      <button className="btn col l3 blue-grey darken-2 sup" onClick={() => this.handleApprove(this.props.index_id,this.props.username)}>APPROVE</button>
      </div>
      </div>
      </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    return(
      <div className="form-signup">
      {dat}
      </div>
    )
  }
}
