import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import Slogo from './Slogo.png'
import './style.css'
import M from 'materialize-css'
import Fhome from './fhome2.png'

class FrontPage extends Component{

    componentDidMount() {

        M.AutoInit();
        let elems = document.querySelectorAll(".modal-trigger");
        M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });

    }
    render(){
    
        return(
            <div className="row">
                <div className="col s12 l6 m12">
                    <div className="fstyle">
                    <center><div className="title-of-home"><h4  className="ework_name">E-Work</h4></div></center><br /><br />
                    <p className="fpara">Ework is a simplified analytics tool.It is a tool to keep trace and record of each and everyday routine of staff members of the institute.E-Work is a tool designed by CARE which majorly focuses on the encouragement of the well deserved ones. <br/><br/><br />
                    <a href="#modal2" className="col l5 m12 s12 waves-effect btn #03a9f4 light-blue modal-trigger"><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></a>
                    <div className="col s2"></div>
                    <Link to="#" className="col s12 m12 l5 waves-effect btn #c0ca33 lime darken-1"><i className="material-icons right">desktop_mac</i><b>About</b></Link>
                    <a className=" col s3 offset-l10 offset-s5 offset-m5 fprivacy blink modal-trigger" href="#modal1">Privacy Policy</a>
                    <div id="modal1" className="modal modal-fixed-footer modal-fixed">
                <div className="modal-content">
                  <h4 className="mheader">Privacy Policy</h4>
                  <p className="mcont">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione a aut unde officiis cum! Odit qui, consequatur at sapiente fugiat corporis obcaecati similique omnis minus nostrum rerum, voluptatum iure sint. Lorem ipsum dolor sit amet consectetur adipisicing elit. Id aliquid quam aspernatur illum nam reiciendis dolores accusamus ullam neque necessitatibus fugiat a odio, hic soluta nulla vero dolorum. Mollitia, doloremque. Lorem ipsum dolor sit amet consectetur adipisicing elit. Et ea, quas repellendus officia quo id impedit obcaecati consequuntur dolores iure quod itaque repudiandae minus libero similique laborum ipsam, quia laudantium! Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto exercitationem perspiciatis odit similique perferendis, accusantium hic cum consectetur, reiciendis est unde. Nihil, harum? Corrupti, dolorum iusto. Sunt quae corporis at.</p>
                </div>
                <div className="modal-footer">
                  <a href="#!" className="modal-close btn-flat"><i className="material-icons right">close</i>Close</a>
                </div>
                </div>

                <div id="modal2" className="modal modal-fixed z-depth-5 modaln">
                <div className="modal-content">
                  <p className="mcont"  style={{marginBottom:'60px'}}>
                      <Link to="/login" className="col s5 waves-effect btn #311b92 deep-purple darken-4">Faculty</Link>
                      <div className="col s2"></div>
                      <Link to="/slogin" className="col s5 waves-effect btn #00c853 green accent-4">Student</Link>
                  </p>
                </div>
                {/* <div className="modal-fFaculty">
                  <a href="#!" className="modal-close btn-flat"><i className="material-icons right">close</i>Close</a>
                </div> */}
                </div>
                    </p>

                    </div>
                </div>
                <div className="col s6 hide-on-med-and-down">
                    <img className="img-home imgf" src={Fhome} alt=""/>
                </div>
            </div>
        );
    }
}

export default FrontPage;
