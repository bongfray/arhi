import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Route, Link } from 'react-router-dom'
import axios from 'axios'
import Slogo from './Slogo.png'
import './style.css'
import Logo from './Logout.png'


class Navbar extends Component {
    constructor() {
        super()
        this.logout = this.logout.bind(this)
    }

    logout(event) {
        event.preventDefault()
        //console.log('logging out')
        axios.post('/user/logout').then(response => {
          //console.log(response.data)
          if (response.status === 200) {
            this.props.updateUser({
              loggedIn: false,
              username: null
            })
            window.location.assign("/");
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }

    render() {
        const loggedIn = this.props.loggedIn;
        //console.log('navbar render, props: ')
        //console.log(this.props);

        return (
          <nav>
            {loggedIn ? (
              <div className="nav-wrapper blue-grey darken-2">
              <div className="row">
                <div className="col s2"> <div className=" col s2 brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div></div>
                   <div className="col s8"><div className="center nav-cen con">SRM Centre for Applied Research in Education</div></div>
                   <div className="col s2 right">
                    <Link to="#" className="right" onClick={this.logout}>
                    <img src={Logo} className="lgout" alt="log"/>
                    </Link>
                    </div>
                    </div>
                    </div>
            ) : (

              <div className="nav-wrapper blue-grey darken-2">
                  <div className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div>
                  <div className="nav-wrapper center nav-cen con">SRM Centre for Applied Research in Education</div>
              </div>

                )}

  </nav>
          /*
            <div>

                <header className="navbar App-header" id="nav-container">
                    <div className="col-4" >
                        {loggedIn ? (
                            <section className="navbar-section">
                                <Link to="#" className="btn btn-link text-secondary" onClick={this.logout}>
                                <span className="text-secondary">logout</span></Link>

                            </section>
                        ) : (
                                <section className="navbar-section">
                                    <Link to="/" className="btn btn-link text-secondary">
                                        <span className="text-secondary">home</span>
                                        </Link>
                                    <Link to="/login" className="btn btn-link text-secondary">
                                    <span className="text-secondary">login</span>
				</Link>
                                    <Link to="/signup" className="btn btn-link">
                                    <span className="text-secondary">sign up</span>
				</Link>
                                </section>
                            )}
                    </div>
                    <div className="col-4 col-mr-auto">
                    <div id="top-filler"></div>
                        <img src={logo} className="App-logo" alt="logo" />
                        <h1 className="App-title">MERN Passport</h1>
                    </div>
                </header>
            </div>
            */

        );

    }
}

export default Navbar
