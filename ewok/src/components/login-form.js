import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from './forget'
import axios from 'axios'
import './style.css'
import M from 'materialize-css';
import { } from 'materialize-css';

var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redirectTo: null
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'Enter all the Details', outDuration:'1000', inDuration:'1000', displayLength:'4000'});
          this.setState({
              cnf_pswd:''
          })
          return false;
        }
        else{
        axios
            .post('/user/login', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                console.log('login response: ')
                console.log(response)
                if (response.status === 200) {
                    this.props.updateUser({
                        loggedIn: true,
                        username: response.data.username,
                        //logins: '/dash/ProfDash'
                    })


                  /*  if(response.data.use==="hod")
                    {
                      this.props.updateUser({
                        redirectTo:'/dash/DirDash'
                      })
                    }*/

                  if(response.data.use==="hod"){
                      window.location.assign("/dash/DirDash");
                  }
                  else if(response.data.use==="prf"){
                    window.location.assign("/dash/ProfDash");
                  }
                  else if(response.data.use==="aprf"){
                    window.location.assign("/dash/AssoDash");
                  }
                  else if(response.data.use==="asprf"){
                    window.location.assign("/dash/AssiDash");
                  }

                }
            }).catch(error => {
                window.M.toast({html: 'Check your Login Credentials', outDuration:'1000', inDuration:'1000', displayLength:'1500'});

            })
            this.setState({
               username: '',
               password: '',
             })
          }
    }
    notifi = () => window.M.toast({html: 'Enter Details', outDuration:'900', inDuration:'900', displayLength:'1700'});

        componentDidMount() {
            this.notifi();
            M.AutoInit();
        }
    render() {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
            return (
              <div className="row">

             <div className="col s4">
             </div>

             <div className="col s4 form-login">
                 <div className="ew center">
                     <h5 className="blue-grey darken-2 highlight">eWORK</h5>
                     <h5 className="logi"><b>LOG IN</b></h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="email" type="email" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <label htmlFor="email">Email</label>
                     </div>
                     <div className="input-field inline col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <label htmlFor="password">Password</label>
                     </div>
                     </div>
                     <br/>
                     <div className="row">
                     <Link to="/Signup" className="waves-effect waves-light btn blue-grey darken-2 col s5 sup">Register</Link>
                     <div className="col s2"></div>
                     <button className="waves-effect waves-light btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <br/>
                     <hr/>
                     <div className="row">
                     <Modal />
                     </div>

                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
            )
        }
    }
}

export default LoginForm
