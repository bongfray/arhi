import React, { Component } from 'react'
import { Route, Link } from 'react-router-dom'
import axios from 'axios'
import M from 'materialize-css';
import { } from 'materialize-css';

var empty = require('is-empty');


class Modal extends Component {
  constructor() {
      super()
      this.state = {
          mailid: ''
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleForgo= this.handleForgo.bind(this)

  }
  handleForgo(e) {
      this.setState({
          mailid: e.target.value
      })
  }
  handleSubmit(event) {
      event.preventDefault()
      if(empty(this.state.mailid)){
        window.M.toast({html: 'Enter your Mailid', outDuration:'1000', inDuration:'1000', displayLength:'4000'});
        return false;
      }
      else{
      axios.post('/user/forgo', {
              mailid: this.state.mailid
          })
          .then(response => {
              console.log('Forget res')
              console.log(response)
              if (response.status === 200) {

              }
          }).catch(error => {

          })
        }
  }
  componentDidMount() {
    const options ={
      inDuration: 250,
      outDuration: 250,
      opacity: 0.5,
      dismissable: false,
      startingTop: "4%",
      endingTop: "10%"
    };
    M.Modal.init(this.Modal,options);
}
    render()
    {
        return(
          <div>

          <a className="col s6 offset-s3 but N/A transparent modal-trigger" data-target="modal1">
          Forget password?
          </a>
          <div ref={Modal => {
            this.Modal =Modal;
          }}
           id="modal1" className="modal">
      <div className="modal-content">
      <h4 className="forgohead">Forget Password</h4>
      <br />
      <p className="ppo">Please enter your email id :</p>

      <div className="row">
      <div className="input-field col s12">
        <input id="mail" type="email" value={this.state.mailid} onChange={this.handleForgo} className="validate" required="true"/>
        <label for="mail">Mail Id</label>
      </div>
      <Link to="#" className="waves-effect waves-light btn blue-grey darken-2 col s4 sup right" onClick={this.handleSubmit}>Send Mail</Link>
      </div>

     </div>
     <div className="modal-footer">
      <a href="#!" className="modal-close waves-effect waves-green btn-flat">Close</a>
      </div>
     </div>
     </div>
        )
    }
}

export default Modal;
