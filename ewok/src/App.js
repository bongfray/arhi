import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import ProfDash from './dash/ProfDash'
import DirDash from './dash/DirDash'
import AssiDash from './dash/AssiDash'
import AssoDash from './dash/AssoDash'
import Signup from './components/sign-up'
import LoginForm from './components/login-form'
import Navbar from './components/navbar'
import './style.css'


class App extends Component {
  constructor() {
    super()
    this.state = {
      loggedIn: false,
      username: null,
      logins: null
    }

    this.getUser = this.getUser.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }


  componentDidMount() {
    this.getUser()
  }

  updateUser (userObject) {
    this.setState(userObject)
  }

  getUser() {
    axios.get('/user/').then(response => {
      console.log('Get user response: ')
      console.log(response.data)
      if (response.data.user) {
        console.log('Get User: There is a user saved in the server session: ')

        this.setState({
          loggedIn: true,
          username: response.data.user.username
        })
      } else {
        console.log('Get user: no user');
        this.setState({
          loggedIn: false,
          username: null
        })
      }
    })
  }


  render() {
    //const redir = this.props.logins;
    return (
       <div className="App">

        <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn} />
        {/* greet user if logged in: */}

        {this.state.loggedIn &&
          <p className="center">Welcome , {this.state.username}!</p>

        }
        {/*this.state.loggedIn &&

          window.location.assign(API_URL+'/signup')
        */}
    {  /*  {this.state.loggedIn ?
        (

          window.location.assign(API_URL+'/signup')
        )
        :
        (
        window.location.assign(API_URL+'/')
        )
      }*/}
        <Route exact path="/" render={() =>
          <LoginForm
         updateUser={this.updateUser}
         />}
       />

       <Route   path="/signup" component={Signup} />

      <Route path="/dash/ProfDash"  component={ProfDash} />
      <Route path="/dash/DirDash" component={DirDash} />
      <Route path="/dash/AssiDash" component={AssiDash} />
      <Route path="/dash/AssoDash" component={AssoDash} />
      </div>
    );
  }
}

export default App;
