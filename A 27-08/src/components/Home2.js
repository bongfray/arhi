import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import Slogo from './Slogo.png'
import './css_home.css'
import M from 'materialize-css'
import Fhome from './fhome2.png'

class Home extends Component{

    render(){
        return(


          <div className="wrapper bdy">
        <div className="floor"></div>
        <div className="left">
        <div id="container sideslid">
	<div className="paper">
		<div className="tape"></div>
		<div className="red-line first"></div>
		<div className="red-line"></div>
		<ul id="lines">
			<li></li>
			<li>Please don`t forget to</li>
			<li>buy some food</li>
			<li></li>
			<li>Thanks</li>
			<li></li>
			<li></li>
		</ul>
		<div className="left-shadow"></div>
		<div className="right-shadow"></div>
	</div>
</div>
        </div>
        <div className="shelf-wrapper">
            <div className="shelf1">
                <div id="slantbook"></div>
                <div id="flatbook"></div>

            </div>
            <div className="shelf2">
                <div id="flatbook3"></div>
                <div className="penstand">
                    <div id="pen"></div>
                </div>

            </div>
            <div className="shelfknob">
                <div className="knob" id="knob1"></div>
                <div className="knob" id="knob2"></div>

            </div>

        </div>
        <div className="window">
            <span id="moon"></span>
        </div><br />
        {/*<div className="right directions">
          <Link to="/login" className="col l5 m12 s12 waves-effect btn #03a9f4 light-blue"><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></Link>
        </div>*/}
        <Link to="/login">
     <div className="right directions">
        <div id="Awesome" className="anim750">
	<div className="sticky anim750">
		<div className="front circle_wrapper anim750">
			<div className="circle anim750"></div>
	  </div>
	</div>

  <h6>Login & SignUP</h6>

  <div className="sticky anim750">
		<div className="back circle_wrapper anim750">
			<div className="circle anim750"></div>
		</div>
	</div>

</div>
</div>
</Link>



        <div className="container">
            <div className="cup"></div>
            <div className="table">
                <span className="handle" id="handle1"></span>
                <span className="handle" id="handle2"></span>
                <div id="margintop">
                    <div className="tableleg" id="table0"></div>
                    <div className="tableleg" id="table1"></div>
                    <div className="tableleg" id="table2"></div>
                    <div className="tableleg" id="table3"></div>
                    <div className="tableleg" id="table4"></div>
                    <div className="tableleg" id="table5"></div>
                    <div className="tableleg" id="table6"></div>
                    <div className="tableleg" id="table7"></div>

                </div>
            </div>
            <div className="lamp">
                <div id="lampjoint"></div>
                <div className="lamphead"></div>
            </div>
            <div className="monitor">
                <span id="monitor-base"></span>
            </div>

            <div className="semicircle"></div>
        </div>




    </div>

        );
    }
}

export default Home;
