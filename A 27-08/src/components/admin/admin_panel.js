import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'
import Nav from '../dynnav'
var empty = require('is-empty');

class APanel extends Component {
  constructor()
  {
    super()
    this.state = {
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Insert User',
          link:'/',
        },
        {
          val:'Delete User',
          link:'/login',
        }
      ],
    }
  }
    componentDidMount() {
        M.AutoInit();
    }

render() {
  return(
    <div>
    <Nav content={this.state.content} get={this.state.get} logout={this.state.logout}/>
     <h5>Admin Here</h5>
    </div>
  )
}
}

export default APanel
