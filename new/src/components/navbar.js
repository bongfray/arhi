import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
//import '../App.css';
import axios from 'axios'
import {API_URL} from '../utils/apiUrl'
import Slogo from './Slogo.png'
import './style.css'
import Logo from './Logout.png'
//import {API} from '../utils/redirect'
class Navbar extends Component {
    constructor() {
        super()
        this.state = {
            redirectTo: null
        }
        this.logout = this.logout.bind(this)
    }

    logout(event) {
        event.preventDefault()
        //console.log('logging out')
        axios.post(API_URL+'/user/').then(response => {
          //console.log(response.data)
          if (response.status === 200) {
            this.props.updateUser({
              loggedIn: false,
              username: null,
            })
            window.location.assign("http://localhost:3000/");
          }
        }).catch(error => {
            //console.log('Logout error')
        })
      }

    render() {
        const loggedIn = this.props.loggedIn;
        console.log('navbar render, props: ')
        console.log(this.props);

        return (
            <nav>
                        {loggedIn ? (
                          <div className="nav-wrapper blue-grey darken-2">
                          <div className="row">
                            <div className="col s2"> <div className=" col s2 brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div></div>
                               <div className="col s8"><div className="center nav-cen con">SRM Centre for Applied Research in Education</div></div>
                               <div className="col s2 right">
                                <Link to="#" className="right" onClick={this.logout}>
                                <img src={Logo} className="lgout" alt="log"/>
                                </Link>
                                </div>
                                </div>
                                </div>
                        ) : (

                              <div className="nav-wrapper blue-grey darken-2">
                                  <div className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div>
                                  <div className="nav-wrapper center nav-cen con">SRM Centre for Applied Research in Education</div>
                              </div>

                            )}

              </nav>

        );

    }
}

export default Navbar
