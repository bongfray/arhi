import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import axios from 'axios'
import './style.css'
import {API_URL} from '../utils/apiUrl'
var empty = require('is-empty');

class LoginForm extends Component {
    constructor() {
      super()
      this.state = {
          username: '',
          password: '',
          redirectTo: null
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleUser= this.handleUser.bind(this)
      this.handlePass= this.handlePass.bind(this)


  }
  handleUser = (e) => {
          this.setState({
              username: e.target.value
          });
      };
  handlePass = (e) => {
              this.setState({
                  password: e.target.value
              });
          };

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password))
        {
            window.M.toast({html: 'Enter all the Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
            this.setState({
                cnf_pswd:''
            })
            return false;
        }
      else{
        axios
            .post(API_URL+'/user/login' , {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                //console.log('login response: ')
                //console.log(response)
                if (response.status === 200) {
                    // update App.js state
                    this.props.updateUser({
                        loggedIn: true,
                        username: response.data.username
                    })
                    alert("Click ok to redirect to Dashboard");
                    //window.M.toast({html: 'We are redirecting you to dashboard', outDuration:'850', inDuration:'800', displayLength:'1500'});
                    // update the state to redirect to home
                    this.setState({
                        redirectTo: '/dash'
                    })
                }
            }).catch(error => {
              window.M.toast({html: 'Check your Login Credentials', outDuration:'1000', inDuration:'1000', displayLength:'1500'});
                //console.log('login error: ')
                //console.log(error);

            })
            this.setState({
            username: '',
            password: '',

        })
          }
    }
    notify = () => window.M.toast({html: 'Welcome to eWork!', outDuration:'850', inDuration:'800', displayLength:'1500'});
    componentDidMount(){
      this.notify();
    }
    render() {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
            return (
              <div className="row">

           <div className="col s4">
           </div>

           <div className="col s4 form-login">
               <div className="ew center">
                   <h5 className="blue-grey darken-2 highlight">eWORK</h5>
                   <h5 className="logi"><b>LOG IN</b></h5>
               </div>
               <form className="form-con">
                   <div className="row">
                   <div className="input-field col s12">
                       <input id="email" type="email" className="validate" value={this.state.username} onChange={this.handleUser} required />
                       <label htmlFor="email">Email</label>
                   </div>
                   <div className="input-field inline col s12">
                       <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                       <label htmlFor="password">Password</label>
                   </div>
                   </div>
                   <br/>
                   <div className="row">
                   <Link to="/Signup" className="waves-effect waves-light btn blue-grey darken-2 col s5 sup">Register</Link>
                   <div className="col s2"></div>
                   <button className="waves-effect waves-light btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                   </div>
                   <br/>
                   <hr/>
                   <div className="row">
                   <Link className="col s6 offset-s3 but" href="#">Forgot Password?</Link>
                   </div>

               </form>
           </div>

           <div className="col s4">
           </div>

           </div>
            )
        }
    }
}

export default LoginForm
