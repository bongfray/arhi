import React, { Component } from 'react';
import axios from 'axios'
import {API_URL} from '../utils/apiUrl'
import { Link } from 'react-router-dom';
import M from 'materialize-css';
//import { } from 'materialize-css';
import './style.css'
var empty = require('is-empty');


class Signup extends Component {
	constructor() {
    super()
    this.state = {
        title: '',
        name: '',
        id: '',
        username: '',
        phone: '',
        password: '',
        cnf_pswd: '',
        campus: '',
        dept: '',
        desgn: ''
    }
    this.handleTitle = this.handleTitle.bind(this)
    this.handleName = this.handleName.bind(this)
    this.handleId = this.handleId.bind(this)
    this.handleEmail = this.handleEmail.bind(this)
    this.handlePhone = this.handlePhone.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.handleConfirmPassword = this.handleConfirmPassword.bind(this)
    this.handleCampus = this.handleCampus.bind(this)
    this.handleDept = this.handleDept.bind(this)
    this.handleDesgn = this.handleDesgn.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleTitle = (e) => {
            this.setState({
                title: e.target.value
            });
        };
    handleName = (e) => {
            this.setState({
                name: e.target.value
            });
        };
    handleId = (e) => {
            this.setState({
                id: e.target.value
            });
        };
    handleEmail = (e) => {
            this.setState({
                username: e.target.value
            });
        };
    handlePhone = (e) => {
            this.setState({
                phone: e.target.value
            });
        };
    handlePasswordChange = (e) => {
            this.setState({
              password: e.target.value
            });
        };
    handleConfirmPassword = (e) => {
              this.setState({
                  cnf_pswd: e.target.value
                })
        };
    handleCampus = (e) => {
            this.setState({
                campus: e.target.value
            });
        };
    handleDept = (e) => {
            this.setState({
                dept: e.target.value
            });
        };
    handleDesgn = (e) => {
            this.setState({
                desgn: e.target.value
            });
        };
handleSubmit(event) {
  //console.log('sign-up handleSubmit, username: ')
  //console.log(this.state.username)
  event.preventDefault()
  if(empty(this.state.title)||empty(this.state.name)||empty(this.state.id)||empty(this.state.username)||empty(this.state.phone)||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.dept)||empty(this.state.desgn))
  {
      window.M.toast({html: 'Enter all the Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
      this.setState({
          cnf_pswd:''
      })
      return false;
  }
  else if(this.state.password !==this.state.cnf_pswd){
     window.M.toast({html: 'Password does not match', outDuration:'850', inDuration:'800', displayLength:'1500'});
     this.setState({
         cnf_pswd:''
     })
     return false;
       // The form won't submit
  }
	else if ((this.state.phone).length===10) {
		return true;
	}
	else if ((this.state.phone).length!==10) {
		window.M.toast({html: 'Enter correct format of Phone no', outDuration:'850', inDuration:'800', displayLength:'1500'});
		this.setState({
		  phone: ''
		})
	}
  else{
  axios.post(API_URL+'/user/', {
    title: this.state.title,
    name: this.state.name,
    id: this.state.id,
    username: this.state.username,
    phone: this.state.phone,
    password: this.state.password,
    campus: this.state.campus,
    dept: this.state.dept,
    desgn:this.state.desgn
  })
    .then(response => {
      console.log(response)
      if (!response.data.errmsg) {
					window.M.toast({html: 'Signedup !! Now you can signin', outDuration:'850', inDuration:'800', displayLength:'1500'});
        //console.log('successful signup')
        window.location.assign("/");
      } else {
					window.M.toast({html: 'Already user with same mail available!!', outDuration:'850', inDuration:'800', displayLength:'1500'});
        //console.log('username already taken')
      }
    }).catch(error => {
			window.M.toast({html: 'Signup Error', outDuration:'850', inDuration:'800', displayLength:'1500'});
      //console.log('signup error: ')
      //console.log(error)

    })
    this.setState({
    title: '',
    name: '',
    id: '',
    username: '',
    phone: '',
    password: '',
    cnf_pswd: '',
    campus: 'default',
    dept: 'default',
    desgn: 'default'

})
  }
}
  notifi = () => window.M.toast({html: 'Enter Details', outDuration:'850', inDuration:'800', displayLength:'1500'});

    componentDidMount() {
        this.notifi();
        M.AutoInit();
    }


render() {
	return (
		<div className="row">

		<div className="col s2">
		</div>

		<div className="col s8 form-signup">
				<div className="ew center">
						<h5 className="reg">REGISTRATION</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">

								<div className="input-field col s2">
										<select value={this.state.title} onChange={this.handleTitle}>
										<option value="" disabled selected>Title</option>
										<option value="Mr.">Mr.</option>
										<option value="Mrs.">Mrs.</option>
										<option value="Miss.">Miss.</option>
										<option value="Dr.">Dr.</option>
										</select>
								</div>

								<div className="input-field col s6">
								<input id="name" type="text" className="validate" value={this.state.name} onChange={this.handleName} required />
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s4">
								<input id="fac_id" type="text" className="validate" value={this.state.id} onChange={this.handleId} required />
								<label htmlFor="fac_id">ID</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s8">
								<input id="email" type="email" className="validate" value={this.state.username} onChange={this.handleEmail} required />
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4">
								<input id="ph_num" type="text" className="validate" value={this.state.phone} onChange={this.handlePhone} required />
								<label htmlFor="ph_num">Phone Number</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6">
								<input onChange={this.handlePasswordChange} id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								</div>

								<div className="input-field col s6">
								<input onChange={this.handleConfirmPassword} id="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>

						</div>
						<div className="input-field row">

								<div className="input-field col s4">
										<select value={this.state.campus} onChange={this.handleCampus}>
										<option value="" disabled selected>Campus</option>
										<option value="ktr">Kattankulathur Campus</option>
										<option value="rpr">Ramapuram Campus</option>
										<option value="vpl">Vadapalani Campus</option>
										<option value="ncr">NCR Campus</option>
										</select>
								</div>
								<div className="input-field col s4">
										<select value={this.state.dept} onChange={this.handleDept}>
										<option value="" disabled selected>Department</option>
										<option value="cse">Computer Science</option>
										<option value="it">Information Technology</option>
										<option value="swe">Software</option>
										<option value="mech">Mechanical</option>
										</select>
								</div>
								<div className="input-field col s4">
										<select value={this.state.desgn} onChange={this.handleDesgn}>
										<option value="" disabled selected>Designation</option>
										<option value="hod">HOD</option>
										<option value="prf">Professor</option>
										<option value="aprf">Associate Professor</option>
										<option value="asprf">Assistant Professor</option>

										</select>
								</div>


						</div>
						<br/>
						<div>
						<Link to='/' className="col s3  log"> <b> Login Instead ?</b></Link>

						<div className="col s5"></div>
						<Link href="#" className="waves-effect waves-light btn blue-grey darken-2 col s4 sup" onClick={this.handleSubmit}>Submit</Link>
						</div>
				</form>
		</div>



		</div>

	)
}
}

export default Signup
