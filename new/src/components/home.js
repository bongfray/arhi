import React, { Component } from 'react'

class Home extends Component {

  notify = () => window.M.toast({html: 'Welcome to Dashboard!', outDuration:'850', inDuration:'800', displayLength:'1500'});
  componentDidMount(){
    this.notify();
  }
    render() {
        return (
            <div>
              <h3 className="center">Welcome to the Home</h3>
            </div>
        )

    }
}

export default Home
