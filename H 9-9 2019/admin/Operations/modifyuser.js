import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'


export default class ModifyUser extends Component{
    constructor(){
        super();
        this.state={
            dayorder: '',
            day: '',
            month: '',
            year: '',
            sday: '',
            smonth: '',
            syear: '',
            eday: '',
            emonth: '',
            eyear: ''
        }
    }
    handleDayorder = (e) =>{
        this.setState({
            dayorder: e.target.value
        })
    }
    handleDay = (e) =>{
        this.setState({
            day: e.target.value
        })
    }
    handleMonth = (e) =>{
        this.setState({
            month: e.target.value
        })
    }
    handleYear = (e) =>{
        this.setState({
            year: e.target.value
        })
    }
    handleSday = (e) =>{
        this.setState({
            sday: e.target.value
        })
    }
    handleSmonth = (e) =>{
        this.setState({
            smonth: e.target.value
        })
    }
    handleSyear = (e) =>{
        this.setState({
            syear: e.target.value
        })
    }
    handleEday = (e) =>{
        this.setState({
            eday: e.target.value
        })
    }
    handleEmonth = (e) =>{
        this.setState({
            emonth: e.target.value
        })
    }
    handlEyear = (e) =>{
        this.setState({
            eyear: e.target.value
        })
    }
    handleSubmit(event){

    }
    handleHoliday(event){

    }
    handleStart(event){

    }
    handleEnd(event){

    }

    render(){
        if(this.props.select === 'start-semester'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="sday" type="text" className="validate" value={this.state.sday} onChange={this.handleSday} required />
								<label htmlFor="sday">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="smonth" type="text" className="validate" value={this.state.smonth} onChange={this.handleSmonth} required />
								<label htmlFor="smonth">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="syear" type="text" className="validate" value={this.state.syear} onChange={this.handleSyear} required />
								<label htmlFor="syear">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleStart}>Start Semester</Link>
                </div>

            )
        }
        else if(this.props.select === 'end-semester'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="eday" type="text" className="validate" value={this.state.eday} onChange={this.handleEday} required />
								<label htmlFor="eday">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="emonth" type="text" className="validate" value={this.state.emonth} onChange={this.handleEmonth} required />
								<label htmlFor="emonth">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="eyear" type="text" className="validate" value={this.state.eyear} onChange={this.handleEyear} required />
								<label htmlFor="eyear">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleEnd}>End Semester</Link>
                </div>

            )
        }
        else if(this.props.select === 'modifytoday'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="dayorder" type="text" className="validate" value={this.state.dayorder} onChange={this.handleDayorder} required />
								<label htmlFor="dayorder">Reset Today's Day Order to:</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Reset Day Order</Link>
                </div>

            )
        }
        else if(this.props.select === 'holiday'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="day" type="text" className="validate" value={this.state.day} onChange={this.handleDay} required />
								<label htmlFor="day">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="month" type="text" className="validate" value={this.state.month} onChange={this.handleMonth} required />
								<label htmlFor="month">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="year" type="text" className="validate" value={this.state.year} onChange={this.handleYear} required />
								<label htmlFor="year">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleHoliday}>Declare a Holiday</Link>
                </div>

            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}
