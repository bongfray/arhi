
import React from 'react'
import { Route, Link } from 'react-router-dom'
import {Modal} from 'react-materialize'
import './components/style.css'
const Insta = props => {

     const divStyle = {
          display: props.displayModal ? 'block' : 'none',
          marginTop:'150px',
     };
     function closeModal(e) {
        e.stopPropagation()
        props.closeModal()
     }
     return (
       <div
         className="modal instructionmodal"
         style={divStyle}
         onClick={ closeModal }>
         <div className="modal-content">
           <h4 className="center">eWork</h4><br /><br />
           <div className="mcont row"  style={{marginBottom:'35px'}}>
             <Link to="/fachome" className="col s5 waves-effect btn #311b92 deep-purple darken-4">eWork for Faculty</Link>
             <div className="col s2"></div>
             <Link to="/stud_home" className="col s5 waves-effect btn #00c853 green accent-4">eWork for Student</Link>
           </div>
         </div>
{ /*        <div className="modal-footer">
           <a href="#!" className="modal-close waves-effect waves-green btn-flat" onClick={ closeModal }>Close</a>
         </div> */}
       </div>
     );
}
export default Insta;
