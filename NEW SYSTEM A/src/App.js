import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import Signup from './components/sign-up'
import SSignup from './components/student/ssignup'
import LoginForm from './components/login-form'
import SLoginForm from './components/student/slogin'
import Navbar from './components/navbar'
import PartB from './components/Sections/par'
import PartA from './components/Sections/PartA'
import Home from './components/Home'
import Home_Stud from './components/Home_stud'
import Dash from './components/DashBoard/dash/global-data/Dashboard'
import Profile from './components/Profile/profile'
import Sprofile from './components/student/studentProfile'
import Con from './components/online-offline/Connection'
import DashProf from './components/Profile/DashProf'
import Home2 from './components/Home2'
import Scroll from './components/Top/scroll_top'
import TimeTab2 from './components/TimeTable/time2'
import TimeTabYes from './components/TimeTable/yesterday_slot'
import DashProf2 from './components/Profile/NewDash'
import Dash3 from './components/Profile/exdash'
import TimeTable from './components/Schedule/Timetable'
import Color from './components/TimeTable/colordiv'
import ResetPass from './components/Reset/reset_password'
import Slot2 from './components/TimeTable/slot2'
import NotFound from './components/default'
import NaVM from './nav-modal'
import './style.css'
require('disable-react-devtools');

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      day_order: '',
      time: '',
      modal: false,

    }

    this.getUser = this.getUser.bind(this)
    this.getanother = this.getanother.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }


  updateUser (userObject) {
    this.setState(userObject)
  }

getanother = () =>{
  axios.get('/user2/').then(response => {
    this.setState({
      loggedIn: true,
      username: response.data.user.username,
    })
  })
}

  getUser() {
    axios.get('/user/').then(response => {
      // console.log('Get user response: ')
      // console.log(response.data)
      if(response.data.user)
      {
      if(response.data.user === false) {
        // console.log('Get User: There is a user saved in the server session: ')
          this.getanother();
      }
      else
      {
        this.setState({
          loggedIn: true,
          username: response.data.user.username,

        })
      }
    }
       else {
        this.setState({
          loggedIn: false,
          username: null,

        })
      }
    })
  }


openModal =(info) =>
{
    this.setState({modal: !this.state.modal})
}

  componentDidMount() {
    this.openModal();
     this.getUser();
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }


  render() {
    return (
       <div className="App">

       <NaVM
           displayModal={this.state.modal}
           closeModal={this.openModal}
       />


        {/*<Con />*/} {/*This is for checking the connectivity */}

        <Route exact path="/fachome" render={() =>
          <Home
         loggedIn={this.state.loggedIn}
         />}
       />

       <Route exact path="/stud_home" render={() =>
         <Home_Stud
        loggedIn={this.state.loggedIn}
        />}
      />

        <Route path="/login" render={() =>
          <LoginForm
         updateUser={this.updateUser} loggedIn={this.state.loggedIn}
         />}
       />

       <Route path="/slogin" render={() =>
         <SLoginForm
        updateUser={this.updateUser} loggedIn={this.state.loggedIn}
        />}
      />

       <Route path="/signup" component={Signup} />

       <Route path="/ssignup" component={SSignup} />
       <Route path="/sprofile" component={Sprofile} />



      <Route path="/partB" render={() =>
        <PartB
       updateUser={this.updateUser} loggedIn={this.state.loggedIn}
       />} />
       <Route path="/partA" render={() =>
         <PartA loggedIn={this.state.loggedIn}
        />} />


       <Route path="/profile1" render={() =>
         <Profile username={this.state.username} loggedIn={this.state.loggedIn}
        />} />

       <Route path="/dashprof" component={DashProf} />
       <Route path="/dash2" render={() =>
         <DashProf2 loggedIn={this.state.loggedIn}
        />} />
       <Route path="/home" component={Home2} />
       <Route path="/Dash" render={() =>
         <Dash
        updateUser={this.updateUser} loggedIn={this.state.loggedIn}
        />} />

         <Route path="/newd" component={Dash3} />
         <Route path="/slot" component={Slot2} />
         <Route path="/blue_print" component={TimeTable} />
         <Route path="/reset_password/:token" component={ResetPass} />


          <Route path="/time_new" render={() =>
            <TimeTab2 loggedIn={this.state.loggedIn} username={this.state.username}
           />} />
           <Route path="/time_yes" render={() =>
             <TimeTabYes loggedIn={this.state.loggedIn} username={this.state.username}
            />} />

            <Route path="/no" component={NotFound} />

         {/*<Scroll/>*/}
      </div>
    );
  }
}

export default App;
