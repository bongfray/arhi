import React, { Component } from 'react'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Particles from 'react-particles-js';
import newId from '../Sections/id'
import Pari from "../Sections/tab";
import '../style.css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'

class Navbar extends Component {
  constructor(){
    super()
    this.state = {
      test:'Arijit',
      username:'',
      name:'',
      phone:'',
      campus:'',
      dept:'',
      desgn:''
    }
    this.getData = this.getData.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  getData() {

  }

    componentDidMount() {
      axios.get('/user/dash2').then(response => {

        // alert(response.data.campus);
        if(response.status === 200)
        {
          this.setState({
            username: response.data.username,
            name: response.data.name,
            phone: response.data.phone,
            dept: response.data.dept,
            campus: response.data.campus,
            desgn: response.data.desgn
          })
        }
          // alert(this.state.name);


      })
      this.getData()
      M.AutoInit()
  }

    render() {
      const name = this.state.username;
      alert(name);
      var data = {
        instances: [
        {
          date_created: this.state.test,
          date: this.state.username
        }
      ],
            fielddata: [
              {
                header: "No. ",
                inputfield: true,
                length: 10,
                name: "date_created",
                placeholder: "",
                content: "text",
                grid : "two",
              },
              {
                header: "Date",
                inputfield: true,
                length: 10,
                name: "date",
                placeholder: "",
                content: "text",
                grid : "two",
              },
              {
                header: "Conference Title",
                inputfield: true,
                length: 10,
                name: "conft",
                content: "text",
                grid : "two",
                placeholder: ""
              },
              {
                header: "Location",
                inputfield: true,
                length: 7,
                name: "loc",
                content: "text",
                grid : "two",
                placeholder: ""
              },
              {
                header: "Mention Your Role",
                inputfield: true,
                length: 7,
                name: "role",
                content: "text",
                grid : "two",
                placeholder: ""
              }
            ]
          };



        const loggedIn = this.props.loggedIn;

              return(
              <div className="grid-of-parts">

<h5>{this.state.username}</h5>
            {/* Here start Sections codes of collapsible */}

              <Collapsible popout>
                    <CollapsibleItem header={<div className="row">
                        <div className="col s6">
                        <h5 className="">Professional Development Activities</h5>
                        </div>
                          </div>
                      } expanded>
       <div className="row">
          <div className="col s1"><h5 className="right td ft">A .</h5>
          </div>
       <div className="col s11 td">
       <h5 className="ft">{this.state.username}</h5>
       </div>
     </div>
<br />
<br />
     <div className="row">
       <Pari data={data} />
     </div>
  </CollapsibleItem>
</Collapsible>

{/*--------------------------------------------------------------------------------------------End of Profrssional Development Activities */}


              </div>
            );





    }
}

export default Navbar
