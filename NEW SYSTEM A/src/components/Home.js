import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Slogo from './Slogo.png'
import Nav from './dynnav'
import './style.css'
import M from 'materialize-css'
import Fhome from './fhome2.png'

class FrontPage extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/newd',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/user/').then(response => {
      if (response.data.user) {
          this.setState({display:'disabled'})
      }
        })
      }


    componentDidMount() {
        this.getUser();
        M.AutoInit();
        let elems = document.querySelectorAll(".modal-trigger");
        M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });

    }
    render(){
        return(
          <React.Fragment>
          <Nav content={this.state.content} get={this.state.get} logout={this.state.logout}/>
            <div className="row">
                <div className="col s12 l6 m12">
                    <div className="fstyle">
                    <center><div className="title-of-home"><h4  className="ework_name">E-Work</h4></div></center><br /><br />
                    <p className="fpara">Ework is a simplified analytics tool.It is a tool to keep trace and record of each and everyday routine of staff members of the institute.E-Work is a tool designed by CARE which majorly focuses on the encouragement of the well deserved ones. </p><br/><br/>
                    <div className="row">
                    <div className="col l1 m1 hide-on-down" />
                      <div className="col l4 s12 m12">
                        <a href="#modal2" style={{width:'100%',}} className={"left waves-effect btn #03a9f4 light-blue modal-trigger"+this.state.display}><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></a>
                      </div>
                      <div id="modal2" className="modal modal-fixed z-depth-5 modaln">
                     <div className="modal-content">
                     <div className="mcont"  style={{marginBottom:'35px'}}>
                       <Link to="/login" className="col s5 waves-effect btn #311b92 deep-purple darken-4">Faculty</Link>
                       <div className="col s2"></div>
                       <Link to="/slogin" className="col s5 waves-effect btn #00c853 green accent-4">Student</Link>
                     </div>
                     </div>

                     </div>
                      <div className="col l2 m2 hide-on-down"></div>
                      <div className="col l4 s12 m12">
                       <Link to="#" style={{width:'100%'}} className="waves-effect btn #c0ca33 lime darken-1 right"><i className="material-icons right">desktop_mac</i><b>About</b></Link>
                      </div>
                      <div className="col l1 m1 hide-on-down" />
                    </div>
                    <a className=" col s3 offset-l9 offset-s5 offset-m5 fprivacy blink modal-trigger" href="#modal1">Privacy Policy</a>
                    <div id="modal1" className="modal modal-fixed-footer modal-fixed">
                <div className="modal-content">
                  <h4 className="mheader center">Privacy Policy</h4>
                  <p className="mcont center">This section will be updated soon !!</p>
                </div>
                <div className="modal-footer">
                  <a href="#!" className="modal-close btn-flat"><i className="material-icons right">close</i>Close</a>
                </div>
                </div>


                    </div>
                </div>
                <div className="col l6 hide-on-med-and-down">
                    <img className="img-home imgf" src={Fhome} alt=""/>
                </div>
            </div>
            </React.Fragment>
        );
    }
}

export default FrontPage;
