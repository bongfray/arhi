import React from "react";
import { Link } from "react-router-dom";
import newId from './newId'
import '../style.css'
/* ----------- BUTTONS START ----------- */
class ButtonGroup extends React.Component {
  componentDidMount() {
    this.id = newId();
  }
  render() {
    return (
      <div className="right" key={this.id}>
        {this.props.buttons}
      </div>
    );
  }
}

class UploadButton extends React.Component {
  render() {
    return (
      <Link
        to="#"
        onClick={this.props.onClick}
        className=" edit btn btn-small blue-grey darken-2 sup  uploadalignB"
      >
        UPLOAD
      </Link>
    );
  }
}

class EditButton extends React.Component {
  render() {
    return (
      <button
        type="button"
        onClick={this.props.onClick}
        className=" edit btn-small btnalign green"
      >
        EDIT
      </button>
    );
  }
}

class DeleteButton extends React.Component {
  render() {
    return (
      <button
        type="button"
        onClick={this.props.onClick}
        className=" edit btn-small btnalign red"
      >
        Delete
      </button>
    );
  }
}

class ConfirmButton extends React.Component {
  render() {
    return (
      <button
        type="button"
        onClick={this.props.onClick}
        className=" edit btn-small btnalign pink"
      >
        SAVE
      </button>
    );
  }
}

class CancelButton extends React.Component {
  render() {
    return (
      <button
        type="button"
        onClick={this.props.onClick}
        className="edit btn-small btnalign red"
      >
        CANCEL
      </button>
    );
  }
}

class FullWidthButton extends React.Component {
  render() {
    return (
      <button
        type="button"
        onClick={this.props.onClick}
        className="btn blue-grey darken-2 sup subm"
      >
        {this.props.buttontext}
      </button>
    );
  }
}

/* ----------- BUTTONS END ----------- */

class DisplayField extends React.Component {
  render() {
    console.log(this.props.field.value);
    return (
      <td
        className="col s2 svalign aligncenter"
        key={this.id}
        onClick={this.props.onClick}
      >
        <b>{this.props.field.value}</b>
      </td>
    );
  }
}

class EditField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.field.value
    };
    this.handleFieldChange = this.handleFieldChange.bind(this);
  }
  handleFieldChange(e) {
    this.setState({
      value: e.target.value
    });
    // Send value back to row - note: state is too slow
    this.props.sendValueToParent(this.props.field.name, e.target.value);
  }
  render() {
    if (this.props.field.content === "text") {
      if (this.props.field.grid === "one") {
        return (
          <td className="col s1">
            <label className="pure-material-textfield-outlined align1">
              <textarea
                className="area"
                type="text"
                placeholder=" "
                min="10"
                max="60"
                value={this.state.value}
                onChange={this.handleFieldChange}
              />
              <span>Enter your Details</span>
            </label>
          </td>
        );
      } else if (this.props.field.grid === "two") {
        return (
          <td className="col s2">
            <label className="pure-material-textfield-outlined align">
              <textarea
                className="area"
                type="text"
                min="10"
                max="60"
                placeholder=" "
                value={this.state.value}
                onChange={this.handleFieldChange}
              />
              <span>Enter your Details</span>
            </label>
          </td>
        );
      } else if (this.props.field.grid === "three") {
        return (
          <td className="col s3">
            <label className="pure-material-textfield-outlined align3">
              <textarea
                className="area"
                type="text"
                placeholder=" "
                min="10"
                max="60"
                value={this.state.value}
                onChange={this.handleFieldChange}
              />
              <span>Enter your Details</span>
            </label>
          </td>
        );
      } else if (this.props.field.grid === "four") {
        return (
          <td className="col s4">
            <label className="pure-material-textfield-outlined align4">
              <textarea
                className="area"
                type="text"
                placeholder=" "
                min="10"
                max="60"
                value={this.state.value}
                onChange={this.handleFieldChange}
              />
              <span>Enter your Details</span>
            </label>
          </td>
        );
      }
    } else if (this.props.field.content === "date") {
      return (
        <td className="col s2 align">
          <input
            type="text"
            value={this.state.value}
            onChange={this.handleFieldChange}
            className="datepicker"
            placeholder="Date"
            id="s2"
          />
        </td>
      );
    } else if (this.props.field.content === "idtype") {
      return (
        <td className="col s2 align ">
          <span className="aligncenter" onChange={this.handleFieldChange}>
            {this.state.value}
          </span>
        </td>
      );
    }
  }
}

class TableRow extends React.Component {
  constructor(props) {
    super(props);

    this.handleSelectRow = this.handleSelectRow.bind(this);
    this.handleEditModeClick = this.handleEditModeClick.bind(this);
    this.handleExitEditModeClick = this.handleExitEditModeClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleConfirmEditClick = this.handleConfirmEditClick.bind(this);
    this.updateValues = this.updateValues.bind(this);

    var editMode = false;
    var added = false;
    // If no created date then row is a newly added row
    if (!this.props.instance.date_created) {
      editMode = true;
      added = true;
    }
    var instance = {};

    this.props.fielddata.forEach(function(fd) {
      instance[fd.name] = this.props.instance[fd.name];
      //console.log(instance);
    }, this);

    this.state = {
      editMode: editMode,
      instance: instance,
      revised_instance: instance,
      deleted: false,
      added: added,
      selected: false
    };
  }

  handleSelectRow() {
    // Need a call to parent as only one row may be selected?
    if (!this.state.selected) {
      this.setState({ selected: true });
    } else {
      this.setState({ selected: false });
    }
    console.log(this.props.instance.id);
    this.props.setSelectedRow(this.props.instance.id);
  }

  handleEditModeClick() {
    this.setState({ editMode: true });
  }

  handleExitEditModeClick() {
    this.setState({ editMode: false });
    this.setState({
      revised_instance: this.state.instance
    });
    if (this.state.added) {
      this.setState({ deleted: true });
    }
  }

  handleDeleteClick() {
    this.setState({ deleted: true });
    console.log("AJAX DELETE");
    console.log(this.state.instance.id);
  }
  handleUploadClick() {

   }
  handleConfirmEditClick() {
    // CONFIRM revised_instance
    this.setState({
      editMode: false,
      instance: this.state.revised_instance
    });
    if (this.state.added) {
      console.log("AJAX POST");
    } else {
      console.log("AJAX PATCH");

      // Reset added flag
      this.setState({ added: false });
    }
  }

  updateValues(key, value) {
    // Method to update values passed from EditField
    var temp_revised_instance = this.state.revised_instance;
    temp_revised_instance[key] = value;
    this.setState({
      revised_instance: temp_revised_instance
    });
  }

  render() {
    let row = [];
    let buttons = null;
    if (this.state.deleted) {
      return <tr />;
    }
    if (this.state.editMode) {
      // In edit mode - set buttons for edit
      buttons = [
        <ConfirmButton key={this.id} onClick={this.handleConfirmEditClick} />,
        <CancelButton key={this.id} onClick={this.handleExitEditModeClick} />
      ];
      // In edit mode - set field values and placeholders
      this.props.fielddata.forEach(function(fd) {
        var field = {
          name: fd.name,
          grid: fd.grid,
          content: fd.content,
          value: this.state.instance[fd.name]
        };
        // In edit mode - add EditFields for editable fields
        if (fd.inputfield) {
          row.push(
            <EditField
              field={field}
              key={field.name}
              sendValueToParent={this.updateValues}
            />
          );
        } else {
          row.push(<DisplayField onClick={""} field={""} key={field.name} />);
        }
      }, this);
    } else {
      // In display mode - add edit/delete buttons
      buttons = [
        <EditButton key={this.id} onClick={this.handleEditModeClick} />,
        <DeleteButton key={this.id} onClick={this.handleDeleteClick} />,
        <br />,
        <UploadButton key={this.id} onClick={this.handleUploadClick} />
      ];
      // In display mode - add DisplayField
      this.props.fielddata.forEach(function(fd) {
        var field = {
          name: fd.name,
          value: this.state.instance[fd.name]
        };
        row.push(
          <DisplayField
            onClick={this.handleSelectRow}
            field={field}
            key={field.value}
          />
        );
      }, this);
    }
    // Set selected status
    if (this.props.instance.selected) {
      return (
        <tr className="success" key={this.id}>
          {row}
          <ButtonGroup buttons={buttons} />
        </tr>
      );
    } else {
      return (
        <tr>
          {row}
          <ButtonGroup buttons={buttons} />
        </tr>
      );
    }
  }
}

class TableHeader extends React.Component {
  render() {
    var tableheaders = [];
    this.props.fielddata.forEach(function(fd) {
      if (fd.grid === "one") {
        tableheaders.push(
          <th className="col s1 aligncenter">
            <b className="aligncenter">{fd.header}</b>
          </th>
        );
      } else if (fd.grid === "two") {
        tableheaders.push(
          <th className="col s2 aligncenter">
            <b className="aligncenter">{fd.header}</b>
          </th>
        );
      } else if (fd.grid === "three") {
        tableheaders.push(
          <th className="col s3 aligncenter">
            <b className="aligncenter">{fd.header}</b>
          </th>
        );
      }
    });
    tableheaders.push(
      <th className="center col s2">
        <b className="aligncenter">Actions</b>
      </th>
    );
    return <tr>{tableheaders}</tr>;
  }
}

class Table extends React.Component {
  constructor(props) {
    super(props);
    // Set state variable for selected row
    this.state = {
      selected: 0
    };
    this.setSelectedRow = this.setSelectedRow.bind(this);
  }

  setSelectedRow(id) {
    this.setState({ selected: id });
    console.log(id);
    this.props.data.instances.forEach(function(instance) {
      if (instance.id == id) {
        console.log(instance.childlinks);
        this.props.onSelect(instance.childlinks);
      }
    }, this);
  }

  render() {
    var rows = [];
    const fielddata = this.props.data.fielddata;
    this.props.data.instances.forEach(function(instance) {
      if (instance.id == this.state.selected) {
        instance.selected = true;
      } else {
        instance.selected = false;
      }
      rows.push(
        <TableRow
          setSelectedRow={this.setSelectedRow}
          instance={instance}
          fielddata={fielddata}
          key={instance.id}
        />
      );
    }, this);
    return (
      <table className="table table-striped">
        <thead>
          <TableHeader fielddata={fielddata} />
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class ChildLinks extends React.Component {
  render() {
    var render_links = [];

    return (
      <div id="childLinks" className="row">
        {render_links}
      </div>
    );
  }
}

export default class TableContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addMode: false,
      childlinks: this.props.data.childlinks
    };
    this.handleAddClick = this.handleAddClick.bind(this);
    this.setChildlinks = this.setChildlinks.bind(this);
    this.getData();
  }

  handleAddClick() {
    this.setState({ addMode: true });
  }

  getData() {
    console.log("AJAX GET");
  }

  setChildlinks(childlinks) {
    this.setState({ childlinks: childlinks });
    // This is too slow - we need to call
    // a method to update directly on childlinks as passed, which is correct
    console.log(this.state.childlinks);
  }

  render() {
    if (this.state.addMode) {
      var instance = {};
      this.props.data.fielddata.forEach(function(fd) {
        instance[fd.name] = "";
      });
      var dataout = this.props.data;
      dataout.instances.push(instance);
    } else {
      dataout = this.props.data;
    }
    if (dataout.instances.length > 0) {
    }
    return (
      <div>
        <div className="table-responsive">
          <legend>{this.props.title}</legend>
          <Table data={dataout} onSelect={this.setChildlinks} />
          <br />
          <FullWidthButton
            buttontext={"Add Row"}
            onClick={this.handleAddClick}
          />
        </div>
      </div>
    );
  }
}
