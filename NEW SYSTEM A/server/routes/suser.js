const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express')
const route = express.Router()
const bcrypt = require('bcryptjs');
const Suser = require('../database/models/STUD/suser')
const passport = require('../passport/stud_pass')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')

console.log("Hi Student Here")


route.post('/ssignup', (req, res) => {
console.log("Stud REG");
    const { regid, password,name,mailid,phone,campus,dept,dob,count,degree,st_year,fn_year} = req.body
    Suser.findOne({ username: regid }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          console.log("Way to signup")
            const newUser = new Suser({
              username: regid,
              password: password,
              name: name,
              mailid: mailid,
              phone: phone,
              campus: campus,
              dept: dept,
              dob: dob,
              degree: degree,
              st_year: st_year,
              fn_year: fn_year,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              count: count,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})


route.post(
    '/slogin',(req, res, next) => {

console.log("In Student dB");
      const { username, password} = req.body;


      Suser.findOne({ username: username }, function(err, objs){
        if(err)
        {
          return;
        }
        else if(objs)
        {
         if (objs.count === "0")
          {
              var counter = 0;
              Suser.updateOne({ username: username }, {count: counter+1},(err, user) => {

              })

          }
          else if (objs.count === "1")
          {
              var coun = 0;
              Suser.updateOne({ username: username }, {count: coun+2},(err, user) => {

              })
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)


route.get('/getstudent', (req, res, next) => {
  console.log(req.user.username)
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})



route.post('/slogout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})





route.post('/sprofile', (req, res) => {

    const {id,up_username,up_phone,up_name,up_dob} = req.body;
    // var password = req.body.new_password;
    Suser.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          // let exist_password = req.body.current_password;
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            // console.log("Successful Authen");
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            // console.log(hash);
            Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              // console.log("Successful Hash");
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)



route.get('/getstudentprofile', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    Suser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});


module.exports = route
