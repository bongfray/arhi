import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import ContentLoader from "react-content-loader"
import {Link } from 'react-router-dom'
import './css_home.css'
import Nav from '../dynnav'
import CompleteHistory from './fetchClassComplete';
import {Typography} from '@material-ui/core';

class FrontPage extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      noti_route:false,
      theme:false,
      complete_class:[],
      facultylist:[],
      show_page:false,
      loader:true,
      username:'',
      disabled:false,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/ework/user2/getstudent')
    .then(response => {
          if (response.data.user)
          {
              this.setState({display:'disabled',theme:response.data.user.nightmode,disabled:true,username:response.data.user.username});
              this.fetchClass_Complete_History();
          }
          else
          {
            this.setState({loader:false})
          }
        })
      }


changeTheme = () => {
  this.setState({
    theme : !this.state.theme
  });
  axios.post('/ework/user2/theme_change',{stat:!this.state.theme})
  .then( res => {

  });
}

fetchClass_Complete_History =()=>{
  axios.get('/ework/user2/fetchClass_Complete_History')
  .then( res => {
    if(res.data.completed.length>0 && res.data.facultylist.length>0)
    {
      this.setState({
        complete_class:res.data.completed,
        facultylist:res.data.facultylist,
      })
      this.showPage();
    }
    this.setState({loader:false})
  });
}

showPage=()=>{
  this.setState({
    show_page:!this.state.show_page,
  })
}



    componentDidMount() {
        this.getUser();
    }
    render(){
      const MyLoader = () => (
            <ContentLoader
              height={160}
              width={400}
              speed={2}
              primaryColor="#f3f3f3"
              secondaryColor="#c0c0c0"
            >
              <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
            </ContentLoader>
          )
       if(this.state.loader === true)
       {
         return(
           <MyLoader />
         )
       }
        else
        {
        return(
          <React.Fragment>
          {this.state.show_page &&
            <CompleteHistory completeHistory ={this.fetchClass_Complete_History} showPage={this.showPage}
            username={this.state.username} complete_class={this.state.complete_class} facultylist={this.state.facultylist} />}
          <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div className={this.state.theme ? "night" : "normal"} >
          <div className="wrapper bdy">

          <input  type="checkbox" style={{display:'none'}} checked={this.state.theme} id="light-switch" onClick={this.changeTheme} />
          <label htmlFor="light-switch" id="light-switch-label">
            <div className="switch"></div>
          </label>

        <div className="floor">

        </div>
        <div className="shelf-wrapper">
            <div className="shelf1">
                <div id="slantbook"></div>
                <div id="flatbook"></div>

            </div>
            <div className="shelf2">
                <div id="flatbook3"></div>
                <div className="penstand">
                    <div id="pen"></div>
                </div>

            </div>
            <div className="shelfknob">
                <div className="knob" id="knob1"></div>
                <div className="knob" id="knob2"></div>

            </div>

        </div>
        <div className="window">
            {this.state.theme=== false ? <span id="moon"></span> :
            <div id="sun"></div>}
        </div><br />
        {!this.state.disabled &&
          <Link to="/ework/slogin">
            <div className="directions" style={{float:'right'}}>
                <div id="Awesome" className="anim750">
                  <div className="sticky anim750">
                    <div className="front circle_wrapper anim750">
                      <div className="circle anim750"></div>
                    </div>
                  </div>
               <Typography align="center" style={{padding:'15px'}} variant="h6">Login & SignUP</Typography>
              </div>
            </div>
          </Link>
        }



        <div className="container">
            <div className="cup"></div>
            <div className="table">
                <span className="handle" id="handle1"></span>
                <span className="handle" id="handle2"></span>
                <div id="margintop">
                    <div className="tableleg" id="table0"></div>

                </div>
            </div>
            {this.state.theme === false && <div className="lamp">
                <div id="lampjoint"></div>
                <div className="lamphead"></div>
            </div>}

                <div style={{marginTop:'132px'}}>
                    <div className = "screen" >
                     <div className="name_e" style={{textAlign:'center'}}>eWork</div>

                    </div>
                 <div className = "keyboard">
                    <div className = "keyboard2">
                    </div>
                 </div>
               </div>



            <div className="semicircle"></div>
        </div>

    </div>
    </div>
            </React.Fragment>
        );
      }
    }
}

export default FrontPage;
