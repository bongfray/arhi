import React from 'react';
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      productId:0,
      render_confirm:false,
      fetching:true,

    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
    this.setState({render_confirm:!this.state.render_confirm,productId:productId});
  }

confirm=()=>{
  let delUrl;
  if(this.props.data.Action === 'FACULTY LIST'){
    delUrl = '/ework/user2/del_faculty_list';
  } else {
    delUrl = '/ework/user2/del';
  }
this.setState({fetching:true})
const { products } = this.state;
axios.post(delUrl,{
  serial: this.state.productId,
  username: this.props.username,
})
    .then(response => {
      if(this.props.oneEntry)
      {
        this.props.oneEntry({
          disabled:false
        })
      }
      this.setState({
        render_confirm:false,
        productId:0,
        response: response,
        products: products.filter(product => product.serial !== this.state.productId),
        fetching:false
     })
    })
}


fetch =() =>{
  let fetchUrl;

    fetchUrl = '/ework/user2/fetchall';

  axios.post(fetchUrl,{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    if(response.data.length>0)
    {
      if(this.props.oneEntry)
      {
        this.props.oneEntry({
          disabled:true
        })
      }
    }
    this.setState({
     products: response.data,
     fetching:false,
   })
  })
}



  render() {
    if(this.state.fetching)
    {
      return(
        <div className="load-me" style={{textAlign:'center'}}>
          <div class="spinner-box">
                <div class="blue-orbit leo">
                </div>

                <div class="green-orbit leo">
                </div>

                <div class="red-orbit leo">
                </div>
                <div class="white-orbit w1 leo">
                </div><div class="white-orbit w2 leo">
                </div><div class="white-orbit w3 leo">
                </div>
          </div>
          <Typography style={{color:'white'}} variant="h5" align="center">Processing Your Request....</Typography>
        </div>
      )
    }
    else
    {
    const { products} = this.state;
      return(
        <React.Fragment>
            <Dialog
              open={this.state.render_confirm}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">Confirmation of Deleting the Data</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  After you click agree button below, your all rating datas, related to this content will be affected.
                  So kindly take decision manually.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.deleteProduct} color="primary">
                  Disagree
                </Button>
                <Button onClick={this.confirm} color="primary" autoFocus>
                  Agree
                </Button>
              </DialogActions>
            </Dialog>


          <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{this.props.title}</Typography><br />
          {this.props.description &&
            <Typography variant="overline" style={{textAlign:'center'}} display="block" gutterBottom>
             {this.props.description}
            </Typography>
          }
          <br />
          <Grid container spacing={1}>
          {products.map((product,ind) => (
            <Grid item xs={12} sm={6} key={ind}>
                <Paper elevation={3} style={{padding:'8px'}}>
                   <Grid container spacing={1}>
                      <Grid item xs={4} sm={4} >
                        {this.props.data.fielddata.map((content,index)=>(
                          <React.Fragment key={index}>
                             <div ><b>{content.header}</b></div><br />
                          </React.Fragment>
                        ))}
                      </Grid>
                      <Grid item xs={8} sm={8}>
                      {!(this.props.referOutside) && <div style={{float:'right'}}>
                        <EditIcon className="go"  onClick={() => this.props.editProduct(product.serial,this.props.action)} />&nbsp;
                        <DeleteIcon className="go" style={{color:'red'}} onClick={() => this.deleteProduct(product.serial,this.props.action)} />
                      </div>}
                        {this.props.data.fielddata.map((content,ind)=>(
                          <React.Fragment key={ind}>
                            {(content.name === 'link' || content.name === 'blog_link' || content.name === 'playstore_link'
                          ||content.name === 'github_link' || content.name === 'hackerrank_link' || content.name === 'hackerearth_link' ||
                          content.name === 'linkedln_link' || content.name === 'other_link') ?
                             <a href={product[content.name]} target="blank">{product[content.name]}</a>
                             :
                             <div>{product[content.name]}</div>
                            }
                            <br />
                          </React.Fragment>
                        ))}
                      </Grid>
                   </Grid>
                </Paper>
            </Grid>
          ))}
          </Grid>
        </React.Fragment>
      )
    }

  }
}
