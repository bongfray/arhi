import React, { Component } from 'react';
import { } from 'materialize-css';
import Logo from './Logo.png';
import Doc from './DocService';
import Axios from 'axios';
import PdfContainer from './PdfContainer';
import Image from './image.png';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


class Resume extends Component {


  constructor(){
    super();
    this.state={
      loading:true,
      profile:[],
      skills:[],
      ssc:[],
      hsc:[],
      grad:[],
      postgrad:[],
      intern:[],
      projects:[],
      pos_res:[],
      train_cert:[],
      social:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.fetchProfile();
  }

  fetchProfile(){
    Axios.post('/ework/user2/fetchall',{
      action:'Personal_Details',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({profile:res.data})
    })
    this.fetchSkills()
  }

  fetchSkills(){
    Axios.post('/ework/user2/fetchall',{
      action:'Skills',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({skills:res.data})
    })
    this.fetchEducation()
  }

  fetchEducation(){
    Axios.post('/ework/user2/fetchall',{
      action:'10th Standard',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({ssc:res.data})
    })

    Axios.post('/ework/user2/fetchall',{
      action:'12th Standard',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({hsc:res.data})
    })

    Axios.post('/ework/user2/fetchall',{
      action:'Graduation',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({grad:res.data})
    })
    this.fetchInternship()
  }

  fetchInternship(){
    Axios.post('/ework/user2/fetchall',{
      action:'Internships',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({intern:res.data})
    })
    this.fetchProjects()
  }

  fetchProjects(){
    Axios.post('/ework/user2/fetchall',{
      action:'Projects',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({projects:res.data})
    })
    this.fetchResponsibility()
  }

  fetchResponsibility(){
    Axios.post('/ework/user2/fetchall',{
      action:'Position of Responsibility',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({pos_res:res.data})
    })
    this.fetchTraining()
  }

  fetchTraining(){
    Axios.post('/ework/user2/fetchall',{
      action:'Trainings or Certificates',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({train_cert:res.data})
    })
    this.fetchSocial()
  }

  fetchSocial(){
    Axios.post('/ework/user2/fetchall',{
      action:'Social Media Account',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({social:res.data})
    })
  }




  createPdf = (html) => Doc.createPdf(html);

  render() {


    return (
      <React.Fragment className="resume">

        <Grid container spacing={1}>
          <Grid item xs={3} sm={3} />
          <Grid item xs={6} sm={6}>
            <PdfContainer loading={this.state.loading} createPdf={this.createPdf}>
          <React.Fragment>
            <section className="resume-pdf-body">
              <Grid container spacing={1}>
               <Grid item xs={2} sm={2}>
                <img alt="prof" src={Logo} className="resume-logo"/>
               </Grid>
               <Grid item xs={8} sm={8}>
                  <Typography variant="h2" style={{  fontSize: '16px'}} align="center">SRM INSTITUTE OF SCIENCE AND TECHNOLOGY</Typography><br />
                  <p className="resume-p" style={{textAlign:'center',marginTop:'-3%', marginBottom:'-3%'}}>STUDENT RESUME</p>
               </Grid>
               <Grid item xs={2} sm={2}/>
              </Grid>
<br />
              <Grid container spacing={1}>
               <Grid item xs={10} sm={10}>
                 {this.state.profile.map((content,index)=>{
                   return(
                     <React.Fragment key={index}>
                     <p className="resume-gen">Name : {content.name} <br/>E-Mail : {content.emailid} <br/>Contact No. : {content.mobileno} <br /> Address : {content.address}</p>
                     </React.Fragment>
                   )
                 })}
               </Grid>
               <Grid item xs={2} sm={2}>
                 <img src={Image} className="col s2 resume-profile-image" alt=""/>
               </Grid>
              </Grid>

              <hr/>

              <Grid container spacing={1}>
                  <p className="resume-gen">
                  <b>Objective:</b> <br/> <br/> To obtain employment with a company that offers a positive atmosphere to learn and implement new skills and technologies for the betterment of the organization.</p>
              </Grid>

              <hr/>

              <Grid container spacing={1}>
                  <p className="resume-gen col s12"><b>Academic Qualification:</b> </p><br/> <br/>

                  <TableContainer >
                    <Table aria-label="simple table" className="resume-table">
                      <TableHead>
                        <TableRow>
                          <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>Degree/Course</TableCell>
                          <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>Institute Name</TableCell>
                          <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>Board</TableCell>
                          <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>Percentage/CGPA</TableCell>
                          <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>Year of Passing</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                      {this.state.grad.map((row,index)=>{
                        return(
                          <TableRow key={index}>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.action} ({row.stream})</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.college}</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.board}</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.cgpa_percentage}</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.end_year}</TableCell>
                          </TableRow>
                        )
                      })}
                      {this.state.hsc.map((row,index)=>{
                        return(
                          <TableRow key={index}>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>Higher Secondary Education</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.college}</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.board}</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.cgpa_percentage}</TableCell>
                            <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{row.end_year}</TableCell>
                          </TableRow>
                        )
                      })}
                        {this.state.ssc.map((row,index)=>{
                          return(
                            <TableRow key={index}>
                              <TableCell align="left" padding={'none'}  style={{fontSize:'10px'}}>Secondary Education</TableCell>
                              <TableCell align="left" padding={'none'}  style={{fontSize:'10px'}}>{row.college}</TableCell>
                              <TableCell align="left" padding={'none'}  style={{fontSize:'10px'}}>{row.board}</TableCell>
                              <TableCell align="left" padding={'none'}  style={{fontSize:'10px'}}>{row.cgpa_percentage}</TableCell>
                              <TableCell align="left" padding={'none'}  style={{fontSize:'10px'}}>{row.end_year}</TableCell>
                            </TableRow>
                          )
                        })}
                      </TableBody>
                    </Table>
                  </TableContainer>
              </Grid>
              <hr/>

              {this.state.skills.length>0 &&
                  <React.Fragment>
              <div style={{padding:'2px 0px 5px 0px'}}>
                  <p className="resume-gen"><b>Skills:</b></p>
                  <Grid container>
                    {this.state.skills.map((content,index)=>{
                      return(
                        <Grid item xs={6} sm={6} key={index}>
                          <span className="resume-gen">{index+1} {content.skill_name}</span><br />
                        </Grid>
                      )
                    })}
                  </Grid>
              </div>

              <hr/>
              </React.Fragment>
            }
              {this.state.intern.length>0 &&
                  <React.Fragment>
              <div style={{padding:'2px 0px 5px 0px'}}>
                  <p className="resume-gen"><b>Internship:</b></p>
                  {this.state.intern.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="resume-gen">{index+1}. <b>{content.profile}</b> <br/>{content.organization}, {content.location} <br/>Duration: {content.start_date} to {content.end_date}
                         <br/>
                         Link : <a href={content.link} target="_blank" rel="noopener noreferrer">{content.link}</a>
                         </div><br />
                      </div>
                    )
                  })}
              </div>
              <hr/>
              </React.Fragment>
            }
              {this.state.projects.length>0 &&
                  <React.Fragment>
              <div style={{padding:'2px 0px 5px 0px'}}>

                  <p className="resume-gen"><b>Projects:</b></p>

                  {this.state.projects.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="resume-gen">
                        {index+1}. <b>{content.title}</b> <br/>{content.description} <br/>
                        Duration: {content.start_date} to {content.end_date}
                        <br/>
                        Project Link: <a href={content.link} target="_blank" rel="noopener noreferrer">{content.link}</a>
                        </div><br />
                      </div>
                    )
                  })}
              </div>
              <hr/>
              </React.Fragment>
            }

              {this.state.pos_res.length>0 &&
                  <React.Fragment>
              <div style={{padding:'2px 0px 5px 0px'}}>
                  <p className="resume-gen"><b>Position of Responsibility:</b></p>
                  {this.state.pos_res.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="resume-gen">{index+1}. {content.description} <br/>Duration: {content.start_date} to {content.end_date}
                         <br/>
                         Link : <a href={content.link} target="_blank" rel="noopener noreferrer">{content.link}</a>
                        </div><br />
                      </div>
                    )
                  })}
              </div>

              <hr/>
              </React.Fragment>
            }

            {this.state.train_cert.length>0 &&
                <React.Fragment>
              <div style={{padding:'2px 0px 5px 0px'}}>
                  <p className="resume-gen"><b>Trainings or Certificates:</b></p>
                  {this.state.train_cert.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="resume-gen">{index+1}. <b>{content.program}</b> <br/>{content.organization}, {content.location} <br/>{content.description} <br/>Duration: {content.start_date} to {content.end_date}
                         <br/>
                         Link : <a href={content.link} target="_blank" rel="noopener noreferrer">{content.link}</a>
                         </div><br />
                      </div>
                    )
                  })}
              </div>
              <hr/>
              </React.Fragment>
            }

            {this.state.social.length>0 &&
              <React.Fragment>
                <div style={{padding:'2px 0px 5px 0px'}}>
                  <p className="resume-gen"><b>Social Media Accounts:</b></p>
                  {this.state.social.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="resume-gen">
                           Blog Link - <a href={content.blog_link} target="_blank" rel="noopener noreferrer">{content.blog_link}</a>
                           <br/>
                           GitHub Account - <a href={content.github_link} target="_blank" rel="noopener noreferrer">{content.github_link}</a>
                           <br/>
                           HackerEarth Link - <a href={content.hackerearth_link} target="_blank" rel="noopener noreferrer">{content.hackerearth_link}</a>
                          <br/>
                          HackerRank Link - <a href={content.hackerrank_link} target="_blank" rel="noopener noreferrer">{content.hackerrank_link}</a>
                         </div>
                         <br />
                      </div>
                    )
                  })}
              </div>
              <hr/>
              </React.Fragment>
            }



              <div>
                  <p className="resume-gen"><b>Declaration:</b> <br/> <br/> I hereby declare that above mentioned details are true to my knowledge.</p>
              </div>

              <Grid container spacing={1}>
                  <Grid item xs={10} sm={10}><p className="resume-gen">2nd Feb 2020 <br/><b>Date</b></p></Grid>
                  <Grid item xs={2} sm={2}><p className="resume-gen"><b>Signature</b></p></Grid>
              </Grid>
            </section>
          </React.Fragment>
        </PdfContainer>

          </Grid>
        </Grid>

      </React.Fragment>
    );

  }
}

export default Resume;
