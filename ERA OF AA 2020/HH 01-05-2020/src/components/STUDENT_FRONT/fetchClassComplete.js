import React, { Component } from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {Button,List,Typography,Snackbar,DialogActions} from '@material-ui/core';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import InfoIcon from '@material-ui/icons/Info';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class ClassComplete extends Component {
  constructor(){
    super()
    this.state={
      disabled:'',
      disp:false,
      covered_s:'',
      key_ind:'',
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidUpdate = this.componentDidUpdate.bind(this)
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps!==this.props)
    {
      this.props.completeHistory();
    }
  }

  handleField=(e,index)=>{
    const value = e.target.value;
    this.setState({covered_s: value, index,key_ind: e.target.name});
  }

  handleAction =(covered_fac,subject_code,date,month,year,faculty_id,serial,day_order,
    hour,slot,week,batch,sem,for_year)=>{
    if(!this.state.covered_s)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Enter all the Details !!',
        alert_type:'warning',
      });
    }
    else{
      this.setState({
        snack_open:true,
        snack_msg:'Updating...',
        alert_type:'info',
      });
    axios.post('/ework/user2/updateValue',{
      covered_fac:covered_fac,subject_code:subject_code,
      username:this.props.username,
      covered:this.state.covered_s,
      week:week,day_order:day_order,hour:hour,slot:slot,date:date,month:month,year:year,
      for_batch:batch,for_year:for_year,for_sem:sem,faculty_id:faculty_id,reference_number:serial})
    .then( res => {
      if(res.data.updateStatus === 'done')
      {
        this.sendStatus(res.data.positive_count,date,month,year,faculty_id,day_order,hour,
        slot,week,res.data.total_student.length,serial,batch,for_year,sem,subject_code);
      }
    });
   }
  }

  sendStatus=(positive_count,date,month,year,faculty_id,day_order,hour,slot,week,total_student,serial,
    batch,for_year,sem,subject_code)=>{
    axios.post('/ework/user/sendStatus',{total_student:total_student,week:week,
      day_order:day_order,hour:hour,slot:slot,date:date,month:month,year:year,batch:batch,for_year:for_year,
      faculty_id:faculty_id,positive_count:positive_count,sem:sem,subject_code:subject_code})
    .then( res => {
      if(res.data)
      {
        this.props.complete_class.filter(item => item.serial!== serial);
        this.setState({
          snack_open:true,
          snack_msg:'Submitted !!',
          alert_type:'success',
        });
        if(this.props.complete_class.length===0)
        {
                  this.props.showPage();
        }
      }
    })
    .catch( err => {
      this.setState({
        snack_open:true,
        snack_msg:'Internal Error !!',
        alert_type:'error',
      });
    });
  }

  showInfo=()=>{
    this.setState({
      disp:!this.state.disp,
    })
  }

  render() {

    this.props.complete_class.filter(item =>{
      return this.props.facultylist.find(citem =>
         (citem.faculty_id === item.faculty_id) && (item.new=== true) &&
        (item.day_slot_time === citem.timing) && (item.for_batch === citem.batch)
      && (item.for_sem === citem.sem) && (item.for_year === citem.year));
    });
    return (
      <Dialog fullScreen open={true} onClose={this.showInfo} TransitionComponent={Transition}>
        <List>

        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        open={this.state.snack_open} autoHideDuration={2000}
        onClose={()=>this.setState({snack_open:false})}>
          <Alert onClose={()=>this.setState({snack_open:false})}
          severity={this.state.alert_type}>
            {this.state.snack_msg}
          </Alert>
        </Snackbar>

      <Typography align="center" variant="h5">Class Completion History</Typography>
         {this.state.disp === false &&
           <div onClick={this.showInfo}
            className="animat bounce" style={{marginTop:'20px',cursor: 'pointer'}}>
            <InfoIcon />
           </div>
         }
         {this.state.disp &&
           <Dialog open={this.state.disp} onClose={this.showInfo} fullWidth>
               <DialogTitle id="alert-dialog-title" align="center">Instructions</DialogTitle>
               <DialogContent>
                 <div style={{padding:'5px'}}>
                      Please submit what are the topics completed today in the listed slot,taken by
                      respective faculty.<br /><br />
                      <span style={{color:'red'}}><b>Remember: </b></span>Your truthfullness and honesty will
                      be helpful to increse your popularity in campus.
                  </div>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.showInfo} color="primary">
                  Close
                </Button>
              </DialogActions>
            </Dialog>
       }
         <br />

         <TableContainer component={Paper} style={{margin:'10px'}}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center"><b>Faculty Id</b></TableCell>
                <TableCell align="center"><b>DayOrder</b></TableCell>
                <TableCell align="center"><b>Hour</b></TableCell>
                <TableCell align="center"><b>Slot</b></TableCell>
                <TableCell align="center"><b>Subject Code</b></TableCell>
                <TableCell align="center"><b>Enter Covered Topic</b></TableCell>
                <TableCell align="center"><b>Action</b></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.props.complete_class.map((content,index)=> (
                <TableRow key={index}>
                  <TableCell align="center">
                    {content.faculty_id}
                  </TableCell>
                  <TableCell align="center">{content.day_order}</TableCell>
                  <TableCell align="center">{content.hour}</TableCell>
                  <TableCell align="center">{content.slot}</TableCell>
                  <TableCell align="center">{content.subject_code}</TableCell>
                  <TableCell align="center">
                      <TextField
                      id="outlined-multiline-static"
                      label="Type Here"
                      multiline
                      fullWidth
                      value={this.state.ind && this.state.covered_s}
                      onChange={e => this.handleField(e, index)}

                      rows="4"
                      variant="filled"
                      />
                  </TableCell>
                  <TableCell align="center">
                    <Button variant="contained" color="primary" onClick={()=>this.handleAction(
                      content.covered_topic,content.subject_code,
                      content.date,content.month,content.year,content.faculty_id,content.serial,
                      content.day_order,content.hour,content.slot,content.week,content.for_batch,content.for_sem,
                      content.for_year
                    )}>Submit</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </List>
      </Dialog>
    );
  }
}
