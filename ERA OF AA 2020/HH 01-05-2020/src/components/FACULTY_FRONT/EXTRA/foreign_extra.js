import React, { Component } from 'react';
import axios from 'axios'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';
import {TextField,Typography} from '@material-ui/core';
import { Redirect } from 'react-router-dom'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class Fextra extends Component {
  constructor()
  {
    super()
    this.state={
      foreign_handle:[],
      dayorder:'',
      redirectTo:'',
      disq:false,
      notfound:'',
      username:'',
      permission:'',
      content:'',
      modal_open:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.loggedin()
    this.fetchForegnReq()
    this.fetchdayorder()
  }

  loggedin = ()=>{
    axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/ework/faculty'})
      }
    })
  }

  fetchdayorder = ()=>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{
          this.setState({
            dayorder: response.data.day_order,
          });
    });
  }
  fetchForegnReq = () =>{
    axios.post('/ework/user/fetch_foreign_slots')
    .then(res=>{
      this.setState({foreign_handle:res.data})
    })
  }

  selectModal =() => {
  this.setState({modal_open: !this.state.modal_open})
}

buttonState = (object) => {
this.setState(object)
}

updatePermission=(e,content)=>{
  this.setState({permission:e.target.value})
  axios.post('/ework/user/giving_permission',{permission:e.target.value,
    content:content})
  .then( res => {
    if(res.data === 'done')
    {
      window.M.toast({html: 'Succeed !!',classes:'rounded green darken-2'});
      this.fetchForegnReq();
    }
  });
}

showModal=(content)=>{
  this.setState({modal_open:true,content:content})
}

  render() {
    var details_data = this.state.foreign_handle.filter(item => ((item.accepted==='accepted')||(item.accepted==='pending')))
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return (
      <React.Fragment>
      {details_data.length>0 ?
      <React.Fragment>
      <TableContainer component={Paper}>
        <Table  aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center"><b>Request From(Id)</b></TableCell>
              <TableCell align="center"><b>Requested Hour</b></TableCell>
              <TableCell align="center"><b>Requested Subject Code</b></TableCell>
              <TableCell align="center"><b>Requested Slot</b></TableCell>
              <TableCell align="center"><b>For Batch-Sem-Year</b></TableCell>
              <TableCell align="center"><b>Deadline(dd-mm-yyyy)</b></TableCell>
              <TableCell align="center"><b>Status</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.foreign_handle.map((content,index)=> (
              <TableRow key={index}>
                <TableCell align="center">
                  {content.order_from}
                </TableCell>
                <TableCell align="center">{content.hour}</TableCell>
                <TableCell align="center">{content.subject_code}</TableCell>
                <TableCell align="center">{content.slot}</TableCell>
                <TableCell align="center">{content.for_batch}-{content.for_sem}-{content.for_year}</TableCell>
                <TableCell align="center">{content.date}-{content.month}-{content.year} [Before 11:59pm]</TableCell>
                <TableCell align="center">
                {content.accepted === 'pending' ?
                  <div className="">
                    <RadioGroup aria-label="permit" name="permit" value={this.state.permission}
                     onChange={(e)=>this.updatePermission(e,content)}>
                     <FormControlLabel style={{color:'green'}} value="accept" control={<Radio />} label="Accept" />
                     <FormControlLabel style={{color:'red'}} value="deny" control={<Radio />} label="Deny" />
                    </RadioGroup>
                  </div>
                  :
                  <div className="center">
                    <Button variant="contained" color="primary" style={{float:'center'}} onClick={(e)=>this.showModal(content)}>UPLOAD</Button>
                  </div>
                }
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      
      {this.state.modal_open && <Moda displayModal={this.state.modal_open}
         closeModal={this.selectModal}
         content={this.state.content}
         username={this.state.username}
         buttonState ={this.buttonState}
         dayorder={this.state.dayorder}
         fetchForegnReq={this.fetchForegnReq}
         />
       }
      </React.Fragment>
      :
      <Typography variant="h5" align="center">No request Found !!</Typography>    }
      </React.Fragment>
    );
  }
  }
}



class Moda extends Component {
  constructor(props) {
    super(props)
    this.state={
     covered:'',
    }
  }
  coveredTopic =(e)=>{
    this.setState({covered:e.target.value})
  }
  completed =(id,req_from,hour,slot)=>
  {
     if(!this.state.covered)
     {
       window.M.toast({html: 'Enter all the details !!',classes:'rounded red'});
     }
     else{
        axios.post('/ework/user/completed_foreign_hour',
        {content:this.props.content,username:this.props.username,covered:this.state.covered})
        .then(res=>{
          if(res.data==='no')
          {
            window.M.toast({html: 'Invalid Entry !!', classes:'rounded red'});
          }
          else{
            window.M.toast({html: 'Submitted !!', classes:'rounded gree darken-1'});
            this.setState({covered:''})
            this.props.fetchForegnReq();
            this.props.closeModal()
          }

        });
      }
  }

  closeModal=(e)=> {
      e.stopPropagation();
      this.props.closeModal()
   }
  render()
  {
    return(
             <div className="">
             <Dialog
  open={this.props.displayModal}
  TransitionComponent={Transition}
  keepMounted
  onClose={this.props.closeModal}
  aria-labelledby="alert-dialog-slide-title"
  aria-describedby="alert-dialog-slide-description"
>
  <DialogContent>
          <div className="center">
               Submitting Data For SLot - <span className="red-text">{this.props.content.slot}</span>
          </div>
          <br />
              <TextField
                id="outlined-multiline-static"
                label="Enter the topic"
                multiline
                rows="4"
                fullWidth
                value={this.state.covered}
                onChange={this.coveredTopic}
                variant="filled"
                />
                <br /><br />
                  <div className="right">
                    <Button align="right" onClick={this.completed} variant="outlined" color="secondary">
                      SUBMIT
                    </Button>
                  </div>
  </DialogContent>
  <DialogActions>
    <Button onClick={this.props.closeModal} color="primary">
      Close
    </Button>
  </DialogActions>
</Dialog>
</div>
    )
  }
}
