import React, { Component,Fragment } from 'react'
import axios from 'axios'
import {TextField,Grid,Paper,Hidden,Typography,Button} from '@material-ui/core';
import BluePrintTable from './TABLE/bluePrintReq_table'

export default class ManageOne extends Component {
  constructor(props)
  {
    super(props)
    this.initialState ={
      action:'For Blue Print Render',
      username:'',
      expired:false,
      status:'pending',
      added:false,
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setState({username:this.props.username})
    if(this.props.student)
    {
      //console.log(this.props.user)
      this.setState({faculty_adviser:this.props.user.faculty_adviser_id})
    }
  }
  handleDatas=(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleRequest =(e) =>{
    e.preventDefault()
    if(!(this.state.req_reason))
    {
      window.M.toast({html: 'Enter all the Details First !!',classes:'rounded red'});
    }
    else{
      let route;
      if(this.props.student)
      {
        route ='/ework/user2/entry_of_blueprint';
      }
      else {
        route = '/ework/user/entry_of_blueprint';
      }
    axios.post(route,this.state).then(response=>{
      if(response.data)
      {
         window.M.toast({html: response.data,classes:'rounded green'});
         this.setState({added:true,req_reason:''})
      }
    })
   }
  }
  render()
  {
    return(
      <Fragment>

      <Grid container spacing={1}>
        <Hidden xsDown><Grid item sm={4}/></Hidden>
        <Grid item sm={4}>
           <Paper elevation={3} style={{padding:'10px 10px 40px 10px'}}>
             <Typography align="center">REQUEST FOR BLUE-PRINT RENDER</Typography><br />
             <TextField
                id="filled-textarea"
                label="Enter the Valid Reason(Hit Enter to resize)"
                name="req_reason"
                value={this.state.req_reason}
                onChange={this.handleDatas}
                multiline
                fullWidth
                variant="filled"
              /><br /><br />
              <Button variant="contained" color="secondary" style={{float:'right'}}
               onClick={this.handleRequest}>Make A Request</Button>

            </Paper>
        </Grid>
        <Hidden xsDown><Grid item sm={4}/></Hidden>
      </Grid>
<br /><br />
        <Status added={this.state.added} student={this.props.student} user={this.props.user} username={this.props.username} />
      </Fragment>
    )
  }
}

class Status extends Component{
  constructor()
  {
    super()
    this.state={
      request:'',
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq();
  }
  componentDidUpdate=(prevProps)=>{
    if((prevProps.added !== this.props.added)||(prevProps.username !== this.props.username))
    {
      this.fetchReq();
    }
  }
  fetchReq=()=>{
    let fetch_route;
    if(this.props.student)
    {
      fetch_route = '/ework/user2/fetch_req_on_blueprint_render';
    }
    else
    {
      fetch_route ='/ework/user/fetch_req_on_blueprint_render';
    }
    axios.post(fetch_route)
    .then(res=>{

      if(res.data)
      {
        if(res.data.length>0)
        {
          var request = res.data.filter(item=>(item.username === this.props.username)
          && (item.expired=== false))
          this.setState({request:request})
        }
      }
      this.setState({loading:false})
    })
  }
  render()
  {
    return(
      <React.Fragment>
        {this.state.loading ?
           <div style={{textAlign:'center',color:'red'}}>Fetching ....</div>
            :
            <React.Fragment>
            {this.state.request.length!==0 ?
              <React.Fragment>
                <BluePrintTable blue_print={this.state.request} />
              </React.Fragment>
            :
            <Typography color="secondary" align="center">No Request Found !!</Typography>
          }
            </React.Fragment>
          }
      </React.Fragment>
    )
  }
}
