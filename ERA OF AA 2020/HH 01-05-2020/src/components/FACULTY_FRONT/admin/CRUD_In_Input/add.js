
import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      val:'',
      link:'',
      action:'',
      usertype:'',
      username:'',
      section_data_name:'',
      responsibilty_root:'',
      responsibilty_root_percentage:'',
      responsibilty_title:'',
      responsibilty_percentage:'',
      designation_name:'',
      designation_order_no:'',
      department_name:'',
      skill_name:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


  componentDidMount(){
    this.setState({
      action:this.props.action,
      usertype:this.props.level,
      inserted_by:this.props.username,
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
      <Dialog open={true}
      fullWidth keepMounted
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
      >
       <DialogContent>
       <div style={{padding:'10px'}}>
              {this.props.data.fielddata.map((content,index)=>(
                <React.Fragment key={index}>
                  <TextField
                    id="outlined-multiline-static"
                    label={content.placeholder}
                    multiline
                    fullWidth
                    name={content.name}
                    value={this.state[content.name]}
                    onChange={e => this.handleD(e, index)}
                    variant="outlined"
                    />
                    <br /><br />
                </React.Fragment>
              ))}
          </div>
        </DialogContent>
         <DialogActions>
         <Button onClick={this.props.cancel} color="secondary">
           Close
         </Button>
           <Button disabled={this.state.disabled} onClick={this.handleSubmit} color="primary">
             UPLOAD
           </Button>
         </DialogActions>
     </Dialog>
    )
  }
}

export default AddProduct;
