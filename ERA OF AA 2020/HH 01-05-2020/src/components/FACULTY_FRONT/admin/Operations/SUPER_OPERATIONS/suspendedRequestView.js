import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import {Typography,TextField,CircularProgress} from '@material-ui/core';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {InputBase,Divider,IconButton,Paper} from '@material-ui/core';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class Lists extends Component {
  constructor(){
      super();
      this.state={
        deniedlist:[],
        value_for_search:'',
        go_id:'',
        go_username:'',
        expired_date:'',
        expired_month:'',
        expired_year:'',
        snack_open:false,
        alert_type:'',
        snack_msg:'',
        loader:true,
        renderExpiry:false,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps.select !==this.props.select)
    {
      this.setState({success:'block'})
    }
  }
  fetch_denied_list =() =>{
    axios.get('/ework/user/fetch_denied_list')
    .then(res=>{
      this.setState({deniedlist: res.data,loader:false})
    })
  }
  handleApprove = (id,username) =>{
    this.setState({renderExpiry:true,disabled:true,go_id:id,go_username:username})
  }
  permissionApprove=()=>{
    if(!this.state.expired_date ||!this.state.expired_month || !this.state.expired_year)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill all the Details !!',
        alert_type:'warning',
      });
    }
    else
    {
    axios.post('/ework/user/approverequest',{
      serial: this.state.go_id,
      username:this.state.go_username,
      expired_date:this.state.expired_date,
      expired_month:this.state.expired_month,
      expired_year:this.state.expired_year
    }).then(res=>{
      if(res.data)
      {
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== this.state.go_id),
          renderExpiry:false
        })
      }
    })
   }
  }

  componentDidMount()
  {
    this.fetch_denied_list()
  }

  searchEngine =(e)=>{
    this.setState({value_for_search:e.target.value})
  }
  showDiv =(userObject) => {
    this.setState(userObject)
  }


  addExpire = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {
    if(this.state.loader)
    {
      return(
        <React.Fragment>
          <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
            <CircularProgress color="secondary" />
          </div>
        </React.Fragment>
      )
    }
    else
    {
      if(this.state.deniedlist.length === 0)
      {
        return(
          <Typography variant="h5" color="secondary" align="center">No Request Found !!</Typography>
        )
      }
      else
      {
        var libraries = this.state.deniedlist,
        searchString = this.state.value_for_search.trim().toLowerCase();
        libraries = libraries.filter(function(i) {
          return i.username.toLowerCase().match( searchString );
        });
        return (
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          {this.state.renderExpiry &&
            <Dialog
              open={true} fullWidth
              onClose={()=>this.setState({renderExpiry:false})}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogContent>
                  <React.Fragment>
                   <Typography variant="h6" align="center">Enter Expiry Date</Typography>
                   <br /><br />
                   <TextField fullWidth id="outlined-basic1" label="Day(dd)" variant="outlined" type="number"
                    name="expired_date" value={this.state.expired_date} onChange={this.addExpire} />
                   <br /><br />
                   <TextField fullWidth id="outlined-basic3" label="Month(mm)" variant="outlined" type="number"
                     name="expired_month" value={this.state.expired_month} onChange={this.addExpire} />
                   <br /><br />
                   <TextField fullWidth id="outlined-basic3" label="Year(yyyy)" variant="outlined" type="number"
                     name="expired_year" value={this.state.expired_year} onChange={this.addExpire} />
                     <br /><br />
                    <Button variant="contained" style={{float:'right'}} color="primary" onClick={this.permissionApprove}
                    >APPROVE</Button>
                  </React.Fragment>
              </DialogContent>
              <DialogActions>
                <Button onClick={()=>this.setState({renderExpiry:false})} color="primary" autoFocus>
                  CLOSE
                </Button>
              </DialogActions>
            </Dialog>
          }

                    <div style={{padding:'30px'}}>
                     <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
                   </div>
                   <br />
                    <TableContainer component={Paper}>
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell align="center"><b>Day Order</b></TableCell>
                            <TableCell align="center"><b>Official ID</b></TableCell>
                            <TableCell align="center"><b>Requested Hour</b></TableCell>
                            <TableCell align="center"><b>Requested Date(dd/mm/yyyy)</b></TableCell>
                            <TableCell align="center"><b>Reason</b></TableCell>
                            <TableCell align="center"><b>Action</b></TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {libraries.map((row,index)=> (
                            <TableRow key={index}>
                              <TableCell align="center">
                                {row.day_order}
                              </TableCell>
                              <TableCell align="center">{row.username}</TableCell>
                              <TableCell align="center">{row.req_hour ? <div>{row.req_hour}</div>: <div>NULL</div>}</TableCell>
                              <TableCell align="center">{row.req_date}/{row.req_month}/{row.req_year}</TableCell>
                              <TableCell align="center">{row.req_reason}</TableCell>
                              <TableCell align="center">
                                  <Button variant="contained" color="primary" onClick={() => this.handleApprove(row.serial,row.username)}>APPROVE</Button>
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
        </React.Fragment>
      );
    }
  }
 }
}



const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search Only Official Id" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
