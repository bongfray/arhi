import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {CircularProgress,Typography} from '@material-ui/core';

import Admin2 from './operation_type'
import Nav from '../../dynnav'
import Arhi from './arhi.js'


export default class Ap extends Component{
  constructor() {
    super()
    this.state={
      isChecked:0,
      active:0,
      logout:'/ework/user/logout',
      get:'/ework/user/',
      noti_route:true,
      nav_route: '',
      toggled:false,
      loader:true,
      user:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
      this.fetchlogin();
  }
  fetchlogin = () =>{
      axios.get('/ework/user/')
      .then(response=>{
        if(response.data.user === null)
        {
          this.setState({
            loader:false,
            redirectTo:'/ework/',
          });
        }
        else {
          this.setState({user:response.data.user,loader:false})
        }
      })
    }
  handleComp = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }


   color =(position) =>{
     if (this.state.active === position) {
         return 'red';
       }
       return '';
   }

  render()
  {
    const dstyle = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
      if(this.state.loader)
      {
        return(
          <div>Loading .....</div>
        )
      }
      else{
    return(
        <React.Fragment>
          <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div style={{marginTop:'10px'}}>

               {this.state.toggled ===  false &&
                 <div onClick={()=>this.setState({toggled:true})} className="dig"
                   style={{float:'center',boxShadow: '0px 10px 10px 2px pink',textAlign:'center',
                   width:'30px',borderRadius: '0px 0px 20px 20px'}}>
                   <ArrowDropDownIcon />
                 </div>
               }

               <Drawer anchor="top" open={this.state.toggled} onClose={()=>this.setState({toggled:false})}>
                 <div
                    className={dstyle.fullList}
                    role="presentation"
                    onKeyDown={()=>this.setState({toggled:false})}
                    >
                    <List>
                        <ListItem onClick={(e)=>{this.handleComp(0)}} button style={{color:this.color(0)}}>
                          <ListItemText align="center" primary={"System"} />
                        </ListItem>
                        <ListItem onClick={(e)=>{this.handleComp(1)}} button style={{color:this.color(1)}}>
                          <ListItemText align="center" primary={"Admin Operations"} />
                        </ListItem>
                        <ListItem onClick={(e)=>{this.handleComp(2)}} button style={{color:this.color(2)}}>
                          <ListItemText align="center" primary={"Student Actions"} />
                        </ListItem>
                        <ListItem onClick={(e)=>{this.handleComp(3)}} button style={{color:this.color(3)}}>
                          <ListItemText align="center" primary={"Complaints/Report"} />
                        </ListItem>
                    </List>
                  </div>
              </Drawer>
          </div>
          <div>
              <System user={this.state.user} choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>

       </React.Fragment>
    )
   }
  }
  }
}


class System extends Component {
  constructor() {
    super()
    this.state={
      activated_user:'',
      suspended_user:'',
      suser:'',
      user:'',
      total_user:'',
      d_user:'',
      admin_user:'',
      fetch_system:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser()
  }
  fetchUser()
  {
    axios.get('/ework/user/know_no_of_user')
    .then( res => {
      const activated_user = (res.data.filter(item => item.active === true)).length;
      const suspended_user = (res.data.filter(item => item.suspension_status === true)).length;
      const user = (res.data.filter(item => item.count=== 0 || item.count === 1 || item.count === 2)).length;
      const admin_user = (res.data.filter(item => item.h_order=== 0)).length;
      const d_user = (res.data.filter(item => item.h_order=== 0.5)).length;
        this.setState({activated_user:activated_user,suspended_user:suspended_user,admin_user:admin_user,user:user,d_user:d_user})
        this.fetchStudent(res.data.length);
    });

  }
  fetchStudent(user)
  {
    axios.get('/ework/user2/know_no_of_suser')
    .then( res => {
        let total =res.data.length;
        this.setState({total_user:user+total,suser:res.data.length,fetch_system:false})
    });
  }
  render()
  {
    if(this.props.choice === 1)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Admin user={this.props.user} />
        }
        </React.Fragment>
      )
    }
    else if(this.props.choice === 2)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Arhi />
        }
        </React.Fragment>
      )
    }
    else if(this.props.choice === 3)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Complaints />
        }
        </React.Fragment>
      )
    }
    else{
      if(this.state.fetch_system)
      {
        return(
          <div style={{float:'center'}}>
            <CircularProgress />
          </div>
        )
      }
      else{
      return(
        <React.Fragment>
        <div style={{marginTop:'50px'}}>
          <Typography style={{color:'#009688',fontFamily: "Cinzel",fontSize:'30px'}} variant="h5" align="center">Welcome To EWork Admin Panel</Typography>
        </div><br /><br />
         <Grid container spacing={1} style={{padding:'5px'}}>
            <Grid item xs={4} sm={2}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>TOTAL USER</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.total_user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={2}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>FACULTY</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={2}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>STUDENT</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.suser}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={2}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>DEPARTMENT ADMIN</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.d_user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={2}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>ADMIN USER</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.admin_user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={2}>
                <Paper elevation={3} style={{height:'110px'}}>
                  <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>SUSPENDED USER</div>
                  <div style={{paddingTop:'30px'}}>
                    <Typography variant="h5" align="center">{this.state.suspended_user}</Typography>
                  </div>
                </Paper>
            </Grid>
         </Grid>
        </React.Fragment>
      )
    }
    }
  }
}


class Admin extends Component{
constructor(){
    super();
    this.state ={
        radio:[{name:'radio1',color:'blue',value:'Insert'},{name:'radio2',color:'black',value:'Update'},
        {name:'radio3',color:'orange',value:'Super'},{name:'radio4',color:'red',value:'Delete'},
        {name:'radio5',color:'yellow',value:'Modify'},{name:'radio6',color:'aqua',value:'View'}],
    }
}
render()
{
    return(
        <React.Fragment>
          <Admin2 radio={this.state.radio} user={this.props.user} />
        </React.Fragment>
    );
}
}



class Complaints extends Component {
  constructor() {
    super()
    this.state={
      complaint:[],
      complaint_details:'',
      complaint_subject:'',
      official_id:'',
      serial_no:0,
      reply:false,
      forward:false,
      sendto:'',
      message_for_receiver:'',
      active:'',
      complaint_fetching:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchComplaint();
  }


  color =(position) =>{
    if (this.state.active === position) {
        return "row particular #e3f2fd blue lighten-5 black-text";
      }
      return "row particular";
  }


  fetchComplaint =()=>{
    axios.get('/ework/user/fetch_complaint')
    .then( res => {
        if(res.data){
          this.setState({complaint:res.data,complaint_fetching:false})
        }
    });
  }

    sendData =(index,official_id,complaint_subject,complaint,serial)=>{
      if (this.state.active === index) {
        this.setState({active : null})
      } else {
        this.setState({active : index})
      }
      this.setState({official_id:official_id,complaint_subject:complaint_subject,complaint_details:complaint,serial_no:serial})
    }
    onReply =(e)=>{
      if(e === 0)
      {
        this.setState({reply:true,forward:false})
      }
      else
      {
        this.setState({forward:true,reply:false})
      }
    }

    handleInput =(e)=>{
      this.setState({[e.target.name]:e.target.value})
    }

    sendReply =(no,id)=>{
      const { complaint } = this.state;
      if(!(this.state.reply_message))
      {
        window.M.toast({html: 'Enter all the Details !!',outDuration:'9000', classes:'rounded yellow black-text'});
      }
      else{
      axios.post('/ework/user/send_reply_to_complaint',{serial:no,official_id:id,message:this.state.reply_message})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({reply:false,message:false,complaint: complaint.filter(product => product.serial !== no),reply_message:''})
          window.M.toast({html: 'Replied !!', classes:'rounded green darken-2'});
        }
      });
     }
    }
    forwardMsg =() =>{
    const { complaint } = this.state;
    if(!(this.state.sendto) || !(this.state.message_for_receiver))
    {
      window.M.toast({html: 'Enter all the Details !!', classes:'rounded yellow black-text'});
    }
    else{
      axios.post('/ework/user/forward_complaint',{data:this.state})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({forward:false,message:false,complaint: complaint.filter(product => product.serial !== this.state.serial_no),sendto:'',message_for_receiver:''})
          window.M.toast({html: 'FORWARDED !!', classes:'rounded green darken-2'});
        }
      });
      }
    }

    deleteOne =(id)=>{
      const { complaint } = this.state;
      axios.post('/ework/user/delete_id',{serial:id})
      .then( res => {
          if(res.data)
          {
            this.setState({complaint: complaint.filter(product => product.serial !== id)})
          }
      });
    }

  render()
  {
    return(
      <React.Fragment>
      {this.state.complaint_fetching === true ?
        <div style={{float:'center'}}>
          <CircularProgress />
        </div>
        :
        <React.Fragment>
      {this.state.complaint.length===0 ?
        <h4 className="center" style={{marginTop:'120px'}}>Can't able to find any complaint !!</h4>
        :
      <div className="row">
         <div className="col l3 xl3 s12 m4">
            <div className="heading">
                <div className="row">
                  <div className="col l12">
                     <h6 className="ework-color center"><b>Complaints or Report</b></h6>
                  </div>
                </div>
            </div>
            <hr />
              {this.state.complaint.map((content,index)=>{
                return(
                  <div key={index} className={this.color(index)} onClick={() =>
                    this.sendData(index,content.official_id,content.complaint_subject,content.complaint,content.serial)}>
                    <div className="col l8 xl8 s8 m8">
                      <h6 className="dotted">{content.complaint_subject}</h6>
                      <label className="dotted hide-on-med-and-down">{content.complaint}</label>
                    </div>
                    <div className="col l4 xl4 s4 m4">
                       <i style={{marginTop:'20px'}} onClick={()=>this.deleteOne(content.serial)} className="small right material-icons go">delete</i>
                    </div>
                  </div>
                );
              })}
         </div>
         {this.state.complaint &&
         <div className="col l9 xl9 s12 m12">
            <div className="form-signup">
                 <React.Fragment>
                    <div className="row">
                      <div className="col l6 s12 m6 xl6">
                             <h6 className="center pink white-text" style={{padding:'8px'}}>COMPLAINT DETAILS</h6>
                              <div className="row">
                                 <div className="col l6 xl6 s6 m6"><b>Complaint From : </b></div><div className="col l6 xl6 s6 m6">{this.state.official_id}</div>
                              </div>
                              <div className="row">
                                 <div className="col l6 xl6 s6 m6"><b>Complaint Subject : </b></div><div className=" col l6 xl6 s6 m6">{this.state.complaint_subject}</div>
                              </div>
                              <div className="row">
                                 <div className="col l6 xl6 xl6 s6 m6"><b>Complaint Details : </b></div>
                              </div>
                              <div className="row" style={{wordWrap: 'break-word',padding:'20px'}}>
                                 {this.state.complaint_details}
                              </div>
                              <div className="row">
                              <div className="col l6 xl6 s6 m6"><span className="styled-btn" onClick={()=>this.onReply(0)}>Reply</span></div>
                              <div className="col l6 xl6 s6 m6"><span className="styled-btn right" onClick={()=>this.onReply(1)}>Forward</span></div>
                              </div>
                      </div>
                      <div className="col l1 xl1 m1 hide-on-small-only cr" />
                      <div className="col l5 xl5 s12 m6">
                          {this.state.reply &&
                             <React.Fragment>
                                <div className="row">
                                      <h6 className="center pink white-text" style={{padding:'8px'}}>REPLY TO USER</h6>
                                      <div className="row">
                                        <div className="col l6 xl6 s6 m6">
                                          <label className="">To : </label><span>{this.state.official_id}</span>
                                        </div>
                                      </div>
                                      <div className="row">
                                        <div className="col l12 xl12 s12 m12">
                                          <label className="pure-material-textfield-outlined alignfull">
                                            <textarea
                                              className=""
                                              type="text"
                                              placeholder=" "
                                              min="10"
                                              max="60"
                                              name="reply_message"
                                              value={this.state.reply_message}
                                              onChange={this.handleInput}
                                            />
                                            <span>Enter the Message</span>
                                          </label>
                                        </div>
                                      </div>
                                      <button className="right btn" onClick={()=>this.sendReply(this.state.serial_no,this.state.official_id)} style={{marginBottom:'5px'}}>REPLY</button>
                                </div>
                                <span className="center red-text">Note : On your reply this complaint will be closed !!</span>
                             </React.Fragment>
                          }
                          {this.state.forward &&
                             <React.Fragment>
                                <div className="row">
                                <h6 className="center pink white-text" style={{padding:'8px'}}>FORWARD MESSAGE</h6>
                                   <div className="input-field">
                                      <input id="forward-to" name="sendto" value={this.state.sendto} className="validate" onChange={this.handleInput} type="text" required/>
                                      <label htmlFor="forward-to">Send To</label>
                                       <span className="helper-text" data-error="Please enter the data !!" data-success="">Should be Official Id</span>
                                   </div>
                                </div>
                                <div style={{padding:'3px'}}>
                                  <div className="row">
                                     <label className="">Complaint From : </label><span>{this.state.official_id}</span>
                                  </div>
                                  <div className="row">
                                     <label className="">Complaint Subject : </label><span>{this.state.complaint_subject}</span>
                                  </div>
                                </div>
                                <div className="row">
                                   <div className="input-field">
                                      <input id="mm" name="message_for_receiver" className="validate" value={this.state.message_for_receiver} onChange={this.handleInput} type="text" required/>
                                      <label htmlFor="mm">Any Message For Receiver</label>
                                   </div>
                                </div>
                                <div className="row">
                                <button className="btn right" onClick={this.forwardMsg}>Forward</button>
                                </div>
                                <div className="row">
                                  <span className="red-text">Remember - </span> Once you forward this complaint, this complaint
                                  will be inactive for the admin user. The person who will receive this message can only answer or delete it.
                                </div>
                             </React.Fragment>
                          }
                      </div>
                    </div>
                 </React.Fragment>

            </div>
         </div>
           }
      </div>
        }
        </React.Fragment>
      }
      </React.Fragment>
    );
  }
}
