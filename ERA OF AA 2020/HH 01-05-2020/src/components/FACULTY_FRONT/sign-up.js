import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import ContentLoader from "react-content-loader"
import RegistrationStopped from './stop'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import {Paper,TextField} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import {InputAdornment} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class Signup extends Component {
	constructor() {
    super()
    this.initialState = {
			display:'block',
			status:'none',
			loader:true,
			redirectTo: null,
			h_order:'',
			title: '',
			name: '',
			mailid: '',
			username: '',
			phone: '',
			password: '',
			cnf_pswd: '',
			campus: '',
			dept: '',
			desgn: '',
		  dob:'',
			show_modal:false,
			color:'green-text',
			designation:[],
			department:[],
			snack_open:false,
			alert_type:'',
			snack_msg:'',
      showPassword:false,
      verify_modal:false,
      otp:'',
	}
	 this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    }
		componentDidMount(){
			this.fetchrender();
		}

fetchrender =() =>{
	axios.post('/ework/user/fac_status')
			.then(response => {
				this.setState({loader:false})
				if(response.data)
				{
					var fac_reg_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="SIGNUP STATUS")));
					if((fac_reg_status.length)>0)
					{
            console.log(fac_reg_status);
						if(fac_reg_status[0].status === true)
						{
							this.setState({status:'block'})
						}
						else {
							this.setState({status:'none'})
						}
					}
					else {
						this.setState({status:'block'})
					}
				}
				this.fetchDesig();
			})
}

		fetchDesig =() =>{
		  axios.post('/ework/user/fetch_designation')
		  .then(res => {
		      if(res.data)
		      {

						        const designation = res.data.filter(item =>
						        item.action ==='Designation');
										const department = res.data.filter(item =>
										item.action ==='Department');
		        this.setState({designation:designation,department:department})
		      }
		  });
		}

		handleField = (e) =>{
				this.setState({
					[e.target.name] : e.target.value,
				})
		}


					handleDesgn = (e) => {
						let order;
						if(e.target.value === "Director")
						{
							order = 1;
							this.setState({display:'none'});
						}
						else if(e.target.value === "Principle")
						{
							order = 2;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Assistant Director")
						{
							order =3;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Dean")
						{
							order =4;
							this.setState({display:'block'});
						}
						else if(e.target.value === "HOD")
						{
							order =5;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Professor")
						{
							order =6;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Assistant Professor")
						{
							order =7;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Associate Professor")
						{
							order = 8;
							this.setState({display:'block'});
						}
						this.setState({
								desgn: e.target.value,
								h_order: order,
						});
			    }


	handleSubmit(event) {
		event.preventDefault()
		if(!(this.state.title)||!(this.state.name)||!(this.state.mailid)||!(this.state.username)||!(this.state.phone)||!(this.state.password)||!(this.state.cnf_pswd)||!(this.state.campus)||!(this.state.desgn))
		{
			this.setState({
				snack_open:true,
				snack_msg:'Enter all the Details !!',
				alert_type:'warning',
			})
      return false;
		}
		else if(!(this.state.password.match(/[a-z]/g) && this.state.password.match(
							/[A-Z]/g) && this.state.password.match(
							/[0-9]/g) && this.state.password.match(
							/[^a-zA-Z\d]/g) && this.state.password.length >= 6))
		{
			this.setState({
				snack_open:true,
				snack_msg:'Follow Correct Format !!',
				alert_type:'warning',
			})
				this.showValidation();
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
			this.setState({
				snack_open:true,
				snack_msg:'Password Does Not Match !!',
				alert_type:'warning',
			})
			this.setState({cnf_pswd:''})
	        return false;
	  }
		else if ((this.state.phone).length!==10)
		{
			this.setState({
				snack_open:true,
				snack_msg:'Phone no should be of 10 digit !!',
				alert_type:'warning',
			})
			return false;
		}
		else{
			this.setState({
				snack_open:true,
				snack_msg:'Signing Up !!',
				alert_type:'info',
			})
		axios.post('/ework/user/', {
			h_order: this.state.h_order,
			username: this.state.username,
			password: this.state.password,
			title: this.state.title,
			name: this.state.name,
			mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
			dept: this.state.dept,
			desgn:this.state.desgn,
			dob: this.state.dob,
		})
			.then(response => {
				if(response.status===200){
					if(response.data.emsg)
					{
						this.setState({
							snack_open:true,
							snack_msg:response.data.emsg,
							alert_type:'info',
						})
				  }
					else if(response.data==='ok')
					{
						this.setState({
								verify_modal:true,
						})
					}
				}
			}).catch(error => {
        this.setState({
  				snack_open:true,
  				snack_msg:'Something Went Wrong !!',
  				alert_type:'error',
  			})
			})
			this.setState({loader:false})
	}
}

showValidation=()=>{
	this.setState({show_modal:!this.state.show_modal})
}

handleMouseDownPassword = event => {
    event.preventDefault();
  };
completeSignup=()=>{
  axios.post('/ework/user/verify_otp',{otp:this.state.otp,username:this.state.username})
  .then( res => {
    if(res.data ==='did')
    {
      this.setState({
        snack_open:true,
        snack_msg:'Already Activatd !! ',
        alert_type:'error',
        verify_modal:false,
      })
    }
    else if (res.data==='done') {
      this.setState({
        snack_open:true,
        snack_msg:'Signed Up...!! ',
        alert_type:'success',
        verify_modal:false,
      })
    }
    else if (res.data==='no') {
      this.setState({
        snack_open:true,
        snack_msg:'User Not Found !! ',
        alert_type:'error',
        verify_modal:false,
      })
    }

  })
  .catch( err => {
    this.setState({
      snack_open:true,
      snack_msg:'Something Went Wrong !!',
      alert_type:'error',

    })
  });
}

render() {
	const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
        </ContentLoader>
      )
			if(this.state.redirectTo) {
					 return <Redirect to={{ pathname: this.state.redirectTo }} />
			 } else {
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status === "none")
	{
		return(
			<RegistrationStopped login_path="/ework/flogin" section_name="FACULTY REGISTRATION"/>
		);
	}
	else{
	return (
		<React.Fragment>

  <Dialog open={this.state.verify_modal} onClose={()=>this.setState({verify_modal:false})} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title" align="center">Verify Your Email</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Please enter the details :
        </DialogContentText>
                <TextField
                  autoFocus
                  id="username"
                  variant="outlined"
                  label="Enter the Username"
                  name="official_id" value={this.state.username} onChange={(e)=>this.setState({username:e.target.value})}
                  fullWidth
                /><br /><br />
                <TextField
                  id="otp"
                  variant="outlined"
                  type="number"
                  label="Enter the OTP"
                  name="official_id" value={this.state.otp} onChange={(e)=>this.setState({otp:e.target.value})}
                  fullWidth
                />
      </DialogContent>
      <DialogActions>
        <Button onClick={()=>this.setState({verify_modal:false})} color="primary">
          Cancel
        </Button>
        <Button disabled={this.state.sending} onClick={this.completeSignup} color="primary">
          VERIFY
        </Button>
      </DialogActions>
    </Dialog>

		<Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
		open={this.state.snack_open} autoHideDuration={2000}
		onClose={()=>this.setState({snack_open:false})}>
			<Alert onClose={()=>this.setState({snack_open:false})}
			severity={this.state.alert_type}>
				{this.state.snack_msg}
			</Alert>
		</Snackbar>

		<Grid container spacing={2}>
		<Grid item xs={1} sm={2} />
		<Grid item xs={10} sm={8}>
		<Paper elevation={3} style={{padding:'10px',marginTop:'30px'}}>
		   <Typography variant="h5" color="secondary" align="center" >REGISTRATION</Typography>
				<form className="form-con">
						<Grid container spacing={2}>
								<Grid item xs={6} sm={2}>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="title">Salutation</InputLabel>
												<Select
													labelId="title"
													id="title"
													name="title" value={this.state.title} onChange={this.handleField}
												>
												<MenuItem value="Mr.">Mr.</MenuItem>
												<MenuItem value="Mrs.">Mrs.</MenuItem>
												<MenuItem value="Miss.">Miss.</MenuItem>
												<MenuItem value="Dr.">Dr.</MenuItem>
												</Select>
										 </FormControl>
								</Grid>
								<Grid item xs={6} sm={6}>
                  <TextField id="name" name="name" type="text" fullWidth
                   value={this.state.name} onChange={this.handleField} label="Name" />
							   </Grid>
								 <Grid item xs={12} sm={4}>
                   <TextField id="fac_id" name="username" type="text" fullWidth
                   value={this.state.username} onChange={this.handleField} label="Official ID" />
								 </Grid>
						</Grid>
<br /><br />
						<Grid container spacing={2}>
							 <Grid item xs={12} sm={5}>
                   <TextField id="email" name="mailid" type="email" fullWidth
                   value={this.state.mailid} onChange={this.handleField} label="Official Mail Id" />
								</Grid>
								<Grid item xs={6} sm={4}>
                  <TextField id="ph_num" type="number" name="phone" fullWidth
                    value={this.state.phone} onChange={this.handleField} label="Phone Number" />
								</Grid>
								<Grid item xs={6} sm={3}>
                    <TextField d="dob" type="date" name="dob" fullWidth helperText="Format: dd-mm-yyyy"
                    value={this.state.dob} onChange={this.handleField} label="D.O.B." />
							  </Grid>
						</Grid>
<br /><br />

						<Grid container spacing={2}>
						     <Grid item xs={12} sm={6}>
                 <FormControl style={{width:'100%'}}>
                    <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                    <Input
                      type={this.state.showPassword ? 'text' : 'password'}
                      onChange={this.handleField}  name="password" id="pswd1" value={this.state.password}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={()=>this.setState({showPassword:!this.state.showPassword})}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }

                    />
                    	<HelpOutlineIcon style={{color:'green'}} onClick={this.showValidation} />
                  </FormControl>
											 <React.Fragment>
												 <Dialog
						               open={this.state.show_modal}
													 onClick={this.showValidation}
						               aria-labelledby="alert-dialog-title"
						               aria-describedby="alert-dialog-description"
						             >
						               <DialogTitle id="alert-dialog-title">Password Should Consist of </DialogTitle>
						               <DialogContent>
						                 <DialogContentText id="alert-dialog-description">
															 <Typography variant="button" display="block" gutterBottom>
															 At least 1 uppercase character.<br />
															 At least 1 lowercase character.<br />
															 At least 1 digit.<br />
															 At least 1 special character.<br />
															 Minimum 6 characters.<br />
												       </Typography>
						                 </DialogContentText>
						               </DialogContent>
						               <DialogActions>
						                 <Button onClick={this.showValidation} color="primary" autoFocus>
						                   Agree
						                 </Button>
						               </DialogActions>
						             </Dialog>
											</React.Fragment>
								</Grid>
								<Grid item xs={12} sm={6}>

                <FormControl style={{width:'100%'}}>
                   <InputLabel htmlFor="standard-adornment-password">Confirm Password</InputLabel>
                   <Input
                     type={this.state.showPassword ? 'text' : 'password'}
                     onChange={this.handleField}  name="cnf_pswd" id="pswd" value={this.state.cnf_pswd}
                     endAdornment={
                       <InputAdornment position="end">
                         <IconButton
                           aria-label="toggle password visibility"
                           onClick={()=>this.setState({showPassword:!this.state.showPassword})}
                           onMouseDown={this.handleMouseDownPassword}
                         >
                           {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                         </IconButton>
                       </InputAdornment>
                     }

                   />
                 </FormControl>
								</Grid>
						</Grid>

            <br /><br />

						<Grid container spacing={2}>
									<Grid item xs={12} sm={4}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="campus">Campus</InputLabel>
													<Select
														labelId="campus"
														id="campus"
														value={this.state.campus} name="campus" onChange={this.handleField}
													>
													<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
													<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
													<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
													<MenuItem value="NCR Campus">NCR Campus</MenuItem>
													</Select>
											 </FormControl>
									</Grid>
									<Grid item xs={12} sm={4} className="col l4 s6 m6 xl4">
										 <FormControl style={{width:'100%'}}>
											 <InputLabel id="desgn">Designation</InputLabel>
												 <Select
													 labelId="desgn"
													 id="desgn"
													 value={this.state.desgn} onChange={this.handleDesgn}
												 >
															 {this.state.designation.map((content,index)=>{
																 return(
																	 <MenuItem key={index} value={content.designation_name}>{content.designation_name}</MenuItem>
																 )
													 })}
													 </Select>
			 								 </FormControl>
									</Grid>
								{this.state.display === 'block' ?
									<Grid item xs={12} sm={4}>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="dept">Department</InputLabel>
												<Select
													labelId="dept"
													id="dept"
													value={this.state.dept} name="dept" onChange={this.handleField}
												>
															{this.state.department.map((content,index)=>{
																return(
																	<MenuItem key={index} value={content.department_name}>{content.department_name}</MenuItem>
																)
													})}
													</Select>
											</FormControl>
									</Grid>
								:
									<Grid item xs={12} sm={4}>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="dept">Department</InputLabel>
												<Select
													labelId="dept"
													id="dept"
													value={this.state.dept} name="dept" onChange={this.handleField}
												>
														<MenuItem value="E & T">E & T</MenuItem>
														<MenuItem value="Medical">Medical</MenuItem>
													</Select>
											</FormControl>
									</Grid>
							}
						</Grid>
						<br/><br />
						<Grid container spacing={2}>
						  <Grid item xs={4} sm={4}>
							  <Link to='/ework/flogin' style={{textDecoration:'none'}} className="log">Login Instead ?</Link>
							</Grid>
							<Grid item sm={4} xs={4} />
							<Grid item xs={4} sm={4}>
							  <Button style={{backgroundColor:'#455a64',color:'white'}}
								fullWidth onClick={this.handleSubmit}>Submit</Button>
							</Grid>
						</Grid>
				</form>
		</Paper>
		</Grid>
		</Grid>
		</React.Fragment>
	);
}
}
}
}
}
