import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import axios from 'axios';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import SendIcon from '@material-ui/icons/Send';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ViewStudentData from '../../FACULTY ADVISER/view_single_student'
import {Snackbar} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

const headCells = [
  { id: 'Student Reg Id', numeric: false, disablePadding: false, label: 'Student Reg Id' },
  { id: 'Department', numeric: true, disablePadding: false, label: 'Department' },
  { id: 'Current Year - Batch', numeric: true, disablePadding: false, label: 'Current Year Batch' },
  { id: 'Current Sem', numeric: true, disablePadding: false, label: 'Current Sem' },
  { id: 'Choosed Subject Code', numeric: true, disablePadding: false, label: 'Choosed Subject Code' },
  { id: 'Choosed Slot', numeric: true, disablePadding: false, label: 'Choosed Slot' },
  { id: 'view', numeric: true, disablePadding: false, label: 'View Data' },
  { id: 'action', numeric: true, disablePadding: false, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>{headCell.label}</b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [userdata, setuserData] = React.useState({
      details_view: false,
      data: '',
    });
  const [mwindow,setMwindow] = React.useState({
      open_popup: false,
      userDetails: '',
    });
    const [state,setState] = React.useState({
      snack_open:false,
      snack_msg:'',
      alert_type:'',
    });
    const [message, setMessage] = React.useState('');
    const [disable_btn, setDisable] = React.useState(false);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = props.student_list.map(n => n.username);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const closeView=()=>{
    setuserData({
      details_view:false,
    })
  }

  const sendMessage=()=>{
    var data;
    if(!message)
    {
      setState({
        snack_open:true,
        snack_msg:'Enter all the Details !!',
        alert_type:'warning',
      })
    }
    else{
        data = {
          user:mwindow.userDetails,
          message:message,
          user_session:props.user_session,
        }
        setState({
          snack_open:true,
          snack_msg:'Sending Mail....',
          alert_type:'info',
        })
      setDisable(true)
      axios.post('/ework/user2/sendMessage_from_UserSide',data)
      .then( res => {
        setDisable(false)
          if(res.data === 'ok')
          {
            setState({
              snack_open:true,
              snack_msg:'Done !!',
              alert_type:'success',
            })
            setMwindow({
              open_popup:false,
            })
          }
          else if(res.data === 'no') {
            setState({
              snack_open:true,
              snack_msg:'Failed To Send !!',
              alert_type:'error',
            })
          }
      })
      .catch( err => {
          if(err)
          {
            setState({
              snack_open:true,
              snack_msg:'Error !!',
              alert_type:'error',
            })
          }
      });
   }
  }

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.student_list.length - page * rowsPerPage);

  return (
    <div className={classes.root}>

    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    open={state.snack_open} autoHideDuration={2000}
    onClose={()=>setState({snack_open:false})}>
      <Alert onClose={()=>setState({snack_open:false})}
      severity={state.alert_type}>
        {state.snack_msg}
      </Alert>
    </Snackbar>
    
    {mwindow.open_popup &&
        <Dialog open={mwindow.open_popup} onClose={()=>setMwindow({open_popup:false,userDetails:''})} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Send Message</DialogTitle>
          <DialogContent>
            <DialogContentText>
             This message will be forwarded to the particular student.
            </DialogContentText>
          <TextField
              required
              id="filled-required"
              label="Enter the message"
              value={message}
              onChange={(e)=>setMessage(e.target.value)}
              variant="filled"
              fullWidth
              multiline
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setMwindow({open_popup:false,userDetails:''})} color="primary">
              Cancel
            </Button>
            <Button disabled={disable_btn} color="primary" onClick={sendMessage}>
              Send Message
            </Button>
          </DialogActions>
         </Dialog>
    }
      {userdata.details_view &&
        <ViewStudentData details_view={userdata.details_view}  closeView={closeView} data={userdata} />
      }
      <Paper className={classes.paper} elevation={2}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={props.student_list.length}
            />
            <TableBody>
              {stableSort(props.student_list, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row.username)}
                      role="checkbox"
                      tabIndex={-1}
                      key={row.username}
                    >
                      <TableCell>
                        {row.username}
                      </TableCell>
                      <TableCell align="center">{row.dept}</TableCell>
                      <TableCell align="center">{row.year} - {row.batch}</TableCell>
                      <TableCell align="center">{row.sem}</TableCell>
                      <TableCell align="center">{row.subject_code}</TableCell>
                      <TableCell align="center">{row.alloted_slots}</TableCell>
                      <TableCell align="center">
                        <Tooltip title="View Details">
                         <VisibilityIcon onClick={() => setuserData({
                           details_view:true,
                           data:row,
                         }) } />
                        </Tooltip>
                      </TableCell>
                      <TableCell align="center">
                          <Tooltip title="Send Message To This Student">
                           <SendIcon onClick={()=>setMwindow({
                             open_popup:true,
                             userDetails:row,
                           })} />
                          </Tooltip>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.student_list.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
