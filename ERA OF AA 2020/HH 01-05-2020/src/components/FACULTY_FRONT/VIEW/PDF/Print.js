import React, { Component } from 'react';
import Logo from './Logo.png';
import Doc from './DocService';
import Axios from 'axios';
import PdfContainer from './PdfContainer';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

var time = new Date(); 

class Print extends Component {


  constructor(){
    super();
    this.state={
      loading:true,
      username:'',
      content: "",
      name: '',
      id: '',
      desgn:'',
      dept: '',
      campus: '',
      mob: '',
      mail: '',
      date:time.format(),
      profile_data:[],
      degree_data:[],
      administrative_data:[],
      section:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setUser();
  }


  fetchOption = () =>{
    this.profileFetch()
  }

  profileFetch = () =>{
    Axios.post('/ework/user/dash2',{username:this.props.username})
    .then(res=>{
      this.degreeFetch();
      this.setState({profile_data:res.data})
    })
  }
  degreeFetch = () =>{
    Axios.post('/ework/user/fetch_degree_for_pdf',{username:this.props.username})
    .then(res=>{
      this.fetchSection();
      this.setState({degree_data:res.data})
    })
  }
  fetchSection=()=>{
    Axios.post('/ework/user/fetch_section_for_view',{username:this.props.username})
    .then(res=>{
      this.administrativeFetch();
      this.setState({section:res.data,loading:false})
    })
  }
  administrativeFetch =()=>{
    Axios.post('/ework/user/fetch_adminis_for_view',{username:this.props.username}).then(res => {
      //console.log(res.data)
      this.setState({
        administrative_data: res.data,
      });
    })
  }

  setUser = () =>{
    Axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.mailid})
        //console.log(this.state.username)
        this.profileFetch()
      }
    })
  }


  createPdf = (html) => Doc.createPdf(html);

  render() {
    let content,section,administrative_data;

    if(this.state.degree_data){
      content=
      <Grid container spacing={1}>

        <Grid container justify="center" item xs={12} sm={12}>
          <p className="resume-mhead"><b>Qualifications</b></p>
        </Grid>

        <TableContainer >
          <Table aria-label="simple table" className="resume-table">
            <TableHead>
              <TableRow>
                <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
                <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Degree Name</b></TableCell>
                <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Institute Name</b></TableCell>
                <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Start Date</b></TableCell>
                <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>End Date</b></TableCell>
                <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Grade/CGPA</b></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.degree_data.map((content,index)=>{
                    return(
                      <TableRow key={index}>
                        <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                        <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.action}</TableCell>
                        <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.College_Name}</TableCell>
                        <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.Start_Year}</TableCell>
                        <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.End_Year}</TableCell>
                        <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.Marks_Grade}</TableCell>
                      </TableRow>
                    )
                })}
            </TableBody>
          </Table>
        </TableContainer>

      </Grid>
    }
    if(this.state.section)
    {
      section =
      <Grid container spacing={1}>

      <Grid container justify="center" item xs={12} sm={12}>
        <p className="resume-mhead"><b>Section Data</b></p>
      </Grid>

      <TableContainer >
        <Table aria-label="simple table" className="resume-table">
          <TableHead>
            <TableRow>
              <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
              <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Description</b></TableCell>
              <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Role</b></TableCell>
              <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Achievement (s)</b></TableCell>
              <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>Start Date</b></TableCell>
              <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}><b>End Date</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.section.map((content,index)=>{
                  return(
                    <TableRow key={index}>
                      <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                      <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.action}</TableCell>
                      <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.role}</TableCell>
                      <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.achivements}</TableCell>
                      <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.date_of_starting}</TableCell>
                      <TableCell align="left" padding={'none'} style={{fontSize:'10px'}}>{content.date_of_complete}</TableCell>
                    </TableRow>
                  )
              })}
          </TableBody>
        </Table>
      </TableContainer>

      </Grid>
   }

    return (
      <React.Fragment>

      <Grid container spacing={1}>
        <Grid item xs={3} sm={3} />
            <Grid item xs={6} sm={6}>

              <PdfContainer loading={this.state.loading} createPdf={this.createPdf}>

                <React.Fragment>

                  <section className="resum-pdf-body" style={{padding:'20px'}}>

                    <Grid container spacing={1}>

                      <Grid item xs={2} sm={2}>
                        <img alt="prof" src={Logo} className="resume-logo"/>
                      </Grid>
                      <Grid item xs={8} sm={8}>
                          <Typography variant="h2" style={{  fontSize: '16px'}} align="center">SRM INSTITUTE OF SCIENCE AND TECHNOLOGY</Typography><br />
                          <p className="resume-p" style={{textAlign:'center',marginTop:'-3%', marginBottom:'-3%'}}>Profile Report (For Faculty Members)</p>
                      </Grid>
                      <Grid item xs={2} sm={2}/>

                      </Grid>
                    
                      <br/>
                      
                      <Grid container spacing={1}>

                        <Grid item xs={3} sm={3}>
                          <p className="resume-gen"><b>Name<br/>Faculty Id<br/>Department<br/>Designation<br/> Mobile No.<br/>Campus<br/></b> </p>
                        </Grid>
                        <Grid item xs={9} sm={9}>
                          <p className="resume-gen">: {this.state.profile_data.name} <br/>: {this.state.profile_data.username} <br/>: {this.state.profile_data.dept} <br/>: {this.state.profile_data.desgn} <br/>: {this.state.profile_data.phone} <br/>: {this.state.profile_data.campus}  </p>
                        </Grid>

                      </Grid>

                      {this.state.degree_data && content}

                      {this.state.section && section}

                      {this.state.administrative_data && administrative_data}

                      <Grid container direction="row" justify="flex-end">
                        <p className="resume-date">{this.state.date}</p>
                      </Grid>

                  </section>
                </React.Fragment>
            </PdfContainer>
            </Grid>
      </Grid>

      </React.Fragment>
    );

  }
}

export default Print;
