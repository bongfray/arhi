const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const profSchema = new Schema({

  username: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  College_Name:{ type: String, unique: false, required: false },
  End_Year:{ type: String, unique: false, required: false },
  Start_Year:{ type: String, unique: false, required: false },
  Marks_Grade:{ type: String, unique: false, required: false },
  verificationlinks:{ type: String, unique: false, required: false },
  verified:{ type: Boolean, unique: false, required: false },
  date:{ type: String, unique: false, required: false },
  month:{ type: String, unique: false, required: false },
  year:{ type: String, unique: false, required: false },
})





profSchema.plugin(autoIncrement.plugin, { model: 'Degree', field: 'serial', startAt: 1,incrementBy: 1 });



var Prof = mongoose.model('Degree', profSchema);

module.exports = Prof
