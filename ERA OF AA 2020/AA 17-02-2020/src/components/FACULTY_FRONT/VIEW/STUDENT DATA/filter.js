import React from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';



export default class Filter extends React.Component {
  constructor()
  {
    super()
    this.initialState={
      dept_filter:'',
      disable_btn:true,
      department:'',
      loader:true,
      filter_years:['1','2','3','4','5']
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.departmentFetch();
  }
  departmentFetch=()=>{
    axios.post('/ework/user/fetch_in_admin',{action:'Department'})
    .then( res => {
        if(res.data)
        {
          this.setState({department:res.data,loader:false})
        }
    });
  }

   setFilter=(f_type)=>{
     this.props.fetchMethod(this.props.fetch_type);
     this.props.closeFilterTab()
     this.props.filterRequired({
       [f_type]:'',
     })
  }

  handleChange = e => {
    this.setState({[e.target.name]:e.target.value,disable_btn:false})
      this.props.filterRequired({
        [e.target.name]:e.target.value,
      })
  };

  checked =(incoming,type)=>
  {
    if(this.state[type] === incoming)
    {
      return true;
    }
    return false;

  }

  checkedY =(incoming,type)=>
  {
    if(this.state[type] === incoming)
    {
      return true;
    }
    return false;

  }

  render() {
    if(this.state.loader)
    {
      return(
        <div>Loading.....</div>
      )
    }
    else{
      if(this.props.type === 'department')
      {
        return(
            <React.Fragment>
               <FormGroup row>
                 {this.state.department.map((row,index)=>{
                   return(
                       <FormControlLabel key={index} style={{color:'black'}}
                         control={
                           <Checkbox checked={this.checked(row.department_name,'f_dept')} name="f_dept" onChange={this.handleChange} value={row.department_name} />
                         }
                         label={row.department_name}
                       />
                   )
                 })}
               </FormGroup>
               <br />
               <Button disabled={this.state.disable_btn} style={{float:'right'}} color="primary" onClick={(e)=>this.setFilter('f_dept')}>
                 APPLY
               </Button>
            </React.Fragment>
        )
      }
      else if (this.props.type === 'year') {
        return(
          <React.Fragment>
             <FormGroup row>
                 {this.state.filter_years.map((row,index)=>{
                   return(
                       <FormControlLabel key={index} style={{color:'black'}}
                         control={
                           <Checkbox checked={this.checked(row,'f_year')} name="f_year" onChange={this.handleChange} value={row} />
                         }
                         label={"Year - "+row}
                       />
                   )
                 })}
             </FormGroup>
             <br />
             <Button disabled={this.state.disable_btn} style={{float:'right'}} color="primary" onClick={(e)=>this.setFilter('f_year')}>
               APPLY
             </Button>
          </React.Fragment>
        )
      }
      else if (this.props.type === 'yd') {
        return(
          <React.Fragment>
            <FormGroup row>
              {this.state.department.map((row,index)=>{
                return(
                    <FormControlLabel key={index} style={{color:'black'}}
                      control={
                        <Checkbox checked={this.checked(row.department_name,'f_dept')} name="f_dept" onChange={this.handleChange} value={row.department_name} />
                      }
                      label={row.department_name}
                    />
                )
              })}
            </FormGroup>
            <FormGroup row>
                {this.state.filter_years.map((row,index)=>{
                  return(
                      <FormControlLabel key={index} style={{color:'black'}}
                        control={
                          <Checkbox checked={this.checked(row,'f_year')} name="f_year" onChange={this.handleChange} value={row} />
                        }
                        label={"Year - "+row}
                      />
                  )
                })}
            </FormGroup>
             <br />
             <Button disabled={this.state.disable_btn} style={{float:'right'}} color="primary" onClick={this.setFilter}>
               APPLY
             </Button>
          </React.Fragment>
        )
      }
    }
  }
}
