import React, { Component } from 'react';
import Logo from './Logo.png';
import Doc from './DocService';
import Axios from 'axios';
import PdfContainer from './PdfContainer';

var time = new Date();

class Print extends Component {


  constructor(){
    super();
    this.state={
      loading:true,
      username:'',
      content: "",
      name: '',
      id: '',
      desgn:'',
      dept: '',
      campus: '',
      mob: '',
      mail: '',
      date:time.format(),
      profile_data:[],
      degree_data:[],
      administrative_data:[],
      section:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setUser();
  }


  fetchOption = () =>{
    this.profileFetch()
  }

  profileFetch = () =>{
    Axios.post('/ework/user/dash2',{username:this.props.username})
    .then(res=>{
      this.degreeFetch();
      this.setState({profile_data:res.data})
    })
  }
  degreeFetch = () =>{
    Axios.post('/ework/user/fetch_degree_for_pdf',{username:this.props.username})
    .then(res=>{
      this.fetchSection();
      this.setState({degree_data:res.data})
    })
  }
  fetchSection=()=>{
    Axios.post('/ework/user/fetch_section_for_view',{username:this.props.username})
    .then(res=>{
      this.administrativeFetch();
      this.setState({section:res.data,loading:false})
    })
  }
  administrativeFetch =()=>{
    Axios.post('/ework/user/fetch_adminis_for_view',{username:this.props.username}).then(res => {
      console.log(res.data)
      this.setState({
        administrative_data: res.data,
      });
    })
  }

  setUser = () =>{
    Axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.mailid})
        console.log(this.state.username)
        this.profileFetch()
      }
    })
  }


  createPdf = (html) => Doc.createPdf(html);

  render() {
    let content,section,administrative_data;

    if(this.state.degree_data){
      content=
      <div className="contains">
      <div className="row mhead"><b>Qualifications</b></div>
              <table>
                <thead>
                  <tr>
                    <td className="td-ch"><b>No.</b></td>
                    <td className="td-ch"><b>Degree Name</b></td>
                    <td className="td-ch"><b>Institute Name</b></td>
                    <td className="td-ch"><b>Start Date</b></td>
                    <td className="td-ch"><b>End Date</b></td>
                    <td className="td-ch"><b>Grade/CGPA</b></td>
                    </tr>
                </thead>

              <tbody>
                {this.state.degree_data.map((content,index)=>{
                  return(

                    <tr key={index}>
                          <td className="td-ch">{index+1}</td>
                          <td className="td-ch">{content.action}</td>
                          <td className="td-ch">{content.Collage_Name}</td>
                          <td className="td-ch">{content.Start_Year}</td>
                          <td className="td-ch">{content.End_Year}</td>
                          <td className="td-ch">{content.Marks_Grade}</td>
                    </tr>
                  )
                })}
              </tbody>

      </table>
      </div>
    }
    if(this.state.section)
    {
      section =
      <div className="contains">
      <div className="row mhead"><b>Section Data</b></div>
              <table>
                <thead>
                  <tr>
                    <td className="td-ch"><b>No.</b></td>
                    <td className="td-ch"><b>Description</b></td>
                    <td className="td-ch"><b>Role</b></td>
                    <td className="td-ch"><b>Achievement(s)</b></td>
                    <td className="td-ch"><b>Start Date</b></td>
                    <td className="td-ch"><b>End Date</b></td>
                  </tr>
                </thead>

              <tbody>
                {this.state.section.map((content,index)=>{
                  return(

                    <tr key={index}>
                          <td className="td-ch">{index+1}</td>
                          <td className="td-ch">{content.action}</td>
                          <td className="td-ch">{content.role}</td>
                          <td className="td-ch">{content.achivements}</td>
                          <td className="td-ch">{content.date_of_starting}</td>
                          <td className="td-ch">{content.date_of_complete}</td>
                    </tr>
                  )
                })}
              </tbody>

      </table>
      </div>



    }

    return (
      <React.Fragment>

        <div className="row">
          <div className="col s3"></div>
          <div className="col s6">
            <PdfContainer loading={this.state.loading} createPdf={this.createPdf}>
          <React.Fragment>
            <section className="pdf-body">
              <div className="row">
                <span><img alt="logo_image" src={Logo} className="logo"/></span>
                <h2 className="htitle"><center>SRM INSTITUTE OF SCIENCE AND TECHNOLOGY <br/>PROFILE REPORT</center></h2>
                <p className="center" style={{marginTop:'-3%', marginBottom:'-3%'}}>(For Faculty Members)</p>
              </div>
              <div className="row">
                <p className="col s3 gen"><b>Name<br/>Faculty Id<br/>Department<br/>Designation<br/> Mobile No.<br/>Campus<br/></b> </p>
                <p className="col s9 gen">: {this.state.profile_data.name} <br/>: {this.state.profile_data.username} <br/>: {this.state.profile_data.dept} <br/>: {this.state.profile_data.desgn} <br/>: {this.state.profile_data.phone} <br/>: {this.state.profile_data.campus}  </p>

              </div>

              {this.state.degree_data && content}

              {this.state.section && section}

              {this.state.administrative_data && administrative_data}

              <div className="row right"> <p className="wat">{this.state.date}</p> </div>
            </section>
          </React.Fragment>
        </PdfContainer>

          </div>
        </div>

      </React.Fragment>
    );

  }
}

export default Print;
