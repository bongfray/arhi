import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import AddProduct from './add';
import ProductList from './prod'
var empty = require('is-empty');

export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      SectionProps:[],
      loading:true,
      username:'',
      redirectTo:'',
      option:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      login:'/ework/flogin',
      get:'/ework/user/',
      noti_route:true,
      nav_route: '/ework/user/fetchnav',
      isExpanded:false,
      disabled_expanse:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }

handleOption = (e) =>{
  this.setState({option: e.target.value,isExpanded:true,disabled_expanse:false})
}
getUser()
{
  axios.get('/ework/user/')
   .then(response =>{
     this.setState({loading: false})
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
       this.getSectionData()
     }
     else{
       this.setState({
         redirectTo:'/ework/faculty',
       });
       window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
     }
   })
}
getSectionData()
{
  axios.post('/ework/user/fetch_section',{action:'Section'}).then(response =>{
    console.log(response.data)
    this.setState({
     SectionProps: response.data,
   })
  })
}

componentDidMount(){
  this.getUser()
}


   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/ework/user/editt';
     } else {
       apiUrl = '/ework/user/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

handleExpand =(e) => {
  this.setState({isExpanded:!this.state.isExpanded});
};

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Description of Your Deeds",
          name: "description",
          placeholder: "Description of your work",
          type: "text",
          grid: "col l3 xl3 s2 m1 center wrap"
        },
        {
          header: "Your Role",
          name: "role",
          placeholder: "Enter Your Role",
          type: "text",
          grid: "col l2 s2 m1 xl2 center wrap"
        },

        {
          header: "Your Achivements",
          name: "achivements",
          type: "text",
          grid: "col l3 s2 m1 xl3 center wrap",
          placeholder: "Enter Details about your Achivements"
        },
        {
          header: "Date of Starting",
          name: "date_of_starting",
          type: "number",
          grid: "col m1 s2 l1 xl1 center wrap",
          placeholder: "Enter the Date of Starting"
        },
        {
          header: "Date of Completing",
          name: "date_of_complete",
          type: "number",
          grid: "col s2 m1 l1 xl1 center wrap",
          placeholder: "Enter the Date of Completion"
        },

      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct cancel={this.updateState} username={this.state.username} action={this.state.option} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }
    let section_datas;
    if(this.state.SectionProps)
    {
      section_datas =
      <FormControl style={{width:'100%'}}>
         <InputLabel id="free_type">Select Here</InputLabel>
         <Select
           labelId="free_type"
           id="free_type"
           value={this.state.option}
           onChange={this.handleOption}
         >
         {this.state.SectionProps.map((content,index)=>{
                 return(
                <MenuItem key={index} value={content.section_data_name}>{content.section_data_name}</MenuItem>                 )
           })}
         </Select>
       </FormControl>
    }
    else
    {
      section_datas =
      <FormControl style={{width:'100%'}}>
         <InputLabel id="free_type1">Select Here</InputLabel>
         <Select
           labelId="free_type1"
           id="free_type1"
         >

                <MenuItem disabled>Fetching....</MenuItem>                 )
         </Select>
       </FormControl>
    }

    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
       <rect x="130" y="20" rx="3" ry="3" width="120" height="20" />
        <rect x="35" y="60" rx="3" ry="3" width="330" height="30" />
      </ContentLoader>
    )
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading ===  true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    return(
      <React.Fragment>
      <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div className="row">
         <div className="col l1 xl1 hide-on-mid-and-down" />
         <div className="col l10 s12 m12 xl10">
             <div className="row particular" style={{marginTop:'30px'}}>
                <div className="col l8 xl8 m12 s12">
                  <p style={{fontSize:'22px'}}>Kindly Select On Which Area You Are Going to Upload You Datas</p>
                </div>
                <div className="col l4 s12 xl4 m12">
                {section_datas}
                </div>
              </div>
          </div>
          <div className="col l1 xl1 hide-on-mid-and-down" />
        </div>
        <div className="section-data" style={{padding:'15px'}}>
                <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
                  <ExpansionPanelSummary
                  style={{borderRadius:'20px'}}
                    expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                    className="mine-collap"
                  >
                    <div style={{color:'yellow',fontSize:'25px'}}>{this.state.option}</div>
                          </ExpansionPanelSummary>
                              <div style={{padding:'15px'}}>
                                {!this.state.isAddProduct &&
                                     <ProductList username={this.props.username} title={this.state.option}  action={this.state.option} data={data}  editProduct={this.editProduct}/>
                                }
                                <br />
                                {!this.state.isAddProduct &&
                                 <React.Fragment>
                                 <div className="row">
                                 <div className="col l6 s12 xl6 m12 left" />
                                 <div className="col l6 s6 xl6 m6">
                                    {this.state.option && <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>}
                                   </div>
                                </div>
                                </React.Fragment>
                              }
                                { productForm }
                                <br/>
                              </div>
                        </ExpansionPanel>
            </div>
      </React.Fragment>

    )
}
}
  }
}
