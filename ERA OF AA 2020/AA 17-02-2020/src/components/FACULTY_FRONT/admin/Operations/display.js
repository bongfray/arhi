import React, { Component} from 'react'
import { } from 'react-router-dom'

import InsertUser from './insertuser'
import UpdateUser from './updateuser'
import SuperUser from './superuser'
import DeleteUser from './deleteuser'
import ModifyUser from './modifyuser'
import ViewUser from './view_user'


export default class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: '',
            select: '',
            active:'',
        }
    }
    handleCheck = (e) => {
        this.setState({
            isChecked: e,
            select: e
        })

        if (this.state.active === e) {
          this.setState({active : null})
        } else {
          this.setState({active : e})
        }

    }
    color =(position) =>{
      if (this.state.active === position) {
          return "#00695c teal darken-3 white-text display-style";
        }
        return "display-style hover-effect";
    }

    render(){
      let choosed_option;
        if(this.props.choosed === 'Insert')
        {
            choosed_option=
              <div className="row ">
              <div></div>
              <div className="col s12 l12 xl12 m12">
                  <div className="col s12 xl2 m2 l2">
                        <div className={this.color('insertadmin')} onClick={(e)=>{this.handleCheck('insertadmin')}} >Insert Department Admin </div>
                        <div className={this.color('insert_in_nav')} onClick={(e)=>{this.handleCheck('insert_in_nav')}} >Insert Into NavBar</div>
                        <div className={this.color('responsibilty_percentages')}  onClick={(e)=>{this.handleCheck('responsibilty_percentages')}} >Insert Responsibilty Percentages</div>
                        <div className={this.color('section_part_insert')} onClick={(e)=>{this.handleCheck('section_part_insert')}} >Insert Options For Section's Dropdown</div>
                        <div className={this.color('insert_designation')}  onClick={(e)=>{this.handleCheck('insert_designation')}}>Insert Designations</div>
                        <div className={this.color('insert_department')}  onClick={(e)=>{this.handleCheck('insert_department')}} >Insert Department</div>
                        </div>
                        <div className="col s12 l10 xl10 m10">
                                <InsertUser select={this.state.select} />
                        </div>


                    </div>
                    </div>
        }
        else if(this.props.choosed === 'Update')
        {
            choosed_option=
                <div className="row ">
                <div className="col s12 l12 xl12 m12">
                    <div className="col s12 xl2 m2 l2">
                        <div className={this.color('facpwd')} onClick={(e)=>{this.handleCheck('facpwd')}}>Reset Faculty or Admin Password</div>
                        <div className={this.color('studpwd')} onClick={(e)=>{this.handleCheck('studpwd')}} >Reset Student Password</div>
                        <div className={this.color('all_user_dayorder_entry')} onClick={(e)=>{this.handleCheck('all_user_dayorder_entry')}}>Render Missed DayOrder</div>
                    </div>
                    <div className="col s12 l10 xl10 m10">
                            <UpdateUser select={this.state.select} />
                    </div>


                </div>
                </div>
        }
        else if(this.props.choosed === 'Super')
        {
            choosed_option=
            <div className="row ">
            <div className="col s12 l12 xl12 m12">
                <div className="col s12 xl2 m2 l2">
                        <div className={this.color('register')} onClick={(e)=>{this.handleCheck('register')}}>Handle Registration</div>
                        <div className={this.color('signup')} onClick={(e)=>{this.handleCheck('signup')}}>Handle Signup</div>
                        <div className={this.color('approve_single_faculty_req')} onClick={(e)=>{this.handleCheck('approve_single_faculty_req')}}>Requests From Users(for data entry for a Single Dayorder)</div>
                        <div className={this.color('blue_print')} onClick={(e)=>{this.handleCheck('blue_print')}}>Requests From Users(For Blue Print Render)</div>
                        <div className={this.color('suspendfac')} onClick={(e)=>{this.handleCheck('suspendfac')}} >Suspend Faculty or Admin</div>
                        <div className={this.color('suspendstu')}  onClick={(e)=>{this.handleCheck('suspendstu')}} >Suspend Student</div>
                        <div className={this.color('suspend_request')} onClick={(e)=>{this.handleCheck('suspend_request')}}>Suspended Request</div>
                    </div>
                    <div className="col l10 s12 m10 xl10">
                            <SuperUser select={this.state.select} />
                    </div>
                </div>
                </div>
        }
        else if(this.props.choosed === 'Delete')
        {
            choosed_option=
                <div className="row">
                <div className="col s12 l12 xl12 m12">
                <div className="col s12 l2 xl2 m2">
                <div className={this.color('deleteadmin')} onClick={(e)=>{this.handleCheck('deleteadmin')}}>Delete Admin</div>
                <div className={this.color('deletefac')}  onClick={(e)=>{this.handleCheck('deletefac')}}>Delete Faculty</div>
                <div className={this.color('deletestu')} onClick={(e)=>{this.handleCheck('deletestu')}}>Delete Student</div>
                </div>
                <div className="col s12 l10 xl10 m10">
                    <DeleteUser select={this.state.select} />
                </div>
                </div>
                </div>
        }
        else if(this.props.choosed === 'Modify')
            {
            choosed_option=
                <div className="row">
                <div className="col s12 l12 m12 xl12">
                <div className="col s12 l2 xl2 m2">
                <div className={this.color('start-semester')} onClick={(e)=>{this.handleCheck('start-semester')}}>Start Semester Date</div>
                <div className={this.color('end-semester')} onClick={(e)=>{this.handleCheck('end-semester')}}>End Semester Date</div>
                <div className={this.color('modifytoday')} onClick={(e)=>{this.handleCheck('modifytoday')}}>Modify Today's Day Order</div>
                <div className={this.color('cancel_or_declare_today_as_a_holiday')} onClick={(e)=>{this.handleCheck('cancel_or_declare_today_as_a_holiday')}} >Cancel Today's DayOrder<br />(Update here only if today is, also a holiday)</div>
                <div className={this.color('holiday')} onClick={(e)=>{this.handleCheck('holiday')}}>Declartion for UpComing Holiday</div>
                </div>
                <div className="col s12 l10 xl10 m10" style={{marginTop:'25px'}}>
                    <ModifyUser select={this.state.select} />
                </div>
                </div>
                </div>
        }
        else if(this.props.choosed === 'View')
        {
            choosed_option=
                <div className="row">
                <div className="col s12 l12 xl12 m12">
                <div className="col s12 l2 xl2 m2">
                <div className={this.color('dept_admin_profile')} onClick={(e)=>{this.handleCheck('dept_admin_profile')}}>View Department Admin Profile</div>
                <div className={this.color('facprofile')} onClick={(e)=>{this.handleCheck('facprofile')}}>View Faculty Profile</div>
                <div className={this.color('studprofile')}  onClick={(e)=>{this.handleCheck('studprofile')}}>View Student Profile</div>
                </div>
                <div className="col s12 xl10 l10 m10">
                    <ViewUser select={this.state.select} />
                </div>
                </div>
                </div>
        }

        else{
            choosed_option=
                <div></div>
        }
        return(
          <React.Fragment>
             <div className="row" style={{marginTop:'15px'}}>
               {choosed_option}
             </div>
          </React.Fragment>
        )
    }
}
