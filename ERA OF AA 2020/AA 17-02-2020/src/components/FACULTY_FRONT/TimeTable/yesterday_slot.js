import React,{ Component} from 'react'
import { } from 'react-router-dom'
import { Link , Redirect} from 'react-router-dom'
import axios from 'axios'
import ColorRep from './colordiv.js'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Nav from '../../dynnav'
require("datejs")


class DayOrder extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class YesSimple extends Component{
  constructor(props) {
    super(props);
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.state = {
      opendir: '',
      display:'block',
      saved_dayorder:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      noti_route:true,
      nav_route: '/ework/user/fetchnav',
    }
  }

  getDayOrder = () =>{
    var yesterday = Date.parse("yesterday").toString("dd");
    var yestermonth = Date.parse("yesterday").toString("M");
    var yesteryear = Date.parse("yesterday").toString("yyyy");
    axios.post('/ework/user/fetch_yesterday_dayorder', {day:yesterday,month:yestermonth,year:yesteryear
    })
    .then(response =>{
       if((response.data.initial ===0)  || (response.data.initial ===1)  || (response.data.stock===0))
        {
          this.setState({saved_dayorder:0,display:'none'})
        }
      else
      {
        if(response.data.initial)
        {
          this.setState({saved_dayorder:response.data.initial})
        }
        else{
          this.setState({saved_dayorder:response.data.stock})
        }
      }
    });
  }



  componentDidMount(){
    this.getDayOrder();
  }

  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };
  render(){
    var yesterday = Date.parse("yesterday").toString("dd");
    var yestermonth = Date.parse("yesterday").toString("M");
    var yesteryear = Date.parse("yesterday").toString("yyyy");
    if (this.state.redirectTo)
    {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     }
      else
      {
    return(
      <React.Fragment>
      <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div className="row">
            <div className="col l4"/>
            <div className="col l4 blue-text center"  style={{fontSize:'20px'}}>Yesterday's DayOrder</div>
            <div className="col l4">
              <Link to="time_new" className="right" style={{color:'black',marginTop:'10px'}}>Go to Current DayOrder</Link><br />
            </div>
          </div>
        <div className="row">
          <div className="col l2">
              <DayOrder day_order={this.state.saved_dayorder}/>
            <br />
            <div className="status_of_day" style={{display:this.state.display}}>
                  <FormControl style={{width:'100%'}}>
                    <InputLabel id="sel_type">Select Here</InputLabel>
                      <Select
                        labelId="sel_type"
                        id="sel_type"
                        value={this.state.opendir}
                        onChange={this.handledir}
                      >
                      <MenuItem value="r_class">Regular Class</MenuItem>
                      </Select>
                   </FormControl>
              </div>
        </div>
        <div className="col l10">
          <Content opendir={this.state.opendir} day={yesterday} month={yestermonth} year={yesteryear} day_order={this.state.saved_dayorder} usern={this.props.username}/>
        </div>
      </div>

      </React.Fragment>
    );
  }
  }
}


class Content extends Component{
  constructor(){
    super();
    this.state={
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }

  render(){
    if(this.props.opendir==="r_class")
    {
          return(
            <React.Fragment>
             <ColorRep usern={this.props.usern} day={this.props.day} month={this.props.month} year={this.props.year} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else{
      return(
        <React.Fragment>
        {this.props.day_order ===0 ? <div className="def-reg center">No DayOrder</div> : <div className="def-reg center">Please Select from the DropDown</div>

       }
        </React.Fragment>
      );
    }

  }
}
