import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Allot2 from './slot2'
import Switch from './absent'

export default class Color extends Component {
	constructor(props) {
    super(props)
    this.state = {
					day:[
						{time:"8:00-8:50",start_time:"8:00",end_time:"8.50",number:'1'},
						{time:"8:50-9:40",start_time:"8:50",end_time:"9.40",number:'2'},
						{time:"9:45-10:35",start_time:"9:45",end_time:"19.10",number:'3'},
						{time:"10:40-11:30",start_time:"10:40",end_time:"11.30",number:'4'},
						{time:"11:35-12:25",start_time:"11:35",end_time:"12.25",number:'5'},
						{time:"12:30-1:20",start_time:"12:30",end_time:"13.20",number:'6'},
						{time:"1:25-2:15",start_time:"13:25",end_time:"14.15",number:'7'},
						{time:"2:20-3:10",start_time:"14:20",end_time:"15.10",number:'8'},
						{time:"3:15-4:05",start_time:"15:15",end_time:"16.05",number:'9'},
						{time:"4:05-4:55",start_time:"16:05",end_time:"16.55",number:'10'}
					],
    }
    }



render() {
	if(this.props.opendir === 'r_class')
			{
			return(
					<div className="rocontent" style={{marginTop:'70px'}}>
								{this.state.day.map((content,index)=>{
										return(
												<div key={index}>
													<Allotment username={this.props.username} date={this.props.date}
														month={this.props.month} year={this.props.year}
														day_order={this.props.day_order} content={content}
															 />
														</div>
													);
									})}
						</div>
						);
			}
			else if(this.props.opendir ==="own_ab")
	    {
	      return(
	        <div>
	         <Switch day_seq={this.state.day} username={this.props.username} render_div={this.state.day}
					   date={this.props.date}
						 month={this.props.month} year={this.props.year} day_order={this.props.day_order} />
	        </div>
	      );
	    }
	    else{
	      return(
	        <div className="def-reg center">Please Select from the DropDown</div>
	      );
	    }
  }
}


class Allotment extends Component{
	constructor(props)
	{
		super(props)
		this.state ={
			loading: true,
			color:'red',
			alreadyhave:'',
			slot:'',
			day_slot_time:'',
			time:'',
			block:'block',
			permit:false,
			received_data:'',
			datas_a:'',
			day_order:'',
			hour:'',
		}
this.componentDidMount = this.componentDidMount.bind(this)
	}

collect()
{
	axios.post('/ework/user/fetchAllotments',{day_order: this.props.day_order,
		hour:this.props.content.number,
	}
)
	.then(response =>
		{
		if(response.data)
		{
			  let allotdata = (response.data.alloted_slots).toUpperCase();
				this.setState({
					datas_a:response.data,
					alreadyhave: allotdata,
				})
		}
		else
		{
			this.setState({
				alreadyhave: 'Free Slot',
			})
		}
	})
}


color =() =>{
	axios.post('/ework/user/fetchfrom', {
		day_order: this.props.day_order,
		hour:this.props.content.number,
		date: this.props.date,
		month: this.props.month,
		year: this.props.year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
				})
		}
	})

}
comp =()=>{
	axios.post('/ework/user/extra_slot',this.props)
	.then(res => {
		this.setState({loading:false})
    if(res.data === 'yes')
		{
			this.setState({
				block:'none',
			})
		}
	});
}


block =()=>{
	axios.post('/ework/user/knowabsent',{	day_order: this.props.day_order,
	hour:this.props.content.number,
	date:this.props.date,month:this.props.month,year:this.props.year})
	.then(res => {
		console.log(res.data)
	   if(res.data === 'yes')
			{
				this.setState({
					block:'none',
				})
			}
	});
}
	componentDidMount()
	{
		this.collect();
		this.color();
		this.block();
		this.comp();
	}

showContent =(date,month,year,slot,time,day_order,hour)=>{
	this.setState({permit:!this.state.permit,date:date,month:month,year:year,
		slot:slot,time:time,day_order:day_order,hour:hour})
}
	showInfo=()=>{
		this.setState({
			permit:!this.state.permit,
		})
	}

	render()
	{
		if(this.state.loading === true)
		{
			return(
				<div><h6 className="center col l4 s12 xl4 m4">Loading Content...</h6></div>
			)
		}
		else
		{
		if(this.state.color === "red")
		{

			return(
				<div>
				<div className="col l4 s12" style={{display:this.state.block}} key={this.props.time}>
							<Allot2 color={this.color} content={this.props.content} date={this.props.date}
							 month={this.props.month} year ={this.props.year}
							 username={this.props.username} slots={this.state.alreadyhave} day_order={this.props.day_order}
							 datas_a={this.state.datas_a}
							 />
				</div>
				</div>
			);
		}
		else if(this.state.color === "green")
		{
			return(
				<div>
				{this.state.permit &&
					<Receive show={this.showInfo} permit={this.state.permit} date={this.props.date}
					 month={this.props.month} year ={this.props.year}
					 slot={this.state.slot} time={this.state.time}
					 day_order={this.props.day_order} hour={this.state.hour} />
				}
				<div className="col l4 s12 xl4 m4" style={{color: this.state.color}} onClick={()=>this.showContent(
					this.props.date,this.props.month,this.props.year,this.state.alreadyhave,this.props.content.time, this.props.day_order,this.props.content.number
				)}  key={this.props.time}>
						<div  className="card hoverable green white-text each_time">
						  <p className="left yellow-text" style={{marginLeft:'5px'}}>{this.props.content.number}</p>
							<p className="center">{this.props.content.time}</p>
							<p className="center"><b>COMPLETED</b></p>
						</div>
				</div>
				</div>
			);
		}
}

	}
}



class Receive extends Component {
	constructor()
	{
		super()
		this.state={
			received_data:[],
			editable:false,
			covered:'',
			loading:true,
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}
	componentDidMount()
	{
		this.fetchDatas();
	}

	fetchDatas =()=>{
		axios.post('/ework/user/fetch_submitted_data',{date:this.props.date,month:this.props.month,
			year:this.props.year,slot:this.props.slot,time:this.props.time,day_order:this.props.day_order,
		hour:this.props.hour})
		.then( res => {
				if(res.data)
				{
					this.setState({received_data:res.data,covered:res.data.covered,loading:false})
				}
		});
	}

	handleEdit =()=>{
		this.setState({editable:!this.state.editable})
	}

	valueChange =(e)=>{
		this.setState({covered:e.target.value})
	}
   sendUpdates =(e)=>{
		 window.M.toast({html: 'Updating.....',classes:'rounded yellow black-text'});
		if(this.state.covered === this.state.received_data.covered)
		{
          window.M.toast({html: 'Updated !!',classes:'rounded green darken-2'});
					this.handleEdit();
		}
		else{
			axios.post('/ework/user/updateSubmittedData',{data:this.state.received_data,covered:this.state.covered})
			.then( res => {
			    if(res.data === 'ok')
					{
	            window.M.toast({html: 'Updated !!',classes:'rounded green darken-2'});
							this.handleEdit();
							this.fetchDatas();
					}
			})
			.catch( err => {
				window.M.toast({html: 'Something went wrong !!',classes:'rounded #f44336 red'});
			});
		}
	}

  render() {
    return (
      <div>
			{this.state.loading === true ?
				<div className="cover_all" onClick={this.props.show}>
				<div className="up">
				<div style={{padding:'15px'}}>
				<div className="center">
	          <div className="preloader-wrapper big active">
	          <div className="spinner-layer spinner-red">
	              <div className="circle-clipper left">
	               <div className="circle"></div>
	              </div><div className="gap-patch">
	               <div className="circle"></div>
	              </div><div className="circle-clipper right">
	               <div className="circle"></div>
	              </div>
	          </div>
	          </div>
	      </div>
				<div className="center">Fetching Datas.....</div>
				</div>
				</div>
				</div>
				:
			<div className="cover_all">
				<div className="up">
				<div style={{padding:'20px'}}>
					<React.Fragment>
					 <h5 className="center">Submitted Data in, <span className="pink-text">{this.state.received_data.freefield}</span></h5><br />
					 <div className="row">
					 {this.state.received_data.freefield === 'Academic' &&
						 <React.Fragment>
								 <React.Fragment>
									{this.state.received_data.verified ?

										<span className="right styled-btn green-text">Verified</span>

										:
											<span className="right styled-btn red-text">Not Verified</span>
									}
									</React.Fragment>

							</React.Fragment>

					}
						</div>
						<div className="row" style={{padding:'10px'}}>
						{this.state.received_data.freeparts && <div className="row">
							 <div className="col l4 xl4 s6 m5"><b>You have submitted the datas on </b></div>
							 <div className="col l8 xl8 s6 m7">{this.state.received_data.freeparts}</div>
						</div>
						 }

						 {this.state.received_data.slot &&
							 	<div className="row">
										<div className="col l4 xl4 s6 m5"><b>Slot</b></div>
										<div className="col l8 xl8 s6 m7">{this.state.received_data.slot}</div>
								</div>
						 }

						 {this.state.received_data.subject_code &&
								<div className="row">
										<div className="col l4 xl4 s6 m5"><b>Subject Code</b></div>
										<div className="col l8 xl8 s6 m7">{this.state.received_data.subject_code}</div>
								</div>
						 }

						 {this.state.received_data.compense_faculty &&
								<div className="row">
										<div className="col l4 xl4 s6 m5"><b>Slot Handled to Faculty</b></div>
										<div className="col l8 xl8 s6 m7">{this.state.received_data.compense_faculty}</div>
								</div>
						 }

						 {this.state.received_data.compense_faculty_topic &&
								<div className="row">
										<div className="col l4 xl4 s6 m5"><b>Handled Topic by Foreign Faculty</b></div>
										<div className="col l8 xl8 s6 m7">{this.state.received_data.compense_faculty_topic}</div>
								</div>
						 }


		{this.state.received_data.freefield === 'Academic' ?
						 <React.Fragment>

					{this.state.received_data.covered && <div className="row">
						 <div className="col l4 xl4 s6 m5"><b>Submitted Data</b></div>
						 <div className="col l8 xl8 s6 m7">
               {this.state.editable === false ?
								 <React.Fragment>
							   	{this.state.received_data.verified === false ?
										<React.Fragment>
										  {this.state.received_data.covered}&ensp;
										 <i onClick={this.handleEdit} className="go material-icons small">edit</i>
										</React.Fragment>
										:
										<React.Fragment>
										  {this.state.received_data.covered}
										</React.Fragment>
									}
								 </React.Fragment>
							 :
								   <React.Fragment>
	                     <div className="input-field">
											   <input id="edit" className="validate" type="text" value={this.state.covered} onChange={this.valueChange} required/>
											 </div>
											 <button onClick={this.handleEdit} className="btn small red white-text">CANCEL</button>&ensp;
											 <button onClick={this.sendUpdates} className="btn small green white-text">SUBMIT</button>
										 </React.Fragment>
							 }
						 </div>
					</div>

					 }
					 </React.Fragment>
					 :
					 <React.Fragment>
							 {this.state.received_data.covered && <div className="row">
									<div className="col l4 xl4 s6 m5"><b>Submitted Data</b></div>
									<div className="col l8 xl8 s6 m7">
										{this.state.editable === false ?
												 <React.Fragment>
													 {this.state.received_data.covered}&ensp;
													<i onClick={this.handleEdit} className="go material-icons small">edit</i>
												 </React.Fragment>
										:
												<React.Fragment>
														<div className="input-field">
															<input id="edit" className="validate" type="text" value={this.state.covered} onChange={this.valueChange} required/>
														</div>
														<button onClick={this.handleEdit} className="btn small red white-text">CANCEL</button>&ensp;
														<button onClick={this.sendUpdates} className="btn small green white-text">SUBMIT</button>
													</React.Fragment>
										}
									</div>
							 </div>

								}
					 </React.Fragment>
				 }

					 {this.state.received_data.problem_statement==="true" && <div className="row">
							<div className="col l4 xl4 s6 m5"><b>Submitted Data</b></div>
							<div className="col l8 xl8 s6 m7">Class was not taken.<br />Reason : {this.state.received_data.problem_statement}</div>
					 </div>
						}
						</div>
						<hr />
						<div className="">
						   <span className="red-text">REMEMBER:</span><br />
							 1. You can only edit the datas within 48 hours.<br />
							 2. Your submitted datas will not be validated,if it's showing NOT VERIFIED (applicable only for Alloted Slots).<br />
							 3. After it is verified, there is no chance to edit it.<br />
							 4. This page will let you to edit the datas only for 24 Hrs. For rest of the
							 24 hours, go for yeaterday dayorder.Rules & Regulation will be applied.
						</div>
					 </React.Fragment>

				<hr />
				<div style={{padding:'10px'}} onClick={this.props.show} className="go right">CLOSE</div>
				</div>
				</div>
		 </div>
	    }
      </div>
    );

  }
}
