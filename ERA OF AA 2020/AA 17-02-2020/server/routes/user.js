const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const Nav = require('../database/models/FAC/nav_stat')
const User = require('../database/models/FAC/user')
const Degrees = require('../database/models/FAC/degree')
const Common_DayOrder = require('../database/models/FAC/dayorder')
const DayOrder_History = require('../database/models/FAC/dayorderhistory')
const Timetable = require('../database/models/FAC/time_table')
const Alloted_Slot = require('../database/models/FAC/schema_for_alloted')
const Request = require('../database/models/FAC/req')
const AdminOrder = require('../database/models/FAC/admin')
const Admin_Operation = require('../database/models/FAC/admin_operation')
const Five = require('../database/models/FAC/fiveyarplan')
const Section = require('../database/models/FAC/section')
const Extra = require('../database/models/FAC/extraop')
const Extra_Handle = require('../database/models/FAC/extrahour_handle')
const Absent = require('../database/models/FAC/absent')
const History = require('../database/models/FAC/history_of_work')
const Notification = require('../database/models/FAC/notification')
const Communication = require('../database/models/FAC/communication')
const passport = require('../passport/fac_pass')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')


/*------------------------------------------------------------------------Changing Day Orders-------------------------------------------- */


let timer;

function nextGo(time)
{
    console.log("Way to fetch next Common_DayOrder.....")
    timer = setTimeout(() =>
    {
    myFunc()
  },time);
}

Common_DayOrder.findOne({day_order : {"$exists" : true} }, function(err, user){
  if(err)
  {

  }
  else if(user)
  {
    let hour = (new Date().getHours())*3600000;
    let minutes = (new Date().getMinutes())*60000;
    let second =(new Date().getSeconds())*1000;
    let total = hour+minutes+second;
    let max = (86400020-total);
    console.log(max);
    nextGo(max);
  }
 else
 {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  var day = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour + minutes + second;
  let time = (86400020-total);
  var day_order;
  var day_order_status;
  if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
  {
    day_order = 0;
    day_order_status = "weekend";
  }
  else
  {
    day_order = 1;
    day_order_status= "online";
  }
    const dayyy = new Common_DayOrder({
      day_order: day_order,
      time: time,
      week: week,
      day_order_status:day_order_status,
      day:day,
      month:month,
      year:year,
      cancel_status: false,
    });
    dayyy.save(() => {
    });

    DayOrder_History.findOne({day_order : {"$exists" : true}}, function(err, user){
      const dayhistory = new DayOrder_History({
        day_order: day_order,
        day:day,
        month:month,
        year:year,
        day_order_status:day_order_status,
        week:week,
        cancel_status: false,
      });
      dayhistory.save(() => {
      });
    });
    nextGo(time);
 }
})

function myFunc()
{
    console.log("Updating Dayorder....")
      Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user1){
        if(err)
        {

        }
        else if(user1)
        {
          var yesday = Date.parse("yesterday").toString("dd");
          var day = Date.today().toString("dd");
          var month = Date.today().toString("M");
          var year = Date.today().toString("yyyy");
          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var today = new Date.today();
          var week =today.getWeek();

          if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
          {
            console.log("Inside weekend Check !!")
            DayOrder_History.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, user){
              if(err)
              {

              }
              else if(!user)
              {
                const dayhistory = new DayOrder_History({
                  day_order: user1.day_order,
                  day:day,
                  month:month,
                  year:year,
                  day_order_status:'weekend',
                  week:week,
                  cancel_status: false,
                });
                dayhistory.save((err, savedUser) => {
                  nextcall();
                });
              }
              else if(user)
              {
              const dayhistory = new DayOrder_History({
                day_order: user1.day_order,
                day:day,
                month:month,
                year:year,
                day_order_status:'weekend',
                week:week,
                cancel_status: user1.cancel_status,
              });
              dayhistory.save((err, savedUser) => {
                const dayorder_n = new Common_DayOrder({
                  day_order: 0,
                  day:day,
                  month:month,
                  year:year,
                  day_order_status:'weekend',
                  time:86400020,
                  week:week,
                  cancel_status: false,
                });
                    dayorder_n.save((err, savedUser) => {
                nextcall();
              });
                  });
            }
            })

          }
          else
          {
            let prev = user1.day_order;
            let recent = prev + 1;
            let next;
            let day_order_status;
            let hour = (new Date().getHours())*3600000;
            let minutes = (new Date().getMinutes())*60000;
            let second =(new Date().getSeconds())*1000;
            let total = hour + minutes + second;
            let max = (86400020-total);
            if(prev === 5)
            {
              next=1;
            }
            else
            {
              next = recent;
            }
            console.log(next);
              DayOrder_History.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, prev_dayorder_history){
                if(err)
                {

                }
                else if(!prev_dayorder_history)
                {
                  day_order_status = "online";
                  let renew_cancel_status = false;
                  update_without_complexity(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else if(prev_dayorder_history)
                {
                    day_order_status = "online";
                    let renew_cancel_status = false;
                   if(prev_dayorder_history.day_order_status === "weekend")
                  {
                    updatedy(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                  }
                  else if(prev_dayorder_history.cancel_status === true)
                  {
                    cancelup(prev_dayorder_history.day_order,day_order_status,day,month,year,max,week,renew_cancel_status)
                  }
                  else
                  {
                    update_without_complexity(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                  }
              }

              })
            }
        }

      });

}

 function updatedy(prev_dayorder,next,day_order_status,day,month,year,max,week,cancel_status)
 {
   Common_DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: next,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(next,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function cancelup(prev_dayorder,day_order_status,day,month,year,max,week,cancel_status)
 {
   Common_DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: prev_dayorder,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(prev_dayorder,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function update_without_complexity(prev_dayorder,today,day_order_status,day,month,year,max,week,cancel_status)
 {
   Common_DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: today,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(today,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function dayorder_history_insert(day_order,day,month,year,week,day_order_status,cancel_status)
 {
   DayOrder_History.findOne({day_order : {"$exists" : true}}, function(err, user){
     const dayhistory = new DayOrder_History({
       day_order: day_order,
       day:day,
       month:month,
       year:year,
       day_order_status:day_order_status,
       week:week,
       cancel_status:cancel_status,
     });
     dayhistory.save(() => {
     });
     return;
   })
 }

function nextcall()
{
  logout();
}

function logout()
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";
  MongoClient.connect(url,{ useUnifiedTopology: true , useNewUrlParser: true } , function(err, db) {
    if (err) throw err;
    var dbo = db.db("eWork");
    dbo.listCollections({name: 'sessions'})
    .next(function(err, collinfo) {
        if (collinfo) {
        dbo.collection("sessions").drop(function(err, delOK) {
          if (err)
          {
            throw err;
          }
          else if (delOK)
          {
              console.log("Session Aborted For All..");
              clearInterval(timer);
              nextGo(86400020);
          }
          db.close();
        });
      }
      else{
        console.log("Session is Not Present..");
        clearInterval(timer);
        nextGo(86400020);
      }
    });
  });
}



/* ---------------------------------------------Fetch Signup Page Render Status-------------------------*/


router.post(
    '/faclogin',(req, res, next) => {
      //console.log(req.ip)
      passport.authenticate('local', function(err, user, info) {
        if (err) {
           return next(err);
         }
        if (!user) {
            res.status(401);
            res.send(info.message);
            return;
        }
        req.logIn(user, function(err) {
          if (err) {
            return next(err);
          }
          if(user.count === 0)
           {
               User.updateOne({ username: req.user.username }, { $inc: { count: 1 }},(err, user) => {
               })
           }
           else if (user.count === 1)
           {
               User.updateOne({ username: req.user.username }, { count: 2 },(err, user) => {
               })
           }
          var userInfo = {
              username: req.user.username,
              count: req.user.count,
          };
          res.send(userInfo);
        });
      })(req, res, next);
    }
)

router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

router.post('/knowSuprimityStatus', (req, res) => {
  User.findOne({username:req.user.username},(err,found)=>{
    if(err){

    }
    else if(found){
      if(found.suprimity === false){
        res.send('no')
      }
      else{
        res.send(found.task)
      }
    }
  })
})




/* -----------------------------------------Fetching Request From Nav Bar ------------------------*/
  router.get('/fetchnav',function(req,res) {
    Nav.find({ }, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

  /*------------------------------------Notification----------------------------------------------*/
  router.get('/fetch_notification', function(req, res) {
    Notification.find({$and:[{for:req.user.username},{new:true}]}, function(err, passed){
      if(err)
      {

      }
      else if(passed)
      {
        res.send(passed)
      }
    })
    });

    router.get('/fetch_new_notification', function(req, res) {
      if(req.user)
      {
        Notification.find({$and:[{new:true},{for:req.user.username}]}, function(err, passed){
          if(err)
          {

          }
          else if(passed)
          {
            res.send(passed)
          }
        })
       }
       else{
         res.send('no_u')
       }
      });

      router.post('/mark_as_read', function(req, res) {
        Notification.updateOne({$and:[{new:true},{serial:req.body.serial},{for:req.user.username}]},{new:false}, function(err, passed){
          if(err)
          {

          }
          else if(passed)
          {
            res.send('ok')
          }
        })
        });

    router.post('/clear_all_notification', function(req, res) {
      Notification.deleteMany({for:req.user.username},function(err,succ){
        if(err)
        {

        }
        else if(succ)
        {
          res.send('done')
        }
      });
      });

      router.post('/clear_one_notification', function(req, res) {
        Notification.deleteOne({$and:[{for:req.user.username},{serial:req.body.id}]},function(err,succ){
          if(err)
          {

          }
          else if(succ)
          {
            res.send('done')
          }
        });
        });

    /*----End Notification*/


/*--------------------------------------To Set the color of the 10 Cards------------------ */

router.post('/fetchfrom', function(req, res) {
  //console.log(req.body)
  const {date,month,year} = req.body;
  Timetable.findOne({$and: [{day_order:req.body.day_order},{hour:req.body.hour},
    {username: req.user.username},{date:date},{month:month},{year:year}]}, function(err, passed)
    {
    res.send(passed);
  })
  });

/*------------------------To Render Slots in 10 Cards in Timetable---------------------------*/

  router.post('/fetchAllotments', function(req, res) {
    //console.log(req.body)
        Alloted_Slot.findOne({$and: [{timing : (req.body.day_order+'.'+req.body.hour)},
          {username: req.user.username}]}, function(err, passed)
          {
            res.send(passed)
        })
      });
//view_it
      router.post('/knowabsent', function(req, res) {
      //  console.log(req.body)
        Absent.findOne({$and:[{official_id:req.user.username},{date:req.body.date},
        {month:req.body.month},{year:req.body.year}]},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {
            console.log(req.body.day_order+'.'+req.body.hour)
            Alloted_Slot.findOne({$and: [{timing : req.body.day_order+'.'+req.body.hour},
            {username: req.user.username}]}, function(err, passed){
                if(err)
                {

                }
                else if(passed)
                {
                  res.send('yes');
                }
                else{
                  res.send('no');
                }
            })
          }
          else
          {
            res.send('no');
          }
        })
          });


          router.post('/extra_slot', function(req, res) {
          			Extra.findOne({$and: [{action:'Foreign Extra Handle'},{faculty_id:req.body.username},{date:req.body.date},{month:req.body.month},
                {year:req.body.year},{day_order:req.body.day_order},{hour:req.body.content.number}]},
                  function(err, passed){
          					if(err)
          					{

          					}
          					else if(passed)
          					{
                      console.log('Got Value')
          						res.send('yes');
          					}
          					else
                    {
                      Extra.findOne({$and: [{action:'Own Extra Handle'},{order_from:req.body.username},{problem_compensation_date:req.body.date.substring(1)},{month:req.body.month},
                      {year:req.body.year},{problem_compensation_hour:req.body.content.number}]},(err1,found1)=>{
                        if(err1)
                        {

                        }
                        else if(found1)
                        {
                          res.send('yes');
                        }
                        else{
                          res.send('no')
                        }
                      })
          					}
          			})
          		});



/*---------------------------------------------------Today's Day Order Fetch----------------------------------- */
  router.get('/fetchdayorder', function(req, res) {
    if(!req.user)
    {
      res.send("Not")
    }
    else
    {
      var day = Date.parse("yesterday").toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      DayOrder_History.findOne({$and: [{username: req.user.username},{day:day},{month:month},{year:year},{day_order_status:"cancel"}]}, function(err, user){
        if(err)
        {

        }
        else if(user)
        {
          var cancel ={
            cancel_day: user.day_order,
          }
          res.send(cancel);
        }
        else
        {
          Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
            if(err){

            }
            else if(user)
            {
                    var day_order ={
                      day_order: user.day_order,
                    }
                    res.send(day_order);

            }
          });
        }

        })
    }
    });
/*--------------------------------------------------YesterDay's Common_DayOrder Fetch----------------------------------------------*/
router.post('/fetch_yesterday_dayorder', function(req, res) {
  console.log(req.body)
  DayOrder_History.findOne({$and: [{day:req.body.day},{month:req.body.month},{year:req.body.year}]}, function(err, started){
    if(err)
    {

    }
    else if(started)
    {
      var stock ={
        stock: started.day_order,
      }
      res.send(stock);
    }
    else
    {
      Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err){

        }
        else if(user)
        {
                var initial ={
                  initial: user.day_order,
                }
                res.send(initial);
        }
      });
    }

    })
  });


  /*----------------------------------------End---------------------------- */

    router.post('/fetchfromtimetable', function(req, res) {
      if(!req.user)
      {
        res.send("Not")
      }
      else
      {
      const {day_slot_time} = req.body;
                  Alloted_Slot.findOne({$and: [{timing: day_slot_time},{username: req.user.username}]}, function(err, passed){
                    if(passed)
                    {
                      var alloted_slots ={
                        alloted_slots: passed.alloted_slots,
                      }
                      res.send(alloted_slots);
                    }
                    else{
                      var message ={
                        message: "There is no Saved Data for This Slot",
                      }
                      res.send(message);
                    }
                  })
          }
      });


router.post('/', (req, res) => {
    User.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          const newUser = new User({
              active:false,
              suspension_status:false,
              username: req.body.username,
              password: req.body.password,
              title: req.body.title,
              name: req.body.name,
              mailid: req.body.mailid,
              phone: req.body.phone,
              campus: req.body.campus,
              dept: req.body.dept,
              desgn: req.body.desgn,
              dob: req.body.dob,
              count: 0,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              h_order: req.body.h_order,
              suprimity:false,
              task:'',
              inserted_by:null,
              faculty_adviser:false,
              faculty_adviser_year:'',
              render_timetable:false,
              })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})


router.post('/fetch_faculty_adviser', (req, res) => {
  //console.log(req.body)
    User.find({$and:[{dept:req.body.datas.dept},{campus:req.body.datas.campus},
      {faculty_adviser_year:req.body.datas.year},{faculty_adviser:true}]}, (err, user) => {
        if(err)
        {

        }
        else
        {
          res.send(user)
        }
    })
})


router.post('/newd', (req, res) => {

    const {up_username,up_phone,up_name,up_dob} = req.body;
    User.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            User.updateOne({username:req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          User.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)

/*---------------------------------------------reseting password by mail ------------------------------- */
router.post('/reset_from_mail', (req, res) => {
  const {token_come,password} = req.body;
  User.findOne({ resetPasswordToken: token_come }, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if(user) {
        let hash = bcrypt.hashSync(password, 10);
          User.updateOne({resetPasswordToken: token_come},{password: hash} ,(err, user) => {
            var succ= {
              succ: "Password Updated!!"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "failed"
          };
          res.send(fail);
        }
      })
    })


/*End of Signup------------------------------------------------------------------------------ */



/* -------------------------------Degree & Certificates--------------------------------------------------------------*/
router.post('/profile1', (req, res) => {
          var tran = new Degrees(req.body.data);
          tran.save((err, savedUser) => {
            var succ= {
              succ: "Successfully Submitted"
            };
            res.send(succ);
          })
})



router.post('/editprofile1', function(req,res) {
  Degrees.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Edited"
    };
    res.send(succ);
  })

});


router.post('/fetchprofile1',function(req,res) {
  if(req.user)
  {
  Degrees.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
  if(err)
  {

  }
  else
  {
  res.send(docs)
  }
  });
}
})

router.post('/fetch_degree_for_pdf',function(req,res) {
  if(req.user)
  {
    let username;
    if(req.body.username)
    {
      username =  req.body.username;
    }
    else{
      username =  req.user.username;
    }
  Degrees.find({username: username}, function(err, docs){
  if(err)
  {

  }
  else
  {
  res.send(docs)
  }
  });
}
})

router.post('/fetchtoedit',function(req,res) {
Degrees.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/profile1del',function(req,res) {
  Degrees.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
})


/*End of PROFILE1-------------------------------------------------------------------------------- */

router.get('/knowcount', function(req, res) {
  if(req.user)
  {
    User.findOne({ username: req.user.username }, function(err, objs){

        if (objs)
        {
            res.send(objs);
        }
    });
}
    });

//view_it
router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    // console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })
})


router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

router.post('/dash2', function(req, res) {
  if(req.user)
  {
  let username;
  if(req.body.username)
  {
    username = req.body.username;
  }
  else{
    username = req.user.username;
  }
    User.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});

    router.post('/timeallot', (req, res) => {
      //console.log(req.body)
        const {date,month,year} = req.body;
              Timetable.findOne({$and: [{day_order:req.body.day_order},{hour:req.body.hour},{username: req.user.username},
                {date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
                  if (err)
                  {
                      return;
                  }
                  else if(objs)
                  {
                    res.send('no');
                  }
                  else if (!objs)
                  {
                          if(req.user.username === req.body.compense_faculty)
                          {
                             res.send('spam');
                          }
                          else{
                            const newTime = new Timetable(req.body)
                            newTime.save((err, savedUser) => {
                              if(err){

                              }
                              else if(savedUser){
                                if(err){

                                }
                                else if(savedUser)
                                {
                                  if(req.body.freeparts === 'Curriculum')
                                  {
                                    var differ = false;
                                    Academic_Calculate(req,res,differ);
                                  }
                                  else
                                  {
                                    if(req.body.compense_faculty)
                                    {
                                      saveExtra(req.body,'Foreign Extra Handle',req,res)
                                    }
                                    else if(req.body.problem_compensation_date)
                                    {
                                      saveExtra(req.body,'Own Extra Handle',req,res)
                                    }
                                    else
                                    {
                                      res.send('done');
                                    }
                                  }

                                  if(req.body.verified === false){
                                    setTimeout(function()
                                    {
                                      validate(req,res,req.body);
                                    }, 172800040);
                                  }
                                }
                              }
                            });
                          }
                  }
            });

  })

  function validate(req,res,content)
  {
      Timetable.findOne({$and: [{day_order:content.day_order},{username:req.user.username},
        {hour:content.hour},{date:content.date},{month:content.month},{year:content.year},
        {week:content.week},{verified:false},{freeparts:'Curriculum'}]}, (err, objs) => {
        if(err)
        {

        }
        else if(objs)
        {
            Timetable.updateOne({$and: [{day_order:content.day_order},{username:req.user.username},
              {hour:content.hour},{date:content.date},{month:content.month},{year:content.year},
              {week:content.week},{verified:false},{freeparts:'Curriculum'}]},{verified:true}, (err, obb) => {
              if(err){
                return;
              }
              else if(obb)
              {
                Academic_Calculate(req,res,false);
              }
            })
        }
        else{

        }
      })
  }


  function Academic_Calculate(req,res,dif)
  {
    let username,desgn;
    if(dif === true)
    {
      username = req.username;
      desgn = req.desgn;
    }
    else {
      username = req.user.username;
      desgn = req.user.desgn;
    }

    var month = parseInt(Date.today().toString("M"));
    var year = parseInt(Date.today().toString("yyyy"));
    Date.prototype.getWeek = function ()
    {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };
    var today = new Date.today();
    var week =today.getWeek();
    Admin_Operation.aggregate([{ $match: {$and:[{usertype:desgn},{action:'Academic'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
      if(err)
      {

      }
      else if(output)
      {
        Timetable.find({username:username},(err, c)=> {
          if(err)
          {

          }
          else if(c)
          {
            var weekly = c.filter(function(content) {
              return ((content.week===week) && (content.verified === true) && (content.freefield === "Academic") && (content.freeparts==='Curriculum') && (content.month=== month) && (content.year === year));
            });
            var monthly = c.filter(function(content) {
              return ((content.month=== month) && (content.verified === true) && (content.freefield === "Academic") && (content.freeparts==='Curriculum') && (content.year === year));
            });
            let week_p= (weekly.length / output[0].totalthings) * 100;
            let month_p= ((monthly.length) / ((output[0].totalthings) *4 ))  * 100;
            console.log(week_p)
            console.log(month_p)
            History.findOne({$and:[{username:username},{term:'Academic'},{action:'GRADE'}]},(er,matched)=>{
                if(er)
                {

                }
                else if(matched)
                {
                  console.log('In Matched')
                  if((week === matched.week) && (month === matched.month) && (year === matched.year))
                  {
                    console.log('In Updation')
                    History.updateOne({$and:[{week:week},{month:month},{year:year},
                      {username:username},{term:'Academic'},{action:'GRADE'}]},{$set:{weekly_percentage:week_p,monthly_percentage:month_p}},(prob,done)=>{
                      if(prob)
                      {

                      }
                      else if(done)
                      {
                        res.send('done')
                      }
                    })
                  }
                  else
                  {
                    console.log('In VERIOUS')
                     if(week!==matched.week)
                     {
                       if((month === matched.month) && (year === matched.year))
                       {
                         addDatas(week,matched.month,matched.year,username,week_p,month_p);
                       }
                       else
                       {
                         addDatas(week,month,matched.year,username,week_p,month_p)
                       }
                     }
                     else if(month!==matched.month)
                     {
                       if((week === matched.week) && (year === matched.year))
                       {
                         addDatas(matched.week,month,matched.year,username,week_p,month_p);
                       }
                       else if((week === matched.week) && (year !== matched.year))
                       {
                         addDatas(matched.week,month,year,username,week_p,month_p);
                       }
                       else
                       {
                         addDatas(week,month,year,username,week_p,month_p)
                       }
                     }
                     else if(year!==matched.year)
                     {
                       if((month === matched.month) && (week === matched.week))
                       {
                         addDatas(matched.week,matched.month,year,username,week_p,month_p);
                       }
                       else if((month === matched.month) && (week !== matched.week))
                       {
                         addDatas(week,matched.month,year,username,week_p,month_p)
                       }
                       else if((month !== matched.month) && (week === matched.week))
                       {
                         addDatas(matched.week,month,year,username,week_p,month_p)
                       }
                     }
                  }
                }
                else
                {
                  console.log('In NEW')
                  addDatas(week,month,year,username,week_p,month_p)
                }
              })

              function addDatas(week,month,year,username,week_p,month_p)
              {
                const allottime = new History({
                  week:week,
                  month:month,
                  year:year,
                  action:'GRADE',
                  username: username,
                  weekly_percentage:week_p,
                  monthly_percentage:month_p,
                  term:'Academic',
                })
                allottime.save((err, saved) => {
                  if(err)
                  {

                  }
                  else if(saved)
                  {
                      res.send('done');
                  }

                })
              }
          }
        })
      }
    })
  }




router.post('/free', (req, res) => {
  const { covered,freefield, order,date,month,year,week } = req.body;
  Extra_Handle.findOne({$and: [{dayorder_hour: req.body.day_order+'.'+req.body.hour},{action:'Foreign_ExtraCompleted'},{handled_by: req.user.username},{day: date},{month: month},{year: year},{week:req.body.week}]}, (err, data) => {
    if(err)
    {

    }
    else if(data)
    {
      var already_have= {
        already_have: "You have already submitted data on behalf of Foreign Request !!"
      };
      res.send(already_have);
    }
    else{
      Timetable.findOne({$and: [{day_slot_time:  req.body.day_order+'.'+req.body.hour},{username: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
          if (err)
          {

          }
          else if (objs)
          {

          }
          else if(!objs) {
            Timetable.countDocuments({$and:[{freeparts:req.body.freeparts},{freefield:req.body.freefield},{username:req.user.username},{week:req.body.week}]}, function(err, c) {
                 if(err)
                 {

                 }
                 else if(c)
                 {
                     Admin_Operation.findOne({$and:[{responsibilty_title:req.body.freeparts},{usertype:req.user.desgn},{action:req.body.freefield}]},(err,found)=>{
                       if(err)
                       {

                       }
                       else if(found)
                       {
                         if(found.responsibilty_percentage === c)
                         {
                           var suc= {
                             suc: "Field,you are going to submit has already reached to it's max!!"
                           };
                           res.send(suc);
                         }
                         else
                         {
                           send()
                         }
                       }
                       else
                       {
                       }
                     })
                 }
                 else
                 {
                   send()
                 }
            });
              }
            })
    }
  })


        function send()
        {
        const newTime = new Timetable({
          date: date,
          month: month,
          year: year,
          username: req.user.username,
          day_order:req.body.day_order,
          order: order,
          freefield: freefield,
          covered: covered,
          freeparts: req.body.freeparts,
          week:week,
          time:req.body.time,
          slot:req.body.slot,
          hour:req.body.hour,
        })
        newTime.save((err, saved) => {
          if(err)
          {

          }
          else if(saved)
          {
            CalculateAdministrative(req,res,req.body.freefield);
          }

        });
        }
})


function CalculateAdministrative(req,res,action)
{
  var month = parseInt(Date.today().toString("M"));
  var year = parseInt(Date.today().toString("yyyy"));
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  if(!req.user)
  {

  }
  else{
          Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:action}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            Timetable.find({username:req.user.username},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                var weekly = c.filter(function(content) {
                  return ((content.week===week) && (content.freefield === action) && (content.month=== month) && (content.year === year));
                });
                var monthly = c.filter(function(content) {
                  return ((content.month=== month) && (content.freefield ===action) && (content.year === year));
                });
                let week_p= (weekly.length / output[0].totalthings) * 100;
                let month_p= ((monthly.length) / ((output[0].totalthings) *4 ))  * 100;
                History.findOne({$and:[{username:req.user.username},{term:action},{action:'GRADE'}]},(er,matched)=>{
                    if(er)
                    {

                    }
                    else if(matched)
                    {
                      console.log('In Matched')
                      if((week === matched.week) && (month === matched.month) && (year === matched.year))
                      {
                        console.log('In Updation')
                        History.updateOne({$and:[{week:week},{month:month},{year:year},
                          {username:req.user.username},{term:action},{action:'GRADE'}]},{$set:{weekly_percentage:week_p,monthly_percentage:month_p}},(prob,done)=>{
                          if(prob)
                          {

                          }
                          else if(done)
                          {
                            res.send('done')
                          }
                        })
                      }
                      else
                      {
                        console.log('In VERIOUS')
                         if(week!==matched.week)
                         {
                           if((month === matched.month) && (year === matched.year))
                           {
                             addDatas(week,matched.month,matched.year,req.user.username,week_p,month_p);
                           }
                           else
                           {
                             addDatas(week,month,matched.year,req.user.username,week_p,month_p)
                           }
                         }
                         else if(month!==matched.month)
                         {
                           if((week === matched.week) && (year === matched.year))
                           {
                             addDatas(matched.week,month,matched.year,req.user.username,week_p,month_p);
                           }
                           else if((week === matched.week) && (year !== matched.year))
                           {
                             addDatas(matched.week,month,year,req.user.username,week_p,month_p);
                           }
                           else
                           {
                             addDatas(week,month,year,req.user.username,week_p,month_p)
                           }
                         }
                         else if(year!==matched.year)
                         {
                           if((month === matched.month) && (week === matched.week))
                           {
                             addDatas(matched.week,matched.month,year,req.user.username,week_p,month_p);
                           }
                           else if((month === matched.month) && (week !== matched.week))
                           {
                             addDatas(week,matched.month,year,req.user.username,week_p,month_p)
                           }
                           else if((month !== matched.month) && (week === matched.week))
                           {
                             addDatas(matched.week,month,year,req.user.username,week_p,month_p)
                           }
                         }
                      }
                    }
                    else
                    {
                      console.log('In NEW')
                      addDatas(week,month,year,req.user.username,week_p,month_p)
                    }
                  })


                  function addDatas(week,month,year,username,week_p,month_p)
                  {
                    const allottime = new History({
                      week:week,
                      month:month,
                      year:year,
                      action:'GRADE',
                      username: req.user.username,
                      weekly_percentage:week_p,
                      monthly_percentage:month_p,
                      term:action,
                    })
                    allottime.save((err, saved) => {
                      if(err)
                      {

                      }
                      else if(saved)
                      {
                          res.send('done');
                      }

                    })
                  }


              }
            })
          }
        })
      }
}



//view_it
router.post('/root_percentage_fetch',(req,res)=>{
  Admin_Operation.find({$and:[{action:req.body.action},{usertype:req.user.desgn}]},(err,obj)=>{
    if(err)
    {

    }
    else if(obj)
    {
      res.send(obj);
    }
  })
})
    router.post('/absent_insert', (req, res) => {
      var date = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      Date.prototype.getWeek = function () {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };

      var today = new Date.today();
      var week =today.getWeek();
      Timetable.findOne({$and:[{username:req.user.username},{date:date},{month:month},{year:year},{freefield:'Academic'}]},(err,data)=>{
        if(err)
        {

        }
        else if(data)
        {
          Timetable.deleteMany({$and:[{username:req.user.username},{date:date},{month:month},{year:year},{freefield:'Academic'}]},(err,deleted)=>{
            if(err)
            {

            }
            else if(deleted)
            {
              var trans = new Absent({
                official_id:req.user.username,
                reason:req.body.absent_reason,
                week:week,
                date:date,
                month:month,
                year:year,
              });
              trans.save((err, savedUser) => {
                if(err)
                {

                }
                else if(savedUser)
                {
                  var differ = false;
                  Academic_Calculate(req,res,differ);
                }
              })
            }
          })
        }
        else
        {
          var trans = new Absent({
            official_id:req.user.username,
            reason:req.body.absent_reason,
            week:week,
            date:date,
            month:month,
            year:year,
          });
          trans.save((err, savedUser) => {
            if(err)
            {

            }
            else if(savedUser)
            {
              res.send('done');
            }
          })
        }
      })

    })

    router.post('/absentStatus', (req, res) => {
      //console.log(req.body)
      Absent.findOne({$and:[{official_id:req.user.username},{date:req.body.date},
        {month:req.body.month},{year:req.body.year}]},(err,found)=>{
        if(err)
        {

        }
        else if(found)
        {
          res.send('ok');
        }
        else{
          res.send('no')
        }
      })
    })



    router.post('/forgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       User.findOne(
         {$and:[{mailid: mailid},{username:req.body.username}]
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'INVALID USER !!'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           // console.log(token);
           User.updateOne({$and:[{mailid: mailid},{username:req.body.username}]},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 1800000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Reset Password:  eWork', // Subject line
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://care.srmist.edu.in/ework/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 30min from now.</p>'
          };
          // console.log("Way to enter.......");
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })
    })


        router.get('/reset_password', function(req, res) {
          User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
            if (!user) {
              // console.log("Expired");
              var expire ={
                expire:"Link is Expired"
              }
              res.send(expire);
            }
            else if(user){
              res.send(user.resetPasswordToken)
            }

          });
        });


/*------------------------Blue Print Send-------------------------------------------------- */
router.post('/send_blue_print', (req, res) => {
  //console.log(req.body)
    const {timing} = req.body
    Alloted_Slot.find({username: req.user.username}, (err1, user) => {
      if(err1)
      {
      //  res.status(500).send('Something broke!')
      }
      else if(user)
      {
        Admin_Operation.findOne({$and:[{action:'Academic'},{responsibilty_title:'Curriculum'},{usertype:req.user.desgn}]},(err2,found)=>{
          if(err2)
          {
            // console.log("No")
          }
          else if(found)
          {
            if(user.length === found.responsibilty_percentage)
            {
              var exceed = {
                exceed: "No of the maximum input reached!!",
              };
                res.send(exceed);
            }
            else{

                      Alloted_Slot.findOne({$and: [{timing: timing},{username: req.user.username}]}, (err, objs) => {
                        if(err)
                        {

                        }
                        else if(objs)
                        {
                          var emsg = {
                            emsg: "We have datas saved on the following Time !!",
                          };
                            res.send(emsg);
                        }
                        else{
                          const allottime = new Alloted_Slot(req.body)
                          allottime.save((err, savedUser) => {
                            var succ= {
                              succ: "Success"
                            };
                            res.send(succ);
                          })
                        }
                      })
            }
          }
        })
      }
    })
  })


  router.post('/fetch_blue_print',function(req,res) {
    if(req.user)
    {
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else {
        username = req.user.username;
      }
    Alloted_Slot.find({$and: [{status:true},{timing: req.body.action},
      {username:username}]}, function(err, docs){
    if(err)
    {

    }
    else if(docs)
    {
      res.send(docs)
    }
  });
}
else{
  res.send('no')
}
  })


  router.post('/del_blue_print',function(req,res) {
    Alloted_Slot.deleteOne({$and: [{serial:  req.body.serial },{username:req.user.username}]},function(err,succ){
      if(err)
      {

      }
      else if(succ)
      {
        res.send('ok');
      }
    });
  })

  router.post('/edit_blue_print', function(req,res) {
    Alloted_Slot.updateOne({$and :[{ serial: req.body.serial},{username: req.user.username}]},req.body, (err, user) => {
      var succ= {
        succ: "Updated"
      };
      res.send(succ);
    })

  });


router.post('/show_editable_blue_print',function(req,res) {
//console.log(req.body)
Alloted_Slot.findOne({$and:[{serial: req.body.id},{username:req.user.username}]}, function(err, docs){
  // console.log(docs)
if(err)
{

}
else if(docs)
{
  res.send(docs)
}
});
})




/*--------------------------------Getting Percentages of Individual /// DashBorad///------------------------------------------------- */

router.post('/know_Complete_Percentage', (req, res) => {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  if(!req.user)
  {

  }
  else{
  Timetable.find({$and:[{freeparts:req.body.res},{username:req.user.username},{week:week}]},(err, c)=> {
    if(err)
    {

    }
    else if(c)
    {
      res.send(c);
    }
  })
}
})


router.get('/knowPercentage', (req, res) => {
  if(!req.user)
  {

  }
  else{
       Admin_Operation.find({usertype:req.user.desgn},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
  }
})


router.get('/knowRootPercentageAc', (req, res) => {
  if(!req.user)
  {

  }
  else{
       Admin_Operation.findOne({$and:[{action:'Academic'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
  }
})
router.get('/knowRootPercentageAd', (req, res) => {
  if(!req.user)
  {

  }
  else{
       Admin_Operation.findOne({$and:[{action:'Administrative'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
  }
})
router.get('/knowRootPercentageRe', (req, res) => {
  if(!req.user)
  {

  }
  else{
       Admin_Operation.findOne({$and:[{action:'Research'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
  }
})

router.get('/fetchAdmins_Actual', (req, res) => {
  if(!req.user)
  {

  }
  else
  {
    History.findOne({$and:[{action:'GRADE'},{term:'Administrative'},{username:req.user.username}]},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        res.send(found);
      }
    })
  }
})

router.get('/fetchRes_Actual', (req, res) => {
  if(!req.user)
  {

  }
  else{
    History.findOne({$and:[{action:'GRADE'},{term:'Research'},{username:req.user.username}]},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        res.send(found);
      }
    })
  }
})
router.get('/fetchAcad_Actual', (req, res) => {
  if(!req.user)
  {

  }
  else{
    History.findOne({$and:[{action:'GRADE'},{term:'Academic'},{username:req.user.username}]},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        res.send(found);
      }
    })
  }
})

router.get('/fetch_other_extra', (req, res) => {
  if(req.user)
  {
  Extra_Handle.find({handled_by:req.user.username},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      Extra.find({faculty_id:req.user.username},(err,count)=>{
        if(err)
        {

        }
        else if(count)
        {
          var data = {
            total:count,
            completed:found
          }
          res.send(data)
        }
      })
    }
  })
}

})


//view_it
router.get('/fetch_absent', (req, res) => {
  if(req.user)
  {
  Absent.find({official_id:req.user.username},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      res.send(found)
    }
  })
}

})
/*----------------------------------------------------------Master Table------------------------------------------ */

router.post('/fetchmaster', function(req, res) {
  if(req.user)
  {
  const {slot_time} = req.body;
    Timetable.findOne({$and: [{day_slot_time : slot_time},{username: req.user.username},{week:req.body.week}]}, function(err, passed){
      res.send(passed);
    })
  }
    else{
      res.send('no');
    }
  });


  /*----------------------------------------------------------View FOR USER---------------------------------------------------*/
  router.post('/authenticate', function(req,res) {
    var data1;
    User.findOne({username:req.body.username},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        if(req.user.h_order === 0)
        {
          data1 ={
            username:found.username,
            allowed:'Allowed',
          }
          res.send(data1)
        }
        else if((req.user.h_order === 1) && !(found.h_order<=req.user.h_order))
        {
          data1 ={
            username:found.username,
            allowed:'Allowed',
          }
          res.send(data1)
        }
        else if((req.user.h_order>1) && (req.user.h_order<=5))
        {
          if(!(found.h_order<=1) || (found.h_order >=6))
          {
            data1 ={
              username:found.username,
              allowed:'Allowed',
            }
            res.send(data1)
          }
          else {
            res.send("Denied")
          }
        }
        else if(req.user.h_order === 6) {
          if((found.campus === req.user.campus) && (found.desgn !== req.user.desgn) && (found.h_order>req.user.h_order) )
          {
            data1 ={
              username:found.username,
              allowed:'Allowed',
            }
            res.send(data1)
          }
          else {
            res.send("Denied")
          }
        }
        else{
          res.send("Denied")
        }
      }
      else
      {
        res.send("User Not Found !!")
      }
    })
  });

  router.get('/validate_list_view', function(req,res) {
    if(req.user)
    {
    User.findOne({username:req.user.username},(err,found)=>{
      if(err)
      {

      }
      else if(found){
        var data_send;
        if(found.h_order === 0)
        {
          data_send ={
            data_send:found.h_order,
            permission:1,
          }
        }
        else if(found.h_order === 1)
        {
          data_send ={
            data_send:found.h_order,
            permission:1,
          }
        }
        else if(found.h_order === 5)
        {
          data_send = {
            data_send:found.h_order,
            permission:1,
          }
        }
        else{
          data_send = {
            permission:0,
          }
        }
        res.send(data_send);
      }
    })
  }
  })

  router.post('/fetch_view_list',function(req,res){
    User.findOne({username:req.user.username},(err,data)=>{
      if(err)
      {

      }
      else if(data)
      {
        Admin_Operation.find({designation_order_no:{ $gt: req.body.start }},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {
            res.send(found);
          }
        })
      }
    })
  })

  router.get('/fetch_department',function(req,res){
    User.findOne({username:req.user.username},(err,data)=>{
      if(err)
      {

      }
      else if(data)
      {
        if(data.h_order === 5)
        {
          res.send([{department_name:data.dept}]);
        }
        else if(data.h_order === 1 || data.h_order === 0)
        {
          Admin_Operation.find({action:'Department'},(err,found)=>{
            if(err)
            {

            }
            else if(found)
            {
              res.send(found);
            }
          })
        }
      }
    })
  })

  router.post('/fetchUser',function(req,res){
    let search;
   if(req.user.h_order === 0)
   {
     search = {$and:[{desgn:req.body.designation},{dept:req.body.department}]};
   }
   else {
     search ={$and:[{desgn:req.body.designation},{dept:req.body.department},{campus:req.user.campus}]};
   }

    if(req.body){
    User.find(search,(err,data)=>{
      if(err)
      {

      }
      else if(data)
      {
        res.send(data);
      }
    })
  }
  })

    router.post('/viewSection',function(req,res){
                Section.find({$and:[{action:req.body.section_ref},{username:req.body.username}]},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })


/*------------------------------------------------View For OwnData---------------------------*/
    router.post('/fetch_five_for_view',function(req,res){
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }
                Five.find({username:username},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })
    router.post('/fetch_adminis_for_view',function(req,res){
    //console.log(req.body)
      let query;
      if(req.user)
      {
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }
      if(req.body.action === 'all')
      {
        query =[{username:username},{freefield:'Administrative'}];
      }
      else if(req.body.datas.setday2)
      {
        let day1 = parseInt(req.body.datas.setday);
        let day2 = parseInt(req.body.datas.setday2);
        let month1 = parseInt(req.body.datas.setmonth);
        let month2 = parseInt(req.body.datas.setmonth2);
        let year1 = parseInt(req.body.datas.setyear);
        let year2 = parseInt(req.body.datas.setyear2);
        //console.log(day1,day2,month2,month1,year1,year2)
        query =[{username:username},{date:{$gte:day1,$lte:day2}},
          {month:{$gte:month1,$lte:month2}},
          {year:{$gte:year1,$lte:year2}},{freefield:'Administrative'}];
      }
       else if(req.body.datas.setday)
       {
         query =[{username:username},{date:req.body.datas.setday},{month:req.body.datas.setmonth}
           ,{year:req.body.datas.setyear},{freefield:'Administrative'}];
       }
       else
       {
         query =[{username:username},{month:req.body.datas.setmonth}
           ,{year:req.body.datas.setyear},{freefield:'Administrative'}];
       }

                Timetable.find({$and:query},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
              }
    })
    router.post('/fetch_research_for_view',function(req,res){
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }
      let query;
      if(req.body.action === 'all')
      {
        query =[{username:username},{freefield:'Research'}];
      }
      else if(req.body.datas.setday2)
      {
        let day1 = parseInt(req.body.datas.setday);
        let day2 = parseInt(req.body.datas.setday2);
        let month1 = parseInt(req.body.datas.setmonth);
        let month2 = parseInt(req.body.datas.setmonth2);
        let year1 = parseInt(req.body.datas.setyear);
        let year2 = parseInt(req.body.datas.setyear2);
        //console.log(day1,day2,month2,month1,year1,year2)
        query =[{username:username},{date:{$gte:day1,$lte:day2}},
          {month:{$gte:month1,$lte:month2}},
          {year:{$gte:year1,$lte:year2}},{freefield:'Research'}];
      }
      else if(req.body.datas.setday)
      {
        query =[{username:username},{date:req.body.datas.setday},{month:req.body.datas.setmonth}
          ,{year:req.body.datas.setyear},{freefield:'Research'}];
      }
      else
      {
        query =[{username:username},{month:req.body.datas.setmonth}
          ,{year:req.body.datas.setyear},{freefield:'Research'}];
      }

                Timetable.find({$and:query},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })
    router.post('/fetch_academic_for_view',function(req,res){
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }

      let query;
      if(req.body.action === 'all')
      {
        query =[{username:username},{freefield:'Academic'}];
      }
      else if(req.body.datas.setday2)
      {
        let day1 = parseInt(req.body.datas.setday);
        let day2 = parseInt(req.body.datas.setday2);
        let month1 = parseInt(req.body.datas.setmonth);
        let month2 = parseInt(req.body.datas.setmonth2);
        let year1 = parseInt(req.body.datas.setyear);
        let year2 = parseInt(req.body.datas.setyear2);
        //console.log(day1,day2,month2,month1,year1,year2)
        query =[{username:username},{date:{$gte:day1,$lte:day2}},
          {month:{$gte:month1,$lte:month2}},
          {year:{$gte:year1,$lte:year2}},{freefield:'Academic'}];
      }
      else if(req.body.datas.setday)
      {
        query =[{username:username},{date:req.body.datas.setday},{month:req.body.datas.setmonth}
          ,{year:req.body.datas.setyear},{freefield:'Academic'}];
      }
      else
      {
        query =[{username:username},{month:req.body.datas.setmonth}
          ,{year:req.body.datas.setyear},{freefield:'Academic'}];
      }

                Timetable.find({$and:query},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })




  /*-------------------End of View-----------*/


  /*--------------------------------------------EXTRA CODE---------------------------*/

  router.post('/check_possibility',function(req,res){
    //console.log(req.body)
    User.findOne({$and:[{username:req.body.id},{dept:req.user.dept},
      {campus:req.user.campus}]},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        if(found.h_order<=req.user.h_order)
        {
            Alloted_Slot.findOne({$and:[
            {timing:req.body.props_content.day_order+'.'+req.body.props_content.content.number},
            {year:req.body.props_content.datas_a.year},{sem:req.body.props_content.datas_a.sem},
            {batch:req.body.props_content.datas_a.batch},{subject_code:req.body.props_content.datas_a.subject_code},
            {username:req.body.id},{alloted_slots:req.body.props_content.slot}]},(err,avail)=>{
              if(err)
              {

              }
              else if(avail)
              {
                res.send('no');
              }
              else{
                Timetable.findOne({$and:[{username:req.body.id},{week:req.body.week},
                  {day_order:req.body.props_content.day_order},{hour:req.body.props_content.content.number},
                  {date:req.body.props_content.date},{month:req.body.props_content.month},
                  {year:req.body.props_content.year}]},(err,ok)=>{
                  if(err)
                  {

                  }
                  else if(ok)
                  {
                    console.log('Step 3 Failed')
                    res.send('no');
                  }
                  else{
                    console.log('Step 3 Passed')
                      Alloted_Slot.findOne({$and:[
                      {timing:req.body.props_content.day_order+'.'+req.body.props_content.content.number},
                      {year:req.body.props_content.datas_a.year},{sem:req.body.props_content.datas_a.sem},
                      {batch:req.body.props_content.datas_a.batch},{subject_code:req.body.props_content.datas_a.subject_code},
                      {username:req.user.username},{alloted_slots:req.body.props_content.slot}]},(err,having)=>{
                        if(err)
                        {

                        }
                        else if(having)
                        {
                          console.log('Step 4 Passed')
                          res.send(having.alloted_slots);
                        }
                        else
                        {
                          console.log('Step 4 Failed')
                          res.send('no');
                        }
                      })
                  }
                })
              }
            })
       }
       else{
         res.send('no')
       }
      }
      else{
        console.log('In user')
        res.send('no');
      }
    })
  })

  router.post('/giving_permission', function(req,res)
  {
    let permission;
    var date = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    if(req.body.permission === 'accept')
    {
      permission = 'accepted';
    }
    else
    {
      permission = 'denied';
    }
    if(!(req.body.content.date === date) || !(req.body.content.month === month) ||!(req.body.content.year === year))
    {
      console.log('Invalid')
      res.send('Invalid');
    }
    else
    {
      Timetable.findOne({$and:[{username:req.user.username},{date:req.body.content.date},{month:req.body.content.month},
      {year:req.body.content.year},{hour:req.body.content.hour},{day_order:req.body.content.day_order},]},(err,done)=>{
        if(err)
        {

        }
      else if(done)
      {
        res.send('Invalid')
      }
      else
      {
        Extra.updateOne({serial:req.body.content.serial},{accepted:permission},(error2,doneOp)=>{
          if(error2)
          {

          }
          else if(doneOp)
          {
            let details;
            if(permission ==='denied')
            {
              Timetable.deleteOne({$and:[{username:req.body.content.order_from},{date:req.body.content.date},
                {month:req.body.content.month},{year:req.body.content.year},{hour:req.body.content.hour},
                {day_order:req.body.content.day_order},]},(err1,done1)=>{
                  if(err1)
                  {

                  }
                  else if (done1) {
                    details =
                    req.user.username+' has denied to handle your requested slot -'+req.body.content.slot+' for '
                    +req.body.content.hour+' hour. No worries !! Still you have a chance to fill the data for this slot.'+
                    ' But if it is beyond 48 hour of actual date for this particular slot, then it is not possible to handle.'+
                    ' In this case, you can make a request to admin to re-render the slot in manage section.';
                    notification_send(req.body.content.order_from,'Requested Hour Handle Denied',details,res,'')
                  }
              })
            }
            else
            {
              details =
              req.user.username+' has accepted your request to handle slot -'+req.body.content.slot+' for '
              +req.body.content.hour+' hour';
              notification_send(req.body.content.order_from,'Requested Hour Handle Accepted',details,res,'');
            }

          }
        })
     }
     })
    }
})



function saveExtra(body,action,req,res)
{
  // console.log('Inside Function');
  // console.log(action)
  // console.log(body)
      var trans = new Extra({
         action:action,
         faculty_id:body.compense_faculty,
         order_from:body.username,
         hour:body.hour,
         for_sem:body.for_sem,
         for_batch:body.for_batch,
         for_year:body.for_year,
         week:body.week,
         day_order:body.day_order,
         date:body.date,
         month:body.month,
         year:body.year,
         slot:body.slot,
         subject_code:req.body.subject_code,
         problem_compensation_date:body.problem_compensation_date,
         problem_compensation_hour:body.problem_compensation_hour,
         problem_compensation_day_order:body.problem_compensation_day_order,
         accepted:'pending',
      });
      trans.save((err, savedUser) => {
        if(err)
        {
          console.log('Error')
         return;
        }
        else if(savedUser)
        {
          //console.log('saved');
          if(action === 'Foreign Extra Handle')
          {
            let details = body.username+' has requested to handle the hour- '+body.hour+'.Kindly go to extra slot handle to know more.'
            notification_send(body.compense_faculty,'Request for '+action,details,res,'');
          }
          else {
            res.send('done');
          }

        }
      })
}


//view_it
router.post('/add_handover_slots',function(req,res){
  var day = parseInt(Date.today().toString("dd"));
  var month = parseInt(Date.today().toString("M"));
  var year = parseInt(Date.today().toString("yyyy"));
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();
console.log(req.body)
    Extra.findOne({$and: [{action:req.body.action},{faculty_id:req.body.faculty_id},
      {order_from: req.user.username},{date:day},{month:month},{year:year},
     {for_sem:parseInt(req.body.sem)},{for_year:parseInt(req.body.year)},{for_batch:parseInt(req.body.batch)},
   {slot:req.body.slot},{hour:parseInt(req.body.hour)},{day_order:req.body.day_order}]}, function(err, docs){
      if(err)
      {
        console.log('No')
      }
      else if(docs)
      {
        console.log('Find')
        res.send('no')
      }
      else
      {
        console.log('Going to insert')
        var tran = new Notification({
          for:req.body.faculty_id,
          subject:'Slot Handle Request from Other Faculty',
          permission:true,
          academic_hour:parseInt(req.body.hour),
          absent_handle:true,
          details:req.user.username+' has requested you to handle his/her alloted slot.Kindly give the permission if you want to handle the slot or else deny the request.'
        });
        tran.save();
        saveExtra(req.body.faculty_id,req.user.username,parseInt(req.body.hour),
        parseInt(req.body.sem),parseInt(req.body.batch),parseInt(req.body.year),week,
        req.body.day_order,day,month,year,req.body.slot,
        res,false,'Foreign HandOver Slot');
      }
  });
})


  router.post('/fetch_handover_slots',function(req,res) {
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
      Extra.find({$and: [{action:req.body.action},
        {order_from: req.user.username},{date:day},{month:month},{year:year}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
        else
        {
          return;
        }
    });
  })


  router.post('/del_handover_slots',function(req,res) {
    Extra.deleteOne({$and: [{serial:  req.body.serial },{order_from: req.user.username}]},function(err,succ){
      res.send("OK")
    });
  });

    router.post('/fetchCovered',function(req,res) {
        Extra_Handle.find({$and: [{handled_by:req.body.faculty_id},{completed_for: req.user.username},{slot:req.body.slot}]}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
      });
    })



/*-----------------------------------Fetch OWN EXTRA--------------------------------------------*/

router.get('/fetch_own_extra',function(req,res) {
  if(req.user)
  {
  var date = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
    Extra.find({$and: [{action:'Own Extra Handle'},{accepted:'pending'},{faculty_id:req.body.username},{problem_compensation_date:date.substring(1)},{month:month},
    {year:year}]}, function(err, docs){
      if(err)
      {

      }
      else if(docs)
      {
        res.send(docs)
      }
  });
}
})
/*----fetch foreign----*/

  router.post('/fetch_foreign_slots',function(req,res) {
    var date = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
      Extra.find({$and: [{action:'Foreign Extra Handle'},{faculty_id: req.user.username},
      {date:date},{month:month},{year:year}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
    });
  })

  router.post('/completed_foreign_hour',function(req,res) {
    //console.log(req.body)
    var date = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    Date.prototype.getWeek = function ()
    {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };
    if(!(date===req.body.content.date)&&!(month===req.body.content.month)&&!(year===req.body.content.year))
    {
      res.send('no')
    }
    else
    {
      const newTime = new Timetable({
        action:'Handled Foreign Hour',
        username:req.user.username,
        freefield:'Academic',
        freeparts:'Curriculum',
        date:req.body.content.date,
        month:req.body.content.month,
        year:req.body.content.year,
        day_order:req.body.content.day_order,
        hour:req.body.content.hour,
        covered:req.body.covered,
        for_sem:req.body.content.for_sem,
        for_batch:req.body.content.for_batch,
        for_year:req.body.content.for_year,
        slot:req.body.content.slot,
        subject_code:req.body.content.subject_code,
        verified:false,
        positive_count:0,
        negative_count:0,
      })
      newTime.save((err, savedUser) => {
        if(err)
        {

        }
        else if(savedUser)
        {
          Timetable.updateOne({$and:[{username:req.body.content.order_from},
                  {freefield:'Academic'},{date:req.body.content.date},{month:req.body.content.month},
                  {year:req.body.content.year},{day_order:req.body.content.day_order},{hour:req.body.content.hour},{for_sem:req.body.content.for_sem},
                  {for_batch:req.body.content.for_batch},{for_year:req.body.content.for_year},{slot:req.body.content.slot}]},
                  {compense_faculty_topic:req.body.covered},(error3,doneUpdation)=>{
                    if(error3)
                    {

                    }
                    else if(doneUpdation)
                    {
                      Extra.updateOne({$and:[{faculty_id:req.user.username},{date:req.body.content.date},
                      {month:req.body.content.month},{year:req.body.content.year},
                      {day_order:req.body.content.day_order},{hour:req.body.content.hour},
                      {for_sem:req.body.content.for_sem},{for_batch:req.body.content.for_batch},
                      {for_year:req.body.content.for_year},{slot:req.body.content.slot}]},{accepted:'completed'},(error4,done3)=>{
                        if(error4)
                        {

                        }
                        else if(done3)
                        {
                          setTimeout(function()
                          {
                            validate(req,res,req.body.content);
                          }, 172800040);

                          var differ = false;
                          Academic_Calculate(req,res,differ);
                        }
                      })
                    }
          })
        }
      })
    }
})


  /*-------End EXTRA---*/

/*--------------------------------------------CRUD CODE--------------------------------------------- */
router.post('/fetch_section',function(req,res){
            Admin_Operation.find({action:'Section'},(err,found)=>{
              if(err)
              {

              }
              else if(found)
              {
                res.send(found);
              }
            })
})
  router.post('/addmm', function(req,res) {
            var trans = new Section(req.body.data);
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
  });

    router.post('/editt', function(req,res) {
      Section.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
        var succ= {
          succ: "Successful SignedUP"
        };
        res.send(succ);
      })

    });


router.post('/fetchall',function(req,res) {
  if(req.user)
  {
  console.log(req.body.action)
    Section.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
  });
}
})
router.post('/fetch_section_for_view',function(req,res) {
  if(req.user)
  {
  let username;
  if(req.body.username)
  {
    username= req.body.username;
  }
  else{
    username = req.user.username;
  }
  console.log(req.body.username)
    Section.find({username: username}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
  });
}
})

router.post('/fetcheditdata',function(req,res) {
  Section.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/del',function(req,res) {
  Section.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
});
/*------------------------------------------------Code For Manage Page----------------------------------- */
router.post('/request_for_entry',function(req,res) {
  //console.log(req.body)
  DayOrder_History.findOne({$and: [{day:req.body.req_date},{month: req.body.req_month},
    {year: req.body.req_year},{day_order:req.body.day_order},{day_order_status: 'online'}]}, function(err, present){
    if(err)
    {
      console.log(err)
    }
    else if(present)
    {
      Request.findOne({$and: [{username:req.user.username},{date:req.body.req_date},
        {month: req.body.req_month},{year: req.body.req_year}]}, function(err, passed){
        if(err)
        {
          console.log(err)
        }
        else if(passed)
        {
          var handled;
          if(passed.action === "approved")
          {
          handled= {
            handled: "Request Already Approved !!"
          };
          res.send(handled);
          }
          else
          {
            handled= {
              handled: "Admin Has Not Approved Till Now!!"
            };
            res.send(handled);
          }
        }
        else{
          if(req.body.hour)
          {
            Timetable.findOne({$and:[{username:req.user.username},
              {date:req.body.req_date},{month: req.body.req_month},
              {year: req.body.req_year},{day_order:req.body.day_order},{hour:req.body.req_hour}]},(er,have_data)=>{
              if(er)
              {

              }
              else if(have_data)
              {
                res.send('Invalid')
              }
              else
              {
                sendRequests()
              }
            })
          }
          else
          {
            sendRequests();
           }
           function sendRequests()
           {
             var requests = new Request(req.body);
             requests.save((err2, saved) => {
               if(err2)
               {

               }
               else if (saved) {
                 var succ= {
                   succ: "We have sent your request!!"
                 };
                 res.send(succ);
               }

             })
           }
        }
      })
    }
    else
    {
      var noday=
      {
        noday:"Invalid Request!!"
      }
      res.send(noday)
    }
  })
})


router.post('/entry_of_blueprint',function(req,res) {
  Request.findOne({$and:[{action:'For Blue Print Render'},
  {username: req.user.username},{expired:false}]}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
    else if(docs)
    {
      Request.findOne({$and:[{action:'For Blue Print Render'},
      {username: req.user.username},{expired:false}]}, function(err1, docs1){
        if(err1)
        {

        }
        else if (docs1) {
          res.send('We have sent your request!!');
        }
      })
    }
  else
  {
    var requests = new Request(req.body);
    requests.save((err, savedUser) => {
      if(err)
      {

      }
      else if(savedUser)
      {
      res.send('We have sent your request!!');
      }
    })
  }
 });
})



router.get('/fetch_requested_list',function(req,res) {
  Request.find({$and:[{username: req.user.username},{expired:false}]}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
  else if(docs)
  {
    res.send(docs);
  }
 });
})



router.post('/request_Expiry_Check',function(req,res) {
  if(req.body.serial)
  {
    Request.updateOne({$and:[{username:req.user.username},{serial:req.body.serial}]},{$set:{expired:true}}, function(err, done){
      if(err)
      {

      }
      else if(done)
      {
        res.send('ok')
      }
      else{
        res.send('no')
      }
    })
  }
  else
  {
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    Request.updateMany({$and:[{expired_date:day},{expired_month:month},{expired_year:year}]},
      {$set:{expired:true}}, function(err, done){
      if(err)
      {

      }
      else if(done)
      {
        //console.log('Here')
        res.send('ok')
      }
      else{
        res.send('no')
      }
    })
  }
})


router.post('/fetch_requested_status',function(req,res) {
  Request.findOne({$and:[{username: req.user.username},{serial:req.body.serial}]}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
  else if(docs)
  {
    if(docs.action ==="approved")
    {
      res.send("approved")
    }
    else if(docs.action ==="denied")
    {
      res.send("denied")
    }
    else if(docs.action ==="false")
    {
      res.send("pending")
    }
  }
  else
  {

  }
});
})


/*----------------------------------------Five Year Plan--------------------------------------------- */


function knowstat(user,body)
{
  var year = Date.today().toString("yyyy");
  var year_int = parseInt(year);
  Five.find({$and: [{username:user},{expired:false},{expire_year:year_int}]},function(err,docs){
    if(docs)
    {
      Five.updateMany({$and :[{username:user},{expire_year: year_int}]},{expired: true},function(err,done){
        if(done)
        {
        console.log("done")
        }
        return;
      })
    }
  })
}

router.post('/update_status_fiveyear', function(req,res) {
  Five.updateOne({$and :[{ serial: req.body.key},{username: req.user.username}]},{completed:req.body.status}, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
  })

});

router.post('/fetch_update_status_fiveyear', function(req,res) {
  Five.findOne({$and :[{ serial: req.body.key},{username: req.user.username}]},{completed:req.body.status}, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
  })

});
router.post('/fetch_complete_fiveyear', function(req,res) {
  Five.findOne({$and :[{ serial: req.body.serial},{username: req.user.username}]}, (err, user) => {
    if(err)
    {

    }
    else if(user)
    {
        res.send(user.completed);
    }
  })

});
    router.post('/add_five_year_plan', function(req,res) {
              var fives = new Five(req.body);
              fives.save((err, savedUser) => {
                console.log("save")
                res.send("Saved");
              })
    });




          router.post('/edit_five_year_plan', function(req,res) {
            Five.updateOne({$and :[{ serial: req.body.serial},{username: req.user.username}]},req.body, (err, user) => {
              var succ= {
                succ: "Updated"
              };
              res.send(succ);
            })

          });


      router.post('/fetch_five_year_plan',function(req,res) {
        if(req.user)
        {
        knowstat(req.user.username,req.body);
        Five.find({$and: [{action: req.body.action},{username: req.user.username},{expired:false}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
      });
    }
    else{
      res.send('no')
    }
      })


      router.post('/fetch_five_year_existing_data',function(req,res) {
        console.log(req.body)
        Five.findOne({$and:[{serial: req.body.id},{username:req.user.username}]}, function(err, docs){
          // console.log(docs)
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
      });
      })

      router.post('/del_five_year_plan',function(req,res) {
        Five.remove({$and: [{serial:  req.body.serial },{username:req.user.username}]},function(err,succ){
          res.send("OK")
        });
      })

      /*-------------------------------------Declare a Holiday---------------------------------------------- */

      router.post('/declare_cancel_or_holiday',function(req,res) {
        var next_dayorder;
        //console.log(req.body)
        Common_DayOrder.findOne({$and: [{day:req.body.cday},{month: req.body.cmonth},{year:req.body.cyear},{cancel_status: false}]}, function(err, result){
           if(err)
           {
             return;
           }
           else if(result)
           {
             if(result.day_order === 1)
             {
               next_dayorder = 5;
             }
             else
             {
               next_dayorder = result.day_order - 1;
             }
                Common_DayOrder.updateOne({day_order: result.day_order},{ $set: { day_order: next_dayorder,cancel_status: true}} ,(err, newdata) => {
                })
                DayOrder_History.updateOne({day_order: result.day_order},{ $set: { day_order: next_dayorder,cancel_status: true}} ,(err, newdata) => {
                })

            res.send("Updated!!")
           }
           else if(!result)
           {
             res.send("It's Already Updated!!")
           }
        })

      })



      /*----------------------------------------------------------Start of Department-Admin Operation----------------------------- */
      router.get('/fetch_request', (req, res) => {
           User.find({$and:[{active:false},{campus:req.user.campus},{dept:req.user.dept}]}, (err, user) => {
             if(err)
             {

             }
             else if(user)
             {
               res.send(user);
             }
           })
        })

        router.get('/fetch_signup_request_for_admin', (req, res) => {
             User.find({active:false}, (err, user) => {
               if(err)
               {

               }
               else if(user)
               {
                 res.send(user);
               }
             })
          })

          router.post('/fetch_dept_users', (req, res) => {
            //console.log(req.body.d_admin)
               User.find({$and:[{dept:req.body.d_admin.dept},{campus:req.body.d_admin.campus}
               ,{activated_by:req.body.d_admin.username}]}, (err, user) => {
                 if(err)
                 {

                 }
                 else if(user)
                 {
                   res.send(user);
                 }
               })
            })

          router.post('/fetch_req_on_blueprint_render', (req, res) => {
              Request.find({$and:[{expired:false},
                {action:'For Blue Print Render'}]},(err,found)=>{
                if(err)
                {

                }
                else {
                  res.send(found);
                }
              })
            })

            router.post('/faculty_adviser_status_modify', (req, res) => {
                User.updateOne({username:req.body.username},
                  {$set:{faculty_adviser:req.body.updated,faculty_adviser_year:req.body.year}},(err,found)=>{
                  if(err)
                  {

                  }
                  else {
                    res.send(found);
                  }
                })
              })

        router.post('/active_user', (req, res) => {

           if(req.body.username)
           {
             User.updateOne({username:req.body.username},{$set:{active:true}},(err,done)=>{
               if(err)
               {

               }
               else if(done)
               {
                 let content = 'This mail is regarding the activation of your account. This is to inform you that your account has been activated by our CARE team. You can now proceed with login.'
                 mail_send(req.body.mailid,content,res,req.body.name);
               }
             })
           }
           else {
             User.updateOne({username:req.body.content.username},
               {$set:{faculty_adviser:req.body.advisor_on,active:true,faculty_adviser_year:req.body.for_year,
                 activated_by:req.user.username}}, (err, user) => {
               if(err)
               {

               }
               else if(user)
               {
                 let content = 'This mail is regarding the activation of your account. This is to inform you that your account has been activated by our CARE team. You can now proceed with login.'
                 mail_send(req.body.content.mailid,content,res,req.body.content.name);
               }
             })
           }

          })

router.post('/allow_blueprint_req', (req, res) => {
   Request.updateOne({$and:[{username:req.body.content.username},{status:'pending'},{expired:false},
   {action:'For Blue Print Render'}]},{$set:{status:'accepted'}},(err,done)=>{
     if(err)
     {

     }
     else if (done) {
       User.updateOne({username:req.body.content.username},{$set:{render_timetable:true}},(err1,done1)=>{
         if(err1)
         {

         }
         else if (done1)
         {
           setTimeout(function()
           {
             expireIt(req);
           }, 172800040);
           res.send('done')
         }
         else {
          res.send('no')
         }
       })
     }
     else {
       res.send('no')
     }
   })
})

function expireIt(req)
{
  Request.updateOne({$and:[{username:req.body.content.username},{status:'pending'},{expired:false},
  {action:'For Blue Print Render'}]},{$set:{expired:true}},(err,done)=>{
    if(err)
    {

    }
    else if (done) {

    }
  })
}





      /*----------------------------------------------End of Department-Admin--------------------------------------------------------*/


/*----------------------------------------------------------Start of Admin Operation----------------------------- */

/*----------------------------------------------Admin Operation--------------------------------------------------------*/
router.post('/auth', (req, res) => {
  const {username,password} = req.body;

   if(req.body.id === "ework_2019")
   {
     User.findOne({username:username}, (err, user) => {
       if(user)
       {
         var existadmin ={
           existadmin:'You already have an account !!',
         }
         res.send(existadmin);
       }
       else
       {
         const newadmin = new User({
           active:true,
           suspension_status:false,
           username:username,
           password: password,
           count: 4,
           mailid:req.body.mailid,
           resetPasswordExpires:'',
           resetPasswordToken:'',
           h_order:0,
         })
         newadmin.save((err, savedUser) => {
           var succ= {
             succ: "Successful SignedUP"
           };
           res.send(succ);
         })
       }
     })
  }
  else
  {
    var wrong ={
      wrong:'Passcode is Wrong !!'
    }
    res.send(wrong)
  }
  })

  router.post('/insert_dept_admin', (req, res) => {
       User.findOne({$and:[{username:req.body.data.username},{h_order:0.5}]}, (err, user) => {
         if(err)
         {

         }
         else if(user)
         {
           res.send('no');
         }
         else
         {
           const newadmin = new User(req.body.data)
           newadmin.save((err, savedUser) => {
             if(err)
             {

             }
             else if(savedUser)
             {
             mailToDeptAdmin(req.body.data.mailid,res,req.body.data,req.body.data.username,req.user.username)
               }
             })
         }
       })
    })


    function mailToDeptAdmin(mailid,res,name)
    {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
           var string = '039@#ABCDEF12GH456IJKLM$%NOPQRST78UVWXYZ';
           let OTP = '';
           var len = string.length;
           for (let i = 0; i < 6; i++ ) {
               OTP += string[Math.floor(Math.random() * len)];
           }
           const token = OTP;
           User.updateOne({$and:[{mailid:mailid},{h_order:0.5}]},{ $set: {deptToken:token}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Team eWork : Department Admin Chosen', // Subject line
            html: '<p>Dear '+name+',</p><br /><p>You are receiving this message from SRM CARE,eWork team.You have been chosen as a department admin for eWork. In order to validate yourself kindlly navigate '+'<a href="http://care.srmist.edu.in/ework/department_admin"><b>Here<b></a> </p><br /><p>Please use this code <b>'+token+'</b> at the time of signup.</p><br />Thank You..'
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else
            {
              res.send('ok');
            }
         });

    }

    router.post('/assign_suprimity', (req, res) => {
         User.findOne({$and:[{username:req.body.data.username}]}, (err, user) => {
           if(err)
           {

           }
           else if(user)
           {
             if(user.suprimity === false)
             {
               mailsend(user.mailid,res,req.body.data,user.name,req.user.username)
             }
             else
             {
               res.send('have')
             }
           }
           else
           {
             res.send('no')
           }
         })
      })

    function mailsend(mailid,res,data,name,user)
    {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Team eWork : REGARDING SUPRIMITY', // Subject line
            html: '<p>Dear '+name+',</p><br /><p>We are glad to inform you that eWork Team of SRM CARE have assign you some task as our supreme user.Your task is to '+data.task+'. In order to find more kindlt navigate '+'<a href="http://care.srmist.edu.in/ework/supreme_admin"><b>Here<b></a> </p><br /><br />Thank You..'
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else if(info)
            {
                  User.updateOne({$and:[{username:data.username}]},{ $set: {suprimity:true,task:data.task,inserted_by:user}} ,(err, updated) => {
                    if(err)
                    {

                    }
                    else if(updated)
                    {
                      res.send('ok');
                    }
                  })
            }
            else
            {
              res.send('no');
            }
         });

    }

    router.post('/response_from_deptadmin', (req, res) => {
      let password = bcrypt.hashSync(req.body.password, 10);
         User.findOne({$and:[{username:req.body.username},{deptToken:req.body.id},{h_order:0.5}]}, (err, user) => {
           if(err)
           {

           }
           else if(user)
           {
             User.updateOne({$and:[{username:req.body.username},{deptToken:req.body.id}]},{$set :{password:password,deptToken:'',activated:Date.now(),suspension_status:false,active:true}}, (err, done) => {
               if(err)
               {

               }
               else if(done)
               {
                 res.send('ok');
               }
             })
           }
           else if(!user)
           {
             res.send('no');
           }
         })
      })

      router.post('/fetch_designation', function(req,res) {
        //console.log('Here')
        Admin_Operation.find({ },(err,data)=>{
          if(err)
          {

          }
          else if(data)
          {
            res.send(data);
          }
        })
      });


    router.post('/addnav', function(req,res) {
      console.log(req.body)
              var trans = new Nav(req.body.data);
              trans.save((err, savedUser) => {
                var succ= {
                  succ: "Done"
                };
                res.send(succ);
              })
    });


      router.post('/editnav', function(req,res) {
        Nav.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
          var succ= {
            succ: "Updated"
          };
          res.send(succ);
        })

      });


  router.post('/fetchnav',function(req,res) {
    Nav.find({action: req.body.action}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })
  router.post('/edit_existing_nav',function(req,res) {
    Nav.findOne({serial: req.body.id}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

  router.post('/delnav',function(req,res) {
    Nav.remove({serial:  req.body.serial },function(err,succ){
      res.send("OK")
    });
  })

  /*---------------------------------------CRUD FOR ADDING Responsibility CODE______________________________________________________________ */
  router.post('/add_from_insert_in_admin', function(req,res) {
        var respon = new Admin_Operation(req.body.data);
        respon.save((err, savedUser) => {
          var succ= {
            succ: "Done"
          };
          res.send(succ);
        })
});


router.post('/edit_inserted_data', function(req,res) {
Admin_Operation.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
var succ= {
  succ: "Updated"
};
res.send(succ);
})

});


router.post('/fetch_in_admin',function(req,res) {

  Admin_Operation.find({$and:[{action: req.body.action},{usertype:req.body.usertype}]}, function(err, docs){
  if(err)
  {

  }
  else
  {

  res.send(docs)
  }
  });
})


router.post('/fetch_for_edit',function(req,res) {
Admin_Operation.findOne({serial: req.body.id}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/del_inserted_data',function(req,res) {
Admin_Operation.remove({serial:  req.body.serial },function(err,succ){
res.send("OK")
});
})


  router.get('/getrequest',function(req,res) {
    Request.find({ }, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

    router.get('/fetch_denied_list',function(req,res) {
      Request.find({action: "denied"}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
    });
    })


  router.post('/approve_request',function(req,res) {
    //console.log(req.body)
    if(req.body.req_hour)
    {
            Timetable.findOne({$and:[{hour:req.body.req_hour},
              {username:req.body.username},{date:req.body.req_date},{month: req.body.req_month},
              {year: req.body.req_year}]}, function(err, docs){
            if(err)
            {

            }
            else if(docs)
            {

              res.send("yes")
            }
            else
            {
              res.send("no")
            }
          });
     }
     else
     {
           Timetable.findOne({$and:[{username:req.body.username},
             {date:req.body.req_date},{month: req.body.req_month},{year: req.body.req_year}]}, function(err, docs){
           if(err)
           {

           }
           else if(docs)
           {

             res.send("yes")
           }
           else
           {
             res.send("no")
           }
         });
     }
  })



    router.post('/denyrequest',function(req,res) {
      Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},
          {serial: req.body.serial}]},{$set:{action: "denied",denying_reason:req.body.denying_reason}},function(err,done){
          if(err)
          {

          }
          else if(done)
          {
              res.send("Done")
          }
        })
      }
    });
    })



    router.post('/approverequest',function(req,res) {

    Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},
          {$set:{action: "approved",expired_date:req.body.expired_date,expired_month:req.body.expired_month,expired_year:req.body.expired_year}},function(err,done){
          if(err)
          {

          }
          else if(done){
           res.send("Done")
          }
        })
      }
    });
    })


    router.post('/handleFaculty_Registration',function(req,res) {

    AdminOrder.findOne({$and:[{usertype: req.body.history},{order:'SIGNUP STATUS'}]}, function(err, docs){
      if(err)
      {

      }
      else if(docs)
      {
        AdminOrder.updateOne({$and:[{usertype: req.body.history},{order:'SIGNUP STATUS'}]},{status: req.body.checked},function(err,done){
          res.send("Done")
        })
      }
      else{
        var trans = new AdminOrder({
          order:'SIGNUP STATUS',
          usertype:req.body.history,
          status: req.body.checked
        }
        );
        trans.save((err, saved) => {
           if(err)
           {

           }
           else if(saved)
           {
                       res.send('done');
           }
        })
      }
    })
    })

    router.post('/handleFaculty_BluePrint',function(req,res) {

    AdminOrder.findOne({$and:[{usertype: req.body.history},{order:'BLUE PRINT STATUS'}]}, function(err, docs){
      if(err)
      {

      }
      else if(docs)
      {
        AdminOrder.updateOne({$and:[{usertype: req.body.history},{order:'BLUE PRINT STATUS'}]},{status: req.body.checked},function(err1,done){
          if(err1)
          {

          }
          else if(done)
          {
            User.updateMany({render_timetable: {"$exists" : true}},{render_timetable:req.body.checked},(error2,updated2)=>{
              if(error2)
              {

              }
              else{
                 res.send('done');
              }
            })
          }
          else {
            res.send('done')
          }
        })
      }
      else
      {
          var trans = new AdminOrder({
            order:'BLUE PRINT STATUS',
            usertype:req.body.history,
            status: req.body.checked
          }
          );
          trans.save((err, saved) => {
             if(err)
             {

             }
             else if(saved)
             {
               User.updateMany({render_timetable: {"$exists" : true}},{render_timetable:req.body.checked},(error1,updated)=>{
                 if(error1)
                 {

                 }
                 else{
                    res.send('done');
                 }
               })
             }
             else {
               res.send('done');
             }
          })
      }
    })
    })



    router.post('/fac_status', (req, res, next) => {
      AdminOrder.find({ },function(err,result){
        if(err)
        {

        }
        else if(result)
        {
          if(req.body.userEnable ==='verify')
          {
            if(result.length>0)
            {
              console.log('Here')
            User.findOne({username:req.user.username},(error,ok)=>{
              if(error)
              {

              }
              else if(ok)
              {
                var fac_blueprint_status = result.filter(item => ((item.usertype==="fac") && (item.order==="BLUE PRINT STATUS")));
                console.log(fac_blueprint_status[0].status)
                console.log(ok.render_timetable)
                if(fac_blueprint_status[0].status === true && ok.render_timetable === false)
                {
                  res.send(result)
                }
                else if(fac_blueprint_status[0].status === true && ok.render_timetable ===true)
                {
                  res.send(result)
                }
                else if(fac_blueprint_status[0].status === false && ok.render_timetable ===true)
                {
                  res.send('ok')
                }
                else if(fac_blueprint_status[0].status === false && ok.render_timetable === false) {
                  res.send('no');
                }
              }
              else
              {
                res.send('no')
              }
            })
           }
           else {
             res.send('no')
           }
          }
          else
          {
            res.send(result)
          }

        }
      })
    })


    router.post('/fetch_submitted_data', (req, res) => {
      console.log(req.body)
      Timetable.findOne({$and:[{username:req.user.username},{date:req.body.date},{month:req.body.month}
      ,{year:req.body.year},{time:req.body.time},{day_order:req.body.day_order},{hour:req.body.hour}]},function(err,result){
        if(err)
        {

        }
        else if(result)
        {
          res.send(result)
        }
      })
    })

    router.post('/updateSubmittedData', (req, res) => {
      Timetable.updateOne({$and:[{username:req.user.username},{date:req.body.data.date},{month:req.body.data.month}
      ,{year:req.body.data.year},{time:req.body.data.time},{day_order:req.body.data.day_order},{hour:req.body.data.hour}]},{covered:req.body.covered},function(err,result){
        if(err)
        {

        }
        else if(result)
        {
          res.send('ok')
        }
      })
    })

    /*----------------------------------Admin Operation for opening a set of dayorder availbale to everyone----------------- */
    /*-----Validate Admin---------------------------------------- */
    router.post('/checkadmin',function(req,res){
      User.findOne({username:req.user.username},function(err,found){
        if(err)
        {

        }
        else if(found)
        {
          if(bcrypt.compareSync(req.body.password, found.password)) {
            var succ ={
              succ:"Matched"
            }
            res.send(succ)
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!found)
        {

        }
      })
    })
 /*----------------------------------------Delete User ------------------------------------------------------ */

 router.post('/deleteadmin',function(req,res){
   User.findOne({username:req.body.username},function(err,found){
     if(err)
     {

     }
     else if(found)
     {
       User.deleteOne({username:found.username},function(err,done)
       {
         if(err)
         {

         }
         else if(done)
         {
          res.send("Deleted")
         }
       })
     }
   })
 })


 router.post('/deleteuser',function(req,res){
  User.findOne({username:req.body.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      User.deleteOne({username:found.username},function(err,done)
      {
      })
      Request.deleteMany({username:found.username},function(err,done)
      {
      })
      Timetable.deleteMany({username:found.username},function(err,done)
      {
      })
      Five.deleteMany({username:found.username},function(err,done)
      {
      })
      Degrees.deleteMany({username:found.username},function(err,done)
      {
      })
      Alloted_Slot.deleteMany({username:found.username},function(err,done)
      {
      })
      res.send("Deleted")
    }
  })
})

/*------------------------------------------------------------------Modaify Part-----------------------------------------------*/
router.post('/handle_change_today_dayorder',function(req,res){
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      Common_DayOrder.findOne({$and:[{day:day},{month:month},{year:year}]}, function (err, doc){
        if(err)
        {

        }
        else if(doc)
        {
          doc.day_order = req.body.day_order;
          doc.day_order_status = "online";
          doc.save();
          res.send("Common_DayOrder Updated!!")
        }
        else{
          res.send("Something went wrong !!")
        }
      })
      DayOrder_History.findOne({$and:[{day:day},{month:month},{year:year}]}, function (err, doc){
        if(err)
        {

        }
        else if(doc)
        {
        doc.day_order = req.body.day_order;
        doc.day_order_status = "online";
        doc.save();
        }
      })
    }
  })
})


router.post('/handle_start_of_semester',function(req,res){
  console.log(req.body.startday)
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      AdminOrder.findOne({order:'Start_Of_Semester'},function(err,foud)
      {
        if(err)
        {

        }
        else if(foud)
        {
          AdminOrder.update({order:'Start_Of_Semester'},{$set:{startday:req.body.startday,startmonth:req.body.startmonth,startyear:req.body.startyear}},function(err,data){
            res.send("Date is Updated !!")
          })
        }
        else
        {
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Saved Into the DB !!");
          })
        }
      })
    }
  })
})

router.post('/handle_end_of_semester',function(req,res){
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      AdminOrder.findOne({order:'End_Of_Semester'},function(err,foud)
      {
        if(err)
        {

        }
        else if(foud)
        {
          AdminOrder.update({order:'End_Of_Semester'},{$set:{endday:req.body.endday,endmonth:req.body.endmonth,endyear:req.body.endyear}},function(err,data){
            res.send("Date is Updated !!")
          })
        }
        else
        {
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Saved Into the DB !!");
          })
        }
      })
    }
  })
})

router.get('/fetch_start_date',function(req,res){
      AdminOrder.findOne({order:'Start_Of_Semester'},function(err,found)
      {
        if(err)
        {

        }
        else if(found)
        {
          res.send(found)
        }
        else
        {

        }
      })
})


router.get('/fetch_end_date',function(req,res){
  AdminOrder.findOne({order:'End_Of_Semester'},function(err,found)
  {
    if(err)
    {

    }
    else if(found)
    {
      res.send(found)
    }
    else
    {

    }
  })
})
router.post('/delete_start_of_sem',function(req,res){
  AdminOrder.remove({order:'Start_Of_Semester'},function(err,succ){
    res.send("Deleted !!")
  });
})

router.post('/delete_start_of_sem',function(req,res){
  AdminOrder.remove({order:'End_Of_Semester'},function(err,succ){
    res.send("Deleted !!")
  });
})

/*-----------------------------------------------Update Operations---------------------------------------------------------------*/
router.post('/update_faculty_password',function(req,res){
  console.log(req.body)
  User.findOne({username:req.body.faculty_username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      let hash = bcrypt.hashSync(req.body.faculty_password, 10);
      User.updateOne({username:req.body.faculty_username},{password: hash} ,(err, done) => {
        if(err)
        {

        }
        else if(done)
        {
          res.send("Updated");
        }
      })
    }
    else
    {
      res.send("User Not Found !!")
    }
  })
})

/*----------------------------------------------Suspension Operation----------------------------------------------------------*/
router.post('/suspension_user',function(req,res){
  console.log(req.body)
  User.findOne({username:req.body.suspend_faculty_username},function(err,found){
    if(err)
    {
      res.status(404).send("error!!")
    }
    else if(found)
    {
      AdminOrder.findOne({$and:[{victim_username:req.body.suspend_faculty_username},{order:'Suspend User'}]},function(err,obj){
        if(err)
        {

        }
        else if(obj)
        {
          res.send("Already Suspended !!")
        }
        else{
          var suspend = new AdminOrder({order:'Suspend User',victim_username:req.body.suspend_faculty_username,reason:req.body.suspend_reason_faculty,suspend_status:true});
          suspend.save((err, savedUser) => {
            if(err)
            {

            }
            else if(savedUser)
            {
              User.updateOne({username:req.body.suspend_faculty_username},{suspension_status: true},function(err,done){
                if(err)
                {

                }
                else if(done)
                {
                  res.send("Suspended !!")
                }
              })
            }
          })
        }
      })
    }
    else{
      res.send("User Not Found !!")
    }
  })
})
router.post('/remove_user_suspension',function(req,res){
  User.findOne({username:req.body.suspend_faculty_username},function(err,found){
    if(err)
    {
      res.status(404).send("error!!")
    }
    else if(found)
    {
      AdminOrder.findOne({$and:[{victim_username:req.body.suspend_faculty_username},{order:'Suspend User'}]},function(err,obj){
        if(err)
        {

        }
        else if(obj)
        {
          User.updateOne({username:req.body.suspend_faculty_username},{suspension_status: false},function(err,done){
            if(err)
            {

            }
            else if(done)
            {
              obj.remove();
              res.send("Removed Suspension !!")
            }
          })
        }
        else{
          res.send("User Not Found !!")
        }
      })
    }
    else{
      res.send("User Not Found !!")
    }
  })
})


router.get('/know_no_of_user',(req,res)=>{
  User.find({},(err,data)=>{
    if(err)
    {

    }
    else if(data)
    {
      res.send(data);
    }
  })
})

/*View Data-----------------------------------------------------------------------------------*/
router.post('/fetch_department_admin_history',(req,res)=>{
  console.log(req.body.username)
  User.findOne({activated_by : {"$exists" : true}},(err1,yes)=>{
    if(err1)
    {

    }
    else if(yes)
    {
      User.find({activated_by:req.body.username},(err,data)=>{
        if(err)
        {

        }
        else if(data)
        {
          //console.log(data)
          res.send(data);
        }
        else {
          res.send('no')
        }
      })
    }
    else{
      res.send('no')
    }
  })
})

router.post('/send_message',(req,res)=>{
    let subject ='Message From Admin';
    notification_send(req.body.username,subject,req.body.message,res,0,null)
})

router.post('/ping',(req,res)=>{
    let subject ='Message From Admin';
    let details = 'There are several requests for account activation pending kindly look to those.';
    notification_send(req.body.username,subject,details,res,0,null)
})

router.post('/contactus',(req,res)=>{
    send_contactus(req.body,res)
})

router.get('/fetch_complaint',(req,res)=>{
  Communication.find({fresh:true},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      res.send(found);
    }
  })
})

router.post('/send_reply_to_complaint',(req,res)=>{
  console.log(req.body)
  let serial = parseInt(req.body.serial);
  Communication.findOne({$and:[{serial:serial},{official_id:req.body.official_id}]},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      Communication.updateOne({$and:[{serial:serial},{official_id:req.body.official_id}]},{$set :{message_from_admin:req.body.message,fresh:false}},function(err,done){
        if(err)
        {

        }
        else if(done)
        {
          notification_send(req.body.official_id,'Complaint Solved '+found.complaint_subject,'Message for you : -' + req.body.message ,res,0)
        }
      })
    }
  })
})

router.post('/delete_id',(req,res)=>{
  Communication.deleteOne({serial:req.body.serial},(err,done)=>{
    if(err)
    {

    }
    else if(done)
    {
      res.send('done');
    }
  })
})

router.post('/forward_complaint',(req,res)=>{
    let ref= parseInt(req.body.data.serial_no);
    Communication.updateOne({serial:ref},{$set :{fresh:false}},function(err,done){
      if(err)
      {

      }
      else if(done)
      {
          notification_send(req.body.data.sendto,'FORWARDED MSG - REGARDING COMPLAINT',req.body.data.message_for_receiver,res,ref)
      }
    })
})

router.post('/complaint',(req,res)=>{
  var trans = new Communication(req.body.data);
  trans.save((err, saved) => {
    if(err)
    {

    }
    else if(saved)
    {
      res.send('ok');
    }
  })
})

router.post('/referNotification_Fetch',(req,res)=>{
  let ref = parseInt(req.body.ref);
  Communication.findOne({serial:ref},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      res.send(found);
    }
  })
})


router.post('/fetch_possible_list',(req,res)=>{
  Alloted_Slot.find({$and:[{status:true},{subject_code:req.body.props.subject_code}]},(err,found)=>{
    if(err)
    {

    }
    else
    {
      res.send(found);
    }
  })
})




function notification_send(username,subject,details,res,ref)
{
  console.log(res)
  var tran = new Notification({
    for:username,
    subject:subject,
    details:details,
    ref:ref,
  });
  tran.save((err,saved)=>{
    if(err)
    {

    }
    else if(saved)
    {
      res.send('done')
    }
  })
}



function send_contactus(req,res)
{
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'ework.care@gmail.com',
           pass: 'uolqdatwxgbhqals'
       }
   });
       const mailOptions = {
        from: 'ework.care@gmail.com',
        to: 'ework.contactus@gmail.com',
        subject: req.subject,
        html: '<p><b>Contact Person Name - </b>'+req.name+'</p><br /><p><b>Contact Person Mail Id - </b>'+req.mailid+'</p><p><b>Official Id Of the Contact Person - </b>'+req.id+'</p><br /><p><b>Message - </b>'+req.message+'</p><br />Team Ework - SRM CARE<br />Thank You.'
      };
      transporter.sendMail(mailOptions, function (err, info) {
        if(err)
        {

        }
        else
        {
          res.send('ok');
        }
     });

}

function mail_send(mailid,content,res,id)
{
  console.log(mailid)
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'ework.care@gmail.com',
           pass: 'uolqdatwxgbhqals'
       }
   });
       const mailOptions = {
        from: 'ework.care@gmail.com', // sender address
        to: mailid, // list of receivers
        subject: 'eWork - A message from eWork Team', // Subject line
        html: '<p>Dear User,</p><br /><p>'+content+'</p><br />Thank You.'
      };
      transporter.sendMail(mailOptions, function (err, info) {
        if(err)
        {

        }
        else
        {
          res.send('ok');
        }
     });
}


router.post('/sendStatus', (req, res) => {
  console.log(req.body);

  Timetable.findOne({$and:[{username:req.body.faculty_id},{date:req.body.date},
   {month:req.body.month},{year:req.body.year},{day_order:req.body.day_order},{hour:req.body.hour},
   {for_sem:req.body.sem},{for_year:req.body.for_year},{for_batch:req.body.batch},
   {slot:req.body.slot},{subject_code:req.body.subject_code},{week:req.body.week},{verified:false}]},(er,found)=>
     {
        if(er)
        {

        }
        else if(found)
        {
          if(req.body.positive_count === true)
          {
              Timetable.updateOne({$and:[{username:req.body.faculty_id},{date:req.body.date},
               {month:req.body.month},{year:req.body.year},{day_order:req.body.day_order},{hour:req.body.hour},
               {for_sem:req.body.sem},{for_year:req.body.for_year},{for_batch:req.body.batch},{subject_code:req.body.subject_code},
               {slot:req.body.slot},{week:req.body.week},{verified:false}]},{ $inc: {positive_count:1} },(error,done)=>
                 {
                   if(error)
                   {

                   }
                   else if(done)
                   {
                        goForVerification(req);
                   }
                 })
           }
           else
           {
             Timetable.updateOne({$and:[{username:req.body.faculty_id},{date:req.body.date},
              {month:req.body.month},{year:req.body.year},{day_order:req.body.day_order},{hour:req.body.hour},
              {for_sem:req.body.sem},{for_year:req.body.for_year},{for_batch:req.body.batch},{subject_code:req.body.subject_code},
              {slot:req.body.slot},{week:req.body.week},{verified:false}]},{ $inc: {negative_count:1} },(error,done)=>
                {
                  if(error)
                  {

                  }
                  else if(done)
                  {
                      goForVerification(req);
                  }
                })
           }
        }
        else{
          res.send('done')
        }
    })

    function goForVerification(req)
    {
      Timetable.findOne({$and:[{username:req.body.faculty_id},{date:req.body.date},
       {month:req.body.month},{year:req.body.year},{day_order:req.body.day_order},{hour:req.body.hour},
       {for_sem:req.body.sem},{for_year:req.body.for_year},{for_batch:req.body.batch},{subject_code:req.body.subject_code},
       {slot:req.body.slot},{week:req.body.week}]},(error1,dataset)=>
         {
           if(error1)
           {

           }
           else if(dataset)
           {
             if((dataset.positive_count+dataset.negative_count)>=(((req.body.total_student)*75)/100))
             {
              if(dataset.positive_count>=dataset.negative_count)
              {
                Timetable.updateOne({$and:[{username:req.body.faculty_id},{date:req.body.date},
                 {month:req.body.month},{year:req.body.year},{day_order:req.body.day_order},{hour:req.body.hour},
                 {for_sem:req.body.sem},{for_year:req.body.for_year},{for_batch:req.body.batch},{subject_code:req.body.subject_code},
                 {slot:req.body.slot},{week:req.body.week}]},{verified:true},(er,done)=>{
                  if(er)
                  {

                  }
                  else if(done)
                  {
                    User.findOne({username:req.body.faculty_id},(problem,having)=>{
                      if(problem)
                      {

                      }
                      else if(having)
                      {
                          var differ = true;
                        Academic_Calculate(having,res,differ);
                      }
                    })
                  }
                })
              }
              else
              {
                Timetable.updateOne({$and:[{username:req.body.faculty_id},{date:req.body.date},
                 {month:req.body.month},{year:req.body.year},{day_order:req.body.day_order},{hour:req.body.hour},
                 {for_sem:req.body.sem},{for_year:req.body.for_year},{for_batch:req.body.batch},{subject_code:req.body.subject_code},
                 {slot:req.body.slot},{week:req.body.week}]},{verified:false},(er,done)=>{
                  if(er)
                  {

                  }
                  else if(done)
                  {
                    User.findOne({username:req.body.faculty_id},(problem,having)=>{
                      if(problem)
                      {

                      }
                      else if(having)
                      {
                        var differ = true;
                        Academic_Calculate(having,res,differ);
                      }
                    })
                  }
                })
              }
            }
            else {
              res.send('done')
            }
        }
        else
        {
          res.send('done')
        }
  })
}
})







module.exports = router
