import React, { Component } from 'react'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       saved_skill:[],

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.fetchSavedSkills()
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

fetchSavedSkills =()=>{
  axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
  .then(res => {
      if(res.data)
      {
        this.setState({saved_skill:res.data})
      }
  });
}


   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user2/editt';
     } else {
       apiUrl = '/ework/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }


   updateState = () =>{
     this.setState({
       isAddProduct:false,
       isEditProduct:false,
     })
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm,title,description;
     var data;
             title ='Skills';
             data = {
               Action:'Skills',
               fielddata: [
                 {
                   header: "Your Skills",
                   name: "skill_name",
                   placeholder: "Enter Your Skill",
                   type: "text",
                   grid:3
                 },
                 {
                   header: "Level of Your Skill",
                   name: "skill_level",
                   placeholder: "Enter Your Lavel in this Skill",
                   type: "text",
                   grid: 3,
                   exception:true,
                 },
                 {
                   header: "Verification",
                   name: "skill_verification",
                   placeholder: "",
                   type: "text",
                   grid:3,
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} saved_skill={this.state.saved_skill} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (

  <div>
    {!this.state.isAddProduct &&

        <ProductList referOutside={this.props.referOutside} username={this.props.username} title={title} description={description}
        action={this.props.options} data={data}  editProduct={this.editProduct}/>
    }
    <br />
    {!this.state.isAddProduct &&
      <React.Fragment>
        <Grid container spacing={1}>
           <Grid item xs={6} sm={6}/>
           <Grid item xs={5} sm={5}>
            {!(this.props.referOutside) && <Button variant="contained" style={{float:'right'}} color="secondary" onClick={(e) => this.onCreate(e,this.props.options)} disabled={this.state.disabled}>
             Add Data
             </Button>}
           </Grid>
           <Grid item xs={1} sm={1}/>
       </Grid>
     </React.Fragment>
   }
     { productForm }
     <br/>
   </div>
);
}
}


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      Action:'',
      skill_name:'',
      skill_level:'',
      skill_verification:'',
      skill_rating:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
    skill_rating:'0',
  })
  if(this.props.data.Action === 'Skills')
  {
    this.setState({
      skill_verification:false,
    })
  }
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  stateSet=(object)=>{
    this.setState(object);
  }


  render() {
    // console.log(this.initialState.username)
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS TO YOUR PERSONAL DATA</h5>
}

    return(
      <Paper elevation={3} style={{padding:'10px'}}>
      <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{pageTitle} </Typography><br />
      <br />
          {this.props.data.fielddata.map((content,index)=>(
          <Grid container spacing={1} key={index}>
           <Grid item xs={1} sm={1}/>
            <Grid item xs={2} sm={2}>
             {content.placeholder === '' ?
               <div></div>
               :
               <div>
               {content.header}
               </div>
              }

            </Grid>
            <Grid item xs={8} sm={8}>
                <React.Fragment>
                   {content.exception ?
                     <FormControl style={{width:'100%'}}>
                       <InputLabel id="skill_level">Select Here</InputLabel>
                         <Select
                           labelId="skill_level"
                           id="skill_level"
                           name="skill_level" value={this.state.skill_level} onChange={this.handleInput}
                         >
                         <MenuItem value="Begineer">Begineer</MenuItem>
                         <MenuItem value="Intermediate">Intermediate</MenuItem>
                         <MenuItem value="Advanced">Advanced</MenuItem>
                         </Select>
                      </FormControl>
                       :
                     <React.Fragment>
                       {content.name === "skill_name" ?
                         <SearchEngine stateSet={this.stateSet} field={this.props.saved_skill} />
                       :
                       <div>
                        {content.placeholder === '' ?
                        <div></div>
                           :
                           <TextField
                             type={content.type}
                             id="outlined-textarea"
                             label={content.placeholder}
                             name={content.name}
                             value={this.state[content.name]}
                             onChange={e => this.handleD(e, index)}
                             multiline
                             variant="filled"
                             fullWidth
                           />
                         }
                         </div>
                       }
                     </React.Fragment>
                   }
                </React.Fragment>
            </Grid>
            <Grid item xs={1} sm={1}/>
          </Grid>
          ))}

<br /><br />
          <Grid container spacing={1}>
            <Grid item xs={9} sm={9} />
            <Grid item xs={1} sm={1}>
              <Button variant="outlined" onClick={this.props.cancel} color="secondary">CANCEL</Button>
            </Grid>
            <Grid item xs={2} sm={2}>
             <Button variant="contained" onClick={this.handleSubmit} style={{backgroundColor:'#455a64',color:'white'}} >SUBMIT</Button>
            </Grid>
          </Grid>
      </Paper>
    )
  }
}


class SearchEngine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchString: "",
      users: []
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.setState({
      users: this.props.field
    });
    this.refs.search.focus();
  }

  handleChange() {
    this.setState({
      searchString: this.refs.search.value
    });
  }

  handleLevel = (e) =>{
    this.setState({searchString:e})
    this.props.stateSet({
      skill_name:e,
    })
  }

  render() {
    let _users = this.state.users;
    let search = this.state.searchString.trim().toLowerCase();

    if (search.length > 0) {
      _users = _users.filter(function(user) {
        return user.skill_name.toLowerCase().match(search);
      });
    }

    return (
        <React.Fragment>
          <input
            type="text"
            value={this.state.searchString}
            ref="search"
            onChange={this.handleChange}
            placeholder="Search Skill Here"
          />
          {this.state.searchString &&
            <Grid container spacing={1}>
            {_users.map((l,index) => {
              return (
                <Grid item xs={3} sm={3} key={index} style={{padding:'8px'}} onClick={()=>{this.handleLevel(l.skill_name)}}>{l.skill_name}</Grid>
              );
            })}
            </Grid>
          }
       </React.Fragment>
    );
  }
}

class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
    this.setState({render_confirm:!this.state.render_confirm,productId:productId});
}

confirm=()=>{
  let delUrl;
  if(this.props.data.Action === 'FACULTY LIST'){
    delUrl = '/ework/user2/del_faculty_list';
  } else {
    delUrl = '/ework/user2/del';
  }

const { products } = this.state;
axios.post(delUrl,{
  serial: this.state.productId,
  username: this.props.username,
})
    .then(response => {
      this.setState({
        render_confirm:false,
        productId:0,
        response: response,
        products: products.filter(product => product.serial !== this.state.productId)
     })
    })
}

fetch =() =>{
  axios.post('/ework/user2/fetchall',{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    if(response.data.length>0)
    {
      if(this.props.oneEntry)
      {
        this.props.oneEntry({
          disabled:false,
        })
      }
    }
    this.setState({
     products: response.data,
   })
  })
}



  render() {
    const {  products} = this.state;
      return(
        <React.Fragment>
        <Dialog
          open={this.state.render_confirm}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Cnfirmation of Deleting the Data</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              After you click agree button below, your all rating datas related to this content will be affected.
              So kindly take decision what do you really want to do .
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.deleteProduct} color="primary">
              Disagree
            </Button>
            <Button onClick={this.confirm} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>

          <Paper elevation={3} style={{padding:'10px'}}>
            <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{this.props.title}</Typography><br />

              <Grid container spacing={1}>
                <Grid item xs={1} sm={1} style={{textAlign:'center'}}><b>Serial No</b></Grid>
                  {this.props.data.fielddata.map((content,index)=>(
                    <Grid item xs={content.grid} sm={content.grid} key={index} style={{textAlign:'center'}}><b>{content.header}</b></Grid>
                  ))}
                <Grid item xs={2} sm={2} style={{textAlign:'center'}}><b>Action</b></Grid>
              </Grid>
              {products.map((product,index) => (
                <Grid container spacing={1} key={index}>
                  <Grid item xs={1} sm={1} style={{textAlign:'center'}}>{index+1}</Grid>
                  {this.props.data.fielddata.map((content,index)=>(
                    <React.Fragment key={index}>
                    {content.placeholder === '' ?
                          <Grid item xs={content.grid} sm={content.grid} style={{textAlign:'center'}}>
                             {product[content.name] === false ?
                                <span className="red-text center">Verification Pending</span>
                                :
                                <span className="green-text center">Verified</span>
                             }
                          </Grid>
                       :
                      <Grid item sm={content.grid} xs={content.grid} style={{textAlign:'center'}}>{product[content.name]}</Grid>
                    }
                    </React.Fragment>
                  ))}

                    <Grid item xs={2} sm={2}>
                      {!(this.props.referOutside) && <div style={{textAlign:'center'}}>
                        <EditIcon className="go"  onClick={() => this.props.editProduct(product.serial,this.props.action)} />&nbsp;
                        <DeleteIcon className="go" style={{color:'red'}} onClick={() => this.deleteProduct(product.serial,this.props.action)} />
                      </div>
                    }
                   </Grid>
                </Grid>
              ))}
            </Paper>
        </React.Fragment>
      )

  }
}
