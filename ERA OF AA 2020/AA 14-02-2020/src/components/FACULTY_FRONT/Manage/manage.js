import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import Nav from '../../dynnav'
import SingleManage from './single_user_manage'
import GlobalAccess from './global_manage'
import Insturction from './instra.'
import BluePrint from './bluePrintReq'
import ContentLoader from "react-content-loader"



export default class Manage extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      loading: true,
      username:'',
      modal:false,
      redirectTo:'',
      option:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  getCount =() =>{
    axios.get('/ework/user/knowcount', {
    })
    .then(response =>{
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}

handleOption = (e) =>{
  this.setState({option:e.target.value})
}
componentDidMount(){
  M.AutoInit()
  this.getCount()
  axios.get('/ework/user/',
  this.setState({loading: true})
)
   .then(response =>{
     this.setState({loading: false})
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/ework/faculty',
       });
       window.M.toast({html: 'You are not Logged In',outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
     }
   })
}


  render()
  {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="35" y="29" rx="3" ry="3" width="330" height="30" />
      </ContentLoader>
    )
    if(this.state.loading ===  true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      {this.state.modal=== false &&
        <div onClick={this.selectModal} className="right animat bounce" style={{margin:'20px 20px 0px 0px',cursor: 'pointer'}}>
          <i className="material-icons small red-text">info_outline</i>
        </div>
      }
      <Insturction
          displayModal={this.state.modal}
          closeModal={this.selectModal}
      />
      <div className="row">
      <div className="col l2 hide-on-small-only" />
      <div className="col l8 s12 m12 xl8 form-signup">
          <div className="row">
            <div className="col l8 xl8 s12 m12">
            <h5 className="">Kindly Choose From <span className="hide-on-small-only">RightHandleSide</span><span className="hide-on-med-and-up">Below</span></h5>
            </div>
            <div className="col l4 s12 xl4 m12">
            <select name="title" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled defaultValue>Select Here...</option>
            <option value="one">Request For a Missed DayOrder Entry</option>
            <option value="one-slot">Request For Missed Slot Entry</option>
            <option value="blue_print">Blue Print Manage</option>
            </select>
            </div>
          </div>
      </div>
      <div className="col l2" />
      </div>
      <Navigate username={this.state.username} selected={this.state.option} />
      </React.Fragment>

    )
  }
}
  }
}


class Navigate extends Component{
  constructor()
  {
    super()
    this.state ={

    }
  }
  render()
  {
    let object;
    if(this.props.selected ==="one")
    {
      object = <SingleManage selected={this.props.selected} username={this.props.username} />
    }
    else if(this.props.selected === "one-slot")
    {
      object = <GlobalAccess selected={this.props.selected}  username={this.props.username} />
    }
    else if (this.props.selected === 'blue_print') {
      object = <BluePrint selected={this.props.selected}  username={this.props.username} />
    }
    else {
      object =<div></div>
    }
    return(
      <div>
       {object}
      </div>
    )
  }
}
