
import React, {Component} from 'react';
import axios from 'axios';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';


import SkillTableShow from './userDetails-skill'
import DomainTableShow from './userDetails-Domains'





export default class ShowStudentList extends Component {
 constructor()
 {
   super()
   this.state={
     loading:true,
     students_skill:'',
     students_domain:'',
     details_view:false,
     data:'',
     user:'',
   }
   this.componentDidMount = this.componentDidMount.bind(this)
 }

 componentDidMount()
 {
   this.fetchData();
 }

 componentDidUpdate=(prevProps)=>{
   if(prevProps!== this.props)
   {
     this.fetchData();
   }
 }

 fetchData=()=>{
   let arr3;
   if(this.props.type === 'skill_fetch')
   {
     axios.post('/ework/user2/fetch_student_on_a_skill',{skill:this.props.skill_selected})
     .then( res => {
       arr3 = res.data.data.map((item, i) => Object.assign({}, item, res.data.users[i]));
         this.setState({students_skill:arr3,loading:false})
     });
   }
   else if(this.props.type === 'domain_fetch') {
     axios.post('/ework/user2/fetch_student_on_a_domain',{domain:this.props.domain_selected})
     .then( res => {
        arr3 = res.data.data.map((item, i) => Object.assign({}, item, res.data.users[i]));
        this.setState({students_domain:arr3,loading:false})
     });
   }

 }

 setDomain=(e)=>{
   if(e === null)
   {

   }
   else{
     this.setState({skill_selected:e.skill_name})
   }
 }

 closeView=()=>{
   this.setState({details_view:false})
 }

 showDetails=(content)=>{
   this.setState({data:content,details_view:true})
 }



 retriveMessage=(e)=>{
   this.setState({r_message:e.target.value})
 }
 sendMessage=()=>{
   if(!this.state.r_message)
   {
              window.M.toast({html: 'Enter the message !!',classes:'rounded red'});
   }
   else {
     axios.post('/user/user2/sendMessage_From_User',{message:this.state.r_message})
     .then( res => {
         if(res.data)
         {
            window.M.toast({html: 'Sent !!',classes:'rounded green darken-1'});
           this.mWindow();
         }
     });
   }
 }

 render() {
   if(this.state.loading)
   {
     return(
       <Backdrop  open={true} >
         <CircularProgress color="secondary" />
       </Backdrop>
     )
   }
   else {
   return (
     <React.Fragment>
      {this.state.students_skill ?
        <React.Fragment>
          {this.state.loading === false && <SkillTableShow user_session={this.props.user_session} data={this.state} skill={this.state.students_skill} />}
       </React.Fragment>
       :
       <React.Fragment>
         {this.state.loading === false && <DomainTableShow user_session={this.props.user_session} data={this.state} domain={this.state.students_domain} />}
       </React.Fragment>
      }
     </React.Fragment>
   );
  }
 }
}
