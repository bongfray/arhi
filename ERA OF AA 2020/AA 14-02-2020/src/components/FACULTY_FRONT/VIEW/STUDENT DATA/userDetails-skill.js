import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import SendIcon from '@material-ui/icons/Send';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Rating from '@material-ui/lab/Rating';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';


import ViewStudentData from '../../FACULTY ADVISER/view_single_student'


function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

const headCells = [
  { id: 'username', numeric: false, disablePadding: false, label: 'Student Reg Id' },
  { id: 'Mail Id', numeric: true, disablePadding: false, label: 'Mail Id' },
  { id: 'Department', numeric: true, disablePadding: false, label: 'Department' },
  { id: 'Current Year', numeric: true, disablePadding: false, label: 'Current Year' },
  { id: 'Current Batch', numeric: true, disablePadding: false, label: 'Current Batch' },
  { id: 'skill_level', numeric: true, disablePadding: false, label: 'Skill Level' },
  { id: 'skill_rating', numeric: true, disablePadding: false, label: 'Skill Rating' },
  { id: 'view', numeric: true, disablePadding: false, label: 'View Data' },
  { id: 'action', numeric: true, disablePadding: false, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>{headCell.label}</b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable(props) {
  //console.log(props)
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [userdata, setuserData] = React.useState({
      details_view: false,
      data: '',
    });
    const [mwindow,setMwindow] = React.useState({
        open_popup: false,
        userDetails: '',
      });
    const [message, setMessage] = React.useState('');
    const [mail_send, setMailOn] = React.useState(false);
    const [disable_btn, setDisable] = React.useState(false);
    const [filter,setFilter] = React.useState(false);
    const [value, handleChange] = React.useState({
      filter_on:false,
      type:'',
    });


  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = props.skill.map(n => n.username);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const closeView=()=>{
    setuserData({
      details_view:false,
    })
  }
  const sendMessage=()=>{
    var data;
    if(!message)
    {
      window.M.toast({html: 'Enter all the Details !!',classes:'rounded red'});
    }
    else{
      if(mail_send)
      {
        data = {
          user:mwindow.userDetails,
          message:message,
          mail_send:mail_send,
          user_session:props.user_session,
        }
          window.M.toast({html: 'Sending Mail....',classes:'rounded orange'});
      }
      else {
        data = {
          user:mwindow.userDetails,
          message:message,
          mail_send:mail_send,
          user_session:props.user_session,
        }
        window.M.toast({html: 'Sending...',classes:'rounded orange'});
      }
      setDisable(true)
      axios.post('/ework/user2/sendMessage_from_UserSide',data)
      .then( res => {
        setDisable(false)
          if(res.data === 'ok')
          {
            window.M.toast({html: 'Sent !!',classes:'rounded green darken-1'});
            setMwindow({
              open_popup:false,
            })
          }
          else if(res.data === 'no') {
            window.M.toast({html: 'Failed To Send !!',classes:'rounded red'});
          }
      })
      .catch( err => {
          if(err)
          {
            window.M.toast({html: 'Error !!',classes:'rounded red'});
          }
      });
   }
  }


  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.skill.length - page * rowsPerPage);

  return (
    <div className={classes.root}>


    {mwindow.open_popup &&
        <Dialog open={mwindow.open_popup} onClose={()=>setMwindow({open_popup:false,userDetails:''})} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Send Message</DialogTitle>
          <DialogContent>
            <DialogContentText>
             This message will be forwarded to the particular student.
            </DialogContentText>
            <div className="switch">
              <label>
                <input checked={mail_send} type="checkbox" onClick={(e)=>setMailOn(!mail_send)} />
                <span className="lever"></span>
                Send As Mail
              </label>
            </div>
            <br />
            {mail_send &&
              <React.Fragment>
                <TextField
                  required
                  id="filled-required"
                  label="Maild Id"
                  value={mwindow.userDetails.mailid}
                  variant="filled"
                  fullWidth
                  multiline
                  disabled
                />
                <br /><br />
             </React.Fragment>
            }
            <TextField
              required
              id="filled-required"
              label="Enter the message"
              value={message}
              onChange={(e)=>setMessage(e.target.value)}
              variant="filled"
              fullWidth
              multiline
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setMwindow({open_popup:false,userDetails:''})} color="primary">
              Cancel
            </Button>
            <Button disabled={disable_btn} color="primary" onClick={sendMessage}>
              Send Message
            </Button>
          </DialogActions>
         </Dialog>
    }

      {userdata.details_view &&
        <ViewStudentData details_view={userdata.details_view}  closeView={closeView} data={userdata} />
      }
      <Paper className={classes.paper}>
        <Button variant="outlined" style={{float:'right',padding:'5px',color:'red'}} onClick={()=>setFilter(!filter)}>Filter</Button>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={props.skill.length}
            />
            <TableBody>
              {stableSort(props.skill, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row.username)}
                      role="checkbox"
                      tabIndex={-1}
                      key={row.username}
                    >
                      <TableCell>
                        {row.username}
                      </TableCell>
                      <TableCell align="center">{row.mailid}</TableCell>
                      <TableCell align="center">{row.dept}</TableCell>
                      <TableCell align="center">{row.year}</TableCell>
                      <TableCell align="center">{row.batch}</TableCell>
                      <TableCell align="center">{row.skill_level}</TableCell>
                      <TableCell align="center">
                         <Rating name="read-only" value={parseInt(row.skill_rating)} readOnly />
                      </TableCell>
                      <TableCell align="center">
                        <Tooltip title="View Details">
                         <VisibilityIcon onClick={() => setuserData({
                           details_view:true,
                           data:row,
                         }) } />
                        </Tooltip>
                      </TableCell>
                      <TableCell align="center">
                          <Tooltip title="Send Message To This Student">
                           <SendIcon onClick={()=>setMwindow({
                             open_popup:true,
                             userDetails:row,
                           })} />
                          </Tooltip>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.skill.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      {filter &&
        <Dialog open={filter}>
        <div style={{padding:'15px'}}>
            {!(value.filter_on) &&
              <FormControl component="fieldset">
               <FormLabel component="legend">Filter Possibilities</FormLabel>
                <RadioGroup aria-label="gender" name="gender1" value={value.type} onChange={(e)=>handleChange({
                  filter_on:true,
                  type:e.target.value
                })}>
                 <FormControlLabel  style={{color:'black'}} value="department" control={<Radio />} label="On Department" />
                 <FormControlLabel  style={{color:'black'}} value="year" control={<Radio />} label="On Year" />
                 <FormControlLabel  style={{color:'black'}} value="sem" control={<Radio />} label="On Sem" />
                 <FormControlLabel  style={{color:'black'}} value="yds" control={<Radio />} label="On Year + Department + Sem" />
                </RadioGroup>
               </FormControl>
             }
             {value.filter_on &&
               <React.Fragment>
                 <ArrowBackIcon onClick={(e)=>handleChange({
                   filter_on:false,
                 })} />
                 <br />
                 <Filter filter={filter} type={value.type} setFilter={setFilter} datas={props}/>
               </React.Fragment>
             }
        </div>
        </Dialog>
      }

    </div>
  );
}


function Filter(props){
  const [dept_filter, setState] = React.useState({});
    const [disable_btn, setDisable] = React.useState(true);

  if(props.type === 'department')
  {
      let dept_data =[];
       for(var i=0;i<props.datas.skill.length;i++)
       {
         if(props.datas.skill[i].dept !== dept_data[i])
         {
           dept_data[i]= props.datas.skill[i].dept;
         }
       }

     dept_data.splice(0, dept_data.length, ...(new Set(dept_data)))


      const handleChange = e => {
        setState(e.target.value);
        setDisable(false);
      };
      const checked =(incoming)=>{
        if(dept_filter === incoming)
        {
          return true;
        }
        return false;
      }
      const setFilter=()=>{
         var data = props.datas.skill.filter(function(item){
          return item.dept === dept_filter;
        })
        props.setFilter(false);
      }

    return(
        <React.Fragment>
           <FormGroup row>
             {dept_data.map((row,index)=>{
               return(
                   <FormControlLabel key={index} style={{color:'black'}}
                     control={
                       <Checkbox checked={checked(row)} onChange={handleChange} value={row} />
                     }
                     label={row}
                   />
               )
             })}
           </FormGroup>
           <br />
           <Button disabled={disable_btn} style={{float:'right'}} color="primary" onClick={setFilter}>
             APPLY
           </Button>
        </React.Fragment>
    )
  }
}
