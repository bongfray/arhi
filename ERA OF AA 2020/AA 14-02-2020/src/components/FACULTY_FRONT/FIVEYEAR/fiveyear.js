import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import ContentLoader from "react-content-loader"
import Nav from '../../dynnav'
import AddProduct from './add'
import ProductList from './prod'



class FiveYear extends Component{
    constructor(){
        super();
        this.state={
          active:0,
          home:'/ework/faculty',
          logout:'/ework/user/logout',
          get:'/ework/user/',
          nav_route: '/ework/user/fetchnav',
          username:'',
          redirectTo:'',
          loading:true,
          isAddProduct: false,
          response: {},
          action:'',
          product: {},
          isEditProduct: false,
          five:[{year:'Year - I',parts:[{name:'Administrative',index:'YearI-Administrative'},{name:'Academic',index:'YearI-Academic'},{name:'Research',index:'YearI-Research'}]},
          {year:'Year - II',parts:[{name:'Administrative',index:'YearII-Administrative'},{name:'Academic',index:'YearII-Academic'},{name:'Research',index:'YearII-Research'}]},
          {year: 'Year - III',parts:[{name:'Administrative',index:'YearIII-Administrative'},{name:'Academic',index:'YearIII-Academic'},{name:'Research',index:'YearIII-Research'}]},
          {year:'Year - IV',parts:[{name:'Administrative',index:'YearIV-Administrative'},{name:'Academic',index:'YearIV-Academic'},{name:'Research',index:'YearIV-Research'}]},
          {year:'Year - V',parts:[{name:'Administrative',index:'YearV-Administrative'},{name:'Academic',index:'YearV-Academic'},{name:'Research',index:'YearV-Research'}]}]
        }
    }
    componentDidMount(){
        M.AutoInit();
        axios.get('/ework/user/'
      )
         .then(response =>{
           this.setState({loading: false})
           if(response.data.user)
           {
             this.setState({username: response.data.user.username})
           }
           else{
             this.setState({
               redirectTo:'/ework/faculty',
             });
             window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
           }
         })
    }


    onCreate = (e,index) => {
      this.setState({ isAddProduct: index ,product: {}});
    }
    onFormSubmit =(data) => {
      // console.log(data)
      let apiUrl;
      if(this.state.isEditProduct)
      {
        apiUrl = '/ework/user/edit_five_year_plan';
      }
       else
       {
        apiUrl = '/ework/user/add_five_year_plan';
       }
      axios.post(apiUrl,data)
          .then(response => {
            this.setState({
              response: response.data,
              isAddProduct: false,
              isEditProduct: false
            })
          })
    }

    editProduct = (productId,index)=> {
      axios.post('/ework/user/fetch_five_year_existing_data',{
        id: productId,
      })
          .then(response => {
            this.setState({
              product: response.data,
              isEditProduct: index,
              isAddProduct: index,
            });
          })

   }


   handleSet=(e)=>{
     this.setState({
       active: e,
     })
   }
   color =(position) =>{
     if (this.state.active === position) {
         return "col l2 xl2 go center #69f0ae green accent-2 black-text active-pressed";
       }
       return "col l2 xl2 #e0e0e0 grey lighten-2 center active-pressed go";
   }


    render(){
      const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="40" y="30" rx="0" ry="0" width="320" height="90" />
          <rect x="40" y="130" rx="0" ry="0" width="320" height="90" />

        </ContentLoader>
      )
      if(this.state.loading === true)
      {
        return(
          <MyLoader />
        );
      }
      else{
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return(
            <React.Fragment>
            <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

               <div className="row">
                  <div className="col l4 xl4 m5 s4"/>
                  <div className="col l4 xl4 s4 m2">
                    <h6 className="center prof-heading black-text">Your Plan For Next Five Year</h6>
                    {/*<div className="row" style={{marginTop:'15px'}}>
                       <div onClick={(e)=>{this.handleSet(0)}} className={this.color(0)}>Year-I</div>
                       <div onClick={(e)=>{this.handleSet(1)}} className={this.color(1)}>Year-II</div>
                       <div onClick={(e)=>{this.handleSet(2)}} className={this.color(2)}>Year-III</div>
                       <div onClick={(e)=>{this.handleSet(3)}} className={this.color(3)}>Year-IV</div>
                       <div onClick={(e)=>{this.handleSet(4)}} className={this.color(4)}>Year-V</div>
                    </div>*/}
                  </div>
                  <div className="col l4 xl4 m5 s4"/>
               </div>

                <div className="row">
                <div className="col l1" />
                <div className="col l10">
                {this.state.five.map((content,index)=>(
                  <div key={index} className="col l12 xl12 s12 m12 card">
                      <p className="center #64ffda teal accent-2 black-text" style={{height: '45px',paddingTop: '12px'}}>{content.year}</p>
                      {content.parts.map((items,no)=>(
                        <ul className="collapsible" key={no}>
                            <li>
                                <div className="center collapsible-header #757575 grey darken-1 white-text">{items.name}</div>
                                <div className="collapsible-body">
                                    <div className="row">
                                    {!this.state.isAddProduct && <ProductList data={items} username={this.state.username}  action={items.index} editProduct={this.editProduct}/>}
                                    {!this.state.isAddProduct &&
                                      <div className="btn-floating btn-small right" onClick={(e) => this.onCreate(e,items.index)}><i className="material-icons go">add</i></div>
                                     }

                                     {((this.state.isAddProduct === items.index) || (this.state.isEditProduct === items.index)) &&
                                     <AddProduct data={items}  action={items.index} username={this.state.username} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                                     }
                                    </div>
                                </div>
                            </li>
                      </ul>
                      ))}
                  </div>
                ))}
                </div>
                <div className="col l1"/>
                </div>
            </React.Fragment>
        )
      }
    }
    }
}

export default FiveYear;
