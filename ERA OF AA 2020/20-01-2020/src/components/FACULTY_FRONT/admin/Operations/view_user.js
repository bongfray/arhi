import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'

export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'',
            modal:false,
            disabled:'',
            allowed:false,
            active:'',
            color:'',
        }
    }
    toggle = (position) =>
    {
      if (this.state.active === position) {
        this.setState({active : null})
      } else {
        this.setState({active : position})
      }
    }
    color =(position) =>{
      if (this.state.active === position) {
          return "col l2 red";
        }
        return "col l2";
    }
    render(){
      let view_content;
      if(this.props.select === 'dept_admin_profile'){
            view_content=
                <div>
                   <DeptAdminData />
                </div>
        }
        else if(this.props.select === 'facprofile'){
            view_content =
                <div className="center">
                   <div className="row">
                      <div className="col l2" />
                      <div className="col l8 blue white-text form-signup" style={{}}>
                        <div  onClick={() => this.toggle(0)} className={"col l2"+this.color(0)} style={{border:'1px solid #dadada',
                            borderRadius: '10px'}}>V2</div>
                        <div className={this.color(1)} onClick={() => this.toggle(1)} style={{border: '1px solid #dadada',
                            borderRadius: '10px'}}>V3</div>
                        <div className={this.color(2)} onClick={() => this.toggle(2)} style={{border: '1px solid #dadada',
                            borderRadius: '10px'}}>V4</div>
                        <div className={this.color(3)} onClick={() => this.toggle(3)} style={{border: '1px solid #dadada',
                            borderRadius: '10px'}}>V5</div>
                        <div className={this.color(4)} onClick={() => this.toggle(4)} style={{border: '1px solid #dadada',
                            borderRadius: '10px'}}>V4</div>
                        <div className={this.color(5)} onClick={() => this.toggle(5)} style={{border: '1px solid #dadada',
                            borderRadius: '10px'}}>V5</div>
                      </div>
                      <div className="col l2" />
                   </div>
                </div>
        }
        else if(this.props.select === 'studprofile'){
            view_content =
                <div>
                 STUD PROFILE
                </div>
        }
        else{
            view_content=
                <div></div>
        }
        return(
          <React.Fragment>
          <div>
            {view_content}
          </div>
          </React.Fragment>
        );
    }
}



class DeptAdminData extends Component {
  constructor()
  {
    super()
    this.state={
      departAdmin:[],
      user:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.data()
  }
  data(){
    axios.get('/ework/user/know_no_of_user')
    .then( res => {
      const d_user = (res.data.filter(item => item.h_order=== 0.5));
      console.log(d_user);
        this.setState({departAdmin:d_user})
    });
  }
  showDetails =(user)=>{
    this.setState({
      render_detail:!this.state.render_detail,
      user:user,
    })
  }

  render() {
    return (
      <React.Fragment>
      {this.state.render_detail && <ShowDetails detail={this.showDetails} user={this.state.user} />}
      <table>
        <thead>
          <tr>
              <th className="center">No</th>
              <th className="center">Official ID</th>
              <th className="center">Department</th>
              <th className="center">Campus</th>
              <th className="center">Mail ID</th>
              <th className="center">Action</th>
          </tr>
        </thead>

        <tbody>
        {this.state.departAdmin.map((content,index)=>(
          <tr key={index}>
            <td className="center">{index+1}</td>
            <td className="center">{content.username}</td>
            <td className="center">{content.dept}</td>
            <td className="center">{content.campus}</td>
            <td className="center">{content.mailid}</td>
            <td className="center"><button className="btn" onClick={ () => this.showDetails(content)}>View History</button></td>
          </tr>
        ))}
        </tbody>
      </table>
      </React.Fragment>
    );
  }
}


class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_accepted:'',
      total_request:'',
      message:'',
      mailid:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.total_details();
  }
  total_details = () =>{
    axios.post('/ework/user/fetch_department_admin_history',{username:this.props.user.username})
    .then( res => {
      const total_request= (res.data).length;
      const total_accepted= (res.data.filter(item => item.active === true)).length;
      this.setState({total_accepted:total_accepted,total_request:total_request})
    });
  }

  retriveMessage =(e)=>{
    this.setState({message:e.target.value})
  }
  sendMessage = (mailid) =>{
    if(!this.state.message)
    {
      window.M.toast({html: 'First fill the message !!',outDuration:'9000', classes:'rounded red '});
    }
    else{
    axios.post('/ework/user/send_message',{username:this.props.user.username,message:this.state.message})
    .then( res => {
        if(res.data)
        {
          window.M.toast({html: 'Sent !!',outDuration:'9000', classes:'rounded green darken-2'});
          this.setState({message:''})
        }
    });
    }
  }
  ping =()=>{
    axios.post('/ework/user/ping',{username:this.props.user.username})
    .then( res => {
        window.M.toast({html: 'Pinged !!',outDuration:'9000', classes:'rounded green darken-2'});
    });
  }

  render() {
    return (
      <div className="notinoti">
          <div className="right go" style={{padding:'8px'}} onClick={this.props.detail}><i className="small material-icons black-text">close</i></div>
          <h5 className="center">You are viewing the profile of <span className="pink-text">{this.props.user.username}</span></h5>
          <div className="row">
              <div className="col l3"/>
              <div className="col l6 form-signup">
              <div className="col l4">
                 <div className="card">
                 <div className="pink white-text center">TOTAL REQUEST</div>
                 <div className="card-content center">
                  <h5>{this.state.total_request}</h5>
                 </div>
                 </div>
              </div>
              <div className="col l4">
                 <div className="card">
                 <div className="pink white-text center">ACCEPTED</div>
                 <div className="card-content center">
                  <h5>{this.state.total_accepted}</h5>
                 </div>
                 </div>
              </div>
              <div className="col l4">
                 <div className="card">
                 <div className="pink white-text center">PENDING</div>
                 <div className="card-content center">
                 <p className="right red-text go" onClick={this.ping}>PING</p>
                  <h5>{this.state.total_request - this.state.total_accepted}</h5>
                 </div>
                 </div>
              </div>
              </div>
              <div className="col l3"/>
          </div>
          <div className="row">
              <div className="col l6">
                 <div className="row">
                    <div className="col l1"/>
                    <div className="col l10 form-signup">
                        <h6 className="center">Leave A Message</h6>
                        <div className="row">
                          <div className="col l6">
                            <label className="">To : </label><span>{this.props.user.username}</span>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col l12">
                            <label className="pure-material-textfield-outlined alignfull">
                              <textarea
                                className=""
                                type="text"
                                placeholder=" "
                                min="10"
                                max="60"
                                value={this.state.message}
                                onChange={this.retriveMessage}
                              />
                              <span>Enter the Message</span>
                            </label>
                          </div>
                        </div>
                        <button className="right btn" onClick={this.sendMessage} style={{marginBottom:'5px'}}>SEND A MESSAGE</button>
                    </div>
                    <div className="col l1"/>
                 </div>
              </div>
              <div className="col l6">
               <div className="row">
               <div className="col l1" />
               <div className="col l10 form-signup">
                  <div className="row">
                      <span className="col l4">Deactivate the Account</span>
                                      <div className="switch col l2">
                                        <label>
                                          <input  checked={this.state.isChecked} value="fac" type="checkbox" />
                                          <span className="lever"></span>
                                        </label>
                                      </div>
                  </div>
                 <div className="row">
                     <div className="col l6">
                        <div className="card">
                        <div className="pink white-text center">NO OF COMPLAINS</div>
                        <div className="card-content center">
                         <h5>6</h5>
                        </div>
                        </div>
                     </div>
                     <div className="col l6">
                        <div className="card">
                        <div className="pink white-text center">FEEDBACK RATING</div>
                        <div className="card-content center">
                         <h5>4.5*</h5>
                        </div>
                        </div>
                     </div>
                 </div>
                 </div>
                 <div className="col l1" />
                 </div>
              </div>
          </div>
      </div>
    );
  }
}
