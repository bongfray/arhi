import React, { Component } from 'react'
import { } from 'react-router-dom'


import axios from 'axios'

export default class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.initialState ={
      expired_day:'',
      expired_month:'',
      expired_year:'',
      denying_reason:'',
      approving:false,
      denying:'',
    }
    this.state=this.initialState;
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleDeny =(id,username) =>{
    axios.post('/ework/user/denyrequest',{
      serial: id,
      username:username,
      denying_reason:this.state.denying_reason,
    }).then(res=>{
      this.props.modalrender();
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  handleApprove = (id,username) =>{
    axios.post('/ework/user/approverequest',{
      serial: id,
      username:username,
      expired_day:this.state.expired_day,
      expired_month:this.state.expired_month,
      expired_year:this.state.expired_year
    }).then(res=>{
      this.props.modalrender();
      if(res.data)
      {
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
      this.setState(this.initialState)
      }
    })
  }
  proceedToApprove = () =>{
    this.setState({approving:true,denying:false})
  }
  proceedToDeny = () =>{
    this.setState({denying:true,approving:false})
  }
  addExpire = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }


  render()
  {
    var dat;
    if(this.props.view_details ==="yes")
    {
      dat= <React.Fragment>
      <h6>Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!</h6>
      <div className="row">
      <button className="btn col l2 right red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      </div>
      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat= <React.Fragment>
      <h6>We can't able to find any data for this request !! Kindly take decison manually !!</h6>
      <div className="row">
      <div className="col l7" />
      <div className="col l5">
      <div className="row">
      <div className="col l5" />
      <button className="btn col l3 sup red" onClick={this.proceedToDeny}>DENY</button>
      <div className="col l1" />
      <button className="btn col l3 blue-grey darken-2 sup" onClick={this.proceedToApprove}>APPROVE</button>
      </div>
      </div>
      {this.state.denying===true &&
        <React.Fragment>
        <div className="row">
          <div className="col l10 input-field">
            <input type="text" id="reason" className="validate" name="denying_reason" value={this.state.denying_reason} onChange={this.addExpire} required/>
            <label htmlFor="reason">Enter the Reason</label>
          </div>
          </div>
          <button className="right btn col l3 sup red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
        </React.Fragment>
      }
      {this.state.approving=== true && <React.Fragment>
        <div className="row"><h6 className="left">Enter Expiry Date -</h6></div>
        <div className="row">
        <div className="col l4 input-field">
           <input type="number" id="day" className="validate" name="expired_day" value={this.state.expired_day} onChange={this.addExpire} required/>
           <label htmlFor="day">Day(dd)</label>
        </div>
        <div className="col l4 input-field">
           <input type="number" id="month" className="validate" name="expired_month" value={this.state.expired_month} onChange={this.addExpire} required/>
           <label htmlFor="month">Month(mm)</label>
        </div>
        <div className="col l4 input-field">
           <input type="number" id="year" className="validate" name="expired_year" value={this.state.expired_year} onChange={this.addExpire} required/>
           <label htmlFor="year">year(yyyy)</label>
        </div>
      </div>
        <button className="btn col l3 blue-grey darken-2 sup right" onClick={() => this.handleApprove(this.props.index_id,this.props.username)}>APPROVE</button>
        </React.Fragment>
    }
      </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    return(
      <React.Fragment>
      {this.props.renderDiv && <div className="cover_all" style={{display:this.props.showButton}}>
        <div className="up" style={{padding:'30px'}}>
          {dat}
          <div className="row">
             <div className="styled-btn right" onClick={this.props.modalrender}>close</div>
          </div>
        </div>
      </div>
    }
    </React.Fragment>
    )
  }
}
