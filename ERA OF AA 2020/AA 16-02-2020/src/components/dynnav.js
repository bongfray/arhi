import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import Slogo from './Slogo.png'
import M from 'materialize-css'
import Notification from './notification'
import ContentLoader from "react-content-loader"
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

export default class Navbar extends Component {
  constructor()
  {
    super()
    this.state ={
      redirectTo:'',
      show:'none',
      loading:true,
      loggedIn:false,
      username:'',
      nav:[],
      notidisp:'none',
      newDisplay:'none',
      count_noti:'',
      showSidebar:false,
      user_details:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.logout = this.logout.bind(this)
  }
  getUser = () =>{
    axios.get(this.props.get).then(response => {
      if (response.data.user) {
        this.fetch();
        this.newNoti();
        this.setState({
          loggedIn: true,
          username: response.data.user.username,
          user_details:response.data.user,
        })
      } else {
        this.setState({
          loading:false,
          loggedIn: false,
          username: null,
        })
      }
    })
  }


  fetch()
  {
    axios.get(this.props.nav_route).then(response =>{
      var nav;
     if(this.state.user_details.count<=2)
     {
       if((this.state.user_details.suprimity === false) && (this.state.user_details.faculty_adviser === false))
       {
           nav = response.data.filter(item => (item.link!=="/ework/faculty_adviser") && (item.link!=="/ework/suprimity_admin") );
       }
       else if((this.state.user_details.suprimity === true) && (this.state.user_details.faculty_adviser === false))
        {
          nav = response.data.filter(item => (item.link!=="/ework/faculty_adviser"));
        }
        else if((this.state.user_details.suprimity === false) && (this.state.user_details.faculty_adviser === true))
         {
           nav = response.data.filter(item => (item.link!=="/ework/suprimity_admin"));
         }
       else {
         nav = response.data;
       }
     }
     else
     {
       nav = response.data;
     }
      this.setState({
       nav: nav,
     })
    })
  }

  componentDidMount = () => {
    M.AutoInit();
    this.getUser();
}

newNoti = () =>{
  axios.get('/ework/user/fetch_new_notification')
  .then( res => {
    this.setState({loading:false})
    if(res.data === 'no_u')
    {
      this.setState({newDisplay:'none'})
    }
      else if(res.data.length === 0)
      {
        this.setState({newDisplay:'none'})
      }
      else{
        this.setState({newDisplay:'block',count_noti:res.data.length})
      }
  });
}

renderSlide =() =>{
  this.setState({showSidebar:!this.state.showSidebar})
}


    logout(event) {
        event.preventDefault()
          window.M.toast({html: 'Logging Out....!!',classes:'pink rounded'});
        axios.post(this.props.logout).then(response => {
          if (response.status === 200) {
            this.setState({
              loggedIn: false,
              username: null,
              redirectTo:'/',
            })
            window.M.toast({html: 'Logged Out....!!',classes:'green rounded'});
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }


      notiPush =() =>{
        this.setState({notidisp:'block'})
      }
      setDisp = (object) =>{
        this.setState(object);
      }
    render() {
      let output

      //console.log(this.state.nav)
      const MyLoader = () => (
        <ContentLoader
          height={15}
          width={400}
          speed={2}
          primaryColor="#E5E8E8"
          secondaryColor="#F8F9F9"
        >
          <rect x="10" y="3" rx="3" ry="3" width="350" height="4" />
          <rect x="15" y="10" rx="3" ry="3" width="380" height="4" />
        </ContentLoader>
      )
  if(this.state.loading === true)
  {
    output = <MyLoader />
  }
  else
  {
 if(this.state.loggedIn === false)
{
  output=
    <div className="navbar-fixed">
  <nav>
    <div className="nav-wrapper blue-grey darken-2">
        <a href="#!" className="brand-logo">
          <img src={Slogo} className="srm" alt="SRM Institute of Science and Technology"
          title="SRM Institute of Science and Technology"/>
        </a>
      <div className="row">
      <div className="col s2 m2 xl2 l2" />
      <div className="col s8 m8 xl8 l8">
       <div className="row">
       <div className="col l1 xl1"/>
       <div className="col l11 xl11">
         <div style={{paddingLeft:'50px'}} className="con nav-cen hide-on-med-and-down">
           SRM Centre for Applied Research in Education
          </div>
       </div>
      </div>
      </div>
      <div className="col s2 xl2 m2 l2">
      </div>
      </div>
    </div>
  </nav>
</div>
}
else if(this.state.loggedIn === true)
{
        output=
          <div className="navbar-fixed">
                     <nav>
                     {this.state.showSidebar &&
                       <Drawer anchor="left" open={this.state.showSidebar}
                       onClose={this.renderSlide}>
                         <IconButton onClick={this.renderSlide}>
                           <ChevronLeftIcon />
                         </IconButton>
                         <Divider />
                           <List>
                           <ListItem button>
                             <Link to={this.props.home} style={{color:'black',textDecoration:'none'}}><ListItemText primary={"Home"} /></Link>
                           </ListItem>
                               {this.state.nav.map((content,index)=>(
                                 <ListItem button key={index}>
                                  <Link style={{color:'black',textDecoration:'none'}} to={content.link}>
                                    <ListItemText primary={content.val} />
                                  </Link>

                                 </ListItem>
                               ))}
                               <Divider />
                               <ListItem button>
                                 <Link to="#" style={{color:'black',textDecoration:'none'}} onClick={this.logout}><ListItemText primary={"LOG OUT"} /></Link>
                               </ListItem>
                           </List>
                       </Drawer>
                       }
                          <div className="nav-wrapper blue-grey darken-2">
                          <div className="row">
                            <div className="col l2 s2 xl2 m2">

                            <div className="left hide-on-large-only">
                                  {this.state.showSidebar=== false &&
                                    <i onClick={this.renderSlide} className="go medium material-icons">dehaze</i>
                                  }
                            </div>
                                 <div className="brand-logo">
                                   <a href="#!">
                                     <img src={Slogo} className="srm"
                                      alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/>
                                   </a>
                                  </div>
                            </div>
                            <div className="col l7 s7 xl7 m7 hide-on-med-and-down ">
                                 <div className="center con" style={{marginLeft:'120px'}}>
                                   <a href="#!">SRM CARE</a>
                                 </div>
                            </div>
                            <div className="col l3 s4 xl3 m4 push-s7 push-m7">
                                <ul className="right">
                                  {this.props.home && <li title="Home">
                                    <Link to={this.props.home}><i className=" hide-on-med-and-down material-icons">home</i></Link>
                                  </li>}
                                  <li title="Log Out">
                                      <Link to="#" className="center" onClick={this.logout}>
                                      <i className="hide-on-med-and-down material-icons right go">exit_to_app</i>
                                      </Link>
                                  </li>
                                  <li title="notification" onClick={this.notiPush}>
                                        <Link to="#" className="center">
                                          <span style={{display:this.state.newDisplay}} className="notification-number">{this.state.count_noti}</span>
                                          <i className="material-icons center">notifications</i>
                                        </Link>
                                  </li>
                                  {this.props.nav_route && <li title="menu" className="hide-on-med-and-down droppp">
                                    <i className="material-icons men" >more_vert</i>
                                    <ul>
                                      {this.state.nav.map((content,index)=>{
                                        return(
                                          <li key={content.val}><Link to={content.link}>{content.val}</Link></li>
                                        )
                                      })}
                                    </ul>
                                    </li>}
                                </ul>
                            </div>
                      </div>
                          </div>
                      </nav>
                      </div>
      }
    }
    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <React.Fragment>
        {this.state.notidisp === 'block' ? <Notification setDisp={this.setDisp}/>
        : <React.Fragment> {output} </React.Fragment>
        }
        </React.Fragment>
      )
    }

    }
}
