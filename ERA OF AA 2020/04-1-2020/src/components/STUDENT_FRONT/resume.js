import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Nav from '../dynnav'
import M from 'materialize-css'
import Education from './RESUME_CONTENT/education'
import Internship from './RESUME_CONTENT/internship'
import Skill from './RESUME_CONTENT/skills'
import Personal from './RESUME_CONTENT/personal'
import Project from './RESUME_CONTENT/project'
import Position from './RESUME_CONTENT/position'
import Training from './RESUME_CONTENT/training'
import SocialAccount from './RESUME_CONTENT/social_account'
import Job from './RESUME_CONTENT/job'

export default class Resume extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      username:'',
      isChecked:false,
      topic:'',
      resume_subject:[{id:1,title:'Personal Info'},{id:2,title:'Education'},{id:3,title:'Skills'},
      {id:4,title:'Internships'},{id:5,title:'Projects'},{id:6,title:'Position of Responsibility'},
      {id:7,title:'Trainings or Certificates'},{id:8,title:'Social Media Account(like Linkdin)'},{id:9,title:'Jobs or Experiences'}
      ,{id:10,title:'Additional Details'}],
      display:'',
      logout:'/user2/slogout',
      get:'/user2/getstudent',
      home:'/student',
      nav_route:'/user2/fetch_snav',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleChecked =(e,index)=>{
      this.setState({
          isChecked: !this.state.isChecked,
          topic: e.target.value
      });
  }


    componentDidMount() {
        M.AutoInit();
        let elems = document.querySelectorAll(".modal-trigger");
        M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });


        axios.get('/user2/getstudent',
        this.setState({loading: true})
      )
         .then(response =>{
           this.setState({loading: false})
           if(response.data.user)
           {
             this.setState({username: response.data.user.username})
           }
           else{
             this.setState({
               redirectTo:'/student',
             });
             window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
           }
         })

    }
    render(){
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return(
          <React.Fragment>
            <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
                  <div className="row">
                     <div className="col l2">
                       <div style={{textAlign: 'center',background: 'linear-gradient(to right bottom, #2f3441 50%, #212531 50%)',marginTop:'4px',fontSize: '20px',position: 'relative',width: '100%',display: 'inline-block'}}>
                         <div className="white-text" style={{height: '65px',position: 'relative',paddingTop: '1em'}}>RECORDS</div>
                       </div>
                       <ul>
                       {this.state.resume_subject.map((content,index)=>(
                          <li key={index} style={{border:'2px solid black'}}>
                               <p>
                               <label>
                               <input type='radio' id="radio-1" name='myRadio' value={content.title} onChange={(e)=>{this.handleChecked(e,index)}}/>
                               <span style={{color:'black'}}>{content.title}</span>
                               </label>
                               </p>
                           </li>
                       ))}
                          </ul>
                     </div>
                     <div className="col l10">
                       <ShowContent topic={this.state.topic} username={this.state.username}/>
                     </div>
                  </div>
          </React.Fragment>
        );
      }
    }
}

class ShowContent extends Component{
  constructor()
  {
    super()
  }

  render()
  {
    let items;
    if(this.props.topic ==="Personal Info")
    {
      items=
      <div><Personal username={this.props.username}/></div>
    }
    else if(this.props.topic ==="Education")
    {
      items =
      <div><Education  username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Skills")
    {
      items =
      <div><Skill username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Internships")
    {
      items =
      <div><Internship username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Projects")
    {
      items =
      <div><Project username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Position of Responsibility")
    {
      items =
      <div><Position username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Social Media Account(like Linkdin)")
    {
      items =
      <div><SocialAccount username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Trainings or Certificates")
    {
      items =
      <div><Training username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Jobs & Experiences")
    {
      items =
      <div><Job username={this.props.username} /></div>
    }
    else{
      items=
      <div></div>
    }
    return(
      <React.Fragment>
      {items}
      </React.Fragment>
    )
  }
}
