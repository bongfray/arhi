import React from 'react';
import axios from 'axios'

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user2/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        if(this.props.oneEntry)
        {
          this.props.oneEntry({
            disabled:'',
          })
        }
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user2/fetchall',{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    if(response.data.length>0)
    {
      if(this.props.oneEntry)
      {
        this.props.oneEntry({
          disabled:'disabled',
        })
      }
    }
    this.setState({
     products: response.data,
   })
  })
}



  render() {
    const { products} = this.state;
      return(
        <React.Fragment>
          <h5 className="center">{this.props.title}</h5><br />
          {this.props.description && <label>{this.props.description}</label>}
          <div className="row">
          {products.map((product,index) => (
            <div className="col l6 card hoverable">
               <div className="row">
                  <div className="col l4">
                    {this.props.data.fielddata.map((content,index)=>(
                      <React.Fragment>
                      <div key={index}><b>{content.header}</b></div><br />
                      </React.Fragment>
                    ))}
                  </div>
                  <div className="col l8">
                  <div className={'right '+this.props.data.button_grid}>
                    <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                    &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                    {this.props.data.fielddata.map((content,ind)=>(
                      <React.Fragment key={ind}>
                        {(content.name === 'link' || content.name === 'blog_link' || content.name === 'playstore_link'
                      ||content.name === 'github_link' || content.name === 'hackerrank_link' || content.name === 'hackerearth_link' ||
                      content.name === 'linkedln_link' || content.name === 'other_link') ?
                         <a href={product[content.name]} target="blank">{product[content.name]}</a>
                         :
                         <div>{product[content.name]}</div>
                        }
                        <br />
                      </React.Fragment>
                    ))}
                  </div>
               </div>
            </div>
          ))}
          </div>
        </React.Fragment>
      )

  }
}
