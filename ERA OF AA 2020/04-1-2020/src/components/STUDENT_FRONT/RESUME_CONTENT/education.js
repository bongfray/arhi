import React, { Component } from 'react'
import axios from 'axios'
import { Select } from 'react-materialize'



 export default class SelectTor extends Component {
   constructor(){
     super()
     this.state={
       degree:'',
     }
   }
   handleInput =(e)=>{
     this.setState({degree:e.target.value})
   }

  render() {
    return (
      <React.Fragment>
        <div className="row">
            <Select name="skill_level" value={this.state.degree} onChange={this.handleInput}>
                <option value="" disabled selected>Level</option>
                <option value="10th Standard">10th Standard</option>
                <option value="12th Standard">12th Standard</option>
                <option value="Graduation">Graduation</option>
                <option value="Post Graduation">Post Graduation</option>
                <option value="PhD">PhD</option>
                <option value="Diploma">Diploma</option>
             </Select>
           </div>
           <br />
           <div className="row">
            {this.state.degree &&  <Gg  degree={this.state.degree} username={this.props.username} /> }
           </div>

      </React.Fragment>
    );
  }
}


 class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:'',

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/user2/editt';
     } else {
       apiUrl = '/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  oneEntry = (object)=>{
    this.setState(object);
  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

   render() {
     console.log(this.state)
     let productForm,title,description;
     var data;
             title ='Education Details';
             data = {
               Action:this.props.degree,
               button_grid:'col l1 m1 s1 center',
               fielddata: [
                 {
                   header: "Status",
                   name: "status_of_education",
                   placeholder: "radio",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Collage/Board",
                   name: "collage_or_board",
                   placeholder: "Enter your Collage or Board Name",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Start Year",
                   name: "start_year",
                   placeholder: "Enter the Start Year",
                   type: "number",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "End Year",
                   name: "end_year",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter the End Year"
                 },
                 {
                   header: "Degree",
                   name: "degree",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Degree(like B.Tech)"
                 },
                 {
                   header: "Stream",
                   name: "stream",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Stream(like Software Engieneer)"
                 },
                 {
                   header: "Percentage/CGPA",
                   name: "cgpa_percentage",
                   type: "number",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Percentage/CGPA"
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} username={this.props.username} action={this.props.degree} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div className="card hoverable">
  <div>
    <div>
  <div>
    {!this.state.isAddProduct && <ProductList  username={this.props.username} title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 left">
  </div>
     <div className="col l6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>
  </div>
</div>
  </div>
);
}
}


 class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      status:'',
      start_year:'',
      end_year:'',
      collage_or_board:'',
      cgpa_percentage:'',
      stream:'',
      degree:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      if((!this.state.collage_or_board) || (!this.state.status) || (!this.state.cgpa_percentage) || (!this.state.end_year)){
        window.M.toast({html: 'Enter all the fields!!',classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
      }
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleChecked =(e)=>{
      this.setState({
          status:e
      });
      if(e === 'Pursuing'){
        this.setState({end_year:e})
      }
  }

  stateSet=(object)=>{
    this.setState(object);
  }
  status =(stat)=>{
    if(this.state.status === stat){
      return "checked";
    }
    else{
      return "";
    }
  }


  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS TO {this.props.data.Action}</h5>
}

    return(
      <div>
      <div className="center">{pageTitle}</div>
      <br />
          {this.props.data.fielddata.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1"/>
          <div className="col l2 m2 s2">
            {this.props.data.Action==='10th Standard' ?
                <div>
                {(content.name === 'degree' || content.name==='stream' || content.name==='start_year') ?
                  <div></div>
                  :
                  <div>
                     {content.header}
                  </div>
                }
                </div>
                :
                <div>{content.header}</div>
            }

          </div>
            <div className="col l8 s8 m8" key={index}>
            <React.Fragment>
               {content.placeholder === "radio" ?
               <React.Fragment>
               <div className="row">
                   <div className="col l6">
                       <p>
                       <label>
                       <input type='radio' id="radio-1" name='myRadio1' checked={this.status('Pursuing')} value={this.state.status} onChange={()=>{this.handleChecked('Pursuing')}}/>
                       <span style={{color:'black'}}>Pursuing</span>
                       </label>
                       </p>
                   </div>
                    <div className="col l6">
                         <p>
                         <label>
                         <input type='radio' id="radio-2" name='myRadio2' checked={this.status('Completed')} value={this.state.status} onChange={(e)=>{this.handleChecked('Completed')}}/>
                         <span style={{color:'black'}}>Completed</span>
                         </label>
                         </p>
                     </div>
                   </div>
               </React.Fragment>
                :
                 <React.Fragment>
                 {content.name === 'end_year' ?
                   <div>
                    {this.state.status === "Pursuing" ?
                    <div>
                        <input
                          className=""
                          type={content.type}
                          placeholder=" "
                          min="10"
                          max="60"
                          name={content.name}
                          value="Pursuing"
                          disabled
                        />
                        <label>{content.placeholder}</label>
                    </div>
                       :
                       <div className="input-field">
                           <input
                             className=""
                             type={content.type}
                             placeholder=" "
                             min="10"
                             id={content.name}
                             max="60"
                             name={content.name}
                             value={this.state[content.name]}
                             onChange={e => this.handleD(e, index)}
                           />
                           <label htmlFor={content.name}>{content.placeholder}</label>
                       </div>
                     }
                     </div>
                     :
                     <div>
                     {this.props.data.Action==='10th Standard' ?
                           <div>
                           {(content.name === 'degree' || content.name==='stream' || content.name==='start_year') ?
                             <div></div>
                             :
                             <div className="input-field">
                                 <input
                                   className=""
                                   type={content.type}
                                   id={content.name}
                                   placeholder=" "
                                   min="10"
                                   max="60"
                                   name={content.name}
                                   value={this.state[content.name]}
                                   onChange={e => this.handleD(e, index)}
                                 />
                                 <label htmlFor={content.name}>{content.placeholder}</label>
                             </div>
                           }
                           </div>
                           :
                           <div className="input-field">
                               <input
                                 className=""
                                 type={content.type}
                                 id={content.name}
                                 placeholder=" "
                                 min="10"
                                 max="60"
                                 name={content.name}
                                 value={this.state[content.name]}
                                 onChange={e => this.handleD(e, index)}
                               />
                               <label htmlFor={content.name}>{content.placeholder}</label>
                           </div>
                    }
                    </div>
                   }
                 </React.Fragment>
               }
            </React.Fragment>

            </div>

            <div className="col l1" />
          </div>
          ))}

          <div className="row">
            <div className="col l9 xl9 hide-on-mid-and-down" />
            <div className="col l3 xl3 s12">
              <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
              <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
            </div>
          </div>

      </div>
    )
  }
}


 class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.data.Action !== this.props.data.Action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user2/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user2/fetchall',{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}



  render() {
    const {  products} = this.state;
      return(
        <React.Fragment>
          <h5 className="center">{this.props.title}</h5><br />
          <div className="row" style={{marginLeft:'20px'}}>
              {products.map((product,index) => (
                <div className="col l4 card" key={index}>
                  <div className="row">
                    <div className="col l10">
                             <div>Degree : {product.action}</div><br />
                             <div>Collage Name : {product.collage_or_board}</div><br />
                             <div>Duration: {product.start_year} - {product.end_year}</div><br />
                             <div>Degree : {product.degree}</div><br />
                             <div>Stream: {product.stream}</div><br />
                             <div>Percentage/CGPA: {product.cgpa_percentage}</div><br />
                    </div>
                    <div className="col l2">
                    <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                    &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                    </div>
                  </div>
                </div>
              ))}
            </div>
        </React.Fragment>
      )

  }
}
