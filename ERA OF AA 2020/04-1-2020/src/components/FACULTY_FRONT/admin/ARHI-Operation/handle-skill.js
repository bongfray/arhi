import React, { Component } from 'react';
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import { Select } from 'react-materialize'
import AddProduct from './add';
import ProductList from './prod'
var empty = require('is-empty');

export default class SkillSetting extends Component {
  constructor()
  {
    super()
    this.state={
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/user/'
    )
      .then(response =>{
        if(response.data.user)
        {
          this.setState({username:response.data.user.username})
        }
        else{
          this.setState({
            redirectTo:'/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }

  render() {
    let target;
      if(this.props.select === 'add-skill')
      {
        target = <AddSkill noChange={this.props.noChange} username={this.state.username}/>
      }
      else if(this.props.select === 'set-task')
      {
        target = <SetTaskSkill noChange={this.props.noChange} username={this.state.username} />
      }
      else{
        target = <div></div>
      }
    return (
      <div>
         {target}
      </div>
    );
  }
}




class SetTaskSkill extends Component {

    constructor(props)
    {
      super(props)
      this.state ={
        username:'',
        redirectTo:'',
        option:'',
        skill_name:'',
        saved_skill:[],
      }
      this.componentDidMount = this.componentDidMount.bind(this)
    }

  handleOption = (e) =>{
    this.setState({option: e.target.value})
  }
  handleLevel = (e) =>{
    this.setState({skill_name: e})
  }

  componentDidMount(){
    this.fetchSavedSkills()
  }

  fetchSavedSkills =() =>{
    axios.post('/user2/fetch_in_admin',{action:'Skill-For-Student'})
    .then(res => {
        if(res.data)
        {
          this.setState({saved_skill:res.data})
        }
    });
  }

  retriveSkill =(e)=>{
    this.setState({skill_name:e.target.value})
  }


    render()
    {
      var libraries = this.state.saved_skill,
      searchString = this.state.skill_name.trim().toLowerCase();
      if(searchString.length > 0)
      {
        libraries = libraries.filter(function(i) {
          return i.skill_name.toLowerCase().match( searchString );
        });
      }

      let datas;
      if(this.state.saved_skill.length !==0)
      {
         datas =
         <React.Fragment>
            <div className="input-field">
              <input type="text" id="search" name="skill_name" value={this.state.skill_name} onChange={this.retriveSkill}/>
              <label htmlFor="search">Type the Skill Name</label>
            </div>
            {this.state.skill_name && <div className="row" >
                  {libraries.map((content,index)=>{
                    return(
                      <div key={index} className="col l3 xl3 m4 s6" onClick={()=>{this.handleLevel(content.skill_name)}}>{content.skill_name}</div>
                    )
                  })}
             </div>
           }
          </React.Fragment>
      }
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
      return(
        <React.Fragment>
        <div className="row">
                <div className="col l6 xl6 m12 s12 card">
                   <div className="row">
                      <div className="center pink white-text">
                       Select Skills
                      </div>
                   </div>
                   <div>
                       {datas}
                   </div>
                </div>

                <div className="col l6 xl6 s12 m12 card">
                        <div className="center pink white-text">Select Level</div>
                        <div className="row">
                          <div className="col l12 xl12 s12 m12">
                            <Select name="title" xl="12"l="12" s="12" m="12" value={this.state.option} onChange={this.handleOption}>
                            <option value="" disabled defaultValue>Select Here...</option>
                            <option value="Begineer">Begineer</option>
                            <option value="Intermediate">Intermediate</option>
                            <option value="Advanced">Advanced</option>
                            </Select>
                          </div>
                      </div>
                </div>

          </div>
          <div className="row">
            <AddTask noChange={this.props.noChange} level={this.state.option} skill_name={this.state.skill_name} username={this.props.username}/>
          </div>
        </React.Fragment>

      )
    }
    }
}


class AddTask extends Component{
    constructor(props) {
       super(props);
       this.state = {
         redirectTo: null,
         username:'',
         isAddProduct: false,
         response: {},
         product: {},
         isEditProduct: false,
         action:this.props.options,
       }
       this.onFormSubmit = this.onFormSubmit.bind(this);
     }


     onCreate = (e,index) => {
       this.setState({ isAddProduct: true,product: {}});
     }

     onFormSubmit(data) {
       let apiUrl;
       var addroute,editroute;
         addroute="/user2/add_from_insert_in_admin";
         editroute = "/user2/edit_inserted_data"
       if(!(data))
       {
         window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
         return false;
       }
       else
       {
       if(this.state.isEditProduct){
         apiUrl = editroute;
       } else {
         apiUrl = addroute;
       }
       axios.post(apiUrl, {data})
           .then(response => {
             this.setState({
               response: response.data,
               isAddProduct: false,
               isEditProduct: false
             })
           })
      }
     }

     editProduct = (productId,index)=> {
       var editProd;
         editProd ="/user2/fetch_for_edit"
       axios.post(editProd,{
         id: productId,
       })
           .then(response => {
             this.setState({
               product: response.data,
               isEditProduct: true,
               isAddProduct: true,
             });
           })

    }

    updateState = () =>{
      this.setState({
        isAddProduct:false,
        isEditProduct:false,
      })
    }


     render() {
       let productForm;

              var  data1 = {
                 fielddata: [
                   {
                     header: "Description of the task",
                     name: "description_of_the_task",
                     placeholder: "Enter the Description of the task",
                     type: "text",
                     grid: 2,
                     div:"col l6 s3 m3 xl6 center",
                   },
                   {
                     header: "Any Reference To Follow",
                     name: "ref_for_task",
                     placeholder: "Enter Any Reference To Follow",
                     type: "text",
                     grid: 2,
                     div:"col l3 s3 m3 xl3 center",
                   }
                 ],
               };
               if(this.state.isAddProduct || this.state.isEditProduct) {
               productForm = <AddProduct cancel={this.updateState} username={this.props.username} action={'Task '+this.props.skill_name} level={this.props.level}  data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
               }
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
  return (
    <React.Fragment>
    <div>
      {!this.state.isAddProduct && <ProductList username={this.props.username} noChange={this.props.noChange} action={'Task '+this.props.skill_name} level={this.props.level}  data={data1}  editProduct={this.editProduct}/>}
      {!this.state.isAddProduct &&
       <React.Fragment>
       <div className="row">
       <div className="col l6 m6 s6 left" />
       <div className="col l6 m6 s6">
         {(this.props.skill_name && this.props.level) && <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button> }
         </div>
      </div>
      </React.Fragment>
    }
      { productForm }
    </div>
  </React.Fragment>
  );
  }
  }
}



class AddSkill extends Component {
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
       addroute="/user2/add_from_insert_in_admin";
       editroute = "/user2/edit_inserted_data"
     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/user2/fetch_for_edit"


     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Skill Name",
                   name: "skill_name",
                   placeholder: "Enter the Name of the Skills",
                   type: "text",
                   grid: 2,
                   div:"col l8 xl8 s5 m5 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <AddProduct username={this.props.username} cancel={this.updateState}  action="Skill-For-Student" data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} noChange={this.props.noChange} action="Skill-For-Student" data={data1}  editProduct={this.editProduct}/>}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</React.Fragment>
);
}
}
}
