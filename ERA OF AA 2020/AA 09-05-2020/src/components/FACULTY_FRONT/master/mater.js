import React, { Component} from 'react';
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import Status from './status'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


export default class Master extends Component {
	constructor() {
    super();
    this.state = {
			loading: true,
			saved_dayorder:0,
			datas: [
				{
					day_order:'Day Order 1',dayor:'1',
					day:[
						{id:"1"},{id:"2"},{id:"3"},{id:"4"},{id:"5"},{id:"6"},{id:"7"},{id:"8"},{id:"9"},{id:"10"}
					]

				},
				{
					day_order:'Day Order 2',dayor:'2',
					day:[
						{id:"1"},{id:"2"},{id:"3"},{id:"4"},{id:"5"},{id:"6"},{id:"7"},{id:"8"},{id:"9"},{id:"10"}					]
				},
				{
					day_order:'Day Order 3',dayor:'3',
					day:[
						{id:"1"},{id:"2"},{id:"3"},{id:"4"},{id:"5"},{id:"6"},{id:"7"},{id:"8"},{id:"9"},{id:"10"}					]
				},
				{
					day_order:'Day Order 4',dayor:'4',
					day:[
						{id:"1"},{id:"2"},{id:"3"},{id:"4"},{id:"5"},{id:"6"},{id:"7"},{id:"8"},{id:"9"},{id:"10"}					]
				},
				{
					day_order:'Day Order 5',dayor:'5',
					day:[
						{id:"1"},{id:"2"},{id:"3"},{id:"4"},{id:"5"},{id:"6"},{id:"7"},{id:"8"},{id:"9"},{id:"10"}					]
				}
			],
			week:'',
			home:'/ework/faculty',
			logout:'/ework/user/logout',
			get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
			noti_route:true,
    };

		this.componentDidMount = this.componentDidMount.bind(this);
    }



    fetchlogin = () =>{
      axios.get('/ework/user/'
	)
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
						loading:false,
          });
        }
				else{
					this.setState({loading:false})
				}
      })
    }

    componentDidMount() {
        this.fetchlogin();
    }


render() {
	const MyLoader = () => (
		<ContentLoader
		  height={160}
		  width={400}
		  speed={2}
		  primaryColor="#f3f3f3"
		  secondaryColor="#c0c0c0"
		>
		  <rect x="24" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="35" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="55" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="75" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="95" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="115" rx="0" ry="0" width="38" height="7" />

		</ContentLoader>
	  )
	if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
	return (
		<React.Fragment>
		<Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>


		<TableContainer component={Paper}>
		 <Table aria-label="simple table">
		 <TableHead>
			 <TableRow>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">Hour /Day Order</TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>1</b></span>
					 <br />08:00 - 08:50
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>2</b></span>
					 <br />08:50 - 09:40
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>3</b></span>
					 <br />09:45 - 10:35
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>4</b></span>
					 <br />10:40 - 11:30
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>5</b></span>
					 <br />11:35 - 12:25
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>6</b></span>
					 <br />12:30 - 01:20
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>7</b></span>
					 <br />01:25 - 02:15
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>8</b></span>
					 <br />02:20 - 03:10
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>9</b></span>
					 <br />03:15 - 04:05
				 </TableCell>
				 <TableCell  style={{borderRight: '1px solid'}} align="center">
					 <span style={{color:'red'}}><b>10</b></span>
					 <br />04:05 - 04:55
				 </TableCell>
			 </TableRow>
		 </TableHead>
			 <TableBody>
				 {this.state.datas.map((content,index)=> (
					 <TableRow key={index}>
 				 			<Stat day_order={this.state.saved_dayorder} content={content} dayor={content.dayor} />
					 </TableRow>
				 ))}
			 </TableBody>
		 </Table>
	 </TableContainer>
</React.Fragment>
);
}
}
}
}


class Stat extends Component{
	render()
	{
		return(
			<React.Fragment>

			<TableCell  style={{borderRight: '1px solid'}}  align="center">
				 {this.props.content.day_order}
			</TableCell>
				 {this.props.content.day.map((content,ind)=>{
						 return(
							 <TableCell style={{borderRight: '1px solid'}} align="center"
							  key={content.id}>
							 		<Status day_order={this.props.dayor} slott={content.id} />
							 </TableCell>
						 );
					 })}

			</React.Fragment>
		);
	}
}
