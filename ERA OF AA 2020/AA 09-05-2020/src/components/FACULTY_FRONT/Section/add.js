import React from 'react';
import TextField from '@material-ui/core/TextField';
import {Button,Dialog,DialogActions,DialogTitle,DialogContent} from '@material-ui/core';



class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      datafrom:'',
      Action:'',
      role:'',
      description:'',
      date_of_starting:'',
      date_of_complete:'',
      achivements:'',
      verified:false,
      date:0,
      month:0,
      year:0,
      verification_link:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  var date = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  this.setState({
    action:this.props.action,
    username: this.props.username,
    datafrom:'Section',
    verified:false,
    date:date,
    month:month,
    year:year,
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = "EDIT DATAS";
} else {
  pageTitle ="ADD DATAS";
}
    return(
      <Dialog
        open={true}
        fullWidth
        onClose={this.props.cancel}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title" align="center">{pageTitle}</DialogTitle>
      <DialogContent >

              {this.props.data.fielddata.map((content,index)=>(
                <React.Fragment key={index}>
                 {content.type === 'date' ?
                   <TextField
                     id={content.name}
                     label={content.placeholder}
                     fullWidth
                     type={content.type}
                     name={content.name}
                     value={this.state[content.name]}
                     onChange={e => this.handleD(e, index)}
                     variant="outlined"
                   />
                  :
                    <TextField
                      id={content.name}
                      label={content.placeholder}
                      fullWidth
                      multiline
                      type={content.type}
                      name={content.name}
                      value={this.state[content.name]}
                      onChange={e => this.handleD(e, index)}
                      variant="outlined"
                    />
                 }
                  <br /><br />
                </React.Fragment>
              ))}

            </DialogContent>
          <DialogActions>
          <Button  variant="outlined" color="secondary" onClick={this.props.cancel} >CANCEL</Button>
          <Button variant="contained" color="primary" type="submit" onClick={this.handleSubmit}>
            UPLOAD
          </Button>
        </DialogActions>

      </Dialog>
    )
  }
}

export default AddProduct;
