import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {Backdrop,Grid} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {InputBase,Divider,IconButton} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InfoIcon from '@material-ui/icons/Info';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            snack_open:false,
            snack_msg:'',
            alert_type:'',
            suspended:[],
            fetching:true,
            value_for_search:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchSuspension()
    }
    componentDidUpdate=(prevProps)=>{
      if(prevProps.select !== this.props.select)
      {
        this.fetchSuspension();
      }
    }
    fetchSuspension=()=>{
      let url;
      if(this.props.select === 'student')
      {
        url= '/ework/user2/fetch_suspension';
      }
      else {
        url = '/ework/user/fetch_suspension';
      }
      axios.get(url)
      .then( res => {
          this.setState({suspended:res.data,fetching:false})
      })
      .catch( err => {
          this.setState({fetching:false})
      });
    }
    searchEngine =(e)=>{
      this.setState({value_for_search:e.target.value})
    }
    RemoveSuspension = (username)=>{
      if(!username)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter all the details !!',
          alert_type:'warning',
        });
      }
      else
      {
      this.setState({fetching:true})
        let route;
        if(this.props.select === "faculty")
        {
          route ="/ework/user/suspension_user"
        }
        else if(this.props.select === "student")
        {
          route ="/ework/user2/suspension_suser"
        }
        var state={
          suspend_username:username,
        }
        axios.post(route,{state:state,status:false,user_session:this.props.user})
        .then(res=>{
          if(res.data === 'no')
          {
            this.setState({
              snack_open:true,
              snack_msg:res.data,
              alert_type:'error',
            });
          }
          else {
            this.setState({
              snack_open:true,
              snack_msg:res.data,
              alert_type:'success',
            });
          }
           var  { suspended} = this.state;

          this.setState({
            fetching:false,
            suspended: suspended.filter(product => product.username !== username),
          })
        })
     }
    }
    removeEvery=()=>{
      this.setState({fetching:true})
      axios.post('/ework/user2/removeallsuspension')
      .then( res => {
        this.setState({
        snack_open:true,
        snack_msg:'Done',
        alert_type:'success',fetching:false})
      });
    }
    render(){
      if(this.state.fetching)
      {
        return(
          <Backdrop  open={true} style={{zIndex:'2040'}}>
            <CircularProgress style={{color:'yellow'}}/>&emsp;
            <div style={{color:'yellow'}}>Processing Your Request...</div>
          </Backdrop>
        )
      }
      else {
        if(this.state.suspended.length === 0)
        {
          return(
            <Typography variant="h5" color="secondary" align="center">No Request Found !!</Typography>
          )
        }
        else {
          var libraries = this.state.suspended,
          searchString = this.state.value_for_search.trim().toLowerCase();
          libraries = libraries.filter(function(i) {
            return i.username.toLowerCase().match( searchString );
          });

          return(
            <React.Fragment>

            {this.state.open_suspended &&
              <Dialog
                open={true}
                fullWidth
                keepMounted
                onClose={()=>this.setState({open_suspended:false})}
                 aria-labelledby="scroll-dialog-title"
                 aria-describedby="scroll-dialog-description"
               >
                 <DialogTitle id="scroll-dialog-title" align="center">Unblocked By</DialogTitle>
                 <DialogContent >
                   <DialogContentText
                     id="scroll-dialog-description"
                     tabIndex={-1}
                   >
                   <div style={{padding:'10px'}}>
                       <Grid container>
                         <Grid item xs={3} sm={3} style={{textAlign:'center',color:'red'}}>Suspended By</Grid>
                         <Grid item xs={3} sm={3} style={{textAlign:'center',color:'red'}}>Suspended Date</Grid>
                         <Grid item xs={3} sm={3} style={{textAlign:'center',color:'red'}}>Suspension Removed By</Grid>
                         <Grid item xs={3} sm={3} style={{textAlign:'center',color:'red'}}>Suspension Removed Date</Grid>
                       </Grid>
                       <hr />
                       <Grid container>
                           <Grid item xs={6} sm={6}>
                             {this.state.modal_data.suspended_by.length>0 &&
                                <React.Fragment>
                                  {this.state.modal_data.suspended_by.map((content,index)=>{
                                    return(
                                      <Grid container key={index}>
                                          <Grid item xs={6} sm={6} style={{textAlign:'center',color:'black'}}>{content.user}</Grid>
                                          <Grid item xs={6} sm={6} style={{textAlign:'center',color:'black'}}>{content.time}</Grid>
                                      </Grid>
                                    )
                                  })}
                                </React.Fragment>
                             }
                           </Grid>
                           <Grid item xs={6} sm={6}>
                             {this.state.modal_data.remove_suspension_by.length>0 &&
                                <React.Fragment>
                                  {this.state.modal_data.remove_suspension_by.map((content,index)=>{
                                    return(
                                      <Grid container key={index}>
                                          <Grid item xs={6} sm={6} style={{textAlign:'center',color:'black'}}>{content.user}</Grid>
                                          <Grid item xs={6} sm={6} style={{textAlign:'center',color:'black'}}>{content.time}</Grid>
                                      </Grid>
                                    )
                                  })}
                                </React.Fragment>
                             }
                           </Grid>
                       </Grid>
                   </div>
                   </DialogContentText>
                 </DialogContent>
                 <DialogActions>
                   <Button onClick={()=>this.setState({open_suspended:false})} color="primary">
                     CLOSE
                   </Button>
                 </DialogActions>
               </Dialog>
            }

            {this.state.open_reason &&
              <Dialog
                open={true}
                fullWidth
                keepMounted
                onClose={()=>this.setState({open_reason:false})}
                 aria-labelledby="scroll-dialog-title"
                 aria-describedby="scroll-dialog-description"
               >
                 <DialogTitle id="scroll-dialog-title" align="center">Suspension Reasons</DialogTitle>
                 <DialogContent >
                   <DialogContentText
                     id="scroll-dialog-description"
                     tabIndex={-1}
                   >
                   <div style={{padding:'10px'}}>
                       <Grid container>
                         <Grid item xs={6} sm={6} style={{textAlign:'center',color:'red'}}>Suspended Reason</Grid>
                         <Grid item xs={6} sm={6} style={{textAlign:'center',color:'red'}}>Suspended Date</Grid>
                       </Grid>
                       <hr />
                       <Grid container>
                             {this.state.reason_data.length>0 &&
                                <React.Fragment>
                                  {this.state.reason_data.map((content,index)=>{
                                    return(
                                      <Grid container key={index}>
                                          <Grid item xs={6} sm={6} style={{textAlign:'center',color:'black'}}>{content.reason}</Grid>
                                          <Grid item xs={6} sm={6} style={{textAlign:'center',color:'black'}}>{content.time}</Grid>
                                      </Grid>
                                    )
                                  })}
                                </React.Fragment>
                             }
                          </Grid>
                   </div>
                   </DialogContentText>
                 </DialogContent>
                 <DialogActions>
                   <Button onClick={()=>this.setState({open_reason:false})} color="primary">
                     CLOSE
                   </Button>
                 </DialogActions>
               </Dialog>
            }


            <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            open={this.state.snack_open} autoHideDuration={2000}
            onClose={()=>this.setState({snack_open:false})}>
              <Alert onClose={()=>this.setState({snack_open:false})}
              severity={this.state.alert_type}>
                {this.state.snack_msg}
              </Alert>
            </Snackbar>

            <Grid container spacing={1} style={{padding:'20px 0px 0px 0px'}}>
               <Grid item xs={12} sm={12} xl={8} lg={8} md={6}>
                  <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
               </Grid>
               <Grid item xs={12} sm={12} xl={4} lg={4} md={6}>
                 {this.props.select === 'student' &&
                     <Button variant="contained" style={{float:'right'}}
                     color="secondary" onClick={this.removeEvery}>Remove All Suspension</Button>
                  }
               </Grid>
              </Grid>
             <br />
             <TableContainer component={Paper}>
               <Table aria-label="simple table">
                 <TableHead>
                   <TableRow>
                     <TableCell align="center"><b>Official Id</b></TableCell>
                     <TableCell align="center"><b>Department- Campus</b></TableCell>
                     {(this.props.select === 'faculty') && <TableCell align="center"><b>Designation</b></TableCell> }
                     {(this.props.select === 'student') && <TableCell align="center"><b>Degree</b></TableCell> }
                     {(this.props.select === 'student') && <TableCell align="center"><b>Year - Sem -Batch</b></TableCell> }
                     <TableCell align="center"><b>Suspension Status</b></TableCell>
                     <TableCell align="center"><b>Suspension Reason</b></TableCell>
                     <TableCell align="center"><b>Suspended By</b></TableCell>
                     <TableCell align="center"><b>Action</b></TableCell>
                   </TableRow>
                 </TableHead>
                 <TableBody>
                   {libraries.map((row,index)=> (
                     <TableRow key={index}>
                       <TableCell align="center">{row.username}</TableCell>
                       <TableCell align="center">{row.dept} - {row.campus}</TableCell>
                       {(this.props.select === 'faculty') && <TableCell align="center">{row.desgn}</TableCell> }
                       {(this.props.select === 'student') && <TableCell align="center">{row.degree}</TableCell> }
                       {(this.props.select === 'student') && <TableCell align="center">{row.year} / {row.sem} /{row.batch}</TableCell>}
                       <TableCell align="center">
                         {row.suspension_status ?
                           <Typography color="secondary">Suspended</Typography>
                           :
                           <Typography style={{color:'green'}}>Open</Typography>
                         }
                       </TableCell>
                       <TableCell align="center">
                         <IconButton onClick={()=>this.setState({open_reason:true,reason_data:row.suspend_reason})}>
                           <InfoIcon />
                         </IconButton>
                       </TableCell>
                         <TableCell align="center">
                           <IconButton onClick={()=>this.setState({open_suspended:true,modal_data:row})}>
                             <InfoIcon />
                           </IconButton>
                         </TableCell>
                       <TableCell align="center">
                          <Button variant="contained" color="primary"
                           disabled={this.state.disabled} onClick={()=>this.RemoveSuspension(row.username)}>Remove Suspension</Button>
                       </TableCell>
                     </TableRow>
                   ))}
                 </TableBody>
               </Table>
             </TableContainer>
            </React.Fragment>
          );
        }
      }
    }
}



const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search Only Official Id" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
