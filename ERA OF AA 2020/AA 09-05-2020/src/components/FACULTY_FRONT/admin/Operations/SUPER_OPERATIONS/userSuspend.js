import React, { Component } from 'react'
import InputLabel from '@material-ui/core/InputLabel';
import {FormControl,Grid,Hidden} from '@material-ui/core';
import {Backdrop} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import axios from 'axios';

import Validate from '../../validateUser'


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'none',
            modal:false,
            disabled:false,
            allowed:false,
            suspend_username:'',
            suspend_reason:'',
            current_action:'',
            snack_open:false,
            snack_msg:'',
            alert_type:'',
            fetching:false,
        }
    }

    componentDidUpdate =(prevProps,prevState) => {
      if (prevState.allowed !== this.state.allowed) {
        if(this.state.current_action === "remove_sespension")
        {
          this.RemoveSuspension();
        }
        else
        {
          this.Suspend();
        }
      }
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }

    handleUsername = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    Suspend = (e)=>{
      if(!this.state.suspend_username || !this.state.suspend_reason)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter all the details !!',
          alert_type:'warning',
        });
      }
      else
      {
        this.setState({success:'block',disabled:true,current_action:'suspend_user'})
        if(this.state.allowed === true)
        {
          this.setState({success:'none',disabled:''})
          let route;
          if(this.props.select === "suspendfac")
          {
            route ="/ework/user/suspension_user"
          }
          else if(this.props.select === "suspendstu")
          {
            route ="/ework/user2/suspension_suser"
          }
          this.handleSuspension(route,true);
        }
      }
    }
    handleSuspension = (route,status) =>{
      this.setState({fetching:true})
      axios.post(route,{state:this.state,status:status,user_session:this.props.user})
      .then(res=>{
        if(res.data === 'no')
        {
          this.setState({
            snack_open:true,
            snack_msg:res.data,
            alert_type:'error',
          });
        }
        else {
          this.setState({
            snack_open:true,
            snack_msg:res.data,
            alert_type:'success',
          });
        }
        this.setState({suspend_username:'',suspend_reason:'',fetching:false})
      })
    }


    render(){

      let suspend_content;
      if(this.props.select === 'suspendfac' || this.props.select === 'suspendstu'){
            suspend_content=
            <Grid container spacing={1}>
            <Hidden xsDown><Grid item sm={2}/></Hidden>
            <Grid item xs={12} sm={8}>
                <Paper style={{padding:'40px'}}>
                <Typography variant="h6" align="center">{"SUSPEND FACULTY OR STUDENT"}</Typography>
                <br />
                   <Grid container spacing={1}>
                      <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-amount">Official Id</InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-amount1"
                              type="text" name="suspend_username"
                              value={this.state.suspend_username} onChange={this.handleUsername}
                              startAdornment={<InputAdornment position="start">Username</InputAdornment>}
                              labelWidth={70}
                            />
                          </FormControl>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-amount">Give any Specific Reason</InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-amount2"
                              type="text" name="suspend_reason" value={this.state.suspend_reason} onChange={this.handleUsername}
                              startAdornment={<InputAdornment position="start">Reason</InputAdornment>}
                              labelWidth={150}
                            />
                          </FormControl>
                      </Grid>
                   </Grid>
                   <br />
                     <div style={{float:'right'}}>
                        <Button variant="contained" color="secondary"
                         disabled={this.state.disabled} onClick={this.Suspend}>Suspend User</Button>
                     </div>
                   </Paper>
                </Grid>
                <Hidden xsDown><Grid item sm={2}/></Hidden>
              </Grid>
        }
        else{
            suspend_content=
                <div></div>
        }
        if(this.state.fetching)
        {
          return(
            <Backdrop  open={true} style={{zIndex:'2040'}}>
              <CircularProgress style={{color:'yellow'}}/>&emsp;
              <div style={{color:'yellow'}}>Processing Your Request...</div>
            </Backdrop>
          )
        }
        else {
        return(
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          {this.state.success ===  'block' &&
            <Validate showDiv={this.showDiv}/>
          }
            {suspend_content}
          </React.Fragment>
        );
       }
    }
}
