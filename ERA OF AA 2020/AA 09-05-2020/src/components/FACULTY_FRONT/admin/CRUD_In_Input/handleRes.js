import React, { Component } from 'react'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {Grid,Paper} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import AddProduct from './add';
import ProductList from './prod';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {Backdrop,CircularProgress} from '@material-ui/core';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class Responsibility extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      redirectTo:'',
      option:'',
      level:'',
      designation:[],
      submit:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleLevel = (e) =>{
  this.setState({[e.target.name]:e.target.value})
}

componentDidMount(){
  this.fetchDesig()
}

fetchDesig =() =>{
  axios.post('/ework/user/fetch_in_admin',{action:'Designation'})
  .then(res => {
      if(res.data)
      {
        this.setState({designation:res.data})
      }
  });
}


  render()
  {
    let datas;
    if(this.state.designation.length !==0)
    {
       datas =
       <FormControl style={{width:'100%'}}>
          <InputLabel id="desgn">Designation</InputLabel>
          <Select
            labelId="desgn"
            id="desgn"
            name="level"
            value={this.state.level} onChange={this.handleLevel}
          >
          {this.state.designation.map((content,index)=>{
                  return(
                    <MenuItem  key={index} value={content.designation_name}>{content.designation_name}</MenuItem>
                  )
            })}
          </Select>
        </FormControl>
    }

    return(
      <React.Fragment>
      <Paper variant="outlined" square style={{padding:'20px'}}>
      <Grid container spacing={1}>
          <Grid container item xs={12} sm={12} lg={6} xl={6} md={6}>
                   <Grid item xs={12} sm={12} lg={6} xl={6} md={6} style={{marginTop:'15px',color:'red'}}>
                     <b>Select Responsibility Category</b>
                   </Grid>
                   <Grid  item xs={12} sm={12} lg={6} xl={6} md={6}>
                      <FormControl style={{width:'100%'}}>
                         <InputLabel id="catagory">Category</InputLabel>
                         <Select
                           labelId="catagory"
                           id="catagory"
                           name="option"
                           value={this.state.option} onChange={this.handleLevel}
                         >
                         <MenuItem value="Administrative">Administrative</MenuItem>
                         <MenuItem value="Academic">Academic</MenuItem>
                         <MenuItem value="Research">Research</MenuItem>
                         </Select>
                       </FormControl>
                  </Grid>
          </Grid>
           <Grid container item  xs={12} sm={12} lg={6} xl={6} md={6}>
                 <Grid  item  xs={12} sm={12} lg={6} xl={6} md={6} style={{marginTop:'15px',color:'red'}}>
                   <b>Select Designation - </b>
                 </Grid>
                <Grid  item  xs={12} sm={12} lg={6} xl={6} md={6}>
                    {datas}
                </Grid>
           </Grid>
        </Grid>
        </Paper>
        {this.state.option && this.state.level &&
            <Gg options={this.state.option} level={this.state.level} username={this.props.user.username}/>
        }
      </React.Fragment>
    )
  }
}

 class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       snack_open:false,
       alert_type:'',
       snack_msg:'',
     }
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
       addroute="/ework/user/add_from_insert_in_admin";
       editroute = "/ework/user/edit_inserted_data"
       if(!(data.responsibilty_title) || !(data.responsibilty_percentage))
       {
         this.setState({
           snack_open:true,
           snack_msg:'Enter All the Details !!',
           alert_type:'warning',
           isAddProduct:false,
           isEditProduct:false,
         });
         return false;
       }
       else
       {
       if(this.state.isEditProduct){
         apiUrl = editroute;
       } else {
         apiUrl = addroute;
       }
       this.setState({submit:true})

       axios.post(apiUrl, {data})
           .then(response => {
             this.setState({
               response: response.data,
               isAddProduct: false,
               isEditProduct: false,
               submit:false,
             })
           })
      }
   }

   editProduct = (productId,index)=> {
     this.setState({submit:true})
     var editProd;
       editProd ="/ework/user/fetch_for_edit"
     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
             submit:false,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }


   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Responsibility Title",
                   name: "responsibilty_title",
                   placeholder: "Enter the Responsibility",
                   type: "text",
                   grid: 2,
                   div:"col l5 s5 m5 xl5 center",
                 },
                 {
                   header: "Alloted Hour/Week",
                   name: "responsibilty_percentage",
                   placeholder: "Enter the Alloted Hour/Week",
                   type: "number",
                   grid: 2,
                   div:"col l5 s4 m5 xl5 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct)
             {
                productForm =
                <AddProduct cancel={this.updateState} username={this.props.username}
                action={this.props.options} level={this.props.level}  data={data1}
                onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
             if(this.state.submit)
             {
               return(
                 <Backdrop  open={true} style={{zIndex:'2040'}}>
                   <CircularProgress style={{color:'yellow'}}/>&emsp;
                   <div style={{color:'yellow'}}>Processing Your Request.....</div>
                 </Backdrop>
               )
             }
             else{
        return (
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

            {!this.state.isAddProduct &&
              <ProductList action={this.props.options} level={this.props.level}
              data={data1}  editProduct={this.editProduct}/>
            }
            {!this.state.isAddProduct &&
              <Grid container spacing={1}>
                <Grid item xs={6} sm={6}/>
                  <Grid item xs={6} sm={6}>
                   {(this.props.options && this.props.level) &&
                     <div style={{float:'right',padding:'10px'}}>
                       <Button variant="contained" color="secondary"
                        onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</Button>
                      </div>
                    }
                 </Grid>
              </Grid>

          }
            { productForm }
        </React.Fragment>
        );
      }
   }
}
