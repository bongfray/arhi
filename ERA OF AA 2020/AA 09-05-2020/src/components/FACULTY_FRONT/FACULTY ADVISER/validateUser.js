import React, { Component } from 'react';
import axios from 'axios'
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import {Paper,Switch} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import MuiAlert from '@material-ui/lab/Alert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
 import {Snackbar,Typography,FormControlLabel,Grid} from '@material-ui/core';


 function Alert(props) {
   return <MuiAlert elevation={6} variant="filled" {...props} />;
 }

export default class ValidateUser extends Component{
  constructor()
  {
    super()
    this.state={
      fetching:true,
      requests:[],
      recheck:[],
      notfound:'',
      isChecked:false,
      username:'',
      faculty_ad:false,
      year_set:false,
      disable_active:true,
      for_year:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchRequests()
  }
  fetchRequests = ()=>{
    axios.post('/ework/user2/fetch_signup_request_for_admin',{user:this.props.user})
    .then(res=>{
        this.setState({requests:res.data,fetching:false})
    })
  }

  render()
  {
    if(this.state.fetching)
    {
      return(
        <Backdrop open={true} style={{zIndex:'2024'}}>
          <CircularProgress style={{color:'yellow'}} />&emsp;
          <div style={{color:'yellow',textAlign:'center'}}>Loading....</div>
        </Backdrop>
      )
    }
    else {
      if(this.state.requests.length === 0)
      {
        return(
          <Grid container>
            <Grid item xs={12} sm={12}>
                <Typography align="center" variant="h6">No Request Found !!</Typography>
            </Grid>
          </Grid>
        )
      }
      else {

      }
        return(
          <React.Fragment>
            <Grid container>
              <Grid item xs={12} sm={12}>
                  <Typography align="center" variant="h6">You Will able to see the requests only of your department and campus</Typography>
              </Grid>
            </Grid>

            <EnhancedTable request={this.state.requests} />
          </React.Fragment>
        )
    }
  }
}




function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}



const headCells = [
  { id: 'Reg ID', numeric: false, disablePadding: false, label: 'Reg ID' },
  { id: 'Mail Id', numeric: true, disablePadding: false, label: 'Mail Id' },
  { id: 'Sem-Batch', numeric: true, disablePadding: false, label: 'Sem-Batch' },
  { id: 'Faculty Advisor', numeric: true, disablePadding: false, label: 'Faculty Advisor' },
  { id: 'Action', numeric: true, disablePadding: true, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>
               {(((headCell.label === 'Information') || (headCell.label === 'Action')) && (props.incoming_action===false)) ?
                 <React.Fragment></React.Fragment>
                   :
                 <React.Fragment>{headCell.label}</React.Fragment>
               }
              </b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [data,setData] = React.useState({
    snack_open:false,
    snack_msg:'',
    alert_type:'',
  });
  const [checked,setChecked] = React.useState({
    isChecked:false,
    active:'',
  });
  const [submit,setSubmit] = React.useState(false)

  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };


  const activeStudent=(e,index,content)=>{
    setData({
      snack_open:true,
      snack_msg:'Activating !!.. Sending Activation Mail.... ',
      alert_type:'info',
    })

    setChecked({
      isChecked:true,
      active:content.username
    });
    setSubmit(false)

        axios.post('/ework/user2/active_user',{
          content: content,
        })
        .then(res=>{
          setSubmit(false)
          setData({
            snack_open:true,
            snack_msg:'Activated !!',
            alert_type:'success',
          })
        })
  }

  props.request.filter(product => product.username !== checked.active)

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.request.length - page * rowsPerPage);


  if(submit)
  {
    return(
      <Backdrop open={true} style={{zIndex:'2024'}}>
        <CircularProgress style={{color:'yellow'}} />&emsp;
        <div style={{color:'yellow',textAlign:'center'}}>Sending Your Data....</div>
      </Backdrop>
    )
  }
  else
  {
  return (
    <div className={classes.root}>

    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    open={data.snack_open} autoHideDuration={2000}
    onClose={()=>setData({snack_open:false})}>
      <Alert onClose={()=>setData({snack_open:false})}
      severity={data.alert_type}>
        {data.snack_msg}
      </Alert>
    </Snackbar>

      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              incoming_action={props.admin_action}
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.request.length}
            />
            <TableBody>
              {stableSort(props.request, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row._id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                    >
                     <TableCell>
                     {row.username}
                     </TableCell>
                     <TableCell align="center">
                       {row.mailid}
                      </TableCell>

                      <TableCell align="center">
                        {row.sem}-{row.batch}
                      </TableCell>
                      <TableCell align="center">
                        {row.faculty_adviser_id}
                      </TableCell>
                      <TableCell align="center">
                        <FormControlLabel
                          control={<Switch checked={checked.isChecked && checked.active === row.username} onChange={(e)=>{activeStudent(e,index,row)}} name="Active" />}
                          label="Active"
                        />
                       </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.request.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
 }
}
