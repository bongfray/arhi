import React, { Component,Fragment } from 'react'
import axios from 'axios'
import Status from '../../FACULTY_FRONT/Manage/bluePrintReq';
import Nav from '../../dynnav'
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

export default class ManageOne extends Component {
  constructor(props)
  {
    super(props)
    this.initialState ={
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      noti_route:false,
      loading:true,
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    if(this.props.prop_data)
    {
      this.setState({username:this.props.prop_data.data.username,loading:false})
    }
    else
    {
      axios.get('/ework/user2/getstudent',
    )
       .then(response =>{
         if(response.data.user)
         {
           this.setState({user:response.data.user,username: response.data.user.username})
         }
         else{
           this.setState({
             redirectTo:'/ework/student',
           });
         }
          this.setState({loading: false})
       })
     }
  }


  render()
  {
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      )
    }
    else{
      return(
        <Fragment>
        <Nav noti_route={this.state.noti_route} home={this.state.home} login={this.state.login} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
        <div style={{marginTop:'20px'}}>
          <Status student={true} user={this.state.user} username={this.state.username} />
        </div>
        </Fragment>
      )
    }

  }
}
