import React from 'react';
import ReactApexChart from 'react-apexcharts'
import Axios from 'axios';
import {Grid,Hidden} from '@material-ui/core';

class Chart extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
          resume_data:'',
        series: [{
          data: []
        }],
        options: {
          chart: {
            type: 'bar',
            height: 350
          },
          plotOptions: {
            bar: {
              vertical: true,
            }
          },
          dataLabels: {
            enabled: false
          },
          xaxis: {
            categories: [],
          }
        },


      };
      this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        Axios.post('/ework/user2/fetchall',{
            action:'Skills',
            username:this.props.prop_data.data.username,
        })
        .then(res=>{
            //console.log(res.data)
            if(res.data)
            {
                var skill_n=[];
                var rating = [];
                var skills = res.data.filter(function(item){
                   skill_n.push(item.skill_name)
                   rating.push(parseFloat(item.skill_rating))
                })
                let a = this.state.series.slice();
                a[0].data = rating;
                this.setState({options:{xaxis:{categories:skill_n}},series:a})
            }
        })
    }

    render() {
      return (
            <React.Fragment>
                <Grid container spacing={1}>
                  <Hidden xsDown><Grid item sm={3}/></Hidden>
                    <Grid item sm={6} xs={12} className="chart">
                        <ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={350} />
                    </Grid>
                    <Hidden xsDown><Grid item sm={3}/></Hidden>
                </Grid>

            </React.Fragment>
    );
}
}

export default Chart;
