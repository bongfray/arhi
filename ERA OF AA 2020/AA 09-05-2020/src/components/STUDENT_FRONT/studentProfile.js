import React, { Component } from 'react'
import axios from 'axios'
import {Redirect } from 'react-router-dom';
import SaveIcon from '@material-ui/icons/Save';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import {Snackbar,TextField} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import Nav from '../dynnav'
import ContentLoader from "react-content-loader"



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class ActualDash extends Component {

 constructor(props) {
   super(props);
   this.state = {
     id:'',
     up_username:'',
     up_name:'',
     up_phone:'',
     campus:'',
     dept:'',
     desgn:'',
     up_dob: '',
     new_password:'',
     sem:'',
     current_password:'',
     up_confirm_password:'',
     home:'/ework/student',
     logout:'/ework/user2/slogout',
     get:'/ework/user2/getstudent',
     nav_route:'/ework/user2/fetch_snav',
     noti_route:false,
     loading:true,
     redirectTo:null,
     snack_open:false,
     snack_msg:'',
     alert_type:'',
   }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.handleSub = this.handleSub.bind(this)
 }
 componentDidMount() {
   axios.get('/ework/user2/getstudentprofile').then(response => {
     if(response.status === 200)
     {
       if(response.data.nologin)
       {
         this.setState({redirectTo:'/ework/student',loading:false})
       }
       else
       {
       this.setState({
         loading:false,
         id: response.data.username,
         up_username: response.data.mailid,
         up_name: response.data.name,
         up_phone: response.data.phone,
         dept: response.data.dept,
         campus: response.data.campus,
         degree: response.data.degree,
         up_dob: response.data.dob,
         sem:response.data.sem,
       })
     }
     }
   })
}
handleSub(event) {
 event.preventDefault()
 if((this.state.up_confirm_password) || (this.state.new_password) || (this.state.current_password))
 {
   if((this.state.up_confirm_password) && (this.state.new_password) && (this.state.current_password))
   {
     if(!(this.state.new_password.match(/[a-z]/g) && this.state.new_password.match(
               /[A-Z]/g) && this.state.new_password.match(
               /[0-9]/g) && this.state.new_password.match(
               /[^a-zA-Z\d]/g) && this.state.new_password.length >= 6))
     {
       this.setState({
         snack_open:true,
         snack_msg:'Wrong Format of New Password !!',
         alert_type:'warning',
       })
     }
     else if(this.state.up_confirm_password === this.state.new_password)
     {
       if((this.state.up_phone).length!==10){
         this.setState({
           snack_open:true,
           snack_msg:'Phone no is not of 10 digit !!',
           alert_type:'warning',
           sending:'SUBMIT',
         });
       }
       else{
  axios.post('/ework/user2/sprofile', {
    id: this.state.id,
    up_username: this.state.up_username,
    up_name: this.state.up_name,
    up_phone: this.state.up_phone,
    up_dob: this.state.up_dob,
    new_password: this.state.new_password,
    current_password: this.state.current_password,
  })
   .then(response => {
     //console.log(response)
     if(response.status===200){
       if(response.data.succ)
        {
          this.setState({
            snack_open:true,
            snack_msg:response.data.succ,
            alert_type:'success',
            sending:'SUBMIT',
          });
       }
       else if(response.data.fail)
       {
         this.setState({
           snack_open:true,
           snack_msg:'Kindly Match Your Current Password !!',
           alert_type:'warning',
           sending:'SUBMIT',
         });
         return false;
       }
     }
    }).catch(error => {
      this.setState({
        snack_open:true,
        snack_msg:'Something Went Wrong !!',
        alert_type:'error',
        sending:'SUBMIT',
      });
    })
   this.setState({
     up_name: this.state.up_name,
     up_username: this.state.up_username,
     up_phone: this.state.up_phone,
     campus: this.state.campus,
     dept: this.state.dept,
     up_dob:this.state.up_dob,
     degree: this.state.degree,
     up_confirm_password: '',
     current_password: '',
     new_password: '',
     })
   }
   }else{
     this.setState({
       errors:'New Password Does not Match !!',
       up_confirm_password: '',
       new_password: '',
       sending:'SUBMIT',
       })
     return false;

   }
 }
   else{
     this.setState({
       snack_open:true,
       snack_msg:'Fill all the Details !!',
       alert_type:'warning',
     });
     return false;
   }
 }
 else
 {
   if((this.state.up_phone).length!==10){
     this.setState({
       snack_open:true,
       snack_msg:'Updated phone no is not of 10 digit!',
       alert_type:'warning',
       sending:'SUBMIT',
     });
   }
   else{
   axios.post('/ework/user2/sprofile', {
     up_username: this.state.up_username,
     up_name: this.state.up_name,
     up_phone: this.state.up_phone,
     up_dob: this.state.up_dob,
     new_password: this.state.new_password,
     up_confirm_password: this.state.up_confirm_password,
     current_password: this.state.current_password
   })
    .then(response => {
      //console.log(response)
      if(response.status===200){
        if(response.data.succ)
         {
           this.setState({
             snack_open:true,
             snack_msg:response.data.succ,
             alert_type:'success',
             sending:'SUBMIT',
           });
        }
      }
     }).catch(error => {
       this.setState({
         snack_open:true,
         snack_msg:'Something Went Wrong !!',
         alert_type:'error',
         sending:'SUBMIT',
       });
     })
    this.setState({
     up_name: this.state.up_name,
     id: this.state.id,
     up_username: this.state.up_username,
     up_phone: this.state.up_phone,
     campus: this.state.campus,
     dept: this.state.dept,
     up_dob:this.state.up_dob,
     degree: this.state.degree,
     up_confirm_password: '',
     current_password: '',
     new_password: '',
      })
    }
}
}
handleChange= (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };


 render() {
   const MyLoader = () => (
     <ContentLoader
       height={160}
       width={400}
       speed={2}
       primaryColor="#f3f3f3"
       secondaryColor="#c0c0c0"
     >
       <rect x="170" y="30" rx="3" ry="3" width="70" height="10" />
       <rect x="10" y="60" rx="3" ry="3" width="70" height="10" />
       <rect x="90" y="60" rx="3" ry="3" width="90" height="10" />
       <rect x="220" y="60" rx="3" ry="3" width="70" height="10" />
       <rect x="300" y="60" rx="3" ry="3" width="90" height="10" />

       <rect x="10" y="90" rx="3" ry="3" width="70" height="10" />
       <rect x="90" y="90" rx="3" ry="3" width="90" height="10" />
       <rect x="220" y="90" rx="3" ry="3" width="70" height="10" />
       <rect x="300" y="90" rx="3" ry="3" width="90" height="10" />

       <rect x="10" y="120" rx="3" ry="3" width="70" height="10" />
       <rect x="90" y="120" rx="3" ry="3" width="90" height="10" />
       <rect x="220" y="120" rx="3" ry="3" width="70" height="10" />
       <rect x="300" y="120" rx="3" ry="3" width="90" height="10" />

     </ContentLoader>
   )
   if(this.state.loading)
   {
     return(
       <MyLoader />
     );
   }
   else if(!this.state.loading)
   {
     if (this.state.redirectTo) {
          return <Redirect to={{ pathname: this.state.redirectTo }} />
      } else {
       return (
         <React.Fragment>

         <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
         open={this.state.snack_open} autoHideDuration={2000}
         onClose={()=>this.setState({snack_open:false})}>
           <Alert onClose={()=>this.setState({snack_open:false})}
           severity={this.state.alert_type}>
             {this.state.snack_msg}
           </Alert>
         </Snackbar>

         <Nav noti_route={this.state.noti_route} home={this.state.home}
         nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
         <br />
         <Paper variant="outlined" square style={{padding:'20px',margin:'8px 15px 8px 15px'}}>
                 <div className="">
                   <Typography variant="h5" align="center" style={{fontFamily: 'Orbitron',
                   paddingTop: '10px',height: '50px'}}>
                       Profile Datas
                    </Typography>
                 <br /><br />
                   <div>
                    {this.renderCells()}
                   </div>
                 </div>
         </Paper>
         </React.Fragment>
         );
     }
   }
 }

 renderCells() {
   return(
     <div className="profile">
        <Grid container spacing={3}>
              <Grid container item xs={12} sm={6}>
                      <Grid item xs={6} sm={4}>
                        <Typography variant="button" color="secondary" display="block" gutterBottom>
                          Official ID
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={8}>
                        <Cell value={ this.state.id } />
                      </Grid>
              </Grid>
             <Grid container item xs={12} sm={6}>
                   <Grid item xs={4} sm={4}>
                     <Typography variant="button" color="secondary" display="block" gutterBottom>
                       Official Mail
                     </Typography>
                   </Grid>
                   <Grid item xs={8} sm={8}>
                     <Cell value={ this.state.up_username } />
                   </Grid>
            </Grid>
         </Grid>


       <br />

       <Grid container spacing={3}>
               <Grid container item xs={12} sm={6}>
                     <Grid item xs={6} sm={4}>
                       <Typography variant="button" color="secondary" display="block" gutterBottom>
                         Name
                       </Typography>
                     </Grid>
                     <Grid item xs={6} sm={8} >
                       <Cell value={ this.state.up_name } name="name"
                       onChange={value => this.setState({up_name: value})} />
                     </Grid>
               </Grid>
                <Grid container item xs={12} sm={6}>
                      <Grid item xs={6} sm={4}>
                        <Typography variant="button" color="secondary" display="block" gutterBottom>
                          Mobile No
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={8}>
                        <Cell value={ this.state.up_phone } name="phone" mention={true}
                        onChange={value => this.setState({up_phone: value})} />
                      </Grid>
               </Grid>
          </Grid>


        <br />


        <Grid container spacing={3}>
            <Grid container item xs={12} sm={6}>
              <Grid item xs={6} sm={4}>
                <Typography variant="button" color="secondary" display="block" gutterBottom>
                  Campus
                </Typography>
              </Grid>
              <Grid item xs={6} sm={8}>
                 <Cell value={ this.state.campus } />
              </Grid>
            </Grid>
             <Grid container item xs={12} sm={6}>
                 <Grid item xs={6} sm={4}>
                   <Typography variant="button" color="secondary" display="block" gutterBottom>
                     Degree
                   </Typography>
                 </Grid>
                 <Grid item xs={6} sm={8}>
                   <Cell value={ this.state.degree }/>
                 </Grid>
            </Grid>
         </Grid>

         <br />

         <Grid container spacing={3}>
             <Grid container item xs={12} sm={6}>
               <Grid item xs={6} sm={4}>
                 <Typography variant="button" color="secondary" display="block" gutterBottom>
                   Department
                 </Typography>
               </Grid>
               <Grid item xs={6} sm={8}>
                  <Cell value={ this.state.dept }  />
               </Grid>
             </Grid>
              <Grid container item xs={12} sm={6}>
                  <Grid item xs={6} sm={4}>
                    <Typography variant="button" color="secondary" display="block" gutterBottom>
                      Current Semester
                    </Typography>
                  </Grid>
                  <Grid item xs={6} sm={8}>
                    <Cell value={ this.state.sem }/>
                  </Grid>
             </Grid>
          </Grid>


          <br />

          <Typography variant="h6" style={{marginLeft:'10px'}} color="primary" gutterBottom>
            RESET YOUR PASSWORD -
          </Typography>
          <br />
         <Grid container spacing={3}>
              <Grid item xs={12} sm={4}>
                 <TextField fullWidth id="curpass" type="password" value={this.state.current_password} name="current_password"
                  onChange={this.handleChange} label="Current Password" variant="outlined" />
              </Grid>
              <Grid item xs={12} sm={4}>
                <TextField fullWidth id="newpass" type="password" name="new_password"
                value={this.state.new_password} onChange={this.handleChange} label="New Password" variant="outlined" />
              </Grid>
             <Grid item xs={12} sm={4}>
                 <TextField fullWidth id="conpass" type="password" name="up_confirm_password" value={this.state.up_confirm_password}
                  onChange={this.handleChange} label="Confirm New Password" variant="outlined" />
             </Grid>
         </Grid>
         <Grid container spacing={3}>
           <Grid item xs={6} sm={9} />
           <Grid item xs={6} sm={3} >
               <Button onClick={this.handleSub} fullWidth variant="contained" color="secondary">
                 SUBMIT
               </Button>
           </Grid>
         </Grid>
     </div>
   );

 }
}

class Cell extends Component {

 constructor(props) {
   super(props);
   this.state = { editing: false };
 }

 render() {
   const { value, onChange } = this.props;
   if(this.state.editing)
   {
     return(
       <Grid container spacing={1}>
          <Grid item xs={10} sm={6}>
            <TextField fullWidth ref='input' type={this.props.mention ? 'number' : 'text'} value={value} onChange={e => onChange(e.target.value)} variant="outlined" />
          </Grid>
            <IconButton aria-label="save"  color="secondary" onClick={ e => this.onBlur()}>
              <SaveIcon fontSize="inherit" />
            </IconButton>
       </Grid>
     );
   }
   else{
     if(this.props.name)
     {
     return(
       <Grid container>
         <Grid item xs={8} sm={8}>
           {value}
         </Grid>
         <Grid item xs={4} sm={4}>
           <IconButton aria-label="edit" color="primary" onClick={() => this.onFocus()}>
             <EditIcon fontSize="inherit" />
           </IconButton>
         </Grid>
       </Grid>
     );
   }
   else
   {
     return(
       <Grid container spacing={1}>
         <Grid item xs={8} sm={12}>
           {value}
         </Grid>
       </Grid>
     );
   }
   }


 }

 onFocus() {
   this.setState({ editing: true }, () => this.refs.input.focus());
 }

 onBlur() {
   this.setState({ editing: false });
 }
}
