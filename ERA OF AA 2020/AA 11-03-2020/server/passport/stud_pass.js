const passport = require('passport')
const LocalStrategy = require('./studentpass')
const Suser = require('../database/models/FAC/user')

passport.serializeUser((user, done) => {
	done(null, { _id: user._id })
})


passport.deserializeUser((id, done) => {
	Suser.findOne(
		{ _id: id },
		['username','sem','year','batch','dept','campus','faculty_adviser_id'],
		(err, user) => {
			done(null, user)
		}
	)
})

//  Use Strategies
passport.use(LocalStrategy)

module.exports = passport
