import React, { } from 'react';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import Slide from '@material-ui/core/Slide';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ListItem from '@material-ui/core/ListItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';




const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

let count=0;
let visibilityChange = null;
if (typeof document.hidden !== 'undefined') {
  visibilityChange = 'visibilitychange';
} else if (typeof document.msHidden !== 'undefined') {
  visibilityChange = 'msvisibilitychange';
} else if (typeof document.webkitHidden !== 'undefined') {
  visibilityChange = 'webkitvisibilitychange';
}


export default class Instructions extends React.Component {
  constructor()
  {
    super()
    this.state={
        start_modal:true,
        test_window:false,
      }

  }

  openTest=()=>{
    this.setState({test_window:true})
    this.fullMe()
  }
  fullMe=()=>{
    document.body.requestFullscreen();
  }

  exitMe=()=>{
    window.document.exitFullscreen()
  }

  closeInstruction=()=>{
    this.props.stateSet({
      question_window:false
    })
    this.setState({test_window:false})
  }




  render() {

    return (
      <React.Fragment>
      {this.props.state.question_window &&
        <Dialog
          open={this.props.state.question_window}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title" style={{textAlign:'center'}}>Instructions</DialogTitle>
          <DialogContent>
            1.  In order to add a skill, first we will verify you.<br/>
            2.  After clicking the agree button below we will reidirect you to full screen page.<br/>
            3.  Please remember if you exit from that window, the particular skill that you are going to add,
               will be blocked forever for you .<br/>
            4.  Pressing any other buttons and switching to another tab will also be treated as a violation to
              our policy and that skill again will be blocked for you.<br/>
            5.  You have to answer 5 correct question out of 10.<br />
            6.  If you give 6 wrong answer, we would not add your skill to you resume.<br />
            7.  But don't worry. You have multiple chance to take the screening test.<br />
            8.  As usual, you have to answer within some particular time, that will be initiated
               after the test start.<br />
            9.  Remember one more thing, questions will be displayed one by one.You can
              move to other question by answering the questions in sequencial manner.<br />
            10.  Once you answer a question, you can't go back to that question.

          </DialogContent>
          <DialogActions>
            <Button onClick={this.openTest} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      }

      {this.state.test_window &&
        <Questions state_data={this.state} props_data={this.props.state} exitMe={this.exitMe}
          closeInstruction={this.closeInstruction}
         stateSet={this.props.stateSet} formSubmit={this.props.formSubmit} cancel={this.props.cancel}
         />
      }
      </React.Fragment>

    )
  }
}

   var alertTimerId;
 class Questions extends React.Component {

  constructor()
  {
    super()
    this.state={
        actions:false,
        start_modal:true,
        fetching:true,
        answer:'',
        expansion_id:0,
        isExpanded:true,
        correct:0,
        total:10,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
  }


  escFunction=(event)=>{
    if(event.keyCode === 27) {
      //console.log('oooppss!!')
      this.props.stateSet({
        question_window:false,
      });
      this.props.cancel();
    }
  }

  componentDidMount() {
        document.addEventListener(visibilityChange, this.handleVisibilityChange, false);
        document.addEventListener("keydown", this.escFunction, false);
        this.fetchQuestions();
  }

  componentDidUpdate=(prevProps)=>
  {
    if(prevProps !== this.props)
    {
      this.fetchQuestions()
    }
  }

  fetchQuestions=()=>{
    let timer;
    axios.post('/ework/user2/fetch_in_admin_for_path',{
      action: "add-questions",
      usertype: this.props.props_data.skill_level,
      for:'QS '+this.props.props_data.skill_name,
    })
    .then( res => {
        if(res.data)
        {
          if(res.data.length<10)
          {
            this.props.closeInstruction();
            this.props.exitMe()
            window.M.toast({html: 'Sorry, questions not available !!',classes:'rounded red'});
          }
          else
          {
            this.setState({questions:res.data})
          }
          this.setState({fetching:false})

        }
        if(this.props.props_data.skill_level=== 'Begineer')
        {
          timer =4*60000;
        }
        else if (this.props.props_data.skill_level ==='Intermediate') {
          timer =6*60000;
        }
        else {
          timer =9*60000;
        }
        alertTimerId = setTimeout(this.submitResult,timer);
    });
  }

  handleVisibilityChange = () => {
     this.setState({actions:true,test_window:false});
  }

  componentWillUnmount()    {
    document.removeEventListener(visibilityChange, this.handleVisibilityChange);
    document.removeEventListener("keydown", this.escFunction, false);
  }

  setAnswer=(e,answer,index)=>{
    this.setState({answer:e.target.value})
    this.verifyCorrectness(e,answer,index)
  }

  verifyCorrectness=(e,answer,index)=>{
    if(e.target.value === answer)
    {
      count++;
    }
    if(index === 9)
    {
      this.submitResult();
    }
    else {
        this.setState({expansion_id:index+1,correct:count,answer:''})
    }

  }

  submitResult=()=>{
    if(count>=5)
    {
      clearTimeout(alertTimerId );
      this.props.exitMe();
      this.props.formSubmit(this.props.props_data);
      this.props.stateSet({
        question_window:false,
      });
    }
    else
    {
      clearTimeout(alertTimerId );
      this.props.exitMe();
      this.props.stateSet({
        question_window:false,
      });
      this.props.cancel();
      window.M.toast({html: 'Sorry , you can not able to make it !!',classes:'rounded red'});
    }
    count = 0;
  }


  render() {
    if(this.state.fetching)
    {
      return(
        <div style={{float:'center'}}>
        <CircularProgress />
        </div>
      )

    }
    else
    {
      if(this.state.questions.length>=10)
      {
      let n =10;
      var result = new Array(n),
          len = this.state.questions.length,
          taken = new Array(len);
        while (n--)
        {
            var x = Math.floor(Math.random() * len);
            result[n] = this.state.questions[x in taken ? taken[x] : x];
            taken[x] = --len in taken ? taken[len] : len;
        }
        let timer;
        if(this.props.props_data.skill_level=== 'Begineer')
        {
          timer =4*60;
        }
        else if (this.props.props_data.skill_level ==='Intermediate') {
          timer =6*60;
        }
        else {
          timer =9*60;
        }
    return (
      <React.Fragment>
        <Dialog
        fullScreen
         open={true}
         TransitionComponent={Transition}>
            <List>
              <div style={{marginTop:'40px'}}>
                <div style={{textAlign:'center'}}>
                    <Typography variant="h4">SET OF QUESTIONS</Typography>
                </div>
              <br />
                <Clock seconds={timer} /><br/>
                {result.map((content,index)=>{
                  return(
                    <Grid container spacing={1} key={index}>
                      <Grid item xs={1} sm={1}/>
                       <Grid item xs={10} sm={10}>
                          <ExpansionPanel
                          expanded={this.state.isExpanded  && this.state.expansion_id === index}
                          >
                            <ExpansionPanelSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1a-content"
                              id="panel1a-header"
                              style={{backgroundColor:'#b9f6ca'}}
                            >
                              <Typography>QUESTION {index+1}</Typography>
                            </ExpansionPanelSummary>
                                <List>
                                    <ListItem button>
                                     <b>Q.{index+1} - {content.question}</b>
                                    </ListItem>
                                    <br />
                                    <ListItem>
                                    <FormControl component="fieldset">
                                      <RadioGroup aria-label="gender" name="gender1" value={this.state.answer}
                                      onChange={(e)=>this.setAnswer(e,content.answer,index)}>
                                        <FormControlLabel value={content.option1}
                                        control={<Radio />} style={{color:'black'}} label={content.option1} />
                                        <FormControlLabel value={content.option2}
                                        control={<Radio />}  style={{color:'black'}} label={content.option2} />
                                        <FormControlLabel value={content.option3}
                                        control={<Radio />} style={{color:'black'}} label={content.option3} />
                                        <FormControlLabel value={content.option4}
                                        control={<Radio />} style={{color:'black'}} label={content.option4} />
                                      </RadioGroup>
                                    </FormControl>
                                  </ListItem>
                                </List>
                          </ExpansionPanel>
                          <br />
                        </Grid>
                        <Grid item xs={1} sm={1}/>
                    </Grid>
                  )
                })}
              </div>
            </List>
          </Dialog>

      </React.Fragment>
    )
  }
 }
}
}


class Clock extends React.Component {
  constructor(props){
    super(props)
    this.tick = this.tick.bind(this)
    this.state = {seconds: props.seconds}
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
  }

  tick(){
    if (this.state.seconds > 0) {
      this.setState({seconds: this.state.seconds - 1})
    } else {
      clearInterval(this.timer);
    }
  }
  render(){
    return(
      <Paper className="float-right" style={{color:'red'}} elevation={3}>
        {this.state.seconds} seconds left
      </Paper>
    )
  }
}
