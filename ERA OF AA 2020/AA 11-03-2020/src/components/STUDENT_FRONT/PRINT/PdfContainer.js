import React,{useEffect} from 'react';

export default (props) => {
  const bodyRef = React.createRef();
 /*if(props.loading === false){
   useEffect(() =>
      createPdf(), [])
  }*/
  const createPdf = () => props.createPdf(bodyRef.current);
  return (
    <section className="resume-pdf-container">
      <section className="resume-pdf-toolbar"><center>
        <button onClick={createPdf}>Generate Resume</button></center>
      </section>
      <section className="resume-pdf-body" ref={bodyRef}>
        {props.children}
      </section>
    </section>
  )
}