import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from '../forget'
import axios from 'axios'
import Nav from '../dynnav'
var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redirectTo: null,
            forgotroute:'/ework/user2/sforgo',
            home:'/ework/student',
            logout:'/ework/user2/logout',
            get:'/ework/user2/getstudent',
            nav_route:'/ework/user2/fetch_snav',
            noti_route:false,
            block_click:false,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'Enter all the details !!',classes:'rounded #f44336 red'});
          this.setState({
              cnf_pswd:''
          })
          return false;
        }
        else{
          window.M.toast({html: 'Logging In....',classes:'rounded orange'});
          this.setState({block_click:true})
        axios.post('/ework/user2/slogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                if (response.status === 200)
                {
                  this.setState({
                    redirectTo:'/ework/student'
                  })
                  window.M.toast({html: 'Logged In !!',classes:'rounded green darken-1'});
                  this.setState({block_click:false})
                }
            }).catch(error => {
              this.setState({block_click:false})
              if(error.response.status === 401)
              {
                window.M.toast({html:error.response.data,classes:'rounded #f44336 red'});
              }
              else
              {
                window.M.toast({html: 'Internal Error !!',classes:'rounded #f44336 red'});
              }
            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }



    getUser = () => {
      axios.get('/ework/user2/getstudent').then(response => {
        if (response.data.user) {
            this.setState({redirectTo:'/ework/student'});
            window.M.toast({html: 'Sorry already loggedIn !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
          })
        }



        componentDidMount() {
            this.getUser();
        }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
            return (
              <React.Fragment>
              <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
              <div className="row">

             <div className="col s4">
             </div>

             <div className="col l4 s12 m12 form-signup">
                 <div className="ew center">
                     <Link to ="/ework/student"><h5 className="blue-grey darken-2 highlight">eWORK</h5></Link>
                     <h5 className="highlight black-text">LOG IN</h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="text" type="text" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <span className="helper-text" data-error="Please enter the data" data-success="">Registration Number Only</span>
                         <label htmlFor="text">Username</label>
                     </div>
                     <div className="input-field inline col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <span className="helper-text" data-error="Please enter the data" data-success=""></span>
                         <label htmlFor="password">Password</label>
                     </div>
                     </div>
                     <div className="row">
                     <Link to="/ework/ssignup" className=" btn blue-grey darken-2 col s5 sup">Register</Link>
                     <div className="col s2"></div>
                     <button disabled={this.state.block_click} className=" btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <hr/>
                     <div className="row">
                     <Modal forgot={this.state.forgotroute}/>
                     </div>

                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
             </React.Fragment>
           );
         }

    }
}

export default LoginForm
