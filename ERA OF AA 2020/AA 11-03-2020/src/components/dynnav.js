import React, { Component } from 'react'
import {Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import Slogo from './Slogo.png'
import Notification from './notification'
import ContentLoader from "react-content-loader"
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Badge from '@material-ui/core/Badge';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import MenuIcon from '@material-ui/icons/Menu';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Hidden from '@material-ui/core/Hidden';
import HomeIcon from '@material-ui/icons/Home';
import Grid from '@material-ui/core/Grid';

export default class Navbar extends Component {
  constructor()
  {
    super()
    this.state ={
      redirectTo:'',
      show:'none',
      loading:true,
      loggedIn:false,
      username:'',
      nav:[],
      notidisp:'none',
      newDisplay:'none',
      count_noti:'',
      showSidebar:false,
      user_details:'',
      menu_open:false,
      anchorEl:null,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.logout = this.logout.bind(this)
  }
  getUser = () =>{
    axios.get(this.props.get).then(response => {
      if (response.data.user) {
        if(this.props.nav_route)
        {
          this.fetch();
        }
        this.newNoti();
        this.setState({
          loggedIn: true,
          username: response.data.user.username,
          user_details:response.data.user,
        })
      } else {
        this.setState({
          loading:false,
          loggedIn: false,
          username: null,
        })
      }
    })
  }


  fetch()
  {
    axios.get(this.props.nav_route).then(response =>{
      var nav;
     if(this.state.user_details.count<=2)
     {
       if((this.state.user_details.suprimity === false) && (this.state.user_details.faculty_adviser === false))
       {
           nav = response.data.filter(item => (item.link!=="/ework/faculty_adviser") && (item.link!=="/ework/suprimity_admin") );
       }
       else if((this.state.user_details.suprimity === true) && (this.state.user_details.faculty_adviser === false))
        {
          nav = response.data.filter(item => (item.link!=="/ework/faculty_adviser"));
        }
        else if((this.state.user_details.suprimity === false) && (this.state.user_details.faculty_adviser === true))
         {
           nav = response.data.filter(item => (item.link!=="/ework/suprimity_admin"));
         }
       else {
         nav = response.data;
       }
     }
     else
     {
       nav = response.data;
     }
      this.setState({
       nav: nav,
     })
    })
  }

  componentDidMount = () => {
    this.getUser();
}

newNoti = () =>{
  let fetch_new_notification;
  if(this.props.noti_route)
  {
    fetch_new_notification = '/ework/user/fetch_new_notification';
  }
  else {
    fetch_new_notification = '/ework/user2/fetch_new_notification';
  }

  axios.get(fetch_new_notification)
  .then( res => {
        this.setState({count_noti:res.data.length})
     this.setState({loading:false})
  });
}

renderSlide =() =>{
  this.setState({showSidebar:!this.state.showSidebar})
}


    logout(event) {
        event.preventDefault()
          window.M.toast({html: 'Logging Out....!!',classes:'pink rounded'});
        axios.post(this.props.logout).then(response => {
          if (response.status === 200) {
            this.setState({
              loggedIn: false,
              username: null,
              redirectTo:'/',
            })
            window.M.toast({html: 'Logged Out....!!',classes:'green rounded'});
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }


      notiPush =() =>{
        this.setState({notidisp:'block'})
        let mark_as_read;
        if(this.props.noti_route)
        {
          mark_as_read = '/ework/user/mark_as_read';
        }
        else {
          mark_as_read = '/ework/user2/mark_as_read';
        }
        axios.post(mark_as_read)
        .then( res => {
        });
      }

      setDisp = (object) =>{
        this.setState(object);
      }

      handleExpand =(e) => {
        this.setState({menu_open:!this.state.menu_open,anchorEl:e.currentTarget});
      };
    render() {
      let output
      const MyLoader = () => (
        <ContentLoader
          height={15}
          width={400}
          speed={2}
          primaryColor="#E5E8E8"
          secondaryColor="#F8F9F9"
        >
          <rect x="10" y="3" rx="3" ry="3" width="350" height="4" />
          <rect x="15" y="10" rx="3" ry="3" width="380" height="4" />
        </ContentLoader>
      )
  if(this.state.loading === true)
  {
    output = <MyLoader />
  }
  else
  {
 if(this.state.loggedIn === false)
{
  output=


  <React.Fragment>
             {this.state.showSidebar &&
               <Drawer anchor="left" open={this.state.showSidebar}
               onClose={this.renderSlide}>
                 <IconButton onClick={this.renderSlide}>
                   <ChevronLeftIcon />
                 </IconButton>
                 <Divider />
                   <List>
                   <ListItem button>
                     <Link to={this.props.home} style={{color:'black',textDecoration:'none'}}><ListItemText primary={"Home"} /></Link>
                   </ListItem>
                       {this.state.nav.map((content,index)=>(
                         <ListItem button key={index}>
                          <Link style={{color:'black',textDecoration:'none'}} to={content.link}>
                            <ListItemText primary={content.val} />
                          </Link>

                         </ListItem>
                       ))}
                       <Divider />
                       <ListItem button>
                         <Link to="#" style={{color:'black',textDecoration:'none'}} onClick={this.logout}><ListItemText primary={"LOG OUT"} /></Link>
                       </ListItem>
                   </List>
               </Drawer>
               }

               <AppBar position="fixed" style={{backgroundColor:'#455a64'}}>
                    <Toolbar>
                      <Grid container spacing={1}>
                          <Grid item xs={3} sm={2}>
                             <Hidden xsDown>
                              <img src={Slogo} className="srm"
                               alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/>
                             </Hidden>
                          </Grid>
                              <Hidden smUp>
                                <Grid item xs={6} sm={8}>
                                  <div style={{textAlign:'center',marginTop:'5px',
                                  fontFamily: 'Cinzel',
                                  fontSize: '30px'}}>
                                    SRM CARE
                                  </div>
                                </Grid>
                               </Hidden>
                               <Hidden xsDown>
                                 <Grid item xs={6} sm={8}>
                                   <div style={{textAlign:'center',marginTop:'5px',fontFamily: 'Cinzel',fontSize: '30px'}}>
                                     SRM Centre for Applied Research in Education
                                    </div>
                                 </Grid>
                                </Hidden>

                           <Grid item xs={3} sm={2}/>
                      </Grid>
                    </Toolbar>
                  </AppBar>

<br /><br /><br />
              </React.Fragment>

}
else if(this.state.loggedIn === true)
{
  console.log(this.props.home)
        output=
          <React.Fragment>
                     {this.state.showSidebar &&
                       <Drawer anchor="left" open={this.state.showSidebar}
                       onClose={this.renderSlide}>
                         <IconButton onClick={this.renderSlide}>
                           <ChevronLeftIcon />
                         </IconButton>
                         <Divider />
                           <List>
                           <ListItem button>
                             <Link to={this.props.home} style={{color:'black',textDecoration:'none'}}><ListItemText primary={"Home"} /></Link>
                           </ListItem>
                                {this.props.nav_route &&
                                    <React.Fragment>
                                       {this.state.nav.map((content,index)=>(
                                         <ListItem button key={index}>
                                          <Link style={{color:'black',textDecoration:'none'}} to={content.link}>
                                            <ListItemText primary={content.val} />
                                          </Link>

                                         </ListItem>
                                       ))}
                                     </React.Fragment>
                                 }
                               <Divider />
                               <ListItem button onClick={this.logout}>
                                  <ListItemText primary={"LOG OUT"} />
                               </ListItem>
                           </List>
                       </Drawer>
                       }

                       <AppBar position="fixed" style={{backgroundColor:'#455a64'}}>
                            <Toolbar>
                            <Grid container spacing={0}>
                                     <Hidden smUp>
                                         <Grid item xs={3} sm={4}>
                                               <IconButton onClick={this.renderSlide}
                                                 edge="start"
                                                 color="inherit"
                                                 aria-label="open drawer"
                                               >
                                                 <MenuIcon />
                                               </IconButton>
                                           </Grid>
                                         </Hidden>

                                           <Hidden xsDown>
                                              <Grid item xs={3} sm={4}>
                                                   <img src={Slogo} className="srm"
                                                    alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/>
                                               </Grid>
                                           </Hidden>

                               <Grid item xs={6} sm={4}>
                                   <div style={{textAlign:'center',marginTop:'5px',
                                   fontFamily: 'Cinzel',
                                   fontSize: '30px'}}>
                                     SRM CARE
                                   </div>
                               </Grid>
                               <Grid item xs={3} sm={4}>
                               <div style={{float:'right',paddingTop:'5px'}}>
                                      {this.props.home &&
                                        <Hidden xsDown>
                                           <Link to={this.props.home}><IconButton style={{color:'white'}} aria-label="show 4 new mails">
                                               <HomeIcon />
                                           </IconButton>
                                           </Link>
                                        </Hidden>
                                       }
                                     <Hidden xsDown>
                                       <IconButton onClick={this.logout} aria-label="show 4 new mails" color="inherit">
                                           <ExitToAppIcon />
                                       </IconButton>
                                     </Hidden>
                                     <IconButton onClick={this.notiPush} aria-label="show 17 new notifications" color="inherit">
                                       <Badge badgeContent={this.state.count_noti} color="secondary">
                                           <NotificationsActiveIcon />
                                       </Badge>
                                     </IconButton>
                                      {this.props.nav_route && <Hidden xsDown>
                                           <IconButton aria-label="settings"
                                           onClick={this.handleExpand}>
                                             <MoreVertIcon style={{color:'white'}} />
                                               <Menu
                                                 anchorEl={this.state.anchorEl}
                                                 anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                                                 keepMounted
                                                 transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                                 open={this.state.menu_open}
                                                 onClose={(e)=>this.handleExpand(e)}
                                               >
                                               {this.state.nav.map((content,index)=>{
                                                 return(
                                                     <Link key={index} style={{textDecoration:'none',color:'black'}}
                                                      to={content.link}><MenuItem>{content.val}</MenuItem></Link>
                                                  )
                                                })}
                                               </Menu>
                                           </IconButton>
                                       </Hidden>
                                     }
                                   </div>
                               </Grid>
                            </Grid>
                            </Toolbar>
                          </AppBar>

<br /><br /><br />
                      </React.Fragment>
      }
    }
    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <React.Fragment>
        {this.state.notidisp === 'block' ?
         <Notification setDisp={this.setDisp} noti_route={this.props.noti_route} />
        : <React.Fragment> {output} </React.Fragment>
        }
        </React.Fragment>
      )
    }

    }
}
