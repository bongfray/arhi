import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import FiveYearTable from './fiveyear_table'

export default class Five extends Component {
  constructor() {
    super()
    this.state={
      five_data:[],
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_five_for_view',{datas:this.props.referDatas,action:null,
      username:this.props.username}).then(res => {
      this.setState({
        five_data: res.data,
        loader:false,
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let five_dat;
    if(this.state.loader === true)
    {
      five_dat =
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
    }
    else{
      five_dat =
       <React.Fragment>
       {this.state.five_data.length>0 ?
          <FiveYearTable username={this.props.username} admin_action={this.props.admin_action} five_year_data={this.state.five_data} />
          :
          <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
       }
        </React.Fragment>
    }




    return (
      <React.Fragment>
       {five_dat}
        </React.Fragment>
    );
  }
}
