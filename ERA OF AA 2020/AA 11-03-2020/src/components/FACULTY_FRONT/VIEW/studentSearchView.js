import React, { Component } from 'react';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import ById from './STUDENT DATA/by-id'
import ByDomain from './STUDENT DATA/by-domain'
import BySkill from './STUDENT DATA/by-skill'



export default class StudentData extends Component {
  constructor()
  {
    super()
    this.state={
      radio:[{value:'Search By Reg Id'},{value:'Search By Domain'},
      {value:'Search By Skill'},],
      choosed: '',
      active:'',
      open_sidebar:true,
    }
  }

  closeDrawer=()=>{
    this.setState({open_sidebar:!this.state.open_sidebar})
  }

  handleChecked =(e,index,value)=>{
      this.setState({
          choosed: value
      });
      this.closeDrawer();
  }

  render() {

    return (
      <React.Fragment>
      {!(this.state.open_sidebar) && <IconButton onClick={this.closeDrawer} style={{float:'right',backgroundColor:'#ec407a'}}>
        <ChevronLeftIcon style={{color:'white'}} />
      </IconButton>
       }
        {this.state.open_sidebar &&
          <Drawer style={{width:'550px'}} anchor="right" open={this.state.open_sidebar} onClose={this.closeDrawer}>
            <IconButton onClick={this.closeDrawer}>
              <ChevronRightIcon />
            </IconButton>
            <Divider />
              <List>
                  {this.state.radio.map((content,index)=>(
                    <ListItem button key={index}  onClick={(e)=>{this.handleChecked(e,index,content.value)}}>
                      <ListItemText primary={content.value} />
                    </ListItem>
                  ))}
              </List>
          </Drawer>
        }

           <Navigator user_session={this.props.user_session} choosed={this.state.choosed} />
      </React.Fragment>
    );
  }
}



class Navigator extends Component {
  render() {
    //console.logthis.props.choosed)
    if(this.props.choosed === 'Search By Reg Id')
    {
      return (
        <ById user_session={this.props.user_session} />
      );
    }
    else if (this.props.choosed === 'Search By Skill') {
      return (
        <BySkill user_session={this.props.user_session} />
      );
    }
    else if (this.props.choosed === 'Search By Domain') {
      return (
        <ByDomain user_session={this.props.user_session} />
      );
    }
    else {
      return (
        <div>

        </div>
      );
    }

  }
}
