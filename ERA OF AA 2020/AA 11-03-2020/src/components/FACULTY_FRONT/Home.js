import React, { Component } from 'react'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Nav from '../dynnav'
import ContentLoader from "react-content-loader"
import Hidden from '@material-ui/core/Hidden';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Fhome from '../fhome2.png'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import DesktopMacIcon from '@material-ui/icons/DesktopMac';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class FrontPage extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      display:false,
      loader:true,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      login:'/ework/flogin',
      about_display:false,
      noti_route:true,
      user:'',
      display_policy:false,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/ework/user/')
  .then(response => {
      if (response.data.user === null) {
         this.setState({loader:false})
      }
      else{
        this.setState({user:response.data.user.username,display:true,loader:false})
      }
        })
      }


    componentDidMount() {
        this.getUser();
    }
    about = () =>{
      this.setState({about_display:!this.state.about_display})
    }

    render(){
      const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="10" y="25" rx="0" ry="0" width="190" height="130" />
          <rect x="240" y="25" rx="0" ry="0" width="150" height="130" />
        </ContentLoader>
      )
      if(this.state.loader=== true)
      {
        return(
          <MyLoader />
        );
      }
      else{
        return(
          <React.Fragment>
          <Nav noti_route={this.state.noti_route} home={this.state.home}
          login={this.state.login} nav_route={this.state.nav_route}
          get={this.state.get} logout={this.state.logout}/>

          {this.state.about_display &&
            <About about={this.about} about_display={this.state.about_display}
             user={this.state.user}/>}
            <Grid container>
                <Grid item xs={12} sm={6} md={6} xl={6} lg={6}>
                    <Paper style={{backgroundColor: '#455a64',color: 'white',
                     padding:'50px 0px 90px 0px',margin: '30px 0px 0px 10px',borderRadius:'10px',
                     width: '100%'}}>
                          <Typography variant="h4" align="center" color="secondary"
                          style={{fontSize:'45px',fontFamily: 'Play',
                            display: 'block',
                            lineHeight: '6px',
                            letterSpacing: '1px'}}>
                            E-Work
                          </Typography><br /><br /><br />
                          <Typography variant="body1" style={{padding: '0px 80px',fontSize:'20px',textAlign:'justify'}}
                          gutterBottom>
                            Ework is a simplified analytics tool.
                            It is a tool to keep trace and record of each and everyday routine of staff members of the institute.
                            E-Work is a tool designed by CARE which majorly focuses on the encouragement of the well deserved ones.
                          </Typography>
                          <br/><br/>
                          <Grid container spacing={3} style={{padding:'0px 65px 0px 65px'}}>
                                  <Grid item xs={12} sm={6}>
                                      {this.state.display ?
                                          <Button fullWidth variant="contained" disabled={true}
                                           style={{background:'#03a9f4',color:'white'}}
                                           endIcon={<ExitToAppIcon />} >
                                               <b>LOGIN or SignUp</b>
                                          </Button>
                                       :
                                       <Link to="/ework/flogin" style={{color:'white',textDecoration:'none'}}>
                                         <Button fullWidth variant="contained" disabled={this.state.display}
                                          style={{background:'#03a9f4',color:'white'}}
                                          endIcon={<ExitToAppIcon />} >
                                              <b>LOGIN or SignUp</b>
                                         </Button>
                                      </Link>
                                      }
                                  </Grid>
                                  <Grid item xs={12} sm={6}>
                                      <Button fullWidth variant="contained" onClick={this.about}
                                       style={{background:'#c0ca33',color:'white'}}
                                       endIcon={<DesktopMacIcon />}>
                                        <b>About</b>
                                      </Button>
                                  </Grid>
                           </Grid>
                           <br />
                             <div style={{float:'right'}} >
                               <Button style={{color:'white',margin:'8px'}}
                                 onClick={()=>this.setState({display_policy:true})}>
                                 Privacy Policy
                               </Button>
                             </div>
                             <Dialog
                               open={this.state.display_policy}
                               TransitionComponent={Transition}
                               keepMounted
                               onClose={()=>this.setState({display_policy:false})}
                               aria-labelledby="alert-dialog-slide-title"
                               aria-describedby="alert-dialog-slide-description"
                             >
                               <DialogTitle id="alert-dialog-slide-title">Privacy Policy</DialogTitle>
                               <DialogContent>
                                 <DialogContentText id="alert-dialog-slide-description">
                                   This Section will be updated later
                                 </DialogContentText>
                               </DialogContent>
                               <DialogActions>
                                 <Button onClick={()=>this.setState({display_policy:false})}
                                 color="primary">
                                   Close
                                 </Button>
                               </DialogActions>
                             </Dialog>

                     </Paper>
                 </Grid>
                 <Hidden xsDown>
                    <Grid item md={6} sm={6} xl={6} lg={6}>
                        <img className="center imgf" src={Fhome} alt=""/>
                    </Grid>
                </Hidden>
            </Grid>
            </React.Fragment>
        );
      }
    }
}



class About extends Component {
  constructor()
  {
    super()
    this.state={
      isChecked:0,
      active:0,
    }
  }


  handleComp = (e) =>{
    this.setState({
      isChecked: e,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }
  toggleDiv =()=>{
    this.setState({toggled:!this.state.toggled})
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "go active-pressed";
      }
      return "go";
  }
  render() {
    return (
      <Dialog fullScreen open={this.props.about_display} onClose={this.props.about} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={3} sm={3}>
                  <IconButton edge="start" color="inherit" onClick={this.props.about} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid container item xs={6} sm={6}>
                      <Grid className={this.color(0)} style={{textAlign:'center'}} onClick={(e)=>{this.handleComp(0)}}
                       item xs={12} sm={4}>ABOUT EWORK</Grid>
                      <Grid className={this.color(1)}
                      onClick={(e)=>{this.handleComp(1)}} style={{textAlign:'center'}}
                      item xs={12} sm={4}>CONTACT US</Grid>
                      {this.props.user && <Grid className={this.color(2)}
                      onClick={(e)=>{this.handleComp(2)}} style={{textAlign:'center'}}
                      item xs={12} sm={4}>COMPLAINTS</Grid>}
               </Grid>
               <Grid item xs={3} sm={3}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
            <div style={{marginTop:'40px'}}>
                <Propagation option={this.state.isChecked} user={this.props.user}/>
            </div>
          </List>
        </Dialog>
    );
  }
}




class Propagation extends Component {
  constructor()
  {
    super()
    this.initialState={
      complaint:'',
      message:'',
      from:'',
      subject:'',
      name:'',
      mailid:'',
      id:'',
      official_id:'',
      complaint_subject:'',
      message_from_admin:null,
      fresh:true,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setState({official_id:this.props.user})
  }
  sendMessage = (e) =>{
    e.preventDefault();
    if ((!this.state.name) || (!this.state.mailid) || (!this.state.subject) || (!this.state.message) )
    {
      this.setState({
        snack_open:true,
        snack_msg:'Enter the Details !!',
        alert_type:'warning',
      })
    }
    else{
      this.setState({
        snack_open:true,
        snack_msg:'Sending Your Mail ...',
        alert_type:'info',
      })
      axios.post('/ework/user/contactus',this.state)
      .then(res=>{
        if(res.data)
        {
          this.setState({
            snack_open:true,
            snack_msg:'We have send your query !!',
            alert_type:'success',
          })
          this.setState(this.initialState)
        }
      })
    }
  }
  sendComplaint = (e) =>{
     e.preventDefault();
     if (!(this.state.mailid) || !(this.state.complaint_subject) || !(this.state.complaint))
     {
       this.setState({
         snack_open:true,
         snack_msg:'Enter the Details !!',
         alert_type:'warning',
       });
     }
     else{
     axios.post('/ework/user/complaint',{data:this.state})
     .then( res => {
         if(res.data === 'ok')
         {
           this.setState({
             snack_open:true,
             snack_msg:'Your Complaint has been sent !!',
             alert_type:'success',
           })
             this.setState(this.initialState)
         }
     });
    }
  }
  intakeInput =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {
    if(this.props.option === 0)
    {
      return(
        <React.Fragment>
               <Grid container spacing={1}>
                    <Grid item xs={2} sm={2} />
                    <Grid item xs={8} sm={8}>
                        <div className="ab-head">
                         About eWork
                       </div>
                       <Paper elevation={3} className="ab-content">
                        eWork is an online evaluation tool developed by the SRM CARE team. It does not focuses on any personal issues of any staff member. eWork is designed and developed to improve the self performance of both staff and non-staff members. The deserving ones are always praised in any organization, so keeping this in mind this tool is made to enhance the performance of the one who really work hard enough to contribute to the institute's image as well as in the growth.
                        eWork is the tool which analyzes day-to-day activity of the staff members and helps in giving them an opportunity in different ways to utilize their time towards institute's contribution as well as in personal growth in technical as well as non-technical side. The performance appraisal process in an organization can sometimes be unpopular with HR teams, line managers, and employees. eWork is designed in order to reduce the time taken and also to help the management team to gather the information on the ones who are really deserving for getting praised. It gives the flexibility to the Director to be in glimpse of everything going on in the campus. The Director can view and analyze the faculty's progression as well as can decide the method for appraisal. This tool only provides the way to the Director to view the datas and progression of the staff members, CARE Team is not involved in deciding the deserving staff members.
                        For any queries related to the application please contact CARE Team
                       </Paper>
                    </Grid>
                    <Grid item xs={2} sm={2} />
                </Grid>
          </React.Fragment>
      )
    }
    else if(this.props.option === 1)
    {
      return(
        <React.Fragment>
          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          <Grid container spacing={1}>
             <Hidden xsDown>
               <Grid item sm={3} />
             </Hidden>
               <Grid item xs={12} sm={6}>
                   <Paper style={{padding:'45px',marginTop:'30px'}}>
                       <Typography variant="h6" gutterBottom align="center">Leave A Message</Typography>
                       <br />
                       <Grid container spacing={3}>
                         <Grid item xs={12} sm={12}>
                           <TextField iid="name" name="name" value={this.state.name}
                            onChange={this.intakeInput} label="Enter Your Name"
                           variant="outlined" fullWidth />
                         </Grid>
                       </Grid>
                       <Grid container spacing={3}>
                         <Grid item xs={12} sm={12}>
                           <TextField id="mailid" name="mailid" value={this.state.mailid}
                           onChange={this.intakeInput} label="Enter Your Official Mail Id"
                           variant="outlined" fullWidth />
                         </Grid>
                       </Grid>
                       <Grid container spacing={3}>
                         <Grid item xs={12} sm={12}>
                           <TextField id="official_id" name="id" value={this.state.id}
                           onChange={this.intakeInput} label="Enter Your Official Id"
                           variant="outlined" fullWidth />
                         </Grid>
                       </Grid>
                       <Grid container spacing={3}>
                         <Grid item xs={12} sm={12}>
                           <TextField id="subject" name="subject" value={this.state.subject}
                           onChange={this.intakeInput}
                           label="Specify the Subject of Your Query"
                           variant="outlined" fullWidth />
                         </Grid>
                       </Grid>
                       <Grid container spacing={3}>
                         <Grid item xs={12} sm={12}>
                             <TextField
                               id="outlined-multiline-static"
                               label="Enter the message"
                               multiline
                               rows="4"
                               fullWidth
                               name="message"
                               value={this.state.message}
                               onChange={this.intakeInput}
                               variant="filled"
                             />
                         </Grid>
                       </Grid>
                       <br />

                         <Button variant="contained" color="secondary" onClick={this.sendMessage}
                         style={{float:'right'}}>SEND A MESSAGE</Button>

                 </Paper>
              </Grid>
            <Hidden xsDown>
              <Grid item sm={3} />
            </Hidden>
          </Grid>
        </React.Fragment>
      )
    }
    else if(this.props.option === 2)
    {
    return (
      <React.Fragment>
      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      <Grid container spacing={1}>
        <Hidden xsDown>
          <Grid item sm={3} />
        </Hidden>
        <Grid item xs={12} sm={6}>
         <Paper style={{padding:'45px',marginTop:'30px'}}>
             <Typography variant="h6" gutterBottom align="center">Leave A Message</Typography>
             <br />
              <Grid container spacing={1}>
                <Grid item xs={12} sm={12}>
                  <TextField id="id" name="mailid" value={this.state.mailid} onChange={this.intakeInput}
                  label="Enter Your Official Mail Id"
                  variant="outlined" fullWidth />
                </Grid>
              </Grid>
              <Grid container spacing={1}>
                <Grid item xs={12} sm={12}>
                  <TextField  id="complaint_subject" name="complaint_subject"
                  value={this.state.complaint_subject} onChange={this.intakeInput}
                  label="Enter the Subject of Your Complaint"
                  variant="outlined" fullWidth />
                </Grid>
              </Grid>
              <Grid container spacing={1}>
                <Grid item xs={12} sm={12}>
                      <TextField
                        id="outlined-multiline-static"
                        label="Enter the Message"
                        multiline
                        rows="4"
                        fullWidth
                        name="complaint"
                        value={this.state.complaint}
                        onChange={this.intakeInput}
                        variant="filled"
                      />
                    </Grid>
                </Grid>
                <br />
                <Button variant="contained" color="secondary"
                onClick={this.sendComplaint} style={{float:'right'}}>SEND</Button>
         </Paper>
        </Grid>
          <Hidden xsDown>
            <Grid item sm={3} />
          </Hidden>
      </Grid>
    </React.Fragment>
    );
  }
  }
}
