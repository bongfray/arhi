import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';


export default class SkillDomain extends Component {
  constructor()
  {
    super()
    this.state={
      username:'',
      loading:true,
      disabled:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.username,loading:false})
      }
      else
      {
        this.setState({redirectTo:'/ework/faculty',loading:false})
         window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }

  upStat =()=>{
    this.setState({disabled:false})
  }

  render() {
    if(this.state.loading)
    {
      return(
        <div style={{textAlign:'center'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else {
      return (
        <React.Fragment>
        {!(this.props.reference==="none") &&
          <React.Fragment>
                  <Fab variant="extended" style={{float:'right',color:'black'}} disabled={this.state.disabled}>
                      <Link to="/ework/faculty" style={{textDecoration:'none',color:'black'}}>
                        Home <ArrowForwardIcon style={{paddingTop:'8px'}} />.
                      </Link>
                  </Fab>
          </React.Fragment>
        }
        <br />
        <Grid container spacing={1}>
          <Grid item xs={12} sm={6}>
             <Setter username={this.state.username} status={this.upStat} type={'Skill'} />
          </Grid>
          <Grid item xs={12} sm={6}>
             <Setter username={this.state.username} status={this.upStat} type={'Domain'} />
          </Grid>
        </Grid>
       </React.Fragment>
      );
    }

  }
}

class Setter extends Component{
 constructor(props) {
    super(props);
    this.state = {
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      action:this.props.options,
      saved_data:[],

    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount()
{
 this.fetchSavedSkills()
 this.setState({
   isAddProduct: false,
   isEditProduct: false,
 })
}

fetchSavedSkills =()=>{
  let action;
  if(this.props.type ==='Skill')
  {
    action = 'Skill-For-Student';
  }
  else {
    action = 'Domain-For-Student';
  }
 axios.post('/ework/user2/fetch_in_admin',{action:action})
 .then(res => {
     if(res.data)
     {
       this.setState({saved_data:res.data})
     }
 });
}


  onCreate = (e,index) => {
    this.setState({ isAddProduct: true ,product: {}});
  }
  onFormSubmit(data) {
    //console.log(data)
    let apiUrl;
    if(this.state.isEditProduct){
      apiUrl = '/ework/user/edit_for_fac';
    } else {
      apiUrl = '/ework/user/add_skill_for_fac';
    }
    axios.post(apiUrl, {data})
        .then(response => {
          this.setState({
            response: response.data,
            isAddProduct: false,
            isEditProduct: false
          })
        })
  }


  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  editProduct = (productId,index)=> {
    axios.post('/ework/user/fetch_to_edit_for_fac',{
      id: productId,
      username: this.props.username,
    })
        .then(response => {
          this.setState({
            product: response.data,
            isEditProduct: true,
            isAddProduct: true,
          });
        })

 }
  render() {
    let productForm,title,description;
    var data;
            title ='Skills';
            data = {
              fielddata: [
                {
                  header: "Your Skills/Domains",
                  name: "name",
                  placeholder: "Enter Your Skill/Domain",
                  type: "text",
                  differ:true,
                },
                {
                  header: "Your Level",
                  name: "level",
                  placeholder: "Enter Your Level in this Skill/Domain",
                  type: "text",
                  grid: 3,
                  differ:false,
                  exception:true,
                },
              ],
            };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} saved_skill={this.state.saved_data}
username={this.props.username} action={this.props.type} data={data}
onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (

 <div>
   {!this.state.isAddProduct &&
     <React.Fragment>
       <ProductList referOutside={this.props.referOutside} status={this.props.status}
        username={this.props.username} title={title} description={description}
       action={this.props.type} data={data}  editProduct={this.editProduct}/>
     </React.Fragment>
   }
   <br />
   {!this.state.isAddProduct &&
     <React.Fragment>
       <Grid container spacing={1}>
          <Grid item xs={6} sm={6}/>
          <Grid item xs={5} sm={5}>
           {!(this.props.referOutside) &&
             <Button variant="contained" style={{float:'right'}} color="secondary"
              onClick={(e) => this.onCreate(e,this.props.type)}
              disabled={this.state.disabled}>
            Add Data
            </Button>}
          </Grid>
          <Grid item xs={1} sm={1}/>
      </Grid>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>
);
}
}


class AddProduct extends React.Component {
 constructor(props) {
   super(props);
   this.initialState = {
     value:'',
     serial:'',
     action:'',
     name:'',
     level:'',
     fresh:false,
   }
   if(this.props.product){
     this.state = this.props.product
   } else {
     this.state = this.initialState;
   }
   this.componentDidMount = this.componentDidMount.bind(this);
   this.handleChange = this.handleChange.bind(this);
   this.handleSubmit = this.handleSubmit.bind(this);
 }


 handleD = (event,index) =>{
   this.setState({
     [event.target.name]: event.target.value
   })

 }


componentDidMount(){
 this.setState({
   action:this.props.action,
   username: this.props.username,
   fresh:true,
 })
}


 handleChange(event) {
   const name = event.target.name;
   const value = event.target.value;

   this.setState({
     [name]: value
   })
 }

 handleSubmit(e) {
   e.preventDefault();
 if(!this.state.name || !this.state.level)
 {
   window.M.toast({html: 'Fill all the Details!!',classes:'rounded red'});
 }
 else {
   this.props.onFormSubmit(this.state);
   this.setState(this.initialState);
 }

 }

 handleInput = (e) =>{
   this.setState({[e.target.name]:e.target.value})
 }

 stateSet=(object)=>{
   this.setState(object);
 }


 render() {
  //console.log(this.state)
   let pageTitle;
if(this.state.serial) {
 pageTitle ="EDIT DATAS"
} else {
 pageTitle = "ADD DATAS TO YOUR PERSONAL DATA";
}

   return(
     <React.Fragment>
     <Paper elevation={3} style={{padding:'10px'}}>
     <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{pageTitle} </Typography><br />
     <br />
         {this.props.data.fielddata.map((content,index)=>(
         <Grid container spacing={1} key={index}>
          <Grid item xs={1} sm={1}/>
           <Grid item xs={2} sm={2}>
              {content.header}
           </Grid>
           <Grid item xs={8} sm={8}>
               <React.Fragment>
                  {content.exception ?
                    <FormControl style={{width:'100%'}}>
                      <InputLabel id="level">Select Here</InputLabel>
                        <Select
                          labelId="level"
                          id="level"
                          name="level" value={this.state.level} onChange={this.handleInput}
                        >
                        <MenuItem value="Begineer">Begineer</MenuItem>
                        <MenuItem value="Intermediate">Intermediate</MenuItem>
                        <MenuItem value="Advanced">Advanced</MenuItem>
                        </Select>
                     </FormControl>
                      :
                    <React.Fragment>
                      {content.name === "name" &&
                        <SearchEngine stateSet={this.stateSet}
                         action={this.props.action} field={this.props.saved_skill} />
                      }
                    </React.Fragment>
                  }
               </React.Fragment>
           </Grid>
           <Grid item xs={1} sm={1}/>
         </Grid>
         ))}

<br /><br />
         <Grid container spacing={1}>
           <Grid item xs={7} sm={7} />
           <Grid item xs={2} sm={2}>
             <Button variant="outlined" onClick={this.props.cancel} color="secondary">CANCEL</Button>
           </Grid>
           <Grid item xs={3} sm={3}>
            <Button variant="contained" onClick={this.handleSubmit}
             style={{backgroundColor:'#455a64',color:'white'}} >SUBMIT</Button>
           </Grid>
         </Grid>
     </Paper>
     </React.Fragment>
   )
 }
}


class SearchEngine extends React.Component {
 setDomain=(e)=>{
   if(e === null)
   {

   }
   else{
    // console.log(this.props.stateSet)
     if(this.props.action ==='Skill')
     {
       this.props.stateSet({
         name:e.skill_name,
       })
     }
     else {
       this.props.stateSet({
         name:e.domain_name,
       })
     }

   }
 }

 render() {
   let options;
   if(this.props.action ==='Skill')
   {
    options = this.props.field.map(option => {
       const firstLetter = option.skill_name[0].toUpperCase();
       return {
         firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
         ...option,
       };
     });
   }
   else {
     options = this.props.field.map(option => {
       const firstLetter = option.domain_name[0].toUpperCase();
       return {
         firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
         ...option,
       };
     });
   }


 return (
       <React.Fragment>
      {this.props.action ==='Skill' ?
         <Autocomplete
           id="grouped-demo"
           options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
           groupBy={option => option.firstLetter}
           getOptionLabel={option => option.skill_name}
           onChange={(event, value) => this.setDomain(value)}
           style={{ width: '100%' }}
           renderInput={params => (
             <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
           )}
         />
         :
         <Autocomplete
           id="grouped-demo"
           options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
           groupBy={option => option.firstLetter}
           getOptionLabel={option => option.domain_name}
           onChange={(event, value) => this.setDomain(value)}
           style={{ width: '100%' }}
           renderInput={params => (
             <TextField {...params} label="Select Domain" variant="outlined" fullWidth />
           )}
         />
       }
      </React.Fragment>
   );
 }
}

class ProductList extends React.Component {
 constructor(props) {
   super(props);
   this.state = {
     error: null,
     products: [],
     action:'',
   }
   this.fetch = this.fetch.bind(this)
   this.componentDidMount = this.componentDidMount.bind(this)
 }

 componentDidMount(){
     this.fetch()
 }

 componentDidUpdate =(prevProps) => {
   if (prevProps.action !== this.props.action) {
     this.fetch();
   }
 }


 deleteProduct = (productId,index) => {
   this.setState({render_confirm:!this.state.render_confirm,productId:productId});
}

confirm=()=>{
 let delUrl;

   delUrl = '/ework/user/del_skill_for_fac';
const { products } = this.state;
axios.post(delUrl,{
 serial: this.state.productId,
 username: this.props.username,
})
   .then(response => {
     this.setState({
       render_confirm:false,
       productId:0,
       response: response,
       products: products.filter(product => product.serial !== this.state.productId)
    })
   })
}

fetch =() =>{
 axios.post('/ework/user/fetch_skill_for_fac',{
   action: this.props.action,
   username: this.props.username,
 })
 .then(response =>{
   this.setState({
    products: response.data,
  })
  if(response.data.length>0)
  {
     this.props.status();
  }
 })
}



 render() {
   const {  products} = this.state;
     return(
       <React.Fragment>
       <Dialog
         open={this.state.render_confirm}
         aria-labelledby="alert-dialog-title"
         aria-describedby="alert-dialog-description"
       >
         <DialogTitle id="alert-dialog-title">Confirmation of Deleting the Data</DialogTitle>
         <DialogContent>
           <DialogContentText id="alert-dialog-description">
             After you click agree button below, your all rating datas related to this content will be affected.
             So kindly take decision what do you really want to do .
           </DialogContentText>
         </DialogContent>
         <DialogActions>
           <Button onClick={this.deleteProduct} color="primary">
             Disagree
           </Button>
           <Button onClick={this.confirm} color="primary" autoFocus>
             Agree
           </Button>
         </DialogActions>
       </Dialog>

         <Paper elevation={3} style={{padding:'10px'}}>
           <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{this.props.action}</Typography><br />

             <Grid container spacing={1}>
               <Grid item xs={2} sm={2} style={{textAlign:'center'}}><b>Serial No</b></Grid>
                 {this.props.data.fielddata.map((content,index)=>(
                   <Grid item xs={4} sm={4} key={index} style={{textAlign:'center'}}>
                     <b>{content.header}</b>
                  </Grid>
                 ))}
               <Grid item xs={2} sm={2} style={{textAlign:'center'}}><b>Action</b></Grid>
             </Grid>
             {products.map((product,index) => (
               <Grid container spacing={1} key={index}>
                 <Grid item xs={2} sm={2} style={{textAlign:'center'}}>{index+1}</Grid>
                 <Grid item xs={4} sm={4} style={{textAlign:'center'}}>
                     {product.name}
                 </Grid>
                 <Grid item sm={4} xs={4} style={{textAlign:'center'}}>
                   {product.level}
                 </Grid>
                   <Grid item xs={2} sm={2}>
                     {!(this.props.referOutside) && <div style={{textAlign:'center'}}>
                       <EditIcon className="go"  onClick={() => this.props.editProduct(product.serial,this.props.action)} />&nbsp;
                       <DeleteIcon className="go" style={{color:'red'}} onClick={() => this.deleteProduct(product.serial,this.props.action)} />
                     </div>
                   }
                  </Grid>
               </Grid>
             ))}
           </Paper>
       </React.Fragment>
     )

 }
}
