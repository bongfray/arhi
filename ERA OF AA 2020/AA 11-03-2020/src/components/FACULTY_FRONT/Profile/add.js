import React from 'react';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      username:'',
      College_Name:'',
      End_Year:'',
      Start_Year:'',
      Marks_Grade:'',
      verificationlinks:'',
      verified:false,
      date:'',
      month:'',
      year:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  var date = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  this.setState({
    action:this.props.action,
    username: this.props.username,
    verified:false,
    date:date,
    month:month,
    year:year,
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {

    return(
      <Dialog open={true} fullWidth="sm">
         <div style={{padding:'30px'}}>
              {this.props.data.fielddata.map((content,index)=>(
                  <React.Fragment key={index}>
                      <TextField  id={content.name} type={content.type}
                        name={content.name} fullWidth
                        value={this.state[content.name]}
                        onChange={e => this.handleD(e, index)}
                      label={content.placeholder} variant="outlined" />
                      <br /><br />
                  </React.Fragment>
              ))}
          </div>

        <DialogActions>
          <Button  variant="outlined" color="secondary" onClick={this.props.cancel} >CANCEL</Button>
          <Button variant="contained" color="primary" type="submit" onClick={this.handleSubmit}>
            UPLOAD
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default AddProduct;
