import React, { Component} from 'react'
import {Link, Redirect } from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';

import axios from 'axios'
import ColorRep from './colordiv.js'
import Insturction from './instructionmodal'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader";
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import Button from '@material-ui/core/Button';
require("datejs")

class DayOrder extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}

export default class Simple extends Component{
  constructor() {
    super();
    this.state = {
      loading:true,
      opendir: '',
      modal: false,
      display:'block',
      saved_dayorder:'',
      redirectTo:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      noti_route:true,
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  getCount =() =>{
    axios.get('/ework/user/knowcount', {
    })
    .then(response =>{
      this.setState({loading:false})
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  getDayOrder = () =>{
    var yesterday = Date.parse("yesterday").toString("dd");
    var yestermonth = Date.parse("yesterday").toString("M");
    var yesteryear = Date.parse("yesterday").toString("yyyy");
    axios.post('/ework/user/fetch_yesterday_dayorder', {date:yesterday,month:yestermonth,year:yesteryear
    })
    .then(response =>{
       if((response.data.initial ===0)  || (response.data.initial ===1)  || (response.data.stock===0))
        {
          this.setState({saved_dayorder:0,display:'none'})
        }
      else
      {
        if(response.data.initial)
        {
          this.setState({saved_dayorder:response.data.initial})
        }
        else{
          this.setState({saved_dayorder:response.data.stock})
        }
      }
      this.setState({loading:false})
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
    this.getDayOrder();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };

      render(){
        const MyLoader = () => (
          <ContentLoader
            height={160}
            width={400}
            speed={2}
            primaryColor="#f3f3f3"
            secondaryColor="#c0c0c0"
          >
            <rect x="10" y="8" rx="5" ry="5" width="80" height="30" />
            <rect x="140" y="30" rx="3" ry="3" width="250" height="120" />

            <rect x="10" y="70" rx="5" ry="5" width="80" height="30" />
            <rect x="10" y="120" rx="1" ry="1" width="80" height="15" />


          </ContentLoader>
        )
        if(this.state.loading === true)
        {
          return(
            <MyLoader />
          );
        }
        else{
        if (this.state.redirectTo) {
             return <Redirect to={{ pathname: this.state.redirectTo }} />
         } else {

        return(
          <React.Fragment>
          <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <Insturction
              displayModal={this.state.modal}
              closeModal={this.selectModal}
          />
          <div style={{padding:'10px'}}>
              <Grid container spacing={1}>
                  <Grid item xs={4} sm={4} />
                 <Grid item xs={4} sm={4}>
                     <div className="center blue-text" style={{fontSize:'20px'}}>Last DayOrder</div>
                 </Grid>
                 <Grid item xs={4} sm={4}>
                   <Link to="/ework/time_new" style={{color: 'black',float:'right'}}>
                   <Button
                     variant="outlined"
                     color="primary"
                     endIcon={<ArrowRightIcon />}
                   >
                   Got o Today's Dayorder
                   </Button></Link>
                 </Grid>
              </Grid>

              <Grid container spacing={1}>
                  <Grid item xs={12} sm={2}>
                      <DayOrder day_order={this.state.saved_dayorder}/>
                    <br />
                    <div style={{display:this.state.display}}>
                         <FormControl style={{width:'100%'}}>
                           <InputLabel id="sel_type">Select Here</InputLabel>
                             <Select
                               labelId="sel_type"
                               id="sel_type"
                               value={this.state.opendir}
                               onChange={this.handledir}
                             >
                             <MenuItem value="r_class">Regular Class</MenuItem>
                             </Select>
                          </FormControl>
                    </div>
                 </Grid>
                <Grid item xs={12} sm={10}>
                  <Content opendir={this.state.opendir}
                  day_order={this.state.saved_dayorder}/>
                </Grid>
            </Grid>
          </div>
          </React.Fragment>
        );
      }
    }
      }
    }


class Content extends Component{
  constructor(){
    super();
    this.state={
      redirectTo:'',
      username:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.getUser();
  }
  getUser =()=>{
    axios.get('/ework/user/')
    .then( res => {
        if(res.data.user){
          this.setState({username:res.data.user.username})
        }
    });
  }
  render(){
    var date = Date.parse("yesterday").toString("dd");
    var month = Date.parse("yesterday").toString("M");
    var year = Date.parse("yesterday").toString("yyyy");
    if (this.state.redirectTo)
    {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else{
          return(
            <React.Fragment>
             <ColorRep opendir={this.props.opendir}
              username={this.state.username}
              date={date} month={month} year={year} day_order={this.props.day_order}/>
            </React.Fragment>
          );
      }
  }
}
