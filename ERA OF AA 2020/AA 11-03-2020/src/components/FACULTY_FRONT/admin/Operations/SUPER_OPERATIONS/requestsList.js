import React, { Component } from 'react'
import {Grid,Paper,Typography,Button} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CardContent from '@material-ui/core/CardContent';
import {InputBase,Divider,IconButton,CircularProgress} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios';

import ViewApprove from './viewRequests'




export default class Lists extends Component {
  constructor(){
      super();
      this.state={
        showButton:'block',
        show:'none',
        display:'none',
        redirectTo:'',
        modal: false,
        isChecked:false,
        isCheckedS:false,
        history:'',
          susername:'',
          username:'',
          index_id:'',
          request:[],
          responded_request:[],
          viewdata:'',
          option:'',
          req_reject_no:'',
          req_accept_no:'',
          selectedOption:'',
          id:'',
          renderDiv:false,
          switch_value:'',
          success:'block',
          value_for_search:'',
      }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps.select !==this.props.select)
    {
      this.setState({success:'block'})
    }
  }
  getRequest =() =>{
    axios.get('/ework/user/getrequest')
        .then(response => {
          const reques = response.data.filter(item => item.action === "false");
          const reject = response.data.filter(item => item.action === "denied");
          const approve = response.data.filter(item => item.action === "approved");
          this.setState({display:'block',request: reques,responded_request:response.data,req_reject_no:reject.length,req_accept_no:approve.length})
        })
  }
  view_Status_of_Request = (content,index) =>{
    this.setState({
      username:content.username,
      index_id:index,
    })
    axios.post('/ework/user/approve_request',content)
        .then(response => {
          if(response.data)
          {
            this.setState({viewdata: response.data})
            this.modalrender();
          }
        })
  }
  modalrender=()=>{
    this.setState({renderDiv:!this.state.renderDiv})
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  showDiv =(userObject) => {
    this.setState(userObject)
  }
  componentDidMount()
  {
    this.openModal();
    this.getRequest();
  }


  handleOption = (e) =>{
    this.setState({option: e.target.value})
  }

  searchEngine =(e)=>{
    this.setState({value_for_search:e.target.value})
  }

  handleRequestModify =(object) =>{
    this.setState(object)
  }

  render() {
    if(this.state.display === 'none')
    {
      return(
        <React.Fragment>
          <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
            <CircularProgress color="secondary" />
          </div>
        </React.Fragment>
      )
    }
    else
    {
      if(this.state.request.length === 0)
      {
        return(
          <Typography variant="h5" color="secondary" align="center">No Request Found !!</Typography>
        )
      }
      else
      {
        var libraries = this.state.request,
        searchString = this.state.value_for_search.trim().toLowerCase();
        libraries = libraries.filter(function(i) {
          return i.username.toLowerCase().match( searchString );
        });
        return (
          <React.Fragment>
                  <div style={{margin:'15px 8px 1px 9px'}}>
                    <Grid container spacing={1}>
                       <Grid item xs={4} sm={4}>
                          <Paper variant="outlined" square>
                            <Card variant="outlined">
                              <CardContent>
                               <Typography style={{backgroundColor:'yellow'}} align="center" variant="h6">
                                Total No of Request Received
                               </Typography>
                               <Typography variant="h5" align="center">
                                 {this.state.responded_request.length}
                               </Typography>
                              </CardContent>
                              </Card>
                          </Paper>
                       </Grid>
                       <Grid item xs={4} sm={4}>
                          <Paper variant="outlined" square>
                            <Card variant="outlined">
                              <CardContent>
                               <Typography style={{backgroundColor:'yellow'}} align="center" variant="h6">
                                Total No of Request Approved
                               </Typography>
                               <Typography variant="h5" align="center">
                                 {this.state.req_accept_no}
                               </Typography>
                              </CardContent>
                              </Card>
                          </Paper>
                       </Grid>
                       <Grid item xs={4} sm={4}>
                          <Paper variant="outlined" square>
                            <Card variant="outlined">
                              <CardContent>
                               <Typography style={{backgroundColor:'yellow'}} align="center" variant="h6">
                                Total No of Request Rejected
                               </Typography>
                               <Typography variant="h5" align="center">
                                 {this.state.req_reject_no}
                               </Typography>
                              </CardContent>
                              </Card>
                          </Paper>
                       </Grid>
                    </Grid>
                    <br />
                    <div style={{padding:'30px'}}>
                     <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
                   </div>
                   <br />
                    <TableContainer component={Paper}>
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell align="center"><b>Day Order</b></TableCell>
                            <TableCell align="center"><b>Official ID</b></TableCell>
                            <TableCell align="center"><b>Requested Hour</b></TableCell>
                            <TableCell align="center"><b>Requested Date(dd/mm/yyyy)</b></TableCell>
                            <TableCell align="center"><b>Reason</b></TableCell>
                            <TableCell align="center"><b>Action</b></TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {libraries.map((row,index)=> (
                            <TableRow key={index}>
                              <TableCell align="center">
                                {row.day_order}
                              </TableCell>
                              <TableCell align="center">{row.username}</TableCell>
                              <TableCell align="center">{row.req_hour ? <div>{row.req_hour}</div>: <div>NULL</div>}</TableCell>
                              <TableCell align="center">{row.req_date}/{row.req_month}/{row.req_year}</TableCell>
                              <TableCell align="center">{row.req_reason}</TableCell>
                              <TableCell align="center">
                                  <Button variant="outlined" color="secondary" onClick={() => this.view_Status_of_Request(row,row.serial)}>View Status</Button>
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>

                    <ViewApprove showDiv={this.showDiv} handleRequestModify={this.handleRequestModify}
                     showButton={this.state.showButton} request={this.state.request}
                     modalrender={this.modalrender} renderDiv={this.state.renderDiv} view_details={this.state.viewdata}
                     username={this.state.username} index_id={this.state.index_id}/>

                  </div>
        </React.Fragment>
      );
    }
  }
 }
}



const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search Only Official Id" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
