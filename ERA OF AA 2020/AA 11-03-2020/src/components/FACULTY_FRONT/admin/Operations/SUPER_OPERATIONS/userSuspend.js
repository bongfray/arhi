

import React, { Component } from 'react'
import InputLabel from '@material-ui/core/InputLabel';
import {FormControl,Grid,Hidden} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import axios from 'axios';

import Validate from '../../validateUser'


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'none',
            modal:false,
            disabled:false,
            allowed:false,
            suspend_username:'',
            suspend_reason:'',
            current_action:'',
            snack_open:false,
            snack_msg:'',
            alert_type:'',
        }
    }

    componentDidUpdate =(prevProps,prevState) => {
      if (prevState.allowed !== this.state.allowed) {
        if(this.state.current_action === "remove_sespension")
        {
          this.RemoveSuspension();
        }
        else
        {
          this.Suspend();
        }
      }
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }
    handleApprove = (id,username) =>{
      axios.post('/ework/user/approverequest',{
        serial: id,
        username:username,
      }).then(res=>{
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== id),
        })
      })
    }
    handleUsername = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    Suspend = (e)=>{
      if(!this.state.suspend_username || !this.state.suspend_reason)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter all the details !!',
          alert_type:'warning',
        });
      }
      else
      {
        this.setState({success:'block',disabled:true,current_action:'suspend_user'})
        if(this.state.allowed === true)
        {
          this.setState({success:'none',disabled:''})
          let route;
          if(this.props.select === "suspendfac")
          {
            route ="/ework/user/suspension_user"
          }
          else if(this.props.select === "suspendstu")
          {
            route ="/ework/user2/suspension_suser"
          }
          this.handleSuspension(route,true);
        }
      }
    }
    RemoveSuspension = (e)=>{
      if(!this.state.suspend_username)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter all the details !!',
          alert_type:'warning',
        });
      }
      else
      {
      this.setState({success:'block',disabled:true,current_action:'remove_sespension'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:false})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/ework/user/suspension_user"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/ework/user2/suspension_suser"
        }
        this.handle_Remove_Suspension(route,false);
      }
     }
    }
    handleSuspension = (route,status) =>{
      axios.post(route,{state:this.state,status:status})
      .then(res=>{
        if(res.data === 'no')
        {
          this.setState({
            snack_open:true,
            snack_msg:res.data,
            alert_type:'error',
          });
        }
        else {
          this.setState({
            snack_open:true,
            snack_msg:res.data,
            alert_type:'success',
          });
        }
        this.setState({suspend_username:'',suspend_reason:''})
      })
    }

    handle_Remove_Suspension = (route,status) =>{
      axios.post(route,{state:this.state,status:status})
      .then(res=>{
        if(res.data === 'no')
        {
          this.setState({
            snack_open:true,
            snack_msg:res.data,
            alert_type:'error',
          });
        }
        else {
          this.setState({
            snack_open:true,
            snack_msg:res.data,
            alert_type:'success',
          });
        }
        this.setState({suspend_username:'',suspend_reason:''})
      })
    }
    render(){
      let suspend_content;
      if(this.props.select === 'suspendfac' || this.props.select === 'suspendstu'){
            suspend_content=
            <Grid container spacing={1}>
            <Hidden xsDown><Grid item sm={2}/></Hidden>
            <Grid item xs={12} sm={8}>
                <Paper style={{padding:'40px'}}>
                <Typography variant="h6" align="center">{"SUSPEND FACULTY OR STUDENT"}</Typography>
                <br />
                   <Grid container spacing={1}>
                      <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-amount">Official Id</InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-amount1"
                              type="text" name="suspend_username"
                              value={this.state.suspend_username} onChange={this.handleUsername}
                              startAdornment={<InputAdornment position="start">Username</InputAdornment>}
                              labelWidth={70}
                            />
                          </FormControl>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-amount">Put Some Reason</InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-amount2"
                              type="text" name="suspend_reason" value={this.state.suspend_reason} onChange={this.handleUsername}
                              startAdornment={<InputAdornment position="start">Reason</InputAdornment>}
                              labelWidth={125}
                            />
                          </FormControl>
                      </Grid>
                   </Grid>
                   <br />
                     <div style={{float:'right'}}>
                        <Button variant="contained" color="primary" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Button>&emsp;
                        <Button variant="contained" color="secondary" disabled={this.state.disabled} onClick={this.Suspend}>Suspend User</Button>
                     </div>
                   </Paper>
                </Grid>
                <Hidden xsDown><Grid item sm={2}/></Hidden>
              </Grid>
        }
        else{
            suspend_content=
                <div></div>
        }
        return(
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          {this.state.success ===  'block' &&
            <Validate showDiv={this.showDiv}/>
          }
            {suspend_content}
          </React.Fragment>
        );
    }
}
