
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import {CircularProgress} from '@material-ui/core';
import axios from 'axios';


export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      val:'',
      link:'',
      action:'',
      usertype:'',
      username:'',
      skill_name:'',
      description_of_the_task:'',
      ref_for_task:'',
      course_to_learn:'',
      for:'',
      disabled:false,
      loading:true
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
    if(this.props.action ==='Skill-For-Student')
    {
      var skills=this.state.saved_skill.filter(function(i) {
        return i.skill_name.toLowerCase().includes(event.target.value.toLowerCase());
      });
      //console.log(skills)
      if(skills.length>0)
      {
        this.setState({disabled:true})
      }
      else if(skills.length===0) {
        this.setState({disabled:false})
      }
    }
    else if(this.props.action ==='Domain-For-Student')
    {
      var dom=this.state.saved_skill.filter(function(i) {
        return i.domain_name.toLowerCase().includes(event.target.value.toLowerCase());
      });
      //console.log(dom)
      if(dom.length>0)
      {
        this.setState({disabled:true})
      }
      else if(dom.length===0) {
        this.setState({disabled:false})
      }
    }
  }

  componentDidMount(){
    this.fetchSavedSkills();
    this.setState({
      action:this.props.action,
      usertype:this.props.level,
      inserted_by:this.props.username,
    })
    if(this.props.for)
    {
      this.setState({for:this.props.for})
    }
  }

  fetchSavedSkills=()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:this.props.action})
    .then(res => {
        if(res.data)
        {
          this.setState({saved_skill:res.data,loading:false})
        }
    });
  }
  stateSet=(object)=>{
    this.setState(object);
  }

  handleSubmit() {
    if((this.props.action === 'Skill-For-Student') && !(this.state.skill_name))
    {
      return;
    }
    else if((this.props.action === 'Domain-For-Student') && !(this.state.domain_name))
    {
      return;
    }
    else {
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
    }
  }

  render() {
    if(this.state.loading)
    {
      return(
        <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else
    {
    return(
    <Dialog open={true}
    keepMounted fullWidth
    aria-labelledby="alert-dialog-slide-title"
    aria-describedby="alert-dialog-slide-description"
    onClose={this.props.cancel}>
     <DialogContent>
      <div style={{padding:'10px'}}>
          {this.props.data.fielddata.map((content,index)=>(
             <Grid container spacing={1} key={index}>
             {!(content.placeholder === 'blank') &&
             <Grid item xs={12} sm={12}>
               <React.Fragment>
                 {!(content.name === 'course_to_learn') ?
                 <TextField
                   id="outlined-multiline-static"
                   label={content.placeholder}
                   multiline
                   fullWidth
                   name={content.name}
                   value={this.state[content.name]}
                   onChange={e => this.handleD(e, index)}
                   variant="filled"
                 />
                 :
                 <div>
                   <SearchEngine stateSet={this.stateSet} field={this.props.saved_skill} />
                 </div>
                  }
               </React.Fragment>
               </Grid>
             }
             </Grid>

          ))}
        </div>
         </DialogContent>
          <DialogActions>
          <Button onClick={this.props.cancel} color="secondary">
            Close
          </Button>
            <Button disabled={this.state.disabled} onClick={this.handleSubmit} color="primary">
              UPLOAD
            </Button>
          </DialogActions>
      </Dialog>
    )
   }
  }
}

class SearchEngine extends React.Component {

  setSkill=(e)=>{
    if(e === null)
    {

    }
    else
    {
      this.props.stateSet({
        course_to_learn:e.skill_name,
      })
    }
  }

  render() {
    if(this.props.field.length>0)
    {
      const options = this.props.field.map(option => {
        const firstLetter = option.skill_name[0].toUpperCase();
        return {
          firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
          ...option,
        };
      });
      return(
        <Autocomplete
          id="grouped-demo"
          options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
          groupBy={option => option.firstLetter}
          getOptionLabel={option => option.skill_name}
          onChange={(event, value) => this.setSkill(value)}
          style={{ width: '100%' }}
          renderInput={params => (
            <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
          )}
        />
      )
    }
    else {
      return(
        <div></div>
      )
    }
  }
}
