import React, { Component,Fragment } from 'react'
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import ReqStat from './approve_stat'


export default class ManageOne extends Component {
  constructor(props)
  {
    super(props)
    this.initialState ={
      action:'false',
      username:props.username,
      req_date:'',
      req_month:'',
      req_year:'',
      req_reason:'',
      day_order:'',
      expired:false,
      denying_reason:'',
      request_props:[],
      added:false,
      disabled:false,
    }
    this.state = this.initialState;
  }

  handleDatas=(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleRequest =(e) =>{
    e.preventDefault()
    this.setState({added:true})
    if(!(this.state.req_date) || !(this.state.req_month) || !(this.state.req_year) || !(this.state.day_order) || !(this.state.req_reason))
    {
      window.M.toast({html: 'Enter all the Details First !!',classes:'rounded red'});
    }
    else{
    window.M.toast({html: 'Validating....', classes:'rounded yellow black-text'});
    this.setState({disabled:true});
    axios.post('/ework/user/request_for_entry',this.state).then(response=>{
      if(response.data.handled)
      {
         window.M.toast({html: response.data.handled,classes:'rounded red'});

      }
      else if(response.data.succ)
      {
         window.M.toast({html: response.data.succ,classes:'rounded green'});

      }
      else if(response.data.noday){
           window.M.toast({html: response.data.noday,classes:'rounded green'});
      }
       this.setState(this.initialState);
    })
   }
  }
  render()
  {
    return(
      <Fragment>
      <Grid container spacing={1}>
        <Hidden xsDown>
          <Grid item sm={1}/>
        </Hidden>
        <Grid item xs={12} sm={10}>
          <Paper variant="outlined" square style={{padding:'40px'}}>
          <Typography variant="h6" align="center" gutterBottom>
            REQUEST FOR AN ENTRY
          </Typography><br />
          <Grid container spacing={1}>
            <Grid item xs={12} sm={2}>
              <div className="input-field">
               <input className="" type="text" name="day_order" id="Request-dayorder" value={this.state.day_order} onChange={this.handleDatas}/>
               <label htmlFor="Request-dayorder">Enter the DayOrder</label>
              </div>
            </Grid>
            <Grid container item xs={12} sm={5}>
                 <Grid item xs={12} sm={4}>
                   <div className="input-field">
                    <input className="" name="req_date" type="text" id="day-req" value={this.state.req_date} onChange={this.handleDatas}/>
                    <label htmlFor="day-req">Day(dd)</label>
                   </div>
                 </Grid>
                 <Grid item xs={12} sm={4}>
                   <div className="input-field">
                    <input className="" type="text" name="req_month" value={this.state.req_month} id="month-req" onChange={this.handleDatas}/>
                    <label htmlFor="month-req">Month(mm)</label>
                   </div>
                 </Grid>
                 <Grid item xs={12} sm={4}>
                   <div className="input-field">
                    <input className="" type="text" name="req_year" value={this.state.req_year} id="year-req" onChange={this.handleDatas}/>
                    <label htmlFor="year-req">Year(yyyy)</label>
                   </div>
                 </Grid>
            </Grid>
            <Grid item xs={12} sm={5}>
                 <TextField
                    id="filled-textarea"
                    label="Enter the Valid Reason(Hit Enter to resize)"
                    name="req_reason"
                    value={this.state.req_reason}
                    onChange={this.handleDatas}
                    multiline
                    fullWidth
                    variant="filled"
                  />
            </Grid>
          </Grid>
            <Button disabled={this.state.disabled} variant="contained" color="primary"
             onClick={this.handleRequest} style={{marginBottom:'8px',float:'right'}}>Make A Request</Button>
          </Paper>
        </Grid>
        <Hidden xsDown>
          <Grid item sm={1}/>
        </Hidden>
      </Grid>
      <br />
      <Paper variant="outlined" square style={{padding:'10px'}}>
           {this.state.added ?
             <ReqStat request_props={this.state} />
              :
             <ReqStat />
           }
      </Paper>
      </Fragment>
    )
  }
}
