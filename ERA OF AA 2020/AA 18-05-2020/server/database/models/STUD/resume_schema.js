const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const resumeSchema = new Schema({
fresh: { type: Boolean, unique: false, required: false },


username: { type: String, unique: false, required: false },
action: { type: String, unique: false, required: false },
dept: { type: String, unique: false, required: false },
year: { type: String, unique: false, required: false },
sem: { type: String, unique: false, required: false },
campus: { type: String, unique: false, required: false },

name: { type: String, unique: false, required: false },
emailid: { type: String, unique: false, required: false },
mobileno: { type: Number, unique: false, required: false },
address: { type: String, unique: false, required: false },
gender: { type: String, unique: false, required: false },


level:{ type: String, unique: false, required: false },
task_id:{ type: Number, unique: false, required: false },
from:{ type: String, unique: false, required: false },

skill_name:{ type: String, unique: false, required: false },
skill_level:{ type: String, unique: false, required: false },
skill_verification:{ type: Boolean, unique: false, required: false },
skill_rating:{ type: String, unique: false, required: false },


status:{ type: String, unique: false, required: false },
start_year:{ type: Number, unique: false, required: false },
end_year:{ type: String, unique: false, required: false },
college:{ type: String, unique: false, required: false },
board:{ type: String, unique: false, required: false },
cgpa_percentage:{ type: Number, unique: false, required: false },
stream:{ type: String, unique: false, required: false },
degree:{ type: String, unique: false, required: false },

profile:{ type: String, unique: false, required: false },
organization:{ type: String, unique: false, required: false },
location:{ type: String, unique: false, required: false },
description:{ type: String, unique: false, required: false },
start_date:{ type: String, unique: false, required: false },
end_date:{ type: String, unique: false, required: false },

title:{ type: String, unique: false, required: false },
link:{ type: String, unique: false, required: false },
github_link:{ type: String, unique: false, required: false },
hackerrank_link:{ type: String, unique: false, required: false },
hackerearth_link:{ type: String, unique: false, required: false },
blog_link:{ type: String, unique: false, required: false },
linkedln_link:{ type: String, unique: false, required: false },
other_link:{ type: String, unique: false, required: false },
playstore_link:{ type: String, unique: false, required: false },
program:{ type: String, unique: false, required: false },

current_adviser:{ type: String, unique: false, required: false },
current_year:{ type: String, unique: false, required: false },
altered_to:{ type: String, unique: false, required: false },
alter_reason:{ type: String, unique: false, required: false },
done_by:{ type: String, unique: false, required: false },
received:{type: Date, default: Date.now}


})
resumeSchema.index({title:"text"});

resumeSchema.plugin(autoIncrement.plugin, { model: 'Resume_Data', field: 'serial', startAt: 1,incrementBy: 1 });



var counter = mongoose.model('resume_data', resumeSchema);




module.exports = counter
