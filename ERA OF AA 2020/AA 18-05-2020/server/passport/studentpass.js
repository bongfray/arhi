const Suser = require('../database/models/FAC/user')
const LocalStrategy = require('passport-local').Strategy

const strategy = new LocalStrategy(
	{
		usernameField: 'username'
	},
	function(username, password, done) {
		Suser.findOne({ username: username }, (err, user) => {
			if (err) {
				return done(err)
			}
			if (!user) {
				return done(null, false, { message: 'Incorrect username' })
			}
			if (!user.checkPassword(password)) {
				return done(null, false, { message: 'Incorrect password' })
			}
			if(user)
			{
				if (user.sem_break === true) {
					return done(null, false, { message: 'Kindly contact with your faculty adviser !!' })
				}
				if (user.suspension_status === true) {
					return done(null, false, { message: 'Account Suspended !!' })
				}
				if (user.active === false) {
					return done(null, false, { message: 'Your account is not activated !!' })
				}
			}
			return done(null, user)
		})
	}
)

module.exports = strategy
