import React, { Component } from 'react';
import BugReportIcon from '@material-ui/icons/BugReport';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import axios from 'axios';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class Bug extends Component {
  constructor()
  {
    super()
    this.state={
      bug_open:false,
      concern:'',
    }
  }

  sendBug=()=>{
    axios.post('/ework/user/bug_save',{complaint_subject:'BUGS-ERRORS MSG',fresh:true,complaint:this.state.concern})
    .then( res => {
        this.setState({bug_open:false,concern:false})
    });
  }

  render() {
    return (
      <React.Fragment>
      <Dialog
        open={this.state.bug_open}
        TransitionComponent={Transition}
        keepMounted
        onClose={()=>this.setState({bug_open:false})}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title" style={{textAlign:'center'}}>Report Bug-Error</DialogTitle>
        <DialogContent>
                       <TextField
                         id="outlined-multiline-static"
                         label="Your Concern"
                         multiline
                         rows="4"
                         fullWidth
                         name="reply_message"
                         value={this.state.concern}
                         onChange={(e)=>this.setState({concern:e.target.value})}
                         variant="filled"
                       />


        </DialogContent>
        <DialogActions>
          <Button onClick={()=>this.setState({bug_open:false})} color="secondary">
            CLOSE
          </Button>
          <Button disabled={this.state.concern ? false : true} onClick={this.sendBug} color="primary">
            SEND
          </Button>
        </DialogActions>
      </Dialog>
      <div style={{position:'fixed',bottom: 0,right: 0,padding:'0px 5px 0px 0px'}} >
         <BugReportIcon onClick={()=>this.setState({bug_open:true})}/>
      </div>
      </React.Fragment>

    );
  }
}
