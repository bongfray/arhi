import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import {CircularProgress,IconButton} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import EditIcon from '@material-ui/icons/Edit';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';

import Allot2 from './slot2';
import Switch from './absent';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




export default class Color extends Component {
	constructor(props) {
    super(props)
    this.state = {
					day:[
						{time:"8:00-8:50",start_time:"8:00",end_time:"8.50",number:'1'},
						{time:"8:50-9:40",start_time:"8:50",end_time:"9.40",number:'2'},
						{time:"9:45-10:35",start_time:"9:45",end_time:"19.10",number:'3'},
						{time:"10:40-11:30",start_time:"10:40",end_time:"11.30",number:'4'},
						{time:"11:35-12:25",start_time:"11:35",end_time:"12.25",number:'5'},
						{time:"12:30-1:20",start_time:"12:30",end_time:"13.20",number:'6'},
						{time:"1:25-2:15",start_time:"13:25",end_time:"14.15",number:'7'},
						{time:"2:20-3:10",start_time:"14:20",end_time:"15.10",number:'8'},
						{time:"3:15-4:05",start_time:"15:15",end_time:"16.05",number:'9'},
						{time:"4:05-4:55",start_time:"16:05",end_time:"16.55",number:'10'}
					],
    }
  }
render() {
	if(this.props.opendir === 'r_class')
			{
			return(
					<Grid container spacing={1} style={{marginTop:'60px'}}>
								{this.state.day.map((content,index)=>{
										return(
													<Allotment key={index} username={this.props.username} date={this.props.date}
														month={this.props.month} year={this.props.year}
														day_order={this.props.day_order} content={content}
															 />
													);
									})}
						</Grid>
						);
			}
			else if(this.props.opendir ==="own_ab")
	    {
	      return(
	        <React.Fragment>
		         <Switch day_seq={this.state.day} username={this.props.username} render_div={this.state.day}
						   date={this.props.date}
							 month={this.props.month} year={this.props.year} day_order={this.props.day_order} />
	        </React.Fragment>
	      );
	    }
	    else{
	      return(
	        <Typography align="center" style={{fontSize:'20px',marginTop:'105px'}}>Please Select from the DropDown</Typography>
	      );
	    }
  }
}


class Allotment extends Component{
	constructor(props)
	{
		super(props)
		this.state ={
			loading: true,
			color:'red',
			alreadyhave:'',
			slot:'',
			day_slot_time:'',
			time:'',
			block:'block',
			permit:false,
			received_data:'',
			datas_a:'',
			day_order:'',
			hour:'',
		}
this.componentDidMount = this.componentDidMount.bind(this)
	}

collect()
{
	axios.post('/ework/user/fetchAllotments',{day_order: this.props.day_order,
		hour:this.props.content.number,
	}
)
	.then(response =>
		{
		if(response.data)
		{
			  let allotdata = (response.data.alloted_slots).toUpperCase();
				this.setState({
					datas_a:response.data,
					alreadyhave: allotdata,
				})
		}
		else
		{
			this.setState({
				alreadyhave: 'Free Slot',
			})
		}
	})
}


color =() =>{
	axios.post('/ework/user/fetchfrom', {
		day_order: this.props.day_order,
		hour:this.props.content.number,
		date: this.props.date,
		month: this.props.month,
		year: this.props.year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
				})
		}
	})

}
comp =()=>{
	axios.post('/ework/user/extra_slot',this.props)
	.then(res => {
		this.setState({loading:false})
    if(res.data === 'yes')
		{
			this.setState({
				block:'none',
			})
		}
	});
}


block =()=>{
	axios.post('/ework/user/knowabsent',{	day_order: this.props.day_order,
	hour:this.props.content.number,
	date:this.props.date,month:this.props.month,year:this.props.year})
	.then(res => {
	   if(res.data === 'yes')
			{
				this.setState({
					block:'none',
				})
			}
	});
}
	componentDidMount()
	{
		this.collect();
		this.color();
		this.block();
		this.comp();
	}

showContent =(date,month,year,slot,time,day_order,hour)=>{
	this.setState({permit:!this.state.permit,date:date,month:month,year:year,
		slot:slot,time:time,day_order:day_order,hour:hour})
}
	showInfo=()=>{
		this.setState({
			permit:!this.state.permit,
		})
	}

	render()
	{
		if(this.state.loading === true)
		{
			return(
         <Grid item xs={12} sm={4} key={this.props.time}>
				    <Typography align="center">Loading Content...</Typography>
          </Grid>
			)
		}
		else
		{
		if(this.state.color === "red")
		{

			return(
					<Grid item xs={12} sm={4} style={{display:this.state.block}} key={this.props.time}>
								<Allot2 color={this.color} content={this.props.content} date={this.props.date}
								 month={this.props.month} year ={this.props.year}
								 username={this.props.username} slots={this.state.alreadyhave} day_order={this.props.day_order}
								 datas_a={this.state.datas_a}
								 />
					</Grid>
			);
		}
		else if(this.state.color === "green")
		{
			return(
				<React.Fragment>
				{this.state.permit &&
					<Receive show={this.showInfo} permit={this.state.permit} date={this.props.date}
					 month={this.props.month} year ={this.props.year}
					 slot={this.state.slot} time={this.state.time}
					 day_order={this.props.day_order} hour={this.state.hour} />
				}
					<Grid item xs={12} sm={4} style={{color: this.state.color}} onClick={()=>this.showContent(
						this.props.date,this.props.month,this.props.year,this.state.alreadyhave,this.props.content.time, this.props.day_order,this.props.content.number
					)}  key={this.props.time}>
						<Paper elevation={3} style={{backgroundColor:'green',color:'white',padding:'5px'}}>
								<Typography align="center" style={{color:'yellow'}} variant="button" display="block" gutterBottom>
									{this.props.content.time} [{this.props.content.number}]
								</Typography>
								<Typography align="center" variant="button" display="block" gutterBottom>
									<b>COMPLETED</b>
								</Typography>
						</Paper>
					</Grid>
				</React.Fragment>
			);
		}
}

	}
}



class Receive extends Component {
	constructor()
	{
		super()
		this.state={
			received_data:[],
			editable:false,
			covered:'',
			loading:true,
			snack_open:false,
			alert_type:'',
			snack_msg:'',
      verification_link:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}
	componentDidMount()
	{
		this.fetchDatas();
	}

	fetchDatas =()=>{
		axios.post('/ework/user/fetch_submitted_data',{date:this.props.date,month:this.props.month,
			year:this.props.year,slot:this.props.slot,time:this.props.time,day_order:this.props.day_order,
		hour:this.props.hour})
		.then( res => {
				if(res.data === 'no')
				{
					this.setState({
						snack_open:true,
						snack_msg:'No Data Found !!',
						alert_type:'error',
					});
					this.props.show();
				}
				else{
					this.setState({received_data:res.data,covered:res.data.covered,verification_link:res.data.verification_link,loading:false})
				}
		});
	}

	handleEdit =()=>{
		this.setState({editable:!this.state.editable})
	}

	valueChange =(e)=>{
		this.setState({covered:e.target.value})
	}
  VerifyChange =(e)=>{
    this.setState({verification_link:e.target.value})
  }

   sendUpdates =(e)=>{
		 this.setState({
			 snack_open:true,
			 snack_msg:'Updating.....',
			 alert_type:'info',
		 });
		if(this.state.covered === this.state.received_data.covered)
		{
			this.setState({
				snack_open:true,
				snack_msg:'Updated !!',
				alert_type:'success',
			});
					this.handleEdit();
		}
		else{
			axios.post('/ework/user/updateSubmittedData',
			{data:this.state.received_data,covered:this.state.covered,verification_link:this.state.verification_link})
			.then( res => {
			    if(res.data === 'ok')
					{
						this.setState({
							snack_open:true,
							snack_msg:'Updated !!',
							alert_type:'success',
						});
							this.handleEdit();
							this.fetchDatas();
					}
			})
			.catch( err => {
				this.setState({
					snack_open:true,
					snack_msg:'Something Went Wrong !!',
					alert_type:'error',
				});
			});
		}
	}

  render() {
    return (
      <div>
			<Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
			open={this.state.snack_open} autoHideDuration={2000}
			onClose={()=>this.setState({snack_open:false})}>
				<Alert onClose={()=>this.setState({snack_open:false})}
				severity={this.state.alert_type}>
					{this.state.snack_msg}
				</Alert>
			</Snackbar>

      <Dialog
         open={true}
         onClose={this.props.show}
         aria-labelledby="scroll-dialog-title"
         aria-describedby="scroll-dialog-description"
       >
         <DialogContent >

           {this.state.loading === false ?
               <React.Fragment>
                   <Typography variant="h5" align="center" gutterBottom>
                       Submitted Data in, <Typography color="secondary">{this.state.received_data.freefield}</Typography>
                   </Typography>

                  <Grid container spacing={1}>
                        <div style={{marginLeft:'70%'}}>
                         {this.state.received_data.verified ?
                           <Button variant="outlined" style={{color:'green'}}>Verified</Button>
                           :
                           <Button variant="outlined" style={{color:'red'}}>Not Verified</Button>
                         }
                         </div>
                   </Grid>

                 <br />

                   {this.state.received_data.freeparts &&
                     <React.Fragment>
                       <Grid container spacing={2}>
                          <Grid item xs={7} sm={7}><b>You have submitted the datas on </b></Grid>
                          <Grid item xs={5} sm={5}>{this.state.received_data.freeparts}</Grid>
                       </Grid>
                     <br />
                    </React.Fragment>
                    }

                    {this.state.received_data.slot &&
                      <React.Fragment>
                         <Grid container spacing={2}>
                             <Grid item xs={7} sm={7}><b>Slot</b></Grid>
                             <Grid item xs={5} sm={5}>{this.state.received_data.slot}</Grid>
                         </Grid>
                         <br />
                      </React.Fragment>
                    }

                    {this.state.received_data.subject_code &&
                      <React.Fragment>
                         <Grid container spacing={2}>
                             <Grid item xs={7} sm={7}><b>Subject Code</b></Grid>
                             <Grid item xs={5} sm={5}>{this.state.received_data.subject_code}</Grid>
                         </Grid>
                         <br />
                      </React.Fragment>
                    }

                    {this.state.received_data.compense_faculty &&
                      <React.Fragment>
                       <Grid container spacing={2}>
                           <Grid item xs={7} sm={7}><b>Slot Handled to Faculty</b></Grid>
                           <Grid item xs={5} sm={5}>{this.state.received_data.compense_faculty}</Grid>
                       </Grid>
                       <br />
                       </React.Fragment>
                    }


                    {this.state.received_data.compense_faculty_topic &&
                      <React.Fragment>
                         <Grid container spacing={2}>
                             <Grid item xs={12} sm={7}><b>Handled Topic by Foreign Faculty</b></Grid>
                             <Grid item xs={12} sm={5}>{this.state.received_data.compense_faculty_topic}</Grid>
                         </Grid>
                         <br />
                     </React.Fragment>
                    }

                    {this.state.received_data.verification_link &&
                      <React.Fragment>
                         <Grid container spacing={2}>
                              <Grid item xs={7} sm={7}><b>Verification Link</b></Grid>
                               <Grid item xs={5} sm={5}>
                                 <a target="_blank" rel="noopener noreferrer" style={{color:'red'}} href={this.state.received_data.verification_link}>
                                 Verification Link
                                 </a>
                               </Grid>
                         </Grid>
                         <br />
                     </React.Fragment>
                    }


                   {this.state.received_data.freefield === 'Academic' ?
                            <React.Fragment>
                                 {this.state.received_data.covered &&
                                   <Grid container spacing={2}>
                                       <Grid item xs={7} sm={7}><b>Submitted Data</b></Grid>
                                        <Grid item xs={5} sm={5}>
                                           {this.state.editable === false ?
                                            <React.Fragment>
                                             {this.state.received_data.verified === false ?
                                               <div style={{textAlign:'center',color:'red'}}>
                                                 {this.state.received_data.covered}&emsp;
                                                   <IconButton aria-label="edit34" color="secondary" onClick={this.handleEdit}>
                                                     <EditIcon fontSize="inherit" />
                                                   </IconButton>
                                               </div>
                                               :
                                               <div style={{textAlign:'center',color:'red'}}>
                                                 {this.state.received_data.covered}
                                               </div>
                                             }
                                            </React.Fragment>
                                          :
                                              <React.Fragment>
                                                  <TextField
                                                    label={'Covered Topic'}
                                                    multiline
                                                    rows="2"
                                                    fullWidth
                                                    id="edit" type="text" value={this.state.covered}
                                                    onChange={this.valueChange}
                                                    variant="filled"
                                                  /><br/><br/>
                                                   <Grid container>
                                                    <Button variant="outlined" color="secondary" onClick={this.handleEdit}>CANCEL</Button>&ensp;
                                                    <Button variant="contained" onClick={this.sendUpdates} color="primary">SUBMIT</Button>
                                                   </Grid>
                                                </React.Fragment>
                                          }
                                        </Grid>
                                  </Grid>
                                }
                          </React.Fragment>
                          :
                          <React.Fragment>
                              {this.state.received_data.covered &&
                                <Grid container spacing={1}>
                                     <Grid item xs={12} sm={12}><b>Submitted Data</b></Grid>
                                     <Grid item xs={12} sm={12}>
                                         {this.state.editable === false ?
                                                 <div style={{textAlign:'center',color:'red'}}>
                                                  {this.state.received_data.covered}&ensp;
                                                  <EditIcon onClick={this.handleEdit} className="go" />
                                                 </div>
                                                :
                                                <React.Fragment>
                                                <TextField
                                                  label={'Covered Topic'}
                                                  multiline
                                                  rows="2"
                                                  fullWidth
                                                  id="edit" className="validate" type="text" value={this.state.covered}
                                                  onChange={this.valueChange}
                                                  variant="filled"
                                                />
                                                <br/><br />
                                                <TextField
                                                  label={'Verification Link'}
                                                  fullWidth
                                                  id="edit1"  type="text" value={this.state.verification_link}
                                                  onChange={this.VerifyChange}
                                                  variant="outlined"
                                                />
                                                <br /><br />
                                                 <Grid container>
                                                   <Button variant="outlined" color="secondary" onClick={this.handleEdit}>CANCEL</Button>&ensp;
                                                    <Button variant="contained" onClick={this.sendUpdates} color="primary">SUBMIT</Button>
                                                  </Grid>
                                                 </React.Fragment>
                                         }
                                     </Grid>
                                </Grid>
                               }
                               <br />
                          </React.Fragment>
                        }

                      {this.state.received_data.problem_statement==="true" &&
                        <React.Fragment>
                          <Grid container spacing={1}>
                             <Grid item xs={8} sm={7}><b>Submitted Data</b></Grid>
                             <Grid item xs={4} sm={5}>Class was not taken.<br />Reason : {this.state.received_data.problem_statement}</Grid>
                          </Grid>
                          <br />
                        </React.Fragment>
                       }

                 <Divider light />
                   <br />
                   <div>
                   <Typography style={{fontSize:'15px'}} color="secondary" variant="button" display="block" gutterBottom>
                   REMEMBER:
                   </Typography><br />
                      1. You can only edit the datas within 48 hours.<br />
                      2. Your submitted datas will not be validated,if it's showing NOT VERIFIED.<br />
                      3. After it is verified, there is no chance to edit it.<br />
                      4. This page will let you to edit the datas only for 24 Hrs. For rest of the
                      24 hours, go for yeaterday dayorder.Rules & Regulation will be applied.
                   </div>
                </React.Fragment>
                :
                <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
                  <CircularProgress color="secondary" />
                </div>
              }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.show} color="primary">
              CLOSE
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );

  }
}
