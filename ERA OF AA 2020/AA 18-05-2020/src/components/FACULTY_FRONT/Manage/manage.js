import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import Nav from '../../dynnav'
import SingleManage from './single_user_manage'
import OneSlot from './global_manage'
import Insturction from './instra.'
import BluePrint from './bluePrintReq'

import ContentLoader from "react-content-loader"
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';



export default class Manage extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      loading: true,
      username:'',
      modal:false,
      redirectTo:'',
      option:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      noti_route:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  getCount =() =>{
    axios.get('/ework/user/knowcount', {
    })
    .then(response =>{
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}

handleOption = (e) =>{
  this.setState({option:e.target.value})
}
componentDidMount(){
  this.getCount()
  axios.get('/ework/user/',
  this.setState({loading: true})
)
   .then(response =>{
     this.setState({loading: false})
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/ework/faculty',
       });
       window.M.toast({html: 'You are not Logged In',classes:'rounded red'});
     }
   })
}


  render()
  {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="35" y="29" rx="3" ry="3" width="330" height="30" />
      </ContentLoader>
    )
    if(this.state.loading ===  true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route}
       get={this.state.get} logout={this.state.logout}/>
      {this.state.modal=== false &&
        <div onClick={this.selectModal} className="animat bounce"
        style={{margin:'10px 20px 0px 0px',cursor: 'pointer',textAlign:'right'}}>
          <InfoOutlinedIcon color="secondary" />
        </div>
      }
      <Insturction
          displayModal={this.state.modal}
          closeModal={this.selectModal}
      />
      <Grid container>
      <Hidden xsDown>
        <Grid item sm={2} />
      </Hidden>
      <Grid item xs={12} sm={8}>
        <Paper elevation={3} style={{padding:'10px'}}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={6}>
              <Typography variant="h5" align="center" gutterBottom>
                Kindly Choose From Here
              </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl style={{width:'100%'}}>
                   <InputLabel id="type">Select Here</InputLabel>
                   <Select
                     labelId="type"
                     id="type"
                     name="type" value={this.state.option} onChange={this.handleOption}
                   >
                   <MenuItem value="one">Request For a Missed DayOrder Entry</MenuItem>
                   <MenuItem value="one-slot">Request For Missed Slot Entry</MenuItem>
                   <MenuItem value="blue_print">Blue Print Manage</MenuItem>
                   </Select>
                 </FormControl>
              </Grid>
            </Grid>
         </Paper>
      </Grid>
      <Hidden xsDown>
        <Grid item sm={2} />
      </Hidden>
      </Grid>
      <br />
      <Navigate username={this.state.username} selected={this.state.option} />
      </React.Fragment>

    )
  }
}
  }
}


class Navigate extends Component{
  constructor()
  {
    super()
    this.state ={

    }
  }
  render()
  {
    let object;
    if(this.props.selected ==="one")
    {
      object = <SingleManage selected={this.props.selected} username={this.props.username} />
    }
    else if(this.props.selected === "one-slot")
    {
      object = <OneSlot selected={this.props.selected}  username={this.props.username} />
    }
    else if (this.props.selected === 'blue_print') {
      object = <BluePrint selected={this.props.selected}  username={this.props.username} />
    }
    else {
      object =<div></div>
    }
    return(
      <div>
       {object}
      </div>
    )
  }
}
