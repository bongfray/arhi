import React, { Component } from 'react'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import BarChart from '../../STUDENT_FRONT/CHART-OF-SKILL/bar-chart'
import Resume from '../../STUDENT_FRONT/resume'
import Task from '../../STUDENT_FRONT/ARHI/taskManage'





const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});






export default class ViewType extends Component {
	constructor()
	{
		super()
		this.state={
      radio:[{value:'Skills-Rating'},{value:'Task History'},
      {value:'Resume Data'},],
      choosed: '',
      active:'',
      open_sidebar:true,
		}
	}

  handleChecked =(e,index,value)=>{
      this.setState({
          choosed: value
      });
      this.closeDrawer();
  }
  closeDrawer=()=>{
    this.setState({open_sidebar:!this.state.open_sidebar})
  }

  render()
  {
    return(

      <Dialog fullScreen open={this.props.data.details_view} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={1} sm={1}>
                  <IconButton edge="start" color="inherit" onClick={this.props.closeView} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={10} sm={10}>
                 <div style={{textAlign:'center',fontSize:'20px'}}>Showing Details of <span className="yellow-text">{this.props.data.data.username}</span></div>
               </Grid>
               <Grid item xs={1} sm={1}>
                 <div className="right">
                   <IconButton onClick={this.closeDrawer} style={{backgroundColor:'yellow'}}>
                     <ChevronLeftIcon style={{color:'red'}} />
                   </IconButton>
                 </div>
               </Grid>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
          <div style={{marginTop:'70px'}}>
            {this.state.open_sidebar &&
              <Drawer style={{width:'550px'}} anchor="right" open={this.state.open_sidebar} onClose={this.closeDrawer}>
                <IconButton onClick={this.closeDrawer}>
                  <ChevronRightIcon />
                </IconButton>
                <Divider />
                  <List>
                      {this.state.radio.map((content,index)=>(
                        <ListItem button key={index}  onClick={(e)=>{this.handleChecked(e,index,content.value)}}>
                          <ListItemText primary={content.value} />
                        </ListItem>
                      ))}
                  </List>
              </Drawer>
            }
                 <Navigator prop_data={this.props.data} choosed={this.state.choosed} />
            </div>
          </List>
        </Dialog>
    )
  }
}


class Navigator extends Component{
  constructor()
  {
    super()
    this.state={

    }
  }
  render()
  {
    if(this.props.choosed === 'Skills-Rating')
    {
      return(
        <BarChart prop_data={this.props.prop_data} />
      )
    }
    else if(this.props.choosed === 'Task History')
    {
      return(
        <Task prop_data={this.props.prop_data} />
      )
    }
    else if(this.props.choosed === 'Resume Data')
    {
      return(
        <Resume prop_data={this.props.prop_data}  />
      )
    }
    else{
      return(
              <div></div>
      )
    }
  }

}
