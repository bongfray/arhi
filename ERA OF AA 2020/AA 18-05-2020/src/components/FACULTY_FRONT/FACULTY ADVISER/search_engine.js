import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Paper,InputBase,Divider,IconButton} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

export default function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search By Student's Id" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search By Student Id">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
