import React, { Component} from 'react';
import axios from 'axios'
import {Link,Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogContentText from '@material-ui/core/DialogContentText';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Backdrop from '@material-ui/core/Backdrop';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class BluePrint extends Component {
	constructor() {
    super();
    this.state = {
			loading: true,
			datas: [
				{
					day_order:'Day Order 1',dayor:'1',
					day:[
						{id:"1.1"},{id:"1.2"},{id:"1.3"},{id:"1.4"},{id:"1.5"},{id:"1.6"},{id:"1.7"},{id:"1.8"},{id:"1.9"},{id:"1.10"}
					]

				},
				{
					day_order:'Day Order 2',dayor:'2',
					day:[
						{id:"2.1"},{id:"2.2"},{id:"2.3"},{id:"2.4"},{id:"2.5"},{id:"2.6"},{id:"2.7"},{id:"2.8"},{id:"2.9"},{id:"2.10"}
					]
				},
				{
					day_order:'Day Order 3',dayor:'3',
					day:[
						{id:"3.1"},{id:"3.2"},{id:"3.3"},{id:"3.4"},{id:"3.5"},{id:"3.6"},{id:"3.7"},{id:"3.8"},{id:"3.9"},{id:"3.10"}
					]
				},
				{
					day_order:'Day Order 4',dayor:'4',
					day:[
						{id:"4.1"},{id:"4.2"},{id:"4.3"},{id:"4.4"},{id:"4.5"},{id:"4.6"},{id:"4.7"},{id:"4.8"},{id:"4.9"},{id:"4.10"}
					]
				},
				{
					day_order:'Day Order 5',dayor:'5',
					day:[
						{id:"5.1"},{id:"5.2"},{id:"5.3"},{id:"5.4"},{id:"5.5"},{id:"5.6"},{id:"5.7"},{id:"5.8"},{id:"5.9"},{id:"5.10"}
					]
				}
			],
			user:'',
			username:'',
			home:'/ework/faculty',
			logout:'/ework/user/logout',
			get:'/ework/user/',
			noti_route:true,
      nav_route: '/ework/user/fetchnav',
			display:'block',
    };

		this.componentDidMount = this.componentDidMount.bind(this);
    }

    fetchlogin = () =>{
      axios.get('/ework/user/'
	)
      .then(response =>{
        if(response.data.user === null)
				{
					this.setState({loading: false})
          this.setState({
            redirectTo:'/ework/',
          });
        }
				else
				{

					this.setState({username:response.data.user.username,user:response.data.user})
						this.fetchRenderStatus();
				}
      })
    }

    componentDidMount() {
			if(this.props.props_to_refer)
			{
				this.setState({username:this.props.props_to_refer.username,loading:false})
			}
			else
			{
				this.fetchlogin();
			}

    }

		fetchRenderStatus=()=>{
			axios.post('/ework/user/fac_status',{userEnable:'verify'})
					.then(response => {
						this.setState({loading: false})
						if(response.data === 'no')
						{
							this.setState({display:'none'})
						}
						else if(response.data === 'ok')
						{
							this.setState({display:'block'})
						}
						else
						{
							var fac_blueprint_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="BLUE PRINT STATUS")));
							if((fac_blueprint_status.length)>0)
							{
								if(fac_blueprint_status[0].status === true)
								{
									this.setState({display:'block'})
								}
								else {
									this.setState({display:'none'})
								}
							}
							else {
								this.setState({status:'block'})
							}
						}
					})
          .catch( err => {
              this.setState({loading:false})
          });
		}


render() {
	const MyLoader = () => (
		<ContentLoader
		  height={160}
		  width={400}
		  speed={2}
		  primaryColor="#f3f3f3"
		  secondaryColor="#c0c0c0"
		>
		  <rect x="24" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="35" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="55" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="75" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="95" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="115" rx="0" ry="0" width="38" height="7" />

		</ContentLoader>
	  )
	if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo)
	{
     return <Redirect to={{ pathname: this.state.redirectTo }} />
  }
	else
	 {
		 if(this.state.display === 'none')
		 {
			 return(
				 <Grid container spacing={1}>
				    <Grid item xs={3} sm={3}/>
						<Grid item xs={6} sm={6}>
									<Card style={{marginTop:'60px'}}>
									<CardContent style={{textAlign:'center'}}>
									<Typography gutterBottom variant="h4" style={{color:'red'}} component="h2">
										Oops !!
									</Typography>
									 <Typography variant="button" display="block" gutterBottom>
										 SUBMISSION OF ANY DATA IN THIS PAGE HAS BEEN CLOSED BY THE ADMIN
									 </Typography>
									 <Typography variant="overline" display="block" gutterBottom>
										 Send request for re-rendering this page in <Link style={{textDecoration:'none'}} to="/ework/manage">Manage Request Section</Link>
									 </Typography>
									</CardContent>
									<CardActions>
									 <Button size="small" color="primary" style={{float:'right'}}>
									   <Link style={{textDecoration:'none'}} to="/ework/faculty" className="btn small blue-grey darken-2">HOME</Link>
									 </Button>
									</CardActions>
									</Card>
						</Grid>
						<Grid item xs={3} sm={3}/>
				 </Grid>
			 );
		 }
		 else
		 {
			return (
				<React.Fragment>
				 {!(this.props.props_to_refer) &&
					 <Nav noti_route={this.state.noti_route}
					 home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
				 }
				 <TableContainer component={Paper}>
					<Table aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell  style={{borderRight: '1px solid'}} align="center">Hour /Day Order</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>1</b></span>
								<br />08:00 - 08:50
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>2</b></span>
								<br />08:50 - 09:40
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>3</b></span>
								<br />09:45 - 10:35
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>4</b></span>
								<br />10:40 - 11:30
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>5</b></span>
								<br />11:35 - 12:25
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>6</b></span>
								<br />12:30 - 01:20
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>7</b></span>
								<br />01:25 - 02:15
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>8</b></span>
								<br />02:20 - 03:10
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>9</b></span>
								<br />03:15 - 04:05
							</TableCell>
							<TableCell  style={{borderRight: '1px solid'}} align="center">
								<span style={{color:'red'}}><b>10</b></span>
								<br />04:05 - 04:55
							</TableCell>
						</TableRow>
					</TableHead>
						<TableBody>
							{this.state.datas.map((content,index)=> (
								<TableRow key={index}>
										<Stat content={content} dayor={content.dayor} user={this.state.user}
										 username = {this.state.username}/>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
		</React.Fragment>
		);
	 }
  }
}
}
}


class Stat extends Component{
	constructor()
	{
		super()
		this.state={
			isAddProduct: false,
			response: {},
			action:'',
			product: {},
			isEditProduct: false,
			added:'',
			disabled:'block',
			editing:'',
			snack_open:false,
			alert_type:'',
			snack_msg:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}

	componentDidMount()
	{
	}

	    onCreate = (e,action) => {
	      this.setState({ isAddProduct: action ,product: {},disabled:'none'});
	    }
	    onFormSubmit =(data) => {
			  this.setState({submit:true})
	      let apiUrl;
	      if(this.state.isEditProduct)
	      {
	        apiUrl = '/ework/user/edit_blue_print';
	      }
	       else
	       {
	        apiUrl = '/ework/user/send_blue_print';
	       }
	      axios.post(apiUrl,data)
	          .then(response => {
              this.setState({submit:false})
              if(response.data === 'no')
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Record is missing !!',
                  alert_type:'error',
                })
              }
							else if(response.data.exceed)
							{
								this.setState({
									snack_open:true,
									snack_msg:response.data.exceed,
									alert_type:'error',
								})
							}
							else if(response.data.succ)
							{
								this.setState({
									snack_open:true,
									snack_msg:response.data.succ,
									alert_type:'success',
								})
							}
							else if(response.data.emsg)
							{
								this.setState({
									snack_open:true,
									snack_msg:response.data.emsg,
									alert_type:'error',
								})
							}
							if(this.state.isEditProduct)
							{
							 this.setState({added:data.serial})
							}
							 else
							 {
								this.setState({added:this.state.isAddProduct})
							 }

							this.setState({
								response: response.data,
								isEditProduct: false,
								isAddProduct: false,
								disabled:'block',
								editing:'',
							})
	          })
            .catch( err => {
              this.setState({
                submit:false,
                snack_open:true,
                snack_msg:'Something Went Wrong !!',
                alert_type:'error',
              })
            });
	    }

	    editProduct = (productId,index)=> {
				 this.setState({editing:productId,submit:true})
	      axios.post('/ework/user/show_editable_blue_print',{
	        id: productId,
	      })
	          .then(response => {
	            this.setState({
                submit:false,
	              product: response.data,
	              isEditProduct: index,
	              isAddProduct: index,
	            });
	          })
            .catch( err => {
              this.setState({
                submit:false,
                snack_open:true,
                snack_msg:'Something Went Wrong !!',
                alert_type:'error',
              })
            });
	   }

		 close=()=>{
			 this.setState({editing:'',isAddProduct:'',isEditProduct:''})
		 }

		 displayPops=(id)=>{
		 	this.setState({popOver:true,popId:id})
		 }
		 displayPopsOff=(id)=>{
			this.setState({popOver:false,popId:id})
		 }

	render()
	{
    if(this.state.submit)
    {
      return(
        <Backdrop open={true} style={{zIndex:'2024'}}>
          <CircularProgress style={{color:'yellow'}} />&emsp;
          <div style={{color:'yellow',textAlign:'center'}}>Processing Your Request....</div>
        </Backdrop>
      )
    }
    else
    {
		return(
			<React.Fragment>

			<Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
			open={this.state.snack_open} autoHideDuration={2000}
			onClose={()=>this.setState({snack_open:false})}>
				<Alert onClose={()=>this.setState({snack_open:false})}
				severity={this.state.alert_type}>
					{this.state.snack_msg}
				</Alert>
			</Snackbar>


			<TableCell  style={{borderRight: '1px solid'}}  align="center">
			   {this.props.content.day_order}{this.state.week}
			</TableCell>
				 {this.props.content.day.map((content,ind)=>{
						 return(
							 <TableCell style={{borderRight: '1px solid'}} align="center" onMouseLeave={()=>this.displayPopsOff(content.id)} onMouseEnter={()=>this.displayPops(content.id)} key={content.id}>
											<ShowDetails editing={this.state.editing} creating={this.state.isAddProduct}
											 create={this.onCreate} added={this.state.added} data={content}
											 username={this.props.username}  action={content.id}
											 editProduct={this.editProduct} popOver={this.state.popOver} popId={this.state.popId}
											 />
											{((this.state.isAddProduct === content.id) || (this.state.isEditProduct === content.id)) &&
											<Add data={content}  action={content.id} username={this.props.username}
											close={this.close} user={this.props.user}
											onFormSubmit={this.onFormSubmit}  product={this.state.product} />
											}
							 </TableCell>
						 );
					 })}
			</React.Fragment>
		);
   }
	}
}




class Add extends Component {
	constructor(props) {
    super(props);
    this.initialState = {
			timing:'',
      username:'',
      action:'',
			subject_code:'',
			status:false,
			snack_open:false,
			alert_type:'',
			snack_msg:'',
      batch_ref:0,
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (e,index) =>{
    const value = e.target.value;
    this.setState({
      [e.target.name]:value.toUpperCase()
    })
  }


componentDidMount(){
  this.setState({
    action:this.props.data.id,
		timing:this.props.data.id,
    username: this.props.username,
		status:true,
		faculty_name:this.props.user.name,
  })
}



  handleSubmit(event,index) {
      event.preventDefault();
			if(!(this.state.year) || !(this.state.sem) || !(this.state.batch) ||  !(this.state.alloted_slots))
			{
				this.setState({
					snack_open:true,
					snack_msg:'Enter all the details !!',
					alert_type:'warning',
				})
			}
			else{
				this.props.onFormSubmit(this.state);
				this.setState(this.initialState);
			}
  }

  render() {

    const number=[{id:'A'},{id:'B'},{id:'C'},{id:'D'},{id:'E'},{id:'F'},{id:'G'},{id:'H'},{id:'I'},{id:'J'},{id:'K'},{id:'L'},{id:'M'}
  ,{id:'N'},{id:'O'},{id:'P'},{id:'Q'},{id:'R'},{id:'S'},{id:'T'},{id:'U'},{id:'V'},{id:'W'},{id:'X'},{id:'Y'},{id:'Z'}]

    return(
      <React.Fragment>

				<Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
				open={this.state.snack_open} autoHideDuration={2000}
				onClose={()=>this.setState({snack_open:false})}>
					<Alert onClose={()=>this.setState({snack_open:false})}
					severity={this.state.alert_type}>
						{this.state.snack_msg}
					</Alert>
				</Snackbar>

			<Dialog open={true} fullWidth onClose={this.props.close}>
					<DialogTitle id="alert-dialog-title" align="center">Enter the slot Details</DialogTitle>
					<DialogContent>
								<TextField
				          id="filled-textarea1"
				          label="Enter Slot Name"
				          variant="outlined"
									type="text"
									name="alloted_slots"
									value={this.state.alloted_slots}
									onChange={e => this.handleD(e,this.props.data.id)}
									fullWidth
				        />
								<br /><br />
								<TextField
									id="filled-textarea2"
									label="Enter Subject Code"
									variant="outlined"
									type="text"
									name="subject_code"
									value={this.state.subject_code}
									onChange={e => this.handleD(e,this.props.data.id)}
									fullWidth
								/>
             <br /><br />
						  <FormGroup row>
									<FormControl style={{width:'100%'}}>
										<InputLabel id="sel_year">Year</InputLabel>
											<Select
												labelId="sel_year"
												id="sel_year"
												name="year"
												value={this.state.year}
												onChange={this.handleD}
											>
											<MenuItem value="" disabled defaultValue>Year</MenuItem>
											<MenuItem value="1">1</MenuItem>
											<MenuItem value="2">2</MenuItem>
											<MenuItem value="3">3</MenuItem>
											<MenuItem value="4">4</MenuItem>
											<MenuItem value="5">5</MenuItem>
											</Select>
									 </FormControl>
									 <FormControl style={{width:'100%'}}>
										 <InputLabel id="sel_sem">Sem</InputLabel>
											 <Select
												 labelId="sel_sem"
												 id="sel_sem"
												 name="sem"
												 value={this.state.sem}
												 onChange={this.handleD}
											 >
											 <MenuItem value="" disabled defaultValue>Sem</MenuItem>
											 <MenuItem value="1">1</MenuItem>
											 <MenuItem value="2">2</MenuItem>
											 <MenuItem value="3">3</MenuItem>
											 <MenuItem value="4">4</MenuItem>
											 <MenuItem value="5">5</MenuItem>
											 <MenuItem value="6">6</MenuItem>
											 <MenuItem value="7">7</MenuItem>
											 <MenuItem value="8">8</MenuItem>
											 <MenuItem value="9">9</MenuItem>
											 <MenuItem value="10">10</MenuItem>
											 </Select>
										</FormControl>
                    <FormControl style={{width:'100%'}}>
  										<InputLabel id="sel_batch">Batch Type</InputLabel>
  											<Select
  												labelId="sel_batch"
  												id="sel_batch"
  												name="batch"
  												value={this.state.batch_ref}
  												onChange={(e)=>this.setState({batch_ref:e.target.value})}
  											>
  											{number.map((content,index)=>{
  												return(
  													<MenuItem key={index} value={content.id}>{content.id}</MenuItem>
  												)
  											})}
  											</Select>
  									 </FormControl>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_batch">Batch</InputLabel>
												<Select
													labelId="sel_batch"
													id="sel_batch"
													name="batch"
													value={this.state.batch}
                          disabled={this.state.batch_ref ? false:true}
													onChange={this.handleD}
												>
                        {Array.apply(null, {length: 10}).map((i, index)=>{
  												return(
  													<MenuItem key={index} value={this.state.batch_ref+(index+1)}>{this.state.batch_ref}{index+1}</MenuItem>
  												)
  											})}
												</Select>
										 </FormControl>

							</FormGroup>
					</DialogContent>
					 <br />
					 <DialogActions>
						<Button variant="outlined" onClick={this.props.close} color="primary">
							CANCEL
						</Button>
						<Button variant="contained" onClick={this.handleSubmit} color="secondary">
							SUBMIT
						</Button>
					</DialogActions>

			</Dialog>
      </React.Fragment>
    )
  }
}



class ShowDetails extends Component {
	constructor(props) {
    super(props);
    this.state = {
      username:'',
      error: null,
      products: [],
      action:'',
			loading:true,
			render_confirm:false,
			productId:0,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if ((prevProps.action !== this.props.action) || (prevProps.added !== this.props.added)) {
      this.fetch();
    }
  }

	deleteProduct=(productId,index)=>{
 	 this.setState({render_confirm:!this.state.render_confirm,productId:productId});
  }

  confirm = () => {
  const { products } = this.state;
  axios.post('/ework/user/del_blue_print',{
    serial: this.state.productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
					render_confirm:false,
					productId:0,
          products: products.filter(product => product.serial !== this.state.productId)
       })
      })
}


fetch =(e) =>{
	let route;
	if(e)
	{
		route = e;
	}
	else
	{
	route =this.props.action;
 }
  axios.post('/ework/user/fetch_blue_print',{
    action: route,
		username:this.props.username,
  })
  .then(response =>{
    this.setState({
			loading:false,
      products: response.data,
   })
  })
}

  render() {
		//console.log(this.props.username)

      return(
        <React.Fragment>
					<Dialog
	          open={this.state.render_confirm}
            onClose={this.deleteProduct}
	          aria-labelledby="alert-dialog-title"
	          aria-describedby="alert-dialog-description"
	        >
	          <DialogTitle id="alert-dialog-title">Confirmation of Deleting the Data</DialogTitle>
	          <DialogContent>
	            <DialogContentText id="alert-dialog-description">
							  This Action will permanently delete you data
	            </DialogContentText>
	          </DialogContent>
	          <DialogActions>
	            <Button style={{float:'left'}} onClick={this.deleteProduct} color="primary">
	              Disagree
	            </Button>
	            <Button style={{float:'right'}} onClick={this.confirm} color="primary" autoFocus>
	              Agree
	            </Button>
	          </DialogActions>
	        </Dialog>

				{this.state.loading === false ?
						<React.Fragment>
									{this.state.products.length!==0 ?
										<React.Fragment>
											        {this.state.products.map((content,index)=>(
																<React.Fragment key={index}>
																	{!(this.props.editing === content.serial) &&
																		<div style={{display:this.props.disabled}}>

																			{((this.props.popOver) && (this.props.popId === this.props.action)) &&
																				<div className="popup_over">
																					<EditIcon onClick={() => this.props.editProduct(content.serial,this.props.action)} />
																					<DeleteIcon onClick={() => this.deleteProduct(content.serial,this.props.action)} />
																			 </div>
																		  }
																				       <Grid container spacing={1}>
																							    <Grid item xs={4} sm={4}>
																								    <span style={{color:'red'}}>Slot - </span>{content.alloted_slots}
																									</Grid>
																								  <Grid item xs={8} sm={8}>
																									   <span style={{color:'red'}}>Sub-Code - </span>{content.subject_code}
																								  </Grid>
																								</Grid>
																						 <Grid container spacing={1}>
																							   <Grid item xs={4} sm={4}>
																								   <span style={{color:'red'}}>Year : </span>{content.year}
																								 </Grid>
																							 <Grid item xs={4} sm={4}>
																							   <span style={{color:'red'}}>Sem : </span>{content.sem}
																							 </Grid>
																							 <Grid item xs={4} sm={4}>
																							   <span style={{color:'red'}}>Batch : </span>{content.batch}
																							 </Grid>
																						 </Grid>
																				</div>
																  }
																	</React.Fragment>
											        ))}
											</React.Fragment>
									 :<div>
									{!(this.props.creating === this.props.data.id) &&
										<Fab color="primary" size="small" aria-label="add" onClick={(e) => this.props.create(e,this.props.data.id)}>
											<AddIcon  />
										</Fab>
									}
									</div>
								}
						</React.Fragment>
			:
				<CircularProgress color="secondary" />
		}
			</React.Fragment>
      )
  }
}
