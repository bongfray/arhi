import React, { Component } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {CircularProgress,TextField,Typography,Backdrop} from '@material-ui/core';
import CallIcon from '@material-ui/icons/Call';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import {IconButton,Tooltip} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {MenuItem} from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import {InputBase,Divider,Paper} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}





export default class Review extends Component {
  constructor()
  {
    super()
    this.state={
      review_data:'',
      loader:true,
      isExpanded:true,
      expanded:0,
      value_for_search:'',
      filter:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchIncomings();
  }
  fetchIncomings=()=>{
    axios.post('/ework/user/fetch_skill_for_fac',{action:'Review-Action'})
    .then( res => {
        if(res.data)
        {
          this.setState({review_data:res.data,loader:false})
        }
    });
  }

    searchEngine =(e)=>{
      this.setState({value_for_search:e.target.value})
    }
    applyFilter=(object)=>{
      this.setState(object)
    }


  render() {
    if(this.state.loader)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Processing Your Request...</div>
        </Backdrop>
      )
    }
    else {
      let review_data;
      if(this.state.filter)
      {
        review_data = this.state.review_data.filter(item=>item.fresh === true)
      }
      else {
        review_data = this.state.review_data.filter(item=>item.fresh === false)
      }
      var libraries = review_data,
      searchString = this.state.value_for_search.trim().toLowerCase();
      libraries = libraries.filter(function(i) {
        return i.task_id.toString().toLowerCase().match( searchString );
      });
      return (
        <React.Fragment>
        <Grid container style={{padding:'10px 30px 10px 30px'}}>
          <Grid item xs={9} sm={9} sl={9} lg={9} md={9}>
             <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
          </Grid>
          <Grid item xs={3} sm={3} sl={3} lg={3} md={3}>
            <FilterMe applyFilter={this.applyFilter} />
          </Grid>

       </Grid><br />
        {libraries.length=== 0 ?
          <Typography variant="h5" align="center">No request Found !!</Typography>
          :
          <React.Fragment>
           <Grid container>
             {libraries.map((content,index)=>{
               return(
                    <Grid item xs={4} sm={4} key={index} style={{padding:'4px'}}>
                       <TaskH serial={content.task_id} admin_action={this.props.admin_action} fetchIncomings={this.fetchIncomings}
                        user={this.props.user}
                         data={content}/>
                    </Grid>
               )
             })}
           </Grid>
          </React.Fragment>
       }
       </React.Fragment>
      );
    }
  }
}


class TaskH extends Component {
  constructor() {
    super()
    this.state={
      fetching:true,
      action:'',
      reason:'',
      message_s:'',
      msg_box:false,
      task_details:'',
      open_reason:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTaskHis();
  }
  componentDidUpdate=(prevProps)=>{
     if(prevProps.data !== this.props.data)
     {
       this.fetchTaskHis();
     }
  }

  fetchTaskHis=()=>{
    axios.post('/ework/user2/fetch_a_task',{data:this.props.data})
    .then( res => {
        if(res.data === 'no')
        {
          this.setState({
            snack_open:true,
            snack_msg:'Not Found !!',
            alert_type:'error',
          });
        }
        else {
          this.setState({task_histoty:res.data})
        }
        this.setState({fetching:false})
    });
  }

  verify=(e,action)=>{
    this.setState({open_reason:true,action:action})
  }

  verifyIt=()=>{
    this.setState({fetching:true})
    this.setState({
      snack_open:true,
      snack_msg:'Updating.....',
      alert_type:'info',
    });
    axios.post('/ework/user2/validate_a_submitted_task',{data:this.state.task_histoty,
    props_data:this.props.data,action:this.state.action,reason:this.state.reason})
    .then( res => {
        if(res.data === 'no')
        {
          this.setState({
            snack_open:true,
            snack_msg:'Failed To Update !!',
            alert_type:'error',
            fetching:false,
          });
        }
        else {
          this.closeCase();
        }
    });
  }

  closeCase=()=>{
    axios.post('/ework/user/mark_as_done_review',{data:this.state.task_histoty,
    props_data:this.props.data})
    .then( res => {
      this.setState({open_reason:false})
        if(res.data === 'no')
        {
          this.setState({
            snack_open:true,
            snack_msg:'Failed To Update !!',
            alert_type:'error',
            fetching:false,
          });
        }
        else {
          this.props.fetchIncomings();
          this.setState({
            snack_open:true,
            snack_msg:'Updated !!',
            alert_type:'success',
            fetching:false,
          });
        }
    });
  }
  sendMessage=()=>{
    axios.post('/ework/user2/send_notification',{user:this.props.user,state_data:this.state})
    .then( res => {
      this.setState({msg_box:false,
        snack_open:true,
        snack_msg:'Sent !!',
        alert_type:'success',})
    });
  }

  render()
  {
    if(this.state.fetching)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Processing Your Request...</div>
        </Backdrop>
      )
    }
    else {
      return(
        <React.Fragment>

        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        open={this.state.snack_open} autoHideDuration={2000}
        onClose={()=>this.setState({snack_open:false})}>
          <Alert onClose={()=>this.setState({snack_open:false})}
          severity={this.state.alert_type}>
            {this.state.snack_msg}
          </Alert>
        </Snackbar>

        <Dialog
          open={this.state.open_reason}
          keepMounted
          onClose={()=>this.setState({open_reason:false})}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title" style={{textAlign:'center'}}>Information</DialogTitle>
          <DialogContent>
                      {!(this.state.action) ?
                         <TextField
                           id="outlined-multiline-static"
                           label="Mention the Reason"
                           multiline
                           rows="4"
                           fullWidth
                           name="reason"
                           value={this.state.reason}
                           onChange={(e)=>this.setState({reason:e.target.value})}
                           variant="filled"
                         />
                         :
                         <div>Are you sure you want to verify this task ?</div>
                       }
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>this.setState({open_reason:false})} color="secondary">
              CLOSE
            </Button>
            <Button
            onClick={this.verifyIt} color="primary">
              {this.state.action ? <React.Fragment>AGREE</React.Fragment>:<React.Fragment>SEND</React.Fragment>}
            </Button>
          </DialogActions>
        </Dialog>



        <Dialog
          open={this.state.msg_box}
          keepMounted
          onClose={()=>this.setState({msg_box:false})}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title" style={{textAlign:'center'}}>Send Message</DialogTitle>
          <DialogContent>
                         <TextField
                           id="outlined-multiline-static"
                           label="Type the Message"
                           multiline
                           rows="4"
                           fullWidth
                           name="reason"
                           value={this.state.message_s}
                           onChange={(e)=>this.setState({message_s:e.target.value})}
                           variant="filled"
                         />
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>this.setState({msg_box:false})} color="secondary">
              CLOSE
            </Button>
            <Button
            onClick={this.sendMessage} color="primary">
              SEND
            </Button>
          </DialogActions>
        </Dialog>

        <Card>
          <CardActionArea>
          <CardHeader
              title={"T "+this.props.serial}
              subheader={this.props.admin_action && <Typography>Faculty Assigned : {this.props.data.username}</Typography>}
              action={
                <Tooltip title="Send Message To the Student">
                    <IconButton disabled={this.state.task_histoty.verification_status === 0 ? false :true}
                    onClick={()=>this.setState({msg_box:true,task_details:this.state.task_histoty})} aria-label="settings">
                      <CallIcon />
                    </IconButton>
                </Tooltip>
                  }
            />
            <CardContent>
            <Typography variant="overline" display="block" gutterBottom>
              {this.state.task_histoty.description.toUpperCase()}
            </Typography>
              <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                Duration  - {this.state.task_histoty.start_date} to {this.state.task_histoty.end_date}
              </Typography>
              <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                {this.props.admin_action && <Typography>Student Username - {this.state.task_histoty.username}</Typography>}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" href={this.state.task_histoty.link}>
              Verification Link
            </Button>
            {this.state.task_histoty.verification_status === 0 ?
                 <React.Fragment>
                       <Tooltip title="Approve">
                           <IconButton color="primary" onClick={(e)=>this.verify(e,true)} aria-label="upload picture" component="span">
                             <CheckIcon style={{color:'green'}}  />
                           </IconButton>
                       </Tooltip>
                      <Tooltip title="Deny">
                        <IconButton color="secondary" onClick={(e)=>this.verify(e,false)} aria-label="upload picture" component="span">
                           <ClearIcon color="secondary"  />
                         </IconButton>
                       </Tooltip>
                 </React.Fragment>
              :
              <div style={{textAlign:'center',color:'green'}}>
                Verified
              </div>
            }
          </CardActions>
        </Card>
        </React.Fragment>

      )
    }

  }
}



const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        fullWidth
        className={classes.input}
        placeholder="Search Only By Task Id like 43 or 75" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}


class FilterMe extends React.Component{
  constructor()
  {
    super()
    this.state={
      anchorEl:null,
      isExpanded:false
    }
  }

  handleExpand =(e) => {
    this.setState({isExpanded:!this.state.isExpanded,anchorEl:e.currentTarget});
  };
  render()
  {
    return(
      <div style={{float:'right'}}>
        <Button variant="contained" color="primary" onClick={this.handleExpand}>Filter

          {this.state.isExpanded &&
            <Menu
              anchorEl={this.state.anchorEl}
              anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
              keepMounted
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              open={this.state.isExpanded}
              onClose={this.handleExpand}
            >
              <MenuItem onClick={() => this.props.applyFilter({filter:true})}>New</MenuItem>
              <MenuItem onClick={() => this.props.applyFilter({filter:false})}>Accepted</MenuItem>
            </Menu>

          }
          </Button>

      </div>
    )
  }
}
