import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Backdrop from '@material-ui/core/Backdrop';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {Typography,Switch} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


import ViewByID from './view_by_id'
import FacList from './list_of_user'
require("datejs")



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class BasicView extends Component {
  constructor() {
    super()
    this.state ={
      start:'',
      isChecked: false,
      history: '',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      agreed:[],
      loading:true,
      user:[],
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.getUser();
  }

    getUser()
    {
      axios.get('/ework/user/'
    )
       .then(response =>{
          if(response.status === 200)
          {
             if(response.data.user)
             {
               this.setState({user:response.data.user,loading:false})
             }
             else
             {
               this.setState({loading:false})
               this.setState({
                 redirectTo:'/ework/faculty',
               });
             }
          }
       })
    }


  handleComp = (e) =>{
    this.setState({
      snack_open:true,
      snack_msg: 'Validating....',
      alert_type:'info',
    });
    axios.get('/ework/user/validate_list_view')
    .then( response => {
      // console.log(response.data)
        if(response.data.permission === 1)
        {
          this.setState({
            snack_open:true,
            snack_msg: 'Accepted !!',
            alert_type:'success',
          });
          this.setState({
            isChecked: !this.state.isChecked,
            start:response.data.data_send,
          })
        }
        else{
          this.setState({
            snack_open:true,
            snack_msg: 'Denied Request !!',
            alert_type:'error',
            isChecked:false,
          });
        }
    });
  }
  render() {
    if(this.state.loading)
    {
      return(
        <div style={{marginLeft:'50%'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else{
    let admin_action;
    if((this.props.admin_action) || (this.state.user.h_order === 1))
    {
      admin_action = true;
    }
    else {
      admin_action = false;
    }
    if (this.state.redirectTo)
     {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
     }
      else
       {
        return (
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

                    <div style={{marginTop:'5px',textAlign:'center',float:'center'}}>
                     Already Having Official Id
                         <Switch
                          style={{float:'center'}}
                          checked={ this.state.isChecked } value="compense" onChange={ this.handleComp}
                           inputProps={{ 'aria-label': 'secondary checkbox' }}
                         />
                     Don't have any Official Id !!
                    </div>
             <br />
             <SwitchS admin_action={admin_action} datas={this.state.isChecked} start={this.state.start}/>
          </React.Fragment>
        );
      }
    }
  }
}


class SwitchS extends Component{
  constructor(props)
  {
    super(props)
    this.state={

    }
  }
  render()
  {
    let render_content;
    if(this.props.datas === true)
    {
      render_content =
      <ListShow admin_action={this.props.admin_action} start={this.props.start}/>
    }
    else{
      render_content =
      <ViewByID admin_action={this.props.admin_action} />
    }
    return(
      <React.Fragment>
        {render_content}
      </React.Fragment>
    );
  }
}


class ListShow extends Component {
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      username:'',
      dept:'',
      desgn:'',
      designation:'',
      desgn_list:[],
      department:[],
      fetching:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user/fetch_view_list',{start:this.props.start})
    .then( res => {
        if(res.data)
        {
          this.setState({desgn_list:res.data});
          this.fetchDepartment();
        }
    });
  }
  fetchDepartment=()=>{
    axios.get('/ework/user/fetch_department')
    .then( res => {
        if(res.data)
        {
          this.setState({department:res.data,fetching:false})
        }
    });
  }
  handleDesgn =(e)=>{
    this.setState({desgn:e.target.value})
  }
  handleDept = (e)=>{
    this.setState({dept:e.target.value})
  }
  showDiv =(username)=>{
    if(username)
    {
      this.setState({display:'block',username:username})
    }
  }

  render() {
    if(this.state.fetching)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress color="secondary" />&emsp;
          <div style={{color:'yellow'}}>Processing Your Request...</div>
        </Backdrop>
      )
    }
    else
    {
      return (
        <div>
        <Grid container>
           <Grid item xs={1} sm={1} lg={3} xl={3} md={2}/>
           <Grid container item xs={10} sm={10} lg={6} xl={6} md={8} spacing={1}>
              <Grid item xs={6} sm={6}>
                 <Paper elevation={3} style={{padding:'10px'}}>
                    <Typography style={{fontSize:'18px'}} align="center" variant="overline" display="block" gutterBottom>
                      Select the Designation
                    </Typography>
                    <FormControl style={{width:'100%'}}>
                      <InputLabel id="desgntype">Select Here..</InputLabel>
                      <Select
                        labelId="desgntype"
                        id="desgntype"
                        value={this.state.desgn}
                        onChange={this.handleDesgn}
                      >
                      {this.state.desgn_list.map((content,index)=>{
                        return(
                          <MenuItem value={content.designation_name} key={index}>{content.designation_name}</MenuItem>
                      )
                      })}
                      </Select>
                    </FormControl>
                  </Paper>
              </Grid>
              <Grid item xs={6} sm={6}>
                  <Paper elevation={3} style={{padding:'10px'}}>
                      <Typography style={{fontSize:'18px'}} align="center" variant="overline" display="block" gutterBottom>
                        Select the Department
                      </Typography>
                      <FormControl style={{width:'100%'}}>
                        <InputLabel id="depttype">Select Here..</InputLabel>
                        <Select
                          labelId="depttype"
                          id="depttype"
                          value={this.state.dept}
                          onChange={this.handleDept}
                        >
                        {this.state.department.map((content,index)=>{
                          return(
                            <MenuItem value={content.department_name} key={index}>{content.department_name}</MenuItem>
                        )
                        })}
                        </Select>
                      </FormControl>
                    </Paper>
              </Grid>
           </Grid>
           <Grid item xs={1} sm={1} lg={3} xl={3} md={2}/>
        </Grid>

<br />
            <div>
                  <FetchUser actionType="globalUser" admin_action={this.props.admin_action}
                   dept={this.state.dept} desgn={this.state.desgn} showDiv={this.showDiv} />
            </div>
        </div>
      );
    }
  }
}






class FetchUser extends Component {
  constructor(props) {
    super(props)
    this.state={
      users:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser();
  }
  componentDidUpdate =(prevProps)=>{
    if((prevProps.dept!==this.props.dept) || (prevProps.desgn!==this.props.desgn))
    {
      this.fetchUser();
    }
  }
  fetchUser = ()=>{
    axios.post('/ework/user/fetchUser',{department:this.props.dept,designation:this.props.desgn})
    .then( res => {
      //console.log(res.data)
        this.setState({users:res.data,loading:false})
    });
  }
  render()
  {
    //console.log(this.props)
    if(this.state.loading)
    {
      return(
        <div>Fetching.....</div>
      )
    }
    else{
    return(
      <React.Fragment>
        <FacList  admin_action={this.props.admin_action} actionType={this.props.actionType} users={this.state.users}/>
      </React.Fragment>
    )
   }
  }
}
