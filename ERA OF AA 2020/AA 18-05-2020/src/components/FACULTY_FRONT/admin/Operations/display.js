import React, { Component} from 'react'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import InsertUser from './insertuser'
import UpdateUser from './updateuser'
import SuperUser from './superuser'
import DeleteUser from './deleteuser'
import ModifyUser from './modifyuser'
import ViewUser from './view_user'
import Communicate from './communicate'


export default class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: '',
            select: '',
            active:'',
            open_drawer:true,
        }
    }
    handleCheck = (e) => {
        this.setState({
            select: e,
            open_drawer:false,
        })

        if (this.state.active === e) {
          this.setState({active : null})
        } else {
          this.setState({active : e})
        }
    }

    componentDidUpdate=(prevProps)=>{
      if(prevProps.choosed !== this.props.choosed)
      {
        this.setState({open_drawer:true})
      }
    }

    color =(position) =>{
      if (this.state.active === position) {
          return "#ff4081";
        }
        return "";
    }

    render(){
      let choosed_option;
        if(this.props.choosed === 0)
        {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('insertadmin')}}
                      onClick={(e)=>{this.handleCheck('insertadmin')}}>
                        <ListItemText primary={"Insert Department Admin"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('insert_in_nav')}}
                      onClick={(e)=>{this.handleCheck('insert_in_nav')}}>
                        <ListItemText primary={"Insert For Navbar"} />
                      </ListItem>
                      <ListItem button  style={{color:this.color('responsibilty_percentages')}}
                       onClick={(e)=>{this.handleCheck('responsibilty_percentages')}}>
                        <ListItemText primary={"Insert Responsibilty Percentages"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('section_part_insert')}}
                       onClick={(e)=>{this.handleCheck('section_part_insert')}}>
                        <ListItemText primary={"Insert For Section's Dropdown"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('insert_designation')}}
                       onClick={(e)=>{this.handleCheck('insert_designation')}}>
                        <ListItemText primary={"Insert Designations"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('insert_department')}}
                       onClick={(e)=>{this.handleCheck('insert_department')}}>
                        <ListItemText primary={"Insert Department"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                  <InsertUser select={this.state.select} user={this.props.user} />
                </main>
            </React.Fragment>

        }
        else if(this.props.choosed === 1)
        {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('facpwd')}}
                      onClick={(e)=>{this.handleCheck('facpwd')}}>
                        <ListItemText primary={"Reset Faculty or Admin Password"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('studpwd')}}
                      onClick={(e)=>{this.handleCheck('studpwd')}}>
                        <ListItemText primary={"Reset Student Password"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                    <UpdateUser select={this.state.select} user={this.props.user} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed === 2)
        {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('register')}}
                      onClick={(e)=>{this.handleCheck('register')}}>
                        <ListItemText primary={"Handle Renders"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('signup')}}
                      onClick={(e)=>{this.handleCheck('signup')}}>
                        <ListItemText primary={"Handle Signup"} />
                      </ListItem>
                      <ListItem button  style={{color:this.color('approve_single_faculty_req')}}
                       onClick={(e)=>{this.handleCheck('approve_single_faculty_req')}}>
                        <ListItemText primary={"Request From User"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('blue_print')}}
                       onClick={(e)=>{this.handleCheck('blue_print')}}>
                        <ListItemText primary={"Request For BluePrint"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('review_projects')}}
                       onClick={(e)=>{this.handleCheck('review_projects')}}>
                        <ListItemText primary={"Review Projects"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('suspendfac')}}
                       onClick={(e)=>{this.handleCheck('suspendfac')}}>
                        <ListItemText primary={"Suspend User"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('suspended_list')}}
                       onClick={(e)=>{this.handleCheck('suspended_list')}}>
                        <ListItemText primary={"Suspended User"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                  <SuperUser user={this.props.user} select={this.state.select} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed === 3)
        {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('deleteadmin')}}
                      onClick={(e)=>{this.handleCheck('deleteadmin')}}>
                        <ListItemText primary={"Delete Admin"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('deletefac')}}
                      onClick={(e)=>{this.handleCheck('deletefac')}}>
                        <ListItemText primary={"Delete Faculty"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('deletestu')}}
                      onClick={(e)=>{this.handleCheck('deletestu')}}>
                        <ListItemText primary={"Delete Student"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                    <DeleteUser select={this.state.select} user={this.props.user} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed === 4)
            {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('modifytoday')}}
                      onClick={(e)=>{this.handleCheck('modifytoday')}}>
                        <ListItemText primary={"Modify Today's DayOrder"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('cancel_or_declare_today_as_a_holiday')}}
                      onClick={(e)=>{this.handleCheck('cancel_or_declare_today_as_a_holiday')}}>
                        <ListItemText primary={"Cancel Today's DayOrder"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('semester-details')}}
                      onClick={(e)=>{this.handleCheck('semester-details')}}>
                        <ListItemText primary={"Start/Stop Semester"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('alter_faculty_adviser')}}
                      onClick={(e)=>{this.handleCheck('alter_faculty_adviser')}}>
                        <ListItemText primary={"Change Faculty Adviser"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('change_faculty_adviser')}}
                      onClick={(e)=>{this.handleCheck('change_faculty_adviser')}}>
                        <ListItemText primary={"Change Student's Faculty Adviser"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                    <ModifyUser select={this.state.select} user={this.props.user} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed ===5)
        {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('dept_admin_profile')}}
                      onClick={(e)=>{this.handleCheck('dept_admin_profile')}}>
                        <ListItemText primary={"Department Admin Profile"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('facprofile')}}
                      onClick={(e)=>{this.handleCheck('facprofile')}}>
                        <ListItemText primary={"Faculty Profile"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('studprofile')}}
                      onClick={(e)=>{this.handleCheck('studprofile')}}>
                        <ListItemText primary={"Student Profile"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                    <ViewUser select={this.state.select} user={this.props.user} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed ===6)
        {
            choosed_option=
            <React.Fragment>
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('connect_user')}}
                      onClick={(e)=>{this.handleCheck('connect_user')}}>
                        <ListItemText primary={"With Faculty/Dept-Admin"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('connect_student')}}
                      onClick={(e)=>{this.handleCheck('connect_student')}}>
                        <ListItemText primary={"With Student"} />
                      </ListItem>
                  </List>
                </Drawer>
              {this.state.select &&
                <main>
                    <Communicate select={this.state.select} user={this.props.user} />
                </main>
              }
            </React.Fragment>
        }
        else
        {
            choosed_option=<div></div>
        }
        return(
          <React.Fragment>
              {this.state.open_drawer === false &&
                <div onClick={()=>this.setState({open_drawer:true})} className="float-left"
                  style={{float:'center',boxShadow: '0px 10px 10px 2px pink',textAlign:'center',
                  width:'42px',borderRadius: '0px 20px 20px 0px'}}>
                  <ChevronRightIcon />
                </div>
              }
             <div style={{marginTop:'15px'}}>
               {choosed_option}
             </div>
          </React.Fragment>
        )
    }
}
