import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import {Typography,TextField} from '@material-ui/core';
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {CircularProgress,Backdrop,FormHelperText} from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.initialState ={
      expired_date:'',
      expired_month:'',
      expired_year:'',
      denying_reason:'',
      approving:false,
      denying:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
      submit:false,
      day_set:[],
    }
    this.state=this.initialState;
  }

  handleDeny =(id,username) =>{
    if(!this.state.denying_reason)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill all the Details !!',
        alert_type:'warning',
      });
    }
    else
    {
      this.setState({submit:true})
    axios.post('/ework/user/denyrequest',{
      serial: id,
      username:username,
      denying_reason:this.state.denying_reason,
    }).then(res=>{
      this.setState({submit:false})
      this.props.modalrender();
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
    .catch( err => {
      this.setState({
        submit:false,
        snack_open:true,
        snack_msg:'Something Went Wrong !!',
        alert_type:'error',
      });
    });
   }
  }
  handleApprove = (id,username) =>{
    if(!this.state.expired_date)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill all the Details !!',
        alert_type:'warning',
      });
    }
    else
    {
      this.setState({submit:true})
    axios.post('/ework/user/approverequest',{
      serial: id,
      username:username,
      expired_date:this.state.expired_date,
    }).then(res=>{
      this.props.modalrender();
      if(res.data)
      {
        var request = this.props.request;
        this.props.handleRequestModify({
          request: request.filter(request => request.serial !== id),
          viewdata:'',
        })
        this.setState(this.initialState)
      }
    })
    .catch( err => {
      this.setState({
        submit:false,
        snack_open:true,
        snack_msg:'Something Went Wrong !!',
        alert_type:'error',
      });
    });
   }
  }
  proceedToApprove = () =>{
    this.setState({approving:true,denying:false})
  }
  proceedToDeny = () =>{
    this.setState({denying:true,approving:false})
  }
  addExpire = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }


  render()
  {
    function leapYear(year)
    {
     return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    }
    let day_set,actual_days;
    var year = parseInt(Date.today().toString("yyyy"));
    if(leapYear(year) === true)
    {
        day_set = [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:30,value:4},
      {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
    ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]

    }
    else
    {
        day_set = [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:30,value:4},
      {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
    ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
    }
    if(day_set.length>0)
    {

        var month = parseInt(Date.today().toString("MM"));
        var today = parseInt(Date.today().toString("dd"));
        var days=[];
        const day_data= day_set.filter(item => item.value===month);
        for(var j=1;j<=day_data[0].day;j++)
        {
          days.push(j);
        }
        actual_days= days.filter(item1=>item1>today)
    }


    var dat;
    if(this.props.view_details ==="yes")
    {
      dat=
      <React.Fragment>
      <Typography variant="h6" align="center">Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!
      </Typography>
      <br />
      <div style={{float:'right'}}>
        <Button variant="contained" color="secondary" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</Button>
      </div>

      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat=
    <React.Fragment>
      <Typography variant="h6" align="center">We can't able to find any data for this request !! Kindly take decision manually !!</Typography>
      <br />
       <div style={{float:'right'}}>
          <Button variant="contained" color="secondary" onClick={this.proceedToDeny}>DENY</Button>
          &ensp;
          <Button variant="contained" color="primary" onClick={this.proceedToApprove}>APPROVE</Button>
        </div>
      <br />
      <div style={{margin:'20px 0px 0px 0px'}}>
          {this.state.denying===true &&
            <React.Fragment>
              <TextField
                id="outlined-multiline-static"
                label="Enter the Reason"
                multiline
                rows="4"
                fullWidth
                type="text" name="denying_reason" value={this.state.denying_reason} onChange={this.addExpire}
                variant="filled"
              />
              <br /><br />
              <Button variant="contained" style={{float:'right'}} color="secondary" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</Button>
            </React.Fragment>
          }

          {this.state.approving=== true &&
            <React.Fragment>
               <Typography variant="h6" align="center" color="secondary">Enter Expiry Date</Typography>
               <br /><br />
               <FormControl style={{width:'100%'}}>
                  <InputLabel id="month">Day (Should be within this month and year)</InputLabel>
                  <Select
                    labelId="month"
                    id="month"
                    name="expired_date" value={this.state.expired_date}
                    onChange={this.addExpire}
                  >
                  {actual_days.map((content,index)=>{
                          return(
                            <MenuItem key={index}  value={content}>{content}</MenuItem>
                          )
                    })}
                  </Select>
                  <FormHelperText>If today is the last date of the month,Please don't response to this request.Try Tommorow</FormHelperText>
                </FormControl>

               <br /><br />
                <Button variant="contained" style={{float:'right'}} color="primary"
                onClick={() => this.handleApprove(this.props.index_id,this.props.username)}>APPROVE</Button>
            </React.Fragment>
        }
    </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    if(this.state.submit)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Processing Your Request.....</div>
        </Backdrop>
      )
    }
    else
    {
    return(
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      {this.props.renderDiv&&
        <Dialog
          open={true} fullWidth
          onClose={this.props.modalrender}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            {dat}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.modalrender} color="primary" autoFocus>
              CLOSE
            </Button>
          </DialogActions>
        </Dialog>
      }
    </React.Fragment>
    )
   }
  }
}
