import React, { Component } from 'react';
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import {FormControl,InputLabel,MenuItem,Select,Grid,Paper,TextField,Button,CircularProgress} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import AddProduct from './add';
import ProductList from './prod'
import SetQuestion from './setQuestion'

export default class SkillSetting extends Component {
  constructor()
  {
    super()
    this.state={
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user)
        {
          this.setState({username:response.data.user.username})
        }
        else{
          this.setState({
            redirectTo:'/ework/',
          });
        }
      })
    }

  render() {
    let target;
      if(this.props.select === 'add-skill')
      {
        target = <AddSkill noChange={this.props.noChange} username={this.state.username}/>
      }
      else if(this.props.select === 'add-project')
      {
        target = <SetProjectSkill noChange={this.props.noChange} username={this.state.username} />
      }
      else if(this.props.select === 'add-question')
      {
        target = <SetQuestion noChange={this.props.noChange} username={this.state.username} />
      }
      else{
        target = <div></div>
      }
    return (
      <div>
         {target}
      </div>
    );
  }
}




class SetProjectSkill extends Component {

    constructor(props)
    {
      super(props)
      this.state ={
        username:'',
        redirectTo:'',
        option:'',
        skill_name:'',
        saved_skill:[],
        loading:true,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
    }

  handleOption = (e) =>{
    this.setState({option: e.target.value})
  }


  componentDidMount(){
    this.fetchSavedSkills()
  }

  fetchSavedSkills =() =>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
    .then(res => {
        if(res.data)
        {
          this.setState({saved_skill:res.data,loading:false})
        }
    });
  }


  setSkill=(e)=>{
    if(e === null)
    {

    }
    else{
      this.setState({skill_name:e.skill_name})
    }
  }


    render()
    {
      if(this.state.loading)
      {
        return(
          <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
            <CircularProgress color="secondary" />
          </div>
        )
      }
      else
      {
      const options = this.state.saved_skill.map(option => {
        const firstLetter = option.skill_name[0].toUpperCase();
        return {
          firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
          ...option,
        };
      });
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
      return(
        <React.Fragment>
        <Grid container>
                <Grid item xs={6} sm={6} style={{padding:'4px'}}>
                   <Paper elevation={2} style={{padding:'20px'}}>
                       <Autocomplete
                         id="grouped-demo"
                         options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                         groupBy={option => option.firstLetter}
                         getOptionLabel={option => option.skill_name}
                         onChange={(event, value) => this.setSkill(value)}
                         style={{ width: '100%' }}
                         renderInput={params => (
                           <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
                         )}
                       />
                   </Paper>
                </Grid>
                <Grid item xs={6} sm={6} style={{padding:'4px'}}>
                          <Paper elevation={2} style={{padding:'20px'}}>
                           <FormControl variant="outlined" style={{width:'100%'}}>
                              <InputLabel id="demo-simple-select-outlined-label">
                                Select Level
                              </InputLabel>
                              <Select
                                labelId="level"
                                id="level"
                                value={this.state.option} onChange={this.handleOption}
                                labelWidth={90}
                              >
                                <MenuItem value="Begineer">Beginner</MenuItem>
                                <MenuItem value="Intermediate">Intermediate</MenuItem>
                                <MenuItem value="Advanced">Advanced</MenuItem>
                              </Select>
                            </FormControl>
                          </Paper>
                </Grid>
          </Grid>
          <br />
            <div style={{margin:'2px'}}>
                <AddTask noChange={this.props.noChange} level={this.state.option}
                skill_name={this.state.skill_name} username={this.props.username}/>
            </div>
        </React.Fragment>

      )
    }
   }
  }
}


class AddTask extends Component{
    constructor(props) {
       super(props);
       this.state = {
         redirectTo: null,
         username:'',
         isAddProduct: false,
         response: {},
         product: {},
         isEditProduct: false,
         action:this.props.options,
       }
       this.onFormSubmit = this.onFormSubmit.bind(this);
     }
     onCreate = (e,index) => {
       this.setState({ isAddProduct: true,product: {}});
     }

     onFormSubmit(data){
       let apiUrl;
       var addroute,editroute;
         addroute="/ework/user2/add_from_insert_in_admin";
         editroute = "/ework/user2/edit_inserted_data";
       if(this.state.isEditProduct)
       {
         apiUrl = editroute;
       }
       else
       {
         apiUrl = addroute;
       }
       axios.post(apiUrl, {data})
           .then(response => {
             this.setState({
               response: response.data,
               isAddProduct: false,
               isEditProduct: false
             })
           })
     }
     editProduct = (productId,index)=> {
       var editProd;
         editProd ="/ework/user2/fetch_for_edit"
       axios.post(editProd,{
         id: productId,
       })
       .then(response => {
             this.setState({
               product: response.data,
               isEditProduct: true,
               isAddProduct: true,
             });
           })
    }

    updateState = () =>{
      this.setState({
        isAddProduct:false,
        isEditProduct:false,
      })
    }

     render() {
       let productForm;
              var  data1 = {
                 fielddata: [
                   {
                     header: "Refer Id",
                     name: "count",
                     placeholder: "blank",
                   },
                   {
                     header: "Description of the task",
                     name: "description_of_the_task",
                     placeholder: "Enter the Description of the task",
                   },
                   {
                     header: "Any Reference To Follow",
                     name: "ref_for_task",
                     placeholder: "Enter Any Reference To Follow",
                   },
                   {
                     header: "Action",
                     name: "no",
                     placeholder: "blank",
                   }
                 ],
               };

               if(this.state.isAddProduct || this.state.isEditProduct)
               {
                 productForm =
                 <AddProduct cancel={this.updateState} username={this.props.username}
                 action={'Task '+this.props.skill_name} level={this.props.level}  data={data1}
                 onFormSubmit={this.onFormSubmit}  product={this.state.product} />
               }
          return(
            <React.Fragment>
              {!this.state.isAddProduct &&
                 <React.Fragment>
                 <Grid container>
                   <Grid item xs={6} sm={6} />
                   <Grid item xs={6} sm={6}>
                     {(this.props.skill_name && this.props.level) &&
                       <Button variant="contained" color="secondary" style={{marginRight:'10px',float:'right'}}
                       onClick={(e) => this.onCreate(e,this.props.options)}>
                          Add Data
                        </Button>
                     }
                    </Grid>
                </Grid>
                </React.Fragment>
              }
              <br />
              {!this.state.isAddProduct &&
                <React.Fragment>
                  {(this.props.skill_name && this.props.level) &&
                    <ProductList username={this.props.username} noChange={this.props.noChange}
                    action={'Task '+this.props.skill_name} level={this.props.level}  data={data1}
                    editProduct={this.editProduct}/>
                  }
                </React.Fragment>
              }
              {productForm}
          </React.Fragment>
      );
  }
}



class AddSkill extends Component {
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
       let apiUrl;
       var addroute,editroute;
         addroute="/ework/user2/add_from_insert_in_admin";
         editroute = "/ework/user2/edit_inserted_data"
       if(this.state.isEditProduct){
         apiUrl = editroute;
       } else {
         apiUrl = addroute;
       }
       axios.post(apiUrl, {data})
           .then(response => {
             this.setState({
               response: response.data,
               isAddProduct: false,
               isEditProduct: false
             })
           })
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/ework/user2/fetch_for_edit"


     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Refer Id",
                   name: "count",
                   placeholder: "blank",
                 },
                 {
                   header: "Skill Name",
                   name: "skill_name",
                   placeholder: "Enter the Name of the Skills",
                 },
                 {
                   header: "Action",
                   name: "no",
                   placeholder: "blank",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct)
             {
                 productForm =
                 <AddProduct username={this.props.username} cancel={this.updateState}
                 action="Skill-For-Student" data={data1} onFormSubmit={this.onFormSubmit}
                 product={this.state.product} />
             }
      return (
        <React.Fragment>
                  {!this.state.isAddProduct &&
                   <React.Fragment>
                     <Grid container>
                       <Grid item xs={6} sm={6} />
                       <Grid item xs={6} sm={6}>
                         <Button  variant="contained" color="secondary" style={{marginRight:'10px',float:'right'}}
                         onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</Button>
                        </Grid>
                    </Grid>
                  </React.Fragment>
                 }
                {!this.state.isAddProduct &&
                  <ProductList username={this.props.username} noChange={this.props.noChange}
                 action="Skill-For-Student" data={data1}  editProduct={this.editProduct}/>
                }
                { productForm }
      </React.Fragment>
      );

}
}
