import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Grid,Hidden} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import {Fab,Typography,CircularProgress,Backdrop} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';

import Nav from '../../dynnav'
import AddProduct from './add'
import ProductList from './prod'



class FiveYear extends Component{
    constructor(){
        super();
        this.state={
          isExpanded:false,
          refer_index:0,
          p_index:0,
          active:0,
          home:'/ework/faculty',
          logout:'/ework/user/logout',
          get:'/ework/user/',
          nav_route: '/ework/user/fetchnav',
          noti_route:true,
          username:'',
          redirectTo:'',
          loading:true,
          isAddProduct: false,
          response: {},
          action:'',
          product: {},
          isEditProduct: false,
          five:[{year:'Year - I',parts:[{name:'Administrative',index:'YearI-Administrative'},{name:'Academic',index:'YearI-Academic'},{name:'Research',index:'YearI-Research'}]},
          {year:'Year - II',parts:[{name:'Administrative',index:'YearII-Administrative'},{name:'Academic',index:'YearII-Academic'},{name:'Research',index:'YearII-Research'}]},
          {year: 'Year - III',parts:[{name:'Administrative',index:'YearIII-Administrative'},{name:'Academic',index:'YearIII-Academic'},{name:'Research',index:'YearIII-Research'}]},
          {year:'Year - IV',parts:[{name:'Administrative',index:'YearIV-Administrative'},{name:'Academic',index:'YearIV-Academic'},{name:'Research',index:'YearIV-Research'}]},
          {year:'Year - V',parts:[{name:'Administrative',index:'YearV-Administrative'},{name:'Academic',index:'YearV-Academic'},{name:'Research',index:'YearV-Research'}]}]
        }
    }
    componentDidMount(){
        axios.get('/ework/user/'
      )
         .then(response =>{
           if(response.data.user)
           {
             this.setState({username: response.data.user.username})
           }
           else{
             this.setState({
               redirectTo:'/ework/faculty',
             });
           }
          this.setState({loading: false})
         })
    }


    onCreate = (e,index) => {
      this.setState({ isAddProduct: index ,product: {}});
    }
    onFormSubmit =(data) => {
      // console.log(data)
      let apiUrl;
      if(this.state.isEditProduct)
      {
        apiUrl = '/ework/user/edit_five_year_plan';
      }
       else
       {
        apiUrl = '/ework/user/add_five_year_plan';
       }
       this.setState({loading:true})
      axios.post(apiUrl,data)
          .then(response => {
            this.setState({
              response: response.data,
              isAddProduct: false,
              isEditProduct: false,
              loading:false,
            })
          })
          .catch( err => {
              this.setState({loading:false})
          });
    }

    editProduct = (productId,index)=> {
         this.setState({loading:true})
      axios.post('/ework/user/fetch_five_year_existing_data',{
        id: productId,
      })
          .then(response => {
            this.setState({
              product: response.data,
              isEditProduct: index,
              isAddProduct: index,
              loading:false,
            });
          })
          .catch( err => {
              this.setState({loading:false})
          });

   }

   handleExpand =(e,serial,index) => {
     this.setState({refer_index:serial,p_index:index,isExpanded:!this.state.isExpanded});
   };

   updateState = () =>{
     this.setState({
       isAddProduct:'',
       isEditProduct:'',
     })
   }


    render(){
      if(this.state.loading === true)
      {
        return(
          <Backdrop  open={true} style={{zIndex:'2040'}}>
            <CircularProgress style={{color:'yellow'}}/>&emsp;
            <div style={{color:'yellow'}}>Processing Your Request.....</div>
          </Backdrop>
        );
      }
      else{
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return(
            <React.Fragment>
            <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

                   <Grid container>
                      <Hidden xsDown><Grid item xl={4} lg={4} md={3}/></Hidden>
                      <Grid item xs={12} sm={12} xl={4} lg={4} md={6}>
                        <Typography variant="h6" align="center" style={{fontFamily: 'Orbitron',
                        paddingTop: '10px'}}>Your Plan For Next Five Year</Typography>
                      </Grid>
                      <Hidden xsDown><Grid item xl={4} lg={4} md={3}/></Hidden>
                   </Grid>
                   <br />

                <Grid container>
                  <Hidden xsDown><Grid item xs={1} sm={1} /></Hidden>
                  <Grid item xs={12} sm={10} >
                        {this.state.five.map((content,index)=>(
                          <React.Fragment key={index}>
                              <Paper elevation={3}  style={{paddingBottom:'2px'}}>
                                  <div style={{backgroundColor:'#455a64',textAlign:'center',
                                  color:'white',height: '45px',paddingTop: '12px'}}>{content.year}</div><br />

                                  {content.parts.map((items,no)=>(
                                        <div style={{padding:'5px'}} key={no}>
                                                    <ExpansionPanel
                                                    expanded={this.state.isExpanded && (this.state.p_index ===  index) && (this.state.refer_index === no)}
                                                    style={{borderRadius:'20px',width:'100%'}} onChange={(e)=>this.handleExpand(e,no,index)}
                                                    >
                                                      <ExpansionPanelSummary
                                                      style={{borderRadius:'20px'}}
                                                        expandIcon={<ExpandMoreIcon  />}
                                                        aria-controls="panel4bh-content"
                                                        id="panel4bh-header"
                                                      >
                                                        <Typography color="secondary" align="center" style={{fontSize:'15px'}}>{items.name}</Typography>
                                                              </ExpansionPanelSummary>

                                                                  {!this.state.isAddProduct && <ProductList data={items}
                                                                  username={this.state.username}  action={items.index} editProduct={this.editProduct}/>}
                                                                  {!this.state.isAddProduct &&
                                                                    <Fab style={{float:'right',marginBottom:'4px'}} size="small"
                                                                    color="primary" onClick={(e) => this.onCreate(e,items.index)}
                                                                    aria-label="add">
                                                                         <AddIcon />
                                                                     </Fab>
                                                                   }

                                                                   {((this.state.isAddProduct === items.index) || (this.state.isEditProduct === items.index)) &&
                                                                   <AddProduct data={items} cancel={this.updateState}
                                                                   action={items.index} username={this.state.username}
                                                                   onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                                                                   }


                                                      </ExpansionPanel>

                                              </div>
                                  ))}
                                </Paper>
                                <br />
                            </React.Fragment>
                        ))}
                  </Grid>
                  <Hidden xsDown><Grid item xs={1} sm={1} /></Hidden>
                </Grid>
            </React.Fragment>
        )
      }
    }
    }
}

export default FiveYear;
