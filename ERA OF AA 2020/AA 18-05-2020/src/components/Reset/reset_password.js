import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import {Hidden,Button} from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import {Paper,Typography} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class ResetPassword extends Component {
    constructor() {
        super()
        this.state = {
            redirectTo: null,
            password: '',
            con_password: '',
            token_come:'',
            snack_open:false,
            alert_type:'',
            snack_msg:'',
            showPassword:false,
            show_modal:false

        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleResetPass = this.handleResetPass.bind(this)
        }


        componentDidMount() {
           axios.get('/ework/user/reset_password',{
             params: {
               resetPasswordToken: this.props.match.params.token,
             },
          }).then(response => {
            if(response.status === 200)
            {
              if(response.data.expire)
              {

                alert("Link is Expired");
                this.setState({
                   redirectTo: '/ework/'
               })
              }
              else
              {
                this.setState({
                  token_come: response.data,
                })
              }
            }
            else{
              this.setState({
                snack_open:true,
                snack_msg:'Something Went Wrong !!',
                alert_type:'error',
             })
            }
          })
        }

            handlePasswordChange =(e) =>{
                this.setState({
                    [e.target.name]: e.target.value,
                });
            }
            handleResetPass =(e) =>{

              if(!(this.state.con_password)||!(this.state.password))
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Enter all the Details !!',
                  alert_type:'warning',
                })
              }
              else if(!(this.state.password.match(/[a-z]/g) && this.state.password.match(
                        /[A-Z]/g) && this.state.password.match(
                        /[0-9]/g) && this.state.password.match(
                        /[^a-zA-Z\d]/g) && this.state.password.length >= 6))
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Follow the correct format of password !!',
                  alert_type:'warning',
                })
                  this.showValidation();
              }
              else if(this.state.password !==this.state.con_password)
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Password Does not Match',
                  alert_type:'warning',
                })
                this.setState({
                  con_password:'',
                })
              }
              else
              {
                axios.post('/ework/user/reset_from_mail', {
                    token_come: this.state.token_come,
                    password: this.state.password,
                  })
                   .then(response => {
                     //console.log(response)
                     if(response.status===200){
                       if(response.data.succ)
                        {
                          this.setState({
                             redirectTo: '/ework/'
                         })
                       }
                     }
                    }).catch(error => {
                        this.setState({
                          snack_open:true,
                          snack_msg:'Something Went Wrong !!',
                          alert_type:'error',
                        })
                    })
                  }
            }

            showValidation=()=>{
            	this.setState({show_modal:!this.state.show_modal})
            }

            handleMouseDownPassword = event => {
                event.preventDefault();
              };


    render(){
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
        return(
          <React.Fragment>
            <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
        		open={this.state.snack_open} autoHideDuration={2000}
        		onClose={()=>this.setState({snack_open:false})}>
        			<Alert onClose={()=>this.setState({snack_open:false})}
        			severity={this.state.alert_type}>
        				{this.state.snack_msg}
        			</Alert>
        		</Snackbar>

            <Grid container spacing={1}>
             <Hidden><Grid item sm={1}/></Hidden>
             <Grid item xs={12} sm={10}>
               <Paper elevation={3} className="reset-pass-box">
                  <div style={{textAlign:'center',backgroundColor:'#ec407a',color:'white',fontFamily: 'Play',fontSize:'25px'}}>
                   RESET YOUR PASSWORD HERE
                  </div>
                  <br />
                  <Grid container spacing={1}>
                     <Grid item xs={12} sm={6}>
                         <FormControl style={{width:'100%'}}>
                            <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                            <Input
                              type={this.state.showPassword ? 'text' : 'password'}
                              onChange={this.handlePasswordChange}  name="password" id="pswd1" value={this.state.password}
                              endAdornment={
                                <InputAdornment position="end">
                                  <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={()=>this.setState({showPassword:!this.state.showPassword})}
                                    onMouseDown={this.handleMouseDownPassword}
                                  >
                                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                  </IconButton>
                                </InputAdornment>
                              }

                            />
                              <HelpOutlineIcon style={{color:'green'}} onClick={this.showValidation} />
                          </FormControl>
                     </Grid>
                     <Grid item xs={12} sm={6}>
                         <FormControl style={{width:'100%'}}>
                            <InputLabel htmlFor="standard-adornment-password">Confirm Password</InputLabel>
                            <Input
                              type={this.state.showPassword ? 'text' : 'password'}
                              onChange={this.handlePasswordChange}  name="con_password" id="pswd2" value={this.state.con_password}
                              endAdornment={
                                <InputAdornment position="end">
                                  <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={()=>this.setState({showPassword:!this.state.showPassword})}
                                    onMouseDown={this.handleMouseDownPassword}
                                  >
                                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                  </IconButton>
                                </InputAdornment>
                              }

                            />
                          </FormControl>
                     </Grid>
                  </Grid>
                  <br />
                  <Button variant="contained" color="secondary" style={{float:'right'}} onClick={this.handleResetPass}>Submit</Button>
               </Paper>
             </Grid>
             <Hidden><Grid item sm={1}/></Hidden>
            </Grid>



            <React.Fragment>
              <Dialog
                open={this.state.show_modal}
                onClick={this.showValidation}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">Password Should Consist of </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    <Typography variant="button" display="block" gutterBottom>
                    At least 1 uppercase character.<br />
                    At least 1 lowercase character.<br />
                    At least 1 digit.<br />
                    At least 1 special character.<br />
                    Minimum 6 characters.<br />
                    </Typography>
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.showValidation} color="primary" autoFocus>
                    Agree
                  </Button>
                </DialogActions>
              </Dialog>
           </React.Fragment>
          </React.Fragment>

        );
    }
    }
}
