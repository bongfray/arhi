const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const nav = new Schema({
  val: {type: String , unique: false, required: false},
  link: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  inserted_by: { type: String, unique: false, required: false },
})



nav.plugin(autoIncrement.plugin, { model: 'NavBar', field: 'serial', startAt: 1,incrementBy: 1 });

const Nav = mongoose.model('NavBar', nav)
module.exports = Nav
