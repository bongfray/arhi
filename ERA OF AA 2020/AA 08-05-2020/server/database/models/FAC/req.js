const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const requested = new Schema({
  action: { type: String, unique: false, required: false },
  denying_reason: { type: String, unique: false, required: false },
  username:{ type: String, unique: false, required: false },
  req_date:{ type: String, unique: false, required: false },
  req_month:{ type: String, unique: false, required: false },
  req_year:{ type: String, unique: false, required: false },
  req_hour:{ type: String, unique: false, required: false },
  req_reason:{ type: String, unique: false, required: false },
  day_order:{ type: String, unique: false, required: false },
  expired_date:{ type: Number, unique: false, required: false },
  expired_month:{ type: Number, unique: false, required: false },
  expired_year:{ type: Number, unique: false, required: false },
  expired:{ type: Boolean, unique: false, required: false },
  status:{ type: String, unique: false, required: false },
  faculty_adviser:{ type: String, unique: false, required: false },
  validity:{ type: String, unique: false, required: false },
  received:{type: Date, default: Date.now}
})



requested.plugin(autoIncrement.plugin, { model: 'Request-From-User', field: 'serial', startAt: 1,incrementBy: 1 });

const Request = mongoose.model('Requests-From-User', requested)
module.exports = Request
