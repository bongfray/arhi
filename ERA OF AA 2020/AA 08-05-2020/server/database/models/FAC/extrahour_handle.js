const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const extra1 = new Schema({
  day_order: { type: String, unique: false, required: false },
  week: { type: String, unique: false, required: false },
  date: { type: String, unique: false, required: false },
  month: { type: String, unique: false, required: false },
  year: { type: String, unique: false, required: false },
  dayorder: { type: String, unique: false, required: false },
  hour: { type: String, unique: false, required: false },
  completed_for: { type: String, unique: false, required: false },
  handled_by: { type: String, unique: false, required: false },
  status: { type: Boolean, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  handledtopic:{ type: String, unique: false, required: false },
  slot:{ type: String, unique: false, required: false },
  for_year: { type: String, unique: false, required: false },
  for_sem: { type: String, unique: false, required: false },
  for_batch: { type: String, unique: false, required: false },
})



extra1.plugin(autoIncrement.plugin, { model: 'Extra_Hour_Handled', field: 'serial', startAt: 1,incrementBy: 1 });

const Extra1 = mongoose.model('Extra_Hour_Handled', extra1)
module.exports = Extra1
