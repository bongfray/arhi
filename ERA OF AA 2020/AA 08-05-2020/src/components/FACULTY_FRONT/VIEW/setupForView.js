import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect} from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {Snackbar,Typography} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

import Section from './DATA TYPES/section_view.js'
import Five from './DATA TYPES/five_year_view.js'
import AdRes from './DATA TYPES/academic_data_view.js'
import Perform from './DATA TYPES/perform_view.js'

import Pdf from './PDF/Print'


import axios from 'axios';
import StudentTable from './STUDENT DATA/studentTable_underFac';

require("datejs")

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});



 export default class ViewSubject extends React.Component{
  constructor()
  {
    super()
    this.state={
      option:'',
      redirectTo:'',
      modal: false,
      open:true,
      active:0,
      s_year:'',
      show_pdf:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
 componentDidMount()
 {
   if(this.props.onlyDownload)
   {
     this.setState({
       snack_open:true,
       snack_msg:'Download-Only Mode!!',
       alert_type:'info',
     });
   }
 }

 handleClose=()=>{
   this.setState({open:!this.state.open})
   if(this.props.closeModal)
   {
     this.props.closeModal();
     this.props.closeDecider({
       details_view:false,
     })
     if(this.props.setDatas)
     {
       this.props.setDatas({
         renderFetch:false
       })
     }

   }
 }

 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }

 handleSet=(e)=>{
   this.setState({
     active: e,
   })
 }

 color =(position) =>{
   if (this.state.active === position) {
       return '#69F0AE';
     }
     return '#e0e0e0';
 }

 setYear=(e)=>{
   this.setState({s_year:e.target.value})
 }

 renderResume=()=>{
   this.setState({show_pdf:!this.state.show_pdf})
 }

 render()
 {
   //console.logthis.props.username)
   const styleObject = makeStyles(theme => ({
     appBar: {
       position: 'relative',
     },
     title: {
       marginLeft: theme.spacing(2),
       flex: 1,
     },
   }));

   if (this.state.redirectTo) {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
 } else {
   return(
     <React.Fragment>

     {this.state.show_pdf &&
       <Dialog fullScreen open={this.state.show_pdf} TransitionComponent={Transition}>
           <AppBar>
             <Toolbar>
              <Grid style={{marginTop:'55'}}container spacing={1}>
                <Grid item xs={1} sm={1}>
                   <IconButton edge="start" color="inherit" onClick={this.renderResume} aria-label="close">
                     <CloseIcon />
                   </IconButton>
                </Grid>
                <Grid item xs={10} sm={10}>
                  <div style={{textAlign:'center',fontSize:'20px'}}>PDF DATA</div>
                </Grid>
                <Grid item xs={1} sm={1}/>
              </Grid>
             </Toolbar>
           </AppBar>
           <List>
           <div style={{marginTop:'70px'}}>
               <Pdf admin_action={this.props.admin_action}
               referDatas={this.props.datas} data={this.state.option}
               username={this.props.username} onlyDownload={this.props.onlyDownload} />
           </div>
           </List>
         </Dialog>
     }

      <Dialog fullScreen open={this.state.open} onClose={this.handleClose} TransitionComponent={Transition}>
          <AppBar className={styleObject.appBar}>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2}>
                  <IconButton edge="start" color="inherit" onClick={this.handleClose} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8}>
                 <div className="center" style={{fontSize:'20px'}}>
                   {this.props.actionType === 'localuser' ?
                     <Typography variant="h6" align="center">Showing Your Datas</Typography>
                     :
                     <Typography variant="h6" align="center">
                       Showing Details of <span style={{color:'yellow'}}>{this.props.username}</span>
                     </Typography>
                   }
                 </div>
               </Grid>
               <Grid item xs={2} sm={2}>
                 <Button variant="contained" onClick={this.renderResume} color="secondary">Download</Button>
                </Grid>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

            <div style={{marginTop:'60px'}}>
             <Grid container>
                  <Grid item sm={4} xs={1} />
                   <Grid item xs={10} sm={4}>
                       <Grid container spacing={1}>
                           <Grid item sm={6} xs={6}>
                             <Paper  onClick={(e)=>{this.handleSet(0)}} style={{height:'40px',textAlign:'center',backgroundColor:this.color(0)}}>Faculty's Data</Paper>
                           </Grid>
                           <Grid item sm={6} xs={6}>
                             <Paper onClick={(e)=>{this.handleSet(1)}} style={{height:'40px',textAlign:'center',backgroundColor:this.color(1)}} >Student Under the Faculty</Paper>
                           </Grid>
                       </Grid>
                   </Grid>
                   <Grid item sm={4} xs={1} />
             </Grid>
             <br />
             {this.state.active === 0 ?
                 <React.Fragment>
                      <Grid container>
                      <Grid item xs={4} sm={4} />
                       <Grid item sm={4} xs={4}>
                              <FormControl style={{width:'100%'}} disabled={this.props.onlyDownload}>
                                <InputLabel id="option">Choose the Field Here...</InputLabel>
                                <Select
                                  labelId="option"
                                  id="option"
                                 value={this.state.option}
                                 onChange={this.handleOption}
                                >
                                <MenuItem value="" disabled defaultValue>Choose the Field Here...</MenuItem>
                                <MenuItem value="Section">Section Datas</MenuItem>
                                <MenuItem value="Academic">Academic Data</MenuItem>
                                <MenuItem value="Administrative">Admininstrative Datas</MenuItem>
                                <MenuItem value="Research">Research Datas</MenuItem>
                                <MenuItem value="Five">Five Year Plan Data</MenuItem>
                                <MenuItem value="Perform">Complete Percentage</MenuItem>
                                </Select>
                              </FormControl>
                          </Grid>
                          <Grid item xs={4} sm={4} />
                        </Grid>
                        <br />
                         <div className="row">
                                <Decider  admin_action={this.props.admin_action}
                                referDatas={this.props.datas} data={this.state.option}
                                username={this.props.username}/>
                         </div>
                  </React.Fragment>
                :
                  <React.Fragment>
                    <Grid container>
                      <Grid item xs={4} sm={4}/>
                       <Grid item xs={4} sm={4}>
                          <FormControl style={{width:'100%'}}>
                            <InputLabel id="option">Choose the Field Here...</InputLabel>
                            <Select
                              labelId="option"
                              id="option"
                             value={this.state.s_year}
                             onChange={this.setYear}
                            >
                            <MenuItem value="" disabled defaultValue>Choose the Field Here...</MenuItem>
                            <MenuItem value="1">First Year</MenuItem>
                            <MenuItem value="2">Second Year</MenuItem>
                            <MenuItem value="3">Third Year</MenuItem>
                            <MenuItem value="4">Fourth Year</MenuItem>
                            <MenuItem value="5"></MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>
                      <Grid item xs={4} sm={4}/>
                    </Grid>
                    <br /><br />
                    <Grid container>
                       <Grid item xs={1} sm={1}/>
                       <Grid item xs={10} sm={10}>
                         {this.state.s_year &&
                           <StudentUnderFaculty year={this.state.s_year} username={this.props.username} />
                         }
                       </Grid>
                       <Grid item xs={1} sm={1}/>
                    </Grid>
                  </React.Fragment>
               }
            </div>
        </List>
      </Dialog>
   </React.Fragment>
 );
}
 }
}

class Decider extends Component{
  render()
  {

    let body,route;
    if(this.props.data === "Section")
    {
      body = <Section admin_action={this.props.admin_action} referDatas={this.props.referDatas}
      username={this.props.username} display={this.props.display} />
    }
    else if(this.props.data === "Five")
    {
      body = <Five admin_action={this.props.admin_action} referDatas={this.props.referDatas}
       username={this.props.username} display={this.props.display} />
    }
    else if((this.props.data === "Administrative"))
    {
      route = '/ework/user/fetch_adminis_for_view';
      body = <AdRes username={this.props.username}  admin_action={this.props.admin_action}
      display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else if(this.props.data === "Research")
    {
      route = '/ework/user/fetch_research_for_view';
      body = <AdRes admin_action={this.props.admin_action}
       username={this.props.username} display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else if(this.props.data === "Academic")
    {
      route = '/ework/user/fetch_academic_for_view';
      body = <AdRes admin_action={this.props.admin_action}
       username={this.props.username} display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else if(this.props.data === "Perform")
    {
      route = '/ework/user/fetch_perform_for_view';
      body = <Perform admin_action={this.props.admin_action}
       username={this.props.username} display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else{
      body=
      <div></div>
    }
    return(
      <React.Fragment>
      {this.props.data.length>0 &&
       <div>
        {body}
       </div>
     }
      </React.Fragment>
    )
  }
}


class StudentUnderFaculty extends Component {
  constructor()
  {
    super()
    this.state={
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStudents();
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps.year!== this.props.year)
    {
      this.fetchStudents();
    }
  }

  fetchStudents=()=>{
    axios.post('/ework/user2/fetchStudents_UnderFaculty',
    {username:this.props.username,year:this.props.year})
    .then( res => {
        if(res.data)
        {
          this.setState({student_list:res.data,loading:false})
        }
    });
  }
  render() {
    if(this.state.loading)
    {
      return(
        <div>Loading...</div>
      )
    }
    else {
       return(
         <React.Fragment>
           {this.state.student_list.length>0 ?
               <StudentTable student_list={this.state.student_list} />
              :
              <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
           }
         </React.Fragment>
       )
    }
  }
}
