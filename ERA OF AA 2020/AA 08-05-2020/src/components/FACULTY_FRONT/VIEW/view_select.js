import React, { Component} from 'react'
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import IconButton from '@material-ui/core/IconButton';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import List from '@material-ui/core/List';
import { Redirect  } from 'react-router-dom'
import Nav from '../../dynnav'
import axios from 'axios'
import ContentLoader from "react-content-loader"

import OwnData from './FACULTY DATA/OWN DATA/view_own'
import SuperData from './FACULTY DATA/SUPER VIEW/view_super'
import StudentData from './studentSearchView'

require("datejs")



export default class Nstart extends Component {
  constructor()
  {
    super()
    this.state = {
      redirectTo:'',
      modal: false,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      allowed:false,
      loading:true,
      fetching:true,
      active:7,
      open_sidebar1:true,
      noti_route:true,
      user_session:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  openModal =() =>
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidMount()
  {
    this.getUser();
  }

  getUser()
  {
    axios.get('/ework/user/'
  )
     .then(response =>{
        if(response.status === 200)
        {
               if(response.data.user)
               {
                 this.openModal();
                 this.knowSuprimity();
                 this.setState({
                   user_session:response.data.user
                 })
               }
               else
               {
                 this.setState({
                   loading:false,
                 })
                 this.setState({
                   redirectTo:'/ework/faculty',
                 });
               }
         }
     })
  }


  knowSuprimity =()=>{
    axios.get('/ework/user/validate_list_view')
    .then( response => {
        if(response.data.permission === 1)
        {
          this.setState({
            allowed:true,
            fetching:false,
          })
        }
        else{
          this.setState({
            allowed:false,
            fetching:false,
          })
        }
        this.setState({
          loading:false,
        })
    });
  }

  handleSet=(e)=>{
    this.setState({
      active: e,
    })
    this.closeDrawer1();
  }


  closeDrawer1=()=>{
    this.setState({open_sidebar1:!this.state.open_sidebar1})
  }

  render() {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="95" y="50" rx="3" ry="3" width="220" height="80" />

      </ContentLoader>
    )
    if(this.state.loading === true)
    {
      return(
        <MyLoader />
      );
    }
    else{

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
    return (
      <React.Fragment>
        <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
               {!(this.state.fetching) ?
                 <React.Fragment>
                 {!(this.state.open_sidebar1) &&
                    <IconButton onClick={this.closeDrawer1} style={{float:'left',backgroundColor:'#311b92'}}>
                      <ChevronRightIcon style={{color:'white'}} />
                    </IconButton>
                  }
                 <Drawer style={{width:'550px'}} anchor="left" open={this.state.open_sidebar1}
                  onClose={this.closeDrawer1}>
                     <div style={{float:'right'}}>
                       <IconButton onClick={this.closeDrawer1}>
                         <ChevronLeftIcon />
                       </IconButton>
                     </div>
                   <Divider />
                       {this.state.allowed ?
                             <List>
                                   <ListItem button onClick={(e)=>{this.handleSet(0)}}>
                                     <ListItemText primary={"View OwnData"} />
                                   </ListItem>
                                   <ListItem button onClick={(e)=>{this.handleSet(1)}}>
                                     <ListItemText primary={"Super View"} />
                                   </ListItem>
                                   <ListItem button onClick={(e)=>{this.handleSet(2)}}>
                                     <ListItemText primary={"View Student Data"} />
                                   </ListItem>
                             </List>
                         :
                         <React.Fragment>
                           <List>
                                 <ListItem button onClick={(e)=>{this.handleSet(0)}}>
                                   <ListItemText primary={"View OwnData"} />
                                 </ListItem>
                                 <ListItem button onClick={(e)=>{this.handleSet(2)}}>
                                   <ListItemText primary={"View Student Data"} />
                                 </ListItem>
                           </List>
                         </React.Fragment>
                       }
                </Drawer>
                 </React.Fragment>
                 :

                 <React.Fragment>
                     <Backdrop  open={true} >
                       <CircularProgress color="secondary" />&emsp;
                       <div style={{color:'yellow'}}>Validating You....</div>
                     </Backdrop>
                 </React.Fragment>
               }
               <React.Fragment>
                           <Direction user_session={this.state.user_session} selected={this.state.active} />
               </React.Fragment>

       </React.Fragment>
    );
  }
}
  }
}


class Direction extends Component{
  constructor()
  {
    super()
    this.state={

    }
  }
  render()
  {
    if(this.props.selected === 0)
    {
      return(
        <OwnData />
      )
    }
    else if (this.props.selected === 1) {
      return(
        <SuperData />
      )
    }
    else if (this.props.selected === 2) {
      return(
        <StudentData user_session={this.props.user_session} />
      )
    }
    else {
      return(
        <div></div>
      )
    }
  }
}
