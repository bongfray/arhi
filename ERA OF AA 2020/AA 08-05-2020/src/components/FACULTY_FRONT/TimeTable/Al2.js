import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';


import axios from 'axios'
import Switch from './slot_handle_problem';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class Allot extends React.Component {
  constructor() {
    super()
    this.state = {
       freeparts:'Curriculum',
        sending:false,
        selected:'',
        covered_topic:'',
        problem_statement:'',
        compday:'',
        comphour:'',
        compday_order:'',
        allot:'',
        count: 0,
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        date:'',
        year:'',
        month:'',
        compense_faculty:'',
        snack_open:false,
        alert_type:'',
        snack_msg:'',
    }
  }


  handleChecked =(e) =>{
    this.setState({
      selected: e.target.value,
    })
  }

  updateAllotV =(userObject) => {
    this.setState(userObject)
  }


  closeModal=()=>{
    this.props.closeModal();
  }
  colorChange=()=>{
    this.props.color()
  }

handleAlloted =(e) =>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();
  let freeparts,verified,positive_count,negative_count,datas_to_send;
  e.preventDefault();
  if(this.state.selected === "Problem")
  {
    freeparts = '';
  }
  else
  {
    freeparts = "Curriculum";
    verified = false;
    positive_count = 0;
    negative_count = 0;
  }

  if(!this.state.selected)
  {
    this.setState({
      snack_open:true,
      snack_msg:'Enter All the Details First',
      alert_type:'warning',
    });
  }
  else{
    if(this.state.selected === 'Problem')
    {
      if(!this.state.problem_statement)
      {
        this.setState({
          snack_open:true,
          snack_msg:'You should mention the reason !!',
          alert_type:'warning',
        });
      }
      else
      {
        if((this.state.comphour) || (this.state.compday))
        {
          if((!this.state.compday) || (!this.state.comphour) || (!this.state.compday_order))
          {
            this.setState({
              snack_open:true,
              snack_msg:'Enter All the Details First',
              alert_type:'warning',
            });
          }
          else
          {
             datas_to_send={
              username: this.props.username,
              freefield:"Academic",
              freeparts:freeparts,
              date:this.props.date,
              month: this.props.month,
              year: this.props.year,
              week:week,
              day_order: this.props.day_order,
              hour:this.props.content.number,
              for_year:this.props.datas_a.year,
              for_batch:this.props.datas_a.batch,
              for_sem:this.props.datas_a.sem,
              selected:this.state.selected,
              problem_statement:this.state.problem_statement,
              slot:this.props.slot,
              subject_code:this.props.datas_a.subject_code,
              time:this.props.content.time,
              problem_compensation_day_order:this.state.compday_order,
              problem_compensation_date:this.state.compday,
              problem_compensation_hour:this.state.comphour,
            }
            this.sendAllot(datas_to_send);
          }
        }
        else
        {
          if(this.state.compense_faculty)
          {
            this.checkWithFaculty(freeparts,verified,positive_count,negative_count,
            this.state.selected,'',this.state.problem_statement,'','','',
            this.state.compense_faculty,this.props)
          }
          else
          {
             datas_to_send={
              username: this.props.username,
              freefield:"Academic",
              freeparts:freeparts,
              date:this.props.date,
              month: this.props.month,
              year: this.props.year,
              week:week,
              day_order: this.props.day_order,
              hour:this.props.content.number,
              for_year:this.props.datas_a.year,
              for_batch:this.props.datas_a.batch,
              for_sem:this.props.datas_a.sem,
              selected:this.state.selected,
              problem_statement:this.state.problem_statement,
              slot:this.props.slot,
              subject_code:this.props.datas_a.subject_code,
              time:this.props.content.time,
            }
            this.sendAllot(datas_to_send);
          }
        }
      }
    }
    else if(this.state.selected === 'Completed')
    {
      if(!this.state.covered_topic)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter All the Details First',
          alert_type:'warning',
        });
      }
      else
      {
        datas_to_send={
          username: this.props.username,
          freefield:"Academic",
          freeparts:freeparts,
          date:this.props.date,
          month: this.props.month,
          year: this.props.year,
          week:week,
          day_order: this.props.day_order,
          hour:this.props.content.number,
          for_year:this.props.datas_a.year,
          for_batch:this.props.datas_a.batch,
          for_sem:this.props.datas_a.sem,
          selected:this.state.selected,
          covered:this.state.covered_topic.toLowerCase(),
          slot:this.props.slot,
          subject_code:this.props.datas_a.subject_code,
          time:this.props.content.time,
          verified:verified,
          positive_count:positive_count,
          negative_count:negative_count,
        }
        this.sendAllot(datas_to_send);
      }
    }
  }
}


sendAllot=(datas)=>
{
        axios.post('/ework/user/timeallot',datas,
        this.setState({
          sending:true
        })
      )
          .then(response => {
            this.setState({sending:''})
            if(response.status===200){
               if(response.data==='done')
              {
                if(datas.freeparts === 'Curriculum')
                {
                  this.sendNoti_To_Student()
                }
                this.setState({
                  snack_open:true,
                  snack_msg:'Submitted !!',
                  alert_type:'success',
                });
                if(this.props.foreign_ref === "true")
                {
                          this.props.closeref();
                }
                else{
                  this.closeModal();
                  this.colorChange();
                }
              }
              else if(response.data === 'no')
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Already Submitted !!',
                  alert_type:'info',
                });
                 this.closeModal();
              }
              else if(response.data === 'spam')
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Invalid Request !!',
                  alert_type:'error',
                });
              }
            }
          })
}


checkWithFaculty=(freeparts,verified,positive_count,negative_count,
  selected,covered_topic,problem_statement,compday,comphour,
  saved_slots,compense_faculty,incoming_props)=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();
  axios.post('/ework/user/check_possibility',{id:compense_faculty.toUpperCase()
    ,week:week,props_content:this.props})
  .then( res => {
    if(res.data === 'no')
    {
      this.setState({
        snack_open:true,
        snack_msg:'Invalid Request !!',
        alert_type:'error',
      });
        this.setState(this.initialState);
      return false;
    }
    else
    {
      this.setState({
        snack_open:true,
        snack_msg:'Submitting....',
        alert_type:'info',
      });
          var datas={
            username: this.props.username,
            freefield:"Academic",
            freeparts:freeparts,
            date:this.props.date,
            month: this.props.month,
            year: this.props.year,
            week:week,
            day_order: this.props.day_order,
            hour:this.props.content.number,
            for_year:this.props.datas_a.year,
            for_batch:this.props.datas_a.batch,
            for_sem:this.props.datas_a.sem,
            selected:this.state.selected,
            problem_statement:this.state.problem_statement,
            slot:this.props.slot,
            subject_code:this.props.datas_a.subject_code,
            time:this.props.content.time,
            verified:true,
            compense_faculty:this.state.compense_faculty.toUpperCase(),
            compense_faculty_topic:'',
          }
          this.sendAllot(datas);
    }
  });
}

sendNoti_To_Student=()=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();

  axios.post('/ework/user2/send_noti_on_class_complition',
  {week:week,slot:this.props.slot,for_year:this.props.datas_a.year,
  for_batch:this.props.datas_a.batch,for_sem:this.props.datas_a.sem,
  day_order: this.props.day_order,subject_code:this.props.datas_a.subject_code,
  hour:this.props.content.number,action:'Class Completed',
  faculty_id:this.props.username,covered_topic:this.state.covered_topic.toLowerCase(),date:
  this.props.date,month:this.props.month,year:this.props.year})
  .then( res => {

  });
}

render(){
  let disabled= false;
  let time = parseFloat(new Date().toString("HH.mm"));
  if((parseFloat(this.props.content.start_time))>time)
  {
    disabled = true;
  }
  return(
    <div style={{padding:'15px'}}>

    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    open={this.state.snack_open} autoHideDuration={2000}
    onClose={()=>this.setState({snack_open:false})}>
      <Alert onClose={()=>this.setState({snack_open:false})}
      severity={this.state.alert_type}>
        {this.state.snack_msg}
      </Alert>
    </Snackbar>

    <Typography variant="button" display="block" gutterBottom>
      <Typography color="secondary">Reminder:</Typography> This slot is booked at the beginig of Semester.
      You are not allowed to edit it. And one more thing, you can't submit the data for alloted slot, before its gets started.
    </Typography>
    <br />
    <Grid container spacing={1}>
      <Grid item xs={12} sm={6}>
        <FormControl component="fieldset">
          <RadioGroup aria-label="gender" name="gender1" value={this.state.selected} onChange={this.handleChecked}>
            <FormControlLabel disabled={disabled} value='Completed' control={<Radio />} style={{color:'green'}} label="Class/Work Completed" />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid item xs={12} sm={6}>
        {!this.props.cday &&
          <FormControl component="fieldset">
            <RadioGroup aria-label="gender" name="gender1" value={this.state.selected} onChange={this.handleChecked}>
              <FormControlLabel value='Problem' control={<Radio />} style={{color:'red'}} label="Problem With Work/Class Completion(like Slot Cancelled)" />
            </RadioGroup>
          </FormControl>
        }
      </Grid>
    </Grid>
      <DivOpen prop_datas={this.props} selected={this.state.selected} updateAllotV={this.updateAllotV}/>
      <br /><br />
     <Button variant="contained" style={{float:'right'}} color="primary"
     disabled={this.state.sending} onClick={this.handleAlloted}>SUBMIT</Button>
   </div>
  );
}
}





/*-------for check radio button----------------------------------- */

 class DivOpen extends Component{
   constructor(){
     super();
     this.state ={
       covered_topic:'',
       problem_statement:'',
     }
   }
handleAllotData= (evt) =>{
     const value = evt.target.value;
     this.setState({
       [evt.target.name]: value
     });
     this.props.updateAllotV({
       [evt.target.name]: value,
     });
   }



   render(){

     if(this.props.selected ==="Completed")
     {
       return(
           <TextField
             id="outlined-multiline-static"
             label="Enter the the things covered in this alloted slot."
             multiline
             rows="3"
             fullWidth
             name="covered_topic" value={this.state.covered_topic}  onChange={this.handleAllotData}
             variant="filled"
           />
       );
     }
     else if(this.props.selected === "Problem")
     {
       return(
         <React.Fragment>
           <TextField
             id="outlined-multiline-static"
             label="Enter the reason"
             multiline
             rows="3"
             fullWidth
             name="problem_statement" className="validate" value={this.state.problem_statement} onChange={this.handleAllotData}
             variant="filled"
           />
         <br /><br />
           <Switch prop_datas={this.props.prop_datas} updateAllotV={this.props.updateAllotV}/>
         </React.Fragment>
       );
     }
     else
     {
       return(
       <div></div>
     );
     }
   }
 }
