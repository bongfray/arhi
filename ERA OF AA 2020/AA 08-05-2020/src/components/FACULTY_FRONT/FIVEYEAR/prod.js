import React from 'react';
import axios from 'axios';
import Checkbox from '@material-ui/core/Checkbox';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching:true,
      username:'',
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user/del_five_year_plan',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/ework/user/fetch_five_year_plan',{
    action: this.props.action
  })
  .then(response =>{
    this.setState({
     products: response.data,
     fetching:false,
   })
  })
}

  render() {
    if(this.state.fetching)
    {
      return(
        <div style={{textAlign:'center'}}>Loading.....</div>
      )
    }
    else{
      return(
        <React.Fragment>
        <TableContainer component={Paper}>
          <Table  aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center"><b>No.</b></TableCell>
                <TableCell align="center"><b>Plan</b></TableCell>
                <TableCell align="center"><b>Verification Link</b></TableCell>
                <TableCell align="center"><b>Status</b></TableCell>
                <TableCell align="center"><b>Action</b></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.products.map((content,index) => (
                <TableRow key={index}>
                  <TableCell align="center">{index+1}</TableCell>
                  <TableCell align="center">{content.content}</TableCell>
                  <TableCell align="center">
                      {content.verification_link ?
                        <a target="_blank" rel="noopener noreferrer" style={{color:'red'}} href={content.verification_link}>Verification Link</a>
                        :
                        <div style={{color:'red'}}>Not Found</div>
                      }
                  </TableCell>
                  <TableCell align="center"><Complete content={content}/></TableCell>
                  <TableCell align="center">
                    <EditIcon className="go"
                    onClick={() => this.props.editProduct(content.serial,this.props.action)}/>
                    &nbsp;
                    <DeleteIcon className="go"
                     onClick={() => this.deleteProduct(content.serial,this.props.action)}  />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

        </React.Fragment>
      )
    }

  }
}
class Complete extends React.Component{
  constructor(props)
  {
    super(props)
    this.state={
      id:'',
      blue_allot:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user/fetch_complete_fiveyear',{serial:this.props.content.serial})
    .then( res => {
      if(res.data)
      {
        this.setState({blue_allot:res.data})
      }
    });
  }
  handleBlue_AllotChange =(e,index) =>{
      const value = e.target.name;
   this.setState({ blue_allot: !this.state.blue_allot, index,id:value});
   axios.post('/ework/user/update_status_fiveyear',{status:!this.state.blue_allot,key:value})
   .then( res => {

   });

  }
  render()
  {
    return(
      <React.Fragment>
          <Checkbox
            name={this.props.content.serial} checked={this.state.blue_allot}
            value={this.state.blue_allot} onChange={e => this.handleBlue_AllotChange(e, this.props.content.serial)}
            inputProps={{ 'aria-label': 'primary checkbox' }}
            />
        Completed ?
      </React.Fragment>
    )
  }
}
