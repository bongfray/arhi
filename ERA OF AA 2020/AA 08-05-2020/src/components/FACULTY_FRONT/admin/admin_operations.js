import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {CircularProgress,Typography,ListItemIcon,Snackbar} from '@material-ui/core';
import {IconButton,Hidden,Button,TextField} from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import MuiAlert from '@material-ui/lab/Alert';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import DeleteIcon from '@material-ui/icons/Delete';

import Admin2 from './operation_type'
import Nav from '../../dynnav'
import Arhi from './arhi.js';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class Ap extends Component{
  constructor() {
    super()
    this.state={
      isChecked:0,
      active:0,
      logout:'/ework/user/logout',
      get:'/ework/user/',
      noti_route:true,
      nav_route: '',
      toggled:false,
      loader:true,
      user:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
      this.fetchlogin();
  }
  fetchlogin = () =>{
      axios.get('/ework/user/')
      .then(response=>{
        if(response.data.user === null)
        {
          this.setState({
            loader:false,
            redirectTo:'/ework/',
          });
        }
        else {
          this.setState({user:response.data.user,loader:false})
        }
      })
    }
  handleComp = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }


   color =(position) =>{
     if (this.state.active === position) {
         return 'red';
       }
       return '';
   }

  render()
  {
    const dstyle = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
      if(this.state.loader)
      {
        return(
          <div>Loading .....</div>
        )
      }
      else{
    return(
        <React.Fragment>
          <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div style={{marginTop:'10px'}}>

               {this.state.toggled ===  false &&
                 <div onClick={()=>this.setState({toggled:true})} className="dig"
                   style={{float:'center',boxShadow: '0px 10px 10px 2px pink',textAlign:'center',
                   width:'30px',borderRadius: '0px 0px 20px 20px'}}>
                   <ArrowDropDownIcon />
                 </div>
               }

               <Drawer anchor="top" open={this.state.toggled} onClose={()=>this.setState({toggled:false})}>
                 <div
                    className={dstyle.fullList}
                    role="presentation"
                    onKeyDown={()=>this.setState({toggled:false})}
                    >
                    <List>
                        <ListItem onClick={(e)=>{this.handleComp(0)}} button style={{color:this.color(0)}}>
                          <ListItemText align="center" primary={"System"} />
                        </ListItem>
                        <ListItem onClick={(e)=>{this.handleComp(1)}} button style={{color:this.color(1)}}>
                          <ListItemText align="center" primary={"Admin Operations"} />
                        </ListItem>
                        <ListItem onClick={(e)=>{this.handleComp(2)}} button style={{color:this.color(2)}}>
                          <ListItemText align="center" primary={"Student Actions"} />
                        </ListItem>
                        <ListItem onClick={(e)=>{this.handleComp(3)}} button style={{color:this.color(3)}}>
                          <ListItemText align="center" primary={"Complaints/Report"} />
                        </ListItem>
                    </List>
                  </div>
              </Drawer>
          </div>

              <System user={this.state.user} choice={this.state.isChecked} toggled={this.state.toggled} />


       </React.Fragment>
    )
   }
  }
  }
}


class System extends Component {
  constructor() {
    super()
    this.state={
      activated_user:'',
      suspended_user:'',
      suser:'',
      user:'',
      total_user:'',
      d_user:'',
      admin_user:'',
      fetch_system:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser()
  }
  fetchUser()
  {
    axios.get('/ework/user/know_no_of_user')
    .then( res => {
      const activated_user = (res.data.filter(item => item.active === true)).length;
      const suspended_user = (res.data.filter(item => item.suspension_status === true)).length;
      const user = (res.data.filter(item => item.count=== 0 || item.count === 1 || item.count === 2)).length;
      const admin_user = (res.data.filter(item => item.h_order=== 0)).length;
      const d_user = (res.data.filter(item => item.h_order=== 0.5)).length;
        this.setState({activated_user:activated_user,suspended_user:suspended_user,admin_user:admin_user,user:user,d_user:d_user})
        this.fetchStudent(res.data.length);
    });

  }
  fetchStudent(user)
  {
    axios.get('/ework/user2/know_no_of_suser')
    .then( res => {
        let total =res.data.length;
        this.setState({total_user:user+total,suser:res.data.length,fetch_system:false})
    });
  }
  render()
  {
    if(this.props.choice === 1)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Admin user={this.props.user} />
        }
        </React.Fragment>
      )
    }
    else if(this.props.choice === 2)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Arhi user={this.props.user}/>
        }
        </React.Fragment>
      )
    }
    else if(this.props.choice === 3)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Complaints />
        }
        </React.Fragment>
      )
    }
    else{
      if(this.state.fetch_system)
      {
        return(
          <div style={{float:'center'}}>
            <CircularProgress />
          </div>
        )
      }
      else{
      return(
        <React.Fragment>
        <div style={{marginTop:'50px'}}>
          <Typography style={{color:'#009688',fontFamily: "Cinzel",fontSize:'30px'}} variant="h5" align="center">Welcome To EWork Admin Panel</Typography>
        </div><br /><br />
         <Grid container style={{padding:'5px'}}>
            <Grid item xs={4} sm={4} lg={2} xl={2} md={2} style={{padding:'2px'}}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>TOTAL USER</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.total_user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={4} lg={2} xl={2} md={2} style={{padding:'2px'}}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>FACULTY</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={4} lg={2} xl={2} md={2} style={{padding:'2px'}}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>STUDENT</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.suser}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={4} lg={2} xl={2} md={2} style={{padding:'2px'}}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>DEPARTMENT ADMIN</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.d_user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={4} lg={2} xl={2} md={2} style={{padding:'2px'}}>
               <Paper elevation={3} style={{height:'110px'}}>
                 <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>ADMIN USER</div>
                 <div style={{paddingTop:'30px'}}>
                   <Typography variant="h5" align="center">{this.state.admin_user}</Typography>
                 </div>
               </Paper>
            </Grid>
            <Grid item xs={4} sm={4} lg={2} xl={2} md={2} style={{padding:'2px'}}>
                <Paper elevation={3} style={{height:'110px'}}>
                  <div style={{backgroundColor:'#ff4081',color:'white',textAlign:'center'}}>SUSPENDED USER</div>
                  <div style={{paddingTop:'30px'}}>
                    <Typography variant="h5" align="center">{this.state.suspended_user}</Typography>
                  </div>
                </Paper>
            </Grid>
         </Grid>
        </React.Fragment>
      )
    }
    }
  }
}


class Admin extends Component{
constructor(){
    super();
    this.state ={
        radio:[{name:'radio1',color:'blue',value:'Insert'},{name:'radio2',color:'black',value:'Update'},
        {name:'radio3',color:'orange',value:'Super'},{name:'radio4',color:'red',value:'Delete'},
        {name:'radio5',color:'yellow',value:'Modify'},{name:'radio6',color:'aqua',value:'View'},
        {name:'radio7',color:'red',value:'Communicate'}],
    }
}
render()
{
    return(
        <React.Fragment>
          <Admin2 radio={this.state.radio} user={this.props.user} />
        </React.Fragment>
    );
}
}



class Complaints extends Component {
  constructor() {
    super()
    this.state={
      complaint:[],
      complaint_details:'',
      complaint_subject:'',
      official_id:'',
      serial_no:0,
      reply:false,
      forward:false,
      sendto:'',
      message_for_receiver:'',
      active:'',
      complaint_fetching:true,
      open_sidebar1:true,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchComplaint();
  }


  color =(position) =>{
    if (this.state.active === position) {
        return "#33eaff";
      }
      return "";
  }


  fetchComplaint =()=>{
    axios.get('/ework/user/fetch_complaint')
    .then( res => {
        if(res.data){
          this.setState({complaint:res.data,complaint_fetching:false})
        }
    });
  }

    sendData =(index,official_id,complaint_subject,complaint,serial)=>{
      if (this.state.active === index) {
        this.setState({active : null})
      } else {
        this.setState({active : index})
      }
      this.setState({official_id:official_id,complaint_subject:complaint_subject,complaint_details:complaint,serial_no:serial,open_sidebar1:false})
    }
    onReply =(e)=>{
      if(e === 0)
      {
        this.setState({reply:true,forward:false})
      }
      else
      {
        this.setState({forward:true,reply:false})
      }
    }

    handleInput =(e)=>{
      this.setState({[e.target.name]:e.target.value})
    }

    sendReply =(no,id)=>{
      const { complaint } = this.state;
      if(!(this.state.reply_message))
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter all the details !!',
          alert_type:'warning',
        })
      }
      else{
      axios.post('/ework/user/send_reply_to_complaint',{serial:no,official_id:id,message:this.state.reply_message})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({reply:false,message:false,complaint: complaint.filter(product => product.serial !== no),reply_message:''})
            this.setState({
              snack_open:true,
              snack_msg:'Replied !!',
              alert_type:'warning',
            })
        }
      });
     }
    }
    forwardMsg =() =>{
    const { complaint } = this.state;
    if(!(this.state.sendto) || !(this.state.message_for_receiver))
    {
      this.setState({
        snack_open:true,
        snack_msg:'Enter all the details !!',
        alert_type:'warning',
      })
    }
    else{
      axios.post('/ework/user/forward_complaint',{data:this.state})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({forward:false,message:false,complaint: complaint.filter(product => product.serial !== this.state.serial_no),sendto:'',message_for_receiver:''})
          this.setState({
            snack_open:true,
            snack_msg:'FORWARDED !!',
            alert_type:'info',
            block_click:true
          })
        }
      });
      }
    }

    deleteOne =(id)=>{
      const { complaint } = this.state;
      axios.post('/ework/user/delete_id',{serial:id})
      .then( res => {
          if(res.data)
          {
            this.setState({complaint: complaint.filter(product => product.serial !== id)})
          }
      });
    }
    closeDrawer1=()=>{
      this.setState({open_sidebar1:!this.state.open_sidebar1})
    }

  render()
  {
    if(this.state.complaint_fetching)
    {
      return(
        <div style={{float:'center'}}>
          <CircularProgress />
        </div>
      )
    }
    else
    {
      if(this.state.complaint.length === 0)
      {
        return(
          <Typography align="center" variant="h4" style={{marginTop:'120px'}}>Can't able to find any complaint !!</Typography>
        )
      }
      else
      {
    return(
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      <Grid container>
          <Hidden xsDown>
             <Grid item xs={12} sm={3} lg={3} xl={3} md={4}>
                    <Grid container>
                      <Grid item xs={12} sm={12} lg={12} xl={12} md={12}>
                         <Typography align="center" variant="h6">Complaints or Report</Typography>
                         <hr />
                      </Grid>
                    </Grid>

                    {this.state.complaint.map((content,index)=>{
                      return(
                        <React.Fragment key={index}>
                          <Grid container key={index} className="particular" style={{backgroundColor:this.color(index)}}
                          onClick={() =>this.sendData(index,content.official_id,content.complaint_subject,content.complaint,content.serial)}>
                            <Grid item xs={8} sm={8} lg={8} xl={8} md={8}>
                              <Typography variant="h6" className="dotted">{content.complaint_subject}</Typography>
                              <label className="dotted">{content.complaint}</label>
                            </Grid>
                            <Grid item xs={4} sm={4} lg={4} xl={4} md={4}>
                                <IconButton style={{marginTop:'10px',float:'right'}} color="secondary" aria-label="Delete" component="span"
                                onClick={()=>this.deleteOne(content.serial)}>
                                  <DeleteIcon />
                                </IconButton>
                            </Grid>
                          </Grid>
                          <br />
                        </React.Fragment>
                      );
                    })}
             </Grid>
           </Hidden>
           <Hidden smUp>
               <Grid item xs={12}>
                   {!(this.state.open_sidebar1) &&
                      <IconButton onClick={this.closeDrawer1} style={{float:'left',backgroundColor:'#311b92'}}>
                        <ChevronRightIcon style={{color:'white'}} />
                      </IconButton>
                    }
                   <Drawer  style={{width:'550px'}} anchor="left" open={this.state.open_sidebar1}
                    onClose={this.closeDrawer1}>
                     <IconButton onClick={this.closeDrawer1}>
                       <ChevronLeftIcon />
                     </IconButton>
                     <Divider />
                     <List>
                       {this.state.complaint.map((content,index)=>(
                             <ListItem button key={index}
                             onClick={() =>this.sendData(index,content.official_id,content.complaint_subject,content.complaint,content.serial)}>
                               <ListItemText primary={content.complaint_subject} />
                               <ListItemIcon>
                                 <IconButton style={{marginTop:'5px',float:'right'}} color="secondary" aria-label="Delete" component="span"
                                 onClick={()=>this.deleteOne(content.serial)}>
                                   <DeleteIcon />
                                 </IconButton>
                               </ListItemIcon>
                             </ListItem>
                        ))}
                        </List>
                  </Drawer>
               </Grid>
           </Hidden>

         <Grid item xs={12} sm={9} lg={9} xl={9} md={8} style={{padding:'10px'}}>
            {this.state.complaint_subject &&
              <Paper elevation={3} style={{padding:'4px'}}>
                <Grid container spacing={1} style={{padding:'4px'}}>
                    <Grid item xs={12} sm={6}>
                          <Typography variant="h6"
                          style={{color:'white',backgroundColor:'#f73378',textAlign:'center',padding:'8px'}}>COMPLAINT DETAILS</Typography>
                            <Grid container spacing={1} style={{padding:'2px 0px 0px 0px'}}>
                              <Grid item xs={6} sm={6}>
                                <Typography variant="button" display="block" gutterBottom>Complaint From : </Typography>
                              </Grid>
                              <Grid item xs={6} sm={6}>
                                <Typography variant="overline" display="block" gutterBottom>{this.state.official_id}</Typography>
                              </Grid>
                            </Grid>

                            <Grid container spacing={1}>
                                <Grid item xs={6} sm={6}>
                                    <Typography variant="button" display="block" gutterBottom>Complaint Subject : </Typography>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                    <Typography variant="overline" display="block" gutterBottom>{this.state.complaint_subject}</Typography>
                                </Grid>
                            </Grid>

                            <Grid container spacing={1}>
                                <Grid item xs={6} sm={6}>
                                   <Typography variant="button" display="block" gutterBottom>Complaint Details : </Typography>
                                </Grid>
                            </Grid>
                            <br />

                            <div style={{wordWrap: 'break-word',padding:'20px'}}>
                                <Typography variant="overline" display="block" gutterBottom>{this.state.complaint_details}</Typography>
                            </div>

                            <Grid container spacing={1}>
                                <Grid item xs={6} sm={6}>
                                  <Button variant="outlined" align="left" color="secondary" onClick={()=>this.onReply(0)}>Reply</Button>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <Button variant="outlined" align="right" color="primary" onClick={()=>this.onReply(1)}>Forward</Button>
                                </Grid>
                            </Grid>
                      </Grid>

                      <Grid item xs={12} sm={6}>
                          {this.state.reply &&
                             <React.Fragment>
                                <Grid container spacing={1}>
                                  <Grid item xs={12} sm={12}>
                                      <Typography variant="h6" style={{color:'white',backgroundColor:'#f73378',textAlign:'center',padding:'8px'}}>REPLY TO USER</Typography>
                                      <Grid container spacing={1}>
                                        <Grid item xs={6} sm={6} style={{padding:'4px 0px 0px 0px'}}>
                                          <label>To : </label><span>{this.state.official_id}</span>
                                        </Grid>
                                      </Grid>
                                      <br />
                                      <Grid container spacing={1}>
                                        <Grid item xs={12} sm={12}>
                                          <TextField
                                            id="outlined-multiline-static"
                                            label="Enter the Message"
                                            multiline
                                            rows="4"
                                            fullWidth
                                            name="reply_message"
                                            value={this.state.reply_message}
                                            onChange={this.handleInput}
                                            variant="outlined"
                                          />
                                        </Grid>
                                      </Grid>
                                      <br />
                                      <Button variant="contained" color="primary" style={{textAlign:'right',marginBottom:'5px'}}
                                       onClick={()=>this.sendReply(this.state.serial_no,this.state.official_id)}>REPLY</Button>
                                    </Grid>
                                </Grid>
                                <span align="center" color="secondary">Note : On your reply this complaint will be closed !!</span>
                             </React.Fragment>
                          }
                          {this.state.forward &&
                             <React.Fragment>
                                <Grid container spacing={1}>
                                   <Grid item xs={12} sm={12}>
                                      <Typography variant="h6"
                                      style={{color:'white',backgroundColor:'#f73378',textAlign:'center',padding:'8px'}}>FORWARD MESSAGE</Typography>
                                      <br />
                                      <TextField id="forward-to" name="sendto" value={this.state.sendto} className="validate" onChange={this.handleInput}
                                       type="text" fullWidth required label="Send To" variant="outlined" helperText="Should be Official Id" />
                                    </Grid>
                                </Grid><br />
                                <div style={{padding:'3px'}}>
                                      <TextField
                                        disabled
                                        id="filled-disabled1"
                                        label="Complaint From"
                                        defaultValue={this.state.official_id}
                                        variant="outlined"
                                        fullWidth
                                      />
                                      <br /><br />
                                      <TextField
                                        disabled
                                        id="filled-disabled2"
                                        label="Complaint Subject"
                                        defaultValue={this.state.complaint_subject}
                                        variant="outlined"
                                        fullWidth
                                      />
                                </div>
                                <br />
                                  <div>
                                    <TextField
                                      id="mm" name="message_for_receiver" className="validate" value={this.state.message_for_receiver}
                                      onChange={this.handleInput} type="text" required
                                      label="Any Message For Receiver"
                                      variant="outlined"
                                      fullWidth
                                    />
                                  </div>
                                  <br />
                                <Grid container spacing={1}>
                                  <Button style={{float:'right'}} variant="contained" color="primary" onClick={this.forwardMsg}>Forward</Button>
                                </Grid>
                                <br />
                                <div>
                                  <span style={{color:'red'}}>Remember - </span> Once you forward this complaint, this complaint
                                  will be inactive for the admin user. The person who will receive this message can only answer or delete it.
                                </div>
                             </React.Fragment>
                          }
                      </Grid>
                </Grid>
            </Paper>
            }
         </Grid>
      </Grid>
    </React.Fragment>
    );
     }
   }
  }
}
