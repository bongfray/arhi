import React from 'react';
import axios from 'axios';
import {CircularProgress,Backdrop} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import ValidateUser from '../validateUser';


export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      index:'',
      modal:false,
      disabled:false,
      allowed:false,
      error: null,
      products: [],
      action:'',
      loading:true,
      success:'none',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.setState({loading:true})
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({success:'block',disabled:true,id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  this.setState({loading:true})
  var delroute;
  if((index === "faculty") || (index === "department_admin")){
    delroute = "/ework/user/delnav";
  }
  else if(index === "student"){
  delroute = "/ework/user2/delnav";
 }
else if((index === "Administrative") || (index === "Academic") || (index === "Section") || (index === "Research") || (index === "Section")||(index === "Designation") || (index === "Department")){
  delroute = "/ework/user/del_inserted_data";
 }
 else if((index === "Skill-For-Student")){
   delroute = "/ework/user2/del_inserted_data";
  }

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:'',
        success:'none',
        response: response,
        products: products.filter(product => product.serial !== productId),
        loading:false,
     })
    })
}


fetch =(action) =>{
  let fetchroute;
  if((action === "faculty") || (action === "department_admin"))
  {
    fetchroute = "/ework/user/fetchnav";
  }
  else if(action === "student")
  {
    fetchroute = "/ework/user2/fetch_student_nav"
  }
  else if((action === "Administrative") || (action === "Academic") || (action === "Research") || (action === "Section")||(action === "Designation") || (action === "Department"))
  {
    fetchroute = "/ework/user/fetch_in_admin";
  }
  else if((action === "Skill-For-Student"))
  {
    fetchroute = "/ework/user2/fetch_in_admin";
  }
  else
  {
    fetchroute = "/ework/user2/fetch_in_admin";
  }

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
  })
  .then(response =>{
    this.setState({
    loading:false,
     products: response.data,
   })
  })
}
  render() {

    const {products} = this.state;
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Processing Your Request....</div>
        </Backdrop>
      );
    }
    else
    {
      return(
        <React.Fragment>

        {this.state.success === 'block' &&
        <ValidateUser showDiv={this.showDiv}/>
        }

          <TableContainer component={Paper} style={{marginTop:'8px'}}>
            <Table size="small" aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Index</TableCell>
                  {this.props.data.fielddata.map((content,index)=>(
                    <TableCell align="center" key={index}><b>{content.header}</b></TableCell>
                  ))}
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map((row,index) => (
                  <TableRow key={index}>
                    <TableCell align="center">
                      {index+1}
                    </TableCell>
                    {this.props.data.fielddata.map((content,ind)=>(
                      <TableCell align="center" key={ind}>{row[content.name]}</TableCell>
                    ))}
                    <TableCell align="center">
                       <div style={{textAlign:'center'}}>
                          <IconButton aria-label="edit" color="primary" onClick={() => this.props.editProduct(row.serial,this.props.action)}>
                            <EditIcon fontSize="inherit" />
                          </IconButton>
                          <IconButton aria-label="edit" color="secondary" onClick={() => this.deleteProduct(row.serial,this.props.action)}>
                            <DeleteIcon fontSize="inherit" />
                          </IconButton>
                        </div>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </React.Fragment>
      )
    }

  }
}
