import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import ColorRep from '../TimeTable/colordiv.js'
import Allot from '../TimeTable/Al2.js'
import Free from '../TimeTable/Free.js'
require("datejs")

/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
})

export default class Simple extends Component{

  constructor() {
    super();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      approved:[],
      viewdata:[],
      available:'no',
      notfound:'',
      loading:true,
    }
  }


  componentDidMount(){
    this.knowExpiry();

  }

  knowExpiry = ()=>{
    axios.post('/ework/user/request_Expiry_Check')
    .then( res => {
        if(res.data)
        {
          this.fetchApprove()
        }
    });
  }
  componentDidUpdate =(prevProps) => {
    if (prevProps.request_props !== this.props.request_props) {
      this.fetchApprove();
    }
  }

  fetchApprove = () =>{
    axios.get('/ework/user/fetch_requested_list').then(res=>{
      if(res.data)
      {
        var requests = res.data.filter(item=>!(item.action === 'For Blue Print Render'))
        this.setState({
          approved: requests,
          loading:false,
        })
      }
    })
  }


  handleViewRequest = (content,index) =>{
    this.componentDidUpdate(index)
    this.setState({
      available:'yes',
      viewdata: content,
    })
  }


deleteRequest=(key)=>{
  axios.post('/ework/user/request_Expiry_Check',{serial:key})
  .then( res => {
      if(res.data)
      {
        this.fetchApprove();
      }
  });
}

      render(){
              return(
                <React.Fragment>
                {this.state.loading ?
                  <Backdrop  open={true} style={{zIndex:'2040'}}>
                    <CircularProgress style={{color:'yellow'}}/>&emsp;
                    <div style={{color:'yellow'}}>Processing Your Request...</div>
                  </Backdrop>
                  :
                      <React.Fragment>
                          <Typography variant="h6" align="center" style={{height:'45px',paddingTop:'12px'}}
                          color="secondary" gutterBottom>
                            STATUS OF YOUR REQUESTS
                          </Typography>
                        <br />

                      {this.state.approved.length!==0 ?
                        <React.Fragment>

                        <TableContainer component={Paper}>
                          <Table aria-label="simple table">
                            <TableHead>
                              <TableRow>
                                <TableCell>Official ID</TableCell>
                                <TableCell align="center">R DayOrder</TableCell>
                                <TableCell align="center">R Date</TableCell>
                                <TableCell align="center">Expire Date</TableCell>
                                <TableCell align="center">Deny Reason</TableCell>
                                <TableCell align="center">Hour</TableCell>
                                <TableCell align="center">Reason</TableCell>
                              <TableCell align="center">Status</TableCell>
                              <TableCell align="center">Action</TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {this.state.approved.map((row,index) => (
                                <TableRow key={index}>
                                  <TableCell component="th" scope="row">
                                    {row.username}
                                  </TableCell>
                                  <TableCell align="center">{row.day_order}</TableCell>
                                  <TableCell align="center">{row.req_date}/{row.req_month}/{row.req_year}</TableCell>
                                  <TableCell align="center">
                                  {row.expired_date ?
                                    <div style={{wordWrap: 'break-word',color:'red'}} >
                                    {row.expired_date}/{row.expired_month}/{row.expired_year}</div>
                                    :
                                    <div>NULL</div>
                                  }
                                  </TableCell>
                                  <TableCell align="center">
                                   {row.denying_reason ?
                                     <div style={{wordWrap: 'break-word'}} >
                                      {row.denying_reason}
                                     </div>
                                     :
                                     <div>NULL</div>
                                   }
                                  </TableCell>
                                  <TableCell align="center">
                                  {row.req_hour ?
                                    <div>{row.req_hour}</div>
                                    :
                                    <div>N/A</div>
                                  }
                                  </TableCell>
                                  <TableCell align="center">
                                     {row.req_reason}
                                  </TableCell>
                                  <TableCell align="center">
                                    <StatusReq content={row} serial={row.serial} />
                                  </TableCell>
                                  <TableCell align="center">
                                     <DeleteIcon onClick={()=>this.deleteRequest(row.serial)} className="go " />
                                  </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </React.Fragment>
                      :
                      <div style={{textAlign:'center'}} >No Request Found !!</div>
                    }
                      </React.Fragment>
                }
              </React.Fragment>
      )
      }
    }

class StatusReq extends Component{
  constructor(props)
  {
    super(props)
    this.state ={
      status:'',
      viewdata:[],
      available:'no',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStatus_Of_Request()
  }


  handleViewRequest = (content,index) =>{
      this.setState({
        available:'yes',
        viewdata: content,
      })
  }

  dataSubmitWindow=()=>{
    this.setState({available:'no'})
  }

  fetchStatus_Of_Request= () =>{
    axios.post('/ework/user/fetch_requested_status',{
      serial: this.props.serial,
    }).then(res=>{
      this.setState({status: res.data})
    })
  }
  render()
  {
    let btn;
    if(this.state.status === "approved")
    {
      btn =
      <Button variant="contained" onClick={() => this.handleViewRequest(this.props.content,this.props.serial)} color="primary">Upload Data</Button>
    }
    else if(this.state.status === "denied")
    {
      btn =
      <Button variant="outlined" color="secondary">Denied</Button>
    }
    else if(this.state.status === "pending")
    {
      btn =
      <Button variant="outlined" color="secondary">Pending</Button>
    }

    return(
      <React.Fragment>
      <div>{btn}</div>
      <Grid container spacing={1}>
       <Content dataSubmitWindow={this.dataSubmitWindow}
        available={this.state.available} view={this.state.viewdata}/>
     </Grid>
      </React.Fragment>
    )
  }
}


/*-----------------------------------------------This class is for dynamically allocating div ------------- */

class Content extends Component{
  constructor(props){
    super(props);
    this.state={
      username: props.view.username,
      day_order: props.view.day_order,
      date:props.view.date,
      month:props.view.month,
      year:props.view.year,
      hour:props.view.hour,
      redirectTo:'',
  }
  }

  render(){
    var content={
      number:this.props.view.req_hour,
    }
    let output;
    if(this.props.available === "yes")
    {
      output =
      <React.Fragment>
        <Dialog fullScreen open={true} onClose={this.props.dataSubmitWindow}   TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
              <IconButton edge="start" color="inherit" onClick={this.props.dataSubmitWindow} aria-label="close">
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
          <List>
          <div style={{padding:'30px'}}>
             <Grid container spacing={1}>
                {!(this.props.view.req_hour) ?
                  <React.Fragment>
                     <ColorRep username={this.props.view.username} opendir="r_class"
                      date={this.props.view.req_date} month={this.props.view.req_month}
                      content={content} year={this.props.view.req_year}
                      day_order={this.props.view.day_order}/>
                  </React.Fragment>
                :
                <React.Fragment>
                  <Grid container spacing={1}>
                     <OtherSource view={this.props.view} closeref={this.props.dataSubmitWindow} />
                  </Grid>
                <Grid container spacing={1}>
                   <span className="red-text">REMEMBER :</span><br />
                   <p>There is no chance to edit the datas,once you submit. So it's a one time
                   process. Kindly fill and submit the details correctly.</p>
                </Grid>
                </React.Fragment>
                 }
             </Grid>
          </div>
          </List>
        </Dialog>
      </React.Fragment>
    }
    else
    {
      output=
      <div></div>
    }
          return(
            <div>
            {output}
            </div>
          );
  }
}


class OtherSource extends Component {
  constructor()
  {
    super()
    this.state={
      dataa_a:'',
      allo1:false,
      modal:true,
      alreadyhave:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    	axios.post('/ework/user/fetchAllotments', {
    		day_order: this.props.view.day_order,hour:this.props.view.req_hour,
    	})
    	.then(response =>{
    		if(response.data)
    		{
          let allotdata = (response.data.alloted_slots).toUpperCase();
    				this.setState({
    					allo1:true,
              datas_a:response.data,
              alreadyhave: allotdata,
    				})
    		}
    	})
  }

  closeModal = (info) => {
  this.setState({modal: !this.state.modal})
}

  render() {
    var content={
      number:this.props.view.req_hour,
    }
    if(this.state.allo1)
    {
      return(
        <React.Fragment>
        <Allot
        slot={this.state.alreadyhave}
        datas_a={this.state.datas_a}
        date ={this.props.view.req_date}
        month ={this.props.view.req_month}
        year={this.props.view.req_year}
        username={this.props.view.username}
        day_order={this.props.view.day_order}
        closeModal={this.closeModal}
        content={content}
        foreign_ref="true"
        closeref={this.props.closeref}
        />
        </React.Fragment>
      )
    }
    else{
      return(
        <React.Fragment>
        <Free
        slot={this.state.data_in}
        day ={this.props.view.req_day}
        month ={this.props.view.req_month}
        year={this.props.view.req_year}
        day_sl={"."+this.props.view.req_hour}
        usern={this.props.view.username }
        day_order={this.props.view.day_order}
        closeModal={this.props.closeModal}
        day_slot_time ={'.'+this.props.view.req_hour}/>
        />
        </React.Fragment>
      )
    }
  }
}
