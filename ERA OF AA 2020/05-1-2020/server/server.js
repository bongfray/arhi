const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const session = require('express-session')
const mongoose = require('mongoose')
const dbConnection = require('./database')
const MongoStore = require('connect-mongo')(session)
const passport = require('./passport/fac_pass');

const app = express()
const PORT = 8080
// Route requires
const user = require('./routes/user')


var http = require('http')
var servem = http.createServer(app)

var SocketServer = require("ws").Server
const wss = new SocketServer({ server: servem });






// app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));
// MIDDLEWARE
app.use(morgan('dev'))
app.use(
	bodyParser.urlencoded({
		extended: true
	})
)
app.use(bodyParser.json())

// Sessions
app.use(
	session({
		secret: 'fraggle-rock',
		store: new MongoStore({ mongooseConnection: dbConnection}),
		resave: false,
		saveUninitialized: false,
		maxAge: Date.now() + (86400020)
	})
)




// Passport
app.use(passport.initialize())
app.use(passport.session()) // calls the deserializeUser


// Routes
app.use('/user', user)



let clientCount = 0;
wss.on('connection', (ws) => {
  console.log('Client connected');
  clientCount++;

  console.log(clientCount);

  ws.on('message', (message) => {
    console.log('Message from client', message);

    let parsedMessage = JSON.parse(message);

    // parsedMessage.id = uuid();
    console.log(message);

    wss.clients.forEach(function each(client) {
      if (client.readyState === ws.OPEN) {
        client.send(JSON.stringify(parsedMessage));
      }
    });
  });

  ws.on('close', () => {
    console.log('Client disconnected');
    clientCount--;
  });

});


// Starting Server
const serve = app.listen(PORT, () => console.log('App listening on port 8000!'));

process.on('SIGTERM', () => {
  console.info('SIGTERM signal received.');
  console.log('Closing http serve.');
  serve.close(() => {
    console.log('Http server closed.');
    // boolean means [force], see in mongoose doc
    mongoose.connection.close(false, () => {
      console.log('MongoDb connection closed.');
      process.exit(0);
    });
  });
});
