import React from 'react';
import axios from 'axios';
import Load from '../loader_simple'
import PropTypes from "prop-types";
import ValidateUser from '../validateUser'



export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
      loading:true,
      value_for_search:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({modal: !this.state.modal,disabled:'disabled',id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
 if((index === "Skill-For-Student")){
   delroute = "/user2/del_inserted_data";
  }
  else{
    delroute = "/user2/del_inserted_data";
   }

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:'',
        success:'none',
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
 if((action === "Skill-For-Student"))
  {
    fetchroute = "/user2/fetch_in_admin";
  }
  else
  {
    fetchroute = "/user2/fetch_in_admin";
  }

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
  })
  .then(response =>{
    this.setState({
    loading:false,
     products: response.data,
   })
  })
}

searchEngine =(e)=>{
  this.setState({value_for_search:e.target.value})
}

  render() {
        const {products} = this.state;

    var libraries = products,
    searchString = this.state.value_for_search.trim().toLowerCase();
    if(searchString.length > 0)
    {
      if(this.props.action === "Skill-For-Student")
      {
        libraries = libraries.filter(function(i) {
          return i.skill_name.toLowerCase().match( searchString );
        });
      }
      else if(this.props.action === "Domain-For-Student"){
        libraries = libraries.filter(function(i) {
          return i.domain_name.toLowerCase().match( searchString );
        });
      }

    }

    if(this.state.loading)
    {
      return(
        <div>
         <Load />
        </div>
      );
    }
    else{
      return(
        <div>
          <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
            {((this.props.action==="Skill-For-Student") || (this.props.action==="Domain-For-Student")) ?
            <div>
                <div className="search-wrapper">
                  <div className="row">
                     <div className="col l11">
                        <input id="search" style={{borderTop:'1px black'}} placeholder="Search" value={this.state.value_for_search} onChange={this.searchEngine}/>
                     </div>
                     <div className="col l1">
                        <i className="material-icons">search</i>
                     </div>
                  </div>
                </div>
            </div>
            :
            <div className="row">
                <div className="col s1 l1 m1 xl1 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className={content.div} key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s2 l2 xl2 m2 center"><b>Action</b></div>
            </div>
          }
            <hr />
            <PageInit username={this.props.username} noChange={this.props.noChange} libraries={libraries} action={this.props.action} disabled={this.state.disabled} data={this.props.data} editProduct={this.props.editProduct} deleteProduct={this.deleteProduct}/>
              </div>
      )
    }

  }
}


class PageInit extends React.Component {
  render() {
    return (
      <Pagination
        data={this.props.libraries}
        action={this.props.action}
        incoming={this.props.data}
        noChange={this.props.noChange}
          username={this.props.username}
      >
        <Example
        action={this.props.action}
        noChange={this.props.noChange}
        data={this.props.libraries}
        incoming={this.props.data}
        editProduct={this.props.editProduct}
        deleteProduct={this.props.deleteProduct}
        username={this.props.username}
         />
      </Pagination>
    );
  }
}


class Pagination extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentPage: null,
      pageCount: null
    }
  }

  componentDidMount() {
    const startingPage = this.props.startingPage
      ? this.props.startingPage
      : 1;
    const data = this.props.data;
    const pageSize = this.props.pageSize;
    let pageCount = parseInt(data.length / pageSize);
    if (data.length % pageSize > 0) {
      pageCount++;
    }
    this.setState({
      currentPage: startingPage,
      pageCount: pageCount
    });
  }

  setCurrentPage(num) {
    this.setState({currentPage: num});
  }

  createControls() {
    let controls = [];
    const pageCount = this.state.pageCount;
    for (let i = 1; i <= pageCount; i++) {
      const baseClassName = 'pagination-controls__button';
      const activeClassName = i === this.state.currentPage ? 'pagination-controls__button active white-text' : '';
      controls.push(
        <li key={i}
          className={`${baseClassName} ${activeClassName}`}
          onClick={() => this.setCurrentPage(i)}
        >
          {i}
        </li>
      );
    }
    return controls;
  }

  createPaginatedData() {
    const data = this.props.data;
    const pageSize = this.props.pageSize;
    const currentPage = this.state.currentPage;
    const upperLimit = currentPage * pageSize;
    const dataSlice = data.slice((upperLimit - pageSize), upperLimit);
    return dataSlice;
  }

  render() {
    return (
      <div className=''>
        <div className='pagination-results'>
          {React.cloneElement(this.props.children, {data: this.createPaginatedData()})}
        </div>
        <ul className='pagination'>
          {this.createControls()}
        </ul>
      </div>
    );
  }
}

Pagination.propTypes = {
  data: PropTypes.array.isRequired,
  pageSize: PropTypes.number.isRequired,
  startingPage:PropTypes.number.isRequired
};

Pagination.defaultProps = {
  pageSize: 12,
  startingPage: 1
};

class Example extends React.Component {
  constructor(){
    super();
    this.state={
      richClass:'',
    }
  }
  render() {

    const data = this.props.data;
    return (
      <div className='row'>

                  {((this.props.action==="Skill-For-Student") || (this.props.action==="Domain-For-Student")) ?
                   <React.Fragment>
                      <div className="row">
                        {data.map((product,index) => (
                          <React.Fragment key={product.serial}>
                          {this.props.noChange === 'have'  ?
                             <React.Fragment>
                            {(product.inserted_by === this.props.username) ?
                             <div className="col l2 xl2 m3 s4 card" style={{padding:'10px'}}>
                                 <div className="row">
                                     <div className="col l8 xl8 m8 s6 center">
                                       {this.props.incoming.fielddata.map((content,index)=>(
                                         <div key={index} style={{whiteSpace: 'pre-line'}}>{product[content.name]}</div>
                                       ))}
                                     </div>
                                    <div className="col s6 l4 xl4 m4 center">
                                     <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                                     &nbsp;<i className="material-icons go" disabled={this.props.disabled} onClick={() => this.props.deleteProduct(product.serial,this.props.action)}>delete</i>
                                     </div>
                                 </div>
                             </div>
                             :
                             <div className="col l2 xl2 m3 s4 card" style={{padding:'10px'}}>
                                 <div className="row">
                                     <div className="col l12 xl12 m12 s12 center">
                                       {this.props.incoming.fielddata.map((content,index)=>(
                                         <div key={index} style={{whiteSpace: 'pre-line'}}>{product[content.name]}</div>
                                       ))}
                                     </div>
                                 </div>
                             </div>

                              }
                          </React.Fragment>
                              :
                              <React.Fragment>
                              <div className="col l2 xl2 m3 s4 card" style={{padding:'10px'}}>
                                  <div className="row">
                                      <div className="col l8 xl8 m8 s6 center">
                                        {this.props.incoming.fielddata.map((content,index)=>(
                                          <div key={index} style={{whiteSpace: 'pre-line'}}>{product[content.name]}</div>
                                        ))}
                                      </div>
                                     <div className="col s6 l4 xl4 m4 center">
                                      <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                                      &nbsp;<i className="material-icons go" disabled={this.props.disabled} onClick={() => this.props.deleteProduct(product.serial,this.props.action)}>delete</i>
                                      </div>
                                  </div>
                              </div>
                              </React.Fragment>
                              }
                             </React.Fragment>
                          ))}
                      </div>
                      </React.Fragment>
                      :
                       <React.Fragment>
                       {data.map((product,index) => (
                         <React.Fragment key={index}>
                         <div className="row">
                          <div className="col l1 xl1 m1 s1 center">{index+1}</div>
                          {this.props.incoming.fielddata.map((content,index)=>(
                            <div className={content.div} key={index} style={{whiteSpace: 'pre-line'}}>{product[content.name]}</div>
                          ))}
                            {!(this.props.noChange ==="have")&&
                              <div className="col s2 l2 xl2 m2 center">
                              <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                              &nbsp;<i className="material-icons go" disabled={this.props.disabled} onClick={() => this.props.deleteProduct(product.serial,this.props.action)}>delete</i>
                              </div>
                            }
                        </div>
                        <hr />
                        </React.Fragment>
                    ))}
                    </React.Fragment>
                    }
      </div>
    );
  }
}
