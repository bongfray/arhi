import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'

var empty = require('is-empty');

export default class Allot extends React.Component {
  constructor() {
    super()
    this.state = {
       freeparts:'Curriculum',
        sending:'SUBMIT',
        selected:'',
        covered_topic:'',
        problem_statement:'',
        compday:'',
        comphour:'',
        allot:'',
        count: 0,
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        date:'',
        year:'',
        month:'',
    }
  }


  handleChecked =(e) =>{
    this.setState({
      selected: e.target.value,
    })
  }

  updateAllotV =(userObject) => {
    this.setState(userObject)
  }


  closeModal=()=>{
    this.props.closeModal();
  }
  colorChange=()=>{
    this.props.color()
  }

handleAlloted =(e) =>{
  let freeparts;
  e.preventDefault();
  if(this.state.selected === "Problem")
  {
    freeparts = '';
  }
  else{
    freeparts = "Curriculum";
  }


  if(empty(this.state.selected))
  {
    window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
  }
  else
  {
    if(this.state.compday)
    {
      var day = Date.parse("today").toString("dd");
      if(empty(this.state.compday) && empty(this.state.comphour))
      {
        window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else if( !((this.state.compday).length<=2) && !((this.state.comphour).length<=2))
      {
        window.M.toast({html: 'Please Follow the Correct Format of Inputting..',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else if(this.state.compday === day)
      {
        window.M.toast({html: 'Please Follow the Correct Format of Inputting..',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else{
        e.preventDefault()

        Date.prototype.getWeek = function () {
            var onejan = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
        };

        var myDate = new Date.today();
        var week =myDate.getWeek();

      axios.post('/user/timeallot', {
        day_slot_time: this.props.day_order+this.props.day_slot_time,
        day_order: this.props.day_order,
        selected: this.state.selected,
        freeparts:freeparts,
        order:this.props.day_order+this.props.day_slot_time+'Alloted',
        saved_slots: this.state.saved_slots,
        problem_statement: this.state.problem_statement,
        slot:this.props.slot,
        time:this.props.time,
        compday: this.state.compday,
        comphour: this.state.comphour,
        date:this.props.day,
        month: this.props.month,
        year: this.props.year,
        week:week,
        day_sl:this.props.day_sl,
      },
      this.setState({
        sending: 'Uploading Datas',
      })
    )
        .then(response => {
          if(response.status===200){
             if(response.data.succ)
            {
              window.M.toast({html: 'Success !!', outDuration:'1200', inDuration:'1200', classes:'rounded #ec407a green lighten-1'});
              this.closeModal();
              this.colorChange();
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }
    }
    else
    {
      if((!this.state.problem_statement) && (this.state.selected === 'Problem'))
      {
            window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else{
    Date.prototype.getWeek = function () {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };

    var myDate1 = new Date.today();
    var week1 =myDate1.getWeek();

  axios.post('/user/timeallot', {
    usern: this.props.usern,
    day_slot_time: this.props.day_order+this.props.day_slot_time,
    day_order: this.props.day_order,
    selected: this.state.selected,
    order:this.props.day_order+this.props.day_slot_time+'Alloted',
    problem_statement: this.state.problem_statement,
    slot:this.props.slot,
    time:this.props.time,
    freeparts:freeparts,
    saved_slots: this.state.saved_slots,
    covered: this.state.covered_topic,
    date:this.props.day,
    month: this.props.month,
    year: this.props.year,
    week:week1,
    day_sl:this.props.day_sl,
    verified:false,
  },this.setState({
    sending: 'Uploading Datas',
  }))
    .then(response => {
      if(response.status===200){
        if(response.data.succ)
        {
          if(freeparts === 'Curriculum')
          {
            this.sendNoti_To_Student()
          }
          window.M.toast({html: 'Success !!', outDuration:'1200', inDuration:'1200', classes:'rounded #ec407a green lighten-1'});
          this.closeModal();
          this.colorChange();
        }
      }
    }).catch(error => {
      window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
    })
  }
  }
 }
}

sendNoti_To_Student=()=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();

  axios.post('/user2/send_noti_on_class_complition',{week:week,slot:this.props.slot,day_slot_time:this.props.day_order+this.props.day_slot_time,action:'Class Completed',faculty_id:this.props.usern,covered_topic:this.state.covered_topic,day:
  this.props.day,month:this.props.month,year:this.props.year})
  .then( res => {

  });
}

render(){
  // console.log(this.props.cday)
  return(
    <div>
    <span style={{color:'red'}}>Reminder: </span><span>This slot is booked at the beginig of Semester.You are not allowed to edit it.</span>
    <div className="row">
      <div className="col l6 xl6 s12 m12">
      <p>
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Completed' onChange={this.handleChecked} />
          <span style={{color:'green'}} className="center"><b>Class/Work Completed</b></span>
        </label>
     </p>
      </div>
      <div className="col l6 xl6 m12 s12">
      <p>
      {!this.props.cday &&
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Problem' onChange={this.handleChecked} />
          <span style={{color:'green'}} className="center"><b>Problem With Work/Class Completion(like Slot Cancelled)</b></span>
        </label>
      }
     </p>
      </div>
    </div>
    <DivOpen  selected={this.state.selected} updateAllotV={this.updateAllotV}/>
  <button className="waves-effect btn col l2 s4 xl2 m2 blue-grey darken-2 sup right" onClick={this.handleAlloted}>{this.state.sending}</button>
   </div>
  );
}
}





/*-------for check radio button----------------------------------- */

 class DivOpen extends Component{
   constructor(){
     super();
     this.state ={
       covered_topic:'',
       problem_statement:'',
     }
     this.componentDidMount = this.componentDidMount.bind(this);
   }
   componentDidMount(){

   }
handleAllotData= (evt) =>{
     const value = evt.target.value;
     this.setState({
       [evt.target.name]: value
     });
     this.props.updateAllotV({
       [evt.target.name]: value,
     });
   }



   render(){

     if(this.props.selected ==="Completed")
     {
       return(
         <div className="input-field">
         <input id="covered" type="text" name="covered_topic" value={this.state.covered_topic}  onChange={this.handleAllotData}/>
         <label htmlFor="covered">Enter the the things covered in this alloted slot.</label>
         </div>
       );
     }
     else if(this.props.selected === "Problem")
     {
       return(
         <React.Fragment>
         <div className="input-field">
         <input id="prob" type="text" name="problem_statement" className="validate" value={this.state.problem_statement} onChange={this.handleAllotData} required/>
           <label htmlFor="prob">Enter the reason</label>
           <span className="helper-text" data-error="You should enter the reason">Enter the reason first,after that only you will able to add some compensation details.Remeber you should type at least 20 character in reason</span>
         </div>
         <div className="">
         {this.state.problem_statement.length>20 &&
           <Switch updateAllotV={this.props.updateAllotV}/>
         }
         </div>
         </React.Fragment>
       );
     }
     else
     {
       return(
       <div></div>
     );
     }
   }
 }




  class Switch extends React.Component {

      constructor ( props ) {
          super( props );
  		this.state = {
  			isChecked: false,
  		}
      }
      statusOfCompensation= (e) =>{
        this.setState({
          isChecked: !this.state.isChecked,
        })
      }
      render () {

          return(
            <div>

              <div className="switch center">
                  <label style={{color:'red',fontSize:'15px'}}>
                      <input  checked={ this.state.isChecked } onChange={ this.statusOfCompensation} type="checkbox" />
                      <span className="lever"></span>
                      Want to compensate this on other day ?
                  </label>
              </div>
              <br />
              <InputValue datas={this.state.isChecked} updateAllotV={this.props.updateAllotV}/>
            </div>
          );
      }

  }

  class InputValue extends Component{
    constructor()
    {
      super()
      this.state ={
        compday:'',
        comphour:'',
      }
    }

    handleComData =(evt)=>{
      const value = evt.target.value;
      this.setState({
        [evt.target.name]: value
      });
      this.props.updateAllotV({
        [evt.target.name]: value,
      });
    }


    render(){
      if(this.props.datas === true)
      {
        return(
          <React.Fragment>
          <div className="row">
          <div className="col l6 s12 m12 xl6">
          <div className="input-field">
             <input id="fac1" name="compday" className="validate" type="number" onChange={this.handleComData} required/>
             <label htmlFor="fac1">Mention the Date(like : "June 1" then type 1)</label>
          </div>
          </div>
           <div className="col l6 s12 m12 xl6">
               <div className="input-field">
                  <input id="fac6" type="number" name="comphour" className="validate" onChange={this.handleComData} required/>
                  <label htmlFor="fac6">Mention the Hour</label>
               </div>
           </div>
          </div>
          <div className="left col l6">
             <span className="green-text"><b><u>REMEMBER : </u></b></span>You are not allowed to put a date beyond this month.
             In this case if today is the last date of the current month,you will miss this slot to enter data for current month.In case you are typing invalid
             date and also submitting that, system will not render that slot. One more thing, the date you are submitting,system will
             not render that hour on that day in Today's DayOrder Section. You can view that hour in Extra part on your prefered date.
          </div>
          </React.Fragment>
        );
      }
      else {
        return(
          <React.Fragment></React.Fragment>
        )

      }
    }
  }
