import React, { Component } from 'react'
import axios from 'axios'
import AddProduct from './RESUME_CONTENT/add_datas';
import ProductList from './RESUME_CONTENT/show_info'


export default class FacultyList extends Component{
 constructor(props) {
    super(props);
    this.state = {
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      action:this.props.options,
      disabled:'',

    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount()
{
 this.setState({
   isAddProduct: false,
   isEditProduct: false,
 })
}

  onCreate = (e,index) => {
    this.setState({ isAddProduct: true ,product: {}});
  }
  onFormSubmit(data) {
    let apiUrl;
    if(this.state.isEditProduct){
      apiUrl = '/user2/edit_faculty';
    } else {
      apiUrl = '/user2/add_faculty';
    }

    if(!(data.faculty_id) || !(data.subject_taking)){
      window.M.toast({html: 'Enter all the details !!', classes:'rounded #f44336 red'});
    }
    else{
    axios.post(apiUrl, {data})
        .then(response => {
          if(response.data === 'have'){
              window.M.toast({html: 'Datas are already saved !!', classes:'rounded #f44336 red'});
          }
          else if(response.data === 'done'){
              window.M.toast({html: 'Saved !!', classes:'rounded green darken-2'});
          }
          this.setState({
            response: response.data,
            isAddProduct: false,
            isEditProduct: false
          })

        })


      }

  }

  editProduct = (productId,index)=> {
    axios.post('/user2/fetcheditdata_facultylist',{
      id: productId,
      username: this.props.username,
    })
        .then(response => {
          this.setState({
            product: response.data,
            isEditProduct: true,
            isAddProduct: true,
          });
        })

 }

 updateState = () =>{
   this.setState({
     isAddProduct:false,
     isEditProduct:false,
   })
 }
  render() {
    let productForm,title,description;
    var data;
            title ='CURRENT SEMESTER FACULTY LIST';
            data = {
              Action:'FACULTY LIST',
              button_grid:'col l1 m1 s1 center',
              fielddata: [
                {
                  header: "Faculty ID",
                  name: "faculty_id",
                  placeholder: "Enter Faculty ID",
                  type: "text",
                  grid:'col l5 xl5 m5 s5 center',
                },
                {
                  header: "Subject Taking",
                  name: "subject_taking",
                  placeholder: "Enter the subject,he takes",
                  type: "text",
                  grid:'col l5 m5 s5 xl5 center',
                },
              ],
            };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct description={description} cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
 <div>
   {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
   {!this.state.isAddProduct &&
    <React.Fragment>
    <div className="row">
    <div className="col l6" />
    <div className="col l6">
      <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
     </div>
   </div>
   </React.Fragment>
   }
   { productForm }
   <br/>
 </div>

);
}
}
