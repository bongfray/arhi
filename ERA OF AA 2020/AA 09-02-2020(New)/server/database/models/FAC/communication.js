const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const communi = new Schema({
  action: {type: String , unique: false, required: false},
  official_id: {type: String , unique: false, required: false},
  complaint_subject: {type: String , unique: false, required: false},
  complaint:{ type: String, unique: false, required: false },
  message_from_admin:{ type: String, unique: false, required: false },
  fresh:{ type: Boolean, unique: false, required: false },
  mailid:{ type: String, unique: false, required: false },
})



communi.plugin(autoIncrement.plugin, { model: 'Communication', field: 'serial', startAt: 1,incrementBy: 1 });

const Communication = mongoose.model('Communication', communi)
module.exports = Communication
