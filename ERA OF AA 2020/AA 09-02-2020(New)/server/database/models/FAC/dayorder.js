const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

//Schema
const dayorder = new Schema({
  cancel_status: { type: Boolean, unique: false, required: false },
  day_order: { type: Number, unique: false, required: false },
  day_order_status: { type: String, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  time: { type: Number, unique: false, required: false },
  day: { type: Number, unique: false, required: false },
  month: { type: Number, unique: false, required: false },
  year: { type: Number, unique: false, required: false },
  updatedat:{type: Date, default: Date.now}
})


const DayOrder = mongoose.model('Common-DayOrder', dayorder)
module.exports = DayOrder
