import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import Nav from '../dynnav'
import Education from './RESUME_CONTENT/education'
import Internship from './RESUME_CONTENT/internship'
import Skill from './RESUME_CONTENT/skills'
import Personal from './RESUME_CONTENT/personal'
import Project from './RESUME_CONTENT/project'
import Position from './RESUME_CONTENT/position'
import Training from './RESUME_CONTENT/training'
import SocialAccount from './RESUME_CONTENT/social_account'
import Job from './RESUME_CONTENT/job'

export default class Resume extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      loading:true,
      username:'',
      isChecked:false,
      topic:'',
      resume_subject:[{id:1,title:'Personal Info'},{id:2,title:'Education'},{id:3,title:'Skills'},
      {id:4,title:'Internships'},{id:5,title:'Projects'},{id:6,title:'Position of Responsibility'},
      {id:7,title:'Trainings or Certificates'},{id:8,title:'Social Media Account(like Linkdin)'},{id:9,title:'Jobs or Experiences'}
      ,{id:10,title:'Additional Details'}],
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleChecked =(e,index)=>{
      this.setState({
          isChecked: !this.state.isChecked,
          topic: e.target.value
      });
  }


    componentDidMount() {
      if(this.props.prop_data)
      {
        this.setState({username:this.props.prop_data.data.username,loading:false})
      }
      else
      {
        axios.get('/ework/user2/getstudent',
      )
         .then(response =>{
           this.setState({loading: false})
           if(response.data.user)
           {
             this.setState({username: response.data.user.username})
           }
           else{
             this.setState({
               redirectTo:'/ework/student',
             });
             window.M.toast({html: 'You are not Logged In', classes:'rounded red'});
           }
         })
       }

    }
    render(){


      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return(
          <React.Fragment>
          {!(this.props.prop_data) &&
            <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          }
                  <Grid container spacing={1}>
                     <Grid item xs={2} sm={2}>
                       <div style={{textAlign: 'center',background: 'linear-gradient(to right bottom, #2f3441 50%, #212531 50%)',marginTop:'4px',fontSize: '20px',position: 'relative',width: '100%',display: 'inline-block'}}>
                         <div style={{color:'white',height: '65px',position: 'relative',paddingTop: '1em'}}>RECORDS</div>
                       </div>
                       <List>
                       {this.state.resume_subject.map((content,index)=>(
                           <ListItem button key={index} style={{border:'1px solid black'}}>
                            <FormControl component="fieldset" style={{padding:'5px'}}>
                              <RadioGroup aria-label="gender" name="gender1" value={this.state.topic}  onChange={(e)=>{this.handleChecked(e,index)}}>
                                <FormControlLabel value={content.title} control={<Radio />} style={{color:'black'}} label={content.title} />
                              </RadioGroup>
                            </FormControl>
                           </ListItem>
                       ))}
                          </List>
                     </Grid>
                     <Grid item xs={10} sm={10}>
                       <ShowContent referOutside={this.props.prop_data} topic={this.state.topic} username={this.state.username}/>
                     </Grid>
                  </Grid>
          </React.Fragment>
        );
      }
    }
}

class ShowContent extends Component{
  render()
  {
    let items;
    if(this.props.topic ==="Personal Info")
    {
      items=
      <div><Personal referOutside={this.props.referOutside} username={this.props.username}/></div>
    }
    else if(this.props.topic ==="Education")
    {
      items =
      <div><Education referOutside={this.props.referOutside}  username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Skills")
    {
      items =
      <div><Skill referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Internships")
    {
      items =
      <div><Internship referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Projects")
    {
      items =
      <div><Project referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Position of Responsibility")
    {
      items =
      <div><Position referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Social Media Account(like Linkdin)")
    {
      items =
      <div><SocialAccount referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Trainings or Certificates")
    {
      items =
      <div><Training referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else if(this.props.topic ==="Jobs & Experiences")
    {
      items =
      <div><Job referOutside={this.props.referOutside} username={this.props.username} /></div>
    }
    else{
      items=
      <div></div>
    }
    return(
      <React.Fragment>
      {items}
      </React.Fragment>
    )
  }
}
