import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import Nav from '../../dynnav'

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';
import ViewStudentData from './view_single_student'
import axios from 'axios'

export default class DepartmentAdmin extends Component {
  constructor()
  {
    super()
    this.state={
      radio:[{name:'radio2',value:'Validate User'},{name:'radio1',value:'Student Under You'}],
      isChecked: false,
      choosed: '',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      login:'/ework/flogin',
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In', classes:'rounded red'});
        }
        else {
          this.setState({user:response.data.user,loader:false})
        }
      })
    }
    handleChecked =(e,index,color)=>{
        this.setState({
            isChecked: !this.state.isChecked,
            choosed: e.target.value
        });
    }


  render() {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else
    {

       if(this.state.loader)
       {
         return(
           <div className="center">Loading....</div>
         )
       }
       else{
          return(
              <React.Fragment>
              <Nav home={this.state.home} login={this.state.login} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
             <div className="row">
                <div className="col s12 m12 l2 xl2">
                  {this.state.radio.map((content,index)=>(
                      <div key={index}>
                          <div className="col l12 s12 m12 xl12 form-signup">
                          <p>
                          <label>
                          <input className="with-gap" type='radio' id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                          <span style={{color:'green'}}><b>{content.value}</b></span>
                          </label>
                          </p>
                          </div>
                    </div>
                  ))}
                  </div>
                  <div className="col s12 m12 l10 xl10">
                     <Display user={this.state.user} choosed={this.state.choosed}/>
                  </div>
             </div>

                  <div className="row">

                  </div>
              </React.Fragment>
          );
        }
}
  }
}



class Display extends Component {
  constructor() {
    super()
    this.state={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    let content;
    if(this.props.choosed === 'Student Under You')
    {
      content = <StundetList user={this.props.user} />
    }
    else if(this.props.choosed === 'Validate User')
    {
      content =<ValidateUser user={this.props.user} />
    }
    else{
      content = <div></div>
    }
    return(
      <React.Fragment>
       {content}
      </React.Fragment>
    );
  }
}


class ValidateUser extends Component{
  constructor()
  {
    super()
    this.state={
      display:'none',
      requests:[],
      recheck:[],
      notfound:'',
      isChecked:false,
      username:'',
      faculty_ad:false,
      year_set:false,
      disable_active:true,
      for_year:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchRequests()
  }
  fetchRequests = ()=>{
    axios.post('/ework/user2/fetch_request_for_faculty_adviser',{user:this.props.user})
    .then(res=>{
        this.setState({display:'block',requests:res.data})
    })
  }

  ActiveFacultyAdvisor =(e,index,username)=>
  {
    this.setState({advisor_on:true,index,year_set:true})
  }

  Active =(e,index,content)=>{
  window.M.toast({html: 'Activating...', classes:'rounded orange'});
  window.M.toast({html: 'Sending Activation Mail...', classes:'rounded orange'});
  this.setState({
        isChecked: !this.state.isChecked,index,
        username: e.target.value,
      })

      axios.post('/ework/user2/active_user',{
        content: content,advisor_on:this.state.advisor_on,for_year:this.state.for_year
      })
      .then(res=>{
            this.setState({advisor_on:false})
            window.M.toast({html: 'Activated !!', classes:'rounded green darken-2'});
            const { requests } = this.state;

                  this.setState({
                    requests: requests.filter(product => product.username !== content.username)
                 })
      })
  }
handleModal=()=>{
  this.setState({year_set:!this.state.year_set})
}
handleInput=(e)=>{
  this.setState({for_year:e.target.value,disable_active:false})
}

  render()
  {
    return(
      <React.Fragment>
      {this.state.year_set &&
        <Dialog
          open={this.state.year_set}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">In Which Year ?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
                <FormControl style={{width:'100%'}}>
                  <InputLabel id="sel_year">Year</InputLabel>
                    <Select
                      labelId="sel_year"
                      id="sel_year"
                      name="year"
                      value={this.state.year}
                      onChange={this.handleInput}
                    >
                      <MenuItem value="" disabled defaultValue>Year</MenuItem>
                      <MenuItem value="1">First Year</MenuItem>
                      <MenuItem value="2">Second Year</MenuItem>
                      <MenuItem value="3">Third Year</MenuItem>
                      <MenuItem value="4">Fourth Year</MenuItem>
                      <MenuItem value="5">Fifth Year</MenuItem>
                    </Select>
                 </FormControl>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleModal} color="primary" autoFocus>
              SUBMIT
            </Button>
          </DialogActions>
        </Dialog>
      }
        <p className="center">You Will able to see the requests only of your department and campus</p>
        <div className="row">
           <div className="col l12">
              <div className="card">
                <div className="card-title center pink white-text">Accept Request</div>
                <div className="card-content">
                  {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                  <div style={{display:this.state.display}}>
                  {this.state.requests.length === 0 ?
                  <h6 className="center">No Request Found</h6>
                  :
                  <React.Fragment>
                  <div className="row">
                    <div className="row">
                         <div className="col l1 left"><b>Index</b></div>
                          <div className="col l2 center"><b>Reg Id</b></div>
                          <div className="col l3 center"><b>Mail Id</b></div>
                          <div className="col l2 center"><b>Sem-Batch</b></div>
                          <div className="col l2 center"><b>Faculty Advisor</b></div>
                          <div className="col l2 center"><b>Action</b></div>
                    </div>
                    <hr />
                     {this.state.requests.map((content,index)=>(
                             <div className="row"  key={index}>
                              <div className="col l1 left">{index+1}</div>
                              <div className="col l2 center">{content.username}</div>
                              <div className="col l3 center">{content.mailid}</div>
                              <div className="col l2 center">{content.sem}-{content.batch}</div>
                              <div className="col l2 center">{content.faculty_adviser_id}</div>
                              <div className="col l2 center">
                                  <div className="switch">
                                    <label>
                                      <input checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content)}} type="checkbox" />
                                      <span className="lever"></span>
                                    </label>
                                  </div>
                              </div>
                            </div>
                     ))}
                   </div>
                  </React.Fragment>
                }
                </div>
                </div>
              </div>
           </div>
        </div>

      </React.Fragment>
    )
  }
}


class StundetList extends Component{
  constructor()
  {
    super()
    this.state={
      students:'',
      details_view:false,
      data:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStudentList();
  }

  fetchStudentList=()=>{
    axios.post('/ework/user2/fetch_student_list_under_faculty_adviser',{user:this.props.user})
  .then( res => {
      if(res.data)
      {
        this.setState({students:res.data})
      }
  });
  }

closeView=()=>{
  this.setState({details_view:false})
}

showDetails=(content)=>{
  this.setState({data:content,details_view:true})
}

  render()
  {
    return(
      <React.Fragment>
      {this.state.details_view  &&
          <ViewStudentData details_view={this.state.details_view}  closeView={this.closeView} data={this.state} />
      }
      <div className="row">
         <div className="col l12">
            <div className="card">
              <div className="card-title center pink white-text">Student List</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.students.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                       <div className="col l1 left"><b>Index</b></div>
                        <div className="col l2 center"><b>Reg Id</b></div>
                        <div className="col l2 center"><b>Mail Id</b></div>
                        <div className="col l2 center"><b>Year - Sem - Batch</b></div>
                        <div className="col l3 center"><b>campus - Department</b></div>
                        <div className="col l2 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.students.map((content,index)=>(
                       <React.Fragment key={index}>
                           <div className="row" >
                            <div className="col l1 left">{index+1}</div>
                            <div className="col l2 center">{content.username}</div>
                            <div className="col l2 center">{content.mailid}</div>
                            <div className="col l2 center"> {content.year} - {content.sem} - {content.batch} </div>
                            <div className="col l3 center">{content.campus} - {content.dept}</div>
                            <div className="col l2 center">
                             <Tooltip title="View Details">
                              <VisibilityIcon onClick={()=>this.showDetails(content)} />
                             </Tooltip>
                            </div>
                          </div>
                          <hr />
                        </React.Fragment>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
      </React.Fragment>
    )
  }
}
