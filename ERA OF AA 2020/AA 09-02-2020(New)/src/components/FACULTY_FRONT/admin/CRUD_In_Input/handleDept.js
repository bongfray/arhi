import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './add';
import ProductList from './prod'

 export default class InsertDept extends Component{
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }

   componentDidMount()
   {
     this.fetchlogin();
   }

   fetchlogin = () =>{
       axios.get('/ework/user/'
     )
       .then(response =>{
         if(response.data.user)
         {
           this.setState({username:response.data.user.username})
         }
         else{
           this.setState({
             redirectTo:'/ework/',
           });
           window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
         }
       })
     }

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
     addroute="/ework/user/add_from_insert_in_admin";
     editroute = "/ework/user/edit_inserted_data";
     if(!(data.department_name))
     {
       window.M.toast({html: 'Enter All the Details !!',classes:'rounded  red'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/ework/user/fetch_for_edit"


     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Department Name",
                   name: "department_name",
                   placeholder: "Enter the Name of the Department",
                   type: "text",
                   grid: 2,
                   div:"col l10 xl10 s9 m10 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <AddProduct username={this.state.username} cancel={this.updateState} action="Department" data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <div className="particular" style={{padding:'5px'}}>
  <div>
    {!this.state.isAddProduct && <ProductList action="Department" data={data1}  editProduct={this.editProduct}/>}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</div>
);
}
}
}
