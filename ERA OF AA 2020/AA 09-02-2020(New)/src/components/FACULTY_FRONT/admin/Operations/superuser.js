import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import { Select } from 'react-materialize'
import axios from 'axios'
import Validate from '../validateUser'
import HandleActivation from './SUPER_OPERATIONS/handleActivation'


export default class SuperUser extends Component{
    constructor(){
        super();
        this.state={
          showButton:'block',
          show:'none',
          display:'none',
          redirectTo:'',
          modal: false,
          isChecked:false,
          isCheckedS:false,
          history:'',
            susername:'',
            username:'',
            index_id:'',
            request:[],
            responded_request:[],
            viewdata:'',
            option:'',
            req_reject_no:'',
            req_accept_no:'',
            selectedOption:'',
            id:'',
            renderDiv:false,
            switch_value:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }


    getRequest =() =>{
      axios.get('/ework/user/getrequest')
          .then(response => {
            const reques = response.data.filter(item => item.action === "false");
            const reject = response.data.filter(item => item.action === "denied");
            const approve = response.data.filter(item => item.action === "approved");
            this.setState({display:'block',request: reques,responded_request:response.data,req_reject_no:reject.length,req_accept_no:approve.length})
          })
    }
    view_Status_of_Request = (content,index) =>{
      this.setState({
        username:content.username,
        index_id:index,
      })
      axios.post('/ework/user/approve_request',content)
          .then(response => {
            if(response.data)
            {
              this.setState({viewdata: response.data})
              this.modalrender();
            }
          })
    }
    modalrender=()=>{
      this.setState({renderDiv:!this.state.renderDiv})
    }
    openModal()
    {
        this.setState({modal: !this.state.modal})
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }
    componentDidMount()
    {
      this.openModal();
      this.getRequest();
    }


    handleOption = (e) =>{
      this.setState({option: e.target.value})
    }


    handleRequestModify =(object) =>{
      this.setState(object)
    }



    render(){
      var viewbutton;
        if(this.props.select === 'register')
        {
            viewbutton =
              <React.Fragment>
                <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                 <HandleActivation />
                </React.Fragment>
        }
        else if(this.props.select === 'signup')
        {
            viewbutton = <React.Fragment>
            <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                    <div style={{display:this.state.show}}>
                        <Select name="" value={this.state.option} onChange={this.handleOption}>
                        <option value="" disabled selected>Select Here...</option>
                        <option value="faculty">Faculty</option>
                        <option value="student">Student</option>
                        </Select>
                    </div>
                       <SignUpReq choice={this.state.option}/>
                    </React.Fragment>
        }
        else if(this.props.select === 'facprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Studnet Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'facreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Student Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === "approve_single_faculty_req")
        {
          viewbutton =
          <React.Fragment>
          <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
          {this.state.display === 'none' ? <h5 className="center">Fetching Requests....</h5> :
          <div style={{margin:'15px 8px 1px 9px',display:this.state.show}}>
          <div className="row">
          <div className="card col l4">
            <div className="head center yellow">Total No of Request Received </div>
            <div className="card-content">{this.state.responded_request.length}</div>
          </div>
          <div className="card col l4">
            <div className="head center yellow">Total No of Request Approved</div>
            <div className="card-content">{this.state.req_accept_no}</div>
          </div>
          <div className="card col l4">
            <div className="head center yellow">Total No of Request Rejected</div>
            <div className="card-content">{this.state.req_reject_no}</div>
          </div>
          </div>
          <div className="row">
            <div className="col l1 center"><b>Serial No.</b></div>
            <div className="col l1 center"><b>Day Order</b></div>
            <div className="col l2 center"><b>Official ID</b></div>
            <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
            <div className="col l3 center"><b>Reason</b></div>
            <div className="col l2 center"><b>Action</b></div>
          </div><hr />
          {this.state.request.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 center">{index+1}</div>
            <div className=" col l1 center">{content.day_order}</div>
            <div className=" col l2 center">{content.username}</div>
            <div className=" col l3 center">{content.req_day}/{content.req_month}/{content.req_year}</div>
            <div className=" col l3 center">{content.req_reason}</div>
            </div>
            <button className="btn col l2 blue-grey darken-2 sup" onClick={() => this.view_Status_of_Request(content,content.serial)}>View Status</button>
            </div><hr />
            </React.Fragment>
          ))}
          <ViewApprove showDiv={this.showDiv} handleRequestModify={this.handleRequestModify} showButton={this.state.showButton} request={this.state.request} modalrender={this.modalrender} renderDiv={this.state.renderDiv} view_details={this.state.viewdata} username={this.state.username} index_id={this.state.index_id}/>
          </div>}
          </React.Fragment>
        }
        else{
          viewbutton =
          <div></div>
        }
        return(
            <div className="center">
              {viewbutton}
              <SuspendUser select={this.props.select}/>
            </div>
        )
    }
}


class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,username,name,mailid)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })

      axios.post('/ework/user/active_user',{
        username: username,
        name:name,
        mailid:mailid,
      })
      .then(res=>{
            window.M.toast({html: 'Activated !!',outDuration:'9000', classes:'rounded green darken-2'});
            const { requests } = this.state;

                  this.setState({
                    requests: requests.filter(product => product.username !== username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_signup_request_for_admin';
    }
    else if(this.props.choice === 'student')
    {
      route = '/ework/user/fetch_student_request';
    }
    axios.get(route)
    .then(res=>{
        this.setState({display:'block',requests:res.data})
    })
  }
  render()
  {
    return(
      <React.Fragment>
            {this.props.choice &&
      <div className="row">
         <div className="col l12 xl12">
            <div className="card">
              <div className="card-title center pink white-text">Accept Request</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.requests.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                        <div className="col l2 center"><b>Name</b></div>
                        <div className="col l1 center"><b>Official Id</b></div>
                        <div className="col l2 center"><b>Mail Id</b></div>
                        <div className="col l2 center"><b>Campus</b></div>
                        <div className="col l2 center"><b>Department</b></div>
                        <div className="col l2 center"><b>Designation</b></div>
                        <div className="col l1 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.requests.map((content,index)=>(
                           <div className="row"  key={index}>
                            <div className="col l2 center">{content.name}</div>
                            <div className="col l1 center" style={{wordWrap: 'break-word'}}>{content.username}</div>
                            <div className="col l2 center">{content.mailid}</div>
                            <div className="col l2 center">{content.campus}</div>
                            <div className="col l2 center">{content.dept}</div>
                            <div className="col l2 center">{content.desgn}</div>
                            <div className="col l1 center">
                                <div className="switch">
                                  <label>
                                    <input  checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content.username,content.name,content.mailid)}} type="checkbox" />
                                    <span className="lever"></span>
                                  </label>
                                </div>
                            </div>
                          </div>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
       }
      </React.Fragment>
    ) ;
  }
}

class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.initialState ={
      expired_day:'',
      expired_month:'',
      expired_year:'',
      denying_reason:'',
      approving:false,
      denying:'',
    }
    this.state=this.initialState;
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleDeny =(id,username) =>{
    axios.post('/ework/user/denyrequest',{
      serial: id,
      username:username,
      denying_reason:this.state.denying_reason,
    }).then(res=>{
      this.props.modalrender();
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  handleApprove = (id,username) =>{
    axios.post('/ework/user/approverequest',{
      serial: id,
      username:username,
      expired_day:this.state.expired_day,
      expired_month:this.state.expired_month,
      expired_year:this.state.expired_year
    }).then(res=>{
      this.props.modalrender();
      if(res.data)
      {
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
      this.setState(this.initialState)
      }
    })
  }
  proceedToApprove = () =>{
    this.setState({approving:true,denying:false})
  }
  proceedToDeny = () =>{
    this.setState({denying:true,approving:false})
  }
  addExpire = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }


  render()
  {
    var dat;
    if(this.props.view_details ==="yes")
    {
      dat= <React.Fragment>
      <h6>Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!</h6>
      <div className="row">
      <button className="btn col l2 right red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      </div>
      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat= <React.Fragment>
      <h6>We can't able to find any data for this request !! Kindly take decison manually !!</h6>
      <div className="row">
      <div className="col l7" />
      <div className="col l5">
      <div className="row">
      <div className="col l5" />
      <button className="btn col l3 sup red" onClick={this.proceedToDeny}>DENY</button>
      <div className="col l1" />
      <button className="btn col l3 blue-grey darken-2 sup" onClick={this.proceedToApprove}>APPROVE</button>
      </div>
      </div>
      {this.state.denying===true &&
        <React.Fragment>
        <div className="row">
          <div className="col l10 input-field">
            <input type="text" id="reason" className="validate" name="denying_reason" value={this.state.denying_reason} onChange={this.addExpire} required/>
            <label htmlFor="reason">Enter the Reason</label>
          </div>
          </div>
          <button className="right btn col l3 sup red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
        </React.Fragment>
      }
      {this.state.approving=== true && <React.Fragment>
        <div className="row"><h6 className="left">Enter Expiry Date -</h6></div>
        <div className="row">
        <div className="col l4 input-field">
           <input type="number" id="day" className="validate" name="expired_day" value={this.state.expired_day} onChange={this.addExpire} required/>
           <label htmlFor="day">Day(dd)</label>
        </div>
        <div className="col l4 input-field">
           <input type="number" id="month" className="validate" name="expired_month" value={this.state.expired_month} onChange={this.addExpire} required/>
           <label htmlFor="month">Month(mm)</label>
        </div>
        <div className="col l4 input-field">
           <input type="number" id="year" className="validate" name="expired_year" value={this.state.expired_year} onChange={this.addExpire} required/>
           <label htmlFor="year">year(yyyy)</label>
        </div>
      </div>
        <button className="btn col l3 blue-grey darken-2 sup right" onClick={() => this.handleApprove(this.props.index_id,this.props.username)}>APPROVE</button>
        </React.Fragment>
    }
      </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    return(
      <React.Fragment>
      {this.props.renderDiv && <div className="cover_all" style={{display:this.props.showButton}}>
        <div className="up" style={{padding:'30px'}}>
          {dat}
          <div className="row">
             <div className="styled-btn right" onClick={this.props.modalrender}>close</div>
          </div>
        </div>
      </div>
    }
    </React.Fragment>
    )
  }
}

class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'',
            modal:false,
            disabled:'',
            allowed:false,
            suspend_faculty_username:'',
            suspend_student_username:'',
            suspend_reason_faculty:'',
            suspend_reason_student:'',
            deniedlist:[],
            current_action:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount()
    {
      this.fetch_denied_list()
    }
    componentDidUpdate =(prevProps,prevState) => {
      if (prevProps.action !== this.props.action) {
        this.fetch(this.props.action);
      }
      if (prevState.allowed !== this.state.allowed) {
        if(this.state.current_action === "remove_sespension")
        {
          this.RemoveSuspension();
        }
        else
        {
          this.Suspend();
        }
      }
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }

    fetch_denied_list =() =>{
      axios.get('/ework/user/fetch_denied_list')
      .then(res=>{
        this.setState({deniedlist: res.data})
      })
    }
    handleApprove = (id,username) =>{
      axios.post('/ework/user/approverequest',{
        serial: id,
        username:username,
      }).then(res=>{
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== id),
        })
      })
    }


    handleUsername = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    Suspend = (e)=>{
      this.setState({modal: !this.state.modal,disabled:'disabled',current_action:'suspend_user'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:''})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/ework/user/suspension_user"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/ework/user2/suspension_suser"
        }
        this.handleSuspension(route);
      }
    }
    RemoveSuspension = (e)=>{
      this.setState({modal: !this.state.modal,disabled:'disabled',current_action:'remove_sespension'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:''})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/ework/user/remove_user_suspension"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/ework/user2/remove_suser_suspension"
        }
        this.handle_Remove_Suspension(route);
      }
    }
    handleSuspension = (route) =>{
      axios.post(route,this.state)
      .then(res=>{
          window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }

    handle_Remove_Suspension = (route) =>{
      axios.post(route,this.state)
      .then(res=>{
          window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }
    render(){
      let suspend_content;
      if(this.props.select === 'suspendfac'){
            suspend_content=
                <div className="center">
                   <div className="row">
                      <div className="input-field col s12 xl6 l6 m6">
        								<input id="username" type="text" name="suspend_faculty_username" className="validate" value={this.state.suspend_faculty_username} onChange={this.handleUsername} required />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Official Id</span>
        								<label htmlFor="username">Username</label>
								      </div>
                      <div className="input-field col s12 xl6 l6 m6">
        								<input id="username" type="text" name="suspend_reason_faculty" className="validate" value={this.state.suspend_reason_faculty} onChange={this.handleUsername}/>
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Ignore if it's Removal of Suspension</span>
        								<label htmlFor="username">Reason</label>
								      </div>
                      </div>
                      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Link>
								      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5 offset-l2 offset-m2 offset-xl2 offset-s2" disabled={this.state.disabled} onClick={this.Suspend}>Suspend Faculty</Link>
                </div>
        }
        else if(this.props.select === 'suspendstu'){
            suspend_content =
                <div className="center">
                   <div className="row">
                      <div className="input-field col s12 l6 xl6 m6">
        								<input id="username" type="text" className="validate" name="suspend_student_username" value={this.state.suspend_student_username} onChange={this.handleUsername} required />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Student Registation Number Only</span>
        								<label htmlFor="username">Username</label>
								      </div>
                      <div className="input-field col s12 l6 xl6 m6">
                        <input id="username" type="text" className="validate" name="suspend_reason_student" value={this.state.suspend_reason_student} onChange={this.handleUsername} />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Ignore if it's Removal of Suspension</span>
                        <label htmlFor="username">Reason</label>
                      </div>
                      </div>
                      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Link>
								      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5 offset-l2 offset-m2 offset-xl2 offset-s2" disabled={this.state.disabled} onClick={this.Suspend}>Suspend Student</Link>
                </div>
        }
        else if(this.props.select === 'suspend_request'){
            suspend_content =
                <div style={{marginTop:'17px',marginRight:'10px'}}>
                <div className="row">
                  <div className="col l1 center"><b>Serial No.</b></div>
                  <div className="col l1 center"><b>Day Order</b></div>
                  <div className="col l2 center"><b>Official ID</b></div>
                  <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
                  <div className="col l3 center"><b>Reason</b></div>
                  <div className="col l2 center"><b>Action</b></div>
                </div><hr />
                {this.state.deniedlist.map((content,index)=>(
                  <React.Fragment key={index}>
                  <div className="row">
                  <div>
                  <div className="col l1 center"><b>{index+1}</b></div>
                  <div className="col l1 center"><b>{content.day_order}</b></div>
                  <div className="col l2 center"><b>{content.username}</b></div>
                  <div className="col l3 center"><b>{content.day}/{content.month}/{content.year}</b></div>
                  <div className="col l3 center"><b>{content.req_reason}</b></div>
                  </div>
                  <button className="btn col l2 " onClick={() => this.handleApprove(content.serial,content.username)}>Approve</button>
                  </div>
                  </React.Fragment>
                ))}
                </div>
        }
        else{
            suspend_content=
                <div></div>
        }
        return(
          <React.Fragment>
          <div style={{display:this.state.success}}>
            <Validate displayModal={this.state.modal} showDiv={this.showDiv}/>
          </div>
          <div style={{}}>
            {suspend_content}
          </div>
          </React.Fragment>
        );
    }
}
