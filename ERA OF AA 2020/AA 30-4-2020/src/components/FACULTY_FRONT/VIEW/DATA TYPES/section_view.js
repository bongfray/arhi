import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import SectionTable from './section_table'

export default class Section extends Component {
  constructor() {
    super()
    this.state={
      section_data:[],
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_section_for_view',{datas:this.props.referDatas,action:null,
      username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        section_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let sec_data;
    if(this.state.loader)
    {
      sec_data = <Backdrop  open={true} >
                <CircularProgress color="secondary" />
              </Backdrop>
    }
    else{
      sec_data =
      <React.Fragment>
           {this.state.section_data.length>0 ?
              <SectionTable username={this.props.username} admin_action={this.props.admin_action}
              section_data={this.state.section_data} />
              :
              <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
           }
         </React.Fragment>

    }
    return (
      <React.Fragment>
         {sec_data}
        </React.Fragment>
    );
  }
}
