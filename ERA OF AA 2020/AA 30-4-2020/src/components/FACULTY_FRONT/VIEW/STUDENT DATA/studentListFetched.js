
import React, {Component} from 'react';
import axios from 'axios';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import {Snackbar} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

import SkillTableShow from './userDetails-skill'
import DomainTableShow from './userDetails-Domains'


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



export default class ShowStudentList extends Component {
 constructor()
 {
   super()
   this.state={
     loading:true,
     students_skill:'',
     students_domain:'',
     details_view:false,
     data:'',
     user:'',
     f_dept:'',
     f_stream:'',
     f_year:'',
     snack_open:false,
     snack_msg:'',
     alert_type:'',
   }
   this.componentDidMount = this.componentDidMount.bind(this)
 }

 componentDidMount()
 {
   this.fetchData();
 }

 componentDidUpdate=(prevProps)=>{
   if(prevProps!== this.props)
   {
     this.fetchData();
   }
 }


 filterRequired=(object)=>
 {
   this.setState(object)
 }

 fetchData=(type)=>{
   let arr3,incoming_type;
   if(type)
   {
     incoming_type = type;
   }
   else {
     incoming_type = this.props.type;
   }
   if(incoming_type === 'skill_fetch')
   {
     axios.post('/ework/user2/fetch_student_on_a_skill',{skill:this.props.skill_selected,filter_data:this.state})
     .then( res => {
       console.log(res.data)


        arr3=  res.data.data.map((item,i)=>{
             return Object.assign({},item,res.data.users.filter(function(citem) {
               return (citem.username === item.username);
             }))
           })
         this.setState({students_skill:arr3,loading:false})
     });
   }
   else if(this.props.type === incoming_type) {
     axios.post('/ework/user2/fetch_student_on_a_domain',{domain:this.props.domain_selected,filter_data:this.state})
     .then( res => {
       arr3=  res.data.data.map((item,i)=>{
            return Object.assign({},item,res.data.users.filter(function(citem) {
              return (citem.username === item.username);
            }))
          })
        this.setState({students_domain:arr3,loading:false})
     });
   }

 }

 setDomain=(e)=>{
   if(e === null)
   {

   }
   else{
     this.setState({skill_selected:e.skill_name})
   }
 }

 closeView=()=>{
   this.setState({details_view:false})
 }

 showDetails=(content)=>{
   this.setState({data:content,details_view:true})
 }



 retriveMessage=(e)=>{
   this.setState({r_message:e.target.value})
 }
 sendMessage=()=>{
   if(!this.state.r_message)
   {
     this.setState({
       snack_open:true,
       snack_msg:'Enter the message !!',
       alert_type:'warning',
     })
   }
   else {
     axios.post('/user/user2/sendMessage_From_User',{message:this.state.r_message})
     .then( res => {
         if(res.data)
         {
           this.setState({
             snack_open:true,
             snack_msg:'Sent !!',
             alert_type:'warning',
           })
           this.mWindow();
         }
     });
   }
 }

 render() {
   if(this.state.loading)
   {
     return(
       <Backdrop  open={true} >
         <CircularProgress style={{color:'#ffff00'}} />
       </Backdrop>
     )
   }
   else {
   return (
     <React.Fragment>

     <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
     open={this.state.snack_open} autoHideDuration={2000}
     onClose={()=>this.setState({snack_open:false})}>
       <Alert onClose={()=>this.setState({snack_open:false})}
       severity={this.state.alert_type}>
         {this.state.snack_msg}
       </Alert>
     </Snackbar>


      {this.state.students_skill ?
        <React.Fragment>
          {this.state.loading === false && <SkillTableShow fetchMethod={this.fetchData} filterRequired={this.filterRequired}
            user_session={this.props.user_session} data={this.state} skill={this.state.students_skill} />}
       </React.Fragment>
       :
       <React.Fragment>
         {this.state.loading === false && <DomainTableShow fetchMethod={this.fetchData} filterRequired={this.filterRequired}
            user_session={this.props.user_session} data={this.state} domain={this.state.students_domain} />}
       </React.Fragment>
      }
     </React.Fragment>
   );
  }
 }
}
