import React, { Component } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import ShowStudentList from './studentListFetched'

export default class StudentData extends Component {
  constructor()
  {
    super()
    this.state={
      skill_selected:'',
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
    .then( response => {
      //console.logresponse.data)
        if(response){
          this.setState({skills:response.data,loading:false})
        }
    });
  }

  setDomain=(e)=>{
    if(e === null)
    {

    }
    else{
      this.setState({skill_selected:e.skill_name})
    }
  }

  render() {


    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress style={{color:'#ffff00'}} />
        </Backdrop>
      )
    }
    else {
      const options = this.state.skills.map(option => {
        const firstLetter = option.skill_name[0].toUpperCase();
        return {
          firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
          ...option,
        };
      });
    return (
      <React.Fragment>
        <Grid container spacing={1}>
           <Grid item xs={1} sm={4}/>
           <Grid item xs={10} sm={4} >
                  <Autocomplete
                    id="grouped-demo"
                    options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                    groupBy={option => option.firstLetter}
                    getOptionLabel={option => option.skill_name}
                    onChange={(event, value) => this.setDomain(value)}
                    style={{ width: '100%' }}
                    renderInput={params => (
                      <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
                    )}
                  />
           </Grid>
           <Grid item xs={1} sm={4}/>
        </Grid>
        <br />
        {this.state.skill_selected &&
           <ShowStudentList user_session={this.props.user_session} type="skill_fetch" skill_selected={this.state.skill_selected} />
        }
      </React.Fragment>
    );
   }
  }
}
