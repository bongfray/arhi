import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import {Button,TextField,DialogTitle,DialogContentText} from '@material-ui/core';

export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      datafrom:'',
      expired: false,
      completed: false,
      expire_year:'',
      value:'',
      username: props.username,
      serial:'',
      action:'',
      content:'',
      verified:false,
      date:0,
      month:0,
      year:0,
      verification_link:''
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const value = event.target.value;

    this.setState({
      [event.target.name]: value,
    })

  }


componentDidMount(){
  var date = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  var num = parseInt(year);
  this.setState({
    action:this.props.data.index,
    username: this.props.username,
    expire_year: num+5,
    expired:false,
    completed:false,
    datafrom:'Five',
    verified:false,
    date:date,
    month:month,
    year:year,
  })
}



  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
      <Dialog
        open={true}
        fullWidth
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Give Details of Plan"}</DialogTitle>
        <DialogContent>
            <TextField
              id="outlined-multiline-static"
              label="Give Brief About your Plan"
              multiline
              rows="2"
              fullWidth
              name="content"
              value={this.state.content}
              onChange={e => this.handleD(e,this.props.data.index)}
              variant="filled"
            />
            <br /><br />
            <TextField
              id="outlined-multiline-static1"
              label="Give a link of your work"
              multiline
              rows="1"
              fullWidth
              name="verification_link"
              value={this.state.verification_link}
              onChange={e => this.handleD(e,this.props.data.index)}
              variant="filled"
            />

        </DialogContent>
        <DialogActions>
        <Button onClick={this.props.cancel} color="secondary">
          Close
        </Button>
          <Button onClick={this.handleSubmit} color="primary">
            UPLOAD
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
