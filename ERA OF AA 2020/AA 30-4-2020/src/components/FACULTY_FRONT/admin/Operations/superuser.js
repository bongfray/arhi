import React, { Component } from 'react'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import {FormControl,Grid} from '@material-ui/core';
import Select from '@material-ui/core/Select';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Menu from '@material-ui/core/Menu';

import Review from '../../EXTRA/review'
import Validate from '../validateUser'
import HandleActivation from './SUPER_OPERATIONS/handleActivation'
import SignUpReq from './SUPER_OPERATIONS/handleSignup'
import BluePrintHandle from './SUPER_OPERATIONS/handleRerender_bluePrint';
import ListOfRequest from './SUPER_OPERATIONS/requestsList';
import SusupendUser from './SUPER_OPERATIONS/userSuspend';
import SusupendedRequest from './SUPER_OPERATIONS/suspendedRequestView';


export default class SuperUser extends Component{
    constructor(){
        super();
        this.state={
          display:'none',
            index_id:'',
            request:[],
            responded_request:[],
            viewdata:'',
            option:'',
            req_reject_no:'',
            req_accept_no:'',
            success:'block',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidUpdate=(prevProps)=>{
      if(prevProps.select !==this.props.select)
      {
        this.setState({success:'block'})
      }
    }
    getRequest =() =>{
      axios.get('/ework/user/getrequest')
          .then(response => {
            const reques = response.data.filter(item => item.action === "false");
            const reject = response.data.filter(item => item.action === "denied");
            const approve = response.data.filter(item => item.action === "approved");
            this.setState({display:'block',request: reques,responded_request:response.data,req_reject_no:reject.length,req_accept_no:approve.length})
          })
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }
    componentDidMount()
    {
      this.getRequest();
    }


    handleOption = (e) =>{
      this.setState({option: e.target.value})
    }


    render(){
      var viewbutton;
        if(this.props.select === 'register')
        {
            viewbutton =
              <React.Fragment>
                {this.state.success === 'block' ?
                  <Validate showDiv={this.showDiv}/>
                   :
                   <HandleActivation />
                }
                </React.Fragment>
        }
        else if(this.props.select === 'signup')
        {
            viewbutton =
                    <React.Fragment>
                    {this.state.success === 'block' ?
                      <Validate showDiv={this.showDiv}/>
                      :
                      <React.Fragment>
                          <Grid container spacing={1} style={{margin:'0px 0px 0px 20px'}}>
                            <Grid item xs={6} sm={3}>
                              <FormControl style={{width:'100%'}}>
                                 <InputLabel id="option">Select Here</InputLabel>
                                 <Select
                                   labelId="option"
                                   id="option"
                                   value={this.state.option} onChange={this.handleOption}
                                 >
                                 <MenuItem value="faculty">Faculty</MenuItem>
                                 <MenuItem value="student">Student</MenuItem>
                                 </Select>
                               </FormControl>
                             </Grid>
                             <Grid item xs={3} sm={5}/>
                             <Grid item xs={3} sm={4}>
                                <FilterMe />
                             </Grid>
                           </Grid>
                           {this.state.option &&
                             <SignUpReq choice={this.state.option}/>
                           }

                      </React.Fragment>
                     }
                    </React.Fragment>
        }
        else if(this.props.select === 'blue_print')
        {
          viewbutton =
                  <React.Fragment>
                  {this.state.success === 'block' ?
                    <Validate showDiv={this.showDiv}/>
                    :
                      <React.Fragment>
                      <Grid container spacing={1} style={{margin:'0px 0px 0px 20px'}}>
                        <Grid item xs={6} sm={3}>
                          <FormControl style={{width:'100%'}}>
                             <InputLabel id="option">Select Here</InputLabel>
                             <Select
                               labelId="option"
                               id="option"
                               value={this.state.option} onChange={this.handleOption}
                             >
                             <MenuItem value="faculty">Faculty</MenuItem>
                             <MenuItem value="student">Student</MenuItem>
                             </Select>
                           </FormControl>
                         </Grid>
                         <Grid item xs={3} sm={5}/>
                         <Grid item xs={3} sm={4}>
                            <FilterMe />
                         </Grid>
                       </Grid>
                       {this.state.option &&
                         <BluePrintHandle choice={this.state.option}/>
                       }
                      </React.Fragment>
                  }
                </React.Fragment>
        }
        else if(this.props.select === 'review_projects')
        {
          viewbutton =
                  <React.Fragment>
                  {this.state.success === 'block' ?
                    <Validate showDiv={this.showDiv}/>
                    :
                    <Review user={this.props.user}  admin_action={true} />
                  }
                  </React.Fragment>
        }
        else if(this.props.select === "approve_single_faculty_req")
        {
          viewbutton =
            <React.Fragment>
                {this.state.success === 'block' ?
                  <Validate showDiv={this.showDiv}/>
                  :
                  <ListOfRequest />
                }
          </React.Fragment>
        }
        else if(this.props.select === 'suspendfac' || this.props.select === 'suspendstu'){
              viewbutton=
              <React.Fragment>
              {this.state.success === 'block' ?
                <Validate showDiv={this.showDiv}/>
                :
                 <SusupendUser select={this.props.select} />
               }
               </React.Fragment>
          }
          else if(this.props.select === 'suspend_request'){
              viewbutton =
              <React.Fragment>
              {this.state.success === 'block' ?
                <Validate showDiv={this.showDiv}/>
                :
              <SusupendedRequest select={this.props.select} />
              }
            </React.Fragment>
          }
        else{
          viewbutton =
          <div></div>
        }
        return(
            <React.Fragment>
              {viewbutton}
            </React.Fragment>
        )
    }
}


class FilterMe extends React.Component{
  constructor()
  {
    super()
    this.state={
      anchorEl:null,
      isExpanded:false
    }
  }

  handleExpand =(e) => {
    this.setState({isExpanded:!this.state.isExpanded,anchorEl:e.currentTarget});
  };
  render()
  {
    return(
      <div style={{float:'right'}}>
        <IconButton aria-label="settings"
        onClick={this.handleExpand}>
          <MoreVertIcon />
          {this.state.isExpanded &&
            <Menu
              anchorEl={this.state.anchorEl}
              anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
              keepMounted
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              open={this.state.isExpanded}
              onClose={this.handleExpand}
            >
              <MenuItem onClick={() => this.clearOne()}>New</MenuItem>
              <MenuItem onClick={() => this.clearOne()}>Accepted</MenuItem>
              <MenuItem onClick={() => this.clearOne()}>Denied</MenuItem>
            </Menu>
          }
        </IconButton>
      </div>
    )
  }
}
