import React, { Component } from 'react'
import axios from 'axios';
import {Switch,Typography,CircularProgress} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,username,name,mailid)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })
      this.setState({
        snack_open:true,
        snack_msg:'Sending Activation Mail...',
        alert_type:'info',
      });
      axios.post('/ework/user/active_user',{
        username: username,
        name:name,
        mailid:mailid,
      })
      .then(res=>{
        this.setState({
          snack_open:true,
          snack_msg:'Activated !!',
          alert_type:'success',
        });
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_signup_request_for_admin';
      axios.get(route)
      .then(res=>{
          this.setState({display:'block',requests:res.data})
      })
    }
    else if(this.props.choice === 'student')
    {
      axios.post('/ework/user2/fetch_signup_request_for_admin',{user:''})
      .then(res=>{
          this.setState({display:'block',requests:res.data})
      })
    }

  }
  render()
  {
        if(this.state.display === 'none')
        {
          return(
            <React.Fragment>
              <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
                <CircularProgress color="secondary" />
              </div>
            </React.Fragment>
          )
        }
        else{
        return(
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          {this.props.choice &&
              <div style={{marginTop:'30px'}}>
              {this.state.requests.length===0 ?
                <React.Fragment>
                  <Typography variant="h5" align="center">No Request Found !!</Typography>
                </React.Fragment>
                :
              <TableContainer component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">Name</TableCell>
                      <TableCell align="center">Official Id</TableCell>
                      <TableCell align="center">Mail Id</TableCell>
                      <TableCell align="center">Campus</TableCell>
                      <TableCell align="center">Department</TableCell>
                      <TableCell align="center">Designation</TableCell>
                      <TableCell align="center">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.requests.map((row,index) => (
                      <TableRow key={index}>
                        <TableCell align="center">
                          {row.name}
                        </TableCell>
                        <TableCell align="center">{row.username}</TableCell>
                        <TableCell align="center">{row.mailid}</TableCell>
                        <TableCell align="center">{row.campus}</TableCell>
                        <TableCell align="center">{row.dept}</TableCell>
                        <TableCell align="center">{row.desgn}</TableCell>
                        <TableCell align="center">
                          <Switch
                             checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,row.username,row.name,row.mailid)}}
                             inputProps={{ 'aria-label': 'secondary checkbox' }}
                             />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
                </TableContainer>
              }
            </div>
           }
          </React.Fragment>
        ) ;
       }
  }
}
