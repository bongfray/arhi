import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {Switch,Typography,CircularProgress} from '@material-ui/core';
import {InputBase,Divider} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

import TimeTable from '../../../Schedule/Timetable'
import TimeTableS from '../../../../STUDENT_FRONT/Timetable/slotdetails'
import VisibilityIcon from '@material-ui/icons/Visibility';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
      enable_view:false,
      content:'',
      req_stud:false,
      loader:true,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
      value_for_search:'',
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq(this.props.student)
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,content)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })
      this.setState({
        snack_open:true,
        snack_msg:'Sending Activation Mail...',
        alert_type:'info',
      });
      let active_route;
      if(this.props.choice === 'student')
      {
        active_route= '/ework/user2/allow_blueprint_req';
      }
      else {
        active_route ='/ework/user/allow_blueprint_req';
      }
      axios.post(active_route,{content:content
      })
      .then(res=>{
              this.setState({
                snack_open:true,
                snack_msg:'Activated !!',
                alert_type:'success',
              });
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== content.username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_req_on_blueprint_render';
    }
    else if(this.props.choice === 'student')
    {
      route = '/ework/user2/fetch_req_on_blueprint_render';
    }
    axios.post(route)
    .then(res=>
      {
        if(this.props.student)
        {
          var list = res.data.filter(item=>(item.faculty_adviser ===this.props.user.username))
          this.setState({display:'block',requests:list})
        }
        else
        {
            this.setState({display:'block',requests:res.data})
        }
        this.setState({loading:false})
    })
  }
handleView=()=>{
  this.setState({enable_view:!this.state.enable_view})
}

showCurrentStatus=(content)=>{
  this.setState({content:content,enable_view:true,})
}

searchEngine =(e)=>{
  this.setState({value_for_search:e.target.value})
}

  render()
  {
    if(this.state.display === 'none')
    {
      return(
        <React.Fragment>
          <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
            <CircularProgress color="secondary" />
          </div>
        </React.Fragment>
      )
    }
    else
    {
      if(this.state.requests.length === 0)
      {
        return(
          <Typography variant="h5" color="secondary" align="center">No Request Found !!</Typography>
        )
      }
      else
      {
    let student_on;
   if(this.props.choice === 'student')
   {
     student_on = true;
   }
   else {
     student_on = false;
   }

   var libraries = this.state.requests,
   searchString = this.state.value_for_search.trim().toLowerCase();
   libraries = libraries.filter(function(i) {
     return i.username.toLowerCase().match( searchString );
   });

    return(
      <React.Fragment>
      {this.state.enable_view &&
        <Dialog fullScreen open={this.state.enable_view} TransitionComponent={Transition}>
            <AppBar>
              <Toolbar>
               <Grid style={{marginTop:'55'}}container spacing={1}>
                 <Grid item xs={1} sm={1}>
                    <IconButton edge="start" color="inherit" onClick={this.handleView} aria-label="close">
                      <CloseIcon />
                    </IconButton>
                 </Grid>
                 <Grid item xs={10} sm={10}>
                   <div className="center" style={{fontSize:'20px'}}>Showing Details of <span className="yellow-text">{this.state.content.username}</span></div>
                 </Grid>
                 <Grid item xs={1} sm={1}/>
               </Grid>
              </Toolbar>
            </AppBar>
            <List>
              <div style={{marginTop:'70px'}}>
               {student_on ?
                  <TimeTableS props_to_refer={this.state.content} />
                 :
                 <TimeTable props_to_refer={this.state.content} />
               }
              </div>
            </List>
        </Dialog>
      }
      {this.props.choice &&
        <React.Fragment>

        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        open={this.state.snack_open} autoHideDuration={2000}
        onClose={()=>this.setState({snack_open:false})}>
          <Alert onClose={()=>this.setState({snack_open:false})}
          severity={this.state.alert_type}>
            {this.state.snack_msg}
          </Alert>
        </Snackbar>


        <div style={{padding:'30px'}}>
         <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
       </div>
       <br />
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center"><b>Official ID</b></TableCell>
                <TableCell align="center"><b>Reason</b></TableCell>
                <TableCell align="center"><b>View Status</b></TableCell>
                <TableCell align="center"><b>Action</b></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {libraries.map((row,index)=> (
                <TableRow key={index}>
                  <TableCell align="center">{row.username}</TableCell>
                  <TableCell align="center">{row.req_reason}</TableCell>
                  <TableCell align="center">
                     <VisibilityIcon onClick={()=>this.showCurrentStatus(row)} />
                  </TableCell>
                  <TableCell align="center">
                      <Switch
                         checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,row)}}
                         inputProps={{ 'aria-label': 'secondary checkbox' }}
                         />
                  </TableCell>

                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        </React.Fragment>
       }
      </React.Fragment>
    ) ;
   }
 }
}
  }


  const useStyles = makeStyles(theme => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  }));

   function CustomizedInputBase(props) {
    const classes = useStyles();
    return (
      <Paper style={{width:'100%'}} component="form" className={classes.root}>
        <InputBase
          className={classes.input}
          placeholder="Search Only Official Id" value={props.value_for_search} onChange={props.searchEngine}
          inputProps={{ 'aria-label': 'search' }}
        />
        <Divider className={classes.divider} orientation="vertical" />
        <IconButton className={classes.iconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Paper>
    );
  }
