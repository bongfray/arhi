import React, { Component } from 'react'
import axios from 'axios'
import AddProduct from './add_datas';
import ProductList from './show_info'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:'',

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user2/editt';
     } else {
       apiUrl = '/ework/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }
   render() {
     //console.log(this.state)
     let productForm,title,description;
     var data;
             title ='Trainings or Certificates';
             data = {
               Action:'Trainings or Certificates',
               button_grid:'col l1 m1 s1 center',
               fielddata: [
                 {
                   header: "Program or Course Name",
                   name: "program",
                   placeholder: "Enter the Program or Course Name(Ex: C++ Certificate)",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Organization",
                   name: "organization",
                   placeholder: "Enter the Name of Organization(Ex: NPTEL)",
                   type: "text",
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "Location",
                   name: "location",
                   placeholder: "Enter the Location",
                   type: "text",
                   long:true,
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "Start Date",
                   name: "start_date",
                   placeholder: "",
                   type: "date",
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "End Date",
                   name: "end_date",
                   placeholder: "Enter the End Date",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "Description",
                   name: "description",
                   placeholder: "Put a Short Description",
                   type: "text",
                   long:true,
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "Any Certificate Link",
                   name: "link",
                   type: "text",
                   grid: 'col l1 m1 s1 center',
                   placeholder: "Enter the link"
                 },
               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct description={description} cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div>
    {!this.state.isAddProduct && <ProductList referOutside={this.props.referOutside} username={this.props.username} title={title} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
      <React.Fragment>
      <Grid container spacing={1}>
         <Grid item xs={6} sm={6}/>
         <Grid item xs={5} sm={5}>
          {!(this.props.referOutside) && <Button variant="contained" style={{float:'right'}} color="secondary" onClick={(e) => this.onCreate(e,this.props.options)} disabled={this.state.disabled}>
           Add Data
           </Button>
          }
         </Grid>
         <Grid item xs={1} sm={1}/>
     </Grid>
     </React.Fragment>
    }
    { productForm }
    <br/>
  </div>

);
}
}
