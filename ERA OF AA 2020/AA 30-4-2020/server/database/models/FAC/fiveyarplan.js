const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');



const five = new Schema({
datafrom: { type: String, unique: false, required: false },
expired: { type: Boolean, unique: false, required: false },
completed: { type: Boolean, unique: false, required: false },
expire_year: { type: Number, unique: false, required: false },
username: { type: String, unique: false, required: false },
action: { type: String, unique: false, required: false },
content:{ type: String, unique: false, required: false },
date:{ type: String, unique: false, required: false },
month:{ type: String, unique: false, required: false },
year:{ type: String, unique: false, required: false },
verified:{ type: Boolean, unique: false, required: false },
verified_by:{ type: String, unique: false, required: false },
verification_link:{ type: String, unique: false, required: false },
})

five.plugin(autoIncrement.plugin, { model: 'Schema-Five_Year_Plan', field: 'serial', startAt: 1,incrementBy: 1 });



var fivep = mongoose.model('five_year_plan', five);




module.exports = fivep
