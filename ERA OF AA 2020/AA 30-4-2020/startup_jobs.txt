<------------------------Start System---------------------------->

1. Open "server" folder--->(open terminal or cmd)---> Type: nodemon server.js 

2. Again in the same folder open a new terminal and type : nodemon s_server.js

3. Come out of the server folder make sure you are inside ework folder where you
   can see package.json file. Now type :  "npm start" in cmd




<___________________________Instructions  ____________________________->

After all the server and frontend server gets started:
  

 1. Kindly navigate to :  localhost:3000/admin  -- then signup as a admin --- then login.
         --> inside that click on Super select option
         --> Then under Handle Registration, enable Faculty & Student Registration
  
 2. Now click Insert option and under that:
     
          --->Click  "Insert Into NavBar"
          ---> Then select Faculty and these datas one by one:--
                  Nav Title            Link
                 -----------         ------------
                a. Profile & Degree     /fprofile
                b. Dashboard            /dash
                c. Section              /partA
                d. Today's DayOrder Activity     /time_new
                e. Manage Input         /manage
                f. Master's TimeTable   /master
                g. Five Year's Plan     /five
		h. Extra Slot           /extra

          ---> And after selecting student option add these datas:
                  
                a. Profile         /sprofile
                b. Resume          /resume

          ----->  Then Go To "InsertResponsibility Percentage" and Percentages as follow: -

                  Director & Principle:  
                                           Administrative--
								50
								Roles & Responsibilities
								10
 
						     		50
								Clerial Work
								5
 
								50
								Planning / Strategy
								5
                                           Reserach--
	
								40
								Publications
								2
 
								
								40
								IPR & Patents
								4
 
								
								40
								Funded / Sponsored Project
								2
 
								
								40
								Technology Devt. & Consultancy
								4
 
								
								40
								Research Center Establishment
								4
					  Academic ---
								10
								Curriculum
								4

                  Assis Director & DEAN & HOD:  

                                           Administrative--
								
								45
								Roles & Responsibilities
								6
 
								
								45
								Clerial Work
								6
 
								
								45
								Plannaing / Strategy
								6
                                           Reserach--
								30
								Publications
								2
 
								
								30
								IPR & Patents
								2
 
								
								30
								Funded / Sponsored Project
								2
 
								
								30
								Technology Devt. & Consultancy
								4
					  Academic ---
								25
								Curriculum
								10
                 Professor:  

                                           Administrative--
								
								25
								Roles & Responsibility
								2
 
								25
								Clerial Work
								2
 
								25
								Planning & Strategy
								6
                                           Reserach--
								40
								Publications
								2
 
								
								40
								IPR & Patents
								4
 
								
								40
								Technology Devt. & Consultancy
								4
 
								
								40
								Funded / Sponsored Project
								4
 
								
								40
								Research Guidance
								2
					  Academic ---
								35
								Curriculum
								14

                Assistant Professor:  

                                           Administrative--
								25
								Roles & Responsibility
								6
								 
								25
								Clerial Work
								2
 

								25
								Planning & Strategy
								2
                                           Reserach--
								25
								Publications
								3
 

								25
								IPR & Patents
								1
 

								25
								Funded / Sponsered Project
								2
 

								25
								Technology Devt. & Consultancy
								2
 

								25
								Research Center Establishment
								2
					  Academic ---
								50
								Curriculum
								16
 

								50
								Co-Curricular
								1
 
								50
								Extra Curricular
								1

								50
								Evaluation & Placement Work
								2

                Associa Professor:  

                                           Administrative--
								20
								Roles & Responsibility
								4

								20
								Clerial Work
								2

								20
								Planning & Strategy
								2
                                           Reserach--
								40
								Publications
								4
 
								40
								IPR & Patents
								2
 

								40
								Funded / Sponsored Project
								4
 
								40
								Technology Devt. & Consultancy
								2
 

								40
								Product Development
								2
 

								40
								Research Guidance
								2
					  Academic ---
								40
								Curriculum
								14

								40
								Co-Curricular
								1
 
								40
								Evaluation & Placement Work
								1

              ----> Add after selecting "Insert Options For Sections Dropdown":
								Thesis & Projects Supervised
								Training or Workshop or Seminar or Conference Attended
								Training or Workshop or Seminar or Conference Held
								Paper Published & Accepted
								Research or Projects Done by You
								Contribution to University
								Department Activities
								Department Committees Membership
								Advising and Counselling Details

             ---> Then click "Insert Designation" and add designtions as follow(can be changed)[Remeber to Add Designation Order which is 
                   actually priority order] ---      

								Director            1
								Principle           2
								Assistant Director  3
								Dean                4
								HOD                 5
								Professor           6
								Associate Professor 7
								Assistant Professor 8

             --->We have also the options called add Department. One should add this as in signup it will be rendered --(So add Accordingly)--


                                            A Sample Data----
                                                                 Computer Science
								 Information Technology
								 Software Engineering
								 Mechanical Engineering     


         

 3. After completing these operations kindly move to localhost:3000 and proceed as per your choice
                          
