const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const different_res = new Schema({
  username: {type: String , unique: false, required: false},
  task_name:{type: String , unique: false, required: false},
  description:{type: String , unique: false, required: false},
  end_date:{type: String , unique: false, required: false},
  start_date:{type: String , unique: false, required: false},
  link:{type: String , unique: false, required: false},
  level:{type: String , unique: false, required: false},
  referTask:{type: Number, unique: false, required: false},
  submittedOn:{type: String , unique: false, required: false},
})



different_res.plugin(autoIncrement.plugin, { model: 'Task', field: 'serial', startAt: 1,incrementBy: 1 });

const Different_res = mongoose.model('Task', different_res)
module.exports = Different_res
