const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const facultylist = new Schema({
  alloted_slots: { type: String, unique: false, required: false },
  faculty_id:{ type: String, unique: false, required: false },
  hour:{ type: String, unique: false, required: false },
  username:{ type: String, unique: false, required: false },
  current:{ type:Boolean, unique: false, required: false,default:true },
  timing:{ type:String, unique: false, required: false },
  batch:{ type: String, unique: false, required: false },
  year:{ type: String, unique: false, required: false },
  sem:{ type:String, unique: false, required: false},
  dept:{ type:String, unique: false, required: false },
})



facultylist.plugin(autoIncrement.plugin, { model: 'FacultyList', field: 'serial', startAt: 1,incrementBy: 1 });

const FacultyList = mongoose.model('FacultyList', facultylist)
module.exports = FacultyList
