import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import { Select } from 'react-materialize';
import AddProduct from './add';
import ProductList from './prod'

export default class Responsibility extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      redirectTo:'',
      option:'',
      level:'',
      designation:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}
handleLevel = (e) =>{
  this.setState({level: e.target.value})
}

componentDidMount(){
  this.fetchDesig()
}

fetchDesig =() =>{
  axios.post('/ework/user/fetch_designation')
  .then(res => {
      if(res.data)
      {
        this.setState({designation:res.data})
      }
  });
}


  render()
  {
    let datas;
    if(this.state.designation.length !==0)
    {
       datas =
       <Select xl="12"l="12" s="12" m="12" value={this.state.level} onChange={this.handleLevel}>
       <option value="" disabled defaultValue>Designation</option>
            {this.state.designation.map((content,index)=>{
              return(
                <option key={index} value={content.designation_name}>{content.designation_name}</option>
              )
            })}
         </Select>
    }
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <div className="row">
        <div className="col l6 xl6 s12 m12">
          <div className="row">
            <div className="col l5 xl5 s6 m6" style={{marginTop:'15px',color:'red'}}><b>Select Root Responsibility - </b></div>
            <div className="col l6 xl6 s12 m12">
              <Select name="title" xl="12"l="12" s="12" m="12" value={this.state.option} onChange={this.handleOption}>
              <option value="" disabled defaultValue>Select Here...</option>
              <option value="Administrative">Administrative</option>
              <option value="Academic">Academic</option>
              <option value="Research">Research</option>
              </Select>
            </div>
            </div>
        </div>
        <div className="col l6 xl6 m12 s12">
        <div className="row">
          <div className="col l6 xl6 s6 m6" style={{marginTop:'15px',color:'red'}}><b>Select Sub-Responsibility Under Root - </b></div>
          <div className="col l6 xl6 s12 m12">
               {datas}
        </div>
        </div>
        </div>
        </div>
        <div className="row">
          <Gg options={this.state.option} level={this.state.level} username={this.state.username}/>
        </div>
      </React.Fragment>

    )
  }
  }
}

 class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.fetchlogin();
}

fetchlogin = () =>{
    axios.get('/ework/user/'
  )
    .then(response =>{
      if(response.data.user)
      {
        this.setState({username:response.data.user.username})
      }
      else{
        this.setState({
          redirectTo:'/ework/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }


   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
       addroute="/ework/user/add_from_insert_in_admin";
       editroute = "/ework/user/edit_inserted_data"
     if(!(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/ework/user/fetch_for_edit"
     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }


   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Overall Percentage for Root Responsibility",
                   name: "responsibilty_root_percentage",
                   placeholder: "Enter the Percentage of Root Responsibility",
                   type: "number",
                   grid: 2,
                   div:"col l3 s3 m3 xl3 center",
                 },
                 {
                   header: "Sub-Responsibility Under Root",
                   name: "responsibilty_title",
                   placeholder: "Enter the Title of Sub Responsibility",
                   type: "text",
                   grid: 2,
                   div:"col l3 s3 m3 xl3 center",
                 },
                 {
                   header: "Perentage For Sub-responsibilty",
                   name: "responsibilty_percentage",
                   placeholder: "Enter the Percentage for Sub-Responsibility",
                   type: "text",
                   grid: 2,
                   div:"col l3 s3 m3 xl3 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <AddProduct cancel={this.updateState} username={this.state.username} action={this.props.options} level={this.props.level}  data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div>
    {!this.state.isAddProduct && <ProductList action={this.props.options} level={this.props.level}  data={data1}  editProduct={this.editProduct}/>}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       {(this.props.options && this.props.level) && <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button> }
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</React.Fragment>
);
}
}
}
