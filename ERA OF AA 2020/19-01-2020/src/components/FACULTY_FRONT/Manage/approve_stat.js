import React, { Component } from 'react'
import { } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from '../TimeTable/colordiv.js'
require("datejs")

/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{

  constructor() {
    super();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      approved:[],
      viewdata:[],
      available:'no',
      notfound:'',
    }
  }


  componentDidMount(){
    M.AutoInit();
    this.knowExpiry();

  }

  knowExpiry = ()=>{
    axios.post('/ework/user/request_Expiry_Check')
    .then( res => {
        if(res.data)
        {
          this.fetchApprove()
        }
    });
  }
  componentDidUpdate =(prevProps) => {
    if (prevProps.request_props !== this.props.request_props) {
      this.fetchApprove();
    }
  }

  fetchApprove = () =>{
    axios.get('/ework/user/fetch_requested_list').then(res=>{
      if(res.data)
      {
        this.setState({
          approved: res.data,
        })
      }
    })
  }


  handleViewRequest = (content,index) =>{
    this.componentDidUpdate(index)
    this.setState({
      available:'yes',
      viewdata: content,
    })
  }


      render(){
              return(
          <React.Fragment>
          <h6 className="center pink white-text" style={{height:'45px',paddingTop:'12px'}}><b>STATUS OF YOUR REQUEST</b></h6><br /><br />
          {this.state.approved.length!==0 ?
            <React.Fragment>
          <div className="row">
            <div className="col l1 xl1 s2 m2 center"><b>Requested Day Order</b></div>
            <div className="col l1 xl1 s2 m2 center"><b>Official ID</b></div>
            <div className="col l1 xl1 s2 m2 center"><b>Requested Date</b></div>
            <div className="col l1 xl1 s2 m2 center"><b>Expire Date</b></div>
            <div className="col l2 xl2 s2 m2 center"><b>Deny Reason</b></div>
            <div className="col l1 xl1 s3 m3 center"><b>Reason</b></div>
            <div className="col l2 xl2 s3 m3 center"><b>Reason</b></div>
            <div className="col l2 xl2 s2 m2 center"><b>Status</b></div>
            <div className="col l1 xl1 s1 m1 center"><b>Action</b></div>
          </div>
          <hr />
          <div className="col l12 s12 m12 xl12">
          {this.state.approved.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 xl1 s2 m2 center">{content.day_order}</div>
            <div className=" col l1 xl1 s2 m2 center">{content.username}</div>
            <div className=" col l1 xl1 s2 m2 center">{content.req_day}/{content.req_month}/{content.req_year}</div>
            {content.expired_day ? <div className=" col l1 xl1 s2 m2 center red-text">{content.expired_day}/{content.expired_month}/{content.expired_year}</div> : <div className=" col l1 xl1 s2 m2 center red-text">NULL</div>}
            {content.denying_reason ? <div className=" col l2 xl2 s2 m2 center red-text">{content.denying_reason}</div>: <div className=" col l2 xl2 s2 m2 center red-text">NULL</div>}
            {content.req_hour ? <div className="col l1 xl1 s1 m1 center"></div>:<div className="col l1 xl1 s1 m1 center">N/A</div>}
            <div className=" col l2 xl2 s3 m3 center">{content.req_reason}</div>
            </div>
            <div className="col l2 xl2"><StatusReq content={content} serial={content.serial} /></div>
            <div className=" col l1 xl1 s1 m1 center"><i className="material-icons small">close</i></div>
            </div>
            </React.Fragment>
          ))}
          </div>
          </React.Fragment> : <div className="center">No Request Found !!</div>
        }
          </React.Fragment>

      )
      }
    }

class StatusReq extends Component{
  constructor(props)
  {
    super(props)
    this.state ={
      status:'',
      viewdata:[],
      available:'no',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStatus_Of_Request()
  }


  handleViewRequest = (content,index) =>{
    this.setState({
      available:'yes',
      viewdata: content,
    })
  }

  fetchStatus_Of_Request= () =>{
    axios.post('/ework/user/fetch_requested_status',{
      serial: this.props.serial,
    }).then(res=>{
      this.setState({status: res.data})
    })
  }
  render()
  {
    let btn;
    if(this.state.status === "approved")
    {
      btn =
      <button className="btn col l12 s2 xl12 m2 right blue-grey darken-2 sup"  onClick={() => this.handleViewRequest(this.props.content,this.props.serial)}>Upload Data</button>
    }
    else if(this.state.status === "denied")
    {
      btn = <div className="col l12 s12 xl2 m2 center" style={{color:'red',borderLeft:'1px solid red',borderRight:'1px solid red',borderTop:'1px solid red',borderBottom:'1px solid red'}}>Denied</div>
    }
    else if(this.state.status === "pending")
    {
      btn = <div className="col l12 s2 xl12 m2 center" style={{color:'red',borderLeft:'1px solid red',borderRight:'1px solid red',borderTop:'1px solid red',borderBottom:'1px solid red'}}>Pending</div>
    }

    return(
      <React.Fragment>
      <div>{btn}</div>
      <div className="col l12 s12 m12 xl12">
       <Content available={this.state.available} view={this.state.viewdata}/>
     </div>
      </React.Fragment>
    )
  }
}


/*-----------------------------------------------This class is for dynamically allocating div ------------- */

class Content extends Component{
  constructor(props){
    super(props);
    this.state={
      username: props.view.username,
      day_order: props.view.day_order,
      day:props.view.day,
      month:props.view.month,
      year:props.view.year,
      redirectTo:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
  }
  render(){
    console.log(this.props.view)
    let output;
    if(this.props.available === "yes") {
      output =
      <React.Fragment>
        <div className="cover_all">
          <div className="up">
             <ColorRep usern={this.props.view.username} day={this.props.view.req_day} month={this.props.view.req_month} year={this.props.view.req_year} day_order={this.props.view.day_order}/>
          </div>
        </div>
      </React.Fragment>
    }
    else
    {
      output=
      <div></div>
    }
          return(
            <div>
            {output}
            </div>
          );
  }
}
