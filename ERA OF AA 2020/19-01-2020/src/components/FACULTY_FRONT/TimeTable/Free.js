import React, { } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import M from 'materialize-css'
import {Select} from 'react-materialize'
var empty = require('is-empty');

export default class Free extends React.Component {
  constructor() {
    super()
    this.state = {
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        covered:'',
        freefield:'',
        freeparts:'',

    }
    this.handlecovered = this.handlecovered.bind(this)
    this.handleFreeVal = this.handleFreeVal.bind(this)
    this.handleFreeField = this.handleFreeField.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    }

    closeModal=()=>{
      this.props.closeModal();
    }
    colorChange=()=>{
      this.props.color()
    }

        handleFreeField =(e) =>{
          this.setState({
            freefield: e.target.value,
          })

        }

        handleFreeVal =(e) =>{
          this.setState({
            covered: e.target.value,
          })

        }


        handlecovered =(e) =>{
          e.preventDefault();
          window.M.toast({html: 'Submitting...', classes:'rounded yellow black-text'});
          if(empty(this.state.freefield)||empty(this.state.covered))
          {
            window.M.toast({html: 'Enter All the Details First',classes:'rounded #ec407a pink lighten-1'});
            this.setState({
              freefield:'',
              covered:'',
            })
          }
          else {
              this.setState({disabled:'disabled'})
          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var myDate = new Date.today();
          var week =myDate.getWeek();

          axios.post('/ework/user/free', {
            order:this.props.day_order+this.props.day_slot_time+'Free',
            day_slot_time: this.props.day_order+this.props.day_slot_time,
            freefield: this.state.freefield,
            covered: this.state.covered,
            freeparts: this.state.freeparts,
            date:this.props.day,
            month: this.props.month,
            year: this.props.year,
            week:week,
            slot:this.props.slot,
            time:this.props.time,
            day_sl:this.props.day_sl,
          })
            .then(response => {
              this.setState({disabled:''});
              if(response.status===200){
                this.closeModal();
                 if(response.data.already_have){
                  window.M.toast({html: response.data.already_have, classes:'rounded pink lighten-1'});
                }
                else if(response.data.suc)
                {
                  window.M.toast({html: response.data.suc, classes:'rounded pink lighten-1'});
                }
                else if(response.data === 'done')
                {
                  window.M.toast({html: 'Submitted !!', classes:'rounded green darken-1'});
                  this.colorChange();
                }
              }
            }).catch(error => {
              window.M.toast({html: 'Internal Error',  displayLength:'1500'});
            })
            this.setState({
              freefield:'',
              covered:'',
          })
}

        }

  componentDidMount()
  {
    let selects = document.querySelectorAll('select');
    M.FormSelect.init(selects, {});
  }

  updateAllotV (userObject) {
    this.setState(userObject)
  }
  freeParts=(userObject)=> {
    this.setState(userObject)
  }

render(){
  return(
    <div>
    <span className="">Plaese Mention on which area you are going to submit the datas :</span>
    <div className="row"><div className="col l4 s12 m12 xl4">
              <select className="over" value={this.state.freefield} onChange={this.handleFreeField}>
                <option value="" disabled defaultValue>Select Here</option>
                <option value="Academic">Academic</option>
                <option value="Research">Research Work</option>
                <option value="Administrative">Administrative Work</option>
              </select></div>
              <div className="col l4 s12 m12 xl4">
                 <FreeS freefield={this.state.freefield} freeParts={this.freeParts} />
              </div>
              </div>
    <label className="pure-material-textfield-outlined alignfull">
      <textarea
        className="area"
        type="text"
        placeholder=" "
        min="10"
        max="60"
        value={this.state.covered}
        onChange={this.handleFreeVal}
      />
      <span>Detail About Your work</span>
    </label>
    <button disabled={this.state.disabled} className="waves-effect btn col l2 s4 blue-grey darken-2 sup right" onClick={this.handlecovered}>SUBMIT</button>

    </div>
  );
}
}


class FreeS extends React.Component {
  constructor()
  {
    super()
    this.state ={
      administrativefreefield:'',
      hfreefield:'',
      researchfreefield:'',
      root_names:[],
    }

  }


componentDidUpdate =(prevProps,prevState) => {
  if (prevProps.freefield !== this.props.freefield) {
    this.getRoles();
  }
}

getRoles =()=>{
  axios.post('/ework/user/root_percentage_fetch',{action:this.props.freefield})
  .then(res=>{
    if(this.props.freefield ==="Academic")
    {
       console.log(res.data)
        const reques = res.data.filter(item => item.responsibilty_title !== "Curriculum");
        this.setState({root_names:reques})
    }
    else{
        this.setState({root_names:res.data})
    }

  })
}

handleHFreeField =(e) =>{
  this.setState({
    hfreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}

render()
{
  if((this.props.freefield ==="Academic") || (this.props.freefield ==="Administrative") || (this.props.freefield ==="Research"))
  {
    return(
      <React.Fragment >
      <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
      <option value="" disabled defaultValue>Please Select the Type</option>
      {this.state.root_names.map((content,index)=>{
        return(
          <option value={content.responsibilty_title} key={index}>{content.responsibilty_title}</option>
      )
      })}
    </Select>
      </React.Fragment>
    );
  }
  else{
    return(
      <React.Fragment>
      </React.Fragment>
    )
  }

}
}
