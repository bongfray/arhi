
import React from 'react';


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      Action:'',
      name: '',
      emailid: '',
      mobileno:'',
      address: '',
      gender:'',
      faculty_id:'',
      subject_taking:'',
      current:false,
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
  })
  if(this.props.data.Action === 'FACULTY LIST'){
    this.setState({current:true})
  }
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {

    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS</h5>
}

    return(
      <div>
      <div className="center">{pageTitle}</div><br />
      {this.props.description && <div className="center"><label style={{fontSize:'15px'}}>{this.props.description}</label></div>}
      <br />

          {this.props.data.fielddata.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1"/>
          <div className="col l2 m2 s2">{content.header}</div>
            <div className="col l8 s8 m8 input-field" key={index}>
                           <input
                             className="validate"
                             type={content.type}
                             placeholder=" "
                             id={content.name}
                             name={content.name}
                             value={this.state[content.name]}
                             onChange={e => this.handleD(e, index)}
                             required
                           />
                           <label htmlFor={content.name}>{content.placeholder}</label>
            </div>

            <div className="col l1" />
          </div>
          ))}
          <div className="row">
            <div className="col l9 xl9 hide-on-mid-and-down" />
            <div className="col l1 xl1 s6">
              <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
            </div>
            <div className="col l2 xl2 s6">
              <button className="btn blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
            </div>
          </div>
      </div>
    )
  }
}

export default AddProduct;
