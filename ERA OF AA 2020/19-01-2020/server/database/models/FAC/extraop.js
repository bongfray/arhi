const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const extra = new Schema({
  day_order: { type: Number, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  day: { type: Number, unique: false, required: false },
  month: { type: Number, unique: false, required: false },
  year: { type: Number, unique: false, required: false },
  order_from: { type: String, unique: false, required: false },
  faculty_id: { type: String, unique: false, required: false },
  hour: { type: String, unique: false, required: false },
  status: { type: Boolean, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  slot: { type: String, unique: false, required: false },
})



extra.plugin(autoIncrement.plugin, { model: 'Extra', field: 'serial', startAt: 1,incrementBy: 1 });

const Extra = mongoose.model('Request-For-ExtraHour', extra)
module.exports = Extra
