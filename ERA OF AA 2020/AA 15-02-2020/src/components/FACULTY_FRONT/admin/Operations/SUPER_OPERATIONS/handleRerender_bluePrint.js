import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import TimeTable from '../../../Schedule/Timetable'
import VisibilityIcon from '@material-ui/icons/Visibility';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
      enable_view:false,
      content:'',
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,content)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })
      window.M.toast({html: 'Sending Activation Mail...',classes:'rounded orange'});
      axios.post('/ework/user/allow_blueprint_req',{content:content
      })
      .then(res=>{
            window.M.toast({html: 'Activated !!', classes:'rounded green darken-2'});
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== content.username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_req_on_blueprint_render';
    }
    else if(this.props.choice === 'student')
    {
      route = '/ework/user/fetch_student_request';
    }
    axios.get(route)
    .then(res=>{
        this.setState({display:'block',requests:res.data})
    })
  }
handleView=()=>{
  this.setState({enable_view:!this.state.enable_view})
}

showCurrentStatus=(content)=>{
  this.setState({enable_view:true,content:content})
}

  render()
  {
    return(
      <React.Fragment>
      {this.state.enable_view &&
        <Dialog fullScreen open={this.state.enable_view} TransitionComponent={Transition}>
            <AppBar>
              <Toolbar>
               <Grid style={{marginTop:'55'}}container spacing={1}>
                 <Grid item xs={1} sm={1}>
                    <IconButton edge="start" color="inherit" onClick={this.handleView} aria-label="close">
                      <CloseIcon />
                    </IconButton>
                 </Grid>
                 <Grid item xs={10} sm={10}>
                   <div className="center" style={{fontSize:'20px'}}>Showing Details of <span className="yellow-text">{this.state.content.username}</span></div>
                 </Grid>
                 <Grid item xs={1} sm={1}/>
               </Grid>
              </Toolbar>
            </AppBar>
            <List>
              <div style={{marginTop:'70px'}}>
               <TimeTable props_to_refer={this.state.content} />
              </div>
            </List>
        </Dialog>
      }
      {this.props.choice &&
      <div className="row">
         <div className="col l12 xl12">
            <div className="card">
              <div className="card-title center pink white-text">Request For Blue Print Render</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.requests.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                        <div className="col l2 center"><b>Index</b></div>
                        <div className="col l2 center"><b>Official Id</b></div>
                        <div className="col l4 center"><b>Reason</b></div>
                        <div className="col l2 center"><b>View Status</b></div>
                        <div className="col l2 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.requests.map((content,index)=>(
                           <div className="row"  key={index}>
                            <div className="col l2 center">{index+1}</div>
                            <div className="col l2 center">{content.username}</div>
                            <div className="col l4 center" style={{wordWrap: 'break-word'}}>{content.req_reason}</div>
                            <div className="col l2 center">
                               <VisibilityIcon onClick={()=>this.showCurrentStatus(content)} />
                            </div>
                            <div className="col l2 center">
                                <div className="switch">
                                  <label>
                                    <input  checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content)}} type="checkbox" />
                                    <span className="lever"></span>
                                  </label>
                                </div>
                            </div>
                          </div>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
       }
      </React.Fragment>
    ) ;
  }
}
