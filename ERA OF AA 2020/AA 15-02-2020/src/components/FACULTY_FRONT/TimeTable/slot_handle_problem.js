import React, { Component } from 'react'
import { } from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';



export default class Switch extends React.Component {

    constructor ( props ) {
        super( props );
    this.state = {
      isChecked: false,
      day_set:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      var year = parseInt(Date.today().toString("yyyy"));
      if(this.leapYear(year)=== true)
      {
        this.setState({
          day_set: [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
        {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
      ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
        })
      }
      else
      {
        this.setState({
          day_set: [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
        {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
      ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
        })
      }
    }

    leapYear(year)
    {
     return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    }

    statusOfCompensation= (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
      })
    }
    render () {

        return(
          <div>

            <div className="switch center">
                <label style={{fontSize:'15px',color:'black'}}>
                    HandOver to Other Faculty
                    <input  checked={ this.state.isChecked } onChange={ this.statusOfCompensation} type="checkbox" />
                    <span className="lever"></span>
                    Want to compensate this on other day ?
                </label>
            </div>
            <br />
            <InputValue day_set={this.state.day_set} prop_datas={this.props.prop_datas} datas={this.state.isChecked} updateAllotV={this.props.updateAllotV}/>
          </div>
        );
    }

}

class InputValue extends Component{
  constructor()
  {
    super()
    this.state ={
      compday:'',
      comphour:'',
      compday_order:'',
      compense_faculty:'',
      hour_set:[{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},
      {value:7},{value:8},{value:9},{value:10}]
    }
  }

  handleComData =(evt)=>{
    const value = evt.target.value;
    this.setState({
      [evt.target.name]: value
    });
    this.props.updateAllotV({
      [evt.target.name]: value,
    });
  }


  render(){
    let actual_days;
    if(this.props.day_set.length>0)
    {
        var month = parseInt(Date.today().toString("MM"));
        var today = parseInt(Date.today().toString("dd"));
        var days=[];
        const day_data= this.props.day_set.filter(item => item.value===month);
        for(var j=1;j<=day_data[0].day;j++)
        {
          days.push(j);
        }
        actual_days= days.filter(item1=>item1>today)
    }
    let time = parseFloat(new Date().toString("HH.mm"));
    if(this.props.datas === true)
    {
      return(
        <div className="row">
         <div className="col l1"/>
         <div className="col l10 particular" style={{padding:'15px'}}>
            <div className="row">
                <div className="col l3 xl3 hide-on-mid-and-down"/>
                <div className="col l2 s12 m12 xl2">
                     <FormControl style={{width:'100%'}}>
                        <InputLabel id="day_order">Day Order</InputLabel>
                        <Select
                          labelId="day_order"
                          id="day_order"
                          name="compday_order"
                          value={this.state.compday_order}
                          onChange={this.handleComData}
                        >
                           <MenuItem value="1">1</MenuItem>
                           <MenuItem value="2">2</MenuItem>
                           <MenuItem value="3">3</MenuItem>
                           <MenuItem value="4">4</MenuItem>
                           <MenuItem value="5">5</MenuItem>
                        </Select>
                      </FormControl>
                </div>
                <div className="col l2 s12 m12 xl2">
                     <FormControl style={{width:'100%'}}>
                        <InputLabel id="month">Day</InputLabel>
                        <Select
                          labelId="month"
                          id="month"
                          name="compday"
                          value={this.state.compday}
                          onChange={this.handleComData}
                        >
                        {actual_days.map((content,index)=>{
                                return(
                                  <MenuItem key={index}  value={content}>{content}</MenuItem>
                                )
                          })}
                        </Select>
                      </FormControl>
                </div>
                 <div className="col l2 s12 m12 xl2">
                       <FormControl style={{width:'100%'}}>
                          <InputLabel id="hour">Hour</InputLabel>
                          <Select
                            labelId="hour"
                            id="hour"
                            name="comphour"
                            value={this.state.comphour}
                            onChange={this.handleComData}
                          >
                          {this.state.hour_set.map((content,index)=>{
                                  return(
                                    <MenuItem key={index}  value={content.value}>{content.value}</MenuItem>
                                  )
                            })}
                          </Select>
                        </FormControl>
                 </div>
                 <div className="col l3 xl3 hide-on-mid-and-down" />
            </div>
            <div className="left col l12 black-text">
               <span className="red-text"><b><u>REMEMBER : </u></b></span><br />
               1.  You are not allowed to put a date beyond this month.<br />
               2.  In this case if today is the last date of the current month,you will miss this slot to enter data for current month.<br />
               3.  In case you are typing invalid date and also submitting that, system will not render that slot.<br />
               4.  One more thing, the date you are submitting,system will not render that hour on that day in Today's DayOrder Section.
                   You can view that hour in Extra Slot on your prefered date.<br />
               5.  The compensation hour your are typing should not collide with your alloted slot.In that
                   case,system will not render that slot on your prefered date.<br />
               6.  You should not put today's date as a compensation date.
            </div>
            </div>
         <div className="col l1"/>
        </div>
      );
    }
    else {
      return(
        <div className="row">
        <div className="col l4"/>
        <div className="col l4 particular">
            {((parseFloat(this.props.prop_datas.content.end_time))<time) ?
              <React.Fragment>
                <h6 className="center red-text">Sorry! Slots can only be handovered within that hour.</h6>
              </React.Fragment>
              :
              <div className="input-field">
                <input type="text" id="faculty_id_c" value={this.state.compense_faculty} name="compense_faculty"
                 onChange={this.handleComData} />
                 <label htmlFor="faculty_id_c">Faculty Id</label>
              </div>
            }
        </div>
        <div className="col l4"/>
        </div>
      )

    }
  }
}
