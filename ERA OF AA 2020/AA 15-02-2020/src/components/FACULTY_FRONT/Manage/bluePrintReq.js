import React, { Component,Fragment } from 'react'
import axios from 'axios'


export default class ManageOne extends Component {
  constructor(props)
  {
    super(props)
    this.initialState ={
      action:'For Blue Print Render',
      username:'',
      expired:false,
      status:'pending',
      added:false,
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setState({username:this.props.username})
  }
  handleDatas=(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleRequest =(e) =>{
    e.preventDefault()
    if(!(this.state.req_reason))
    {
      window.M.toast({html: 'Enter all the Details First !!',classes:'rounded red'});
    }
    else{
    axios.post('/ework/user/entry_of_blueprint',this.state).then(response=>{
      if(response.data === 'exist')
      {
         window.M.toast({html:'Request Already There!!',classes:'rounded red'});
      }
      else
      {
         window.M.toast({html: response.data,classes:'rounded green'});
         this.setState({added:true,req_reason:''})
      }
    })
   }
  }
  render()
  {
    return(
      <Fragment>
      <div className="row">
      <div className="col l4 xl4 hide-on-small-only" />
        <div className="col l4 s12 xl4 m12 particular">
          <h6 className="center">REQUEST FOR BLUE-PRINT RENDER</h6><br />
          <div className="row">
            <div className="col l12 xl12 s12 m4">
              <label className="pure-material-textfield-outlined alignfull">
                <textarea
                  className=""
                  type="text"
                  placeholder=" "
                  min="10"
                  max="60"
                  name="req_reason"
                  value={this.state.req_reason}
                  onChange={this.handleDatas}
                />
                <span>Enter the Valid Reason</span>
              </label>
            </div>
          </div>
          <button className="btn right blue-grey darken-2 sup" onClick={this.handleRequest} style={{marginBottom:'8px'}}>Make A Request</button>
        </div>
        <div className="col l4 xl4 hide-on-small-only" />
      </div>
        <Status username={this.props.username} />
      </Fragment>
    )
  }
}

class Status extends Component{
  constructor()
  {
    super()
    this.state={
      request:'',
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq();
  }
  componentDidUpdate=(prevProps)=>{
    if((prevProps.added !== this.props.added)||(prevProps.username !== this.props.username))
    {
      this.fetchReq();
    }
  }
  fetchReq=()=>{
    axios.get('/ework/user/fetch_req_on_blueprint_render')
    .then(res=>{
      if(res.data)
      {
        if(res.data.length>0)
        {
          var request = res.data.filter(item=>(item.username === this.props.username) && (item.expired=== false))
          this.setState({request:request,loading:false})
        }
      }
    })
  }
  render()
  {
    return(
      <React.Fragment>
        {this.state.loading ?
           <div>Fetching ....</div>
            :
            <React.Fragment>
            {this.state.request.length!==0 ?
              <React.Fragment>
                <div className="row">
                  <div className="col l3 xl3"/>
                  <div className="col l6 xl6">
                      <h6 className="center pink white-text" style={{height:'45px',paddingTop:'12px'}}><div>STATUS OF YOUR REQUESTS</div></h6><br /><br />
                    <div className="row">
                      <div className="col l4 xl4 s2 m2 center"><b>Reason</b></div>
                      <div className="col l4 xl4 s4 m4 center"><b>Status</b></div>
                      <div className="col l4 xl4 s4 m4 center"><b>Expired Status</b></div>
                    </div>
                    <div className="row">
                    {this.state.request.map((content,index)=>(
                      <React.Fragment key={index}>
                        <div className="col l4 xl4 s2 m2 center">{content.req_reason}</div>
                        <div className="col l4 xl4 s4 m4 center">
                         {content.status}
                        </div>
                        <div className="col l4 xl4 s4 m4 center">
                        {content.expired ?
                          <div className="red-text">Expired</div>
                          :
                          <div className="green-text">Not Expired</div>
                        }
                        {content.expired} [Will be expired within 48 Hour]</div>
                      <hr />
                      </React.Fragment>
                    ))}
                </div>
            </div>
            <div className="col l3 xl3"/>
            </div>
            </React.Fragment>
            :
            <div className="center">No Request Found !!</div>
          }
            </React.Fragment>
          }
      </React.Fragment>
    )
  }
}
