import React, { Component } from 'react';

import axios from 'axios'
import Nav from '../../dynnav'

export default class ARHI extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      searched_domain:'',
      selected_domain:'',
      list_of_domains:[],
      detail_domain_name:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
   this.getUser();
  }
  getUser =()=>
  {
    axios.get('/ework/user2/getstudent').then(response =>{
      if (response.data.user)
      {
        this.fetch_domains();
        this.fetch_selected();
      }
      else{

      }
     })
  }
  fetch_domains =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Domain-For-Student'})
    .then( response => {
        if(response){
          this.setState({fetched_domain:response.data})
        }
    });
  }
  fetch_selected=()=>{
    axios.get('/ework/user2/fetch_domains')
    .then( res => {
        if(res.data){
          this.setState({list_of_domains:res.data})
        }
    });
  }
  search =(e)=>{
    this.setState({searched_domain:e.target.value})
  }

  selected =(data)=>{
    this.setState({selected_domain:data})
    axios.post('/ework/user2/entry_domain',{domain:data.domain_name})
    .then( res => {
        if(res.data ==='have'){
          window.M.toast({html: 'Already Present !!',classes:'rounded #ec407a pink lighten-1'});
        }
        else if(res.data === 'done'){
          window.M.toast({html: 'Saved !!',classes:'rounded green darken-2'});
          this.setState({searched_domain:''})
          this.fetch_selected();
        }
    });
  }

  postDetail =(detail_domain_name)=>{
    this.setState({detail_domain_name:detail_domain_name})
  }

  render() {
    var domains = this.state.fetched_domain,
    searchString = this.state.searched_domain.trim().toLowerCase();
    if(searchString.length > 0)
    {
      domains = domains.filter(function(i) {
        return i.domain_name.toLowerCase().match( searchString );
      });
    }



    return (
      <React.Fragment>
        <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
           <div className="row search-engine">
             <div className="col l4 xl4 s1 m2" />
             <div className="col l4 xl4 s10 m8" style={{minHeight:'10px',borderRadius:'10px 10px 0px 0px',boxShadow:'0 1px 2px 0 rgba(60,64,67,0.3), 0 1px 3px 1px rgba(60,64,67,0.15)',marginTop:'10px'}}>
                <div className="row">
                   <div className="col l12 xl12 s12 m12">
                      <input type="text" placeholder="Search the Domain" onChange={this.search} value={this.state.searched_domain} />
                   </div>
                </div>
                {this.state.searched_domain &&
                   <div className="row flow-on-me" style={{minHeight:'10px',borderRadius:'0px 0px 10px 10px',boxShadow:'0 1px 2px 0 rgba(60,64,67,0.3), 0 1px 3px 1px rgba(60,64,67,0.15)'}}>
                     {domains.map((content,index)=>(
                          <div className="col l4 center" key={index} style={{padding:'5px'}} onClick={()=>this.selected(content)}>{content.domain_name}</div>
                     ))}
                  </div>
              }
             </div>
             <div className="col l4 xl4 s1 m2" />
           </div>
           <div className="row">
              {this.state.list_of_domains.map((content,index)=>(
                <div className="col l2 xl2 m3 s4 card hoverable go" style={{borderRadius:'5px',minHeight:'60px'}} onClick={()=>this.postDetail(content.domain_name)}>
                   <div>
                    <i className="material-icons small right">close</i>
                    <h6 className="center">{content.domain_name}</h6>
                   </div>
                </div>

              ))}
           </div>
           <Detailer detail_domain_name={this.state.detail_domain_name} />
      </React.Fragment>
    );
  }
}

class Detailer extends Component {
  constructor() {
    super()
    this.state={
      isChecked:'Begineer',
      active:'Begineer',
      toggled:false,
      instructions:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetch_Domain_Level()
  }
  componentDidUpdate =(prevProps,prevState)=>{
    if(prevState.isChecked!==this.state.isChecked){
      this.fetch_Domain_Level()
    }
    if(prevProps.detail_domain_name!== this.props.detail_domain_name){
      this.fetch_Domain_Level()
    }
  }
  fetch_Domain_Level =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Domain-'+this.props.detail_domain_name,usertype:this.state.isChecked})
    .then( res => {
        if(res.data){
          this.setState({instructions:res.data})
        }
    });
  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "pink go white-text";
      }
      return "go";
  }


  render(){
    return(
      <React.Fragment>
         {this.props.detail_domain_name &&
             <React.Fragment>
                <div className="card hoverable" style={{marginRight:'7px',marginLeft:'7px',borderRadius:'5px'}}>
                    <h5 className="center">Selected Domain <span className="pink-text">{this.props.detail_domain_name}</span></h5>
                    <h6 className="center">Overall Rating : <span className="red-text">3*</span></h6>
                    <div className="row">
                        <div className="col l3 xl3 m4 s9">
                           <div className="row div-style">
                             <div className={'col l4 xl4 s4 m4 center particular '+this.color('Begineer')} onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</div>
                             <div className={'col l4 xl4 s4 m4 center particular '+this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</div>
                             <div className={'col l4 xl4 s4 m4 center particular '+this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</div>
                           </div>
                        </div>
                        <div className="col l7 xl7 hide-on-small-only m4"/>
                        <div className="col l2 xl2 s3 m4 card">
                          <h6 className="center">Your Rating in this level : <span className="red-text">4*</span></h6>
                        </div>
                    </div>
                    <div className="row head">
                       <div className="center col l3 xl3 s3 m3"><b>LEARN</b></div>
                       <div className="center col l3 xl3 s3 m3"><b>LINK TO REFER</b></div>
                       <div className="center col l2 xl2 s2 m2"><b>TASK COMPLETED</b></div>
                       <div className="center col l2 xl2 s2 m2"><b>RATING</b></div>
                       <div className="center col l2 xl2 s2 m2"><b>CALLS</b></div>
                    </div>
                    <hr />
                    <div className="body">
                      {this.state.instructions.map((content,index)=>(
                        <React.Fragment key={index}>
                          <div className="row">
                             <div className="center col l3 xl3 s3 m3">{content.course_to_learn}</div>
                             <div className="center col l3 xl3 s3 m3">
                               <div className="hide-on-med-and-down"><a  rel="noopener noreferrer" href={content.ref_link_for_skill} target="_blank">{content.ref_link_for_skill}</a></div>
                               <div className="hide-on-large-only"><a rel="noopener noreferrer" href={content.ref_link_for_skill} target="_blank">LINK</a></div>
                             </div>
                             <div className="center col l2 xl2 s2 m2"><TaskCompleted course={content.course_to_learn} /></div>
                             <div className="center col l2 xl2 s2 m2"><Rating course={content.course_to_learn} /></div>
                             <div className="center col l2 xl2 s2 m2">NO</div>
                          </div>
                          <hr />
                        </React.Fragment>
                      ))}
                    </div>
                </div>
             </React.Fragment>
         }
      </React.Fragment>
    )
  }
}


class TaskCompleted extends Component {
  constructor(){
    super()
    this.state={
      total:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetchCompletationHistory()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props){
      this.fetchCompletationHistory()
    }
  }
  fetchCompletationHistory =()=>{
    axios.post('/ework/user2/fetchCompletationHistory',{course:this.props.course})
    .then( res => {
        if(res.data === 'No Data'){
          this.setState({total:res.data})
        }
        else{
          this.setState({total:res.data.length})
        }
    });
  }
  render() {
    return (
      <div>
        {this.state.total}
      </div>
    );
  }
}

class Rating extends Component {
  constructor(){
    super()
    this.state={
      rating:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetchRating()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props){
      this.fetchRating()
    }
  }
  fetchRating =()=>{
    axios.post('/ework/user2/fetchRating',{course:this.props.course})
    .then( res => {
      console.log(res.data)
        if(res.data){
          this.setState({rating:res.data})
        }
    });
  }
  render() {
    return (
      <div>
        {this.state.rating}
      </div>
    );
  }
}
