const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const different_res = new Schema({
  action: {type: String , unique: false, required: false},
  usertype: {type: String , unique: false, required: false},
  username: {type: String , unique: false, required: false},
  domain_name: {type: String , unique: false, required: false},
})



different_res.plugin(autoIncrement.plugin, { model: 'Arhi', field: 'serial', startAt: 1,incrementBy: 1 });

const Different_res = mongoose.model('Arhi', different_res)
module.exports = Different_res
