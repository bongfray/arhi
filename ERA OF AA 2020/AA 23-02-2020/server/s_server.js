const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const session_stud = require('express-session')
const dbConnection_stud = require('./database/studentdb')
const MongoStoreS = require('connect-mongo')(session_stud)
const passport = require('./passport/stud_pass');
const apps = express()
const PORT = 8081

const suser = require('./routes/suser')

// MIDDLEWARE
apps.use(morgan('dev'))
apps.use(
	bodyParser.urlencoded({
		extended: true
	})
)
apps.use(bodyParser.json())

// Sessions
apps.use(
	session_stud({
		secret: 'fraggle-rock',
		store: new MongoStoreS({ mongooseConnection: dbConnection_stud}),
		resave: false,
		saveUninitialized: false
	})
)




// Passport
apps.use(passport.initialize())
apps.use(passport.session())


// Routes
apps.use('/ework/user2', suser)




// Starting Server
apps.listen(PORT, () => {
	console.log(`App listening on PORT: ${PORT}`)
})
