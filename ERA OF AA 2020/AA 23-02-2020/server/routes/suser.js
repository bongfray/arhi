const express = require('express')
const route = express.Router()
const bcrypt = require('bcryptjs');
const Suser = require('../database/models/FAC/user')
const passport = require('../passport/stud_pass')
const NavS = require('../database/models/STUD/navstud')
const AdminOrder = require('../database/models/FAC/admin')
const Admin_Operation = require('../database/models/FAC/admin_operation')
const Resume = require('../database/models/STUD/resume_schema')
const Task = require('../database/models/STUD/task')
const Arhi = require('../database/models/STUD/path')
const Faculty = require('../database/models/STUD/faculty_list')
const Notification = require('../database/models/FAC/notification')
const StudentSubmission = require('../database/models/FAC/time_table')
const Request = require('../database/models/FAC/req')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')
var stringSimilarity = require('string-similarity');
console.log("Student Server")



route.post('/ssignup', (req, res) => {
//console.log(req.body);
    Suser.findOne({ username: req.body.username }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new Suser(req.body)
            newUser.save((err, savedUser) => {
              if(err)
              {

              }
              else if(savedUser)
              {
                 console.log('here')
                var succ= {
                  succ: "Successful SignedUP"
                };
                res.send(succ);
              }
            })
        }
    })
})


route.post(
    '/slogin',(req, res, next) => {
      //console.log(req.ip)
      passport.authenticate('local', function(err, user, info) {
        if (err) {
           return next(err);
         }
        if (!user) {
            res.status(401);
            res.send(info.message);
            return;
        }
        req.logIn(user, function(err) {
          if (err) {
            return next(err);
          }
          var userInfo = {
              username: req.user.username,
              count: req.user.count,
          };
          res.send(userInfo);
        });
      })(req, res, next);
    }
)


route.get('/getstudent', (req, res, next) => {
//  console.log(req.user)
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

route.post('/slogout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

route.post('/sprofile', (req, res) => {

    const {up_username,up_phone,up_name,up_dob} = req.body;
    Suser.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)

route.get('/getstudentprofile', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    Suser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});

    route.post('/sforgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       Suser.findOne(
         {mailid: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'Invalid User'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           console.log(token);
           Suser.updateOne({mailid:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 900000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com',
            to: mailid,
            subject: 'Reset Password:  eWorks',
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 15min</p>'
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })

        })



        route.post('/addnav', function(req,res) {
                  var trans = new NavS(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });


/*-------------------------------------------------------------Fetch Request From Nav Bar---------------------- */
        route.get('/fetch_snav',function(req,res) {
          NavS.find({ }, function(err, docs){
          if(err)
          {
            console.log(err)
          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/send_task', function(req,res) {
          //console.log(req.body)
        Task.findOne({referTask:req.body.referTask},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {
            Task.updateOne({referTask:req.body.referTask},
              {$set:{description:req.body.description,link:req.body.link,start_date:req.body.start_date,end_date:req.body.end_date}},(err,done)=>{
              if(err)
              {

              }
              else if(done)
              {
                res.send('updated');
              }
            })
          }
          else{
              var respon = new Task({
                username:req.user.username,
                task_name:'Task '+req.body.task_name,
                level:req.body.level,
                description:req.body.description,
                link:req.body.link,
                start_date:req.body.start_date,
                end_date:req.body.end_date,
                referTask:req.body.referTask,
              });
              respon.save((err, savedUser) => {
                if(err)
                {

                }
                else if(savedUser)
                {
                  validateSkill(req,res);
                }
              })
          }

        })
      });



   function validateSkill(req,res){
     console.log(req.body.task_name)
     Admin_Operation.find({$and:[{action:'Task '+req.body.task_name}]},(err,docs)=>{
       if(err){

       }
       else if(docs)
       {
         Resume.findOne({$and:[{action:'Skills'},{username:req.user.username},
         {skill_name:req.body.task_name}]},(err,have)=>{
           if(err)
           {

           }
           else if(have)
           {
             Task.find({$and:[{task_name:'Task '+req.body.task_name},{username:req.user.username}]},(err,done)=>{
               if(err){

               }
               else if(done)
               {
                  console.log(done.length);
                  console.log(docs.length);
                 if((done.length)>=((((docs.length)*75)/100)))
                 {
                   Resume.updateOne({$and:[{action:'Skills'},{username:req.user.username},
                   {skill_name:req.body.task_name}]},{skill_verification:true},(err,updated)=>{
                     if(err){

                     }
                     else if(updated)
                     {
                       //res.send('done')
                     }
                     else{
                       //res.send('done')
                     }
                   })
                 }
                 else
                 {
                   Resume.updateOne({$and:[{action:'Skills'},{username:req.user.username},
                   {skill_name:req.body.task_name}]},{skill_verification:false},(err,updated)=>{
                     if(err){

                     }
                     else if(updated)
                     {
                       //res.send('done')
                     }
                     else {
                       //res.send('done')
                     }
                   })
                 }
               }
               else
               {
                   //res.send('done');
               }
             })
             rateSkill(req,res,req.body.task_name,docs);
           }
           else
           {
               res.send('done');
           }
         })
       }
       else
       {
           res.send('done');
       }
     })
   }

      function rateSkill(req,res,course,admin_saved)
      {
        console.log('Inside Rate')
              Task.find({$and:[{task_name:'Task '+course},{username:req.user.username}]},(er,objs)=>{
                console.log(objs.length)
                if(er){

                }
                else
                {
                      let grade;
                      let count = ((objs.length) / (admin_saved.length))*100;
                      console.log(count)
                      if(count>0 && count<10)
                      {
                        grade = '0.5';
                      }
                      else if(count>=10 && count<20)
                      {
                        grade = '1';
                      }
                      else if(count>=20 && count<30)
                      {
                        grade = '1.5';
                      }
                      else if(count>=30 && count<40)
                      {
                        grade = '2';
                      }
                      else if(count>=40 && count<50)
                      {
                        grade = '2.5';
                      }
                      else if(count>=50 && count<60)
                      {
                        grade = '3';
                      }
                      else if(count>=60 && count<70)
                      {
                        grade = '3.5';
                      }
                      else if(count>=70 && count<80)
                      {
                        grade = '4';
                      }
                      else if(count>=80 && count<90)
                      {
                        grade = '4.5';
                      }
                      else if(count>=90 && count<=100)
                      {
                        grade = '5';
                      }
                      else if(count === 0)
                      {
                        grade = '0';
                      }
                      console.log(grade)
                      Resume.updateOne({$and:[{username:req.user.username},{action:'Skills'},{skill_name:course}]},{skill_rating:grade},(err,rated)=>{
                        if(err){

                        }
                        else if(rated){
                          res.send('done');
                        }
                      })
                }
              })
      }



      function labelRating_of_Domain(){

      }
      function domainRating()
      {

      }


      route.post('/fetchRating', function(req, res) {
        Resume.findOne({$and:[{username:req.user.username},{action:'Skills'},{skill_name:req.body.course}]},(err,found)=>{
          if(err){

          }
          else if(found){
            res.send(found.skill_rating);
          }
          else{
            res.send('No Data')
          }
        })
      })

      route.post('/fetchTasks',function(req,res) {
        let username;
        if(req.body.username)
        {
          username = req.body.username;
        }
        else {
          username= req.user.username;
        }
        Task.find({$and:[{level: req.body.level},{task_name:req.body.action},{username:username}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send(docs)
        }
        });
      })

      route.post('/deleteTask',function(req,res) {
        Task.deleteOne({$and:[{referTask: req.body.key},{username:req.user.username}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
          validateSkill(req,res);
        }
        });
      })



/*-------------------------------------------------------------Fetch Request From Admin Panel---------------------- */
        route.post('/fetch_student_nav',function(req,res) {
          NavS.find({action: req.body.action}, function(err, docs){
          if(err)
          {
            console.log(err)
          }
          else
          {
            res.send(docs)
          }
        });
        })


        route.post('/editnav', function(req,res) {
          NavS.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
            var succ= {
              succ: "Updated"
            };
            res.send(succ);
          })

        });

        route.post('/edit_existing_nav',function(req,res) {
          NavS.findOne({serial: req.body.id}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/delnav',function(req,res) {
          NavS.remove({serial:  req.body.serial },function(err,succ){
            res.send("OK")
          });
        })




        route.post('/handleStudent_Registration',function(req,res) {

        AdminOrder.findOne({$and:[{usertype: req.body.history},{order:'SIGNUP STATUS'}]}, function(err, docs){
          if(err)
          {

          }
          else if(docs)
          {
            AdminOrder.updateOne({$and:[{usertype: req.body.history},{order:'SIGNUP STATUS'}]},{registration_status: req.body.checked},function(err,done){
              res.send("Done")
            })
          }
          else{
            var trans = new AdminOrder({
              order:'SIGNUP STATUS',
              usertype:req.body.history,
              status: req.body.checked
            }
            );
            trans.save((err, saved) => {
              if(err)
              {

              }
              else if(saved)
              {
                 res.send('done');
              }
            })
          }
        })
        })



        route.post('/handleStudent_BluePrint',function(req,res) {

        AdminOrder.findOne({$and:[{usertype: req.body.history},{order:'BLUE PRINT STATUS'}]}, function(err, docs){
          if(err)
          {

          }
          else if(docs)
          {
            AdminOrder.updateOne({$and:[{usertype: req.body.history},{order:'BLUE PRINT STATUS'}]},{status: req.body.checked},function(err1,done){
              if(err1)
              {

              }
              else if(done)
              {
                Suser.updateMany({render_timetable: {"$exists" : true}},{render_timetable:req.body.checked},(error2,updated2)=>{
                  if(error2)
                  {

                  }
                  else{
                     res.send('done');
                  }
                })
              }
              else {
                res.send('done')
              }
            })
          }
          else
          {
              var trans = new AdminOrder({
                order:'BLUE PRINT STATUS',
                usertype:req.body.history,
                status: req.body.checked
              }
              );
              trans.save((err, saved) => {
                 if(err)
                 {

                 }
                 else if(saved)
                 {
                   Suser.updateMany({render_timetable: {"$exists" : true}},{render_timetable:req.body.checked},(error1,updated)=>{
                     if(error1)
                     {

                     }
                     else{
                        res.send('done');
                     }
                   })
                 }
                 else {
                   res.send('done');
                 }
              })
          }
        })
        })




        route.post('/stud_status', (req, res, next) => {
          AdminOrder.find({ },function(err,result){
            if(err)
            {

            }
            else if(result)
            {
              if(req.body.userEnable ==='verify')
              {
                if(result.length>0)
                {
                  console.log('Here')
                Suser.findOne({username:req.user.username},(error,ok)=>{
                  if(error)
                  {

                  }
                  else if(ok)
                  {
                    var fac_blueprint_status = result.filter(item => ((item.usertype==="stud") && (item.order==="BLUE PRINT STATUS")));
                    console.log(fac_blueprint_status[0].status)
                    console.log(ok.render_timetable)
                    if(fac_blueprint_status[0].status === true && ok.render_timetable === false)
                    {
                      res.send(result)
                    }
                    else if(fac_blueprint_status[0].status === true && ok.render_timetable ===true)
                    {
                      res.send(result)
                    }
                    else if(fac_blueprint_status[0].status === false && ok.render_timetable ===true)
                    {
                      res.send('ok')
                    }
                    else if(fac_blueprint_status[0].status === false && ok.render_timetable === false) {
                      res.send('no');
                    }
                  }
                  else
                  {
                    res.send('no')
                  }
                })
               }
               else {
                 res.send('no')
               }
              }
              else
              {
                res.send(result)
              }

            }
          })
        })




        /*----------------------------------------RESUME CODE0--------------------------------------------------------- */
        route.post('/addmm', function(req,res) {
          console.log(req.body.data)
                  var trans = new Resume(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });

          route.post('/editt', function(req,res) {
            Resume.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })

          });


      route.post('/fetchall',function(req,res) {
        console.log(req.body)
        let username;
        if(req.body.username)
        {
          username = req.body.username;
        }
        else{
          username=  req.user.username;
        }
          Resume.find({$and: [{action:req.body.action},{username:username}]}, function(err, docs){
            if(err)
            {

            }
            else
            {
              res.send(docs)
            }
        });
      })

      route.post('/fetcheditdata',function(req,res) {
        Resume.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
       });
      })

      route.post('/del',function(req,res) {
        Resume.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
          res.send("OK")
        });
      });



      route.post('/deletesuser',function(req,res){
        Suser.findOne({username:req.body.username},function(err,found){
          if(err)
          {

          }
          else if(found)
          {
            Suser.deleteOne({username:found.username},function(err,done)
            {
            })
            Resume.deleteMany({username:found.username},function(err,done)
            {
            })
            res.send("Deleted")
          }
        })
      })
      route.post('/update_student_password',function(req,res){
        Suser.findOne({username:req.body.student_username},function(err,found){
          if(err)
          {

          }
          else if(found)
          {
            let hash = bcrypt.hashSync(req.body.student_password, 10);
            Suser.updateOne({username:req.body.student_username},{password: hash} ,(err, done) => {
              if(err)
              {

              }
              else if(done)
              {
                res.send("Updated");
              }
            })
          }
          else
          {
            res.send("User Not Found !!")
          }
        })
      })


      route.post('/suspension_suser',function(req,res){
        console.log(req.body)
        Suser.findOne({username:req.body.suspend_student_username},function(err,found){
          if(err)
          {
            res.status(404).send("error!!")
          }
          else if(found)
          {
            AdminOrder.findOne({$and:[{victim_username:req.body.suspend_student_username},{order:'Suspend Suser'}]},function(err,obj){
              if(err)
              {

              }
              else if(obj)
              {
                res.send("Already Suspended !!")
              }
              else{
                var suspend = new AdminOrder({order:'Suspend Suser',victim_username:req.body.suspend_student_username,reason:req.body.suspend_reason_student,suspend_status:true});
                suspend.save((err, savedUser) => {
                  if(err)
                  {

                  }
                  else if(savedUser)
                  {
                    Suser.updateOne({username:req.body.suspend_student_username},{suspension_status: true},function(err,done){
                      if(err)
                      {

                      }
                      else if(done)
                      {
                        res.send("Suspended !!")
                      }
                    })
                  }
                })
              }
            })
          }
          else{
            res.send("User Not Found !!")
          }
        })
      })
      route.post('/remove_suser_suspension',function(req,res){
        console.log(req.body)
        Suser.findOne({username:req.body.suspend_student_username},function(err,found){
          if(err)
          {
            res.status(404).send("error!!")
          }
          else if(found)
          {
            AdminOrder.findOne({$and:[{victim_username:req.body.suspend_student_username},{order:'Suspend Suser'}]},function(err,obj){
              if(err)
              {

              }
              else if(obj)
              {
                Suser.updateOne({username:req.body.suspend_student_username},{suspension_status: false},function(err,done){
                  if(err)
                  {

                  }
                  else if(done)
                  {
                    obj.remove()
                    res.send("Removed Suspension !!")
                  }
                })
              }
              else{
                res.send("User Not Found !!")
              }
            })
          }
          else{
            res.send("User Not Found !!")
          }
        })
      })









      route.get('/know_no_of_suser', function(req, res) {
        Suser.find({ }, function(err, result){
          if(err)
          {

          }
          else if(result)
          {
            res.send(result)
          }
        })
      })

      route.post('/fetch_in_admin',function(req,res) {
        //console.log(req.body)
        Admin_Operation.find({$and:[{action: req.body.action},{usertype:req.body.usertype}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send(docs)
        }
        });
      })

      route.post('/fetch_in_admin_for_path',function(req,res) {
        console.log(req.body)
        Admin_Operation.find({$and:[{action: req.body.action},
          {usertype:req.body.usertype},{for:req.body.for}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
          console.log(docs)
        res.send(docs)
        }
        });
      })

      route.post('/del_inserted_data',function(req,res) {
        console.log(req.body)
      Admin_Operation.deleteOne({serial:  req.body.serial },function(err,succ){
      res.send("OK")
      });
      })

      route.post('/add_from_insert_in_admin', function(req,res) {
        //console.log(req.body.data)
            var respon = new Admin_Operation(req.body.data);
            respon.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
    });

    route.post('/edit_inserted_data', function(req,res) {
    Admin_Operation.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
    })

    });

    route.post('/fetch_for_edit',function(req,res) {
    Admin_Operation.findOne({serial: req.body.id}, function(err, docs){
    if(err)
    {

    }
    else
    {
    res.send(docs)
    }
    });
    })

    route.post('/entry_domain', function(req, res) {
      Arhi.findOne({$and:[{domain_name:req.body.domain},{dept:req.body.user.dept},{year:req.body.user.year},
        {campus:req.body.user.campus},{username:req.body.user.username},{action:'Fav-Domain'}]},(err,found)=>{
        if(err){

        }
        else if(found){
          res.send('have')
        }
        else{
          var trans = new Arhi({
            action:'Fav-Domain',
            username:req.body.user.username,
            domain_name:req.body.domain,
            dept:req.body.user.dept,
            year:req.body.user.year,
            campus:req.body.user.campus,
          });
          trans.save((err, saved) => {
            if(err){

            }
            else if(saved){
                res.send('done');
            }
          })
        }
      })

    })

    route.get('/fetch_domains', function(req, res) {
      Arhi.find({$and:[{username:req.user.username},{action:'Fav-Domain'}]},(err,found)=>{
        if(err)
        {

        }
        else if(found){
          res.send(found);
        }
      })
    })

  route.post('/fetchCompletationHistory', function(req, res) {
      Task.findOne({$and:[{task_name:'Task '+req.body.course},{username:req.user.username}]},(err,found)=>{
        if(err){

        }
        else if(found){

            Task.find({$and:[{task_name:'Task '+req.body.course},{username:req.user.username}]},(er,objs)=>{
              if(er){

              }
              else if(objs){
                res.send(objs);
              }
            })
        }
        else{
          res.send('No Data');
        }
      })
    })


/*Faculty List-----------------------------------------------------------------*/

    route.post('/add_faculty', function(req,res) {
    //  console.log(req.body)
       Faculty.findOne({$and:[{timing:req.body.timing},{username:req.user.username},{faculty_id:req.body.faculty_id},{current:true}]},(err,found)=>{
         if(err){

         }
         else if(found){
             var trans = new Faculty(req.body);
             trans.save((err, savedUser) => {
                 if(err){

                 }
                 else if(savedUser){
                                res.send('done');
                 }
             })

         }
         else
         {
           var user = new Faculty(req.body);
          user.save((err, savedUser) => {
               if(err){

               }
               else if(savedUser){
                  res.send('done');
               }
           })
         }
       })
    });

      route.post('/edit_faculty', function(req,res) {
      //  console.log(req.body)
        Faculty.updateOne({$and: [{ serial: req.body.serial},{username: req.user.username}]},req.body, (err, user) => {
          if(err){

          }
          else if(user){
            var succ= {
              succ: "Updated !!"
            };
            res.send(succ);
          }
        })

      });


  route.post('/fetchall_faculty',function(req,res) {
    let username;
    if(req.body.username)
    {
      username = req.body.username;
    }
    else {
      username = req.user.username;
    }
      Faculty.find({$and: [{timing:req.body.action},{username: username},{current:true}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
          res.send(docs)
        }
    });
  })

  route.post('/fetcheditdata_facultylist',function(req,res) {
    console.log(req.body)
    Faculty.findOne({$and: [{serial: req.body.id},{username: req.user.username},{current:true}]}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
   });
  })

  route.post('/del_faculty_list',function(req,res) {
    Faculty.deleteOne({$and: [{serial:  req.body.serial },{username: req.user.username},{current:true}]},function(err,succ){
      if(err){

      }
      else if(succ){
              res.send("OK")
      }
    });
  });


  route.post('/send_noti_on_class_complition',function(req,res) {
    var user = new Notification(req.body);
     user.save((err, savedUser) => {
          if(err){

          }
          else if(savedUser){
             res.send('done');
          }
      })
  });


  route.get('/fetchClass_Complete_History',function(req,res) {
    console.log('Here In')
    Notification.find({$and:[{action:'Class Completed'},{new:true}]},(err,found)=>{
      if(err){

      }
      else if(found){
        Faculty.find({$and:[{username:req.user.username},{current:true}]},(er,objs)=>{
          if(er){

          }
          else if(objs)
          {
            var datas={
              completed:found,
              facultylist:objs,
            }
            res.send(datas)
          }
        })
      }
      else
      {
        res.send('not found')
      }
    })
  });


  route.post('/updateValue',function(req,res) {
    //console.log(req.body)
  Notification.updateOne({$and:[{new:true},{faculty_id:req.body.faculty_id},
    {serial:req.body.reference_number},
    {action:'Class Completed'},{for_year:req.body.for_year},{for_batch:req.body.for_batch},
    {for_sem:req.body.for_sem},
    {date:req.body.date},{week:req.body.week},{month:req.body.month},{year:req.body.year}]},{new:false},
    (er,done)=>{
      if(er){

      }
      else if(done)
      {
        console.log('Find')
        StringMatching(req.body.covered_fac.toLowerCase(),req.body.covered.toLowerCase(),req,res);
      }
      else
      {
        res.send('done')
      }
    })
  });

  function StringMatching(covered_fac,covered_s,req,res)
  {
      console.log('Sting Mathing...')

    let positive_count,s_feedback,percentage;
    if((covered_s.includes('mass bunk')) ||covered_s.includes('massbunk'))
      {
        positive_count=true;
        s_feedback=false;
        countPass(req,res,positive_count,s_feedback);
      }
    else if((covered_s.includes('not completed'))||(covered_s.includes('notcompleted'))
    ||(covered_s.includes('not handled'))||(covered_s.includes('nothandled'))
    ||(covered_s.includes('not handle'))||(covered_s.includes('nothandle'))
    ||(covered_s.includes('not taken'))||(covered_s.includes('nottaken'))
    ||(covered_s.includes('was cancelled'))||(covered_s.includes('wascancelled'))
    ||(covered_s.includes('has been cancelled'))||(covered_s.includes('hasbeencancelled'))
    ||(covered_s.includes('has cancelled'))||(covered_s.includes('hascancelled'))
    ||(covered_s.includes('had cancelled'))||(covered_s.includes('hadcancelled'))
    ||(covered_s.includes('not take'))||(covered_s.includes('nottake'))
    ||((covered_s.includes('not present')) && (covered_s.includes('sir')))
    ||((covered_s.includes('notpresent')) && (covered_s.includes('sir'))))
            {
              positive_count=false;
              s_feedback=true;
              countPass(req,res,positive_count,s_feedback);
            }
    else if((covered_s.includes('absent')) || (covered_s.includes('notpresent')) || (covered_s.includes('not present')) ||
    (covered_s.includes('apsent')))
          {
            positive_count=true;
            s_feedback=true;
            countPass(req,res,positive_count,s_feedback);
          }
          else
          {
            percentage = (stringSimilarity.compareTwoStrings(covered_fac,covered_s))*100;
            if(percentage>=40)
            {
              positive_count=true;
              s_feedback=true;
            }
            else {
              positive_count=false;
              s_feedback=true;
            }
            countPass(req,res,positive_count,s_feedback);
          }
  }


  function countPass(req,res,positive_count,s_feedback)
  {
    AddStudentSubmission(req,res,positive_count,s_feedback);
  }

  function AddStudentSubmission(req,res,positive_count,s_feedback)
  {
    console.log('Add Student AddStudentSubmission')
  console.log(req.body)
    var urr = new StudentSubmission(req.body);
     urr.save((error, savedUser) => {
          if(error)
          {
              console.log('Error2')
          }
          else if(savedUser)
          {
            console.log('Hey')
            //console.log(req.body)
            //console.log(req.body.day_order+'.'+req.body.hour)
            Faculty.find({$and:[{faculty_id:req.body.faculty_id},
            {timing:req.body.day_order+'.'+req.body.hour},
            {batch:req.body.for_batch},{sem:req.body.for_sem},{year:req.body.for_year}]},(err,objs)=>{
              if(err){
                console.log('Error1')
              }
              else if(objs)
              {
                console.log('Here')
                var datas={
                  updateStatus:'done',
                  total_student:objs,
                  positive_count:positive_count,
                  s_feedback:s_feedback,
                }
                res.send(datas)
              }
              else {
                console.log('Ooops!!')
              }
            })
          }
      })
  }


  route.post('/fetch_student_list_under_faculty_adviser', (req, res) => {
    //console.log(req.body)
       Suser.find({$and:[{active:true},{campus:req.body.user.campus},{dept:req.body.user.dept},
         {faculty_adviser_id:req.body.user.username}]}, (err, user) => {
         if(err)
         {

         }
         else if(user)
         {
           res.send(user);
         }
       })
    })

  route.post('/fetch_signup_request_for_admin', (req, res) => {
    //console.log(req.body)
       Suser.find({active:false}, (err, user) => {
         if(err)
         {

         }
         else if(user)
         {
           if(!(req.body.user))
           {
             res.send(user)
           }
           else
           {
             var list = user.filter(item=>((item.campus === req.body.user.campus)
              && (item.dept === req.body.user.dept) && (item.year === req.body.user.faculty_adviser_year)));
              res.send(list);
           }
         }
       })
    })

    route.post('/active_user', (req, res) => {
     console.log(req.body)
         Suser.updateOne({username:req.body.content.username},
           {$set:{active:true}}, (err, user) => {
           if(err)
           {

           }
           else if(user)
           {
             let content = 'This mail is regarding the activation of your account. This is to inform you that your account has been activated by our CARE team. You can now proceed with login.'
             mail_send(req.body.content.mailid,content,res,req.body.content.name);
           }
         })
      })

      route.post('/fetch_student_on_a_skill', (req, res) => {
        console.log(req.body)
        let search_s;
        if(req.body.filter_data.f_dept)
        {
          search_s ={$and:[{dept:req.body.filter_data.f_dept},{action:'Skills'},{skill_name:req.body.skill}]};
        }
        else if (req.body.filter_data.f_year) {
          search_s ={$and:[{year:req.body.filter_data.f_year},
            {action:'Skills'},{skill_name:req.body.skill}]};
        }
        else if((req.body.filter_data.f_dept) && (req.body.filter_data.f_year))
        {
          search_s ={$and:[{year:req.body.filter_data.f_year},{dept:req.body.filter_data.f_dept},{action:'Skills'},{skill_name:req.body.skill}]};
        }
        else{
          search_s ={$and:[{action:'Skills'},{skill_name:req.body.skill}]};
        }
           Resume.find(search_s,(err, data) => {
             if(err)
             {

             }
             else
             {
               let search;
               if(req.body.filter_data.f_dept)
               {
                 search ={dept:req.body.filter_data.f_dept};
               }
               else if (req.body.filter_data.f_year) {
                 search_s ={year:req.body.filter_data.f_year};
               }
               else if((req.body.filter_data.f_dept) && (req.body.filter_data.f_year))
               {
                 search_s ={$and:[{year:req.body.filter_data.f_year},{dept:req.body.filter_data.f_dept}]};
               }
               else{
                 search ={};
               }
               Suser.find(search,(err1,users)=>{
                 if(err1)
                 {

                 }
                 else
                 {
                   var datas={
                     data:data,
                     users:users,
                   }
                   res.send(datas)
                 }
               })
             }
           })
        })



        route.post('/fetch_student_on_a_domain', (req, res) => {
          let search_s;
          if(req.body.filter_data.f_dept)
          {
            search_s ={$and:[{dept:req.body.filter_data.f_dept},{action:'Fav-Domain'},{domain_name:req.body.domain}]};
          }
          else if (req.body.filter_data.f_year) {
            search_s ={$and:[{year:req.body.filter_data.f_year},
              {action:'Fav-Domain'},{domain_name:req.body.domain}]};
          }
          else if((req.body.filter_data.f_dept) && (req.body.filter_data.f_year))
          {
            search_s ={$and:[{year:req.body.filter_data.f_year},{dept:req.body.filter_data.f_dept},{action:'Fav-Domain'},{domain_name:req.body.domain}]};
          }
          else{
            search_s ={$and:[{action:'Fav-Domain'},{domain_name:req.body.domain}]};
          }
             Arhi.find(search_s,(err, data) => {
               if(err)
               {

               }
               else
               {
                 let search;
                 if(req.body.filter_data.f_dept)
                 {
                   search ={dept:req.body.filter_data.f_dept};
                 }
                 else if (req.body.filter_data.f_year) {
                   search_s ={year:req.body.filter_data.f_year};
                 }
                 else if((req.body.filter_data.f_dept) && (req.body.filter_data.f_year))
                 {
                   search_s ={$and:[{year:req.body.filter_data.f_year},{dept:req.body.filter_data.f_dept}]};
                 }
                 else{
                   search ={};
                 }
                 Suser.find(search,(err1,users)=>{
                   if(err1)
                   {

                   }
                   else
                   {
                     var datas={
                       data:data,
                       users:users,
                     }
                     res.send(datas)
                   }
                 })
               }
             })
          })

          route.post('/check_student', (req, res) => {
          console.log(req.body)
               Suser.findOne({$and:[{username:req.body.regid},{active:true}]},(err, data) => {
                 if(err)
                 {

                 }
                 else if(data)
                 {
                   res.send(data)
                 }
                 else {
                   res.send('no')
                 }
               })
            })

            route.post('/sendMessage_from_UserSide', (req, res) => {
              console.log(req.body)
               if(req.body.mail_send)
               {
                 let content = 'This mail is from '+req.body.user_session.name+' [Faculty Id :'+req.body.user_session.username+'] '+' of '+
                 req.body.user_session.dept+' department.<b><br />Message content is - </b>'+req.body.message;
                 mail_send(req.body.user.mailid,content,res);
               }
               else
               {
                  var notifcation = new Notification({
                    action:'Call From User',
                    for:req.body.user.username,
                    subject:'A message From '+req.body.user_session.name+' [Faculty Id :'+req.body.user_session.username+'] '+' of '+
                    req.body.user_session.dept+' department.',
                    details:req.body.message,
                  });
                  notifcation.save((err, saved) => {
                    if(err){

                    }
                    else if(saved){
                        res.send('ok');
                    }
                    else {
                      res.send('no')
                    }
                  })
               }
              })

              route.post('/entry_of_blueprint',function(req,res) {
                Request.findOne({$and:[{action:'For Blue Print Render'},
                {username: req.user.username},{expired:false}]}, function(err, docs){
                if(err)
                {
                  console.log(err)
                }
                  else if(docs)
                  {
                    Request.updateOne({$and:[{action:'For Blue Print Render'},
                    {username: req.user.username},{expired:false}]}, function(err1, docs1){
                      if(err1)
                      {

                      }
                      else if (docs1) {
                        res.send('We have sent your request!!');
                      }
                    })
                  }
                else
                {
                  var requests = new Request(req.body);
                  requests.save((err, savedUser) => {
                    if(err)
                    {

                    }
                    else if(savedUser)
                    {
                    res.send('We have sent your request!!');
                    }
                  })
                }
               });
              })

              route.post('/fetch_req_on_blueprint_render', (req, res) => {
                  Request.find({$and:[{expired:false},
                    {action:'For Blue Print Render'}]},(err,found)=>{
                    if(err)
                    {

                    }
                    else
                    {
                      res.send(found)
                    }
                  })
                })

                route.post('/fetchStudents_UnderFaculty',(req,res)=>{
                  console.log(req.body)
                  Faculty.find({$and:[{faculty_id:req.body.username},
                    {year:req.body.year},{current:true}]},(err,found)=>{
                    if(err)
                    {

                    }
                    else
                    {
                      console.log(found)
                      res.send(found);
                    }
                  })
                })

                route.post('/allow_blueprint_req', (req, res) => {
                   Request.updateOne({$and:[{username:req.body.content.username},{status:'pending'},{expired:false},
                   {action:'For Blue Print Render'}]},{$set:{status:'accepted'}},(err,done)=>{
                     if(err)
                     {

                     }
                     else if (done) {
                       Suser.updateOne({username:req.body.content.username},{$set:{render_timetable:true}},(err1,done1)=>{
                         if(err1)
                         {

                         }
                         else if (done1)
                         {
                           setTimeout(function()
                           {
                             expireIt(req);
                           }, 172800040);
                           res.send('done')
                         }
                         else {
                          res.send('no')
                         }
                       })
                     }
                     else {
                       res.send('no')
                     }
                   })
                })

                function expireIt(req)
                {
                  Request.updateOne({$and:[{username:req.body.content.username},{status:'pending'},{expired:false},
                  {action:'For Blue Print Render'}]},{$set:{expired:true}},(err,done)=>{
                    if(err)
                    {

                    }
                    else if (done) {

                    }
                  })
                }

                route.get('/fetch_notification', function(req, res) {
                  Notification.find({$and:[{for:req.user.username}]}, function(err, passed){
                    if(err)
                    {

                    }
                    else if(passed)
                    {
                      res.send(passed)
                    }
                  })
                  });

                  route.get('/fetch_new_notification', function(req, res) {
                    if(req.user)
                    {
                      Notification.find({$and:[{new:true},{for:req.user.username}]}, function(err, passed){
                        if(err)
                        {

                        }
                        else if(passed)
                        {
                          res.send(passed)
                        }
                      })
                     }
                     else{
                       res.send('no_u')
                     }
                    });

                  route.post('/mark_as_read', function(req, res) {
                    Notification.updateMany({$and:[{new:true},{for:req.user.username}]},{new:false}, function(err, passed){
                      if(err)
                      {

                      }
                      else if(passed)
                      {
                        res.send('ok')
                      }
                    })
                    });

                  route.post('/clear_one_notification', function(req, res) {
                    Notification.deleteOne({$and:[{for:req.user.username},{serial:req.body.id}]},function(err,succ){
                      if(err)
                      {

                      }
                      else if(succ)
                      {
                        res.send('done')
                      }
                    });
                    });

                    route.post('/clear_all_notification', function(req, res) {
                      Notification.deleteMany({for:req.user.username},function(err,succ){
                        if(err)
                        {

                        }
                        else if(succ)
                        {
                          res.send('done')
                        }
                      });
                      });



      function mail_send(mailid,content,res)
      {
        var transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
                 user: 'ework.care@gmail.com',
                 pass: 'uolqdatwxgbhqals'
             }
         });
             const mailOptions = {
              from: 'ework.care@gmail.com', // sender address
              to: mailid, // list of receivers
              subject: 'eWork - A message from eWork Team', // Subject line
              html: '<p>Dear Student,</p><br /><p>'+content+'</p><br />Thank You.'
            };
            transporter.sendMail(mailOptions, function (err, info) {
              if(err)
              {

              }
              else
              {
                res.send('ok');
              }
           });
      }






module.exports = route
