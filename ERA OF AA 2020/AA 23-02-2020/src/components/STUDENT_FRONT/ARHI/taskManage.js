import React, { Component } from 'react';
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import { Redirect } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class TaskManage extends Component {
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      noti_route:false,
      skills:[],
      div_no:'',
      skill_name:'',
      loader:true,
      redirectTo:'',
      render_detail:false,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    if(this.props.prop_data)
    {
      this.setState({username:this.props.prop_data.data.username,loading:false})
      this.fetchSkills(this.props.prop_data.data.username);
    }
    else
    {
         this.getUser();
    }
  }
  getUser=()=> {
    axios.get('/ework/user2/getstudent').then(response=>{
      if (response.data.user){
         this.fetchSkills()
      }
      else
      {
        this.setState({loader:false,redirectTo:'/ework/student'});
        window.M.toast({html: 'Not Logged In !!',classes:'rounded #ec407a pink lighten-1'});
      }
        })
      }


  fetchSkills =(username)=>{
    axios.post('/ework/user2/fetchall',{action:'Skills',username:username})
    .then( res => {
        if(res.data)
        {
          this.setState({skills:res.data,loader:false})
        }
    });
  }

setDivOn =(div)=>{
  this.setState({div_no:div})
}

showDetails =(skill_name)=>{
  this.setState({
    render_detail:!this.state.render_detail,
    skill_name:skill_name,
  })
}

  render(){
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="5" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="73" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="140" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="210" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="280" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="350" y="35" rx="0" ry="0" width="45" height="18" />

        <rect x="5" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="73" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="140" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="210" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="280" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="350" y="75" rx="0" ry="0" width="45" height="18" />

      </ContentLoader>
      )
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else
     {
       if(this.state.loader)
       {
         return(
           <MyLoader />
         );
       }
       else {
          return(
            <React.Fragment>
            {!(this.props.prop_data) && <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
            }
                {this.state.render_detail &&
                  <ShowDetails  username={this.state.username} prop_data={this.props.prop_data}
                  render_detail={this.state.render_detail} detail={this.showDetails} skill_name={this.state.skill_name} />}
              <h4 className="center">Task-Manager</h4>
              <div className="row" style={{padding:'10px'}}>
              {this.state.skills.map((content,index)=>(
                <React.Fragment key={index}>
                <div className="col l2 xl2 m3 s4 go"  onClick={()=>{this.showDetails(content.skill_name)}} >
                  <h6 className="center card" style={{height:'50px',paddingTop:'15px'}}>{content.skill_name}
                  </h6>
                </div>
                </React.Fragment>
              ))}
              </div>
            </React.Fragment>
          );
        }
   }
  }
}

class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_completed:'',
      total_request:'',
      message:'',
      mailid:'',
      isChecked:'Begineer',
      active:'Begineer',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "pink go white-text";
      }
      return "go";
  }
  totalSet =(object)=>{
    this.setState(object);
  }

  render() {
    return (
      <Dialog fullScreen open={this.props.render_detail} onClose={this.props.detail} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2} sm={2}>
                  <IconButton edge="start" color="inherit" onClick={this.props.detail} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8} sm={8}>
                 <div style={{textAlign:'center',fontSize:'25px'}}>Complete Details of  <span className="yellow-text">{this.props.skill_name}</span></div>
               </Grid>
               <Grid item xs={2} sm={2}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List style={{marginTop:'70px'}}>
          <div className="row">
              <div className="col l3">
                 <div className="row div-style">
                   <div className={'col l4 center particular '+this.color('Begineer')} onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</div>
                   <div className={'col l4 center particular '+this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</div>
                   <div className={'col l4 center particular '+this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</div>
                 </div>
              </div>
              <div className="col l6"/>
              <div className="col l3">
              <div className="col l6">
                 <div className="card">
                 <div className="red white-text center">PENDING TASK</div>
                 <div className="card-content center">
                  <h6>{this.state.total_request}</h6>
                 </div>
                 </div>
              </div>
              <div className="col l6">
                 <div className="card">
                 <div className="green white-text center">COMPLETED</div>
                 <div className="card-content center">
                  <h6>{this.state.total_completed}</h6>
                 </div>
                 </div>
              </div>
              </div>
          </div>
          <div className="row">
              <TaskHistory  prop_data={this.props.prop_data} level={this.state.isChecked} username={this.props.username} skill_name={this.props.skill_name} totalSet={this.totalSet} />
          </div>
      </List>
    </Dialog>
    );
  }
}




class TaskHistory extends Component {
  constructor()
  {
    super()
    this.state={
      tasks:[],
      completed_tasks:[],
      loading:true,
      display_on:false,
      delete_key:0,
      confirm_dialog:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTasksFromAdmin()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.level !== this.props.level) {
      this.fetchTasksFromAdmin();
    }
  }

  fetchTasksFromAdmin =()=>{
    axios.post('/ework/user2/fetch_in_admin',{usertype:this.props.level,action:'Task '+this.props.skill_name})
    .then( res => {
        if(res.data)
        {
          this.fetchCompletedHistory(res.data)
        }
    });
  }
  fetchCompletedHistory = (adminTasks) =>{
    axios.post('/ework/user2/fetchTasks',{level:this.props.level,action:'Task '+this.props.skill_name,
    username:this.props.username})
    .then( res => {
        if(res.data)
        {

          const tasks = adminTasks.filter(item =>{
            return !res.data.find(citem => citem.referTask === item.serial);
          });

          this.setState({
            tasks:tasks,
            completed_tasks:res.data,
            loading:false,
          })
          this.props.totalSet({
            total_request:tasks.length,
            total_completed:res.data.length,
          })
        }
    });
  }

  callEdit =(key) =>{
    const referTaskDetails = this.state.completed_tasks.filter(item => item.referTask=== key);
    this.setState({
      display_on:!this.state.display_on,
      referTaskDetails:referTaskDetails,
    })
  }

  confirmDelete=(key)=>{
    this.setState({confirm_dialog:!this.state.confirm_dialog,delete_key:key})
  }

  deleteTask=()=>{
    window.M.toast({html: 'Deleting !!', classes:'rounded orange'});
    const { completed_tasks } = this.state;
    axios.post('/ework/user2/deleteTask',{key:this.state.delete_key,task_name:this.props.skill_name})
    .then(res=>{
      if(res.data){
        this.setState({
            completed_tasks: completed_tasks.filter(items => items.referTask !== this.state.delete_key)
        })
        this.confirmDelete();
        window.M.toast({html: 'Deleted !!', classes:'rounded red'});
        this.fetchTasksFromAdmin();
      }
    })
  }

  handleExpand=()=>{
    this.setState({isExpanded:!this.state.isExpanded})
  }

  render() {
    if(this.state.loading)
    {
      return(
        <div className="center">
            <div className="preloader-wrapper big active">
            <div className="spinner-layer spinner-red">
                <div className="circle-clipper left">
                 <div className="circle"></div>
                </div><div className="gap-patch">
                 <div className="circle"></div>
                </div><div className="circle-clipper right">
                 <div className="circle"></div>
                </div>
            </div>
            </div>
        </div>
      )
    }
    else
    {
    return (
      <React.Fragment>

          <Dialog
            open={this.state.confirm_dialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title" style={{textAlign:'center'}}>Confirmation of Deletion</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                  Deleting this data may cause your rating to be decreased in this particular skill. So take decision
                  accordingly.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.confirmDelete} color="primary">
                Disagree
              </Button>
              <Button onClick={this.deleteTask} color="primary" autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>

      {this.state.display_on &&
        <FloatingWindow display_on={this.state.display_on} fetchTasksFromAdmin={this.fetchTasksFromAdmin}
        callEdit={this.callEdit} referTaskDetails={this.state.referTaskDetails} level={this.props.level} skill_name={this.props.skill_name}
        />
      }
      {this.props.level &&
        <React.Fragment>
          {this.state.tasks.length>0 ?
        <div>
        <div className="row">
          <div className="col l1 xl1 s1 m1 center"><b>Task-No</b></div>
          <div className="col l5 xl5 m6 s6 center"><b>Description of the Task</b></div>
          <div className="col l4 xl4 s3 m3 center"><b>Link to Refer</b></div>
           <div className="col l2 xl2 s2 m2 center"><b>Status</b></div>
        </div>
        <hr />
        {this.state.tasks.map((content,index)=>(
          <React.Fragment key={index}>
           <div className="row">
             <div className="col l1 xl1 s1 m1 center">T{content.serial}</div>
             <div className="col l5 xl5 m6 s6 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.description_of_the_task}</div>
             <div className="col l4 xl4 s3 m3 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.ref_for_task}</div>
              <div className="col l2 xl2 s2 m2 center">
              {!(this.props.prop_data) &&
                <Complete fetchTasksFromAdmin={this.fetchTasksFromAdmin}
                level={this.props.level} skill_name={this.props.skill_name}
                serial={content.serial} />
              }
              </div>
           </div>
             <hr />
          </React.Fragment>

        ))}
        </div>
        :
        <h5 className="center">No Pending Task Found !!</h5>
      }

      <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand}>
        <ExpansionPanelSummary
        style={{borderRadius:'20px'}}
          expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
          className="mine-collap"
        >
          <div style={{color:'yellow',fontSize:'25px'}}>COMPLETED TASKS</div>
                </ExpansionPanelSummary>


                {this.state.completed_tasks.length>0 ?
                <div style={{padding:'15px'}}>
                  <div className="row">
                    <div className="col l1 xl1 s1 m1 center"><b>Task-No</b></div>
                    <div className="col l4 xl4 m6 s6 center"><b>Description</b></div>
                    <div className="col l3xl3 s3 m3 center"><b>Links</b></div>
                    <div className="col l3xl3 s3 m3 center"><b>Duration</b></div>
                     <div className="col l1 xl1 s2 m2 center"><b>Action</b></div>
                  </div>
                  <hr />
                    {this.state.completed_tasks.map((content,index)=>(
                      <React.Fragment key={index}>
                       <div className="row">
                         <div className="col l1 xl1 s1 m1 center">T{content.referTask}</div>
                         <div className="col l4 xl4 m6 s6 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.description}</div>
                         <div className="col l3 xl3 s3 m3 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.link}</div>
                         <div className="col l3 xl3 s3 m3 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.start_date}-{content.end_date}</div>
                          <div className="col l1 xl1 s2 m2">
                          {!(this.props.prop_data) &&
                              <React.Fragment>
                                 <i className="material-icons small go green-text" onClick={()=>this.callEdit(content.referTask)}>edit</i>
                                 <i className="material-icons small go red-text" onClick={()=>this.confirmDelete(content.referTask)}>delete</i>
                              </React.Fragment>
                           }
                          </div>
                       </div>
                         <hr />
                      </React.Fragment>

                    ))}
                </div>
                :
                  <h5 className="center">No Completed Task Found !!</h5>
                }
        </ExpansionPanel>
        </React.Fragment>
      }

      </React.Fragment>
    );
   }
  }
}


class Complete extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display_overlay:false,
      level:'',
      skill_name:'',
      serial:'',
    }
  }

displayChanges=()=>{
  this.setState({
    display_overlay:!this.state.display_overlay,
  })
}
  handleChange =(level,skill_name,serial) =>{
    this.displayChanges();
    this.setState({
      level:level,
      skill_name:skill_name,
      serial:serial,
    })
  }
  render()
  {
    return(
      <React.Fragment>

        {this.state.display_overlay && <FloatingWindow display_overlay={this.state.display_overlay} displayChanges={this.displayChanges} fetchTasksFromAdmin={this.props.fetchTasksFromAdmin} handleChange={this.handleChange}
          level={this.state.level} skill_name={this.state.skill_name} serial={this.state.serial}/>
        }
        <p>
          <label>
           <input type="checkbox" className="filled-in" checked={this.state.blue_allot} value={this.state.blue_allot} onChange={() => this.handleChange(this.props.level,this.props.skill_name,this.props.serial)}/>
           <span className="black-text">Completed ?</span>
          </label>
        </p>
      </React.Fragment>
    )
  }
}

class  FloatingWindow extends Component {
  constructor()
  {
    super()
    this.state={
      start_date:'',
      end_date:'',
      link:'',
      description:'',
      level:'',
      skill_name:'',
      serial:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.putData()
  }
  putData =()=>{
    if(this.props.referTaskDetails)
    {
      this.setState({
        start_date:this.props.referTaskDetails[0].start_date,
        end_date:this.props.referTaskDetails[0].end_date,
        description:this.props.referTaskDetails[0].description,
        link:this.props.referTaskDetails[0].link,
        level:this.props.level,
        skill_name:this.props.skill_name,
        serial:this.props.referTaskDetails[0].referTask,
      })
    }
    else
    {
      this.setState({
        level:this.props.level,
        skill_name:this.props.skill_name,
        serial:this.props.serial,
      })
    }
  }

  retriveData =(e)=>{
    this.setState({[e.target.name]:e.target.value})
  }
  submitTask =(e)=>{
    e.preventDefault()
    if((!this.state.start_date) || (!this.state.end_date) || (!this.state.description))
    {
        window.M.toast({html: 'Fill up the required fileds !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    }
    else{
      axios.post('/ework/user2/send_task',{
        start_date:this.state.start_date,
        end_date:this.state.end_date,
        description:this.state.description,
        level:this.state.level,
        task_name:this.state.skill_name,
        link:this.state.link,
        referTask:this.state.serial,
      })
      .then(res=>{
         if(res.data === 'done')
         {
           window.M.toast({html: 'Saved !!',outDuration:'9000', classes:'rounded green darken-1'});
           this.props.handleChange();
           this.props.fetchTasksFromAdmin();
         }
         else if(res.data === 'updated')
         {
           window.M.toast({html: 'Updated !!',outDuration:'9000', classes:'rounded green darken-2'});
           this.props.callEdit();
            this.props.fetchTasksFromAdmin();
         }
      })
    }
  }
  render()
  {
    var closeAction,openAction;

    if(this.props.callEdit)
    {
      closeAction=this.props.callEdit;
      openAction = this.props.display_on;
    }
    else{
      closeAction= this.props.displayChanges;
      openAction=this.props.display_overlay;
    }
    return(
      <React.Fragment>

      <Dialog open={openAction} onClose={closeAction} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Details of the solution on {this.props.skill_name} in {this.props.level} Level</DialogTitle>
        <DialogContent>
            <TextField
              id="filled-password-input"
              label="Description of your work"
              type="text"
              variant="filled"
              name="description"
              multiline
              fullWidth
              value={this.state.description}
              onChange={this.retriveData}
              required
            />
            <br /><br />
            <TextField
              id="filled-password-input"
              label="Start Date"
              type="text"
              variant="filled"
              multiline
              fullWidth
              value={this.state.start_date} name="start_date" onChange={this.retriveData}
              required
            />
            <br /><br />
            <TextField
              id="filled-password-input"
              label="End Date"
              type="text"
              variant="filled"
              multiline
              fullWidth
              value={this.state.end_date} name="end_date" onChange={this.retriveData}
              required
            />
            <br /><br />
            <TextField
              id="filled-password-input"
              label="Enter any link of your work"
              type="text"
              variant="filled"
              multiline
              fullWidth
              value={this.state.link}  name="link" onChange={this.retriveData}
              required
            />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeAction} color="primary">
            Cancel
          </Button>
          <Button onClick={this.submitTask} color="primary">
            Submit Data
          </Button>
        </DialogActions>
      </Dialog>
      </React.Fragment>
    )
  }
}
