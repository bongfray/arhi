import React, { Component } from 'react'
import axios from 'axios'
import {Link ,Redirect } from 'react-router-dom'
import Nav from '../dynnav'
import ContentLoader from "react-content-loader"


export default class ActualDash extends Component {

 constructor(props) {
   super(props);
   this.state = {
     id:'',
     up_username:'',
     up_name:'',
     up_phone:'',
     campus:'',
     dept:'',
     desgn:'',
     up_dob: '',
     new_password:'',
     sem:'',
     current_password:'',
     up_confirm_password:'',
     home:'/ework/student',
     logout:'/ework/user2/slogout',
     get:'/ework/user2/getstudent',
     nav_route:'/ework/user2/fetch_snav',
     noti_route:false,
     loading:true,
     redirectTo:null,
   }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.handleSub = this.handleSub.bind(this)
     this.handleNewP = this.handleNewP.bind(this)
     this.handleConfirmPassword = this.handleConfirmPassword.bind(this)
     this.handleCurrentPass = this.handleCurrentPass.bind(this)
 }
 componentDidMount() {
   axios.get('/ework/user2/getstudentprofile').then(response => {
     if(response.status === 200)
     {
       if(response.data.nologin)
       {
         window.M.toast({html: 'You are not loggedIn', classes:'rounded #ec407a pink lighten-1'});
         this.setState({redirectTo:'/ework/student',loading:false})
       }
       else
       {
       this.setState({
         loading:false,
         id: response.data.username,
         up_username: response.data.mailid,
         up_name: response.data.name,
         up_phone: response.data.phone,
         dept: response.data.dept,
         campus: response.data.campus,
         degree: response.data.degree,
         up_dob: response.data.dob,
         sem:response.data.sem,
       })
     }
     }
   })
}
handleSub(event) {
 event.preventDefault()
 if((this.state.up_confirm_password) || (this.state.new_password) || (this.state.current_password))
 {
   if((this.state.up_confirm_password) && (this.state.new_password) && (this.state.current_password))
   {
     if(this.state.up_confirm_password === this.state.new_password)
     {
       if((this.state.up_phone).length!==10){
         window.M.toast({html: 'Phone no is not of 10 digit !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
       }
       else{
  axios.post('/ework/user2/sprofile', {
    id: this.state.id,
    up_username: this.state.up_username,
    up_name: this.state.up_name,
    up_phone: this.state.up_phone,
    up_dob: this.state.up_dob,
    new_password: this.state.new_password,
    current_password: this.state.current_password,
  })
   .then(response => {
     //console.log(response)
     if(response.status===200){
       if(response.data.succ)
        {
          window.M.toast({html: 'Updated !!', classes:'rounded green darken-2'});
       }
       else if(response.data.fail)
       {
         window.M.toast({html: 'Kindly Match Your Current Password', outDuration:'4200', inDuration:'500', displayLength:'1500'});
         return false;
       }
     }
    }).catch(error => {
      window.M.toast({html: 'Error !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    })
   this.setState({
     up_name: this.state.up_name,
     up_username: this.state.up_username,
     up_phone: this.state.up_phone,
     campus: this.state.campus,
     dept: this.state.dept,
     up_dob:this.state.up_dob,
     degree: this.state.degree,
     up_confirm_password: '',
     current_password: '',
     new_password: '',
     })
   }
   }else{
     window.M.toast({html: 'New Password Does not Match',  displayLength:'1500'});
     this.setState({
       up_confirm_password: '',
       new_password: '',
       })
     return false;

   }
 }
   else{
     window.M.toast({html: 'Fill all the fields',});
     return false;
   }
 }
 else
 {
   if((this.state.up_phone).length!==10){
     window.M.toast({html: 'Phone no is not of 10 digit!!',classes:'rounded #ec407a pink lighten-1'});
   }
   else{
   axios.post('/ework/user2/sprofile', {
     up_username: this.state.up_username,
     up_name: this.state.up_name,
     up_phone: this.state.up_phone,
     up_dob: this.state.up_dob,
     new_password: this.state.new_password,
     up_confirm_password: this.state.up_confirm_password,
     current_password: this.state.current_password
   })
    .then(response => {
      console.log(response)
      if(response.status===200){
        if(response.data.succ)
         {
           window.M.toast({html: 'Updated !!', classes:'rounded green darken-2'});
        }
      }
     }).catch(error => {
       window.M.toast({html: 'Internal Error', classes:'rounded red'});
     })
    this.setState({
     up_name: this.state.up_name,
     id: this.state.id,
     up_username: this.state.up_username,
     up_phone: this.state.up_phone,
     campus: this.state.campus,
     dept: this.state.dept,
     up_dob:this.state.up_dob,
     degree: this.state.degree,
     up_confirm_password: '',
     current_password: '',
     new_password: '',
      })
    }
}
}
handleNewP= (e) => {
       this.setState({
           new_password: e.target.value
       });

   };
   handleConfirmPassword= (e) =>{
           this.setState({
               up_confirm_password: e.target.value
           });
       };
       handleCurrentPass = (e) =>{
               this.setState({
                   current_password: e.target.value
               });
           };


 render() {
   const MyLoader = () => (
     <ContentLoader
       height={160}
       width={400}
       speed={2}
       primaryColor="#f3f3f3"
       secondaryColor="#c0c0c0"
     >
       <rect x="170" y="30" rx="3" ry="3" width="70" height="10" />
       <rect x="10" y="60" rx="3" ry="3" width="70" height="10" />
       <rect x="90" y="60" rx="3" ry="3" width="90" height="10" />
       <rect x="220" y="60" rx="3" ry="3" width="70" height="10" />
       <rect x="300" y="60" rx="3" ry="3" width="90" height="10" />

       <rect x="10" y="90" rx="3" ry="3" width="70" height="10" />
       <rect x="90" y="90" rx="3" ry="3" width="90" height="10" />
       <rect x="220" y="90" rx="3" ry="3" width="70" height="10" />
       <rect x="300" y="90" rx="3" ry="3" width="90" height="10" />

       <rect x="10" y="120" rx="3" ry="3" width="70" height="10" />
       <rect x="90" y="120" rx="3" ry="3" width="90" height="10" />
       <rect x="220" y="120" rx="3" ry="3" width="70" height="10" />
       <rect x="300" y="120" rx="3" ry="3" width="90" height="10" />

     </ContentLoader>
   )
   if(this.state.loading)
   {
     return(
       <MyLoader />
     );
   }
   else if(!this.state.loading)
   {
     if (this.state.redirectTo) {
          return <Redirect to={{ pathname: this.state.redirectTo }} />
      } else {
       return (
         <React.Fragment>
         <Nav noti_route={this.state.noti_route} home={this.state.home}
         nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
         <div className="profile-root">
                 <div className="hoverable card">
                 <div className="">
                   <Link to="/ework/student"><h5 className="prof-heading center black-text">PROFILE DATA</h5></Link>
                 </div><br />
                       <div>
                        {this.renderCells()}
                       </div>
                 </div>
         </div>
         </React.Fragment>
         );
     }
   }
 }

 renderCells() {
   return(
     <div className="profile">

       <div name="phone" className="row">
           <div className="col l6 s12">
           <div className="row">
             <div className="col l4 m6 s6">
             <span className="tcr">Official ID</span>
             </div>
             <div className="col l8">
             <Cell value={ this.state.id }  />
             </div>
           </div>
           </div>
            <div className="col l6">
            <div className="row">
                <div className="col l4">
                  <span className="tcr">Official Mail:</span>
                </div>
                <div className="col l8">
                  <Cell value={ this.state.up_username } onChange={value => this.setState({up_username: value})} />
                </div>
            </div>
           </div>
        </div>

       <br />



       <div className="row">
           <div className="col l6">
           <div className="row">
             <div className="col l4">
             <span className="tcr">Name:</span>
             </div>
             <div className="col l8">
             <Cell value={ this.state.up_name } name="name"  onChange={value => this.setState({up_name: value})} />
             </div>
           </div>
           </div>
            <div className="col l6">
            <div className="row">
                <div className="col l4">
                  <span className="tcr">Mobile No:</span>
                </div>
                <div className="col l8">
                  <Cell value={ this.state.up_phone } name="phone" onChange={value => this.setState({up_phone: value})} />
                </div>
            </div>
           </div>
        </div>

        <br />

        <div className="row">
            <div className="col l6">
            <div className="row">
              <div className="col l4">
              <span className="tcr">Campus: </span>
              </div>
              <div className="col l8">
              <Cell value={ this.state.campus }  onChange={value => this.setState({campus: value})} />
              </div>
            </div>
            </div>
             <div className="col l6">
             <div className="row">
                 <div className="col l4">
                   <span className="tcr">Degree: </span>
                 </div>
                 <div className="col l8">
                   <Cell value={ this.state.degree } onChange={value => this.setState({degree: value})} />
                 </div>
             </div>
            </div>
         </div>

         <br />
         <div className="row">
             <div className="col l6">
             <div className="row">
               <div className="col l4">
               <span className="tcr">Department:</span>
               </div>
               <div className="col l8">
               <Cell value={ this.state.dept }  onChange={value => this.setState({dept: value})} />
               </div>
             </div>
             </div>
              <div className="col l6">
              <div className="row">
                  <div className="col l4">
                    <span className="tcr">Semester :</span>
                  </div>
                  <div className="col l8">
                    <Cell value={ this.state.sem } onChange={value => this.setState({up_dob: value})} />
                  </div>
              </div>
             </div>
          </div>
          <br />
          <div className="row div-reset">
              <div className="input-field col l4">
                <input id="curpass" type="password" className="validate" value={this.state.current_password} onChange={this.handleCurrentPass} required />
                <label htmlFor="curpass">Current Password</label>
              </div>
               <div className="input-field col l4">
                   <input id="newpass" type="password" className="validate" value={this.state.new_password} onChange={this.handleNewP} required />
                   <label htmlFor="newpass">New Password</label>
              </div>
              <div className="input-field col l4">
                  <input id="conpass" type="password" className="validate" value={this.state.up_confirm_password} onChange={this.handleConfirmPassword} required />
                  <label htmlFor="conpass">Confirm Password</label>
             </div>
           </div>

      <div className="row">

        <button className="waves-effect btn blue-grey darken-2 col s3 prof-submit right" onClick={this.handleSub}>Submit</button>
     </div>
     </div>
   );

 }
}

class Cell extends Component {

 constructor(props) {
   super(props);
   this.state = { editing: false };
 }

 render() {
   const name = this.props.name;
   const { value, onChange } = this.props;
   if(this.state.editing)
   {
     return(
       <div>
       <div className="col l8"><input className="values-to-show" ref='input' value={value} onChange={e => onChange(e.target.value)} onBlur={ e => this.onBlur()} /></div>
       <span><a className="btn left waves-effect btn  editable-save pink" href="#!" onBlur={ e => this.onBlur()}>Save</a></span>
       </div>

     );
   }
   else{
     if((name === "phone") || (name=== "name") || (name==="mail_id"))
     {
     return(
       <div>
       <div className="col l8">
       <span className="values-to-show">{value}</span>
       </div>
       <div className="col l4">
       <a href="#!" className="editable-edit waves-effect btn blue-grey darken-2 " onClick={() => this.onFocus()}>Edit</a>
       </div>
       </div>
     );
   }
   else{
     return(
       <div>
       <div className="col l6">
       <span className="values-to-show">{value}</span>
       </div>
       </div>
     );
   }
   }


 }

 onFocus() {
   this.setState({ editing: true }, () => this.refs.input.focus());
 }

 onBlur() {
   this.setState({ editing: false });
 }
}
