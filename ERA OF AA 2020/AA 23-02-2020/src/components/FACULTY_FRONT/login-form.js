import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from '../forget'
import axios from 'axios'
import Nav from '../dynnav'
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redirectTo: null,
            forgotroute:'/ework/user/forgo',
            home:'/ework/faculty',
            logout:'/ework/user/logout',
            get:'/ework/user/',
            nav_route: '/ework/user/fetchnav',
            display_welcomenote:false,
            display_nav:true,
            block_click:false,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)
    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'First Enter Details!!', classes:'rounded #f44336 red'});
          return false;
        }
        else{
          window.M.toast({html: 'Logging in...',classes:'rounded orange'});
          this.setState({block_click:true})
        axios.post('/ework/user/faclogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                // console.log(response)
                if (response.status === 200) {
                if(response.data.count===0)
                {
                  this.setState({
                      redirectTo: '/ework/fprofdata'
                  })
                }
                else if((response.data.count === 2)||(response.data.count ===1))
                {
                  this.setState({
                      redirectTo: '/ework/faculty'
                  })
                }
                else if(response.data.count === 3)
                {
                  this.setState({
                    redirectTo:'/ework/dadmin_panel'
                  })
                }
                else if(response.data.count === 4)
                {
                  this.setState({
                    redirectTo:'/ework/admin_panel'
                  })
                }
                window.M.toast({html: 'Logged In !!',classes:'rounded green darken-1'});
                this.setState({block_click:false})
                }
            }).catch(error => {
              this.setState({block_click:false})
              if(error.response.status === 401)
              {
                window.M.toast({html:error.response.data,classes:'rounded #f44336 red'});
              }
              else
              {
                window.M.toast({html: 'Internal Error !!',classes:'rounded #f44336 red'});
              }
            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }



    getUser = () => {
      axios.get('/ework/user/').then(response => {
        if (response.data.user) {
              if((response.data.user.count === 1) || (response.data.user.count === 0) || (response.data.user.count === 2))
              {
                this.setState({redirectTo:'/ework/faculty'})
              }
              else if((response.data.user.count === 3))
              {
                this.setState({redirectTo:'/ework/dadmin_panel'})
              }
              else if((response.data.user.count === 4))
              {
                this.setState({redirectTo:'/ework/admin_panel'})
              }
        }
          })
        }



        componentDidMount() {
            this.getUser();
        }

        Welcome =() =>{
          this.setState({
            display_nav:!this.state.display_nav,
            display_welcomenote:!this.state.display_welcomenote,
          })
        }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

            return (
              <React.Fragment>
              <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
              <div className="row">

             <div className="col l4 xl4">
             {this.state.display_nav &&
              <div onClick={this.Welcome} className="left animat bounce" style={{marginTop:'20px',cursor: 'pointer'}}>
               <i className="material-icons small">info_outline</i>
              </div>
             }
               <Dialog
                 open={this.state.display_welcomenote}
                 keepMounted
                 onClose={this.Welcome}
                 aria-labelledby="alert-dialog-slide-title"
                 aria-describedby="alert-dialog-slide-description"
               >
                 <DialogTitle id="alert-dialog-slide-title" align="center">Welcome !!</DialogTitle>
                 <DialogContent>
                   If you have signed up recently, in that case, your account is in verification stage now.
                   Kindly wait for the approve mail before proceeding for login. It may take time.
                    If you have not received any mail for a long time kindly contact SRM CARE.
                 </DialogContent>
                 <DialogActions>
                   <Button onClick={this.Welcome} color="primary">
                     Close
                   </Button>
                 </DialogActions>
               </Dialog>
             </div>

             <div className="col l4 s12 m12 form-signup">
                 <div className="ew center">
                     <Link to ="/ework/faculty"><h5 className="blue-grey darken-2 highlight">eWORK</h5></Link>
                     <h5 className="highlight black-text">LOG IN</h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="email" type="text" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <label htmlFor="email">Username</label>
                         <span className="helper-text" data-error="Please enter username !!" data-success="">Official Id or College Id</span>
                     </div>
                     <div className="input-field col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <label htmlFor="password">Password</label>
                         <span className="helper-text" data-error="Please enter password !!" data-success=""></span>
                     </div>
                     </div>
                     <div className="row">
                     <Link to="/ework/fsignup" className=" btn blue-grey darken-2 col s5 sup">Register</Link>
                     <div className="col s2"></div>
                     <button disabled={this.state.block_click} className=" btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <hr/>
                     <div className="row">
                        <Modal forgot={this.state.forgotroute}/>
                     </div>
                 </form>
             </div>

             <div className="col s4"/>

             </div>
             </React.Fragment>
           );
         }

    }
}

export default LoginForm
