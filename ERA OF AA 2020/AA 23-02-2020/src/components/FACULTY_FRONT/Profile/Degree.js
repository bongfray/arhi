import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './add';
import ProductList from './prod';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       degrees:'',
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:true,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  axios.get('/ework/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else
     {
       this.setState({
         redirectTo:'/ework/faculty',
       });
       window.M.toast({html: 'You are not Logged In !!',classes:'rounded #ec407a pink lighten-1'});
     }
   })
}

   onCreate = (e,index) => {
     this.setState({isAddProduct:true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user/editprofile1';
     } else {
       apiUrl = '/ework/user/profile1';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId)=> {
     axios.post('/ework/user/fetchtoedit',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  handleDegress =(e) =>{
    this.setState({degrees:e.target.value})
  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

 upStat =()=>{
   this.setState({disabled:false})
 }
   render() {
             var data = {
               fielddata: [
                 {
                   name: "College_Name",
                   placeholder: "Enter the College Name",
                   type: "text",
                   grid: "col l2 s2 m2 xl2 center"
                 },
                 {
                   name: "Start_Year",
                   placeholder: "Starting Year",
                   type: "number",
                   grid: "col l2 s2 m2 xl2 center"
                 },
                 {
                   name: "End_Year",
                   type: "number",
                   grid: "col l2 s2 m2 xl2 center",
                   placeholder: "Ending Year"
                 },
                 {
                   name: "Marks_Grade",
                   type: "text",
                   grid: "col l2 s2 m2 xl2 center",
                   placeholder: "Enter Your Grade/ Mark"
                 },

               ],
             };

  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div className="row">
  <div className="col l4" />
  <div className="col l4" style={{marginTop:'10px'}}>
    {this.props.reference === 'none' ?
    <h5 className="prof-heading center black-text">Your Degrees</h5>
    :
    <h5 className="prof-heading center black-text">Add Your Degrees</h5>
    }
  </div>
   <div className="col l4"/>
  </div>
  <div className="row">
  </div>
  <div className="row">
     <div className="red-text col l3 xl3 hide-on-mid-and-down" />
       <div className=" col l7 xl7 s12 m12 particular" style={{padding:'10px'}}>
          <div className="red-text">Choose the Name Of The Degree....</div>
               <FormControl style={{width:'100%'}}>
                 <InputLabel id="sel_degree">Choose your option</InputLabel>
                   <Select
                     labelId="sel_degree"
                     id="sel_degree"
                     value={this.state.degrees}
                     onChange={this.handleDegress}
                   >
                   <MenuItem value="" disabled defaultValue>Choose your option</MenuItem>
                   <MenuItem value="UG">UG</MenuItem>
                   <MenuItem value="PG">PG</MenuItem>
                   <MenuItem value="PHD">PHD</MenuItem>
                   <MenuItem value="POST-DOCTORATE">POST DOCTORATE</MenuItem>
                   </Select>
                </FormControl>
      </div>
      <div className="col l3 xl3 hide-on-mid-and-down" />
  </div><br /><br />

     {this.state.degrees &&
       <div className="particular" style={{margin:'15px'}}>
          <div className="row">
            <div className="col l1 s2 m2 xl1" style={{wordWrap: 'break-word'}}><b>Degree</b></div>
            <div className="col l1 s1 m1 xl1 center hide-on-small-only"><b>No.</b></div>
            <div className="col l2  s2 m2 xl2 center" style={{wordWrap: 'break-word'}}><b>College Name</b></div>
            <div className="col l2 s2 xl2 m2 center" style={{wordWrap: 'break-word'}}><b>Year of Starting</b></div>
            <div className="col l2 s2 xl2 m2 center" style={{wordWrap: 'break-word'}}><b>Year of Completion</b></div>
            <div className="col l2 s2 xl2 m2 center" style={{wordWrap: 'break-word'}}><b>Marks</b></div>
            <div className="col l2 xl2 s2 m2 center" style={{wordWrap: 'break-word'}}><b>Actions</b></div>
          </div>

      <hr />
              <div className="row ">
                {!this.state.isAddProduct && <ProductList
                  status={this.upStat} username={this.state.username}
                  title={this.state.degrees} action={this.state.degrees}
                  data={data}  editProduct={this.editProduct}/>}
                {!this.state.isAddProduct &&
                 <React.Fragment>
                   <button className="btn right blue-grey darken-2 subm" name={this.state.degrees} onClick={(e) => this.onCreate(e,data)}>Add Degree</button>
                </React.Fragment>
                }
                {(this.state.isAddProduct || this.state.isEditProduct) &&
                  <AddProduct cancel={this.updateState} username={this.state.username} action={this.state.degrees} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                }
              </div>
          </div>
        }
</React.Fragment>
);
}
}
}
