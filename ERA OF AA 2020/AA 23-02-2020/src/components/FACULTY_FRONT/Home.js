import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Nav from '../dynnav'
import ContentLoader from "react-content-loader"
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Fhome from '../fhome2.png'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class FrontPage extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      loader:true,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      login:'/ework/flogin',
      about_display:false,
      noti_route:true,
      user:'',
      display_policy:false,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/ework/user/')
  .then(response => {
      if (response.data.user === null) {
         this.setState({loader:false})
      }
      else{
        this.setState({display:"disabled",loader:false,user:response.data.user.username})
      }
        })

      }


    componentDidMount() {
        this.getUser();
    }
    about = () =>{
      this.setState({about_display:!this.state.about_display})
    }

    render(){
      const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="10" y="25" rx="0" ry="0" width="190" height="130" />
          <rect x="240" y="25" rx="0" ry="0" width="150" height="130" />
        </ContentLoader>
      )
      if(this.state.loader=== true)
      {
        return(
          <MyLoader />
        );
      }
      else{
        return(
          <React.Fragment>
          <Nav noti_route={this.state.noti_route} home={this.state.home} login={this.state.login} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          {this.state.about_display && <About about={this.about} about_display={this.state.about_display}
          user={this.state.user}/>}
            <div className="row">
                <div className="col l6 m12 s12">
                    <div className="fstyle">
                          <center><h4  className="ework_name">E-Work</h4></center><br /><br />
                          <p className="fpara">
                            Ework is a simplified analytics tool.
                            It is a tool to keep trace and record of each and everyday routine of staff members of the institute.
                            E-Work is a tool designed by CARE which majorly focuses on the encouragement of the well deserved ones.
                          </p><br/><br/>
                          <div className="row">
                                  <div className="col l1 m1 hide-on-down" />
                                  <div className="col l4 s12 m12">
                                     <Link to="/ework/flogin" style={{width:'100%'}} disabled={this.state.display} className="left waves-effect btn #03a9f4 light-blue"><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></Link>
                                  </div>
                                  <div className="col l2 m2 hide-on-down" />
                                  <div className="col l4 s12 m12">
                                      <Link to="#" style={{width:'100%'}} onClick={this.about} className="btn #c0ca33 lime darken-1 right"><i className="material-icons right">desktop_mac</i><b>About</b></Link>
                                  </div>
                                  <div className="col l1 m1 hide-on-down" />
                           </div>

                           <Button style={{float:'right',color:'white',padding:'30px'}} onClick={()=>this.setState({display_policy:true})}>
                             Privacy Policy
                           </Button>


                             <Dialog
                               open={this.state.display_policy}
                               TransitionComponent={Transition}
                               keepMounted
                               onClose={()=>this.setState({display_policy:false})}
                               aria-labelledby="alert-dialog-slide-title"
                               aria-describedby="alert-dialog-slide-description"
                             >
                               <DialogTitle id="alert-dialog-slide-title">Privacy Policy</DialogTitle>
                               <DialogContent>
                                 <DialogContentText id="alert-dialog-slide-description">
                                   This Section will be updated later
                                 </DialogContentText>
                               </DialogContent>
                               <DialogActions>
                                 <Button onClick={()=>this.setState({display_policy:false})} color="primary">
                                   Close
                                 </Button>
                               </DialogActions>
                             </Dialog>

                     </div>
                 </div>
                <div className="col l6 hide-on-med-and-down center">
                    <img className="center imgf" src={Fhome} alt=""/>
                </div>
            </div>
            </React.Fragment>
        );
      }
    }
}



class About extends Component {
  constructor()
  {
    super()
    this.state={
      isChecked:0,
      active:0,
    }
  }


  handleComp = (e) =>{
    this.setState({
      isChecked: e,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }
  toggleDiv =()=>{
    this.setState({toggled:!this.state.toggled})
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "center go active-pressed";
      }
      return "center go";
  }
  render() {
    return (
      <Dialog fullScreen open={this.props.about_display} onClose={this.props.about} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={3} sm={3}>
                  <IconButton edge="start" color="inherit" onClick={this.props.about} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid container item xs={6} sm={6}>
                      <Grid className={this.color(0)}  onClick={(e)=>{this.handleComp(0)}} item xs={4} sm={4}>ABOUT EWORK</Grid>
                      <Grid className={this.color(1)} onClick={(e)=>{this.handleComp(1)}} item xs={4} sm={4}>CONTACT US</Grid>
                      {this.props.user && <Grid className={this.color(2)} onClick={(e)=>{this.handleComp(2)}} item xs={4} sm={4}>COMPLAINTS</Grid>}
               </Grid>
               <Grid item xs={3} sm={3}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
            <div style={{marginTop:'40px'}}>
                <Propagation option={this.state.isChecked} user={this.props.user}/>
            </div>
          </List>
        </Dialog>
    );
  }
}




class Propagation extends Component {
  constructor()
  {
    super()
    this.initialState={
      complaint:'',
      message:'',
      from:'',
      subject:'',
      name:'',
      mailid:'',
      id:'',
      official_id:'',
      complaint_subject:'',
      message_from_admin:null,
      fresh:true,
    }
    this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setState({official_id:this.props.user})
  }
  sendMessage = (e) =>{
    e.preventDefault();
    if ((!this.state.name) || (!this.state.mailid) || (!this.state.subject) || (!this.state.message) )
    {
      window.M.toast({html: 'Enter the Details !!',classes:'rounded red'});
    }
    else{
      window.M.toast({html: 'Sending Your Mail ...',classes:'rounded pink'});
      axios.post('/ework/user/contactus',this.state)
      .then(res=>{
        if(res.data)
        {
          window.M.toast({html: 'We have send your query !!',classes:'rounded green darken-2'});
          this.setState(this.initialState)
        }
      })
    }
  }
  sendComplaint = (e) =>{
     e.preventDefault();
     if (!(this.state.mailid) || !(this.state.complaint_subject) || !(this.state.complaint))
     {
       window.M.toast({html: 'Enter the Details !!',classes:'rounded red'});
     }
     else{
     axios.post('/ework/user/complaint',{data:this.state})
     .then( res => {
         if(res.data === 'ok')
         {
             window.M.toast({html: 'Your Complaint has been sent !!',classes:'rounded green darken-2'});
             this.setState(this.initialState)
         }
     });
    }
  }
  intakeInput =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {
    if(this.props.option === 0)
    {
      return(
               <Grid container spacing={1}>
                    <Grid item xs={2} sm={2} />
                    <Grid item xs={8} sm={8}>
                        <div className="center ab-head">
                         About eWork
                       </div>
                       <Paper elevation={3} className="ab-content">
                        eWork is an online evaluation tool developed by the SRM CARE team. It does not focuses on any personal issues of any staff member. eWork is designed and developed to improve the self performance of both staff and non-staff members. The deserving ones are always praised in any organization, so keeping this in mind this tool is made to enhance the performance of the one who really work hard enough to contribute to the institute's image as well as in the growth.
                        eWork is the tool which analyzes day-to-day activity of the staff members and helps in giving them an opportunity in different ways to utilize their time towards institute's contribution as well as in personal growth in technical as well as non-technical side. The performance appraisal process in an organization can sometimes be unpopular with HR teams, line managers, and employees. eWork is designed in order to reduce the time taken and also to help the management team to gather the information on the ones who are really deserving for getting praised. It gives the flexibility to the Director to be in glimpse of everything going on in the campus. The Director can view and analyze the faculty's progression as well as can decide the method for appraisal. This tool only provides the way to the Director to view the datas and progression of the staff members, CARE Team is not involved in deciding the deserving staff members.
                        For any queries related to the application please contact CARE Team
                       </Paper>
                    </Grid>
                    <Grid item xs={2} sm={2} />
                </Grid>
      )
    }
    else if(this.props.option === 1)
    {
      return(
        <React.Fragment>
          <div className="row">
             <div className="col l3 xl3 hide-on-mid-and-down" />
             <div className="col l6 xl6 s12 m12 form-signup" style={{padding:'10px'}}>
                 <h6 className="center">Leave A Message</h6>
                 <div className="row">
                   <div className="col l12 xl12 s12 m12">
                     <div className="input-field">
                       <input type="text" className="validate" id="name" name="name" value={this.state.name} onChange={this.intakeInput} required/>
                       <label htmlFor="name">Enter Your Name</label>
                     </div>
                   </div>
                 </div>
                 <div className="row">
                   <div className="col l12 xl12 s12 m12">
                     <div className="input-field">
                       <input type="text" className="validate" id="mailid" name="mailid" value={this.state.mailid} onChange={this.intakeInput} required/>
                       <label htmlFor="mailid">Enter Your Official Mail Id</label>
                     </div>
                   </div>
                 </div>
                 <div className="row">
                   <div className="col l12 xl12 s12 m12">
                     <div className="input-field">
                       <input type="text" className="validate" id="id" name="id" value={this.state.id} onChange={this.intakeInput} required/>
                       <label htmlFor="id">Enter Your Official Id</label>
                     </div>
                   </div>
                 </div>
                 <div className="row">
                   <div className="col l12 xl12 s12 m12">
                     <div className="input-field">
                       <input type="text" className="validate" id="subject" name="subject" value={this.state.subject} onChange={this.intakeInput} required/>
                       <label htmlFor="subject">Specify the Subject of Your Query</label>
                     </div>
                   </div>
                 </div>
                 <div className="row">
                   <div className="col l12 xl12 s12 m12">
                       <TextField
                         id="outlined-multiline-static"
                         label="Enter the message"
                         multiline
                         rows="4"
                         fullWidth
                         name="message"
                         value={this.state.message}
                         onChange={this.intakeInput}
                         variant="filled"
                       />
                   </div>
                 </div>
                 <button className="right btn" onClick={this.sendMessage} style={{marginBottom:'5px'}}>SEND A MESSAGE</button>
         </div>
             <div className="col l3 xl3 hide-on-mid-and-down" />
          </div>
        </React.Fragment>
      )
    }
    else if(this.props.option === 2)
    {
    return (
      <div className="row">
        <div className="col l3 xl3 hide-on-mid-and-down" />
        <div className="col l6 xl6 s12 m12 form-signup">
            <h6 className="center">Leave A Message</h6>
            <div className="row">
              <div className="col l12 xl12 s12 m12">
                <div className="input-field">
                  <input type="email" className="validate" id="id" name="mailid" value={this.state.mailid} onChange={this.intakeInput} required/>
                  <label htmlFor="id">Enter Your Official Mail Id</label>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col l12 xl12 s12 m12">
                <div className="input-field">
                  <input type="text" className="validate" id="complaint_subject" name="complaint_subject" value={this.state.complaint_subject} onChange={this.intakeInput} required/>
                  <label htmlFor="complaint_subject">Enter the Subject of Your Complaint</label>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col l12 xl12 s12 m12">
                    <TextField
                      id="outlined-multiline-static"
                      label="Enter the Message"
                      multiline
                      rows="4"
                      fullWidth
                      name="complaint"
                      value={this.state.complaint}
                      onChange={this.intakeInput}
                      variant="filled"
                    />
                  </div>
                </div>
                <button className="right btn" onClick={this.sendComplaint} style={{marginBottom:'5px'}}>SEND</button>
        </div>
        <div className="col l3 xl3 hide-on-mid-and-down" />
      </div>
    );
  }
  }
}
