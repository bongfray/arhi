import React from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching:true,
      username:'',
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user/del_five_year_plan',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/ework/user/fetch_five_year_plan',{
    action: this.props.action
  })
  .then(response =>{
    this.setState({
     products: response.data,
     fetching:false,
   })
  })
}

  render() {
    if(this.state.fetching)
    {
      return(
        <div style={{textAlign:'center'}}>Loading.....</div>
      )
    }
    else{
      return(
        <div>
        <Grid container spacing={1}>
          <Grid item xs={1} sm={1}><b>No.</b></Grid>
          <Grid item xs={6} sm={6}><b>Plan</b></Grid>
          <Grid item xs={2} sm={2}><b>Status</b></Grid>
          <Grid item xs={3} sm={3}><b>Action</b></Grid>
        </Grid>

        {this.state.products.map((content,index)=>(
          <Grid container spacing={1} key={index}>
                <Grid item xs={1} sm={1}>{index+1}</Grid>
                <Grid item xs={6} sm={6}>{content.content}</Grid>
                <Grid item xs={2} sm={2}style={{marginTop:'-12px'}}>
                  <Complete content={content}/>
                </Grid>
                <Grid  item xs={3} sm={3}>
                   <EditIcon className="go"
                   onClick={() => this.props.editProduct(content.serial,this.props.action)}/>
                   &nbsp;
                   <DeleteIcon className="go"
                    onClick={() => this.deleteProduct(content.serial,this.props.action)}  />
                </Grid>
          </Grid>
        ))}
        </div>
      )
    }

  }
}


class Complete extends React.Component{
  constructor(props)
  {
    super(props)
    this.state={
      id:'',
      blue_allot:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user/fetch_complete_fiveyear',{serial:this.props.content.serial})
    .then( res => {
      if(res.data)
      {
        this.setState({blue_allot:res.data})
      }
    });
  }
  handleBlue_AllotChange =(e,index) =>{
      const value = e.target.name;
   this.setState({ blue_allot: !this.state.blue_allot, index,id:value});
   axios.post('/ework/user/update_status_fiveyear',{status:!this.state.blue_allot,key:value})
   .then( res => {

   });

  }
  render()
  {
    return(
      <React.Fragment>
        <p>
          <label>
           <input type="checkbox" className="filled-in" name={this.props.content.serial} checked={this.state.blue_allot} value={this.state.blue_allot} onChange={e => this.handleBlue_AllotChange(e, this.props.content.serial)}/>
           <span className="black-text">Completed ?</span>
          </label>
        </p>
      </React.Fragment>
    )
  }
}
