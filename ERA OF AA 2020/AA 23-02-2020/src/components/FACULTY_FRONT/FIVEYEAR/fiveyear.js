import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ContentLoader from "react-content-loader"
import Collapse from '@material-ui/core/Collapse';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import Nav from '../../dynnav'
import AddProduct from './add'
import ProductList from './prod'



class FiveYear extends Component{
    constructor(){
        super();
        this.state={
          isExpanded:false,
          refer_index:0,
          active:0,
          home:'/ework/faculty',
          logout:'/ework/user/logout',
          get:'/ework/user/',
          nav_route: '/ework/user/fetchnav',
          noti_route:true,
          username:'',
          redirectTo:'',
          loading:true,
          isAddProduct: false,
          response: {},
          action:'',
          product: {},
          isEditProduct: false,
          five:[{year:'Year - I',parts:[{name:'Administrative',index:'YearI-Administrative'},{name:'Academic',index:'YearI-Academic'},{name:'Research',index:'YearI-Research'}]},
          {year:'Year - II',parts:[{name:'Administrative',index:'YearII-Administrative'},{name:'Academic',index:'YearII-Academic'},{name:'Research',index:'YearII-Research'}]},
          {year: 'Year - III',parts:[{name:'Administrative',index:'YearIII-Administrative'},{name:'Academic',index:'YearIII-Academic'},{name:'Research',index:'YearIII-Research'}]},
          {year:'Year - IV',parts:[{name:'Administrative',index:'YearIV-Administrative'},{name:'Academic',index:'YearIV-Academic'},{name:'Research',index:'YearIV-Research'}]},
          {year:'Year - V',parts:[{name:'Administrative',index:'YearV-Administrative'},{name:'Academic',index:'YearV-Academic'},{name:'Research',index:'YearV-Research'}]}]
        }
    }
    componentDidMount(){
        axios.get('/ework/user/'
      )
         .then(response =>{
           if(response.data.user)
           {
             this.setState({username: response.data.user.username})
           }
           else{
             this.setState({
               redirectTo:'/ework/faculty',
             });
             window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
           }
                      this.setState({loading: false})
         })
    }


    onCreate = (e,index) => {
      this.setState({ isAddProduct: index ,product: {}});
    }
    onFormSubmit =(data) => {
      // console.log(data)
      let apiUrl;
      if(this.state.isEditProduct)
      {
        apiUrl = '/ework/user/edit_five_year_plan';
      }
       else
       {
        apiUrl = '/ework/user/add_five_year_plan';
       }
      axios.post(apiUrl,data)
          .then(response => {
            this.setState({
              response: response.data,
              isAddProduct: false,
              isEditProduct: false
            })
          })
    }

    editProduct = (productId,index)=> {
      axios.post('/ework/user/fetch_five_year_existing_data',{
        id: productId,
      })
          .then(response => {
            this.setState({
              product: response.data,
              isEditProduct: index,
              isAddProduct: index,
            });
          })

   }

   handleExpand =(e,serial,index) => {
     this.setState({isExpanded:!this.state.isExpanded,index,refer_index:serial});
   };

   updateState = () =>{
     this.setState({
       isAddProduct:'',
       isEditProduct:'',
     })
   }


    render(){
      const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="40" y="30" rx="0" ry="0" width="320" height="90" />
          <rect x="40" y="130" rx="0" ry="0" width="320" height="90" />

        </ContentLoader>
      )
      if(this.state.loading === true)
      {
        return(
          <MyLoader />
        );
      }
      else{
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return(
            <React.Fragment>
            <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

                   <Grid container spacing={1}>
                      <Grid item xs={4} sm={4}/>
                      <Grid item xs={4} sm={4}>
                        <h6 className="center prof-heading black-text">Your Plan For Next Five Year</h6>
                      </Grid>
                      <Grid item xs={4} sm={4}/>
                   </Grid>

                <Grid container spacing={1}>
                  <Grid item xs={1} sm={1} />
                  <Grid item xs={10} sm={10} >
                        {this.state.five.map((content,index)=>(
                          <Paper key={index} elevation={3} style={{paddingBottom:'2px'}}>
                              <p style={{backgroundColor:'#455a64',textAlign:'center',
                              color:'white',height: '45px',paddingTop: '12px'}}>{content.year}</p>

                              {content.parts.map((items,no)=>(
                                <Grid container style={{padding:'2px 10px 2px 10px '}}>
                                    <Paper style={{width:'100%',borderRadius:'15px'}}>
                                         <Grid container item xs={12} sm={12}>
                                            <Grid item xs={6} sm={6}>
                                                <div style={{padding:'2px 0px 0px 10px '}}>{items.name}</div>
                                            </Grid>
                                            <Grid item xs={6} sm={6}>
                                                  <IconButton
                                                   style={{float:'right'}}
                                                    onClick={(e)=>this.handleExpand(e,no,index)}
                                                    aria-expanded={this.state.isExpanded}
                                                    aria-label="show more"
                                                  >
                                                       {(this.state.isExpanded && (this.state.refer_index === no)) ?
                                                         <ExpandLessIcon />
                                                         :
                                                         <ExpandMoreIcon />
                                                        }
                                                  </IconButton>
                                            </Grid>
                                         </Grid>
                                            {(this.state.isExpanded && (this.state.refer_index === no)) &&
                                                <div>
                                                   <Collapse in={true} timeout="auto" unmountOnExit>
                                                     <CardContent>
                                                         <div>
                                                         {!this.state.isAddProduct && <ProductList data={items} username={this.state.username}  action={items.index} editProduct={this.editProduct}/>}
                                                         {!this.state.isAddProduct &&
                                                           <Fab style={{float:'right',marginBottom:'4px'}} size="small" color="primary" onClick={(e) => this.onCreate(e,items.index)}
                                                           aria-label="add">
                                                                <AddIcon />
                                                            </Fab>
                                                          }

                                                          {((this.state.isAddProduct === items.index) || (this.state.isEditProduct === items.index)) &&
                                                          <AddProduct data={items} cancel={this.updateState}
                                                          action={items.index} username={this.state.username} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                                                          }
                                                         </div>
                                                     </CardContent>
                                                   </Collapse>
                                                </div>
                                              }
                                    </Paper>
                                </Grid>
                              ))}
                          </Paper>
                        ))}
                  </Grid>
                  <Grid item xs={1} sm={1} />
                </Grid>
            </React.Fragment>
        )
      }
    }
    }
}

export default FiveYear;
