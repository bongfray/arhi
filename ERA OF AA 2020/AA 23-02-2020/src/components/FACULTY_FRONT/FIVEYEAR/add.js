import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      datafrom:'',
      expired: false,
      completed: false,
      expire_year:'',
      value:'',
      username: props.username,
      serial:'',
      action:'',
      content:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const value = event.target.value;

    this.setState({
      content: value,
    })

  }


componentDidMount(){
  var year = Date.today().toString("yyyy");
  var num = parseInt(year);
  this.setState({
    action:this.props.data.index,
    username: this.props.username,
    expire_year: num+5,
    expired:false,
    completed:false,
    datafrom:'Five',
  })
}



  handleSubmit(event) {
    console.log('Acted')
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12}>
                <React.Fragment>
                    <input
                      className=""
                      type="text"
                      min="10"
                      max="60"
                      name={this.props.data.index}
                      value={this.state.content}
                      onChange={e => this.handleD(e,this.props.data.index)}
                      placeholder="Enter Here..."
                    />
                  <br />
                    <Button variant="contained" color="primary" style={{float:'right'}}
                    onClick={this.handleSubmit}>UPLOAD</Button>
                    <Button  color="secondary" style={{float:'right'}} onClick={this.props.cancel}>Cancel</Button>

                </React.Fragment>
            </Grid>
          </Grid>

    )
  }
}
