import React, { Component } from 'react'
import axios from 'axios'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CircularProgress from '@material-ui/core/CircularProgress';

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
			logout:'/ework/user/logout',
			get:'/ework/user/',
      count:0,
      loading:false,
      nav_route: '/ework/user/fetchnav',
      percent_ad:[],
      percent_ac:[],
      percent_re:[],
      total_percent_ad:'',
      total_percent_ac:'',
      total_percent_re:'',
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

fetchPercentage = () =>{
  this.setState({loading:true})
  axios.get('/ework/user/knowPercentage', {
  })
  .then(response =>{
    const administrative = response.data.filter(item => item.action=== "Administrative");
    const academic = response.data.filter(item => item.action=== "Academic");
    const research = response.data.filter(item => item.action=== "Research");
    this.setState({percent_ad:administrative,percent_ac:academic,percent_re:research})
    this.fetchRootPercentageAd();
  })
}
fetchRootPercentageAd = () =>{
  axios.get('/ework/user/knowRootPercentageAd', {
  })
  .then(response =>{
    this.setState({total_percent_ad:response.data.responsibilty_root_percentage})
    this.fetchRootPercentageAcademic();
  })
}
fetchRootPercentageAcademic = () =>{
  axios.get('/ework/user/knowRootPercentageAc', {
  })
  .then(response =>{
    this.setState({total_percent_ac:response.data.responsibilty_root_percentage})
    this.fetchRootPercentageResearch();
  })
}
fetchRootPercentageResearch = () =>{
  axios.get('/ework/user/knowRootPercentageRe', {
  })
  .then(response =>{
    this.setState({total_percent_re:response.data.responsibilty_root_percentage,loading:false})
  })
}

    componentDidMount() {
        this.fetchPercentage();
    }
  render()
  {
    if(this.state.loading)
    {
      return(
        <div className="center" style={{padding:'30px'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else{
        return(
          <React.Fragment>
          <div className="row">
            <div className="col l4 xl4 s12 m12">
               <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
                  <ExpansionPanelSummary
                  style={{borderRadius:'20px'}}
                    expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                    className="mine-collap"
                  >
                <div style={{color:'yellow',fontSize:'18px'}}>Administrative Works</div>
                      </ExpansionPanelSummary>
                    <div style={{padding:'15px'}}>
                      <div className="row">
                        <div className="col l11 left">Total Percentage Allotted for Administrative Work: </div>
                        <div className="col l1">{this.state.total_percent_ad}</div>
                      </div>
                      <table>
                        <thead>
                          <tr>
                              <th>Different Works</th>
                              <th className="center">Allotted (H/W)</th>
                              <th className="center">Completed (H/W)</th>
                          </tr>
                        </thead>
                        <tbody>
                        {this.state.percent_ad.map((content,index)=>{
                          return(
                          <tr key={index}>
                            <td className="">{content.responsibilty_title}</td>
                            <td className="center">{content.responsibilty_percentage}</td>
                            <td className="center"><Total role={content.responsibilty_title}/></td>
                          </tr>
                        )
                        })}
                        </tbody>
                      </table>
                    </div>
                  </ExpansionPanel>
            </div>

            <div className="col l4 xl4 s12 m12">
             <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
                <ExpansionPanelSummary
                style={{borderRadius:'20px'}}
                  expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                  aria-controls="panel4bh-content"
                  id="panel4bh-header"
                  className="mine-collap"
                >
              <div style={{color:'yellow',fontSize:'18px'}}>Academic Work</div>
                    </ExpansionPanelSummary>
                    <div style={{padding:'15px'}}>
                        <div className="row">
                          <div className="col l11 left">Total Percentage Allotted for Academic Work: </div>
                          <div className="col l1">{this.state.total_percent_ac}</div>
                        </div>

                        <table>
                          <thead>
                            <tr>
                                <th>Different Works</th>
                                <th className="center">Allotted (H/W)</th>
                                <th className="center">Completed (H/W)</th>
                            </tr>
                          </thead>
                          <tbody>
                          {this.state.percent_ac.map((content,index)=>{
                            return(
                            <tr key={index}>
                              <td className="">{content.responsibilty_title}</td>
                              <td className="center">{content.responsibilty_percentage}</td>
                              <td className="center"><Total role={content.responsibilty_title}/></td>
                            </tr>
                          )
                          })}
                          </tbody>
                        </table>
                    </div>
                </ExpansionPanel>
            </div>
            <div className="col l4 xl4 s12 m12">

            <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
               <ExpansionPanelSummary
               style={{borderRadius:'20px'}}
                 expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                 aria-controls="panel4bh-content"
                 id="panel4bh-header"
                 className="mine-collap"
               >
             <div style={{color:'yellow',fontSize:'18px'}}>Research Work</div>
                   </ExpansionPanelSummary>
                   <div style={{padding:'15px'}}>
                         <div className="row">
                           <div className="col l12 left">Total Percentage Allotted for Research Work: <span>{this.state.total_percent_re}</span></div>
                         </div>
                         <table>
                         <thead>
                           <tr>
                               <th>Different Works</th>
                               <th className="center">Allotted (H/W)</th>
                               <th className="center">Completed (H/W)</th>
                           </tr>
                         </thead>
                         <tbody>
                         {this.state.percent_re.map((content,index)=>{
                           return(
                           <tr key={index}>
                             <td className="">{content.responsibilty_title}</td>
                             <td className="center">{content.responsibilty_percentage}</td>
                             <td className="center"><Total role={content.responsibilty_title}/></td>
                           </tr>
                         )
                         })}
                         </tbody>
                         </table>
                   </div>
               </ExpansionPanel>
            </div>
          </div>

          </React.Fragment>
        )
   }
  }
}



class Total extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      count:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTotal();
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.role !== this.props.role){
      this.fetchTotal();
    }
  }
  fetchTotal=()=>
  {
    axios.post('/ework/user/know_Complete_Percentage',{res:this.props.role})
    .then(response =>{
      let len= ((response.data).length)
      this.setState({count:len})
    })
  }
  render()
  {
    return(
      <React.Fragment>
         {this.state.count}
      </React.Fragment>
    );
  }
}
