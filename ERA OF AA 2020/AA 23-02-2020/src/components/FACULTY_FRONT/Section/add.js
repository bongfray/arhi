import React from 'react';
import TextField from '@material-ui/core/TextField';

class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      datafrom:'',
      Action:'',
      role:'',
      description:'',
      date_of_starting:'',
      date_of_complete:'',
      achivements:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.action,
    username: this.props.username,
    datafrom:'Section',
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    // console.log(this.initialState.username)
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS</h5>
}

    return(
      <div>
      <div className="center">{pageTitle}</div>
      <div>
      <div className="row">
        <div className="col s12 l12 xl12 m12" >

          {this.props.data.fielddata.map((content,index)=>(
            <div className="col l3 s12 xl3 m3" key={index}>
            <TextField
              id="outlined-multiline-static"
              label={content.placeholder}
              multiline
              rows="2"
              fullWidth
              name={content.name}
              value={this.state[content.name]}
              onChange={e => this.handleD(e, index)}
              variant="filled"
            />

            </div>
          ))}

        </div>
      </div>
      <div className="row">
        <div className="col l9 xl9 hide-on-mid-and-down" />
        <div className="col l3 xl3 s12">
          <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
          <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
        </div>
      </div>

      </div>
      </div>
    )
  }
}

export default AddProduct;
