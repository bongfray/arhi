import React, { Component } from 'react'
import { Redirect} from 'react-router-dom'
import axios from 'axios'

export default class ValidateUser extends Component {
    constructor(props){
        super(props)
        this.state ={
            redirectTo:'',
            password:'',
        }
    }
    check =(e) =>{
        this.setState({password:e.target.value})
    }
    Validate =(e)=>{
        e.preventDefault()
        axios.post('/ework/user/checkadmin',{password:this.state.password})
        .then(res=>{
            if(res.data.succ)
            {
                this.props.showDiv({
                    show: 'block',
                    modal:false,
                    showButton:'block',
                    disabled:'',
                    allowed:true,
                  });
            }
            else if(res.data.fail)
            {
                this.setState({redirectTo:'/ework/'})
                window.M.toast({html: 'Wrong PassWord!',outDuration:'1000', classes:'rounded #f44336 red'});
            }
        })
        this.setState({password:''})
    }
    render() {
        const divStyle =
        {
          display: this.props.displayModal ? 'block' : 'none',
        };
    if(this.state.redirectTo)
    {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else{
        return (
            <div className="cover" style={divStyle}>
               <div className="up" style={{padding:'20px'}}>
                <div className="col l4 s6 xl4 m4 push-l4 push-xl4 push-s3 push-m4">
                    <form onSubmit={this.Validate}>
                        <div className="input-field">
                            <input placeholder="Type your password" className="validate" type="password" value={this.state.password} onChange={this.check} required/>
                        </div>
                        <button className="btn small right pink" style={{marginBottom:'10px'}}>SUBMIT</button>
                    </form>
               </div>
               </div>
            </div>
        )
    }
    }
}
