import React, { Component } from 'react'
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import AddProduct from './add';
import ProductList from './prod'
import ValidateUser from '../validateUser';


export default class Section extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      redirectTo:'',
      option:'',
    }
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}
  render()
  {

    return(
      <Paper elevation={3} style={{padding:'15px'}}>
       <Grid container spacing={1}>
            <Grid item xs={2} sm={2}>
                <FormControl style={{width:'100%'}}>
                   <InputLabel id="catagory">Category</InputLabel>
                   <Select
                     labelId="catagory"
                     id="catagory"
                     name="title" value={this.state.option} onChange={this.handleOption}
                   >
                   <MenuItem value="faculty">Faculty Nav</MenuItem>
                   <MenuItem value="student">Student Nav</MenuItem>
                   <MenuItem value="department_admin">Department Admin Nav</MenuItem>
                   </Select>
                 </FormControl>
            </Grid>
            <Grid item xs={5} sm={5} />
            <Grid item xs={5} sm={5}/>
        </Grid>
        {this.state.option &&
              <Collap options={this.state.option} username={this.state.username}/>
        }
      </Paper>
    )
  }
}



class Collap extends Component{
 constructor(props) {
    super(props);
    this.state = {
      modal:false,
      show:'block',
      redirectTo: null,
      username:'',
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      action:this.props.options,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount()
{
  this.fetchlogin()
}
fetchlogin = () =>{
    axios.get('/ework/user/'
  )
    .then(response =>{
      if(response.data.user)
      {
        this.setState({username:response.data.user.username})
      }
      else{
        this.setState({
          redirectTo:'/ework/',
        });
        window.M.toast({html: 'You are not Logged In', classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }

showDiv =(userObject) => {
 this.setState(userObject)
}
  onCreate = (e,index) => {
    this.setState({ isAddProduct: true,product: {}});
  }

  onFormSubmit(data) {
    let apiUrl;
    var addroute,editroute;
    if((this.props.options === "faculty") || (this.props.options === "department_admin"))
    {
      addroute="/ework/user/addnav";
      editroute = "/ework/user/editnav"
    }
    else if(this.props.options === "student")
    {
      addroute="/ework/user2/addnav";
      editroute = "/ework/user2/editnav"
    }

    if(!(data.val)||!(data.link))
    {
      window.M.toast({html: 'Enter All the Details !!',classes:'rounded  red lighten-1'});
      return false;
    }
    else
    {
    if(this.state.isEditProduct){
      apiUrl = editroute;
    } else {
      apiUrl = addroute;
    }
    axios.post(apiUrl, {data})
        .then(response => {
          this.setState({
            response: response.data,
            isAddProduct: false,
            isEditProduct: false
          })
        })
   }
  }

  editProduct = (productId,index)=> {
    var editProd;
     if(this.props.options === "faculty")
    {
      editProd ="/ework/user/edit_existing_nav"
    }
    else if(this.props.options === "student")
    {
      editProd = "/ework/user2/edit_existing_nav"
    }
    axios.post(editProd,{
      id: productId,
    })
        .then(response => {
          this.setState({
            product: response.data,
            isEditProduct: true,
            isAddProduct: true,
          });
        })

 }
 updateState = () =>{
   this.setState({
     isAddProduct:false,
     isEditProduct:false,
   })
 }

  render() {
    let productForm;

           var  data = {
              fielddata: [
                {
                  header: "Nav Title",
                  name: "val",
                  placeholder: "Enter the Nav Title",
                  type: "text",
                  grid: 2,
                  div: "col s5 m5 l5 xl5 center",
                },
                {
                  header: "Enter the Link Address",
                  name: "link",
                  placeholder: "Enter the Link Address",
                  type: "text",
                  grid: 2,
                  div: "col s4 m5 l5 xl5 center",
                },

              ],
            };
            if(this.state.isAddProduct || this.state.isEditProduct)
            {
              productForm =
              <AddProduct cancel={this.updateState} username={this.state.username}
              action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}
              product={this.state.product} />
            }
          return (
           <React.Fragment>
              <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
           <div style={{display:this.state.show}}>
             {!this.state.isAddProduct &&

                  <ProductList username={this.state.username} action={this.props.options}
                  data={data}  editProduct={this.editProduct}/>

             }
             {!this.state.isAddProduct &&

                <Grid container spacing={1}>
                 <Grid item xs={6} sm={6}/>
                  <Grid item xs={6} sm={6}>
                    {this.props.options &&
                      <div style={{float:'right',padding:'10px'}}>
                        <Button variant="contained" color="secondary"
                        onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</Button>
                      </div>
                    }
                  </Grid>
                </Grid>
           }
                { productForm }
           </div>
          </React.Fragment>
          );

}
}
