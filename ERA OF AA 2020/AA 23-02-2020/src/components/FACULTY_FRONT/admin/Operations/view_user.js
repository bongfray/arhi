import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

import ViewUserBYList from '../../VIEW/FACULTY DATA/SUPER VIEW/view_super'
import Student from '../../VIEW/studentSearchView'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'',
            modal:false,
            disabled:'',
            allowed:false,
            active:'',
            color:'',
        }
    }
    toggle = (position) =>
    {
      if (this.state.active === position) {
        this.setState({active : null})
      } else {
        this.setState({active : position})
      }
    }
    color =(position) =>{
      if (this.state.active === position) {
          return "col l2 red";
        }
        return "col l2";
    }
    render(){
      let view_content;
      if(this.props.select === 'dept_admin_profile'){
            view_content=
                <div>
                   <DeptAdminData />
                </div>
        }
        else if(this.props.select === 'facprofile'){
            view_content =
            <ViewUserBYList admin_action={true} />
        }
        else if(this.props.select === 'studprofile'){
            view_content =
                <div>
                 <Student />
                </div>
        }
        else{
            view_content=
                <div></div>
        }
        return(
          <React.Fragment>
          <div>
            {view_content}
          </div>
          </React.Fragment>
        );
    }
}



class DeptAdminData extends Component {
  constructor()
  {
    super()
    this.state={
      departAdmin:[],
      user:[],
      render_detail:false
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.data()
  }
  data(){
    axios.get('/ework/user/know_no_of_user')
    .then( res => {
      const d_user = (res.data.filter(item => item.h_order=== 0.5));
      console.log(d_user);
        this.setState({departAdmin:d_user})
    });
  }
  showDetails =(user)=>{
    this.setState({
      render_detail:!this.state.render_detail,
      user:user,
    })
  }

  render() {
    return (
      <React.Fragment>
    <ShowDetails show_div={this.state.render_detail} detail={this.showDetails} user={this.state.user} />
      <table>
        <thead>
          <tr>
              <th className="center">No</th>
              <th className="center">Official ID</th>
              <th className="center">Department</th>
              <th className="center">Campus</th>
              <th className="center">Mail ID</th>
              <th className="center">Action</th>
          </tr>
        </thead>

        <tbody>
        {this.state.departAdmin.map((content,index)=>(
          <tr key={index}>
            <td className="center">{index+1}</td>
            <td className="center">{content.username}</td>
            <td className="center">{content.dept}</td>
            <td className="center">{content.campus}</td>
            <td className="center">{content.mailid}</td>
            <td className="center"><button className="btn" onClick={ () => this.showDetails(content)}>View History</button></td>
          </tr>
        ))}
        </tbody>
      </table>
      </React.Fragment>
    );
  }
}


class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_accepted:'',
      total_request:'',
      message:'',
      mailid:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.total_details();
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps.user.username!==this.props.user.username)
    {
      this.total_details();
    }
  }
  total_details = () =>{
    axios.post('/ework/user/fetch_department_admin_history',{username:this.props.user.username})
    .then( res => {
      const total_request= (res.data).length;
      const total_accepted= (res.data.filter(item => item.active === true)).length;
      this.setState({total_accepted:total_accepted,total_request:total_request})
    });
  }

  retriveMessage =(e)=>{
    this.setState({message:e.target.value})
  }
  sendMessage = (mailid) =>{
    if(!this.state.message)
    {
      window.M.toast({html: 'First fill the message !!',outDuration:'9000', classes:'rounded red '});
    }
    else{
    axios.post('/ework/user/send_message',{username:this.props.user.username,message:this.state.message})
    .then( res => {
        if(res.data)
        {
          window.M.toast({html: 'Sent !!',outDuration:'9000', classes:'rounded green darken-2'});
          this.setState({message:''})
        }
    });
    }
  }
  ping =()=>{
    axios.post('/ework/user/ping',{username:this.props.user.username})
    .then( res => {
        window.M.toast({html: 'Pinged !!',outDuration:'9000', classes:'rounded green darken-2'});
    });
  }

  render() {
    return (
      <Dialog fullScreen open={this.props.show_div} onClose={this.props.detail} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2} sm={2}>
                  <IconButton edge="start" color="inherit" onClick={this.props.detail} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8} sm={8}>
                 <div className="center" style={{fontSize:'20px'}}>Profile Details Of {this.props.user.username}</div>
               </Grid>
               <Grid item xs={2} sm={2}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
          <div className="row" style={{marginTop:'56px'}}>
              <div className="col l3"/>
              <div className="col l6 form-signup">
              <div className="col l4">
                 <div className="card">
                 <div className="pink white-text center">TOTAL REQUEST</div>
                 <div className="card-content center">
                  <h5>{this.state.total_request}</h5>
                 </div>
                 </div>
              </div>
              <div className="col l4">
                 <div className="card">
                 <div className="pink white-text center">ACCEPTED</div>
                 <div className="card-content center">
                  <h5>{this.state.total_accepted}</h5>
                 </div>
                 </div>
              </div>
              <div className="col l4">
                 <div className="card">
                 <div className="pink white-text center">PENDING</div>
                 <div className="card-content center">
                 <p className="right red-text go" onClick={this.ping}>PING</p>
                  <h5>{this.state.total_request - this.state.total_accepted}</h5>
                 </div>
                 </div>
              </div>
              </div>
              <div className="col l3"/>
          </div>
          <div className="row">
              <div className="col l6">
                 <div className="row">
                    <div className="col l1"/>
                    <div className="col l10 form-signup">
                        <h6 className="center">Leave A Message</h6>
                        <div className="row">
                          <div className="col l6">
                            <label className="">To : </label><span>{this.props.user.username}</span>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col l12">
                            <label className="pure-material-textfield-outlined alignfull">
                              <textarea
                                className=""
                                type="text"
                                placeholder=" "
                                min="10"
                                max="60"
                                value={this.state.message}
                                onChange={this.retriveMessage}
                              />
                              <span>Enter the Message</span>
                            </label>
                          </div>
                        </div>
                        <button className="right btn" onClick={this.sendMessage} style={{marginBottom:'5px'}}>SEND A MESSAGE</button>
                    </div>
                    <div className="col l1"/>
                 </div>
              </div>
              <div className="col l6">
               <div className="row">
               <div className="col l1" />
               <div className="col l10 form-signup">
                  <div className="row">
                      <span className="col l4">Deactivate the Account</span>
                                      <div className="switch col l2">
                                        <label>
                                          <input  checked={this.state.isChecked} value="fac" type="checkbox" />
                                          <span className="lever"></span>
                                        </label>
                                      </div>
                  </div>
                 <div className="row">
                     <div className="col l6">
                        <div className="card">
                        <div className="pink white-text center">NO OF COMPLAINTS</div>
                        <div className="card-content center">
                         <h5>6</h5>
                        </div>
                        </div>
                     </div>
                     <div className="col l6">
                        <div className="card">
                        <div className="pink white-text center">FEEDBACK RATING</div>
                        <div className="card-content center">
                         <h5>4.5*</h5>
                        </div>
                        </div>
                     </div>
                 </div>
                 </div>
                 <div className="col l1" />
                 </div>
              </div>
          </div>
          </List>
        </Dialog>
    );
  }
}
