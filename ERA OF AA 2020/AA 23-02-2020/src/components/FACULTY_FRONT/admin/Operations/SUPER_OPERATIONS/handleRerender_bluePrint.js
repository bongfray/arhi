import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

import TimeTable from '../../../Schedule/Timetable'
import TimeTableS from '../../../../STUDENT_FRONT/Timetable/slotdetails'
import VisibilityIcon from '@material-ui/icons/Visibility';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
      enable_view:false,
      content:'',
      req_stud:false,
      loader:true,
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq(this.props.student)
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,content)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })
      window.M.toast({html: 'Sending Activation Mail...',classes:'rounded orange'});
      let active_route;
      if(this.props.choice === 'student')
      {
        active_route= '/ework/user2/allow_blueprint_req';
      }
      else {
        active_route ='/ework/user/allow_blueprint_req';
      }
      axios.post(active_route,{content:content
      })
      .then(res=>{
            window.M.toast({html: 'Activated !!', classes:'rounded green darken-2'});
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== content.username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_req_on_blueprint_render';
    }
    else if(this.props.choice === 'student')
    {
      route = '/ework/user2/fetch_req_on_blueprint_render';
    }
    axios.post(route)
    .then(res=>
      {
        if(this.props.student)
        {
          var list = res.data.filter(item=>(item.faculty_adviser ===this.props.user.username))
          this.setState({display:'block',requests:list})
        }
        else
        {
            this.setState({display:'block',requests:res.data})
        }
        this.setState({loading:false})
    })
  }
handleView=()=>{
  this.setState({enable_view:!this.state.enable_view})
}

showCurrentStatus=(content)=>{
  this.setState({content:content,enable_view:true,})
}

  render()
  {
    let student_on;
   if(this.props.choice === 'student')
   {
     student_on = true;
   }
   else {
     student_on = false;
   }
   //console.log(student_on)
    return(
      <React.Fragment>
      {this.state.enable_view &&
        <Dialog fullScreen open={this.state.enable_view} TransitionComponent={Transition}>
            <AppBar>
              <Toolbar>
               <Grid style={{marginTop:'55'}}container spacing={1}>
                 <Grid item xs={1} sm={1}>
                    <IconButton edge="start" color="inherit" onClick={this.handleView} aria-label="close">
                      <CloseIcon />
                    </IconButton>
                 </Grid>
                 <Grid item xs={10} sm={10}>
                   <div className="center" style={{fontSize:'20px'}}>Showing Details of <span className="yellow-text">{this.state.content.username}</span></div>
                 </Grid>
                 <Grid item xs={1} sm={1}/>
               </Grid>
              </Toolbar>
            </AppBar>
            <List>
              <div style={{marginTop:'70px'}}>
               {student_on ?
                  <TimeTableS props_to_refer={this.state.content} />
                 :
                 <TimeTable props_to_refer={this.state.content} />
               }
              </div>
            </List>
        </Dialog>
      }
      {this.props.choice &&
      <div className="row">
         <div className="col l12 xl12">
            <div className="card">
              <div className="card-title center pink white-text">Request For Blue Print Render</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.requests.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                        <div className="col l2 center"><b>Index</b></div>
                        <div className="col l2 center"><b>Official Id</b></div>
                        <div className="col l4 center"><b>Reason</b></div>
                        <div className="col l2 center"><b>View Status</b></div>
                        <div className="col l2 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.requests.map((content,index)=>(
                           <div className="row"  key={index}>
                            <div className="col l2 center">{index+1}</div>
                            <div className="col l2 center">{content.username}</div>
                            <div className="col l4 center" style={{wordWrap: 'break-word'}}>{content.req_reason}</div>
                            <div className="col l2 center">
                               <VisibilityIcon onClick={()=>this.showCurrentStatus(content)} />
                            </div>
                            <div className="col l2 center">
                                <div className="switch">
                                  <label>
                                    <input  checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content)}} type="checkbox" />
                                    <span className="lever"></span>
                                  </label>
                                </div>
                            </div>
                          </div>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
       }
      </React.Fragment>
    ) ;
   }
  }
