import React, { Component } from 'react'
import axios from 'axios'
import Switch from './slot_handle_problem'


export default class Allot extends React.Component {
  constructor() {
    super()
    this.state = {
       freeparts:'Curriculum',
        sending:'',
        selected:'',
        covered_topic:'',
        problem_statement:'',
        compday:'',
        comphour:'',
        compday_order:'',
        allot:'',
        count: 0,
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        date:'',
        year:'',
        month:'',
        compense_faculty:'',
    }
  }


  handleChecked =(e) =>{
    this.setState({
      selected: e.target.value,
    })
  }

  updateAllotV =(userObject) => {
    this.setState(userObject)
  }


  closeModal=()=>{
    this.props.closeModal();
  }
  colorChange=()=>{
    this.props.color()
  }

handleAlloted =(e) =>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();
  let freeparts,verified,positive_count,negative_count,datas_to_send;
  e.preventDefault();
  if(this.state.selected === "Problem")
  {
    freeparts = '';
  }
  else
  {
    freeparts = "Curriculum";
    verified = false;
    positive_count = 0;
    negative_count = 0;
  }

  if(!this.state.selected)
  {
      window.M.toast({html: 'Enter All the Details First',classes:'rounded pink'});
  }
  else{
    if(this.state.selected === 'Problem')
    {
      if(!this.state.problem_statement)
      {
        window.M.toast({html: 'You should mention the reason !!', classes:'rounded pink'});
      }
      else
      {
        if((this.state.comphour) || (this.state.compday))
        {
          if((!this.state.compday) || (!this.state.comphour) || (!this.state.compday_order))
          {
            window.M.toast({html: 'Enter All the Details First', classes:'rounded pink'});
          }
          else
          {
             datas_to_send={
              username: this.props.username,
              freefield:"Academic",
              freeparts:freeparts,
              date:this.props.date,
              month: this.props.month,
              year: this.props.year,
              week:week,
              day_order: this.props.day_order,
              hour:this.props.content.number,
              for_year:this.props.datas_a.year,
              for_batch:this.props.datas_a.batch,
              for_sem:this.props.datas_a.sem,
              selected:this.state.selected,
              problem_statement:this.state.problem_statement,
              slot:this.props.slot,
              subject_code:this.props.datas_a.subject_code,
              time:this.props.content.time,
              problem_compensation_day_order:this.state.compday_order,
              problem_compensation_date:this.state.compday,
              problem_compensation_hour:this.state.comphour,
            }
            this.sendAllot(datas_to_send);
          }
        }
        else
        {
          if(this.state.compense_faculty)
          {
            this.checkWithFaculty(freeparts,verified,positive_count,negative_count,
            this.state.selected,'',this.state.problem_statement,'','','',
            this.state.compense_faculty,this.props)
          }
          else
          {
             datas_to_send={
              username: this.props.username,
              freefield:"Academic",
              freeparts:freeparts,
              date:this.props.date,
              month: this.props.month,
              year: this.props.year,
              week:week,
              day_order: this.props.day_order,
              hour:this.props.content.number,
              for_year:this.props.datas_a.year,
              for_batch:this.props.datas_a.batch,
              for_sem:this.props.datas_a.sem,
              selected:this.state.selected,
              problem_statement:this.state.problem_statement,
              slot:this.props.slot,
              subject_code:this.props.datas_a.subject_code,
              time:this.props.content.time,
            }
            this.sendAllot(datas_to_send);
          }
        }
      }
    }
    else if(this.state.selected === 'Completed')
    {
      if(!this.state.covered_topic)
      {

            window.M.toast({html: 'Enter All the Details First',classes:'rounded pink'});
      }
      else
      {
        datas_to_send={
          username: this.props.username,
          freefield:"Academic",
          freeparts:freeparts,
          date:this.props.date,
          month: this.props.month,
          year: this.props.year,
          week:week,
          day_order: this.props.day_order,
          hour:this.props.content.number,
          for_year:this.props.datas_a.year,
          for_batch:this.props.datas_a.batch,
          for_sem:this.props.datas_a.sem,
          selected:this.state.selected,
          covered:this.state.covered_topic.toLowerCase(),
          slot:this.props.slot,
          subject_code:this.props.datas_a.subject_code,
          time:this.props.content.time,
          verified:verified,
          positive_count:positive_count,
          negative_count:negative_count,
        }
        this.sendAllot(datas_to_send);
      }
    }
  }
}


sendAllot=(datas)=>
{
        axios.post('/ework/user/timeallot',datas,
        this.setState({
          sending: 'disabled',
        })
      )
          .then(response => {
            this.setState({sending:''})
            if(response.status===200){
               if(response.data==='done')
              {
                if(datas.freeparts === 'Curriculum')
                {
                  this.sendNoti_To_Student()
                }
                window.M.toast({html: 'Success !!',classes:'rounded #ec407a green lighten-1'});
                if(this.props.foreign_ref === "true")
                {
                          this.props.closeref();
                }
                else{
                  this.closeModal();
                  this.colorChange();
                }
              }
              else if(response.data === 'no')
              {
                window.M.toast({html: 'Already Submitted !!', classes:'rounded red'});
                 this.closeModal();
              }
              else if(response.data === 'spam')
              {
                  window.M.toast({html: 'Invalid Request !!', classes:'rounded red'});
              }
            }
          })
}


checkWithFaculty=(freeparts,verified,positive_count,negative_count,
  selected,covered_topic,problem_statement,compday,comphour,
  saved_slots,compense_faculty,incoming_props)=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();
  axios.post('/ework/user/check_possibility',{id:compense_faculty.toUpperCase()
    ,week:week,props_content:this.props})
  .then( res => {
    if(res.data === 'no')
    {
      window.M.toast({html: 'Invalid Request !!', classes:'rounded  red'});
        this.setState(this.initialState);
      return false;
    }
    else
    {
          window.M.toast({html: 'Submitting...', classes:'rounded orange black-text'});
          var datas={
            username: this.props.username,
            freefield:"Academic",
            freeparts:freeparts,
            date:this.props.date,
            month: this.props.month,
            year: this.props.year,
            week:week,
            day_order: this.props.day_order,
            hour:this.props.content.number,
            for_year:this.props.datas_a.year,
            for_batch:this.props.datas_a.batch,
            for_sem:this.props.datas_a.sem,
            selected:this.state.selected,
            problem_statement:this.state.problem_statement,
            slot:this.props.slot,
            subject_code:this.props.datas_a.subject_code,
            time:this.props.content.time,
            verified:true,
            compense_faculty:this.state.compense_faculty.toUpperCase(),
            compense_faculty_topic:'',
          }
          this.sendAllot(datas);
    }
  });
}

sendNoti_To_Student=()=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();

  axios.post('/ework/user2/send_noti_on_class_complition',
  {week:week,slot:this.props.slot,for_year:this.props.datas_a.year,
  for_batch:this.props.datas_a.batch,for_sem:this.props.datas_a.sem,
  day_order: this.props.day_order,subject_code:this.props.datas_a.subject_code,
  hour:this.props.content.number,action:'Class Completed',
  faculty_id:this.props.username,covered_topic:this.state.covered_topic.toLowerCase(),date:
  this.props.date,month:this.props.month,year:this.props.year})
  .then( res => {

  });
}

render(){
  let disabled= false;
  let time = parseFloat(new Date().toString("HH.mm"));
  if((parseFloat(this.props.content.start_time))>time)
  {
    disabled = true;
  }
  return(
    <React.Fragment>
    <span className="pink-text darken-4">Reminder: </span>
    <span>This slot is booked at the beginig of Semester.You are not allowed to edit it.And one
    more thing, you can't submit the data for alloted slot, before its get started.</span>
    <div className="row">
      <div className="col l6 xl6 s12 m12">
      <p>
        <label>
        <input disabled={disabled} type='radio' id='radio-1' name='myRadio' value='Completed' onChange={this.handleChecked} />
          <span className="center green-text"><b>Class/Work Completed</b></span>
        </label>
     </p>
      </div>
      <div className="col l6 xl6 m12 s12">
      <p>
      {!this.props.cday &&
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Problem' onChange={this.handleChecked} />
          <span className="center red-text"><b>Problem With Work/Class Completion(like Slot Cancelled)</b></span>
        </label>
      }
     </p>
      </div>
    </div>
      <DivOpen prop_datas={this.props} selected={this.state.selected} updateAllotV={this.updateAllotV}/>
     <button className="waves-effect btn col l2 s4 xl2 m2 blue-grey darken-2 sup right" disabled={this.state.sending} onClick={this.handleAlloted}>SUBMIT</button>
   </React.Fragment>
  );
}
}





/*-------for check radio button----------------------------------- */

 class DivOpen extends Component{
   constructor(){
     super();
     this.state ={
       covered_topic:'',
       problem_statement:'',
     }
   }
handleAllotData= (evt) =>{
     const value = evt.target.value;
     this.setState({
       [evt.target.name]: value
     });
     this.props.updateAllotV({
       [evt.target.name]: value,
     });
   }



   render(){

     if(this.props.selected ==="Completed")
     {
       return(
         <div className="input-field">
         <input id="covered" type="text" name="covered_topic" value={this.state.covered_topic}  onChange={this.handleAllotData}/>
         <label htmlFor="covered">Enter the the things covered in this alloted slot.</label>
         </div>
       );
     }
     else if(this.props.selected === "Problem")
     {
       return(
         <React.Fragment>
         <div className="input-field">
         <input id="prob" type="text" name="problem_statement" className="validate" value={this.state.problem_statement} onChange={this.handleAllotData} required/>
           <label htmlFor="prob" >Enter the reason</label>
         </div>
         <div className="">
           <Switch prop_datas={this.props.prop_datas} updateAllotV={this.props.updateAllotV}/>
         </div>
         </React.Fragment>
       );
     }
     else
     {
       return(
       <div></div>
     );
     }
   }
 }
