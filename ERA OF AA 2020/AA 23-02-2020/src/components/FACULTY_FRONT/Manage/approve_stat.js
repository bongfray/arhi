import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import ColorRep from '../TimeTable/colordiv.js'
import Allot from '../TimeTable/Al2.js'
import Free from '../TimeTable/Free.js'
require("datejs")

/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
})

export default class Simple extends Component{

  constructor() {
    super();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      approved:[],
      viewdata:[],
      available:'no',
      notfound:'',
      loading:true,
    }
  }


  componentDidMount(){
    this.knowExpiry();

  }

  knowExpiry = ()=>{
    axios.post('/ework/user/request_Expiry_Check')
    .then( res => {
        if(res.data)
        {
          this.fetchApprove()
        }
    });
  }
  componentDidUpdate =(prevProps) => {
    if (prevProps.request_props !== this.props.request_props) {
      this.fetchApprove();
    }
  }

  fetchApprove = () =>{
    axios.get('/ework/user/fetch_requested_list').then(res=>{
      if(res.data)
      {
        var requests = res.data.filter(item=>!(item.action === 'For Blue Print Render'))
        this.setState({
          approved: requests,
          loading:false,
        })
      }
    })
  }


  handleViewRequest = (content,index) =>{
    this.componentDidUpdate(index)
    this.setState({
      available:'yes',
      viewdata: content,
    })
  }


deleteRequest=(key)=>{
  axios.post('/ework/user/request_Expiry_Check',{serial:key})
  .then( res => {
      if(res.data)
      {
        this.fetchApprove();
      }
  });
}

      render(){
              return(
                <React.Fragment>
                {this.state.loading ?
                      <div className="center">Fetching Datas....</div>
                      :
                      <React.Fragment>
                      <h6 className="center pink white-text" style={{height:'45px',paddingTop:'12px'}}><b>STATUS OF YOUR REQUESTS</b></h6><br /><br />
                      {this.state.approved.length!==0 ?
                        <React.Fragment>
                      <div className="row">
                        <div className="col l1 xl1 s1 m1 center"><b>R DayOrder</b></div>
                        <div className="col l1 xl1 hide-on-small-only m1 center"><b>Official ID</b></div>
                        <div className="col l1 xl1 s1 m1 center" style={{wordWrap: 'break-word'}} ><b>R Date</b></div>
                        <div className="col l1 xl1 s1 m1 center" style={{wordWrap: 'break-word'}} ><b>Expire Date</b></div>
                        <div className="col l2 xl2 s2 m2 center" style={{wordWrap: 'break-word'}} ><b>Deny Reason</b></div>
                        <div className="col l1 xl1 s1 m1 center" style={{wordWrap: 'break-word'}} ><b>Hour</b></div>
                        <div className="col l2 xl2 s2 m2 center" style={{wordWrap: 'break-word'}} ><b>Reason</b></div>
                        <div className="col l2 xl2 s2 m2 center"><b>Status</b></div>
                        <div className="col l1 xl1 s1 m1 center"><b>Action</b></div>
                      </div>
                      <hr />
                      <div className="col l12 s12 m12 xl12">
                      {this.state.approved.map((content,index)=>(
                        <React.Fragment key={index}>
                        <div className="row">
                        <div>
                        <div className=" col l1 xl1 s1 m1 center">{content.day_order}</div>
                        <div className=" col l1 xl1  hide-on-small-only m1 center">{content.username}</div>
                        <div className=" col l1 xl1 s1 m1 center" style={{wordWrap: 'break-word'}} >{content.req_date}/{content.req_month}/{content.req_year}</div>
                        {content.expired_date ? <div className=" col l1 xl1 s1 m1 center red-text" style={{wordWrap: 'break-word'}} >{content.expired_date}/{content.expired_month}/{content.expired_year}</div> : <div className=" col l1 xl1 s2 m2 center red-text">NULL</div>}
                        {content.denying_reason ? <div className=" col l2 xl2 s2 m2 center red-text" style={{wordWrap: 'break-word'}} >{content.denying_reason}</div>: <div className=" col l2 xl2 s2 m2 center red-text">NULL</div>}
                        {content.req_hour ? <div className="col l1 xl1 s1 m1 center">{content.req_hour}</div>:<div className="col l1 xl1 s1 m1 center">N/A</div>}
                        <div style={{wordWrap: 'break-word'}} className=" col l2 xl2 s2 m2 center">{content.req_reason}</div>
                        </div>
                        <div className="col l2 xl2 s2 m2">
                          <StatusReq content={content} serial={content.serial} />
                        </div>
                        <div className=" col l1 xl1 s1 m1 center"><i title="Dismiss" onClick={()=>this.deleteRequest(content.serial)} className=" go material-icons small">close</i></div>
                        </div>
                        <hr />
                        </React.Fragment>
                      ))}
                      </div>

                      </React.Fragment> : <div className="center">No Request Found !!</div>
                    }
                      </React.Fragment>
                }
              </React.Fragment>
      )
      }
    }

class StatusReq extends Component{
  constructor(props)
  {
    super(props)
    this.state ={
      status:'',
      viewdata:[],
      available:'no',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStatus_Of_Request()
  }


  handleViewRequest = (content,index) =>{
      this.setState({
        available:'yes',
        viewdata: content,
      })
  }

  dataSubmitWindow=()=>{
    this.setState({available:'no'})
  }

  fetchStatus_Of_Request= () =>{
    axios.post('/ework/user/fetch_requested_status',{
      serial: this.props.serial,
    }).then(res=>{
      this.setState({status: res.data})
    })
  }
  render()
  {
    let btn;
    if(this.state.status === "approved")
    {
      btn =
      <button className="btn col l12 s12 xl12 m12 right blue-grey darken-2 sup"
        onClick={() => this.handleViewRequest(this.props.content,this.props.serial)}>Upload Data</button>
    }
    else if(this.state.status === "denied")
    {
      btn = <div className="col l12 s12 xl2 m2 center" style={{color:'red',borderLeft:'1px solid red',borderRight:'1px solid red',borderTop:'1px solid red',borderBottom:'1px solid red'}}>Denied</div>
    }
    else if(this.state.status === "pending")
    {
      btn = <div className="col l12 s2 xl12 m2 center" style={{color:'red',borderLeft:'1px solid red',borderRight:'1px solid red',borderTop:'1px solid red',borderBottom:'1px solid red'}}>Pending</div>
    }

    return(
      <React.Fragment>
      <div>{btn}</div>
      <div className="col l12 s12 m12 xl12">
       <Content dataSubmitWindow={this.dataSubmitWindow}
        available={this.state.available} view={this.state.viewdata}/>
     </div>
      </React.Fragment>
    )
  }
}


/*-----------------------------------------------This class is for dynamically allocating div ------------- */

class Content extends Component{
  constructor(props){
    super(props);
    this.state={
      username: props.view.username,
      day_order: props.view.day_order,
      date:props.view.date,
      month:props.view.month,
      year:props.view.year,
      hour:props.view.hour,
      redirectTo:'',
  }
  }

  render(){
    var content={
      number:this.props.view.req_hour,
    }
    let output;
    if(this.props.available === "yes")
    {
      output =
      <React.Fragment>
        <Dialog fullScreen open={true} onClose={this.props.dataSubmitWindow}   TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
              <IconButton edge="start" color="inherit" onClick={this.props.dataSubmitWindow} aria-label="close">
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
          <List>
          <div style={{padding:'30px'}}>
             <div className="row">
                {!(this.props.view.req_hour) ?
                  <React.Fragment>
                     <ColorRep username={this.props.view.username} opendir="r_class"
                      date={this.props.view.req_date} month={this.props.view.req_month}
                      content={content} year={this.props.view.req_year}
                      day_order={this.props.view.day_order}/>
                  </React.Fragment>
                :
                <React.Fragment>
                <div className="row">
                  <OtherSource view={this.props.view} closeref={this.props.dataSubmitWindow} />
                </div>
                <div className="row">
                   <span className="red-text">REMEMBER :</span><br />
                   <p>There is no chance to edit the datas,once you submit. So it's a one time
                   process. Kindly fill and submit the details correctly.</p>
                </div>
                </React.Fragment>
                 }
             </div>
          </div>
          </List>
        </Dialog>
      </React.Fragment>
    }
    else
    {
      output=
      <div></div>
    }
          return(
            <div>
            {output}
            </div>
          );
  }
}


class OtherSource extends Component {
  constructor()
  {
    super()
    this.state={
      dataa_a:'',
      allo1:false,
      modal:true,
      alreadyhave:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    	axios.post('/ework/user/fetchAllotments', {
    		day_order: this.props.view.day_order,hour:this.props.view.req_hour,
    	})
    	.then(response =>{
    		if(response.data)
    		{
          let allotdata = (response.data.alloted_slots).toUpperCase();
    				this.setState({
    					allo1:true,
              datas_a:response.data,
              alreadyhave: allotdata,
    				})
    		}
    	})
  }

  closeModal = (info) => {
  this.setState({modal: !this.state.modal})
}

  render() {
    var content={
      number:this.props.view.req_hour,
    }
    if(this.state.allo1)
    {
      return(
        <React.Fragment>
        <Allot
        slot={this.state.alreadyhave}
        datas_a={this.state.datas_a}
        date ={this.props.view.req_date}
        month ={this.props.view.req_month}
        year={this.props.view.req_year}
        username={this.props.view.username}
        day_order={this.props.view.day_order}
        closeModal={this.closeModal}
        content={content}
        foreign_ref="true"
        closeref={this.props.closeref}
        />
        </React.Fragment>
      )
    }
    else{
      return(
        <React.Fragment>
        <Free
        slot={this.state.data_in}
        day ={this.props.view.req_day}
        month ={this.props.view.req_month}
        year={this.props.view.req_year}
        day_sl={"."+this.props.view.req_hour}
        usern={this.props.view.username }
        day_order={this.props.view.day_order}
        closeModal={this.props.closeModal}
        day_slot_time ={'.'+this.props.view.req_hour}/>
        />
        </React.Fragment>
      )
    }
  }
}
