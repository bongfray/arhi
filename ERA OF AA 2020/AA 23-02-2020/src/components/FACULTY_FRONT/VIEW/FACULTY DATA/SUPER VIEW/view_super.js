import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Backdrop from '@material-ui/core/Backdrop';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';


import ViewByID from './view_by_id'
import FacList from './list_of_user'
require("datejs")




export default class BasicView extends Component {
  constructor() {
    super()
    this.state ={
      start:'',
      isChecked: false,
      history: '',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      agreed:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.getUser();
  }

    getUser()
    {
      axios.get('/ework/user/'
    )
       .then(response =>{
         this.setState({
           loading:false,
         })
          if(response.status === 200)
          {
             if(response.data.user)
             {

             }
             else
             {
               this.setState({
                 redirectTo:'/ework/faculty',
               });
               window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
             }
          }
       })
    }


  handleComp = (e) =>{
    window.M.toast({html: 'Validating....', classes:'rounded orange'});
    axios.get('/ework/user/validate_list_view')
    .then( response => {
      // console.log(response.data)
        if(response.data.permission === 1)
        {
          window.M.toast({html: 'Accepted!!', classes:'rounded green'});
          this.setState({
            isChecked: !this.state.isChecked,
            start:response.data.data_send,
          })
        }
        else{
          window.M.toast({html: 'Denied Request !!', classes:'rounded red'});
          this.setState({
            isChecked:false,
            redirectTo:'/ework/view',
          })
        }
    });
  }
  render() {
    let admin_action;
    if(this.props.admin_action)
    {
      admin_action = this.props.admin_action;
    }
    else {
      admin_action = false;
    }
    if (this.state.redirectTo)
     {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
     }
      else
       {
        return (
          <React.Fragment>
             <div className="switch center"  style={{marginTop:'18px'}}>
                 <label style={{color:'red',fontSize:'15px'}}>
                     Already Having Official Id
                     <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                     <span className="lever"></span>
                     Don't have any Official Id !!
                 </label>
             </div>
             <br />
             <Switch admin_action={admin_action} datas={this.state.isChecked} start={this.state.start}/>
          </React.Fragment>
        );
      }
  }
}


class Switch extends Component{
  constructor(props)
  {
    super(props)
    this.state={

    }
  }
  render()
  {
    let render_content;
    if(this.props.datas === true)
    {
      render_content =
      <ListShow admin_action={this.props.admin_action} start={this.props.start}/>
    }
    else{
      render_content =
      <ViewByID admin_action={this.props.admin_action} />
    }
    return(
      <React.Fragment>
        {render_content}
      </React.Fragment>
    );
  }
}


class ListShow extends Component {
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      username:'',
      dept:'',
      desgn:'',
      designation:'',
      desgn_list:[],
      department:[],
      fetching:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user/fetch_view_list',{start:this.props.start})
    .then( res => {
        if(res.data)
        {
          this.setState({desgn_list:res.data});
          this.fetchDepartment();
        }
    });
  }
  fetchDepartment=()=>{
    axios.get('/ework/user/fetch_department')
    .then( res => {
        if(res.data)
        {
          this.setState({department:res.data,fetching:false})
        }
    });
  }
  handleDesgn =(e)=>{
    this.setState({desgn:e.target.value})
  }
  handleDept = (e)=>{
    this.setState({dept:e.target.value})
  }
  showDiv =(username)=>{
    if(username)
    {
      this.setState({display:'block',username:username})
    }
  }

  render() {
    if(this.state.fetching)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      )
    }
    else
    {
      return (
        <div>
        <Grid container spacing={1}>
           <Grid item xs={3} sm={3}/>
           <Grid container item xs={6} sm={6} spacing={1}>
              <Grid item xs={6} sm={6}>
                 <Paper elevation={3} style={{padding:'10px'}}>
                    <Typography style={{fontSize:'18px'}} align="center" variant="overline" display="block" gutterBottom>
                      Select the Designation
                    </Typography>
                    <FormControl style={{width:'100%'}}>
                      <InputLabel id="desgntype">Select Here..</InputLabel>
                      <Select
                        labelId="desgntype"
                        id="desgntype"
                        value={this.state.desgn}
                        onChange={this.handleDesgn}
                      >
                      {this.state.desgn_list.map((content,index)=>{
                        return(
                          <MenuItem value={content.designation_name} key={index}>{content.designation_name}</MenuItem>
                      )
                      })}
                      </Select>
                    </FormControl>
                  </Paper>
              </Grid>
              <Grid item xs={6} sm={6}>
                  <Paper elevation={3} style={{padding:'10px'}}>
                      <Typography style={{fontSize:'18px'}} align="center" variant="overline" display="block" gutterBottom>
                        Select the Department
                      </Typography>
                      <FormControl style={{width:'100%'}}>
                        <InputLabel id="depttype">Select Here..</InputLabel>
                        <Select
                          labelId="depttype"
                          id="depttype"
                          value={this.state.dept}
                          onChange={this.handleDept}
                        >
                        {this.state.department.map((content,index)=>{
                          return(
                            <MenuItem value={content.department_name} key={index}>{content.department_name}</MenuItem>
                        )
                        })}
                        </Select>
                      </FormControl>
                    </Paper>
              </Grid>
           </Grid>
           <Grid item xs={3} sm={3}/>
        </Grid>

<br />
            <div>
                  <FetchUser actionType="globalUser" admin_action={this.props.admin_action}
                   dept={this.state.dept} desgn={this.state.desgn} showDiv={this.showDiv} />
            </div>
        </div>
      );
    }
  }
}






class FetchUser extends Component {
  constructor(props) {
    super(props)
    this.state={
      users:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser();
  }
  componentDidUpdate =(prevProps)=>{
    if((prevProps.dept!==this.props.dept) || (prevProps.desgn!==this.props.desgn))
    {
      this.fetchUser();
    }
  }
  fetchUser = ()=>{
    axios.post('/ework/user/fetchUser',{department:this.props.dept,designation:this.props.desgn})
    .then( res => {
      console.log(res.data)
        this.setState({users:res.data,loading:false})
    });
  }
  render()
  {
    console.log(this.props)
    if(this.state.loading)
    {
      return(
        <div>Fetching.....</div>
      )
    }
    else{
    return(
      <React.Fragment>
        <FacList  admin_action={this.props.admin_action} actionType={this.props.actionType} users={this.state.users}/>
      </React.Fragment>
    )
   }
  }
}
