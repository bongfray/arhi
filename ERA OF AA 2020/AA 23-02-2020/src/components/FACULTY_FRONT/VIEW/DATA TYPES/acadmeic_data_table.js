import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import EditIcon from '@material-ui/icons/Edit';
import InfoIcon from '@material-ui/icons/Info';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';




function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}


const headCells = [
  { id: 'Type of Responsibility', numeric: false, disablePadding: false, label: 'Type of Responsibility' },
  { id: 'Status', numeric: true, disablePadding: false, label: 'Status' },
  { id: 'Submitted Data', numeric: true, disablePadding: false, label: 'Submitted Data' },
  { id: 'Date of Submit', numeric: true, disablePadding: false, label: 'Date of Submit' },
  { id: 'Submitted DatOrder / Hour', numeric: true, disablePadding: false, label: 'Submitted DatOrder / Hour' },
  { id: 'Information', numeric: true, disablePadding: true, label: 'Information' },
  { id: 'Action', numeric: true, disablePadding: true, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>
               {(((headCell.label === 'Action') || (headCell.label === 'Information')) && !(props.incoming_action)) ?
                 <React.Fragment></React.Fragment>
                   :
                 <React.Fragment>{headCell.label}</React.Fragment>
               }
              </b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

 export default function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [active, setActive] = React.useState('');
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [info,openInfo] = React.useState({
    info:false,
    info_data:'',

  });
  const [edit,setEdit] = React.useState({
    action:false,
    edit_data:'',
    covered:'',
  });
  const [covered,setDataToSend] = React.useState(edit.covered);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const submitEdit=(e,datal)=>{
    axios.post('/ework/user/updateSubmittedData',
    {data:edit.edit_data,covered:covered})
    .then( res => {
        if(res.data === 'ok')
        {
            window.M.toast({html: 'Updated !!',classes:'rounded green darken-2'});
            setEdit({action:false,edit_data:''})
        }
        else
        {
            window.M.toast({html: 'Can not able to update !!',classes:'rounded red'});
              setEdit({action:false,edit_data:''})
        }
    })
    .catch( err => {
      window.M.toast({html: 'Something went wrong !!',classes:'rounded #f44336 red'});
    });

  }



  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.academic_data.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
    {info.info &&
      <Dialog
        open={info.info}
        keepMounted
        onClose={()=>openInfo({
          info:false,
        })}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title" style={{textAlign:'center'}}>Information</DialogTitle>
        <DialogContent>
           <div style={{padding:'10px'}}>

            {info.info_data.selected === 'Problem' ?
                <React.Fragment>
                   {info.info_data.compense_faculty ?
                     <React.Fragment>
                       Compensation Faculty : {info.info_data.compense_faculty}<br /><br />
                       Topic Covered By Compensation Faculty : {info.info_data.compense_faculty_topic}<br /><br />
                     </React.Fragment>
                   :
                     <React.Fragment>
                       Own Compensation Date: {info.info_data.problem_compensation_date} / {info.info_data.problem_compensation_month}
                       / {info.info_data.problem_compensation_year}<br /><br />
                       Own Compensation Dayorder / Hour : {info.info_data.problem_compensation_day_order} /
                       {info.info_data.problem_compensation_hour}<br /><br />
                     </React.Fragment>
                  }
                </React.Fragment>
             :
             <React.Fragment>
             Total Positive Reponse : {info.info_data.positive_count}<br /><br />
             Total Negative Reponse : {info.info_data.negative_count}<br /><br />
             Verification Status : {info.info_data.verified ?
               <span style={{color:'green'}}>Verified</span>
              :
              <span style={{color:'red'}}>Not Verified</span>}<br /><br />
                <FormControl component="fieldset" className={classes.formControl}>
                  <RadioGroup aria-label="verify" name="verify1" value={active} onChange={(e)=>setActive(e.target.value)}>
                    <FormControlLabel value={true} control={<Radio />} style={{color:'black'}} label="Verify the User" />
                  </RadioGroup>
                </FormControl>
              </React.Fragment>
            }
           </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>openInfo(false)} color="secondary">
            CLOSE
          </Button>
        </DialogActions>
      </Dialog>
    }

    {edit.action &&
      <Dialog
        open={edit.action}
        keepMounted
        onClose={()=>setEdit({
          action:false,
        })}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title" style={{textAlign:'center'}}>Edit Window</DialogTitle>
        <DialogContent>
           <div style={{padding:'10px'}}>
              {edit.edit_data.covered &&
                 <TextField
                   id="outlined-multiline-static"
                   label="Covered Topic"
                   multiline
                   fullWidth
                   value={edit.covered}
                   onChange={(e)=>setEdit(e.target.value)}
                   variant="filled"
                 />
               }
               <br /><br />
               <div style={{float:'right',padding:'10px'}}>
                 <Button variant="contained" color="secondary"
                  onClick={(e)=>submitEdit(e,edit.edit_data)}>Submit</Button>
                </div>
           </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>setEdit({
            action:false
          })} color="secondary">
            CLOSE
          </Button>
        </DialogActions>
      </Dialog>
    }


      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              incoming_action={props.admin_action}
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.academic_data.length}
            />
            <TableBody>
              {stableSort(props.academic_data, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row._id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                    >
                     <TableCell>
                     {row.freeparts ? <div style={{textAlign:'center'}}>{row.freeparts}</div>
                      :
                      <div style={{textAlign:'center'}}>NULL</div>
                      }
                     </TableCell>
                     <TableCell align="center">
                       {row.selected ?
                         <React.Fragment>{row.selected}</React.Fragment>
                         :
                         <React.Fragment>Status Not Applicable</React.Fragment>
                        }
                      </TableCell>

                      <TableCell align="center">
                      {row.covered ?
                        <React.Fragment>{row.covered}</React.Fragment>
                        :
                        <React.Fragment>{row.problem_statement}</React.Fragment>
                      }
                      </TableCell>
                      <TableCell align="center">
                        {row.date} / {row.month} /{row.year}
                      </TableCell>
                      <TableCell align="center">{row.day_order} / {row.hour} </TableCell>
                      <TableCell align="center">
                         {props.admin_action &&
                           <React.Fragment>
                              {row.freefield === 'Academic' ?
                                <InfoIcon onClick={()=>openInfo({
                                  info:true,
                                  info_data:row,
                                })}/>
                                :
                                <div>Not Available</div>
                              }
                          </React.Fragment>
                         }
                      </TableCell>
                        <TableCell align="center">
                           {props.admin_action &&
                             <IconButton >
                               <EditIcon onClick={()=>setEdit({
                                 action:true,
                                 edit_data:row,
                                 covered:row.covered,
                               })} />
                             </IconButton>
                           }
                        </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.academic_data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
