const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const notification = new Schema({
  action: { type: String, unique: false, required: false },
  for: { type: String, unique: false, required: false },
  subject: { type: String, unique: false, required: false },
  details: { type: String, unique: false, required: false },
  ref: { type: Number, unique: false, required: false },
  db_name: { type: String, unique: false, required: false },
  new: { type: Boolean, unique: false, required: false, default:true },
  faculty_id: { type: String, unique: false, required: false },
  covered_topic: { type: String, unique: false, required: false },
  day:{ type: Number, unique: false, required: false },
  month:{ type: Number, unique: false, required: false },
  year:{ type: Number, unique: false, required: false },
  week:{ type: Number, unique: false, required: false },
  slot:{ type: String, unique: false, required: false },
  day_slot_time:{ type: String, unique: false, required: false },
  for_sem: { type: Number, unique: false, required: false },
  for_year: { type: Number, unique: false, required: false },
  for_batch: { type: Number, unique: false, required: false },
  permission: { type: Boolean, unique: false, required: false },
  academic_hour: { type: Number, unique: false, required: false },
  absent_handle: { type: Boolean, unique: false, required: false },
})


notification.plugin(autoIncrement.plugin, { model: 'Notification', field: 'serial', startAt: 1,incrementBy: 1 });

const noti = mongoose.model('Notification', notification)
module.exports = noti
