import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import DatasPass from './decider_type'
require("datejs")


export default class ViewByID extends Component {
  constructor() {
    super()
    this.state ={
      display:'none',
      username:'',
      redirectTo:'',
      send_user:'',
    }
    this.retriveValue = this.retriveValue.bind(this)
    this.authenticate = this.authenticate.bind(this)
  }
retriveValue =(e)=>{
  this.setState({username:e.target.value})
}
  authenticate =(e)=>{
    e.preventDefault()
    axios.post('/ework/user/authenticate',{username:this.state.username})
    .then(res=>{
    if(res.data.username)
    {
      this.setState({
        display:'block',
        send_user:res.data.username
      })
      window.M.toast({html: res.data.allowed , classes:'rounded #ec407a pink darken-1'});
    }
    else{
      this.setState({display:'none'})
        window.M.toast({html: res.data ,classes:'rounded #ec407a pink darken-1'});
    }

    })
  }

  render()
  {
    return(
      <React.Fragment>
         <div className="row">
         <div className="col l3 xl3" />
           <div className="col l6 s12 m12 xl6">
               <div className="form-signup">
                  <div className="row">
                          <div className="input-field">
                             <input type="text" value={this.state.user} onChange={this.retriveValue} id="id" className="validate" required />
                             <label htmlFor="id">Input Official ID</label>
                          </div>
                          <button className="btn col l2 pink right" onClick={this.authenticate} >View</button>
                  </div>
                  <br />
                  {(this.props.action!=='admin')&&<div className="row">
                     <span className="green-text">DISCLAIMER: </span>Please Remember in this section you can only see the User's Data only if they are not coming in your same or upper designation level.
                     One more thing keep in mind users in medium level(like HOD , Professor) have only access to view datas within their department only.
                  </div>}
                </div>
           </div>
         <div className="col l3 s12 m12 xl3" />
         </div>
         <div className="row">
             <div style={{display:this.state.display}}>
                 <DatasPass admin_action={this.props.admin_action} showAction="globaluser" display={this.state.display} username={this.state.send_user}/>
              </div>
         </div>
      </React.Fragment>
    );
  }
}
