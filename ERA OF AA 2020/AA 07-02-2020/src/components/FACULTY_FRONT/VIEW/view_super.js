import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DatasPass from './decider_type'
import ViewByID from './view_by_id'
require("datejs")




export default class BasicView extends Component {
      _isMounted = false;
  constructor() {
    super()
    this.state ={
      start:'',
      isChecked: false,
      history: '',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      agreed:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this._isMounted = true;
    this.getUser();
    M.AutoInit()
  }

    getUser()
    {
      axios.get('/ework/user/'
    )
       .then(response =>{
        if (this._isMounted) {
          if(response.status === 200)
          {
            this.setState({
              loading:false,
            })
         if(response.data.user)
         {

         }
         else{
           this.setState({
             redirectTo:'/ework/faculty',
           });
           window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
         }
       }
       }
       })
    }


  handleComp = (e) =>{
    window.M.toast({html: 'Validating..',outDuration:'3000', classes:'rounded pink'});
    axios.get('/ework/user/validate_list_view')
    .then( response => {
      // console.log(response.data)
        if(response.data.permission === 1)
        {
          window.M.toast({html: 'Accepted!!',outDuration:'3000', classes:'rounded green'});
          this.setState({
            isChecked: !this.state.isChecked,
            start:response.data.data_send,
          })
        }
        else{
          window.M.toast({html: 'Denied Request !!',outDuration:'3000', classes:'rounded red'});
          this.setState({
            isChecked:false,
            redirectTo:'/ework/view',
          })
        }
    });
  }
  render() {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="130" y="10" rx="3" ry="3" width="135" height="10" />
        <rect x="100" y="30" rx="3" ry="3" width="200" height="100" />

      </ContentLoader>
    )
    if(this.state.loading === true)
    {
      return(
        <MyLoader />
      );
    }
    else if(this.state.loading === false){

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

                            <div className="switch center"  style={{marginTop:'18px'}}>
                                <label style={{color:'red',fontSize:'15px'}}>
                                    Already Having Official Id
                                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                                    <span className="lever"></span>
                                    Don't have any Official Id !!
                                </label>
                            </div>
                            <br />
                            <Switch datas={this.state.isChecked} start={this.state.start}/>
      </React.Fragment>
    );
  }
}
  }
}


class Switch extends Component{
  constructor(props)
  {
    super(props)
    this.state={

    }
  }
  render()
  {
    let render_content;
    if(this.props.datas === true)
    {
      render_content =
      <ListShow  start={this.props.start}/>
    }
    else{
      render_content =
      <ViewByID />
    }
    return(
      <React.Fragment>
        {render_content}
      </React.Fragment>
    );
  }
}


class ListShow extends Component {
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      username:'',
      dept:'',
      desgn:'',
      designation:'',
      desgn_list:[],
      department:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user/fetch_view_list',{start:this.props.start})
    .then( res => {
      // console.log(res.data)
        if(res.data)
        {
          this.setState({desgn_list:res.data});
          this.fetchDepartment();
        }
    });
  }
  fetchDepartment=()=>{
    axios.get('/ework/user/fetch_department')
    .then( res => {
      // console.log(res.data)
        if(res.data)
        {
          this.setState({department:res.data})
        }
    });
  }
  handleDesgn =(e)=>{
    this.setState({desgn:e.target.value})
  }
  handleDept = (e)=>{
    this.setState({dept:e.target.value})
  }
  showDiv =(username)=>{
    if(username)
    {
      this.setState({display:'block',username:username})
    }
  }

  render() {
    return (
      <div>
       <div className="row">
          <div className="card col l5">
              <div className="row">
                 <div className="col l6">
                     <h6 className="center pink-text">Select the Designation</h6>

                     <FormControl style={{width:'100%'}}>
                       <InputLabel id="desgntype">Select Here..</InputLabel>
                       <Select
                         labelId="desgntype"
                         id="desgntype"
                         value={this.state.desgn}
                         onChange={this.handleDesgn}
                       >
                       {this.state.desgn_list.map((content,index)=>{
                         return(
                           <MenuItem value={content.designation_name} key={index}>{content.designation_name}</MenuItem>
                       )
                       })}
                       </Select>
                     </FormControl>
                 </div>
                 <div className="col l6">
                    <h6 className="center pink-text">Select the Department</h6>

                                         <FormControl style={{width:'100%'}}>
                                           <InputLabel id="depttype">Select Here..</InputLabel>
                                           <Select
                                             labelId="depttype"
                                             id="depttype"
                                             value={this.state.dept}
                                             onChange={this.handleDept}
                                           >
                                           {this.state.department.map((content,index)=>{
                                             return(
                                               <MenuItem value={content.department_name} key={index}>{content.department_name}</MenuItem>
                                           )
                                           })}
                                           </Select>
                                         </FormControl>
                 </div>
              </div>
          </div>
          <div className="col l7">
            <div className="card">
                <FetchUser dept={this.state.dept} desgn={this.state.desgn} showDiv={this.showDiv} />
         </div>

          </div>
          </div>
          <div className="row">
              <div>
               {this.state.username.length>0 && <DatasPass display={this.state.display} username={this.state.username}/>}
               </div>
          </div>
      </div>
    );
  }
}


class FetchUser extends Component {
  constructor(props) {
    super(props)
    this.state={
      users:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser();
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!==this.props)
    {
      this.fetchUser();
    }
  }
  fetchUser = ()=>{
    axios.post('/ework/user/fetchUser',{department:this.props.dept,designation:this.props.desgn})
    .then( res => {
      // console.log(res.data)
        this.setState({users:res.data})
    });
  }
  render()
  {
    return(
      <React.Fragment>
              <table>
                  <thead>
                  <tr>
                     <th>Serial No.</th>
                     <th>Name</th>
                     <th>Official Id</th>
                     <th>Mail Id</th>
                     <th>Contact No</th>
                     <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  {this.state.users.map((content,index)=>{
                    return(
                      <tr key={index}>
                       <td>{index+1}</td>
                       <td>{content.name}</td>
                       <td>{content.username}</td>
                       <td>{content.mailid}</td>
                       <td>{content.phone}</td>
                       <td><div className="btn #fafafa grey lighten-5 pink-text" onClick={() => this.props.showDiv(content.username)}>View</div></td>
                      </tr>
                  )
                  })}

                  </tbody>
        </table>
      </React.Fragment>
    )
  }
}
