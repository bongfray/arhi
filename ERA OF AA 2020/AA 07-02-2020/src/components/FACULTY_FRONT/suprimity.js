import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import Nav from '../dynnav';
import AddDomain from './admin/ARHI-Operation/handle-domain';
import AddSkill from './admin/ARHI-Operation/handle-skill';


export default class Suprimity extends Component {
  constructor(){
    super()
    this.state={
      redirectTo:null,
      forgotroute:'/ework/user/forgo',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      disp:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    axios.get('/ework/user/')
    .then( res => {
        if(res.data.user){
          this.knowSuprimity()
        }
        else{
          this.setState({redirectTo:'/ework/faculty'})
          window.M.toast({html: 'Not Logged In !!', classes:'rounded #f44336 red'});
        }
    });
  }
  knowSuprimity =()=>{
    axios.post('/ework/user/knowSuprimityStatus')
    .then( res => {
        if(res.data === 'no'){
          this.setState({redirectTo:'/ework/faculty'})
          window.M.toast({html: 'Not Authorized !!', classes:'rounded #f44336 red'});
        }
        else{
          this.setState({proceed:res.data})
        }
    });
  }
  showInfo=()=>{
    this.setState({
      disp:!this.state.disp,
    })
  }
  render() {
    let path;
    let tilt;
    let description =
    <div style={{padding:'20px'}}>
    <h5 className="center red-text">Instructions</h5><br />
    In this page you have to add datas according to the responsibilty you have got through mail.
    Remember,you should not enter any kind of invalid or duplicate data.You can only edit or delete your entered datas.
    Others data can't be editable. You can only view it to understand or to know what are the things are present already.
    Please avoid to enter those existing datas.<br />
    <b className="red-text">Important: </b>If you have been assign a task to show path to achieve domains, in this
    case you need to add the skills or course one should learn to be good in any domain.Like if it's Web Design and the
    level is Advanced,one should learn React Js very well.
    <hr />
    <div style={{padding:'10px'}} onClick={this.showInfo} className="go right">CLOSE</div>
    </div>
    ;
    if(this.state.proceed==="add-skill"){
      tilt =<span>add Skills</span>;
    }
    else if(this.state.proceed==="add-domain"){
      tilt =<span>all Domains</span>;
    }
    else if(this.props.proceed === 'achieve-domain'){
      tilt =<span>show path to achieve Domais</span>;
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
       if(this.state.proceed==="add-skill"){
         path =
         <React.Fragment>
         <div className="row">
           <div className="col l2">
             {this.state.disp === false && <div title="Instructions" onClick={this.showInfo} className="go" style={{marginTop:'10px',marginLeft:'-10px',boxShadow: '0px 10px 10px 2px pink',width:'30px',borderRadius: '0px 20px 20px 0px'}}>
               <i className="material-icons small">chevron_right</i>
             </div>
             }
           </div>
           <div className="col l8">
              <h5 className="center">Your Role is to <span className="pink-text">{tilt}</span></h5>
           </div>
         </div>

        {this.state.disp &&
         <div className="cover_all" onClick={this.showInfo}>
           <div className="up">{description}</div>
        </div>}
          <AddSkill noChange="have" select={this.state.proceed}/>
         </React.Fragment>
       }
       else if((this.state.proceed==="add-domain") || (this.state.proceed==="achieve-domain")){
        path =
        <React.Fragment>
          <div className="row">
            <div className="col l2">
              {this.state.disp === false && <div onClick={this.showInfo} className="go" style={{marginTop:'10px',marginLeft:'-10px',boxShadow: '0px 10px 10px 2px rgba(0, 0, 0, 0.1)',width:'30px',borderRadius: '0px 20px 20px 0px'}}>
                <i className="material-icons small">chevron_right</i>
              </div>
              }
            </div>
            <div className="col l8">
               <h5 className="center">Your Role is to <span className="pink-text">{tilt}</span></h5>
            </div>
          </div>

         {this.state.disp &&
          <div className="cover_all" onClick={this.showInfo}>
            <div className="up">{description}</div>
         </div>}
         <AddDomain noChange="have" select={this.state.proceed} />
        </React.Fragment>
       }
       else{
         path = <div></div>
       }
    return (
      <React.Fragment>
      <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
        {path}
      </React.Fragment>
    );
  }
 }
}
