const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const userSchema = new Schema({
suspension_status: { type: Boolean, unique: false, required: false },
active: { type: Boolean, unique: false, required: false },
username: { type: String, unique: false, required: false },
password: { type: String, unique: false, required: false },
title: { type: String, unique: false, required: false },
name: { type: String, unique: false, required: false },
mailid: { type: String, unique: false, required: false },
phone: { type: String, unique: false, required: false },
cnf_pswd: { type: String, unique: false, required: false },
campus: { type: String, unique: false, required: false },
dept: { type: String, unique: false, required: false },
desgn: { type: String, unique: false, required: false },
dob: { type: String, unique: false, required: false },
count: { type: Number, unique: false, required: false },
resetPasswordToken: { type: String, unique: false, required: false },
deptToken: { type: String, unique: false, required: false },
resetPasswordExpires: { type: String, unique: false, required: false },
week: { type: Number, unique: false, required: false },
h_order: { type: Number, unique: false, required: false },
activated:{type: Date, unique: false, required: false },
activated_by:{type: String, unique: false, required: false },
inserted_by:{type: String, unique: false, required: false },
suprimity:{type: Boolean, unique: false, required: false },
task:{type: String, unique: false, required: false },
degree: { type: String, unique: false, required: false },
sem: { type: Number, unique: false, required: false },
year: { type: Number, unique: false, required: false },
batch: { type: Number, unique: false, required: false },
priority: { type: Number, unique: false, required: false },
})


userSchema.methods = {
	checkPassword: function (inputPassword) {
		return bcrypt.compareSync(inputPassword, this.password)
	},
	hashPassword: plainTextPassword => {
		return bcrypt.hashSync(plainTextPassword, 10)
	}
}



userSchema.pre('save', function (next) {
	if (!this.password) {
		next()
	} else {
		this.password = this.hashPassword(this.password)
		next()
	}
})



const User = mongoose.model('User', userSchema)
module.exports = User
