import React, { Component } from 'react';
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import ForeignExtra from './foreign_extra'
import TimeDiv from '../TimeTable/slot2'
require("datejs")


export default class ownExtra extends Component {
  constructor()
  {
    super()
    this.state={
      active:0,
      loading: true,
      isChecked: false,
      history:'',
      redirectTo:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleComp = (e) =>{
    this.setState({
      isChecked: !this.state.isChecked,
      history: e.target.value,
    })
  }
  componentDidMount()
  {
    this.loggedin()
  }
  loggedin = ()=>{
    axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/ework/faculty'})
         window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }


  handleSet=(e)=>{
    this.setState({
      active: e,
    })
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "col l6 xl6 go center #69f0ae green accent-2 black-text active-pressed";
      }
      return "col l6 xl6 #e0e0e0 grey lighten-2 center active-pressed go";
  }

  render() {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div>
      <div className="row">
         <div className="col l4 xl4 m5 s4"/>
         <div className="col l4 xl4 s4 m2">
           <div className="row" style={{marginTop:'15px'}}>
              <div onClick={(e)=>{this.handleSet(0)}} className={this.color(0)}>Own Extra</div>
              <div onClick={(e)=>{this.handleSet(1)}} className={this.color(1)}>Foreign Extra</div>
           </div>
         </div>
         <div className="col l5 xl5 m5 s4"/>
      </div>
        <br /><br />
        <InputValue datas={this.state.active} username={this.state.username} />
      </div>
      </React.Fragment>
    );
  }
  }
}



class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
      date:'',
      dayorder:'',
      hour:'',
      day_order:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  addAllot =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }
  componentDidMount()
  {
    this.fetchdayorder()
  }
  fetchdayorder =()=>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{
          this.setState({
            day_order: response.data.day_order,
          });
    });
  }
  render(){
    if(this.props.datas === 1)
    {
      return(
        <React.Fragment>
          <ForeignExtra />
        </React.Fragment>
      );
    }
    else
    {
      return(
        <React.Fragment>
          <OwnExtra username={this.props.username} day_order={this.state.day_order} />
        </React.Fragment>
      );
    }
  }
}


class  OwnExtra extends Component {
  constructor() {
    super()
    this.state={
      compense_data:[],
      modal: false,
      color:'red',
      notfound:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    M.AutoInit()
    this.fetchCompenseClasses()

  }
  fetchCompenseClasses = ()=>{
    axios.get('/ework/user/fetchcompense')
    .then( res => {
      if(res.data.length === 0)
      {
        this.setState({notfound:'No Data Found !!'})
      }
      else{
        this.setState({compense_data:res.data})
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}

color =(hour,day,month,year) =>{
	axios.post('/ework/user/fetchfrom', {
		slot_time: this.props.day_order+'.'+hour,
		day: day,
		month:month,
		year: year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
					loading:false,
				})
		}
	})

}

  render()
  {
    // console.log(this.props.day_order)
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    return(
      <React.Fragment>
        <div className="row card" style={{marginLeft:'10px',marginRight:'10px'}}>
         <div className="card-title center" style={{background:'linear-gradient(to bottom, #33ccff 0%, #99ffcc 100%'}}>Slots You want to compensate</div>
         <div className="card-content row">
         <h5 className="center">{this.state.notfound}</h5>
         {this.state.compense_data.map((content,index)=>{
           return(
             <div className="col l4"  style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}}>
             <p className="center">You missed this slot on {content.date}-{month}-{year}</p>
             {this.state.color === 'red'?
             <div className="red-text">
             <TimeDiv day_order={this.props.day_order} usern={this.props.username} day={day} month={month} year={year}
              num={content.problem_compensation_hour} slots={content.slot} time={content.time}
              day_slot_time={content.day_sl}
              displayModal={this.state.modal}
              closeModal={this.selectModal}
              cday={content.date}
              color={() => this.color(content.problem_compensation_hour,day,month,year)}
              />
              </div>
              :
              <div className="card green-text"> COMPLETED</div>
           }
              </div>
           )
         })}
         </div>
        </div>
      </React.Fragment>
    );
  }
}
