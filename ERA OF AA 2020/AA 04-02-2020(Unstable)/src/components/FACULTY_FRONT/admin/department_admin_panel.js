import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import Nav from '../../dynnav'
import axios from 'axios'

export default class DepartmentAdmin extends Component {
  constructor()
  {
    super()
    this.state={
      radio:[{name:'radio2',value:'Validate User'},{name:'radio1',value:'Accepted Requests'}],
      isChecked: false,
      choosed: '',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      login:'/ework/flogin',
      nav_route: '',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
    handleChecked =(e,index,color)=>{
        this.setState({
            isChecked: !this.state.isChecked,
            choosed: e.target.value
        });
    }


  render() {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
    return(
        <React.Fragment>
       <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
       <div className="row">
          <div className="col s12 m12 l2 xl2">
            {this.state.radio.map((content,index)=>(
                <div key={index}>
                    <div className="col l12 s12 m12 xl12 form-signup">
                    <p>
                    <label>
                    <input className="with-gap" type='radio' id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                    <span style={{color:'green'}}><b>{content.value}</b></span>
                    </label>
                    </p>
                    </div>
              </div>
            ))}
            </div>
            <div className="col s12 m12 l10 xl10">
               <Display choosed={this.state.choosed}/>
            </div>
       </div>

            <div className="row">

            </div>
        </React.Fragment>
    );
}
  }
}



class Display extends Component {
  constructor() {
    super()
    this.state={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    let content;
    if(this.props.choosed === 'Accepted Requests')
    {
      content = <AcceptedRequests />
    }
    else if(this.props.choosed === 'Validate User')
    {
      content =<ValidateUser />
    }
    else{
      content = <div></div>
    }
    return(
      <React.Fragment>
       {content}
      </React.Fragment>
    );
  }
}


class ValidateUser extends Component{
  constructor()
  {
    super()
    this.state={
      display:'none',
      requests:[],
      recheck:[],
      notfound:'',
      isChecked:false,
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchRequests()
  }
  fetchRequests = ()=>{
    axios.get('/ework/user/fetch_request')
    .then(res=>{
        this.setState({display:'block',requests:res.data})
    })
  }
  Active =(e,index,username)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })

      axios.post('/ework/user/active_user',{
        username: username,
      })
      .then(res=>{
            window.M.toast({html: 'Activated !!',outDuration:'9000', classes:'rounded green darken-2'});
            const { requests } = this.state;

                  this.setState({
                    requests: requests.filter(product => product.username !== username)
                 })
      })
  }
  render()
  {
    return(
      <React.Fragment>
        <p className="center">You Will able to see the requests only of your department and campus</p>
        <div className="row">
           <div className="col l12">
              <div className="card">
                <div className="card-title center pink white-text">Accept Request</div>
                <div className="card-content">
                  {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                  <div style={{display:this.state.display}}>
                  {this.state.requests.length === 0 ?
                  <h6 className="center">No Request Found</h6>
                  :
                  <React.Fragment>
                  <div className="row">
                    <div className="row">
                         <div className="col l1 left"><b>Index</b></div>
                          <div className="col l3 center"><b>Name</b></div>
                          <div className="col l2 center"><b>Official Id</b></div>
                          <div className="col l4 center"><b>Mail Id</b></div>
                          <div className="col l2 center"><b>Action</b></div>
                    </div>
                    <hr />
                     {this.state.requests.map((content,index)=>(
                             <div className="row"  key={index}>
                              <div className="col l1 left">{index+1}</div>
                              <div className="col l3 center">{content.name}</div>
                              <div className="col l2 center">{content.username}</div>
                              <div className="col l4 center">{content.mailid}</div>
                              <div className="col l2 center">
                                  <div className="switch">
                                    <label>
                                      <input  checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content.username)}} type="checkbox" />
                                      <span className="lever"></span>
                                    </label>
                                  </div>
                              </div>
                            </div>
                     ))}
                   </div>
                  </React.Fragment>
                }
                </div>
                </div>
              </div>
           </div>
        </div>

      </React.Fragment>
    )
  }
}


class AcceptedRequests extends Component{
  constructor()
  {
    super()
    this.state={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    return(
      <React.Fragment>
        Accepted Requests
      </React.Fragment>
    )
  }
}
