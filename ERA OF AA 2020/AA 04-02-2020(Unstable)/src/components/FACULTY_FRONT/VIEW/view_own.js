import React, {} from 'react'
import { } from 'react-router-dom'
import M from 'materialize-css'
import { Redirect} from 'react-router-dom'
import Nav from '../../dynnav'
import axios from 'axios'
import ContentLoader from "react-content-loader"
import DeciderPath from './decider_type'






 export default class ViewSubject extends React.Component{
      _isMounted = false;
  constructor()
  {
    super()
    this.state={
      loading:true,
      option:'',
      username:'',
      redirectTo:'',
      modal: false,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
 componentDidMount()
 {
   this._isMounted = true;
   this.getUser();
   M.AutoInit()
 }
 getUser()
 {
   axios.get('/ework/user/'
 )
    .then(response =>{
     if (this._isMounted) {
       if(response.status === 200)
       {
         this.setState({
           loading:false,
         })
      if(response.data.user)
      {

      }
      else{
        this.setState({
          redirectTo:'/ework/faculty',
        });
        window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    }
    }
    })
 }



 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }
 render()
 {
//console.log(this.props.display)
   const MyLoader = () => (
     <ContentLoader
       height={160}
       width={400}
       speed={2}
       primaryColor="#f3f3f3"
       secondaryColor="#c0c0c0"
     >
       <rect x="95" y="15" rx="3" ry="3" width="220" height="20" />
       <rect x="320" y="15" rx="3" ry="3" width="20" height="20" />

     </ContentLoader>
   )
   if(this.state.loading === true)
   {
     return(
       <MyLoader />
     );
   }
   else if(this.state.loading === false){

   if (this.state.redirectTo) {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
 } else {
   return(

     <React.Fragment>
        <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
        <DeciderPath display="none" username={this.state.username} showAction="localuser" />
     </React.Fragment>
 );
}
}
 }
}
