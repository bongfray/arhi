import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect, Link } from 'react-router-dom'
import Nav from '../../dynnav'
import axios from 'axios'
import ContentLoader from "react-content-loader"
require("datejs")



export default class Nstart extends Component {
    _isMounted = false;
  constructor()
  {
    super()
    this.state = {
      redirectTo:'',
      modal: false,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      allowed:false,
      loading:true,
      fetching:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  openModal =() =>
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidMount()
  {
    this._isMounted = true;
    this.getUser();
  }

  getUser()
  {
    axios.get('/ework/user/'
  )
     .then(response =>{
      if (this._isMounted) {
        if(response.status === 200)
        {
          this.setState({
            loading:false,
          })
       if(response.data.user)
       {
         this.openModal();
         this.knowSuprimity();
       }
       else{
         this.setState({
           redirectTo:'/ework/faculty',
         });
         window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
       }
     }
     }
     })
  }


  knowSuprimity =()=>{
    axios.get('/ework/user/validate_list_view')
    .then( response => {
        if(response.data.permission === 1)
        {
          this.setState({
            allowed:true,
            fetching:false,
          })
        }
        else{
          this.setState({
            allowed:false,
            fetching:false,
          })
        }
    });
  }
  render() {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="95" y="50" rx="3" ry="3" width="220" height="80" />

      </ContentLoader>
    )
    if(this.state.loading === true)
    {
      return(
        <MyLoader />
      );
    }
    else if(this.state.loading === false){

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
       <div className="Navigator">
       <Insta
           allowed ={this.state.allowed}
           displayModal={this.state.modal}
           closeModal={this.openModal}
           fetching={this.state.fetching}
       />
       </div>
       </React.Fragment>
    );
  }
}
  }
}

const Insta = props =>{
    const divStyle =
    {
      display: props.displayModal ? 'block' : 'none',
      marginTop:'170px',
    };

     return(
       <React.Fragment>
       {props.fetching === false ?
       <div
         className="modal instructionmodal"
         style={divStyle}>
         <div className="modal-content">
           <h4 className="center">View</h4><br /><br />

           { props.allowed ?  <div className="mcont row"  style={{marginBottom:'35px'}}>
               <Link to="/ework/view_own" className="col s5 waves-effect btn green white-text">View Own Data</Link>
                <div className="col s2"></div>
                <Link to="/ework/view_super" className="col s5 waves-effect pink btn white-text">SuperView</Link>
             </div>
             :
              <div className="mcont row"  style={{marginBottom:'35px'}}>
                <div className="col l4 xl4 s3 m3"></div>
                <Link to="/view_own" className="col l4 xl4 s6 m6 waves-effect btn green white-text">View Own Data</Link>
                 <div className="col l4 xl4 s3 m3"></div>
              </div>
            }
         </div>
       </div>
         :
         <div className="center" style={{marginTop:'100px'}}>
             <div className="preloader-wrapper big active">
               <div className="spinner-layer spinner-red">
                   <div className="circle-clipper left">
                    <div className="circle"></div>
                   </div><div className="gap-patch">
                    <div className="circle"></div>
                   </div><div className="circle-clipper right">
                    <div className="circle"></div>
                   </div>
               </div>
             </div>
         </div>
     }
       </React.Fragment>
     );
}
