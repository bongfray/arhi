import React, { } from 'react'
import { } from 'react-router-dom'
import {Redirect } from 'react-router-dom'
import M from 'materialize-css'
import Modal2 from './Modal'

/*---------------------------------------------------------Code for regular classes time table------------------------------------ */



export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',
      modal: false,
    };

  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
    M.AutoInit();
  }
  render()
  {

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <div className="root-of-time">
        <div className="node">
        <div onClick={ this.selectModal } className="red white-text card hoverable">
        <p className="left yellow-text" style={{marginLeft:'5px'}}>{this.props.num}</p>
        <p className="center">{this.props.time}</p>
        <p className="go center"><b>{this.props.slots}</b></p>
        </div>
   <Modal2
   datas_a={this.props.datas_a}
   day={this.props.day}
   month={this.props.month}
   year={this.props.year}
   displayModal={this.state.modal}
   closeModal={this.selectModal}
   day_slot_time={this.props.day_slot_time}
   usern={this.props.usern}
   day_order={this.props.day_order}
   color={this.props.color}
   time={this.props.time}
   slot={this.props.slots}
   day_sl={this.props.day_sl}
   cday={this.props.cday}
   num={this.props.num}
   start_time={this.props.start_time}
   end_time={this.props.end_time}
   />
   </div>
   </div>
      )
  }
  }
}



/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */
