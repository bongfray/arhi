
import React from 'react'
const Insta = props => {
  // console.log(props.displayModal)
     const divStyle = {
          display: props.displayModal ? 'block' : 'none'
     };
     function closeModal(e) {
        e.stopPropagation()
        props.closeModal()
     }
     return (
       <div className="cover" style={divStyle} onClick={ closeModal }>
       <div
         className="up">
         <div className="modal-content" style={{padding:'20px'}}>
           <h4 className="center">Instructions</h4>
           <p >1. Click on the "Select Here" part in the left hand side<br/>
              2. When you click on Regular Class, some contents will appear.<br />
              3. The contents with <span style={{color:'red'}}>red color</span> implies the task is pending till now.<br />
              4. By clicking on the each div with red color,you can easily see the details of the following div and there itself you have to submit datas.<br />
              5. And contents with <span style={{color:'green'}}>green color</span> is the confirmation of your task completion.<br />
              6. Remember one thing,you have only 48 hours to edit a submitted data.<br />
              7. On the left hand side in the top of the regular class page you can see Yesterday dayorder navigation.<br />
              8. Within the 48 hour, this page will let you to edit the submitted datas only for today, means for 24 hour,
                  Rest of the 24 Hrs will be rendered in yesterday's dayorder slot.<br />
              9. In case you cannot able to see any hour in your window, that may be because of two reason : <br />
              &emsp;&emsp; A. You are absent today that's why no alloted slots is rendered.<br />
              &emsp;&emsp; B. You had requested previously to handle some cancelled slot today.<br />
                <span className="center" style={{color: 'red'}}>If it's your first time login. This Insturction Steps will come frequently as you refresh.To avoid this kindly login second time.</span>
           </p>

         </div>
         <div className="modal-footer">
           <span style={{margin:'0px 5px 15px 0px'}} className="right styled-btn pink-text" onClick={ closeModal }>GOT IT</span>
         </div>
       </div>
       </div>
     );
}
export default Insta;
