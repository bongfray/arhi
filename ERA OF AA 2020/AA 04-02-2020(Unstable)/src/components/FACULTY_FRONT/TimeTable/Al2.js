import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


export default class Allot extends React.Component {
  constructor() {
    super()
    this.state = {
       freeparts:'Curriculum',
        sending:'',
        selected:'',
        covered_topic:'',
        problem_statement:'',
        compday:'',
        comphour:'',
        allot:'',
        count: 0,
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        date:'',
        year:'',
        month:'',
        compense_faculty:'',
    }
  }


  handleChecked =(e) =>{
    this.setState({
      selected: e.target.value,
    })
  }

  updateAllotV =(userObject) => {
    this.setState(userObject)
  }


  closeModal=()=>{
    this.props.closeModal();
  }
  colorChange=()=>{
    this.props.color()
  }

handleAlloted =(e) =>{
  let freeparts,verified,positive_count,negative_count;
  e.preventDefault();
  if(this.state.selected === "Problem")
  {
    freeparts = '';
  }
  else
  {
    freeparts = "Curriculum";
    verified = false;
    positive_count = 0;
    negative_count = 0;
  }

  if(!this.state.selected)
  {
      window.M.toast({html: 'Enter All the Details First',classes:'rounded pink'});
  }
  else{
    if(this.state.selected === 'Problem')
    {
      if(!this.state.problem_statement)
      {
        window.M.toast({html: 'You should mention the reason !!', classes:'rounded pink'});
      }
      else
      {
        if((this.state.comphour) || (this.state.compday))
        {
          if((!this.state.compday) || (!this.state.comphour))
          {
            window.M.toast({html: 'Enter All the Details First', classes:'rounded pink'});
          }
          else
          {
            this.sendAllot(freeparts,verified,positive_count,negative_count,this.state.selected,'',this.state.problem_statement,this.state.compday,this.state.comphour,'');
          }
        }
        else
        {
          if(this.state.compense_faculty)
          {
            this.checkWithFaculty(freeparts,verified,positive_count,negative_count,this.state.selected,'',this.state.problem_statement,'','','',this.state.compense_faculty)
          }
        }
      }
    }
    else if(this.state.selected === 'Completed')
    {
      if(!this.state.covered_topic)
      {

            window.M.toast({html: 'Enter All the Details First',classes:'rounded pink'});
      }
      else
      {
        this.sendAllot(freeparts,verified,positive_count,negative_count,this.state.selected,this.state.covered_topic,null,false,false,this.state.saved_slots);
      }
    }
  }
}


sendAllot=(freeparts,verified,positive_count,negative_count,selected,covered_topic,problem_statement,compday,comphour,saved_slots)=>
{
          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var myDate = new Date.today();
          var week =myDate.getWeek();

        axios.post('/ework/user/timeallot', {
          username: this.props.usern,
          freefield:"Academic",
          freeparts:freeparts,
          date:this.props.day,
          month: this.props.month,
          year: this.props.year,
          week:week,
          day_order: this.props.day_order,
          hour:this.props.num,
          for_year:this.props.datas_a.year,
          for_batch:this.props.datas_a.batch,
          for_sem:this.props.datas_a.sem,
          day_slot_time: this.props.day_order+'.'+this.props.num,
          selected: selected,
          covered: covered_topic,
          problem_statement: problem_statement,
          saved_slots: saved_slots,
          slot:this.props.slot,
          time:this.props.time,
          problem_compensation_date: compday,
          problem_compensation_hour: comphour,
          verified:verified,
          positive_count:positive_count,
          negative_count:negative_count,
          compense_faculty:this.state.compense_faculty,
        },
        this.setState({
          sending: 'disabled',
        })
      )
          .then(response => {
            this.setState({sending:''})
            if(response.status===200){
               if(response.data==='done')
              {
                if(freeparts === 'Curriculum')
                {
                  this.sendNoti_To_Student()
                }
                window.M.toast({html: 'Success !!',classes:'rounded #ec407a green lighten-1'});
                if(this.props.foreign_ref === "true")
                {
                          this.props.closeref();
                }
                else{
                  this.closeModal();
                  this.colorChange();
                }
              }
              else if(response.data === 'no')
              {
                window.M.toast({html: 'Already Submitted !!', classes:'rounded red'});
                 this.closeModal();
              }
              else if(response.data === 'spam')
              {
                  window.M.toast({html: 'Invalid Request !!', classes:'rounded red'});
              }
            }
          })
}


checkWithFaculty=(freeparts,verified,positive_count,negative_count,selected,covered_topic,problem_statement,compday,comphour,saved_slots,compense_faculty)=>{
  axios.post('/ework/user/check_possibility',{id:compense_faculty,
    hour:this.props.num,day_order:this.props.day_order,year:this.props.datas_a.year,
    batch:this.props.datas_a.batch,sem:this.props.datas_a.sem,slot:this.props.slot,})
  .then( res => {
    if(res.data === 'no')
    {
      window.M.toast({html: 'Invalid Request !!', classes:'rounded  red'});
        this.setState(this.initialState);
      return false;
    }
    else
    {
          window.M.toast({html: 'Submitting...', classes:'rounded orange black-text'});
          this.sendAllot(freeparts,verified,positive_count,negative_count,selected,'',problem_statement,'','','');
    }
  });
}

sendNoti_To_Student=()=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();

  axios.post('/ework/user2/send_noti_on_class_complition',
  {week:week,slot:this.props.slot,for_year:this.props.datas_a.year,for_batch:this.props.datas_a.batch,
  for_sem:this.props.datas_a.sem,
  day_slot_time:this.props.day_order+this.props.day_slot_time,action:'Class Completed',
  faculty_id:this.props.usern,covered_topic:this.state.covered_topic,day:
  this.props.day,month:this.props.month,year:this.props.year})
  .then( res => {

  });
}

render(){
//  console.log(this.props)
  return(
    <React.Fragment>
    <span className="pink-text darken-4">Reminder: </span><span>This slot is booked at the beginig of Semester.You are not allowed to edit it.</span>
    <div className="row">
      <div className="col l6 xl6 s12 m12">
      <p>
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Completed' onChange={this.handleChecked} />
          <span className="center green-text"><b>Class/Work Completed</b></span>
        </label>
     </p>
      </div>
      <div className="col l6 xl6 m12 s12">
      <p>
      {!this.props.cday &&
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Problem' onChange={this.handleChecked} />
          <span className="center red-text"><b>Problem With Work/Class Completion(like Slot Cancelled)</b></span>
        </label>
      }
     </p>
      </div>
    </div>
    <DivOpen prop_datas={this.props} selected={this.state.selected} updateAllotV={this.updateAllotV}/>
  <button className="waves-effect btn col l2 s4 xl2 m2 blue-grey darken-2 sup right" disabled={this.state.sending} onClick={this.handleAlloted}>SUBMIT</button>
   </React.Fragment>
  );
}
}





/*-------for check radio button----------------------------------- */

 class DivOpen extends Component{
   constructor(){
     super();
     this.state ={
       covered_topic:'',
       problem_statement:'',
     }
     this.componentDidMount = this.componentDidMount.bind(this);
   }
   componentDidMount(){

   }
handleAllotData= (evt) =>{
     const value = evt.target.value;
     this.setState({
       [evt.target.name]: value
     });
     this.props.updateAllotV({
       [evt.target.name]: value,
     });
   }



   render(){

     if(this.props.selected ==="Completed")
     {
       return(
         <div className="input-field">
         <input id="covered" type="text" name="covered_topic" value={this.state.covered_topic}  onChange={this.handleAllotData}/>
         <label htmlFor="covered">Enter the the things covered in this alloted slot.</label>
         </div>
       );
     }
     else if(this.props.selected === "Problem")
     {
       return(
         <React.Fragment>
         <div className="input-field">
         <input id="prob" type="text" name="problem_statement" className="validate" value={this.state.problem_statement} onChange={this.handleAllotData} required/>
           <label htmlFor="prob" >Enter the reason</label>
         </div>
         <div className="">
           <Switch prop_datas={this.props.prop_datas} updateAllotV={this.props.updateAllotV}/>
         </div>
         </React.Fragment>
       );
     }
     else
     {
       return(
       <div></div>
     );
     }
   }
 }




  class Switch extends React.Component {

      constructor ( props ) {
          super( props );
  		this.state = {
  			isChecked: false,
        day_set:[],
  		}
      this.componentDidMount = this.componentDidMount.bind(this)
      }
      componentDidMount()
      {
        var year = parseInt(Date.today().toString("yyyy"));
        if(this.leapYear(year)=== true)
        {
          this.setState({
            day_set: [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
          {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
        ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
          })
        }
        else
        {
          this.setState({
            day_set: [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
          {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
        ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
          })
        }
      }

      leapYear(year)
      {
       return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
      }

      statusOfCompensation= (e) =>{
        this.setState({
          isChecked: !this.state.isChecked,
        })
      }
      render () {

          return(
            <div>

              <div className="switch center">
                  <label style={{fontSize:'15px',color:'black'}}>
                      HandOver to Other Faculty
                      <input  checked={ this.state.isChecked } onChange={ this.statusOfCompensation} type="checkbox" />
                      <span className="lever"></span>
                      Want to compensate this on other day ?
                  </label>
              </div>
              <br />
              <InputValue day_set={this.state.day_set} prop_datas={this.props.prop_datas} datas={this.state.isChecked} updateAllotV={this.props.updateAllotV}/>
            </div>
          );
      }

  }

  class InputValue extends Component{
    constructor()
    {
      super()
      this.state ={
        compday:'',
        comphour:'',
        compense_faculty:'',
        hour_set:[{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},
        {value:7},{value:8},{value:9},{value:10}]
      }
    }

    handleComData =(evt)=>{
      const value = evt.target.value;
      this.setState({
        [evt.target.name]: value
      });
      this.props.updateAllotV({
        [evt.target.name]: value,
      });
    }


    render(){
      let actual_days;
      if(this.props.day_set.length>0)
      {
          var month = parseInt(Date.today().toString("MM"));
          var today = parseInt(Date.today().toString("dd"));
          var days=[];
          const day_data= this.props.day_set.filter(item => item.value===month);
          for(var j=1;j<=day_data[0].day;j++)
          {
            days.push(j);
          }
          actual_days= days.filter(item1=>item1>today)
      }
      let time = parseFloat(new Date().toString("HH.mm"));
      if(this.props.datas === true)
      {
        return(
          <div className="row">
           <div className="col l1"/>
           <div className="col l10 particular" style={{padding:'15px'}}>
              <div className="row">
                  <div className="col l3 xl3 hide-on-mid-and-down"/>
                  <div className="col l3 s12 m12 xl3">
                       <FormControl style={{width:'100%'}}>
                          <InputLabel id="month">Day</InputLabel>
                          <Select
                            labelId="month"
                            id="month"
                            name="compday"
                            value={this.state.compday}
                            onChange={this.handleComData}
                          >
                          {actual_days.map((content,index)=>{
                                  return(
                                    <MenuItem key={index}  value={content}>{content}</MenuItem>
                                  )
                            })}
                          </Select>
                        </FormControl>
                  </div>
                   <div className="col l3 s12 m12 xl3">
                         <FormControl style={{width:'100%'}}>
                            <InputLabel id="hour">Hour</InputLabel>
                            <Select
                              labelId="hour"
                              id="hour"
                              name="comphour"
                              value={this.state.comphour}
                              onChange={this.handleComData}
                            >
                            {this.state.hour_set.map((content,index)=>{
                                    return(
                                      <MenuItem key={index}  value={content.value}>{content.value}</MenuItem>
                                    )
                              })}
                            </Select>
                          </FormControl>
                   </div>
                   <div className="col l3 xl3 hide-on-mid-and-down" />
              </div>
              <div className="left col l12 black-text">
                 <span className="red-text"><b><u>REMEMBER : </u></b></span><br />
                 1.  You are not allowed to put a date beyond this month.<br />
                 2.  In this case if today is the last date of the current month,you will miss this slot to enter data for current month.<br />
                 3.  In case you are typing invalid date and also submitting that, system will not render that slot.<br />
                 4.  One more thing, the date you are submitting,system will not render that hour on that day in Today's DayOrder Section.
                     You can view that hour in Extra Slot on your prefered date.<br />
                 5.  The compensation hour your are typing should not collide with your alloted slot.In that
                     case,system will not render that slot on your prefered date.<br />
                 6.  You should not put today's date as a compensation date.
              </div>
              </div>
           <div className="col l1"/>
          </div>
        );
      }
      else {
        return(
          <div className="row">
          <div className="col l4"/>
          <div className="col l4 particular">
              {((parseFloat(this.props.prop_datas.end_time))<time) ?
                <React.Fragment>
                  <h6 className="center red-text">Sorry! Slots can only be handovered within that hour.</h6>
                </React.Fragment>
                :
                <div className="input-field">
                  <input type="text" id="faculty_id_c" value={this.state.compense_faculty} name="compense_faculty"
                   onChange={this.handleComData} />
                   <label htmlFor="faculty_id_c">Faculty Id</label>
                </div>
              }
          </div>
          <div className="col l4"/>
          </div>
        )

      }
    }
  }
