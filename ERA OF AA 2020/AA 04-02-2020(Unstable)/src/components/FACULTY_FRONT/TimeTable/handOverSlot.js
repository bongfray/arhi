import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Redirect } from 'react-router-dom'
import axios from 'axios'
import { Button , Modal } from 'react-materialize'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
require("datejs")


export default class HandOverSlot extends Component {
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  axios.get('/ework/user/')
  .then( res => {
      if(res.data)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/ework/faculty'})
      }
  });
}
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data){
     let apiUrl;
     var addroute,editroute;
     addroute="/ework/user/add_handover_slots";
     editroute = "";
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl,data)
         .then(response => {
           if(response.data === 'no')
           {
                   window.M.toast({html: 'Already Saved !!', classes:'rounded #ec407a pink lighten-1'});
                   this.setState({
                     isAddProduct: false,
                     isEditProduct: false
                   })
           }
           else
           {
              window.M.toast({html: 'Sent !!', classes:'rounded #ec407a pink lighten-1'});
              this.setState({
               response: response.data,
               isAddProduct: false,
               isEditProduct: false
             })
           }
         })

   }
   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Faculty ID",
                   name: "faculty_id",
                   placeholder: "Enter the Faculty ID",
                   type: "text",
                   grid: 2,
                   div:"col l2 xl2 s2 m2 center",
                 },
                 {
                   header: "Slot",
                   name: "slot",
                   placeholder: "Enter the Slot",
                   type: "text",
                   grid: 2,
                   div:"col l2 xl2 s2 m2 center",
                 },
                 {
                   header: "Mention Hour",
                   name: "hour",
                   placeholder: "none",
                   type: "text",
                   grid: 2,
                   div:"col l1 xl1 s2 m2 center",
                 },
                 {
                   header: "For Year",
                   name: "year",
                   placeholder: "none",
                   type: "text",
                   grid: 2,
                   div:"col l1 xl1 s2 m2 center",
                 },
                 {
                   header: "For Semester",
                   name: "sem",
                   placeholder: "none",
                   type: "text",
                   grid: 2,
                   div:"col l1 xl1 s2 m2 center",
                 },
                 {
                   header: "For Batch",
                   name: "batch",
                   placeholder: "none",
                   type: "text",
                   grid: 2,
                   div:"col l2 xl2 s2 m2 center",
                 },
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <Add  action="Foreign HandOver Slot"  day_order={this.props.day_order} data={data1} order_from={this.state.username} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div>
    {!this.state.isAddProduct && <ShowAndEdit action="Foreign HandOver Slot" data={data1} />}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</React.Fragment>
);
}
}
}




class Add extends Component{
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      action:'',
      order_from:'',
      faculty_id:'',
      hour:'',
      status:false,
      day_order:'',
      slot:'',
      expired:'',
      year:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


  componentDidMount(){
    this.setState({
      action:this.props.action,
      order_from:this.props.order_from,
      status:false,
      day_order:this.props.day_order
    })
  }

  handleSubmit(event) {
    window.M.toast({html: 'Validating....', classes:'rounded  yellow black-text'});
      event.preventDefault();
      if((this.state.faculty_id)  && (this.state.hour ))
      {
        axios.post('/ework/user/check_possibility',{id:this.state.faculty_id,
        hour:this.state.hour,day_order:this.props.day_order.toString(),sem:parseInt(this.state.sem),
        batch:parseInt(this.state.batch),year:parseInt(this.state.year),slot:this.state.slot})
        .then( res => {
          if(res.data === 'no')
          {
            window.M.toast({html: 'Invalid Request !!', classes:'rounded  red'});
              this.setState(this.initialState);
            return false;
          }
          else
          {
                window.M.toast({html: 'Submitting...', classes:'rounded orange black-text'});
                this.setState({slot:res.data})
                this.props.onFormSubmit(this.state);
                this.setState(this.initialState);
          }
        });

      }
      else
      {
        window.M.toast({html: 'Enter all the Details !!',classes:'rounded  red lighten-1'});
      }
  }

  render() {
    return(
    <React.Fragment>
      <div className="row">
          {this.props.data.fielddata.map((content,index)=>(
            <div className={content.div} key={index}>
             {((content.placeholder ==='none')) ?
             <React.Fragment>
               {content.name ==="hour" &&
                 <FormControl style={{width:'100%'}}>
                    <InputLabel id="hour">Hour</InputLabel>
                    <Select
                      labelId="hour"
                      id="hour"
                      name={content.name}
                      value={this.state[content.name]}
                      onChange={e => this.handleD(e, index)}
                    >
                      <MenuItem value="1">1</MenuItem>
                      <MenuItem value="2">2</MenuItem>
                      <MenuItem value="3">3</MenuItem>
                      <MenuItem value="4">4</MenuItem>
                      <MenuItem value="5">5</MenuItem>
                      <MenuItem value="6">6</MenuItem>
                      <MenuItem value="7">7</MenuItem>
                      <MenuItem value="8">7</MenuItem>
                      <MenuItem value="9">9</MenuItem>
                      <MenuItem value="10">10</MenuItem>

                    </Select>
                  </FormControl>
                }
                {content.name ==="sem" &&
                  <FormControl style={{width:'100%'}}>
                     <InputLabel id="sem">Sem</InputLabel>
                     <Select
                       labelId="sem"
                       id="sem"
                       name={content.name}
                       value={this.state[content.name]}
                       onChange={e => this.handleD(e, index)}
                     >
                       <MenuItem value="1">1</MenuItem>
                       <MenuItem value="2">2</MenuItem>
                       <MenuItem value="3">3</MenuItem>
                       <MenuItem value="4">4</MenuItem>
                       <MenuItem value="5">5</MenuItem>
                       <MenuItem value="6">6</MenuItem>
                       <MenuItem value="7">7</MenuItem>
                       <MenuItem value="8">7</MenuItem>
                       <MenuItem value="9">9</MenuItem>
                       <MenuItem value="10">10</MenuItem>

                     </Select>
                   </FormControl>
                 }
                 {content.name ==="year" &&
                   <FormControl style={{width:'100%'}}>
                      <InputLabel id="year">Year</InputLabel>
                      <Select
                        labelId="year"
                        id="year"
                        name={content.name}
                        value={this.state[content.name]}
                        onChange={e => this.handleD(e, index)}
                      >
                        <MenuItem value="1">1</MenuItem>
                        <MenuItem value="2">2</MenuItem>
                        <MenuItem value="3">3</MenuItem>
                        <MenuItem value="4">4</MenuItem>
                        <MenuItem value="5">5</MenuItem>
                      </Select>
                    </FormControl>
                  }
                  {content.name ==="batch" &&
                    <FormControl style={{width:'100%'}}>
                       <InputLabel id="batch">Batch</InputLabel>
                       <Select
                         labelId="batch"
                         id="batch"
                         name={content.name}
                         value={this.state[content.name]}
                         onChange={e => this.handleD(e, index)}
                       >
                         <MenuItem value="1">B-1</MenuItem>
                         <MenuItem value="2">B-2</MenuItem>
                       </Select>
                     </FormControl>
                   }
             </React.Fragment>
              :

                <div className="input-field">
                  <input
                    id={content.name}
                    className=""
                    type={content.type}
                    placeholder=" "
                    min="10"
                    max="60"
                    name={content.name}
                    value={this.state[content.name]}
                    onChange={e => this.handleD(e, index)}
                  />
                  <label htmlFor={content.name}>{content.placeholder}</label>
                </div>
              }
            </div>
          ))}
        </div>
        <div>
        <div className="row">
          <button className="btn btn-small red" onClick={this.props.close}>Cancel</button>
          <button className="btn right blue-grey darken-2 sup" onClick={this.handleSubmit} type="submit">UPLOAD</button>
        </div>
        </div>
      </React.Fragment>
    )
  }
}


class ShowAndEdit extends Component{
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
      covered:[],
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch(this.props.action)
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
  }

  deleteProduct = (productId,index) => {
      this.deleteOperation(productId,index);
}
deleteOperation= (productId,index) =>{
  var delroute;
    delroute = '/ework/user/del_handover_slots';

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  var today= Date.parse("today").toString("dd");
  let fetchroute;

    fetchroute = "/ework/user/fetch_handover_slots";

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
    expire_date:today,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
showTopics =(id,slot)=>{
  axios.post('/ework/user/fetchCovered',{faculty_id:id,slot:slot})
  .then( res => {
    // console.log(res.data)
      this.setState({covered:res.data})
  });
}
  render() {

    const {products} = this.state;
      return(
        <div>
            <div className="row">
                <div className="col s2 l1 m2 xl1 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className={content.div} key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s2 l1 m1 xl1 center"><b>Status</b></div>
                <div className="col s2 l1 xl1 m2 center"><b>Action</b></div>
            </div>
            <hr />
            <div>
              {products.map((product,index) => (
                <div className="row" key={product.serial}>
                  <div className="col l1 xl1 m1 s1 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className={content.div} key={index}>{product[content.name]}</div>
                  ))}
                  <div className="col l1 xl1 m1 s1 center">
                  {product.status === true ?
                    <div className="center green-text">Completed</div> : <div className="center red-text">Pending</div>
                  }</div>
                  {product.status === false ?
                    <React.Fragment>
                      <div className="col s2 m2 l1 xl1 center">
                        <i className="material-icons go" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                      </div>
                    </React.Fragment>
                  :
                  <div>
                  <Button href="#modal1" onClick={() => this.showTopics(product.faculty_id,product.slot)} className="modal-trigger right btn-small" title="Click when completed">Coverd Topic</Button>
                  <Modal id="modal1">
                               <div className="col l12">
                                 {this.state.covered.map((content,index) => (
                                   <React.Fragment>
                                   <div className="row">
                                      <div className="col l6">Slot Handled</div>
                                      <div className="col l6">{content.slot}</div>
                                   </div>
                                   <div className="row">
                                      <div className="col l6">Topics Covered</div>
                                      <div className="col l6">{content.handledtopic}</div>
                                   </div>
                                   <div className="row">
                                      <div className="col l6">Handled By</div>
                                      <div className="col l6">{content.handled_by}</div>
                                   </div>
                                   </React.Fragment>
                                 )
                               )}
                               </div>
                 </Modal>
                  </div>
                }
                </div>
              ))}
            </div>
        </div>
      )

  }
}
