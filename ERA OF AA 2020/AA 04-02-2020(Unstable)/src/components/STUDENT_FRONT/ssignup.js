import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import RegistrationStopped from '../FACULTY_FRONT/stop'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {Button,Modal,Icon} from 'react-materialize'
import ContentLoader from "react-content-loader"
import M from 'materialize-css';

var empty = require('is-empty');



class SSignup extends Component {
	constructor() {
    super()
    this.initialState = {
		loader:true,
		error:'',
		status:"none",
		redirectTo: null,
		name: '',
		mailid: '',
		regid: '',
		phone: '',
		password: '',
		cnf_pswd: '',
		campus: '',
		dept: '',
		degree: '',
		startyear: '',
		year: '',
		dob:'',
		count: 4,
		color:'green-text',
    }
		this.state = this.initialState;
		this.componentDidMount = this.componentDidMount.bind(this)
		this.handleInput = this.handleInput.bind(this)
	}
	handleInput = (e) =>{
		this.setState({[e.target.name]:e.target.value})
	}
		componentDidMount(){
			M.AutoInit()
			axios.get('/ework/user2/fetch_srender_status')
			.then(res=>{
				this.setState({loader:false})
				if(res.data === true)
				{
				this.setState({status:"block"})
			  }
			})
		}


	handleSubmit=(event)=> {
		var verify = this.state.regid;
		var vermail = this.state.mailid;
		event.preventDefault()
		if(empty(this.state.year)||empty(this.state.degree)||empty(this.state.name)
		||empty(this.state.mailid)||empty(this.state.regid)||empty(this.state.phone)
		||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)
		||empty(this.state.dept))
		{
			window.M.toast({html: 'Enter all the Details',outDuration:'1000', classes:'rounded red'});
      return false;
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
			this.setState({
				error:'Password does not match !!',
				cnf_pswd:'',
			})

	     return false;

	  }
	  else if(!verify.includes('RA'))
	  {
		this.setState({
			error:'Enter Registration Number starting with RA !!',
			regid:'',
		})
		return false;
	  }
	  else if(verify.length!==15)
	  {
		this.setState({
			error:'Registration Number should be of 15 digit !!',
			regid:'',
		})
		return false;
	  }
	  else if((!vermail.includes('srmuniv.edu.in'))&&(!vermail.includes('srmist.edu.in')))
	  {
		this.setState({
			error:'Enter official SRM Mail Id Please!!',
			mailid:'',
		})
		return false;
	  }
		else if ((this.state.phone).length!==10)
		{
			this.setState({
				error:'Enter correct format of Phone no !!',
				phone:'',
			})
			return false;
		}
		else{
		axios.post('/ework/user2/ssignup', {
			username: this.state.regid,
			password: this.state.password,
      name: this.state.name,
      mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
	    dept: this.state.dept,
			batch: this.state.batch,
			degree: this.state.degree,
			sem: this.state.sem,
			year: this.state.year,
			count: this.state.count,
			suspension_status:false,
			resetPasswordExpires:'',
			resetPasswordToken:'',
		})
			.then(response => {
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, outDuration:'9000', classes:'rounded #ba68c8 purple lighten-2'});
							this.setState(this.initialState);
				  }
					else if(response.data.succ)
					{
						window.M.toast({html: response.data.succ, outDuration:'2000', classes:'rounded #ba68c8 purple lighten-2'});
						this.setState({
								redirectTo: '/ework/slogin'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
			})
		this.setState({loader:false})
	}
}


render() {
	const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
        </ContentLoader>
      )
			if (this.state.redirectTo) {
					 return <Redirect to={{ pathname: this.state.redirectTo }} />
			 } else {
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status ==="none")
	{
		return(
			<RegistrationStopped login_path="/ework/slogin" section_name="STUDENT REGISTRATION"/>
		);
	}
	else{
	return (
		<div className="row">

		<div className="col s2 l2 m2 xl2" />

		<div className="col l8 s12 m12 form-signup">
				<div className="center">
						<h5 className="reg">Student Registration</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">

								<div className="input-field col s6 l6 xl6 m6">
								<input id="name" type="text" className="validate" name="name" value={this.state.name} onChange={this.handleInput} required />
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s6 xl6 l6 m6">
								<input id="stud_id" type="text" className="validate" name="regid" value={this.state.regid} onChange={this.handleInput} required />
								<label htmlFor="stud_id">Registration Number</label>
								</div>
						</div>



						<div className="row">
								<div className="input-field col s5 l5 xl5 m5">
								<input id="email" type="email" className="validate" name="mailid" value={this.state.mailid} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Only official mail id is allowed</span>
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="ph_num" type="number" className="validate" name="phone" value={this.state.phone} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Enter Correct Format of Phone no</span>
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="col s3 l3 xl3 m3">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_degree">Degree</InputLabel>
												<Select
													labelId="sel_degree"
													id="sel_degree"
													name="degree"
													value={this.state.degree}
													onChange={this.handleInput}
												 >
														<MenuItem value="" disabled defaultValue>Year</MenuItem>
														<MenuItem value="B.tech">B.Tech</MenuItem>
														<MenuItem value="M.tech">M.Tech</MenuItem>
														<MenuItem value="BCA">BCA</MenuItem>
														<MenuItem value="MCA">MCA</MenuItem>
												</Select>
										 </FormControl>
								</div>

						</div>

						<div className="row">

								<div className="input-field col s6 l6 xl6 m6">
								<input onChange={this.handleInput} name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								<div>
								<Button
								 href="#modal1" className="modal-trigger N/A transparent" floating small waves="light" icon={<Icon className={this.state.color} tiny> help_outline </Icon>} />
								<Modal id="modal1">
									 <h5 className="center">Required Field in Password</h5>
									 <p className="center">
									 At least 1 uppercase character.<br />
									 At least 1 lowercase character.<br />
									 At least 1 digit.<br />
									 At least 1 special character.<br />
									 Minimum 6 characters.<br />
									 </p>
								</Modal>
								</div>
								</div>

								<div className="input-field col s6 l6 xl6 m6">
								<input onChange={this.handleInput} id="cnf_pswd" value={this.state.cnf_pswd} name="cnf_pswd" type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>

						</div>

						<div className="row">
								<div className="col s4 l4 xl4 m4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_year">Year</InputLabel>
												<Select
													labelId="sel_year"
													id="sel_year"
													name="year"
													value={this.state.year}
													onChange={this.handleInput}
												>
													<MenuItem value="" disabled defaultValue>Year</MenuItem>
													<MenuItem value="1">First Year</MenuItem>
													<MenuItem value="2">Second Year</MenuItem>
													<MenuItem value="3">Third Year</MenuItem>
													<MenuItem value="4">Fourth Year</MenuItem>
													<MenuItem value="5">Fifth Year</MenuItem>
												</Select>
										 </FormControl>
								</div>

								<div className="col s4 l4 xl4 m4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_sem">Semester</InputLabel>
												<Select
													labelId="sel_sem"
													id="sel_sem"
													name="sem"
													value={this.state.sem}
													onChange={this.handleInput}
												>
													<MenuItem value="" disabled defaultValue>Year</MenuItem>
													<MenuItem value="1">1</MenuItem>
													<MenuItem value="2">2</MenuItem>
													<MenuItem value="3">3</MenuItem>
													<MenuItem value="4">4</MenuItem>
													<MenuItem value="5">5</MenuItem>
													<MenuItem value="6">6</MenuItem>
													<MenuItem value="7">7</MenuItem>
													<MenuItem value="8">8</MenuItem>
													<MenuItem value="9">9</MenuItem>
													<MenuItem value="10">10</MenuItem>
												</Select>
										 </FormControl>
								</div>

								<div className="col s4 l4 xl4 m4">
									<FormControl style={{width:'100%'}}>
										<InputLabel id="sel_batch">Batch</InputLabel>
											<Select
												labelId="sel_batch"
												id="sel_batch"
												name="batch"
												value={this.state.batch}
												onChange={this.handleInput}
											>
											<MenuItem value="1">B-1</MenuItem>
											<MenuItem value="2">B-2</MenuItem>
											</Select>
									 </FormControl>
								</div>

						</div>
						<div className="row">

								<div className="col xl6 l6 m6 s6">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_campus">Campus</InputLabel>
												<Select
													labelId="sel_campus"
													id="sel_campus"
													name="campus"
													value={this.state.campus}
													onChange={this.handleInput}
												>
												<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
												<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
												<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
												<MenuItem value="NCR Campus">NCR Campus</MenuItem>
												</Select>
										 </FormControl>
								</div>
								<div className="col xl6 l6 s6 m4">
								<FormControl style={{width:'100%'}}>
									<InputLabel id="sel_dept">Department</InputLabel>
										<Select
											labelId="sel_dept"
											id="sel_dept"
											name="dept"
											value={this.state.dept}
											onChange={this.handleInput}
										>
										<MenuItem value="Computer Science">Computer Science</MenuItem>
										<MenuItem value="Information Technology">Information Technology</MenuItem>
										<MenuItem value="Software Engineering">Software</MenuItem>
										<MenuItem value="Mechanical Engineering">Mechanical</MenuItem>
										</Select>
								 </FormControl>
								</div>
						</div>
						<br/>
						<div className="row"><div className="col l4 m4 s4 xl4 left">
							<Link to='/slogin' className="log">Login Instead ?</Link></div>
							<div className="col l4 s4 xl4 m4 center" style={{color:'red'}}>{this.state.error}</div>
							<div className="col l4 s4 m4 xl4">
							<button className="waves-effect btn blue-grey darken-2 sup" style={{width:'100%'}} onClick={this.handleSubmit}>Submit</button>
							</div>
						</div>
				</form>
		</div>
		</div>

	);
}
	}
}
}
}

export default SSignup;
