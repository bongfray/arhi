import React, { Component } from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

export default class ClassComplete extends Component {
  constructor(){
    super()
    this.state={
      disabled:'',
      disp:false,
      covered_s:'',
      key_ind:'',
    }
    this.componentDidUpdate = this.componentDidUpdate.bind(this)
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps!==this.props)
    {
      this.props.completeHistory();
    }
  }

  handleField=(e,index)=>{
    const value = e.target.value;
    this.setState({covered_s: value, index,key_ind: e.target.name});
  }

  handleAction =(covered_fac,day,month,year,faculty_id,serial,day_slot_time,slot,week,batch,sem,for_year)=>{
    if(!this.state.covered_s)
    {
      window.M.toast({html: 'Enter all the details',classes:'rounded red'});
    }
    else{
    window.M.toast({html: 'Updating...',classes:'rounded orange black-text'});
    axios.post('/ework/user2/updateValue',{
      covered_fac:covered_fac,
      username:this.props.username,
      submitted_data:this.state.covered_s,
      week:week,day_slot_time:day_slot_time,slot:slot,day:day,month:month,year:year,
      batch:batch,for_year:for_year,sem:sem,faculty_id:faculty_id,serial:serial})
    .then( res => {
      if(res.data.updateStatus === 'done')
      {
        this.sendStatus(res.data.positive_count,day,month,year,faculty_id,day_slot_time,
        slot,week,res.data.total_student.length,serial,batch,for_year,sem);
      }
    });
   }
  }

  sendStatus=(positive_count,day,month,year,faculty_id,day_slot_time,slot,week,total_student,serial,
    batch,for_year,sem)=>{
    axios.post('/ework/user/sendStatus',{total_student:total_student,week:week,
      day_slot_time:day_slot_time,slot:slot,day:day,month:month,year:year,batch:batch,for_year:for_year,
      faculty_id:faculty_id,positive_count:positive_count,sem:sem})
    .then( res => {
      if(res.data)
      {
        this.props.completeHistory();
        this.props.complete_class.filter(item => item.serial!== serial);
        window.M.toast({html: 'Submitted !!',classes:'rounded green darken-2'});
        if(this.props.complete_class.length===0)
        {
                  this.props.showPage();
        }
      }
    });
  }

  showInfo=()=>{
    this.setState({
      disp:!this.state.disp,
    })
  }

  render() {

    const textFieldStyle = makeStyles(theme => ({
      root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
          width: 200,
        },
      },
    }));

    this.props.complete_class.filter(item =>{
      return this.props.facultylist.find(citem =>
         (citem.faculty_id === item.faculty_id) && (item.new=== true) &&
        (item.day_slot_time === citem.timing) && (item.for_batch === citem.batch)
      && (item.for_sem === citem.sem) && (item.for_year === citem.year));
    });
    return (
      <div className="notinoti">
         <h5 className="center">Class Completion History</h5>
         {this.state.disp === false && <div title="Instructions" onClick={this.showInfo} className="go" style={{marginTop:'10px',boxShadow: '0px 10px 10px 2px pink',width:'30px',borderRadius: '0px 20px 20px 0px'}}>
           <i className="material-icons small red-text">chevron_right</i>
         </div>
         }
         {this.state.disp &&
          <div className="cover_all" onClick={this.showInfo}>
            <div className="up">
                <div style={{padding:'10px'}}>
                <h5 className="center red-text">Instructions</h5>
                 <div className="row" style={{padding:'10px'}}>
                      Please submit what are the topics completed today in the listed slot,taken by
                      respective faculty.<br /><br />
                      <span className="red-text"><b>Remember: </b></span>Your truthfullness and honesty will
                      be helpful to increse your popularity in campus.
                  </div>
                <hr />
                  <div style={{padding:'10px'}} onClick={this.showInfo} className="go right">CLOSE</div>
                </div>
            </div>
         </div>}
         <br />
         <div className="row">
            <div className="col l2 xl2 center"><b>Faculty Id</b></div>
            <div className="col l2 xl2 center"><b>DayOrder.Hour</b></div>
            <div className="col l2 xl2 center"><b>Slot</b></div>
            <div className="col l4 xl4 center"><b>Enter Covered Topic</b></div>
            <div className="col l2 xl2 center"><b>Action</b></div>
         </div>
         <hr />
         {this.props.complete_class.length===0 ?
           <h5 className="center">No Data Found</h5>
           :
         <div>
           {this.props.complete_class.map((content,index)=>(
             <React.Fragment key={index}>
              <div className="row">
                <div className="col l2 xl2 center">
                   {content.faculty_id}
                </div>
                <div className="col l2 xl2 center">
                   {content.day_slot_time}
                </div>
                <div className="col l2 xl2 center">
                   {content.slot}
                </div>
                <div className="col l4 xl4 center">
                    <div className={textFieldStyle.root}>
                        <TextField
                        id="outlined-multiline-static"
                        label="Type Here"
                        multiline
                        fullWidth
                        value={this.state.ind && this.state.covered_s}
                        onChange={e => this.handleField(e, index)}

                        rows="4"
                        variant="filled"
                        />
                    </div>
                </div>
                <div className="col l2 xl2 center">
                  <button className="btn btn-small" onClick={()=>this.handleAction(
                    content.covered_topic,
                    content.day,content.month,content.year,content.faculty_id,content.serial,
                    content.day_slot_time,content.slot,content.week,content.for_batch,content.for_sem,
                    content.for_year
                  )}>Submit</button>
                </div>
              </div>
              <hr />
             </React.Fragment>
           ))}
           </div>
         }

      </div>
    );
  }
}
