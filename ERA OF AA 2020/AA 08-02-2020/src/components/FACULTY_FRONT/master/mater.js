import React, { Component} from 'react';
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import M from 'materialize-css';
import Status from './status'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"


export default class Master extends Component {
	constructor() {
    super();
    this.state = {
			loading: true,
			adwork:10,
			allot_adwork: null,
			academic_work:12,
			allot_academic_work: null,
			research_work:14,
			allot_research_work: null,
			datas: [
				{
					day_order:'Day Order 1',dayor:'1',
					day:[
						{id:"1.1"},{id:"1.2"},{id:"1.3"},{id:"1.4"},{id:"1.5"},{id:"1.6"},{id:"1.7"},{id:"1.8"},{id:"1.9"},{id:"1.10"}
					]

				},
				{
					day_order:'Day Order 2',dayor:'2',
					day:[
						{id:"2.1"},{id:"2.2"},{id:"2.3"},{id:"2.4"},{id:"2.5"},{id:"2.6"},{id:"2.7"},{id:"2.8"},{id:"2.9"},{id:"2.10"}
					]
				},
				{
					day_order:'Day Order 3',dayor:'3',
					day:[
						{id:"3.1"},{id:"3.2"},{id:"3.3"},{id:"3.4"},{id:"3.5"},{id:"3.6"},{id:"3.7"},{id:"3.8"},{id:"3.9"},{id:"3.10"}
					]
				},
				{
					day_order:'Day Order 4',dayor:'4',
					day:[
						{id:"4.1"},{id:"4.2"},{id:"4.3"},{id:"4.4"},{id:"4.5"},{id:"4.6"},{id:"4.7"},{id:"4.8"},{id:"4.9"},{id:"4.10"}
					]
				},
				{
					day_order:'Day Order 5',dayor:'5',
					day:[
						{id:"5.1"},{id:"5.2"},{id:"5.3"},{id:"5.4"},{id:"5.5"},{id:"5.6"},{id:"5.7"},{id:"5.8"},{id:"5.9"},{id:"5.10"}
					]
				}
			],
			week:'',
			home:'/ework/faculty',
			logout:'/ework/user/logout',
			get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

		getPercentage =() =>{
			axios.get('/ework/user/getPercentage', {
			})
			.then(response =>{
				if(response.data){
					this.setState({
						allot_adwork: response.data.adpercentage,
						allot_academic_work: response.data.academicpercentage,
						allot_research_work: response.data.researchpercentage,
					});
				}
			})
		}

    fetchlogin = () =>{
      axios.get('/ework/user/'
	)
      .then(response =>{
				this.setState({loading: false})
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
				else{
					this.getPercentage();
				}
      })
    }

    componentDidMount() {
        M.AutoInit();
        this.fetchlogin();
    }


render() {
	const MyLoader = () => (
		<ContentLoader
		  height={160}
		  width={400}
		  speed={2}
		  primaryColor="#f3f3f3"
		  secondaryColor="#c0c0c0"
		>
		  <rect x="24" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="35" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="55" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="75" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="95" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="115" rx="0" ry="0" width="38" height="7" />

		</ContentLoader>
	  )
	if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
	return (
		<React.Fragment>
		<Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>


        <div className="row">
        <table className="center">
        <thead style={{backgroundColor:'white',color:'black'}}>
        <tr>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>Hour /</span><br />Day Order</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>1</span><br />08:00 - 08:50</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>2</span><br />08:50 - 09:40</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>3</span><br />09:45 - 10:35</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>4</span><br />10:40 - 11:30</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>5</span><br />11:35 - 12:25</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>6</span><br />12:30 - 01:20</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>7</span><br />01:25 - 02:15</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>8</span><br />02:20 - 03:10</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>9</span><br />03:15 - 04:05</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>10</span><br />04:05 - 04:55</th>
        </tr>
        </thead>

        <tbody>
				{this.state.datas.map((content,index)=>{
					return(
					<tr style={{backgroundColor: 'white',bordeBottom:'1px solid'}} className="" key={index}>
					  <Stat content={content} dayor={content.dayor} />

					</tr>
				);

				})}
        </tbody>
      </table>


	</div>



</React.Fragment>
);
}
}
}
}


class Stat extends Component{
	constructor()
	{
		super()
		this.state={
			week:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}
know =()=>
{
	Date.prototype.getWeek = function () {
	    var onejan = new Date(this.getFullYear(), 0, 1);
	    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
	};
}

	componentDidMount()
	{
		this.know();
	}

	render()
	{
		return(
			<React.Fragment>
			<td style={{borderRight: '1px solid',bordeBottom:'1px solid'}} className="center"><b>{this.props.content.day_order}{this.state.week}</b></td>
			{this.props.content.day.map((content,ind)=>{
											return(
												<td style={{borderRight: '1px solid '}} key={content.id}>
														<Status slott={content.id} week={this.state.week}/>
												</td>
											);

											})}
			</React.Fragment>
		);
	}
}
