import React, { Component } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


import AddProduct from './add_datas';
import ProductList from './show_info'

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:false,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     console.log(data)
     let apiUrl;

     if(!(data.emailid) || !(data.name) || !(data.gender) || !(data.mobileno) || !(data.address))
     {
       window.M.toast({html: 'Enter All the Details !!',classes:'rounded  red'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/ework/user2/editt';
     } else {
       apiUrl = '/ework/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  oneEntry = (object)=>{
    this.setState(object);
  }

   render() {
     let productForm,title,description;
     var data;
             title ='Contact Details';
             data = {
               Action:'Personal_Details',
               button_grid:1,
               fielddata: [
                 {
                   header: "Name",
                   name: "name",
                   placeholder: "Enter your name",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Email ID",
                   name: "emailid",
                   placeholder: "Enter your mailId",
                   type: "email",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Gender",
                   name: "gender",
                   placeholder: "Enter your gender",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "Mobile No.",
                   name: "mobileno",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Mobile No."
                 },
                 {
                   header: "Address",
                   name: "address",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Permanent Address"
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <React.Fragment>
    {!this.state.isAddProduct &&
      <ProductList oneEntry={this.oneEntry} username={this.props.username} title={title} description={description}
      action={this.props.options} data={data}  editProduct={this.editProduct}/>
    }
    <br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <Grid container spacing={1}>
        <Grid item xs={6} sm={6}/>
        <Grid item xs={5} sm={5}>
         <Button variant="contained" style={{float:'right'}} color="secondary" onClick={(e) => this.onCreate(e,this.props.options)} disabled={this.state.disabled}>
          Add Data
          </Button>
        </Grid>
        <Grid item xs={1} sm={1}/>
    </Grid>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </React.Fragment>
);
}
}
