const express = require('express')
const route = express.Router()
const bcrypt = require('bcryptjs');
const Suser = require('../database/models/FAC/user')
const passport = require('../passport/stud_pass')
const NavS = require('../database/models/STUD/navstud')
const AdminOrder = require('../database/models/FAC/admin')
const Admin_Operation = require('../database/models/FAC/admin_operation')
const Resume = require('../database/models/STUD/resume_schema')
const Task = require('../database/models/STUD/task')
const Arhi = require('../database/models/STUD/path')
const Faculty = require('../database/models/STUD/faculty_list')
const Notification = require('../database/models/FAC/notification')
const StudentData = require('../database/models/STUD/studentData')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')
var stringSimilarity = require('string-similarity');
console.log("Student Server")





route.get('/fetch_srender_status', function(req, res) {
  AdminOrder.findOne({usertype:'stud'}, function(err, result){
    if(err)
    {

    }
    else if(result)
    {
      res.send(result.registration_status)
    }
  })
})




route.post('/ssignup', (req, res) => {
//console.log(req.body);
    Suser.findOne({ username: req.body.username }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new Suser(req.body)
            newUser.save((err, savedUser) => {
              if(err)
              {

              }
              else if(savedUser)
              {
                 console.log('here')
                var succ= {
                  succ: "Successful SignedUP"
                };
                res.send(succ);
              }
            })
        }
    })
})


route.post(
    '/slogin',(req, res, next) => {
      //console.log(req.ip)
      passport.authenticate('local', function(err, user, info) {
        if (err) {
           return next(err);
         }
        if (!user) {
            res.status(401);
            res.send(info.message);
            return;
        }
        req.logIn(user, function(err) {
          if (err) {
            return next(err);
          }
          var userInfo = {
              username: req.user.username,
              count: req.user.count,
          };
          res.send(userInfo);
        });
      })(req, res, next);
    }
)


route.get('/getstudent', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

route.post('/slogout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

route.post('/sprofile', (req, res) => {

    const {up_username,up_phone,up_name,up_dob} = req.body;
    Suser.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)

route.get('/getstudentprofile', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    Suser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});

    route.post('/sforgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       Suser.findOne(
         {mailid: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'Invalid User'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           console.log(token);
           Suser.updateOne({mailid:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 900000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com',
            to: mailid,
            subject: 'Reset Password:  eWorks',
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 15min</p>'
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })

        })



        route.post('/addnav', function(req,res) {
                  var trans = new NavS(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });


/*-------------------------------------------------------------Fetch Request From Nav Bar---------------------- */
        route.get('/fetch_snav',function(req,res) {
          NavS.find({ }, function(err, docs){
          if(err)
          {
            console.log(err)
          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/send_task', function(req,res) {
          //console.log(req.body)
        Task.findOne({referTask:req.body.referTask},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {
            Task.updateOne({referTask:req.body.referTask},
              {$set:{description:req.body.description,link:req.body.link,start_date:req.body.start_date,end_date:req.body.end_date}},(err,done)=>{
              if(err)
              {

              }
              else if(done)
              {
                res.send('updated');
              }
            })
          }
          else{
              var respon = new Task({
                username:req.user.username,
                task_name:'Task '+req.body.task_name,
                level:req.body.level,
                description:req.body.description,
                link:req.body.link,
                start_date:req.body.start_date,
                end_date:req.body.end_date,
                referTask:req.body.referTask,
              });
              respon.save((err, savedUser) => {
                if(err)
                {

                }
                else if(savedUser)
                {
                  validateSkill(req,res);
                }
              })
          }

        })
      });



   function validateSkill(req,res){
     Admin_Operation.find({$and:[{action:'Task '+req.body.task_name}]},(err,docs)=>{
       if(err){

       }
       else if(docs){
         Resume.findOne({$and:[{action:'Skills'},{username:req.user.username},
         {skill_name:req.body.task_name}]},(err,have)=>{
           if(err){

           }
           else if(have){
             Task.find({$and:[{task_name:'Task '+req.body.task_name},{username:req.user.username}]},(err,done)=>{
               if(err){

               }
               else if(done)
               {
                 if((done.length)>=((((docs.length)*75)/100)))
                 {
                   Resume.updateOne({$and:[{action:'Skills'},{username:req.user.username},
                   {skill_name:req.body.task_name}]},{skill_verification:true},(err,updated)=>{
                     if(err){

                     }
                     else if(updated)
                     {
                       //res.send('done')
                     }
                     else{
                       //res.send('done')
                     }
                   })
                 }
                 else
                 {
                   Resume.updateOne({$and:[{action:'Skills'},{username:req.user.username},
                   {skill_name:req.body.task_name}]},{skill_verification:false},(err,updated)=>{
                     if(err){

                     }
                     else if(updated)
                     {
                       //res.send('done')
                     }
                     else {
                       //res.send('done')
                     }
                   })
                 }
               }
               else
               {
                   //res.send('done');
               }
             })

           }
           else
           {
               //res.send('done');
           }
         })
         rateSkill(req,res,req.body.task_name,docs);
       }
       else{
           res.send('done');
       }
     })
   }

      function rateSkill(req,res,course,admin_saved,){
        Task.findOne({$and:[{task_name:'Task '+course},{username:req.user.username}]},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {

              Task.find({$and:[{task_name:'Task '+course},{username:req.user.username}]},(er,objs)=>{
                if(er){

                }
                else if(objs){
                      let grade;
                      let count = ((objs.length) / (admin_saved.length))*100;
                      console.log(count)
                      if(count>0 && count<15)
                      {
                        grade = '1';
                      }
                      else if(count>=15 && count<30)
                      {
                        grade = '2';
                      }
                      else if(count>=30 && count<40)
                      {
                        grade = '3';
                      }
                      if(count>=40 && count<55)
                      {
                        grade = '3.5';
                      }
                      else if(count>=55 && count<75)
                      {
                        grade = '4';
                      }
                      else if(count>=75 && count<90)
                      {
                        grade = '4.5';
                      }
                      else if(count>=90 && count<=100)
                      {
                        grade = '5';
                      }
                      else if(count === 0)
                      {
                        grade = 'F';
                      }
                      console.log(grade)
                      Resume.updateOne({$and:[{username:req.user.username},{action:'Skills'},{skill_name:course}]},{skill_rating:grade},(err,rated)=>{
                        if(err){

                        }
                        else if(rated){
                          res.send('done');
                        }
                      })
                }
              })
          }
          else
          {
            res.send('No Data');
          }
        })
      }


      route.post('/fetchRating', function(req, res) {
        Resume.findOne({$and:[{username:req.user.username},{action:'Skills'},{skill_name:req.body.course}]},(err,found)=>{
          if(err){

          }
          else if(found){
            res.send(found.skill_rating);
          }
          else{
            res.send('No Data')
          }
        })
      })

      route.post('/fetchTasks',function(req,res) {
        console.log(req.body)
        Task.find({$and:[{level: req.body.level},{task_name:req.body.action},{username:req.user.username}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send(docs)
        }
        });
      })

      route.post('/deleteTask',function(req,res) {
        Task.deleteOne({$and:[{referTask: req.body.key},{username:req.user.username}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send('del')
        }
        });
      })



/*-------------------------------------------------------------Fetch Request From Admin Panel---------------------- */
        route.post('/fetch_student_nav',function(req,res) {
          NavS.find({action: req.body.action}, function(err, docs){
          if(err)
          {
            console.log(err)
          }
          else
          {
            res.send(docs)
          }
        });
        })


        route.post('/editnav', function(req,res) {
          NavS.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
            var succ= {
              succ: "Updated"
            };
            res.send(succ);
          })

        });

        route.post('/edit_existing_nav',function(req,res) {
          NavS.findOne({serial: req.body.id}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/delnav',function(req,res) {
          NavS.remove({serial:  req.body.serial },function(err,succ){
            res.send("OK")
          });
        })




        route.post('/handleStudent_Registration',function(req,res) {

        AdminOrder.findOne({usertype: req.body.history}, function(err, docs){
          if(err)
          {

          }
          else if(docs)
          {
            AdminOrder.updateOne({usertype:req.body.history},{registration_status: req.body.checked},function(err,done){
              res.send("Done")
            })
          }
          else{
            var trans = new AdminOrder({
              usertype:req.body.history,
              registration_status: req.body.checked
            }
            );
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
          }
        })
        })



        route.get('/stud_reg_status', (req, res, next) => {
          AdminOrder.findOne({usertype:'stud'},function(err,result){
            if(err)
            {

            }
            else if(result)
            {
              res.send(result.registration_status)
            }
          })
        })



        /*----------------------------------------RESUME CODE0--------------------------------------------------------- */
        route.post('/addmm', function(req,res) {
          console.log(req.body.data)
                  var trans = new Resume(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });

          route.post('/editt', function(req,res) {
            Resume.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })

          });


      route.post('/fetchall',function(req,res) {
        console.log(req.body)
          Resume.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
            if(err)
            {

            }
            else
            {
              res.send(docs)
            }
        });
      })

      route.post('/fetcheditdata',function(req,res) {
        Resume.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
       });
      })

      route.post('/del',function(req,res) {
        Resume.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
          res.send("OK")
        });
      });



      route.post('/deletesuser',function(req,res){
        Suser.findOne({username:req.body.username},function(err,found){
          if(err)
          {

          }
          else if(found)
          {
            Suser.deleteOne({username:found.username},function(err,done)
            {
            })
            Resume.deleteMany({username:found.username},function(err,done)
            {
            })
            res.send("Deleted")
          }
        })
      })
      route.post('/update_student_password',function(req,res){
        Suser.findOne({username:req.body.student_username},function(err,found){
          if(err)
          {

          }
          else if(found)
          {
            let hash = bcrypt.hashSync(req.body.student_password, 10);
            Suser.updateOne({username:req.body.student_username},{password: hash} ,(err, done) => {
              if(err)
              {

              }
              else if(done)
              {
                res.send("Updated");
              }
            })
          }
          else
          {
            res.send("User Not Found !!")
          }
        })
      })


      route.post('/suspension_suser',function(req,res){
        console.log(req.body)
        Suser.findOne({username:req.body.suspend_student_username},function(err,found){
          if(err)
          {
            res.status(404).send("error!!")
          }
          else if(found)
          {
            AdminOrder.findOne({$and:[{victim_username:req.body.suspend_student_username},{order:'Suspend Suser'}]},function(err,obj){
              if(err)
              {

              }
              else if(obj)
              {
                res.send("Already Suspended !!")
              }
              else{
                var suspend = new AdminOrder({order:'Suspend Suser',victim_username:req.body.suspend_student_username,reason:req.body.suspend_reason_student,suspend_status:true});
                suspend.save((err, savedUser) => {
                  if(err)
                  {

                  }
                  else if(savedUser)
                  {
                    Suser.updateOne({username:req.body.suspend_student_username},{suspension_status: true},function(err,done){
                      if(err)
                      {

                      }
                      else if(done)
                      {
                        res.send("Suspended !!")
                      }
                    })
                  }
                })
              }
            })
          }
          else{
            res.send("User Not Found !!")
          }
        })
      })
      route.post('/remove_suser_suspension',function(req,res){
        console.log(req.body)
        Suser.findOne({username:req.body.suspend_student_username},function(err,found){
          if(err)
          {
            res.status(404).send("error!!")
          }
          else if(found)
          {
            AdminOrder.findOne({$and:[{victim_username:req.body.suspend_student_username},{order:'Suspend Suser'}]},function(err,obj){
              if(err)
              {

              }
              else if(obj)
              {
                Suser.updateOne({username:req.body.suspend_student_username},{suspension_status: false},function(err,done){
                  if(err)
                  {

                  }
                  else if(done)
                  {
                    obj.remove()
                    res.send("Removed Suspension !!")
                  }
                })
              }
              else{
                res.send("User Not Found !!")
              }
            })
          }
          else{
            res.send("User Not Found !!")
          }
        })
      })









      route.get('/know_no_of_suser', function(req, res) {
        Suser.find({ }, function(err, result){
          if(err)
          {

          }
          else if(result)
          {
            res.send(result)
          }
        })
      })

      route.post('/fetch_in_admin',function(req,res) {
        Admin_Operation.find({$and:[{action: req.body.action},{usertype:req.body.usertype}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send(docs)
        }
        });
      })

      route.post('/del_inserted_data',function(req,res) {
      Admin_Operation.remove({serial:  req.body.serial },function(err,succ){
      res.send("OK")
      });
      })

      route.post('/add_from_insert_in_admin', function(req,res) {
        console.log(req.body.data)
            var respon = new Admin_Operation(req.body.data);
            respon.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
    });

    route.post('/edit_inserted_data', function(req,res) {
    Admin_Operation.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
    })

    });

    route.post('/fetch_for_edit',function(req,res) {
    Admin_Operation.findOne({serial: req.body.id}, function(err, docs){
    if(err)
    {

    }
    else
    {
    res.send(docs)
    }
    });
    })

    route.post('/entry_domain', function(req, res) {
      Arhi.findOne({$and:[{domain_name:req.body.domain},{username:req.user.username},{action:'Fav-Domain'}]},(err,found)=>{
        if(err){

        }
        else if(found){
          res.send('have')
        }
        else{
          var trans = new Arhi({
            action:'Fav-Domain',
            username:req.user.username,
            domain_name:req.body.domain,
          });
          trans.save((err, saved) => {
            if(err){

            }
            else if(saved){
                res.send('done');
            }
          })
        }
      })

    })

    route.get('/fetch_domains', function(req, res) {
      Arhi.find({$and:[{username:req.user.username},{action:'Fav-Domain'}]},(err,found)=>{
        if(err)
        {

        }
        else if(found){
          res.send(found);
        }
      })
    })

  route.post('/fetchCompletationHistory', function(req, res) {
      Task.findOne({$and:[{task_name:'Task '+req.body.course},{username:req.user.username}]},(err,found)=>{
        if(err){

        }
        else if(found){

            Task.find({$and:[{task_name:'Task '+req.body.course},{username:req.user.username}]},(er,objs)=>{
              if(er){

              }
              else if(objs){
                res.send(objs);
              }
            })
        }
        else{
          res.send('No Data');
        }
      })
    })


/*Faculty List-----------------------------------------------------------------*/

    route.post('/add_faculty', function(req,res) {
      console.log(req.body)
       Faculty.findOne({$and:[{timing:req.body.timing},{username:req.user.username},{faculty_id:req.body.faculty_id},{current:true}]},(err,found)=>{
         if(err){

         }
         else if(found){
             var trans = new Faculty(req.body);
             trans.save((err, savedUser) => {
                 if(err){

                 }
                 else if(savedUser){
                                res.send('done');
                 }
             })

         }
         else
         {
           var user = new Faculty(req.body);
          user.save((err, savedUser) => {
               if(err){

               }
               else if(savedUser){
                  res.send('done');
               }
           })
         }
       })
    });

      route.post('/edit_faculty', function(req,res) {
      //  console.log(req.body)
        Faculty.updateOne({$and: [{ serial: req.body.serial},{username: req.user.username}]},req.body, (err, user) => {
          if(err){

          }
          else if(user){
            var succ= {
              succ: "Updated !!"
            };
            res.send(succ);
          }
        })

      });


  route.post('/fetchall_faculty',function(req,res) {
console.log(req.body)
      Faculty.find({$and: [{timing:req.body.action},{username: req.user.username},{current:true}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
          res.send(docs)
        }
    });
  })

  route.post('/fetcheditdata_facultylist',function(req,res) {
    console.log(req.body)
    Faculty.findOne({$and: [{serial: req.body.id},{username: req.user.username},{current:true}]}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
   });
  })

  route.post('/del_faculty_list',function(req,res) {
    Faculty.deleteOne({$and: [{serial:  req.body.serial },{username: req.user.username},{current:true}]},function(err,succ){
      if(err){

      }
      else if(succ){
              res.send("OK")
      }
    });
  });


  route.post('/send_noti_on_class_complition',function(req,res) {
    var user = new Notification(req.body);
     user.save((err, savedUser) => {
          if(err){

          }
          else if(savedUser){
             res.send('done');
          }
      })
  });


  route.get('/fetchClass_Complete_History',function(req,res) {
    Notification.find({$and:[{action:'Class Completed'},{new:true}]},(err,found)=>{
      if(err){

      }
      else if(found){
        Faculty.find({$and:[{username:req.user.username},{current:true}]},(er,objs)=>{
          if(er){

          }
          else if(objs)
          {
            var datas={
              completed:found,
              facultylist:objs,
            }
            res.send(datas)
          }
        })
      }
      else{
        res.send('not found')
      }
    })
  });


  route.post('/updateValue',function(req,res) {
  Notification.updateOne({$and:[{new:true},{faculty_id:req.body.faculty_id},{serial:req.body.serial},
    {action:'Class Completed'},{for_year:req.body.for_year},{for_batch:req.body.batch},
    {for_sem:req.body.sem},
    {day:req.body.day},{week:req.body.week},{month:req.body.month},{year:req.body.year}]},{new:false},
    (er,done)=>{
      if(er){

      }
      else if(done)
      {
        StringMatching(req.body.covered_fac.toLowerCase(),req.body.submitted_data.toLowerCase(),req,res);
      }
      else
      {
        res.send('done')
      }
    })
  });

  function StringMatching(covered_fac,covered_s,req,res)
  {
    let positive_count,s_feedback,percentage;
    if((covered_s.includes('mass bunk')) ||covered_s.includes('massbunk'))
      {
        positive_count=true;
        s_feedback=false;
        countPass(req,res,positive_count,s_feedback);
      }
    else if((covered_s.includes('not completed'))||(covered_s.includes('notcompleted'))
    ||(covered_s.includes('not handled'))||(covered_s.includes('nothandled'))
    ||(covered_s.includes('not handle'))||(covered_s.includes('nothandle'))
    ||(covered_s.includes('not taken'))||(covered_s.includes('nottaken'))
    ||(covered_s.includes('was cancelled'))||(covered_s.includes('wascancelled'))
    ||(covered_s.includes('has been cancelled'))||(covered_s.includes('hasbeencancelled'))
    ||(covered_s.includes('has cancelled'))||(covered_s.includes('hascancelled'))
    ||(covered_s.includes('had cancelled'))||(covered_s.includes('hadcancelled'))
    ||(covered_s.includes('not take'))||(covered_s.includes('nottake'))
    ||((covered_s.includes('not present')) && (covered_s.includes('sir')))
    ||((covered_s.includes('notpresent')) && (covered_s.includes('sir'))))
            {
              positive_count=false;
              s_feedback=true;
              countPass(req,res,positive_count,s_feedback);
            }
    else if((covered_s.includes('absent')) || (covered_s.includes('notpresent')) || (covered_s.includes('not present')) ||
    (covered_s.includes('apsent')))
          {
            positive_count=true;
            s_feedback=true;
            countPass(req,res,positive_count,s_feedback);
          }
          else
          {
            percentage = (stringSimilarity.compareTwoStrings(covered_fac,covered_s))*100;
            if(percentage>=40)
            {
              positive_count=true;
              s_feedback=true;
            }
            else {
              positive_count=false;
              s_feedback=true;
            }
            countPass(req,res,positive_count,s_feedback);
          }
  }


  function countPass(req,res,positive_count,s_feedback)
  {
    AddStudentSubmission(req.body,req,res,positive_count,s_feedback);
  }

  function AddStudentSubmission(data,req,res,positive_count,s_feedback)
  {
    var user = new StudentData(data);
     user.save((err, savedUser) => {
          if(err){

          }
          else if(savedUser)
          {
            Faculty.find({$and:[{faculty_id:req.body.faculty_id},{timing:req.body.day_slot_time},
            {batch:req.body.batch},{sem:req.body.sem},{year:req.body.for_year}]},(err,objs)=>{
              if(err){

              }
              else if(objs)
              {
                console.log('Here')
                var datas={
                  updateStatus:'done',
                  total_student:objs,
                  positive_count:positive_count,
                  s_feedback:s_feedback,
                }
                res.send(datas)
              }
            })
          }
      })
  }


  function Priority(action)
  {
    if(action ==='task_completed')
    {

    }
    else if(action ==='skill_rated')
    {

    }
    else if(action ==='responded')
    {

    }
  }





module.exports = route
