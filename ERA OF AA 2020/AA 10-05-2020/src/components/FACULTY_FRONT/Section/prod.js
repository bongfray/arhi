import React from 'react';
import axios from 'axios';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      fetching:true,
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/ework/user/fetchall',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
     fetching:false
   })
  })
}
  render() {
    //console.log(this.state.fetching)
    const { products} = this.state;
    if(this.state.fetching)
    {
      return(
        <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else {
      return(
        <React.Fragment>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                {this.props.data.fielddata.map((content,index)=>(
                  <TableCell align="center" key={index}><b>{content.header}</b></TableCell>
                ))}
                <TableCell align="center">Status</TableCell>
                <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map((row,index)=> (
                  <TableRow key={index}>
                    {this.props.data.fielddata.map((content,ind)=>(
                      <TableCell align="center" key={ind}>
                          {content.link ?
                            <a target="_blank" rel="noopener noreferrer" style={{color:'red'}} href={row[content.name]}>Verification Link</a>
                            :
                            <Typography>{row[content.name]}</Typography>
                          }
                       </TableCell>
                    ))}
                    <TableCell align="center">
                      {row.verified ?
                        <Typography style={{color:'green'}}>Verified</Typography>
                        :
                        <Typography style={{color:'red'}}>Not Verified</Typography>
                      }
                    </TableCell>
                    <TableCell align="center">
                      {row.verified ?
                        <Typography style={{color:'red'}}>NOT EDITABLE</Typography>
                         :
                         <React.Fragment>
                           <EditIcon className="go"
                           onClick={() => this.props.editProduct(row.serial,this.props.action)} />&emsp;
                          <DeleteIcon color="secondary" className="go"
                          onClick={() => this.deleteProduct(row.serial,this.props.action)} />
                         </React.Fragment>
                      }
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </React.Fragment>
      )
    }
  }
}
