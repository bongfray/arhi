import React, {} from 'react'
import { Redirect} from 'react-router-dom'
import axios from 'axios';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import DeciderPath from '../../decider_type';

 export default class ViewSubject extends React.Component{
  constructor()
  { 
    super()
    this.state={
      loading:true,
      option:'',
      username:'',
      redirectTo:'',
      modal: false,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      details_view:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
 componentDidMount()
 {
   this.getUser();
 }
 getUser()
 {
   axios.get('/ework/user/')
    .then(response =>{
       if(response.status === 200)
       {
          if(response.data.user)
          {
            this.setState({username:response.data.user.username})
          }
          else{
            this.setState({
              redirectTo:'/ework/faculty',
            });
          }
          this.setState({
            loading:false,
          })
        }
    })
 }

 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }

closeDecider=(object)=>{
  this.setState(object)
}

 render()
 {
   if (this.state.redirectTo)
   {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
   }
  else
   {
     if(this.state.loading)
     {
       return(
         <Backdrop  open={true} >
           <CircularProgress color="secondary" />&emsp;
           <div style={{color:'yellow'}}>Validating You....</div>
         </Backdrop>
       )
     }
     else{
       return(
         <React.Fragment>
            <DeciderPath display={this.state.details_view} closeDecider={this.closeDecider}
             username={this.state.username} actionType="localuser" admin_action={false} />
         </React.Fragment>
     );
     }
}
 }
}
