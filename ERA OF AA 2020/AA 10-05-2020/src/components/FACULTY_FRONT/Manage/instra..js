
import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const Insta = props => {

     function closeModal(e) {
        e.stopPropagation()
        props.closeModal()
     }
     
     return (
       <Dialog
          open={props.displayModal}
          onClose={closeModal}
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title" align="center">Insturctions</DialogTitle>
          <DialogContent >
            <DialogContentText
              id="scroll-dialog-description"
              tabIndex={-1}
            >
           <Typography>
           1. Kindly read the Instructions first.<br />
           2. This is the page where you can upload you missed dayorder on a particluar slot because of some reason.<br />
           3. You have to make a request to upload your missing data with valid reason.<br />
           4. Remember request with a invalid date and dayorder will simply not let you to submit the data.<br />
           5. Most importantly you are allowed to upload this data within a particular span of time.<br />
           6. Once CARE will accept your request after veryfing all the datas you can upload the data with a span of time,which will be initiated to you.<br />
           7. Incase if you can't able to upload this data within the time, at that case your datas will be blank on that particular day.<br />
           8. By the way this page is only for uploading missed dayorder or slot,not for any updation of your previous dayorder histories.<br />
           9. After your request if it is showing pending for a long time(after 1 week) kindly contact CARE.<br />
           10. Request for a single slot entry, is strictly non-editable,once you submit the data.<br />
           11. Where as a whole dayorder request, will give you a chance to edit the datas after the submission also.<br />
           12. For any type of help contact SRM CARE.<br />
           13. <span className="red-text">We are recommending you to browse this page in desktop or tab mode.</span>
           </Typography>
           </DialogContentText>
           </DialogContent>
           <DialogActions>
           <Button onClick={closeModal} color="primary">
             GOT IT
           </Button>
           </DialogActions>
           </Dialog>
     );
}
export default Insta;
