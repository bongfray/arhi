import React, { } from 'react'
import {Fab,Zoom} from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';



class GoTop extends React.Component {
    state = {
         intervalId: 0,
         thePosition: false
     };

    componentDidMount() {
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                this.setState({ thePosition: true })
            } else {
                this.setState({ thePosition: false })
            }
        });
        window.scrollTo(0, 0);
    }

    onScrollStep = () => {
        if (window.pageYOffset === 0){
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }

    scrollToTop = () => {
        let intervalId = setInterval(this.onScrollStep, this.props.delayInMs);
        this.setState({ intervalId: intervalId });
    }

    renderGoTopIcon = () => {
        if (this.state.thePosition){
            return (
                  <div id="go-to-top">
                   <Zoom in={true}>
                    <Fab color="secondary" size="small" onClick={ () => { this.scrollToTop()}} aria-label="scroll back to top">
                      <KeyboardArrowUpIcon />
                      </Fab>
                    </Zoom>
                  </div>
            )
        }
    }

    render(){
        return (
            <React.Fragment>
                {this.renderGoTopIcon()}
     </React.Fragment>
      );
   }
}

export default class ScrollApp extends React.Component {
  render () {
    return <div>
    <GoTop scrollStepInPx="50" delayInMs="30" />
           </div>
  }
}
