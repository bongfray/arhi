import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import RegistrationStopped from '../FACULTY_FRONT/stop'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import {Paper,TextField,Hidden} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import ContentLoader from "react-content-loader";


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class SSignup extends Component {
	constructor() {
    super()
    this.initialState = {
		loader:true,
		status:"none",
		disable_signup:false,
		redirectTo: null,
		color:'green-text',
		enable:false,
		faculty_adviser:'',
		department:'',
		showPassword:false,
		snack_open:false,
		alert_type:'',
		snack_msg:'',
		show_modal:false,
		batch_ref:0,
    }
		this.state = this.initialState;
		this.componentDidMount = this.componentDidMount.bind(this)
		this.handleInput = this.handleInput.bind(this)
	}
	handleInput = (e) =>{
		this.setState({[e.target.name]:e.target.value})
	}
		componentDidMount(){
			axios.post('/ework/user2/stud_status')
	        .then(response => {
						if(response.data)
						{
							var stud_reg_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="SIGNUP STATUS")));
              //console.log(stud_reg_status);
							if((stud_reg_status.length)>0)
							{
								if(stud_reg_status[0].status === true)
								{
									this.setState({status:'block'})
								}
								else
								{
									this.setState({loader:false})
									this.setState({status:'none'})
								}
							}
							else
							{
								this.setState({status:'block'})
							}
						}
						else
						{
							this.setState({status:'block'})
						}
						this.fetchDesig();
	        })
		}

		fetchDesig =() =>{
			axios.post('/ework/user/fetch_designation')
			.then(res => {
					if(res.data)
					{
							const department = res.data.filter(item =>item.action ==='Department');
						this.setState({department:department,loader:false})
					}
			});
		}


	handleSubmit=(event)=> {
		//console.log(this.state)
		var verify = this.state.regid;
		var vermail = this.state.mailid;
		event.preventDefault()
		if(!(this.state.year)||!(this.state.degree)||!(this.state.name)
		||!(this.state.mailid)||!(this.state.regid)||!(this.state.phone)
		||!(this.state.password)||!(this.state.cnf_pswd)||!(this.state.campus)
		||!(this.state.dept))
		{
			this.setState({
				snack_open:true,
				snack_msg:'Enter all the Details !!',
				alert_type:'warning',
			})
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
			this.setState({
				snack_open:true,
				snack_msg:'Password Does Not Match !!',
				alert_type:'warning',
			})
	  }
	  else if(!verify.includes('RA'))
	  {
			this.setState({
				snack_open:true,
				snack_msg:'Enter Registration Number starting with RA !!',
				alert_type:'warning',
			})
	  }
	  else if(verify.length!==15)
	  {
			this.setState({
				snack_open:true,
				snack_msg:'Registration Number should be of 15 digit !!',
				alert_type:'warning',
			})
	  }
	  else if((!vermail.includes('srmuniv.edu.in'))&&(!vermail.includes('srmist.edu.in')))
	  {
			this.setState({
				snack_open:true,
				snack_msg:'Enter official SRM Mail Id Please!!',
				alert_type:'warning',
			})
	  }
		else if ((this.state.phone).length!==10)
		{
			this.setState({
				snack_open:true,
				snack_msg:'Enter correct format of Phone no !!',
				alert_type:'warning',
			})
		}
		else
		{
		this.setState({disable_signup:true,enable:true})
	}
}


signedUP=(faculty_adviser)=>{
	if(!faculty_adviser)
	{
		this.setState({
			snack_open:true,
			snack_msg:'Enter Faculty Adviser Name !!',
			alert_type:'warning',
		})
	}
	else
	{
	this.closeEnable();
	this.setState({
		snack_open:true,
		snack_msg:'Signing Up !!',
		alert_type:'info',
	})

	axios.post('/ework/user2/ssignup', {
		username: this.state.regid,
		password: this.state.password,
		name: this.state.name,
		mailid: this.state.mailid,
		phone: this.state.phone,
		campus: this.state.campus,
		dept: this.state.dept,
		batch: this.state.batch,
		degree: this.state.degree,
		sem: this.state.sem,
		year: this.state.year,
		count: 4,
		faculty_adviser_id:faculty_adviser,
		suspension_status:false,
		active:false,
		render_timetable:false,
		sem_break:false,
		nightmode:false,
		resetPasswordExpires:'',
		resetPasswordToken:'',
	})
		.then(response => {
			if(response.status===200){
				if(response.data.emsg)
				{
					this.setState({
						snack_open:true,
						snack_msg:response.data.emsg,
						alert_type:'info',
					})
					this.setState(this.initialState);
				}
				else if(response.data.succ)
				{
					this.setState({
							redirectTo: '/ework/slogin'
					})
				}
			}
		}).catch(error => {
			this.setState({
				snack_open:true,
				snack_msg:'Something Went Wrong !!',
				alert_type:'error',
			})
		})
	this.setState({loader:false})
  }
}


showValidation=()=>{
	this.setState({show_modal:!this.state.show_modal})
}

closeEnable=()=>{
	this.setState({enable:!this.state.enable})
}

handleMouseDownPassword = event => {
    event.preventDefault();
  };

render() {
	const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
        </ContentLoader>
      )
			if (this.state.redirectTo) {
					 return <Redirect to={{ pathname: this.state.redirectTo }} />
			 } else {
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status ==="none")
	{
		return(
			<RegistrationStopped login_path="/ework/slogin" section_name="STUDENT REGISTRATION"/>
		);
	}
	else{

	//	console.log(this.state.batch_ref)

		const number=[{id:'A'},{id:'B'},{id:'C'},{id:'D'},{id:'E'},{id:'F'},{id:'G'},{id:'H'},{id:'I'},{id:'J'},{id:'K'},{id:'L'},{id:'M'}
	,{id:'N'},{id:'O'},{id:'P'},{id:'Q'},{id:'R'},{id:'S'},{id:'T'},{id:'U'},{id:'V'},{id:'W'},{id:'X'},{id:'Y'},{id:'Z'}]

	return (
	  <React.Fragment>
		{this.state.enable &&
			 <FacultyAdviser closeEnable={this.closeEnable}
			 signUP={this.signedUP} state={this.state} />
	   }

		 <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
		 open={this.state.snack_open} autoHideDuration={2000}
		 onClose={()=>this.setState({snack_open:false})}>
			 <Alert onClose={()=>this.setState({snack_open:false})}
			 severity={this.state.alert_type}>
				 {this.state.snack_msg}
			 </Alert>
		 </Snackbar>

		<Grid container spacing={1}>
		<Hidden xsDown><Grid item sm={2}/></Hidden>
		<Grid item xs={12} sm={8}>
		<Paper elevation={3} style={{padding:'10px',marginTop:'30px'}}>
			<Typography variant="h5" color="secondary" align="center" >REGISTRATION</Typography>
				<form className="form-con">
						<Grid container spacing={1}>
								<Grid item xs={12} sm={6}>
									<TextField id="name" name="name" type="text" fullWidth
									 value={this.state.name} onChange={this.handleInput} label="Name" />
								</Grid>
								<Grid item xs={12} sm={6}>
									<TextField id="stud_id" type="text" className="validate" name="regid" value={this.state.regid} onChange={this.handleInput}
									fullWidth label="Registration Number" />
								</Grid>
						</Grid>
<br /><br />


						<Grid container spacing={1}>
								<Grid item xs={12} sm={5}>
									<TextField id="email" type="email" className="validate" name="mailid" value={this.state.mailid} onChange={this.handleInput}
									fullWidth label="Official Mail Id" />
								</Grid>

								<Grid item xs={12} sm={4}>
									<TextField id="ph_num" type="number" className="validate" name="phone" value={this.state.phone} onChange={this.handleInput}
									fullWidth label="Phone Number" />
								</Grid>
									<Grid item xs={12} sm={3}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="sel_degree">Degree</InputLabel>
													<Select
														labelId="sel_degree"
														id="sel_degree"
														name="degree"
														value={this.state.degree}
														onChange={this.handleInput}
													 >
															<MenuItem value="" disabled defaultValue>Year</MenuItem>
															<MenuItem value="B.tech">B.Tech</MenuItem>
															<MenuItem value="M.tech">M.Tech</MenuItem>
															<MenuItem value="BCA">BCA</MenuItem>
															<MenuItem value="MCA">MCA</MenuItem>
													</Select>
											 </FormControl>
									</Grid>

						</Grid>
<br /><br />
						<Grid container spacing={1}>
								<Grid item xs={12} sm={6}>
								<FormControl style={{width:'100%'}}>
									 <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
									 <Input
										 type={this.state.showPassword ? 'text' : 'password'}
										 onChange={this.handleInput}  name="password" id="pswd1" value={this.state.password}
										 endAdornment={
											 <InputAdornment position="end">
												 <IconButton
													 aria-label="toggle password visibility"
													 onClick={()=>this.setState({showPassword:!this.state.showPassword})}
													 onMouseDown={this.handleMouseDownPassword}
												 >
													 {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
												 </IconButton>
											 </InputAdornment>
										 }

									 />
										 <HelpOutlineIcon style={{color:'green'}} onClick={this.showValidation} />
								 </FormControl>
											<React.Fragment>
												<Dialog
													open={this.state.show_modal}
													onClick={this.showValidation}
													aria-labelledby="alert-dialog-title"
													aria-describedby="alert-dialog-description"
												>
													<DialogTitle id="alert-dialog-title">Password Should Consist of </DialogTitle>
													<DialogContent>
														<DialogContentText id="alert-dialog-description">
															<Typography variant="button" display="block" gutterBottom>
															At least 1 uppercase character.<br />
															At least 1 lowercase character.<br />
															At least 1 digit.<br />
															At least 1 special character.<br />
															Minimum 6 characters.<br />
															</Typography>
														</DialogContentText>
													</DialogContent>
													<DialogActions>
														<Button onClick={this.showValidation} color="primary" autoFocus>
															Agree
														</Button>
													</DialogActions>
												</Dialog>
										 </React.Fragment>
								</Grid>

								<Grid item xs={12} sm={6}>

								<FormControl style={{width:'100%'}}>
									 <InputLabel htmlFor="standard-adornment-password">Confirm Password</InputLabel>
									 <Input
										 type={this.state.showPassword ? 'text' : 'password'}
										 onChange={this.handleInput}  name="cnf_pswd" id="pswd" value={this.state.cnf_pswd}
										 endAdornment={
											 <InputAdornment position="end">
												 <IconButton
													 aria-label="toggle password visibility"
													 onClick={()=>this.setState({showPassword:!this.state.showPassword})}
													 onMouseDown={this.handleMouseDownPassword}
												 >
													 {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
												 </IconButton>
											 </InputAdornment>
										 }

									 />
								 </FormControl>
								</Grid>
						</Grid>
<br /><br />
						<Grid container spacing={1}>
								<Grid item xs={12} sm={3}>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_year">Year</InputLabel>
												<Select
													labelId="sel_year"
													id="sel_year"
													name="year"
													value={this.state.year}
													onChange={this.handleInput}
												>
													<MenuItem value="" disabled defaultValue>Year</MenuItem>
													<MenuItem value="1">First Year</MenuItem>
													<MenuItem value="2">Second Year</MenuItem>
													<MenuItem value="3">Third Year</MenuItem>
													<MenuItem value="4">Fourth Year</MenuItem>
													<MenuItem value="5">Fifth Year</MenuItem>
												</Select>
										 </FormControl>
								</Grid>

							<Grid item xs={12} sm={3}>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_sem">Semester</InputLabel>
												<Select
													labelId="sel_sem"
													id="sel_sem"
													name="sem"
													value={this.state.sem}
													onChange={this.handleInput}
												>
													<MenuItem value="" disabled defaultValue>Year</MenuItem>
													<MenuItem value="1">1</MenuItem>
													<MenuItem value="2">2</MenuItem>
													<MenuItem value="3">3</MenuItem>
													<MenuItem value="4">4</MenuItem>
													<MenuItem value="5">5</MenuItem>
													<MenuItem value="6">6</MenuItem>
													<MenuItem value="7">7</MenuItem>
													<MenuItem value="8">8</MenuItem>
													<MenuItem value="9">9</MenuItem>
													<MenuItem value="10">10</MenuItem>
												</Select>
										 </FormControl>

								</Grid>
								<Grid container spacing={1} item xs={12} sm={6}>

								<Grid item xs={6} sm={6}>
									<FormControl style={{width:'100%'}}>
										<InputLabel id="sel_batch">Batch Type</InputLabel>
											<Select
												labelId="sel_batch"
												id="sel_batch"
												name="batch"
												value={this.state.batch_ref}
												onChange={(e)=>this.setState({batch_ref:e.target.value})}
											>
											{number.map((content,index)=>{
												return(
													<MenuItem value={content.id}>{content.id}</MenuItem>
												)
											})}
											</Select>
									 </FormControl>
								</Grid>
								<Grid item xs={6} sm={6}>
									<FormControl style={{width:'100%'}}>
										<InputLabel id="sel_batch">Select Batch</InputLabel>
											<Select
												labelId="sel_batch"
												id="sel_batch"
												name="batch"
												disabled={this.state.batch_ref ? false:true}
												value={this.state.batch}
												onChange={this.handleInput}
											>
											{Array.apply(null, {length: 10}).map((i, index)=>{
												return(
													<MenuItem value={this.state.batch_ref+(index+1)}>{this.state.batch_ref}{index+1}</MenuItem>
												)
											})}
											</Select>
									 </FormControl>
								</Grid>
								</Grid>

						</Grid>
						<br /><br />
					<Grid container spacing={1}>
								<Grid item xs={12} sm={6}>
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_campus">Campus</InputLabel>
												<Select
													labelId="sel_campus"
													id="sel_campus"
													name="campus"
													value={this.state.campus}
													onChange={this.handleInput}
												>
												<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
												<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
												<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
												<MenuItem value="NCR Campus">NCR Campus</MenuItem>
												</Select>
										 </FormControl>
								</Grid>
									<Grid item xs={12} sm={6}>
									<FormControl style={{width:'100%'}}>
										<InputLabel id="dept">Department</InputLabel>
											<Select
												labelId="dept"
												id="dept"
												value={this.state.dept} name="dept" onChange={this.handleInput}
											>
														{this.state.department.map((content,index)=>{
															return(
																<MenuItem key={index} value={content.department_name}>{content.department_name}</MenuItem>
															)
												})}
												</Select>
										</FormControl>
								</Grid>
						</Grid>
						<br/><br />
						<Grid container spacing={2}>
						  <Grid item xs={4} sm={4}>
							  <Link to='/ework/slogin' style={{textDecoration:'none'}} className="log">Login Instead ?</Link>
							</Grid>
							<Grid item sm={4} xs={4} />
							<Grid item xs={4} sm={4}>
							  <Button style={{backgroundColor:'#455a64',color:'white'}}
								fullWidth disabled={this.state.disable_signup} onClick={this.handleSubmit}>Submit</Button>
							</Grid>
						</Grid>
				</form>
				</Paper>
				</Grid>
		</Grid>

		</React.Fragment>
	);
}
	}
}
}
}



class FacultyAdviser extends Component {
	constructor()
	{
		super()
		this.state={
			adviser:'',
			selected:'',
		}
		this.componentDidMount= this.componentDidMount.bind(this)
	}
	componentDidMount()
	{
this.fetchDatas();
	}

	fetchDatas=()=>{
		axios.post('/ework/user/fetch_faculty_adviser',{datas:this.props.state})
		.then( res => {
				if(res.data)
				{
					//console.log(res.data)
					this.setState({adviser:res.data})
				}
		});
	}

handleField=(e)=>{
	this.setState({selected:e.target.value})
}

  render() {
		//console.log(this.props.state)
    return (
      <React.Fragment>
					<Dialog
						open={this.props.state.enable}
						aria-labelledby="alert-dialog-title"
						aria-describedby="alert-dialog-description"
					>
						<DialogTitle id="alert-dialog-title">Please Select Your Faculty Adviser</DialogTitle>
						<DialogContent>
							<DialogContentText id="alert-dialog-description">
									{this.state.adviser.length>0 ?
										<FormControl style={{width:'100%'}}>
										<InputLabel id="dept">Faculty Adviser</InputLabel>
											<Select
												labelId="adviser"
												id="adviser"
												value={this.state.selected} name="adviser" onChange={this.handleField}
											>
														{this.state.adviser.map((content,index)=>{
															return(
																<MenuItem key={index} value={content.username}>{content.username} - {content.name}</MenuItem>
															)
												})}
												</Select>
										</FormControl>
										:
										<div className="center">No Data Found !!</div>
									}
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button onClick={()=>this.props.signUP(this.state.selected)} color="primary" autoFocus>
								SUBMIT
							</Button>
						</DialogActions>
					</Dialog>
      </React.Fragment>
    );
  }
}
