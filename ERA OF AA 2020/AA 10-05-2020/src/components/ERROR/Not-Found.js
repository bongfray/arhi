import React from 'react';
import Error from './error3.png'
import { Link } from 'react-router-dom';
import {Typography} from '@material-ui/core'

export default class Not extends React.Component{
    render(){
        return (
            <React.Fragment>

                    <div className="error-img" style={{float:'center'}}>
                    <img alt="Error Page"src={Error} className="error-image"/>
                    </div>

                <div className="row fof">
                        <Typography variant="h1" align="center">Oops! You took a wrong turn...</Typography>
                </div>
                <br />
                    <div style={{textAlign:'center'}}>
                      <Link style={{textDecoration:'none'}} to="/ework">
                        <button className="button-home">Go to Home Page</button>
                      </Link>
                    </div>

            </React.Fragment>
        )
    }
}
