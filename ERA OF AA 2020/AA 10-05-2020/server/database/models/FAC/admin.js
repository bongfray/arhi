const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const adminorder = new Schema({
  usertype: {type: String , unique: false, required: false},
  order:{type: String , unique: false, required: false},
  action:{type: String , unique: false, required: false},
  start_date:{type: String , unique: false, required: false},
  end_date:{type: String , unique: false, required: false},
  academic_year:{type: String , unique: false, required: false},
  status:{type: Boolean, unique: false, required: false},
  active: {type: Number, unique: false, required: false},
  start_sem: {type: Boolean, unique: false, required: false},
  end_sem: {type: Boolean, unique: false, required: false},
  start_by:{type: String, unique: false, required: false},
  end_by:{type: String, unique: false, required: false},
})

adminorder.plugin(autoIncrement.plugin, { model: 'Admin-Order', field: 'serial', startAt: 1,incrementBy: 1 });



var Admin = mongoose.model('Admin-Order', adminorder);

module.exports = Admin
