const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

//Schema
const timeSchema = new Schema({
  ation: { type: String, unique: false, required: false },
  verified: { type: Boolean, unique: false, required: false },
  positive_count: { type: Number, unique: false, required: false },
  negative_count: { type: Number, unique: false, required: false },
  username: { type: String, unique: false, required: false },
  day_order: { type: String, unique: false, required: false },
  order: { type: String, unique: false, required: false },
  selected: { type: String, unique: false, required: false },
  freefield: { type: String, unique: false, required: false },
  freeslot: { type: String, unique: false, required: false },
  freeparts: { type: String, unique: false, required: false },
  compensation_status: { type: Boolean, unique: false, required: false },
  c_cancelled: { type: String, unique: false, required: false },
  problem_statement: { type: String, unique: false, required: false },
  covered: { type: String, unique: false, required: false },
  time: { type: String, unique: false, required: false },
  slot: { type: String, unique: false, required: false },
  date: { type: String, unique: false, required: false },
  month: { type: String, unique: false, required: false },
  year: { type: String, unique: false, required: false },
  week: { type: String, unique: false, required: false },
  problem_compensation_date: { type: String, unique: false, required: false },
  problem_compensation_month: { type: String, unique: false, required: false },
  problem_compensation_year: { type: String, unique: false, required: false },
  problem_compensation_hour: { type: String, unique: false, required: false },
  for_year: { type: String, unique: false, required: false },
  for_sem: { type: String, unique: false, required: false },
  for_batch: { type: String, unique: false, required: false },
  compense_faculty: { type: String, unique: false, required: false },
  hour: { type: String, unique: false, required: false },
  compense_faculty_topic: { type: String, unique: false, required: false },
  subject_code: { type: String, unique: false, required: false },
  problem_compensation_day_order: { type: String, unique: false, required: false },
})
const Time_table = mongoose.model('TimeTable', timeSchema)
module.exports = Time_table
