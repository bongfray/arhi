const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const notification = new Schema({
  action: { type: String, unique: false, required: false },
  for: { type: String, unique: false, required: false },
  subject: { type: String, unique: false, required: false },
  details: { type: String, unique: false, required: false },
  ref: { type: Number, unique: false, required: false },
  db_name: { type: String, unique: false, required: false },
  new: { type: Boolean, unique: false, required: false, default:true },
  faculty_id: { type: String, unique: false, required: false },
  covered_topic: { type: String, unique: false, required: false },
  date:{ type: String, unique: false, required: false },
  month:{ type: String, unique: false, required: false },
  year:{ type: String, unique: false, required: false },
  week:{ type: String, unique: false, required: false },
  slot:{ type: String, unique: false, required: false },
  day_order:{ type: String, unique: false, required: false },
  hour:{ type: String, unique: false, required: false },
  submitted_data:{ type: String, unique: false, required: false },
  username:{ type: String, unique: false, required: false },
  batch:{ type: String, unique: false, required: false },
  for_year:{ type: String, unique: false, required: false },
  sem:{ type:String, unique: false, required: false},
  dept:{ type:String, unique: false, required: false },
})


notification.plugin(autoIncrement.plugin, { model: 'Student Data', field: 'acc_number', startAt: 1,incrementBy: 1 });

const noti = mongoose.model('Student Data', notification)
module.exports = noti
