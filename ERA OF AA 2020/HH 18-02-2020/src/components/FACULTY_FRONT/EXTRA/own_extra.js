import React, { Component } from 'react';
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import Button from '@material-ui/core/Button';
import ForeignExtra from './foreign_extra'
import TimeDiv from '../TimeTable/slot2'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';
require("datejs")



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class ownExtra extends Component {
  constructor()
  {
    super()
    this.state={
      active:0,
      loading: true,
      isChecked: false,
      history:'',
      redirectTo:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      noti_route:true,
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleComp = (e) =>{
    this.setState({
      isChecked: !this.state.isChecked,
      history: e.target.value,
    })
  }
  componentDidMount()
  {
    this.loggedin()
  }
  loggedin = ()=>{
    axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/ework/faculty'})
         window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }


  handleSet=(e)=>{
    this.setState({
      active: e,
    })
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "col l6 xl6 go center #69f0ae green accent-2 black-text active-pressed";
      }
      return "col l6 xl6 #e0e0e0 grey lighten-2 center active-pressed go";
  }

  render() {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return (
      <React.Fragment>
                <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div>
      <div className="row">
         <div className="col l4 xl4 m5 s4"/>
         <div className="col l4 xl4 s4 m2">
           <div className="row" style={{marginTop:'15px'}}>
              <div onClick={(e)=>{this.handleSet(0)}} className={this.color(0)}>Own Extra</div>
              <div onClick={(e)=>{this.handleSet(1)}} className={this.color(1)}>Foreign Extra</div>
           </div>
         </div>
         <div className="col l5 xl5 m5 s4"/>
      </div>
        <br /><br />
        <InputValue datas={this.state.active} username={this.state.username} />
      </div>
      </React.Fragment>
    );
  }
  }
}



class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
      date:'',
      dayorder:'',
      hour:'',
      day_order:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  addAllot =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }
  componentDidMount()
  {
    this.fetchdayorder()
  }
  fetchdayorder =()=>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{
          this.setState({
            day_order: response.data.day_order,
          });
    });
  }
  render(){
    if(this.props.datas === 1)
    {
      return(
        <React.Fragment>
          <ForeignExtra />
        </React.Fragment>
      );
    }
    else
    {
      return(
        <React.Fragment>
          <OwnExtra username={this.props.username} day_order={this.state.day_order} />
        </React.Fragment>
      );
    }
  }
}


class  OwnExtra extends Component {
  constructor() {
    super()
    this.state={
      compense_data:[],
      modal: false,
      color:'red',
      notfound:'',
      content:'',
      datas_a:'',
      props_content:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchCompenseClasses()

  }
  fetchCompenseClasses = ()=>{
    axios.get('/ework/user/fetch_own_extra')
    .then( res => {
      if(res.data.length === 0)
      {
        this.setState({notfound:'No Data Found !!'})
      }
      else{
        this.setState({compense_data:res.data})
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
showModal=(e,content)=>{
  this.setState({modal:true,content:content,
  datas_a:{
    for_year:content.for_year,
    for_sem:content.for_sem,
    for_batch:content.for_batch,
  },
  props_content:{
    number:content.hour,
  }
})
}

color =(hour,day,month,year) =>{
	axios.post('/ework/user/fetchfrom', {
		day_order:this.props.day_order,
    hour:hour,
		date: day,
		month:month,
		year: year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
					loading:false,
				})
		}
	})
}

  render()
  {
    // console.log(this.props.day_order)
    var date = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    return(
      <React.Fragment>
      <React.Fragment>
      {this.state.compense_data.length>0 ?
      <React.Fragment>
      <table>
         <thead>
           <tr>
             <td className="center"><b>Requested On</b></td>
             <td className="center"><b>Requested DayOrder / Hour</b></td>
             <td className="center"><b>Requested Slot</b></td>
             <td className="center"><b>For Batch-Sem-Year</b></td>
             <td className="center"><b>Deadline(dd-mm-yyyy)</b></td>
             <td className="center"><b>Status</b></td>
           </tr>
         </thead>
         <tbody>
         {this.state.compense_data.map((content,index)=>(
             <tr key={index}>
                 <td className="center">{content.date}-{content.month}-{content.year}</td>
                 <td className="center">{content.day_order} / {content.hour}</td>
                 <td className="center">{content.slot}</td>
                 <td className="center">{content.for_batch}-{content.for_sem}-{content.for_year}</td>
                 <td className="center">{content.date}-{content.month}-{content.year} [Before 11:59pm]</td>
                 <td className="center">
                   <button className="btn btn-small" onClick={(e)=>this.showModal(e,content)}>UPLOAD</button>
                 </td>
             </tr>
         ))}
         </tbody>
      </table>
      </React.Fragment>
      :
      <h5 className="center">No request Found !!</h5>
      }
      </React.Fragment>

               {this.state.modal &&
                 <Dialog
      open={this.state.modal}
      TransitionComponent={Transition}
      keepMounted
      onClose={this.selectModal}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogContent style={{width:'230px'}}>
                    <div className="row">
                       <div className="col l12">
                           <TimeDiv day_order={this.props.day_order} username={this.props.username}
                            date={date} month={month} year={year}
                            datas_a={this.state.datas_a}
                            slots={this.state.content.slot}
                            content={this.state.props_content}
                            displayModal={this.state.modal}
                            closeModal={this.selectModal}
                            cday={true}
                            color={() => this.color(this.state.content.problem_compensation_hour,date,month,year)}
                            />
                       </div>
                    </div>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={this.selectModal} color="primary">
                        Close
                      </Button>
                    </DialogActions>
                  </Dialog>
                  }
      </React.Fragment>
    );
  }
}
