import React, { Component } from 'react'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import Degree from './Degree'

export default class ActualDash extends Component {
  constructor(props) {
		super(props);
    this.state = {
      active:0,
      loading: true,
      sending:'SUBMIT',
      id:'',
      up_username:'',
      up_name:'',
      up_phone:'',
      campus:'',
      dept:'',
      desgn:'',
      up_dob: '',
      new_password:'',
      current_password:'',
      up_confirm_password:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      login:'/ework/user/flogin',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      noti_route:true,
    }
      this.componentDidMount = this.componentDidMount.bind(this)
      this.handleSub = this.handleSub.bind(this)
  }
  componentDidMount() {
    this.loggedIn()
}
loggedIn = ()=>{
  axios.get('/ework/user/')
  .then(res=>{
    if(res.data.user)
    {
      this.setState({username:res.data.user.username})
      this.getContent();
    }
    else
    {
      this.setState({redirectTo:'/ework/faculty',loading:false})
       window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
    }
  })
}
getContent =()=>{
  axios.post('/ework/user/dash2')
  .then(response => {
    if(response.status === 200)
    {
      if(response.data)
      {
      this.setState({
        loading:false,
        datas_of_user:response.data,
        id: response.data.username,
        up_username: response.data.mailid,
        up_name: response.data.name,
        up_phone: response.data.phone,
        dept: response.data.dept,
        campus: response.data.campus,
        desgn: response.data.desgn,
        up_dob: response.data.dob
      })
    }
    }
  })
  .catch( err => {
    window.M.toast({html: 'Error !!',classes:'rounded red'});
  });
}


handleSub(event) {
  event.preventDefault()
  if((this.state.up_confirm_password) || (this.state.new_password) || (this.state.current_password))
  {
    if((this.state.up_confirm_password) && (this.state.new_password) && (this.state.current_password))
    {
      if(this.state.up_confirm_password === this.state.new_password)
      {
        if((this.state.up_phone).length!==10){
          window.M.toast({html: 'Phone no is not of 10 digit !!',classes:'rounded #ec407a pink lighten-1'});
          this.setState({
             sending:'SUBMIT',
         })
        }
        else{
   axios.post('/ework/user/newd', {
     id: this.state.id,
     up_username: this.state.up_username,
     up_name: this.state.up_name,
     up_phone: this.state.up_phone,
     up_dob: this.state.up_dob,
     new_password: this.state.new_password,
     current_password: this.state.current_password,
   },this.setState({
     sending: 'UPDATING...',
   }))
    .then(response => {
      console.log(response)
      if(response.status===200){
        if(response.data.succ)
         {
          window.M.toast({html: response.data.succ,classes:'rounded #ec407a green lighten-1'});
           this.setState({
              redirectTo: '/ework/newd',
              sending:'SUBMIT',
          })
        }
        else if(response.data.fail)
        {
          this.setState({
             errors:'Kindly Match Your Current Password',
             sending:'SUBMIT',
         })
          return false;
        }
      }
     }).catch(error => {
       window.M.toast({html: 'Error !!',classes:'rounded #ec407a pink lighten-1'});
       this.setState({
          sending:'SUBMIT',
      })
     })
    this.setState({
      up_name: this.state.up_name,
      id: this.state.id,
      up_username: this.state.up_username,
      up_phone: this.state.up_phone,
      campus: this.state.campus,
      dept: this.state.dept,
      up_dob:this.state.up_dob,
      desgn: this.state.desgn,
      up_confirm_password: '',
      current_password: '',
      new_password: '',
      })
    }
    }else{
      this.setState({
        errors:'New Password Does not Match !!',
        up_confirm_password: '',
        new_password: '',
        sending:'SUBMIT'
        })
      return false;

    }
  }
    else{
      window.M.toast({html: 'Fill all the fields'});
      return false;
    }
  }
  else
  {
    if((this.state.up_phone).length!==10)
    {
      this.setState({
         errors:'Updated phone no is not of 10 digit!',
         sending:'SUBMIT',
     })
    }
    else
    {
    axios.post('/ework/user/newd', {
      id: this.state.id,
      up_username: this.state.up_username,
      up_name: this.state.up_name,
      up_phone: this.state.up_phone,
      up_dob: this.state.up_dob,
      new_password: this.state.new_password,
      up_confirm_password: this.state.up_confirm_password,
      current_password: this.state.current_password
    })
     .then(response => {
       console.log(response)
       if(response.status===200){
         if(response.data.succ)
          {
           window.M.toast({html: response.data.succ,classes:'rounded #ec407a green darken-1'});
         }
       }
      }).catch(error => {
        window.M.toast({html: 'Internal Error!!',});
      })
     this.setState({
      up_name: this.state.up_name,
      id: this.state.id,
      up_username: this.state.up_username,
      up_phone: this.state.up_phone,
      campus: this.state.campus,
      dept: this.state.dept,
      up_dob:this.state.up_dob,
      desgn: this.state.desgn,
      up_confirm_password: '',
      current_password: '',
      new_password: '',
       })
     }
}
}
handleChange= (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });

    };
    handleSet=(e)=>{
      this.setState({
        active: e,
      })
    }

    color =(position) =>{
      if (this.state.active === position) {
          return "col l5 xl5 go center #69f0ae green accent-2 black-text active-pressed";
        }
        return "col l5 xl5 #e0e0e0 grey lighten-2 center active-pressed go";
    }


  render() {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="170" y="30" rx="3" ry="3" width="70" height="10" />
        <rect x="10" y="60" rx="3" ry="3" width="70" height="10" />
        <rect x="90" y="60" rx="3" ry="3" width="90" height="10" />
        <rect x="220" y="60" rx="3" ry="3" width="70" height="10" />
        <rect x="300" y="60" rx="3" ry="3" width="90" height="10" />

        <rect x="10" y="90" rx="3" ry="3" width="70" height="10" />
        <rect x="90" y="90" rx="3" ry="3" width="90" height="10" />
        <rect x="220" y="90" rx="3" ry="3" width="70" height="10" />
        <rect x="300" y="90" rx="3" ry="3" width="90" height="10" />

        <rect x="10" y="120" rx="3" ry="3" width="70" height="10" />
        <rect x="90" y="120" rx="3" ry="3" width="90" height="10" />
        <rect x="220" y="120" rx="3" ry="3" width="70" height="10" />
        <rect x="300" y="120" rx="3" ry="3" width="90" height="10" />

      </ContentLoader>
    )
    if(this.state.loading === true)
    {
      return(
        <MyLoader />
      );
    }
    else if(this.state.loading === false){
		return (
      <React.Fragment>
      <Nav noti_route={this.state.noti_route} login={this.state.login} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div className="row">
         <div className="col l5 xl5 m5 s4"/>
         <div className="col l2 xl2 s4 m2">
           <div className="row" style={{marginTop:'15px'}}>
              <div onClick={(e)=>{this.handleSet(0)}} className={this.color(0)}>Profile</div>
              <div className="col l2"/>
              <div onClick={(e)=>{this.handleSet(1)}} className={this.color(1)}>Degree</div>
           </div>
         </div>
         <div className="col l5 xl5 m5 s4"/>
      </div>
       {this.state.active ===0 ?
         <div className="particular" style={{margin:'0px 15px 0px 15px'}}>
                 <div className="">
                 <div className="">
                   <Link to="/ework/faculty"><h5 className="prof-heading black-text center">Profile Datas</h5></Link>
                 </div><br /><br />
                       <div>
                        {this.renderCells()}
                       </div>
                 </div>
         </div>
         :
        <Degree reference="none" />
       }
      </React.Fragment>
      );
    }
  }

  renderCells() {
    return(
      <div className="profile">

        <div className="row">
              <div className="col l6 xl6 m12 s12">
                <div className="row">
                      <div className="col l4 xl4 m6 s6">
                        <span className="proftitle">Official ID</span>
                      </div>
                      <div className="col l8 s6 m6 xl8">
                        <Cell value={ this.state.id }  />
                      </div>
                </div>
              </div>
             <div className="col l6 xl6 m12 s12">
               <div className="row">
                   <div className="col l4 xl4 m6 s4">
                     <span className="proftitle">Official Mail:</span>
                   </div>
                   <div className="col l8 s8 m8 xl8">
                     <Cell value={ this.state.up_username } />
                   </div>
               </div>
            </div>
         </div>

      <div className="row">
              <div className="col l6 xl6 m12 s12">
                  <div className="row">
                    <div className="col l4 xl4 m6 s6">
                      <span className="proftitle">Name</span>
                    </div>
                    <div className="col l8 s6 m6 xl8">
                      <Cell value={ this.state.up_name } name="name"  onChange={value => this.setState({up_name: value})} />
                    </div>
                  </div>
              </div>
               <div className="col l6 xl6 m12 s12">
                 <div className="row">
                     <div className="col l4 xl4 m6 s6">
                       <span className="proftitle">Mobile No:</span>
                     </div>
                     <div className="col l8 s6 m6 xl8 left">
                       <Cell value={ this.state.up_phone } name="phone" onChange={value => this.setState({up_phone: value})} />
                     </div>
                 </div>
              </div>
         </div>

         <div className="row">
             <div className="col l6 xl6 m12 s12">
             <div className="row">
               <div className="col l4 xl4 m6 s6">
               <span className="proftitle">Campus: </span>
               </div>
               <div className="col l8 s6 m6 xl8">
               <Cell value={ this.state.campus } />
               </div>
             </div>
             </div>
              <div className="col l6 xl6 m12 s12">
              <div className="row">
                  <div className="col l4 xl4 m6 s6">
                    <span className="proftitle">Designation: </span>
                  </div>
                  <div className="col l8 s6 m6 xl8">
                    <Cell value={ this.state.desgn }/>
                  </div>
              </div>
             </div>
          </div>


          <div className="row">
              <div className="col l6 xl6 m12 s12">
              <div className="row">
                <div className="col l4 xl4 m6 s6">
                <span className="proftitle">Department:</span>
                </div>
                <div className="col l8 s6 m6 xl8">
                <Cell value={ this.state.dept } />
                </div>
              </div>
              </div>
               <div className="col l6 xl6 m12 s12">
               <div className="row">
                   <div className="col l4 xl4 m6 s6">
                     <span className="proftitle">DOB:</span>
                   </div>
                   <div className="col l8 s6 m6 xl8">
                     <Cell value={ this.state.up_dob } name="phone" onChange={value => this.setState({up_dob: value})} />
                   </div>
               </div>
              </div>
           </div>
           <h6 className="tcr" style={{marginLeft:'10px'}}>RESET YOUR PASSWORD -</h6><br />
           <div className="row">

          <div className="input-field col l4 xl4 s12 m12">
                 <input id="curpass" type="password" className="validate" value={this.state.current_password} name="current_password" onChange={this.handleChange} required />
                 <label htmlFor="curpass">Current Password</label>
               </div>
                <div className="input-field col l4 xl4 s12 m12">
                    <input id="newpass" type="password" className="validate" name="new_password" value={this.state.new_password} onChange={this.handleChange} required />
                    <label htmlFor="newpass">New Password</label>
               </div>
               <div className="input-field col l4 xl4 s12 m12">
                   <input id="conpass" type="password" className="validate" name="up_confirm_password" value={this.state.up_confirm_password} onChange={this.handleChange} required />
                   <label htmlFor="conpass">Confirm Password</label>
              </div>
            </div>
<div className="row">
  <div className="col l4 xl4 s4 m4"/>
  <div className="col l4 xl4 s4 m4 center red-text">{this.state.errors}</div>
  <div className="col l1 xl1 s1 m1"/>
  <div className="col l3 xl3 s3 m3 center red-text">
      <button className="waves-effect btn blue-grey darken-2 col s3 xl3 m3 l3 prof-submit sup" style={{width:'100%'}} onClick={this.handleSub}>{this.state.sending}</button>
  </div>
</div>
      </div>
		);

	}
}

class Cell extends Component {

	constructor(props) {
		super(props);
		this.state = { editing: false };
	}

	render() {
		const { value, onChange } = this.props;
    if(this.state.editing)
    {
      return(
        <div className="row">
          <div className="col l6 xl6 m6 s10">
            <input ref='input' value={value} onChange={e => onChange(e.target.value)} onBlur={ e => this.onBlur()} />
          </div>
         <span><button className="btn left waves-effect btn pink" href="#" onBlur={ e => this.onBlur()}>Save</button></span>
        </div>

      );
    }
    else
    {
      if(this.props.name)
      {
      return(
        <div className="row">
          <div className="col l8 xl8 s8 m8">
            <span>{value}</span>
          </div>
        <div className="col l4 s4 xl4 m4">
          <button className="btn blue-grey darken-2 " onClick={() => this.onFocus()}>Edit</button>
        </div>
        </div>
      );
    }
    else{
      return(
        <div className="row">
          <div className="col l12 s8 xl12 m8">
            <span>{value}</span>
          </div>
        </div>
      );
    }
    }


	}

	onFocus() {
		this.setState({ editing: true }, () => this.refs.input.focus());
	}

	onBlur() {
		this.setState({ editing: false });
	}
}
