import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class Notification extends Component {
    constructor()
    {
        super()
        this.state={
            isExpanded:false,
            display:'',
            loader:true,
            order_action:'faculty',
            notification:[],
            covered:[],
            fetching:true,
            reply_message:'',
            permission:'',
            modal_open:false,
            refer_index:0,
            anchorEl:null,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchNoti()
    }

  fetchNoti = ()=>{
    let noti_route;
    if(this.props.noti_route)
    {
      noti_route = '/ework/user/fetch_notification';
    }
    else {
      noti_route = '/ework/user2/fetch_notification';
    }
    axios.get(noti_route)
    .then( res => {
          this.setState({notification:res.data,loader:false})
    });
  }
    clearOne =(id)=>{
      let clear_one;
      if(this.props.noti_route)
      {
        clear_one = '/ework/user/clear_one_notification';
      }
      else {
        clear_one = '/ework/user2/clear_one_notification';
      }

      const { notification } = this.state;
      axios.post(clear_one,{id:id})
          .then(response => {
            this.setState({
              response: response,
              notification: notification.filter(noti => noti.serial !== id)
           })
          })
    }
    clearAll = ()=>{
      let clear_all;
      if(this.props.noti_route)
      {
        clear_all = '/ework/user/clear_all_notification';
      }
      else {
        clear_all = '/ework/user2/clear_all_notification';
      }

      axios.post(clear_all)
      .then( res => {
        this.setState({
          notification: [],
       })
      });
    }
    setDisplay =() =>{
      this.props.setDisp({
        notidisp: 'none',
      });
    }
    referOther =(ref,dbname)=>{
      axios.post('/ework/user/referNotification_Fetch',{ref:ref})
      .then( res => {
          if(res.data)
          {
              this.setState({covered:res.data,fetching:false})
          }
      });
    }

    handleInput =(e)=>{
      this.setState({[e.target.name]:e.target.value})
    }

    sendReply =(no,id)=>{
      if(!(this.state.reply_message))
      {
        window.M.toast({html: 'Enter all the Details !!', classes:'rounded yellow black-text'});
      }
      else{
      axios.post('/ework/user/send_reply_to_complaint',{serial:no,official_id:id,message:this.state.reply_message})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({reply_message:''});
          this.handleClose();
          window.M.toast({html: 'Replied !!',classes:'rounded green darken-2'});
        }
      });
     }
    }



    handleExpand =(e,serial,index) => {
      this.setState({isExpanded:!this.state.isExpanded,index,refer_index:serial,anchorEl:e.currentTarget});
    };
    handleClose=()=>{
      this.setState({modal_open:!this.state.modal_open})
    }

    render() {

      function formatDate(string){
        var dateString =string.toString();
          var options = { year: 'numeric', month: 'long', day: 'numeric' };
          return new Date(dateString).toLocaleDateString([],options);
      }
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

        return (
            <React.Fragment>
            <div className="notinoti">
            <div className="row blue-grey darken-2 white-text">
            <div className="col l1">
            <div className="left go" style={{marginTop:'10px',marginLeft:'10px'}} onClick={this.setDisplay}><i className="small material-icons white-text">close</i></div>
            </div>
            <div className="col l10 center"><h5>Notifications</h5></div>
            <div className="col l1" />
            </div>
            {this.state.loader ?
                <React.Fragment>
                  <Backdrop  open={true} >
                    <CircularProgress color="secondary" />
                  </Backdrop>
                </React.Fragment>
                :
            <Grid container spacing={1}>
            <Grid item xs={1} sm={1}/>
            <Grid item xs={10} sm={10}>
            {this.state.notification.length>0 ?
              <React.Fragment>
                  {this.state.notification.map((content,index)=>{
                    return(
                      <React.Fragment key={index}>
                      <Grid container spacing={0}>
                        <Grid item xs={12} sm={12}>
                        <Card style={{width:'100%',borderRadius:'15px'}}>
                            <CardHeader
                              action={
                                <IconButton aria-label="settings"
                                onClick={(e)=>this.handleExpand(e,content.serial,index)}>
                                  <MoreVertIcon />
                                  {(this.state.isExpanded && (this.state.refer_index === content.serial)) &&
                                    <Menu
                                      anchorEl={this.state.anchorEl}
                                      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                                      keepMounted
                                      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                      open={this.state.isExpanded}
                                      onClose={this.handleExpand}
                                    >
                                      <MenuItem onClick={() => this.clearOne(content.serial)}>Delete</MenuItem>
                                    </Menu>
                                  }
                                </IconButton>
                              }
                              title={content.subject}
                              subheader={formatDate(content.received)}
                              />
                          <CardContent>
                            {content.details}
                            <br />
                            {content.ref &&
                              <React.Fragment>
                               <Typography variant="button" display="block" gutterBottom>
                                  MESSAGE FROM EWORK :
                               </Typography>
                                <Typography variant="overline" display="block" gutterBottom>
                                 You have been given the supreme priority for this complaint. You have to handle this complaint.
                                </Typography>
                              </React.Fragment>
                            }
                          </CardContent>
                        {content.ref &&
                            <CardActions>
                              <Button onClick={()=>this.referOther(content.ref,content.db_name)} size="small" color="primary">
                                Reference
                              </Button>
                            </CardActions>
                          }
                        </Card>
                        </Grid>
                      </Grid>
                      <br />
                    </React.Fragment>
                    );
                    })
                  }
            </React.Fragment>
            :
            <Typography variant="button" display="block" gutterBottom>
               Can't able to find any notification for you !!
            </Typography>
          }
              </Grid>
              <Grid item xs={1} sm={1}/>
              </Grid>
            }

            <Dialog
              open={this.state.modal_open}
              TransitionComponent={Transition}
              keepMounted
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-slide-title"
              aria-describedby="alert-dialog-slide-description"
            >
              <DialogTitle id="alert-dialog-slide-title">{"Use Google's location service?"}</DialogTitle>
              <DialogContent>
              {this.state.fetching === true ?
                <div className="center">Fetching Information....</div>
                :
                       <React.Fragment>
                       <Grid container spacing={1}>
                          <Grid item xs={6} sm={6}>Complaint By</Grid>
                          <Grid item xs={6} sm={6}>{this.state.covered.official_id}</Grid>
                       </Grid>
                       <Grid container spacing={2}>
                          <Grid item xs={6} sm={6}>Mail Id to Contact</Grid>
                          <Grid item xs={6} sm={6}>{this.state.covered.mailid}</Grid>
                       </Grid>
                       <Grid container spacing={2}>
                          <Grid item xs={6} sm={6}>Complaint Subject</Grid>
                          <Grid item xs={6} sm={6}>{this.state.covered.complaint_subject}</Grid>
                       </Grid>
                       <Grid container spacing={2}>
                          <Grid item xs={6} sm={6}>Complaint Details</Grid>
                          <Grid item xs={6} sm={6}>{this.state.covered.complaint}</Grid>
                       </Grid>
                             <TextField
                               id="outlined-multiline-static"
                               label="Reply to the User"
                               multiline
                               rows="4"
                               fullWidth
                               name="reply_message"
                               value={this.state.reply_message}
                               onChange={this.handleInput}
                               variant="filled"
                             />
                           <br />
                       <Button className="right" color="secondary" onClick={()=>this.sendReply(this.state.covered.serial,this.state.covered.official_id)} style={{marginBottom:'5px'}}>REPLY</Button>
                      </React.Fragment>
                 }
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  CLOSE
                </Button>
              </DialogActions>
            </Dialog>

            <Grid container spacing={1}>
            {this.state.notification.length!==0 &&
              <Tooltip title="Delete All" placement="top-start">
                <IconButton aria-label="settings" style={{position:'fixed',bottom: 0,left: 0}}
                onClick={this.clearAll}>
                  <DeleteForeverIcon color="secondary" fontSize="large" />
                </IconButton>
              </Tooltip>
            }
           </Grid>
</div>
            </React.Fragment>
        );
      }
      }
}
