import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect} from 'react-router-dom'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';

import Pdf from './pdf_alldata'
import StudentView from './studentView'

require("datejs")



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});



 export default class ViewSubject extends React.Component{
  constructor()
  {
    super()
    this.state={
      option:'',
      redirectTo:'',
      modal: false,
      open:true,
      active:0,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
 componentDidMount()
 {
   if(this.props.onlyDownload)
   {
     window.M.toast({html: 'Download-Only Mode!!',classes:'rounded #ec407a pink lighten-1'});
   }
 }

 handleClose=()=>{
   this.setState({open:!this.state.open})
 }

 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }

 handleSet=(e)=>{
   this.setState({
     active: e,
   })
 }

 color =(position) =>{
   if (this.state.active === position) {
       return '#69F0AE';
     }
     return '#e0e0e0';
 }

 render()
 {
   const styleObject = makeStyles(theme => ({
     appBar: {
       position: 'relative',
     },
     title: {
       marginLeft: theme.spacing(2),
       flex: 1,
     },
   }));

   if (this.state.redirectTo) {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
 } else {
   return(
     <React.Fragment>
     {(this.props.display==='none') ?
     <React.Fragment>
     <div style={{marginTop:'15px'}} className="row">
     <div  className="col offset-l3 offset-xl3 l6 xl6 s12">
         <FormControl style={{width:'100%'}} disabled={this.props.onlyDownload}>
           <InputLabel id="option">Choose the Field Here...</InputLabel>
           <Select
             labelId="option"
             id="option"
            value={this.state.option}
            onChange={this.handleOption}
           >
           <MenuItem value="" disabled defaultValue>Choose the Field Here...</MenuItem>
           <MenuItem value="Section">Section Datas</MenuItem>
           <MenuItem value="Academic">Academic Data</MenuItem>
           <MenuItem value="Administrative">Admininstrative Datas</MenuItem>
           <MenuItem value="Research">Research Datas</MenuItem>
           <MenuItem value="Five">Five Year Plan Data</MenuItem>
           </Select>
         </FormControl>
       </div>
       <div className="col l3"><Pdf /></div>
       </div>
    <div className="row">
           <Decider username={this.props.username} referDatas={this.props.datas} data={this.state.option} />
    </div>
   </React.Fragment>
   :
      <Dialog fullScreen open={this.state.open} onClose={this.handleClose} TransitionComponent={Transition}>
          <AppBar className={styleObject.appBar}>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2}>
                  <IconButton edge="start" color="inherit" onClick={this.handleClose} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8}>
                 <div className="center" style={{fontSize:'20px'}}>Showing Details of <span className="yellow-text">{this.props.username}</span></div>
               </Grid>
               <Grid item xs={2}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
            <div style={{marginTop:'60px',display:this.props.display}}>
             <Grid container spacing={1}>
                  <Grid item sm={4} xs={1} />
                   <Grid item xs={10} sm={4}>
                       <Grid container spacing={1}>
                           <Grid item sm={6} xs={6}>
                             <Paper  onClick={(e)=>{this.handleSet(0)}} style={{height:'40px',textAlign:'center',backgroundColor:this.color(0)}}>Faculty's Data</Paper>
                           </Grid>
                           <Grid item sm={6} xs={6}>
                             <Paper onClick={(e)=>{this.handleSet(1)}} style={{height:'40px',textAlign:'center',backgroundColor:this.color(1)}} >Student Under the Faculty</Paper>
                           </Grid>
                       </Grid>
                   </Grid>
                   <Grid item sm={4} xs={1} />
             </Grid>
             <br />
             {this.state.active === 0 ?
                 <React.Fragment>
                      <Grid container spacing={1}>
                      <Grid item xs={4} sm={4} />
                       <Grid item sm={4} xs={4}>
                              <FormControl style={{width:'100%'}} disabled={this.props.onlyDownload}>
                                <InputLabel id="option">Choose the Field Here...</InputLabel>
                                <Select
                                  labelId="option"
                                  id="option"
                                 value={this.state.option}
                                 onChange={this.handleOption}
                                >
                                <MenuItem value="" disabled defaultValue>Choose the Field Here...</MenuItem>
                                <MenuItem value="Section">Section Datas</MenuItem>
                                <MenuItem value="Academic">Academic Data</MenuItem>
                                <MenuItem value="Administrative">Admininstrative Datas</MenuItem>
                                <MenuItem value="Research">Research Datas</MenuItem>
                                <MenuItem value="Five">Five Year Plan Data</MenuItem>
                                </Select>
                              </FormControl>
                          </Grid>
                          <Grid item xs={4} sm={4} >
                            <Pdf username={this.props.username}/>
                          </Grid>
                        </Grid>
                        <br />
                         <div className="row">
                                <Decider admin_action={this.props.admin_action} referDatas={this.props.datas} data={this.state.option} username={this.props.username}/>
                         </div>
                  </React.Fragment>
                :
                  <React.Fragment>
                    <StudentView username={this.props.username} />
                  </React.Fragment>
               }
            </div>
        </List>
      </Dialog>
   }
   </React.Fragment>
 );
}
 }
}

class Decider extends Component{
  render()
  {
    let body,route;
    if(this.props.data === "Section")
    {
      body = <Section admin_action={this.props.admin_action} username={this.props.username} display={this.props.display} />
    }
    else if(this.props.data === "Five")
    {
      body = <Five username={this.props.username} display={this.props.display} />
    }
    else if((this.props.data === "Administrative"))
    {
      route = '/ework/user/fetch_adminis_for_view';
      body = <AdRes username={this.props.username} display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else if(this.props.data === "Research")
    {
      route = '/ework/user/fetch_research_for_view';
      body = <AdRes username={this.props.username} display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else if(this.props.data === "Academic")
    {
      route = '/ework/user/fetch_academic_for_view';
      body = <AdRes username={this.props.username} display={this.props.display} referDatas={this.props.referDatas} action={route}/>
    }
    else{
      body
       = <div></div>
    }
    return(
      <React.Fragment>
      {this.props.data.length>0 &&
       <div>
        {body}
       </div>
     }
      </React.Fragment>
    )
  }
}

class Section extends Component {
  constructor() {
    super()
    this.state={
      section_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_section_for_view',{username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        section_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let sec_data;
    if(this.state.loader === true)
    {
      sec_data = <div className="center">
        <div class="preloader-wrapper big active">
        <div className="spinner-layer spinner-red">
            <div className="circle-clipper left">
             <div className="circle"></div>
            </div><div className="gap-patch">
             <div className="circle"></div>
            </div><div className="circle-clipper right">
             <div className="circle"></div>
            </div>
        </div>
        </div>
      </div>
    }
    else{
      sec_data =
      <React.Fragment>
         <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">
           {this.state.section_data.length === 0 ? <h5 className="red-text center" style={{marginTop:'30px'}}> No Data Found !!</h5>
            :
             <React.Fragment>
             <div className="row">
             <div className="col l1"/>
             <div className="col l10">
             <table style={{border:'1px solid black'}}>
                <thead>
                   <tr>
                     <td className="center"><b>No</b></td>
                     <td className="center"><b>Description</b></td>
                     <td className="center"><b>Your Role</b></td>
                     <td className="center"><b>Your Achivements</b></td>
                     <td className="center"><b>Duration</b></td>
                     {this.props.admin_action && <td className="center"><b>Action</b></td>}
                   </tr>
                </thead>
                <tbody>
               {this.state.section_data.slice(0, this.state.visible).map((item, index) => {
                   return (
                     <tr key={index}>
                           <td className="center">{index+1}</td>
                            <td className="center">{item.description}</td>
                            <td className="center">{item.role}</td>
                            <td className="center">{item.achivements}</td>
                            <td className="center">{item.date_of_starting} - {item.date_of_complete}</td>
                            {this.props.admin_action &&
                              <td className="center">
                                <i className="material-icons small">edit</i>
                              </td>
                            }
                     </tr>
                   );
                 })}
                 </tbody>
              </table>
              </div>
                <div className="col l1"/>
              </div>
             </React.Fragment>
           }
           </div>
           <br />
           <div className="row center">
           {this.state.visible < this.state.section_data.length &&
              <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
           }
           </div>
         </React.Fragment>

    }
    return (
      <React.Fragment>
         {sec_data}
        </React.Fragment>
    );
  }
}


class AdRes extends Component {
  constructor() {
    super()
    this.state={
      free_data:[],
      visible: 10,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 10};
    });
  }

  componentDidMount()
  {
    this.fetchData(this.props.action);
  }

  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props)
    {
      this.fetchData(this.props.action)
    }
  }

  fetchData=(route)=>{
    axios.post(route,{datas:this.props.referDatas,action:null,username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        free_data: res.data,
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let adres_data;
    if(this.state.loader === true)
    {
      adres_data = <div className="center">
        <div class="preloader-wrapper big active">
        <div className="spinner-layer spinner-red">
            <div className="circle-clipper left">
             <div className="circle"></div>
            </div><div className="gap-patch">
             <div className="circle"></div>
            </div><div className="circle-clipper right">
             <div className="circle"></div>
            </div>
        </div>
        </div>
      </div>
    }
    else{
      adres_data =      <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">
                {this.state.free_data.length === 0 ? <h5 className="red-text center" style={{marginTop:'30px'}}> No Data Found !!</h5>
                 :
                  <React.Fragment>
                  <div className="row">
                  <div className="col l1"/>
                  <div className="col l10">
                  <table style={{border:'1px solid black'}}>
                     <thead>
                        <tr>
                          <td className="center"><b>No</b></td>
                          <td className="center"><b>Type of Responsibility</b></td>
                          <td className="center"><b>Status</b></td>
                          <td className="center"><b>Submitted Data</b></td>
                          <td className="center"><b>Date of Submit</b></td>
                          <td className="center"><b>Submitted DatOrder.Slot</b></td>
                        </tr>
                     </thead>
                     <tbody>
                    {this.state.free_data.slice(0, this.state.visible).map((item, index) => {
                        return (
                          <tr key={index}>
                                <td className="center">{index+1}</td>
                                {item.freeparts ? <td className="center">{item.freeparts}</td>
                                 :
                                 <td className="center">NULL</td>
                                 }
                                 {item.selected ? <td className="center">{item.selected}</td>
                                 :
                                 <td className="center red-text">Not Applicable</td>
                                  }
                                 {item.covered ?
                                   <td className="center">{item.covered}</td>
                                   :
                                   <td className="center">{item.problem_statement}</td>
                                 }
                                 <td className="center">{item.date} / {item.month} /{item.year}</td>
                                 <td className="center">{item.day_slot_time}</td>
                          </tr>
                        );
                      })}
                      </tbody>
                   </table>
                   </div>
                   <div className="col l1"/>
                   </div>
                  </React.Fragment>
                }
                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.free_data.length &&
                   <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
                }
                </div>
              </React.Fragment>
    }
    return (
      <React.Fragment>
        {adres_data}
        </React.Fragment>
    );
  }
}


class Five extends Component {
  constructor() {
    super()
    this.state={
      five_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_five_for_view',{username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        five_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let five_dat;
    if(this.state.loader === true)
    {
      five_dat = <div className="center">
        <div class="preloader-wrapper big active">
        <div className="spinner-layer spinner-red">
            <div className="circle-clipper left">
             <div className="circle"></div>
            </div><div className="gap-patch">
             <div className="circle"></div>
            </div><div className="circle-clipper right">
             <div className="circle"></div>
            </div>
        </div>
        </div>
      </div>
    }
    else{
      five_dat =       <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">
              {this.state.five_data.length === 0 && <h5 className="red-text center" style={{marginTop:'30px'}}> No Data Found !!</h5>}
                {this.state.five_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}} >
                        <h6 className="count center">{item.action}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>About the Plan</p>
                             <p>Plan Will Expire on</p>
                             <p>Completed Status</p>
                           </div>
                           <div className="col l6">
                             <p>{item.content}</p>
                             <p>Year {item.expire_year}</p>
                             {item.completed?  <p>Completed</p> : <p>Not Completed</p>}
                           </div>
                        </div>
                      </div>
                      </div>
                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.five_data.length &&
                   <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
                }
                </div>
              </React.Fragment>
    }




    return (
      <React.Fragment>
       {five_dat}
        </React.Fragment>
    );
  }
}
