import React, { Component } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import ShowStudentList from './studentListFetched'




export default class StudentData extends Component {
  constructor()
  {
    super()
    this.state={
      skill_selected:'',
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
    .then( response => {
      console.log(response.data)
        if(response){
          this.setState({skills:response.data.sort(),loading:false})
        }
    });
  }

  setDomain=(e)=>{
    if(e === null)
    {

    }
    else{
      this.setState({skill_selected:e.skill_name})
    }
  }

  render() {
// console.log(this.state.skill_selected)
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      )
    }
    else {
    return (
      <React.Fragment>
        <Grid container spacing={1}>
           <Grid item xs={4} sm={4}/>
           <Grid item xs={4} sm={4}>
                 <Autocomplete
                    id="combo-box-demo"
                    onChange={(event, value) => this.setDomain(value)}
                    options={this.state.skills}
                    getOptionLabel={option => option.skill_name}
                    style={{width:'100%'}}
                    renderInput={params => (
                      <TextField {...params}
                       label="Combo box" variant="outlined" fullWidth />
                    )}
                  />
           </Grid>
           <Grid item xs={4} sm={4}/>
        </Grid>
        <br />
        {this.state.skill_selected &&
           <ShowStudentList type="skill_fetch" skill_selected={this.state.skill_selected} />
        }
      </React.Fragment>
    );
   }
  }
}
