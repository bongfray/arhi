import React, { Component } from 'react';
import { render } from 'react-dom';
import './style.css';
import M from 'materialize-css';
import { } from 'materialize-css';
import Logo from './Logo.png';
import Image from './image.png';
import Doc from './DocService';
import Axios from 'axios';
import PdfContainer from './PdfContainer';


var time = new Date();
// var d = time.format('m/dd/yy');

class Resume extends Component {


  constructor(){
    super();
    this.state={
      loading:true,
      profile:[],
      skills:[],
      ssc:[],
      hsc:[],
      grad:[],
      postgrad:[],
      intern:[],
      projects:[],
      pos_res:[],
      train_cert:[],
      social:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.fetchProfile();
  }

  fetchProfile(){
    Axios.post('/ework/user2/fetchall',{
      action:'Personal_Details',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({profile:res.data})
    })
    this.fetchSkills()
  }

  fetchSkills(){
    Axios.post('/ework/user2/fetchall',{
      action:'Skills',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({skills:res.data})
    })
    this.fetchEducation()
  }

  fetchEducation(){
    Axios.post('/ework/user2/fetchall',{
      action:'10th Standard',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({ssc:res.data})
    })

    Axios.post('/ework/user2/fetchall',{
      action:'12th Standard',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({hsc:res.data})
    })

    Axios.post('/ework/user2/fetchall',{
      action:'Graduation',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({grad:res.data})
    })
    this.fetchInternship()
  }

  fetchInternship(){
    Axios.post('/ework/user2/fetchall',{
      action:'Internships',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({intern:res.data})
    })
    this.fetchProjects()
  }

  fetchProjects(){
    Axios.post('/ework/user2/fetchall',{
      action:'Projects',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({projects:res.data})
    })
    this.fetchResponsibility()
  }

  fetchResponsibility(){
    Axios.post('/ework/user2/fetchall',{
      action:'Position of Responsibility',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({pos_res:res.data})
    })
    this.fetchTraining()
  }

  fetchTraining(){
    Axios.post('/ework/user2/fetchall',{
      action:'Trainings or Certificates',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({train_cert:res.data})
    })
    this.fetchSocial()
  }

  fetchSocial(){
    Axios.post('/ework/user2/fetchall',{
      action:'Social Media Account',
      username:this.props.username,
    })
    .then(res=>{
      this.setState({social:res.data})
    })
  }




  createPdf = (html) => Doc.createPdf(html);

  render() {


    return (
      <React.Fragment className="resume">

        <div className="row">
          <div className="col s3"></div>
          <div className="col s6">
            <PdfContainer loading={this.state.loading} createPdf={this.createPdf}>
          <React.Fragment>
            <section className="pdf-body">
              <div className="row">
                <span><img alt="prof" src={Logo} className="logo"/></span>
                <h2 className="htitle"><center>SRM INSTITUTE OF SCIENCE AND TECHNOLOGY</center></h2>
                <p className="center" style={{marginTop:'-3%', marginBottom:'-3%'}}>STUDENT RESUME</p>
              </div>

              <div className="row">
                  {this.state.profile.map((content,index)=>{
                    return(
                      <React.Fragment key={index}>
                      <p className="col s10 gen">Name : {content.name} <br/>E-Mail : {content.emailid} <br/>Contact No. : {content.mobileno} <br /> Address : {content.address}</p>
                      </React.Fragment>
                    )
                  })}
                <img src={Image} className="col s2 profile-image" alt=""/>
              </div>

              <hr/>

              <div className="row">
                  <p className="col s12 gen"><b>Objective:</b> <br/> <br/> To obtain employment with a company that offers a positive atmosphere to learn and implement new skills and technologies for the betterment of the organization.</p>
              </div>

              <hr/>

              <div className="row">
                  <p className="gen col s12"><b>Academic Qualification:</b> <br/> <br/>
                  <table>
                      <thead>
                          <tr>
                              <td className="td-ch"><b>Degree/Course</b></td>
                              <td className="td-ch"><b>Institute Name</b></td>
                              <td className="td-ch"><b>Board</b></td>
                              <td className="td-ch"><b>Percentage/CGPA</b></td>
                              <td className="td-ch"><b>Year of Passing</b></td>
                          </tr>
                      </thead>
                      <tbody>
                        {this.state.grad.map((content,index)=>{
                            return(
                              <tr key={index}>
                                  <td className="td-ch">{content.action} ({content.stream})</td>
                                  <td className="td-ch">{content.college}</td>
                                  <td className="td-ch">{content.board}</td>
                                  <td className="td-ch">{content.cgpa_percentage}</td>
                                  <td className="td-ch">{content.end_year}</td>
                              </tr>
                            )
                        })}

                        {this.state.hsc.map((content,index)=>{
                          return(
                            <tr key={index}>
                                <td className="td-ch">Senior Secondary Education</td>
                                <td className="td-ch">{content.college}</td>
                                <td className="td-ch">{content.board}</td>
                                <td className="td-ch">{content.cgpa_percentage}</td>
                                <td className="td-ch">{content.end_year}</td>
                            </tr>
                          )
                        })}


                        {this.state.ssc.map((content,index)=>{
                          return(
                            <tr key={index}>
                                <td className="td-ch">Secondary Education</td>
                                <td className="td-ch">{content.college}</td>
                                <td className="td-ch">{content.board}</td>
                                <td className="td-ch">{content.cgpa_percentage}</td>
                                <td className="td-ch">{content.end_year}</td>
                            </tr>
                          )
                        })}

                      </tbody>
                  </table></p>
              </div>

              <hr/>

              <div className="row">
                  <p className="gen col s12"><b>Skills:</b></p>
                  {this.state.skills.map((content,index)=>{
                    return(
                      <React.Fragment key={index}>
                      <span className="col s12 gen">{index+1} {content.skill_name}</span>
                    </React.Fragment>
                    )
                  })}
              </div>

              <hr/>

              <div className="row">
                  <p className="gen col s12"><b>Internship:</b></p>
                  {this.state.intern.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="gen col s12">{index+1}. <b>{content.profile}</b> <br/>{content.organization}, {content.location} <br/>Duration: {content.start_date} to {content.end_date} <br/></div>
                      </div>
                    )

                  })}
              </div>

              <hr/>

              <div className="row">

                  <p className="gen col s12"><b>Projects:</b></p>

                  {this.state.projects.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="gen col s12">{index+1}. <b>{content.title}</b> <br/>{content.description} <br/>{content.link} <br/>Duration: {content.start_date} to {content.end_date} <br/></div>
                      </div>
                    )
                  })}

              </div>

              <hr/>

              <div className="row">

                  <p className="gen col s12"><b>Position of Responsibility:</b></p>

                  {this.state.pos_res.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="gen col s12">{index+1}. {content.description} <br/>Duration: {content.start_date} to {content.end_date} <br/></div>
                      </div>
                    )
                  })}

              </div>

              <hr/>

              <div className="row">

                  <p className="gen col s12"><b>Trainings or Certificates:</b></p>

                  {this.state.train_cert.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="gen col s12">{index+1}. <b>{content.program}</b> <br/>{content.organization}, {content.location} <br/>{content.description} <br/>Duration: {content.start_date} to {content.end_date} <br/></div>
                      </div>
                    )
                  })}

              </div>

              <hr/>

              <div className="row">

                  <p className="gen col s12"><b>Social Media Accounts:</b></p>

                  {this.state.social.map((content,index)=>{
                    return(
                      <div key={index}>
                        <div className="gen col s12"> {content.blog_link} <br/>{content.github_link} <br/>{content.hackerearth_link} <br/>{content.hackerrank_link} </div>
                      </div>
                    )
                  })}

              </div>

              <hr/>

              <div className="row">
                  <p className="gen col s10"><b>Declaration:</b> <br/> <br/> I hereby declare that above mentioned details are true to my knowledge.</p>
              </div>

              <div className="row">
                  <p className="gen col s10">2nd Feb 2020 <br/><b>Date</b></p>
                  <p className="gen col s2"><b>Signature</b></p>
              </div>




              <div className="row right"> <p className="wat">{this.state.date}</p> </div>
            </section>
          </React.Fragment>
        </PdfContainer>

          </div>
        </div>

      </React.Fragment>
    );

  }
}

export default Resume;
