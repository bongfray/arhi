const passport = require('passport')
const LocalStrategy = require('./studentpass')
const SUser = require('../database/models/STUD/suser')

passport.serializeUser((user, done) => {
	done(null, { _id: user._id })
})


passport.deserializeUser((id, done) => {
	SUser.findOne(
		{ _id: id },
		'username',
		(err, user) => {
			done(null, user)
		}
	)
})

//  Use Strategies
passport.use(LocalStrategy)

module.exports = passport
