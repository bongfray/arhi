import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      fetching:true,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user/profile1del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/ework/user/fetchprofile1',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
       fetching:false,
     products: response.data,

   })
  })
}
  render() {

    const { products} = this.state;
      return(
        <React.Fragment>
        {this.state.fetching === false ?
        <div className="row">
        <div className="col l1 s1 m1 xl1 left proftitle">{this.props.title}</div>
            <div className="col l11 s11 m11 xl11">
              {products.map((product,index) => (
                  <div className="row" key={index}>
                    <div className="col l10 xl10 m10 s10">
                      <div className="row">
                          <div className="col l1 xl1 s1 m1 center hide-on-med-and-down">{index+1}</div>
                          <div className="col l11 s12 m12 xl11">
                             <div className="row" key={product.serial}>
                              {this.props.data.fielddata.map((content,index)=>(
                                <div className=" col l3 s3 xl3 center" key={index}>{product[content.name]}</div>
                              ))}
                              </div>
                          </div>
                        </div>
                      </div>
                      <div className="col l2 xl2 s2 m2">
                          <div className="center">
                            <i className="material-icons small go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                            &nbsp;<i className="material-icons small go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                          </div>
                      </div>
                    </div>

              ))}
            </div>
        </div>
        :
        <div className="center" style={{marginTop:'100px'}}>
            <div className="preloader-wrapper big active">
              <div className="spinner-layer spinner-red">
                  <div className="circle-clipper left">
                   <div className="circle"></div>
                  </div><div className="gap-patch">
                   <div className="circle"></div>
                  </div><div className="circle-clipper right">
                   <div className="circle"></div>
                  </div>
              </div>
            </div>
        </div>
      }
        </React.Fragment>

      )

  }
}
