import React, { Component } from 'react';
import {} from 'materialize-css'
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import Daa from './DAA'
import ContentLoader from "react-content-loader"
import {
  CircularProgressbar,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./AnimatedProgressProvider";



export default class Dash extends Component {

    constructor(props) {
      super(props);
      this.state = {
        loading: true,
        home:'/ework/faculty',
        logout:'/ework/user/logout',
        login:'/ework/flogin',
        get:'/ework/user/',
        nav_route: '/ework/user/fetchnav',
        isChecked:0,
        active:0,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchlogin()
    }
    fetchlogin = () =>{
      axios.get('/ework/user/fetchdayorder')
      .then(response =>{
        this.setState({loading: false})
        if(response.data === "Not"){
          this.setState({
            redirectTo:'/ework/faculty',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'1500', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: e,
      })
      if (this.state.active === e) {
        this.setState({active : null})
      } else {
        this.setState({active : e})
      }
    }

    color =(position) =>{
      if (this.state.active === position) {
          return "col l6 xl6 center go active-pressed";
        }
        return "col l6 xl6 center go";
    }

  render()
  {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="20" y="10" rx="5" ry="5" width="110" height="50" />
        <rect x="140" y="10" rx="3" ry="3" width="110" height="50" />
        <rect x="270" y="10" rx="3" ry="3" width="110" height="50" />

        <rect x="20" y="90" rx="1" ry="1" width="110" height="15" />
        <rect x="140" y="90" rx="1" ry="1" width="110" height="15" />
        <rect x="270" y="90" rx="1" ry="1" width="110" height="15" />


      </ContentLoader>
    )

    if(this.state.loading=== true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav login={this.state.login} home={ this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div className="row">
        <div className="col l5"/>
        <div className="col l2">
           <ul className="row">
             <li style={{  fontSize:'15px'}} onClick={(e)=>{this.handleComp(0)}} className={this.color(0)}>Weekly</li>
             <li style={{  fontSize:'15px'}} onClick={(e)=>{this.handleComp(1)}} className={this.color(1)}>Monthly</li>
           </ul>
        </div>
        <div className="col l5"/>
      </div>
      <Dashh choice={this.state.isChecked}/>
      </React.Fragment>
    )
  }
}
  }
}


class Dashh extends Component {

  constructor(props) {
    super(props);
    this.state = {
      administrative:'',
      academics:'',
      research:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.countPercentage()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice !== this.props.choice)
    {
      this.countPercentage()
    }
  }
  countPercentage =() =>{
    let adroute,acaroute,resroute;
      adroute ='/ework/user/fetchAdmins_Actual';
      acaroute = '/ework/user/fetchAcad_Actual';
      resroute = '/ework/user/fetchRes_Actual'
    this.AdministrativeP(adroute);
    this.ResearchP(resroute);
    this.AcademicP(acaroute);
  }


    AdministrativeP = (adroute) =>{
      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();

      axios.get(adroute)
      .then( response => {


        const reques2 = response.data.completed.filter(item => item.freefield === "Administrative"
        && item.week === week && item.month === month && item.year === year);
        let administrative_week = (reques2.length / response.data.alloted) * 100;

        const reques3 = response.data.completed.filter(item => item.freefield === "Administrative"
        && item.month === month && item.year === year);
        let administrative_month = (reques3.length / (response.data.alloted *4 ) ) * 100;

        if(this.props.choice === 0)
        {
          this.setState({
            administrative:administrative_week,
          })
        }
        else if(this.props.choice === 1){
          this.setState({
            administrative:administrative_month,
          })
        }

      });
    }
    AcademicP = (acaroute) =>{
      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();
      axios.get(acaroute)
      .then( response => {

                const reques2 = response.data.completed.filter(item => item.freefield === "Academic" && item.freeparts==='Curriculum'
                && item.week === week && item.month === month && item.year === year);
                let academics_week = (reques2.length / response.data.alloted) * 100;

                const reques3 = response.data.completed.filter(item => item.freefield === "Academic" && item.freeparts==='Curriculum'
                && item.month === month && item.year === year);
                let academics_month = (reques3.length / (response.data.alloted *4) ) * 100;

                if(this.props.choice === 0)
                {
                  this.setState({
                    academics:academics_week,
                  })
                }
                else{
                  this.setState({
                    academics:academics_month,
                  })
                }
      });
    }
    ResearchP = (resroute) =>{
      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();
      axios.get(resroute)
      .then( response => {
        const reques2 = response.data.completed.filter(item => item.freefield === "Research"
        && item.week === week && item.month === month && item.year === year);
        let research_week = (reques2.length / response.data.alloted) * 100;

        const reques3 = response.data.completed.filter(item => item.freefield === "Research"
        && item.month === month && item.year === year);
        let research_month = (reques3.length / (response.data.alloted *4 )) * 100;

        if(this.props.choice === 0)
        {
          this.setState({
            research:research_week,
          })
        }
        else{
          this.setState({
            research:research_month,
          })
        }
      });
    }

  componentWillUnmount =()=> {
    this._isMounted = false;
  }

render(){

  return (
    <React.Fragment>
    <div className="row">
    <div className="col l4 xl4 s12 m12">
    <Example label="Administrative">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.state.administrative}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    <div className="col l4 xl4 s12 m12">
    <Example label="Academics">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.state.academics}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>

    <div className="col l4 xl4 s12 m12">
    <Example label="Research">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.state.research}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    </div>
    <div>
    {this.props.choice=== 0 && <Daa />}
     <FreeSlots choice={this.props.choice}/>

    </div>
    </React.Fragment>
    );
    }

  }
  function Example(props) {
  return (
  <div className="card hoverable" style={{paddingTop: '10px', paddingBottom:'10px'}}>
     <div className="card-title center #64ffda teal accent-2 black-text">{props.label}</div><br />
      <div style={{marginLeft:'26%',width:'200px'}}>{props.children}</div>
  </div>
  );
  }


  class FreeSlots extends Component {
    constructor() {
      super()
      this.state={
        free_slot_own:'',
        free_slot_other:'',
        absent:'',
      }
      this.componentDidMount =  this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchFree()
    }
    fetchFree = () =>{
      let free_slot_own_route,free_slot_other_route,absent_route;

        // free_slot_own_route ='/ework/user/fetch_own_extra_week';
        free_slot_other_route = '/ework/user/fetch_other_extra';
        absent_route = '/ework/user/fetch_absent'

      // this.FreeSlotOwn(free_slot_own_route);
      this.FreeSlotOther(free_slot_other_route);
      this.Absent(absent_route);
    }

    FreeSlotOwn = (adfroute) =>{
      axios.get(adfroute)
      .then( response => {
        this.setState({
          free_slot_own:response.data.result,
        })
      });
    }
    FreeSlotOther = (free_slot_other_route) =>{
      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();


      axios.get(free_slot_other_route)
      .then( response => {



        const reques2 = response.data.total.filter(item =>
        item.week === week && item.month === month && item.year === year);
        let free_other_total_week = reques2.length;

        const reques3 = response.data.completed.filter(item =>
        item.week === week && item.month === month && item.year === year);
        let free_other_handled_week = reques3.length;

        const reques4 = response.data.total.filter(item =>item.month === month && item.year === year);
        let free_other_total_month = reques4.length;

        const reques5 = response.data.completed.filter(item =>  item.month === month && item.year === year);
        let free_other_handled_month = reques5.length;

        if(this.props.choice === 0)
        {
          this.setState({
            total_other:free_other_total_week,
            free_slot_other:free_other_handled_week,
          })
        }
        else{
          this.setState({
            total_other:free_other_total_month,
            free_slot_other:free_other_handled_month,
          })
        }
      });
    }



    Absent = (absent_route) =>{

      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();


      axios.get(absent_route)
      .then( response => {

        const reques3 = response.data.filter(item =>
        item.week === week && item.month === month && item.year === year);
        let absent_week = reques3.length;

        const reques4 = response.data.filter(item =>item.month === month && item.year === year);
        let absent_month = reques4.length;


        if(this.props.choice === 0)
        {
          this.setState({
           absent:absent_week,
          })
        }
        else{
          this.setState({
            absent:absent_month,
          })
        }
      });
    }

    render()
    {
      return(
        <React.Fragment>
          <div className="row">
             <div className="col l4 xl4 s12 m12">
             <div className="yellow center ">Total Extra Slot Taken(By You)</div>
               <div className="card">
                 <div className="card-content">
                        No of the Extra Slot taken : {this.state.free_slot_own}
                 </div>
               </div>
             </div>

             <div className="col l4 xl4 s12 m12">
             <div className="yellow center ">Total Extra Slot Taken(For other Faculty)</div>
               <div className="card">
                 <div className="card-content">
                   <div className="row">
                      <div className="col l8 xl8 s8 m8">Hanlded Slot -- Total Request - </div>
                      <div className="col l4 xl4 m4 s4">{this.state.free_slot_other} -- {this.state.total_other}</div>
                   </div>
                 </div>
               </div>
             </div>

             <div className="col l4 xl4 s12 m12">
             <div className="yellow center">Total Absent</div>
               <div className="card">
                 <div className="card-content">
                   Total Number of  Absent -  <span className=" red-text"><b>{this.state.absent}</b></span>
                 </div>
               </div>
             </div>

</div>

        </React.Fragment>
      );
    }
  }
