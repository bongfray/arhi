import React, { Component } from 'react';
import axios from 'axios'
import M from 'materialize-css'
import { Redirect } from 'react-router-dom'

export default class Fextra extends Component {
  constructor()
  {
    super()
    this.state={
      foreign_handle:[],
      dayorder:'',
      redirectTo:'',
      modal:false,
      disq:false,
      notfound:'',
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    M.AutoInit()
    this.loggedin()
    this.fetchForegnReq()
    this.fetchdayorder()
  }

  loggedin = ()=>{
    axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/ework/faculty'})
         window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }

  fetchdayorder = ()=>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{
          this.setState({
            dayorder: response.data.day_order,
          });
    });
  }
  fetchForegnReq = () =>{
    axios.post('/ework/user/fetch_foreign_slots')
    .then(res=>{
      if(res.data.length === 0)
      {
        this.setState({notfound:'No Request Found !!'})
      }
      else
      {
      this.setState({foreign_handle:res.data})
      }
    })
  }

  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}

buttonState = (object) => {
this.setState(object)
}

  render() {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return (
      <React.Fragment>
      <h4 className="center">{this.state.notfound}</h4>
      {this.state.foreign_handle.map((content,index)=>(
        <React.Fragment key={index}>
        <div className="row">
        <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%',marginLeft:'10px'}}>
          <div className="row">
             <div className="col l6">
               <p>Request From</p>
               <p>Requested Hour(Today Only)</p>
               <p>Requested Slot(Today Only)</p>
             </div>
             <div className="col l6">
               <p>{content.order_from}</p>
               <p>{content.hour}</p>
               <p>{content.slot}</p>
             </div>
          </div>
          {content.status ===  true ?
                        <React.Fragment>
                        <p className="left yellow-text">Completed !!</p>
                        <button style={{background:'linear-gradient(to bottom, #00ffff 0%, #cc33ff 100%'}} className="disabled right btn-floating btn-large waves-effect waves-light" title="Click when completed"><i className="small material-icons">check</i></button>
                        </React.Fragment>
                        :
                        <React.Fragment>
                        {this.state.disq  && <p className="left yellow-text">Completed !!</p>}
                        <button onClick={ this.selectModal } disabled={this.state.disq} style={{background:'linear-gradient(to bottom, #00ffff 0%, #cc33ff 100%'}} className="right btn-floating btn-large waves-effect waves-light" title="Click when completed"><i className="small material-icons">check</i></button>
                        </React.Fragment>
          }
        </div>
        </div>
        <Moda displayModal={this.state.modal}
           closeModal={this.selectModal}
           content={content}
           buttonState ={this.buttonState}
           dayorder={this.state.dayorder}
           />
        </React.Fragment>
      ))}

      </React.Fragment>
    );
  }
  }
}



class Moda extends Component {
  constructor(props) {
    super(props)
    this.state={
     covered:'',
    }
  }
  coveredTopic =(e)=>{
    this.setState({covered:e.target.value})
  }
  completed =(id,req_from,hour,slot)=>{
    axios.post('/ework/user/completed_foreign_hour',{serial:id,dayorder:this.props.dayorder,req_from:req_from,hour:hour,coveredTopic:this.state.covered,slot:slot})
    .then( res => {
        window.M.toast({html: 'Submitted',outDuration:'1000', classes:'rounded green darken-1'});
        this.setState({covered:''})
        this.props.buttonState({disq:true})
        this.props.closeModal()
    });
  }

  closeModal=(e)=> {
      e.stopPropagation();
      this.props.closeModal()
   }
  render()
  {
    const divStyle = {
         display: this.props.displayModal ? 'block' : 'none',
         top:  0,
         zIndex:'100',
         height: '60vh',
         marginTop:'110px',
    };
    return(
             <div className="modal instructionmodal" style={divStyle}>
             <div style={{marginTop:'10px',marginLeft:'10px',marginRight:'10px'}}>
               <button className="float_cancel btn-floating btn-small waves-effect black  right" onClick={ this.closeModal }><i className="material-icons">cancel</i></button>
               <br />
               <div className="row">
                 <div className="row">
                    <div className="col l6"><b>Submitting Data For SLot</b> <span className="red-text">{this.props.content.slot}</span></div>
                 </div>
                 <div className="row">
                 <div className="input-field col l12">
                    <input id="topics" type="text" value={this.state.covered} onChange={this.coveredTopic} className="validate" />
                    <label htmlFor="topics">Enter the topics you have covered</label>
                 </div>
                  <button href="#modal1" className="btn right" onClick={() => this.completed(this.props.content.serial,this.props.content.order_from,this.props.content.hour,this.props.content.slot)}>Submit</button>
                 </div>
                 </div>
                 </div>
             </div>
    )
  }
}
