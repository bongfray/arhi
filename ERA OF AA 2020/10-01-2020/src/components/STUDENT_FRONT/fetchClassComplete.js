import React, { Component } from 'react';
import axios from 'axios';

export default class ClassComplete extends Component {
  constructor(){
    super()
    this.state={
      disabled:'',
      disp:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleAction =(action,day,month,year,faculty_id,serial,day_slot_time,slot,week)=>{
    window.M.toast({html: 'Updating...',classes:'rounded #ec407a pink lighten-1'});

    this.setState({disabled:'disabled'})

    axios.post('/ework/user2/updateValue',{action:action,week:week,day_slot_time:day_slot_time,slot:slot,day:day,month:month,year:year,faculty_id:faculty_id,serial:serial})
    .then( res => {
      if(res.data === 'deactivated')
      {
        window.M.toast({html: 'Updated !!',classes:'rounded green darken-2'});
      }
        else if(res.data.updateStatus === 'done')
        {
          this.sendStatus(action,day,month,year,faculty_id,day_slot_time,slot,week,res.data.total_student.length,serial);
        }

    });
  }

  sendStatus=(action,day,month,year,faculty_id,day_slot_time,slot,week,total_student,serial)=>{
    axios.post('/ework/user/sendStatus',{total_student:total_student,week:week,day_slot_time:day_slot_time,slot:slot,day:day,month:month,year:year,faculty_id:faculty_id,action:action})
    .then( res => {
      if(res.data)
      {
        this.props.complete_class.filter(item => item.serial!== serial);
        this.setState({disabled:''});
        window.M.toast({html: 'Updated !!',classes:'rounded green darken-2'});
      }
    });
  }

  showInfo=()=>{
    this.setState({
      disp:!this.state.disp,
    })
  }

  render() {
    var tasks = this.props.complete_class.filter(item =>{
      return this.props.facultylist.find(citem => (citem.faculty_id === item.faculty_id) && (item.new=== true));
    });
    return (
      <div className="notinoti">
         <h5 className="center">Class Completion History</h5>
         {this.state.disp === false && <div title="Instructions" onClick={this.showInfo} className="go" style={{marginTop:'10px',boxShadow: '0px 10px 10px 2px pink',width:'30px',borderRadius: '0px 20px 20px 0px'}}>
           <i className="material-icons small red-text">chevron_right</i>
         </div>
         }
         {this.state.disp &&
          <div className="cover_all" onClick={this.showInfo}>
            <div className="up">
            <div style={{padding:'20px'}}>
            <h5 className="center red-text">Instructions</h5><br />
              In this page you need to enter your opinion about the listed datas. Those datas are about
              the completed classes. There are three option like AGREE, DISAGREE, NO CLASS. NO CLASS signifies
              two thing whether you don't have that class today or you are absent. Remember untill you are
              not submitting all the datas,you can't move forward to any page.<br /><span className="red-text">
               Please refresh the page,once you submit all the datas.</span>
            <hr />
            <div style={{padding:'10px'}} onClick={this.showInfo} className="go right">CLOSE</div>
            </div>
            </div>
         </div>}
         <br />
         <div className="row">
            <div className="col l2 xl2 center"><b>Faculty Id</b></div>
            <div className="col l1 xl1 center"><b>DayOrder.Hour</b></div>
            <div className="col l1 xl1 center"><b>Slot</b></div>
            <div className="col l4 xl4 center"><b>Covered Topic</b></div>
            <div className="col l4 xl4 center"><b>Action</b></div>
         </div>
         <hr />
         {this.props.complete_class.length===0 ?
           <h5 className="center">No Data Found</h5>
           :
         <div>
           {tasks.map((content,index)=>(
             <React.Fragment key={index}>
              <div className="row">
                <div className="col l2 xl2 center">
                   {content.faculty_id}
                </div>
                <div className="col l1 xl1 center">
                   {content.day_slot_time}
                </div>
                <div className="col l1 xl1 center">
                   {content.slot}
                </div>
                <div className="col l4 xl4 center">
                    {content.covered_topic}
                </div>
                <div className="col l4 xl4 center">
                  <div className="row">
                      <div className="col l4 xl4">
                          <p>
                            <label>
                              <input className="with-gap" disabled={this.state.disabled} name="group1" type="radio" onClick={()=>this.handleAction('agree',content.day,
                            content.month,content.year,content.faculty_id,content.serial,content.day_slot_time,content.slot,content.week)} />
                              <span className="green-text">AGREE</span>
                            </label>
                          </p>
                        </div>
                         <div className="col l4 xl4">
                           <p>
                             <label>
                               <input className="with-gap" disabled={this.state.disabled} name="group2" type="radio" onClick={()=>this.handleAction('disagree',content.day,
                             content.month,content.year,content.faculty_id,content.serial,content.day_slot_time,content.slot,content.week)} />
                               <span className="red-text">DISAGREE</span>
                             </label>
                           </p>
                          </div>
                          <div className="col l4 xl4">
                            <p>
                              <label>
                                <input className="with-gap" disabled={this.state.disabled} name="group3" type="radio" onClick={()=>this.handleAction('no class',content.day,
                              content.month,content.year,content.faculty_id,content.serial,content.day_slot_time,content.slot,content.week)} />
                                <span className="red-text">NO CLASS</span>
                              </label>
                            </p>
                           </div>
                      </div>
                </div>
              </div>
              <hr />
             </React.Fragment>
           ))}
           </div>
         }

      </div>
    );
  }
}
