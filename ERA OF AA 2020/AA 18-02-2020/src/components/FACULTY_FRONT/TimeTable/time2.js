import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios'
import ColorRep from './colordiv.js'
import Insturction from './instructionmodal'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
require("datejs")

/*-------------------------------------------------Clock for Current Date and time------------------- */

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
        <span>{new Date().toDateString()}</span><span> ({this.state.time})</span>
      </div>
    );
  }
}



/*-----------------------------------------Automatically Chanege the day order-------------------------------*/


class DayOrder extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{
  constructor() {
    super();
    this.state = {
      loading:true,
      opendir: '',
      modal: false,
      display:'block',
      saved_dayorder:'',
      redirectTo:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      noti_route:true,
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  getCount =() =>{
    axios.get('/ework/user/knowcount', {
    })
    .then(response =>{
      this.setState({loading:false})
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  getDayOrder = () =>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{
      if(response.data === "Not")
      {
        this.setState({
          redirectTo:'/ework/',
        });
        window.M.toast({html: 'You are not Logged In', classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
        this.getCount();
        if(response.data.day_order===0)
        {
          this.setState({saved_dayorder:'0',display:'none'})
        }
        else
        {
          this.setState({
            saved_dayorder: response.data.day_order.toString(),
          });
        }
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
    this.getDayOrder();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };

      render(){
        const MyLoader = () => (
          <ContentLoader
            height={160}
            width={400}
            speed={2}
            primaryColor="#f3f3f3"
            secondaryColor="#c0c0c0"
          >
            <rect x="10" y="8" rx="5" ry="5" width="80" height="30" />
            <rect x="140" y="30" rx="3" ry="3" width="250" height="120" />

            <rect x="10" y="70" rx="5" ry="5" width="80" height="30" />
            <rect x="10" y="120" rx="1" ry="1" width="80" height="15" />


          </ContentLoader>
        )
        if(this.state.loading === true)
        {
          return(
            <MyLoader />
          );
        }
        else{
        if (this.state.redirectTo) {
             return <Redirect to={{ pathname: this.state.redirectTo }} />
         } else {

        return(
          <React.Fragment>
          <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <Insturction
              displayModal={this.state.modal}
              closeModal={this.selectModal}
          />
          <div style={{padding:'10px'}}>
              <div className="row">
                  <div className="col l4 s4 xl4 m2">
                    <Clock />
                    <br />
                    <Link to="/ework/time_yes" style={{color: 'black'}} >Go to Yesterday's Dayorder</Link>
                  </div>
                 <div className="col l4 s4 xl4 m10">
                     <div className="center blue-text" style={{fontSize:'20px'}}>Today's DayOrder</div>
                 </div>
                 <div className="col l4 xl4 s4">
                     {this.state.modal=== false &&
                       <div onClick={this.selectModal} className="right animat bounce" style={{margin:'10px 20px 0px 0px',cursor: 'pointer'}}>
                         <i className="material-icons small red-text">info_outline</i>
                       </div>
                     }
                 </div>
              </div>

              <div className="row">
                  <div className="col l2 s12 xl2 m12">
                      <DayOrder day_order={this.state.saved_dayorder}/>
                    <br />
                    <div style={{display:this.state.display}}>
                         <FormControl style={{width:'100%'}}>
                           <InputLabel id="sel_type">Select Here</InputLabel>
                             <Select
                               labelId="sel_type"
                               id="sel_type"
                               value={this.state.opendir}
                               onChange={this.handledir}
                             >
                             <MenuItem value="r_class">Regular Class</MenuItem>
                             <MenuItem value="own_ab">Absent</MenuItem>
                             </Select>
                          </FormControl>
                    </div>
                 </div>
                <div className="col l10 s12 m12 xl10">
                  <Content opendir={this.state.opendir}
                  day_order={this.state.saved_dayorder}/>
                </div>
            </div>
          </div>
          </React.Fragment>
        );
      }
    }
      }
    }




/*-----------------------------------------------This class is for dynamically allocating div ------------- */

class Content extends Component{
  constructor(){
    super();
    this.state={
      redirectTo:'',
      username:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.getUser();
  }
  getUser =()=>{
    axios.get('/ework/user/')
    .then( res => {
        if(res.data.user){
          this.setState({username:res.data.user.username})
        }
    });
  }
  render(){
    var date = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    if (this.state.redirectTo)
    {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else{
          return(
            <React.Fragment>
             <ColorRep opendir={this.props.opendir}
              username={this.state.username}
              date={date} month={month} year={year} day_order={this.props.day_order}/>
            </React.Fragment>
          );
      }
  }
}
