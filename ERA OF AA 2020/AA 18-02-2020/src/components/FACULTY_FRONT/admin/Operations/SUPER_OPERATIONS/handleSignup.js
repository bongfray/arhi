import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'


export default class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,username,name,mailid)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })
      window.M.toast({html: 'Sending Activation Mail...',classes:'rounded orange'});
      axios.post('/ework/user/active_user',{
        username: username,
        name:name,
        mailid:mailid,
      })
      .then(res=>{
            window.M.toast({html: 'Activated !!', classes:'rounded green darken-2'});
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_signup_request_for_admin';
      axios.get(route)
      .then(res=>{
          this.setState({display:'block',requests:res.data})
      })
    }
    else if(this.props.choice === 'student')
    {
      axios.post('/ework/user2/fetch_signup_request_for_admin',{user:''})
      .then(res=>{
          this.setState({display:'block',requests:res.data})
      })
    }

  }
  render()
  {
    return(
      <React.Fragment>
            {this.props.choice &&
      <div className="row">
         <div className="col l12 xl12">
            <div className="card">
              <div className="card-title center pink white-text">Accept Request</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.requests.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                        <div className="col l2 center"><b>Name</b></div>
                        <div className="col l1 center"><b>Official Id</b></div>
                        <div className="col l2 center"><b>Mail Id</b></div>
                        <div className="col l2 center"><b>Campus</b></div>
                        <div className="col l2 center"><b>Department</b></div>
                        <div className="col l2 center"><b>Designation</b></div>
                        <div className="col l1 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.requests.map((content,index)=>(
                           <div className="row"  key={index}>
                            <div className="col l2 center">{content.name}</div>
                            <div className="col l1 center" style={{wordWrap: 'break-word'}}>{content.username}</div>
                            <div className="col l2 center">{content.mailid}</div>
                            <div className="col l2 center">{content.campus}</div>
                            <div className="col l2 center">{content.dept}</div>
                            <div className="col l2 center">{content.desgn}</div>
                            <div className="col l1 center">
                                <div className="switch">
                                  <label>
                                    <input  checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content.username,content.name,content.mailid)}} type="checkbox" />
                                    <span className="lever"></span>
                                  </label>
                                </div>
                            </div>
                          </div>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
       }
      </React.Fragment>
    ) ;
  }
}
