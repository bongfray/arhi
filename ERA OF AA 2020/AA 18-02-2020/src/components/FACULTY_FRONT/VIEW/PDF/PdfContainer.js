import React,{ } from 'react';

export default (props) => {
  const bodyRef = React.createRef();
 /*if(props.loading === false){
   useEffect(() =>
      createPdf(), [])
  }*/
  const createPdf = () => props.createPdf(bodyRef.current);
  return (
    <section className="pdf-container">
      <section className="pdf-toolbar"><center>
        <button onClick={createPdf}>Download PDF</button></center>
      </section>
      <section className="pdf-body" ref={bodyRef}>
        {props.children}
      </section>
    </section>
  )
}
