import React, { Component } from 'react';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import ViewStudentData from '../../FACULTY ADVISER/view_single_student'




export default class StudentData extends Component {
  constructor()
  {
    super()
    this.state={
      red_id:'',
      details_view:false,
      data:'',
      loading:false,
    }
  }

  closeView=()=>{
    this.setState({details_view:false})
  }

  fetchUser=()=>
  {
    if(!this.state.reg_id)
    {
      window.M.toast({html: 'Enter the id !!',classes:'rounded red'});
    }
    else {
      this.setState({loading:true})
      axios.post('/ework/user2/check_student',{regid:this.state.reg_id.toUpperCase()})
      .then( res => {
          this.setState({loading:false})
          if(res.data === 'no')
          {
            window.M.toast({html: 'User Not Found !!',classes:'rounded red'});
          }
          else
          {
            this.setState({data:res.data,loading:false,details_view:true})
          }
      });
    }
  }

setRegId=(e)=>{
  this.setState({reg_id:e.target.value})
}

  render() {
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      )
    }
    else {
    return (
      <React.Fragment>
        <Grid container spacing={1}>
           <Grid item xs={4} sm={4}/>
           <Grid item xs={4} sm={4}>
             <Paper elevation={3} style={{minHeight:'120px',padding:'10px'}}>
               <TextField
                 required
                 id="filled-required"
                 label="Enter Registration id "
                 value={this.state.reg_id}
                 onChange={this.setRegId}
                 variant="filled"
                 fullWidth
                 multiline
               />
                <Button style={{float:'right',marginTop:'5px'}} onClick={this.fetchUser} variant="contained" color="secondary">View History</Button>
               </Paper>
           </Grid>
           <Grid item xs={4} sm={4}/>
        </Grid>
        <br />
        {this.state.data &&
           <ViewStudentData details_view={this.state.details_view}  closeView={this.closeView} data={this.state} />
        }
      </React.Fragment>
    );
   }
  }
}
