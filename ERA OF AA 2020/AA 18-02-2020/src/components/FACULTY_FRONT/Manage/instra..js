
import React from 'react'
const Insta = props => {

     const divStyle = {
          display: props.displayModal ? 'block' : 'none'
     };
     function closeModal(e) {
        e.stopPropagation()
        props.closeModal()
     }
     return (
       <div className="cover" style={divStyle} onClick={ closeModal }>
       <div
         className="up">
         <div className="modal-content" style={{padding:'20px'}}>
           <h4 className="center">Instructions</h4>
           <p>
           1. Kindly read the Insturctions first.<br />
           2. This is the page where you can upload you missed dayorder on a particluar slot because of some reason.<br />
           3. You have to make a request to upload your missing data with valid reason.<br />
           4. Remember request with a invalid date and dayorder will simply not let you to submit the data.<br />
           5. Most importantly you are allowed to upload this data within a particular span of time.<br />
           6. Once CARE will accept your request after veryfing all the datas you can upload the data with a span of time,which will be initiated to you.<br />
           7. Incase if you can't able to upload this data within the time, at that case your datas will be blank on that particular day.<br />
           8. By the way this page is only for uploading missed dayorder or slot,not for any updation of your previous dayorder histories.<br />
           9. After your request if it is showing pending for a long time(after 1 week) kindly contact CARE.<br />
           10. Request for a single slot entry, is strictly non-editable,once you submit the data.<br />
           11. Where as a whole dayorder request, will give you a chance to edit the datas after the submission also.<br />
           12. For anytype of help contact SRM CARE.<br />
           13. <span className="red-text">We are recommending you to browse this page in destop or tab mode.</span>
           </p>
         </div>
         <div className="modal-footer">
         <span style={{margin:'0px 5px 15px 0px'}} className="right styled-btn pink-text" onClick={ closeModal }>GOT IT</span>
         </div>
       </div>
       </div>
     );
}
export default Insta;
