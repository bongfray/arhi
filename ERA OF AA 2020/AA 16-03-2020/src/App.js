import React, { Component } from 'react';
import { Route, Switch} from 'react-router-dom'

import { MuiThemeProvider, createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';





import ReportB from './bud-error'
import NotFound from './components/ERROR/Not-Found'
import FiveYear from './components/FACULTY_FRONT/FIVEYEAR/fiveyear'
import Section from './components/FACULTY_FRONT/Section/Gg'
import Initial from './Navigator'
import FSignup from './components/FACULTY_FRONT/sign-up'
import SSignup from './components/STUDENT_FRONT/ssignup'
import FLoginForm from './components/FACULTY_FRONT/login-form'
import SLoginForm from './components/STUDENT_FRONT/slogin'
import FacultyHome from './components/FACULTY_FRONT/Home'
import StudentHome from './components/STUDENT_FRONT/Home_stud'
import Dash from './components/FACULTY_FRONT/DashBoard/Dashboard'
import Faculty_Profile from './components/FACULTY_FRONT/Profile/exdash'
import Sprofile from './components/STUDENT_FRONT/studentProfile'
import OFFLINE from './components/online-offline/Connection'
import Scroll from './components/Top/scroll_top'
import YesterdayTimeTable from './components/FACULTY_FRONT/TimeTable/yesterday_slot'
import TodayTimeTable from './components/FACULTY_FRONT/TimeTable/time2'
import Manage from './components/FACULTY_FRONT/Manage/manage'
import FProfileData from './components/FACULTY_FRONT/Profile/stepper'
import Master from './components/FACULTY_FRONT/master/mater'
import BluePrint from './components/FACULTY_FRONT/Schedule/Timetable'
import ResetPass from './components/Reset/reset_password'
import AdminAuth from './components/FACULTY_FRONT/admin/auth'
import Panel from './components/FACULTY_FRONT/admin/admin_operations'
import ViewSelect from './components/FACULTY_FRONT/VIEW/view_select'
import Extra from './components/FACULTY_FRONT/EXTRA/own_extra'
import DeptAdmin from './components/FACULTY_FRONT/admin/deptadmin'
import DeptAdmin_Panel from './components/FACULTY_FRONT/admin/department_admin_panel'
import Suprimity from './components/FACULTY_FRONT/suprimity'
import FacultyAdviser from './components/FACULTY_FRONT/FACULTY ADVISER/faculty_adviser'

/*------------------Student Part------------------*/
import Resume from './components/STUDENT_FRONT/resume'
import TaskManager from './components/STUDENT_FRONT/ARHI/taskManage'
import Arhi from './components/STUDENT_FRONT/ARHI/arhiStudent'
import TimeTableS from './components/STUDENT_FRONT/Timetable/slotdetails'
import ManageS from './components/STUDENT_FRONT/MANAGE/manageS';

import Brightness3Icon from '@material-ui/icons/Brightness3';
import Brightness5Icon from '@material-ui/icons/Brightness5';
import pink from '@material-ui/core/colors/pink';
import CssBaseline from '@material-ui/core/CssBaseline';

class App extends Component {
  constructor() {
    super()
    this.state = {
      themeType:'light',
      offline_view:false,
    }
  }

changeTheme=()=>{
  if(this.state.themeType ==='dark')
  {
    this.setState({themeType:'light'})
  }
  else {
    this.setState({themeType:'dark'})
  }
}

setGoOffline=()=>{
  this.setState({offline_view:true})
}


  render() {
    let THEME = createMuiTheme({
      palette:{
        primary:{
          light: "#7986cb",
          main: "#3f51b5",
          dark: "#303f9f"
        },
        secondary:{
          light:pink[500],
          main:pink[500],
          dark:pink[500],
        },
        type:this.state.themeType
      },
   typography: {
    "fontFamily": "Play",
   }
});
THEME = responsiveFontSizes(THEME);

    return (
      <MuiThemeProvider theme={THEME}>
      <CssBaseline />
      <React.Fragment>
        {(!this.state.offline_view) &&
          <OFFLINE go_offline={this.setGoOffline} />
        }
       <div className="App">
      <Switch>
        <Route exact path="/" render={() =>
          <Initial
         />}
       />

        <Route path="/ework/faculty" render={() =>
          <FacultyHome
         />}
       />

       <Route exact path="/ework/student" render={() =>
         <StudentHome
        />}
      />

        <Route path="/ework/flogin" render={() =>
          <FLoginForm
         />}
       />

       <Route path="/ework/slogin" render={() =>
         <SLoginForm
        />}
      />
       <Route path="/ework/fsignup" component={FSignup} />
       <Route path="/ework/ssignup" component={SSignup} />

       <Route path="/ework/fprofile" component={Faculty_Profile} />
       <Route path="/ework/sprofile" component={Sprofile} />


       <Route path="/ework/partA" render={() =>
         <Section
        />} />


       <Route path="/ework/fprofdata" render={() =>
         <FProfileData
        />} />

       <Route path="/ework/dash" render={() =>
         <Dash
        />} />
        <Route path="/ework/master" component={Master} />
        <Route path="/ework/view" component={ViewSelect} />
         <Route path="/ework/blue_print" component={BluePrint} />
         <Route path="/ework/reset_password/:token" component={ResetPass} />


          <Route path="/ework/time_new" render={() =>
            <TodayTimeTable
           />} />
           <Route path="/ework/time_yes" render={() =>
             <YesterdayTimeTable
            />} />

            <Route path="/ework/admin_panel" render={() =>
              <Panel
             />} />

            <Route path="/ework/admin" component={AdminAuth} />

            <Route path="/ework/manage" component={Manage} />
              <Route path="/ework/five" component={FiveYear} />
              <Route path="/ework/extra" component={Extra} />
              <Route path="/ework/suprimity_admin" component={Suprimity} />
              <Route path="/ework/faculty_adviser" component={FacultyAdviser} />




              <Route path="/ework/department_admin" component={DeptAdmin} />
              <Route path="/ework/dadmin_panel" component={DeptAdmin_Panel} />

             <Route path="/ework/resume" component={Resume} />
             <Route path="/ework/task_student" component={TaskManager} />
             <Route path="/ework/arhi_student" component={Arhi} />
             <Route path="/ework/timetables" component={TimeTableS} />
             <Route path="/ework/manageS" component={ManageS} />
             <Route path="*" component={NotFound}/>
             </Switch>
         <Scroll/>
         <ReportB />
         <div style={{position:'fixed',bottom: 0,left: 0,padding:'10px'}} onClick={this.changeTheme} >
            {this.state.themeType ==='dark' ? <Brightness3Icon /> : <Brightness5Icon /> }
         </div>
      </div>


      </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

export default App;
