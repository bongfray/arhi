import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormGroup from '@material-ui/core/FormGroup';



export default class Modal extends Component {
  constructor() {
      super()
      this.state = {
          mailid: '',
          official_id:'',
          redirectTo: null,
          sending: false,
          forgot_action:false,
      }
  }
  handleForgo=(e)=> {
      this.setState({
          [e.target.name]: e.target.value
      })
  }
  handleSubmit=(event)=> {
      event.preventDefault()
      if(!(this.state.mailid) || !(this.state.official_id)){
        window.M.toast({html: 'Enter the Details!!',classes:'rounded #f44336 red'});
        return false;
      }
      else{
        this.setState({
          sending:true,
        })
      axios.post(this.props.forgot, {
              mailid: this.state.mailid,
              username:this.state.official_id,
          }
        )
          .then(response => {
              if (response.status === 200)
              {
                      if(response.data.success)
                        {
                          this.setState({
                           loading: '',
                           redirectTo: '/ework'
                          })
                         window.M.toast({html: 'Check your mail to reset password !!', classes:'rounded #ffeb3b yellow black-text text-darken-2'});
                        }
                      else if(response.data.nodata)
                        {
                          window.M.toast({html:response.data.nodata, classes:'rounded red'});
                        }
              }
              this.setState({
                sending:false,
              })

          }).catch(error => {
            this.setState({
              sending:false,
            })
            window.M.toast({html: 'Error !!!'});
          })
        }
  }
    render()
    {
      if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
        return(
          <div>
          <Grid container spacing={1}>
             <Grid item xs={3}/>
             <Grid item xs={6}>
                <div style={{textAlign:'center',color:'red'}} className="go" onClick={()=>this.setState({forgot_action:true})}>Forgot password?</div>
             </Grid>
             <Grid item xs={3}/>
          </Grid>
          <Dialog open={this.state.forgot_action} onClose={()=>this.setState({forgot_action:false})} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title" align="center">Forgot Password</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Please enter the details :
              </DialogContentText>
                  <FormGroup row>
                      <TextField
                        autoFocus
                        margin="dense"
                        id="regid"
                        variant="filled"
                        multiline
                        label="Your Official Id or Registration Number"
                        name="official_id" value={this.state.official_id} onChange={this.handleForgo}
                        fullWidth
                      />
                      <TextField
                        margin="dense"
                        id="mailid"
                        label="Email Address"
                        type="email"
                        variant="filled"
                        multiline
                        name="mailid" value={this.state.mailid} onChange={this.handleForgo}
                        fullWidth
                      />
                  </FormGroup>
            </DialogContent>
            <DialogActions>
              <Button onClick={()=>this.setState({forgot_action:false})} color="primary">
                Cancel
              </Button>
              <Button disabled={this.state.sending} onClick={this.handleSubmit} color="primary">
                Send Mail
              </Button>
            </DialogActions>
          </Dialog>
          </div>

        )
    }

    }
}
