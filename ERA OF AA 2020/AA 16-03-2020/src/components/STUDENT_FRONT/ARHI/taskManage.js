import React, { Component } from 'react';
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import { Redirect } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {Button,Paper,Typography,Card,CardContent,Checkbox,Hidden,CircularProgress} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class TaskManage extends Component {
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      noti_route:false,
      skills:[],
      div_no:'',
      skill_name:'',
      loader:true,
      redirectTo:'',
      render_detail:false,
      username:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    if(this.props.prop_data)
    {
      this.setState({username:this.props.prop_data.data.username,loading:false})
      this.fetchSkills(this.props.prop_data.data.username);
    }
    else
    {
         this.getUser();
    }
  }
  getUser=()=> {
    axios.get('/ework/user2/getstudent').then(response=>{
      if (response.data.user){
        this.setState({username:response.data.user.username})
         this.fetchSkills(response.data.user.username)
      }
      else
      {
        this.setState({loader:false,redirectTo:'/ework/student'});
      }
        })
      }


  fetchSkills =(username)=>{
    axios.post('/ework/user2/fetchall',{action:'Skills',username:username})
    .then( res => {
        if(res.data)
        {
          this.setState({skills:res.data,loader:false})
        }
    });
  }

setDivOn =(div)=>{
  this.setState({div_no:div})
}

showDetails =(skill_name)=>{
  this.setState({
    render_detail:!this.state.render_detail,
    skill_name:skill_name,
  })
}

  render(){
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="5" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="73" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="140" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="210" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="280" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="350" y="35" rx="0" ry="0" width="45" height="18" />

        <rect x="5" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="73" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="140" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="210" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="280" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="350" y="75" rx="0" ry="0" width="45" height="18" />

      </ContentLoader>
      )
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else
     {
       if(this.state.loader)
       {
         return(
           <MyLoader />
         );
       }
       else {
          return(
            <React.Fragment>
            {!(this.props.prop_data) && <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
            }
            <br />
                {this.state.render_detail &&
                  <ShowDetails  username={this.state.username} prop_data={this.props.prop_data}
                  render_detail={this.state.render_detail} detail={this.showDetails} skill_name={this.state.skill_name} />
                }
                <Typography variant="h4" align="center">Task Manager</Typography>
                <br />
                <Grid container spacing={1} style={{padding:'5px'}}>
                  {this.state.skills.map((content,index)=>(
                    <Grid item xs={6}  sm={2} key={index}>
                    <Paper elevation={3} style={{padding:'10px'}}  onClick={()=>{this.showDetails(content.skill_name)}} >
                      <Typography align="center" variant="h6">{content.skill_name}
                      </Typography>
                    </Paper>
                    </Grid>
                  ))}
                </Grid>
            </React.Fragment>
          );
        }
   }
  }
}

class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_completed:'',
      total_request:'',
      message:'',
      mailid:'',
      isChecked:'Begineer',
      active:'Begineer',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    const divStyle={
      backgroundColor:'#ff4081',
      color:'white',
      textAlign:'center',padding:'3px',borderRadius:'20px',
    }
    const divS={
      textAlign:'center',padding:'3px',borderRadius:'20px',
    }
    if (this.state.active === position) {
        return divStyle;
      }
      return divS;
  }

  totalSet =(object)=>{
    this.setState(object);
  }

  render() {
    return (
      <Dialog fullScreen open={this.props.render_detail} onClose={this.props.detail} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2} sm={2}>
                  <IconButton edge="start" color="inherit" onClick={this.props.detail} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8} sm={8}>
                 <div style={{textAlign:'center',fontSize:'25px'}}>Complete Details of  <span style={{color:'yellow'}}>{this.props.skill_name}</span></div>
               </Grid>
               <Grid item xs={2} sm={2}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List style={{marginTop:'70px'}}>

          <Grid container>
              <Grid item xs={12} sm={3}>
                 <Paper style={{padding:'6px',boxShadow:'0px 0px 4px 0px rgba(186, 186, 186, 0.75)',borderRadius:'5px'}}>
                   <Grid container spacing={1}>
                      <Grid item xs={4} sm={4}>
                         <Paper className="go" style={this.color('Begineer')}  onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</Paper>
                      </Grid>
                      <Grid item xs={4} sm={4}>
                         <Paper className="go" style={this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</Paper>
                      </Grid>
                      <Grid item xs={4} sm={4}>
                           <Paper className="go" style={this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</Paper>
                      </Grid>
                   </Grid>
                 </Paper>
              </Grid>
              <Hidden xsDown><Grid item sm={5}/></Hidden>
              <Grid container spacing={1} item xs={12} sm={4}>
                    <Grid item xs={6} sm={6}>
                        <Card >
                        <CardContent>
                        <Typography align="center" variant="h6">
                          PENDING TASK
                        </Typography><br />
                          <Typography align="center" style={{fontSize:'20px',color:'#f50057'}}>
                            {this.state.total_request}
                          </Typography>
                        </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={6} sm={6}>
                        <Card >
                          <CardContent>
                          <Typography align="center" variant="h6">
                            COMPLETED
                          </Typography>
                          <br />
                            <Typography align="center" style={{fontSize:'20px',color:'#4caf50'}}>
                              {this.state.total_completed}
                            </Typography>
                          </CardContent>
                        </Card>
                    </Grid>
              </Grid>
          </Grid>


              <br />

              <TaskHistory  prop_data={this.props.prop_data}
              level={this.state.isChecked} username={this.props.username}
              skill_name={this.props.skill_name} totalSet={this.totalSet} />
      </List>
    </Dialog>
    );
  }
}




class TaskHistory extends Component {
  constructor()
  {
    super()
    this.state={
      tasks:[],
      completed_tasks:[],
      loading:true,
      display_on:false,
      delete_key:0,
      confirm_dialog:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTasksFromAdmin()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.level !== this.props.level) {
      this.fetchTasksFromAdmin();
    }
  }

  fetchTasksFromAdmin =()=>{
    axios.post('/ework/user2/fetch_in_admin',
    {usertype:this.props.level,action:'Task '+this.props.skill_name})
    .then( res => {
        if(res.data)
        {
          this.fetchCompletedHistory(res.data)
        }
    });
  }
  fetchCompletedHistory = (adminTasks) =>{
    axios.post('/ework/user2/fetchTasks',{level:this.props.level,action:'Task '+this.props.skill_name,
    username:this.props.username})
    .then( res => {
        if(res.data)
        {

          const tasks = adminTasks.filter(item =>{
            return !res.data.find(citem => citem.referTask === item.serial);
          });

          this.setState({
            tasks:tasks,
            completed_tasks:res.data,
            loading:false,
          })
          this.props.totalSet({
            total_request:tasks.length,
            total_completed:res.data.length,
          })
        }
    });
  }

  callEdit =(key) =>{
    const referTaskDetails = this.state.completed_tasks.filter(item => item.referTask=== key);
    this.setState({
      display_on:!this.state.display_on,
      referTaskDetails:referTaskDetails,
    })
  }

  confirmDelete=(key)=>{
    this.setState({confirm_dialog:!this.state.confirm_dialog,delete_key:key})
  }

  deleteTask=()=>{
    window.M.toast({html: 'Deleting !!', classes:'rounded orange'});
    const { completed_tasks } = this.state;
    axios.post('/ework/user2/deleteTask',{key:this.state.delete_key,task_name:this.props.skill_name})
    .then(res=>{
      if(res.data){
        this.setState({
            completed_tasks: completed_tasks.filter(items => items.referTask !== this.state.delete_key)
        })
        this.confirmDelete();
        window.M.toast({html: 'Deleted !!', classes:'rounded red'});
        this.fetchTasksFromAdmin();
      }
    })
  }

  handleExpand=()=>{
    this.setState({isExpanded:!this.state.isExpanded})
  }

  render() {
    if(this.state.loading)
    {
      return(
        <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else
    {
    return (
      <React.Fragment>

          <Dialog
            open={this.state.confirm_dialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title" style={{textAlign:'center'}}>Confirmation of Deletion</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                  Deleting this data may cause your rating to be decreased in this particular skill. So take decision
                  accordingly.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.confirmDelete} color="primary">
                Disagree
              </Button>
              <Button onClick={this.deleteTask} color="primary" autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>

      {this.state.display_on &&
        <FloatingWindow display_on={this.state.display_on} fetchTasksFromAdmin={this.fetchTasksFromAdmin}
        callEdit={this.callEdit} referTaskDetails={this.state.referTaskDetails}
        level={this.props.level} skill_name={this.props.skill_name} username={this.props.username}
        />
      }
      {this.props.level &&
        <React.Fragment>
          {this.state.tasks.length>0 ?

            <TableContainer component={Paper} style={{margin:'10px'}}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center"><b>Task-No</b></TableCell>
                    <TableCell align="center"><b>Description of the Task</b></TableCell>
                    <TableCell align="center"><b>Link to Refer</b></TableCell>
                    <TableCell align="center"><b>Status</b></TableCell>
                  </TableRow>
                </TableHead>
                {this.state.fetching ?
                  <div style={{textAlign:'center'}}>
                    Fetching Data .....
                  </div>
                  :
                <TableBody>
                    {this.state.tasks.map((content,index) => (
                      <TableRow key={index}>
                        <TableCell align="center">T{content.serial}</TableCell>
                        <TableCell align="center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.description_of_the_task}</TableCell>
                        <TableCell align="center">
                           <a  rel="noopener noreferrer" href={content.ref_for_task} target="_blank">LINK</a>
                        </TableCell>
                        <TableCell align="center">
                          {!(this.props.prop_data) &&
                            <Complete fetchTasksFromAdmin={this.fetchTasksFromAdmin}
                            level={this.props.level} skill_name={this.props.skill_name}
                            serial={content.serial} username={this.props.username} />
                          }
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
                }
              </Table>
            </TableContainer>
        :
        <Typography variant="h5" align="center">No Pending Task Found !!</Typography>
      }
<br />
      <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand}>
        <ExpansionPanelSummary
        style={{borderRadius:'20px'}}
          expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
          className="mine-collap"
        >
          <div style={{color:'yellow',fontSize:'25px'}}>COMPLETED TASKS</div>
                </ExpansionPanelSummary>


                {this.state.completed_tasks.length>0 ?

                  <TableContainer component={Paper} style={{padding:'10px'}}>
                    <Table aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell align="center"><b>Task-No</b></TableCell>
                          <TableCell align="center"><b>Description of the Task</b></TableCell>
                          <TableCell align="center"><b>Link to Refer</b></TableCell>
                          <TableCell align="center"><b>Duration</b></TableCell>
                          <TableCell align="center"><b>Status</b></TableCell>
                        </TableRow>
                      </TableHead>
                      {this.state.fetching ?
                        <div style={{textAlign:'center'}}>
                          Fetching Data .....
                        </div>
                        :
                      <TableBody>
                          {this.state.completed_tasks.map((content,index) => (
                            <TableRow key={index}>
                              <TableCell align="center">T{content.serial}</TableCell>
                              <TableCell align="center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.description}</TableCell>
                              <TableCell align="center">
                                 <a  rel="noopener noreferrer" href={content.link} target="_blank">LINK</a>
                              </TableCell>
                              <TableCell align="center">{content.start_date}-{content.end_date}</TableCell>
                              <TableCell align="center">
                                {!(this.props.prop_data) &&
                                    <React.Fragment>
                                     {content.verification_status === 0 &&
                                       <React.Fragment>
                                         <EditIcon onClick={()=>this.callEdit(content.referTask)}/>
                                         <DeleteIcon onClick={()=>this.confirmDelete(content.referTask)}/>
                                       </React.Fragment>
                                     }
                                     {content.verification_status === 1 &&
                                       <div style={{textAlign:'center',color:'green'}}>
                                         Verified
                                       </div>
                                     }
                                     {content.verification_status === -1 &&
                                       <div style={{textAlign:'center',color:'red'}}>
                                         Denied [ Reason - {content.denying_reason}]
                                       </div>
                                     }
                                    </React.Fragment>
                                 }
                              </TableCell>
                            </TableRow>
                          ))}
                      </TableBody>
                      }
                    </Table>
                  </TableContainer>
              :
              <Typography variant="h5" align="center">No Completed Task Found !!</Typography>
            }
        </ExpansionPanel>
        </React.Fragment>
      }

      </React.Fragment>
    );
   }
  }
}


class Complete extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display_overlay:false,
      level:'',
      skill_name:'',
      serial:'',
    }
  }

displayChanges=()=>{
  this.setState({
    display_overlay:!this.state.display_overlay,
  })
}
  handleChange =(level,skill_name,serial) =>{
    this.displayChanges();
    this.setState({
      level:level,
      skill_name:skill_name,
      serial:serial,
    })
  }
  render()
  {
    return(
      <React.Fragment>

        {this.state.display_overlay &&
          <FloatingWindow display_overlay={this.state.display_overlay}
          displayChanges={this.displayChanges} username={this.props.username}
          fetchTasksFromAdmin={this.props.fetchTasksFromAdmin} handleChange={this.handleChange}
          level={this.state.level} skill_name={this.state.skill_name} serial={this.state.serial}/>
        }

        <Checkbox
           checked={this.state.blue_allot} value={this.state.blue_allot}
          onChange={() => this.handleChange(this.props.level,this.props.skill_name,this.props.serial)}
          inputProps={{ 'aria-label': 'primary checkbox' }}
          />
      Completed ?

      </React.Fragment>
    )
  }
}

class  FloatingWindow extends Component {
  constructor()
  {
    super()
    this.state={
      start_date:'',
      end_date:'',
      link:'',
      description:'',
      level:'',
      skill_name:'',
      serial:'',
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.putData()
  }
  putData =()=>{
    if(this.props.referTaskDetails)
    {
      this.setState({
        start_date:this.props.referTaskDetails[0].start_date,
        end_date:this.props.referTaskDetails[0].end_date,
        description:this.props.referTaskDetails[0].description,
        link:this.props.referTaskDetails[0].link,
        level:this.props.level,
        skill_name:this.props.skill_name,
        serial:this.props.referTaskDetails[0].referTask,
      })
    }
    else
    {
      this.setState({
        level:this.props.level,
        skill_name:this.props.skill_name,
        serial:this.props.serial,
      })
    }
  }

  retriveData =(e)=>{
    this.setState({[e.target.name]:e.target.value})
  }
  submitTask =(e)=>{
    e.preventDefault()
    if((!this.state.start_date) || (!this.state.end_date) || (!this.state.description))
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill up the required fileds !!',
        alert_type:'warning',
      })
    }
    else{
      axios.post('/ework/user2/send_task',{
        start_date:this.state.start_date,
        end_date:this.state.end_date,
        description:this.state.description,
        level:this.state.level,
        task_name:this.state.skill_name,
        link:this.state.link,
        referTask:this.state.serial,
      })
      .then(res=>{
         if(res.data === 'done')
         {
           this.sendToChoose();
         }
         else if(res.data === 'updated')
         {
           this.setState({
             snack_open:true,
             snack_msg:'Updated !!',
             alert_type:'success',
           })
           this.props.callEdit();
            this.props.fetchTasksFromAdmin();
         }
      })
    }
  }

  sendToChoose=()=>{
    axios.post('/ework/user/send_for_review',{skill_name:this.state.skill_name,
    username:this.props.username,referTask:this.state.serial})
    .then( res => {
      this.setState({
        snack_open:true,
        snack_msg:'Saved !!',
        alert_type:'success',
      })
      this.props.handleChange();
      this.props.fetchTasksFromAdmin();
    });
  }
  render()
  {
    var closeAction,openAction;

    if(this.props.callEdit)
    {
      closeAction=this.props.callEdit;
      openAction = this.props.display_on;
    }
    else{
      closeAction= this.props.displayChanges;
      openAction=this.props.display_overlay;
    }
    return(
      <React.Fragment>

        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    		open={this.state.snack_open} autoHideDuration={2000}
    		onClose={()=>this.setState({snack_open:false})}>
    			<Alert onClose={()=>this.setState({snack_open:false})}
    			severity={this.state.alert_type}>
    				{this.state.snack_msg}
    			</Alert>
    		</Snackbar>
      <Dialog open={openAction} onClose={closeAction} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Details of the solution on {this.props.skill_name} in {this.props.level} Level</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Remember you should save your works in any public platform (which can be a drive also). So that our review team can review
            your project easily. Without a work proof link, your task would not be verified.
          </DialogContentText>

            <TextField
              id="filled-password-input"
              label="Description of your work"
              type="text"
              variant="filled"
              name="description"
              multiline
              fullWidth
              required
              value={this.state.description}
              onChange={this.retriveData}
            />
            <br /><br />
            <TextField
              id="filled-password-input"
              label="Start Date"
              type="text"
              variant="filled"
              multiline
              fullWidth
              value={this.state.start_date} name="start_date" onChange={this.retriveData}
              required
            />
            <br /><br />
            <TextField
              id="filled-password-input"
              label="End Date"
              type="text"
              variant="filled"
              multiline
              fullWidth
              value={this.state.end_date} name="end_date" onChange={this.retriveData}
              required
            />
            <br /><br />
            <TextField
              id="filled-password-input"
              label="Enter any link of your work"
              type="text"
              variant="filled"
              multiline
              fullWidth
              value={this.state.link}  name="link" onChange={this.retriveData}
              required
            />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeAction} color="primary">
            Cancel
          </Button>
          <Button onClick={this.submitTask} color="primary">
            Submit Data
          </Button>
        </DialogActions>
      </Dialog>
      </React.Fragment>
    )
  }
}
