import React, { Component } from 'react';
import Rating from '@material-ui/lab/Rating';
import axios from 'axios'
import Nav from '../../dynnav'
import { makeStyles } from '@material-ui/core/styles';
import {Paper,Hidden} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class ARHI extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      noti_route:false,
      searched_domain:'',
      selected_domain:'',
      list_of_domains:[],
      detail_domain_name:'',
      display_details:false,
      user:'',
      loader:true,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
   this.getUser();
  }
  getUser =()=>
  {
    axios.get('/ework/user2/getstudent').then(response =>{
      if (response.data.user)
      {
        this.setState({user:response.data.user})
        this.fetch_domains();
      }
      else{

      }
     })
  }
  fetch_domains =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Domain-For-Student'})
    .then( response => {
        this.fetch_selected();
        if(response){
          this.setState({fetched_domain:response.data})
        }
    });
  }
  fetch_selected=()=>{
    axios.get('/ework/user2/fetch_domains')
    .then( res => {
      this.setState({loader:false})
        if(res.data){
          this.setState({list_of_domains:res.data})
        }
    });
  }
  search =(e)=>{
    this.setState({searched_domain:e.target.value})
  }

  selected =(data)=>{
    if(data === null)
    {

    }
    else{
      this.setState({
        selected_domain:data.domain_name,
      })
      axios.post('/ework/user2/entry_domain',{domain:data.domain_name,user:this.state.user})
      .then( res => {
          if(res.data ==='have'){
            window.M.toast({html: 'Already Present !!',classes:'rounded #ec407a pink lighten-1'});
          }
          else if(res.data === 'done'){
            window.M.toast({html: 'Saved !!',classes:'rounded green darken-2'});
            this.setState({searched_domain:''})
            this.fetch_selected();
          }
      });
    }
  }

  showDivOpen=()=>{
    this.setState({display_details:!this.state.display_details})
  }

  postDetail =(detail_domain_name)=>{
    this.showDivOpen();
    this.setState({detail_domain_name:detail_domain_name})
  }

  render() {
    if(this.state.loader)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      );
    }
    else{
    const options = this.state.fetched_domain.map(option => {
      const firstLetter = option.domain_name[0].toUpperCase();
      return {
        firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
        ...option,
      };
    });


    return (
      <React.Fragment>
        <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/><br/><br />
           <Grid container spacing={1}>
             <Grid item xs={4} sm={4}  xl={4}/>
             <Grid item xs={4} xl={4}sm={4} >
                <Grid container item xs={12} sm={12} xl={12} md={12}>
                   <Grid item  xs={12} sm={12} xl={12} md={12}>
                     <Autocomplete
                       id="grouped-demo"
                       options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                       groupBy={option => option.firstLetter}
                       getOptionLabel={option => option.domain_name}
                       onChange={(event, value)=>this.selected(value)}
                       style={{ width: '100%' }}
                       renderInput={params => (
                         <TextField {...params} label="Search Domain" variant="outlined" fullWidth />
                       )}
                     />
                   </Grid>
                </Grid>
             </Grid>
             <Grid item xs={4} sm={4} xl={4} />
           </Grid>

           <Grid container spacing={1} style={{padding:'8px'}}>
              {this.state.list_of_domains.map((content,index)=>(
                <Grid item xs={2} sm={2} style={{borderRadius:'5px',minHeight:'60px'}} key={index} onClick={()=>this.postDetail(content.domain_name)}>
                    <Paper elevation={3} style={{padding:'2px'}}>
                        <Grid container spacing={1}>
                           <Grid item xs={10}>
                              <Typography variant="overline" display="block" style={{fontSize:'15px',textAlign:'center'}} gutterBottom>
                               {content.domain_name}
                              </Typography>
                           </Grid>
                           <Grid item xs={2} sm={2}>
                              <CloseIcon className="go" />
                           </Grid>
                        </Grid>
                    </Paper>
                </Grid>

              ))}
           </Grid>

           <Detailer display_details={this.state.display_details} showDivOpen={this.showDivOpen} detail_domain_name={this.state.detail_domain_name} />
      </React.Fragment>
    );
   }
  }
}

class Detailer extends Component {
  constructor() {
    super()
    this.state={
      isChecked:'Begineer',
      active:'Begineer',
      toggled:false,
      instructions:[],
      fetch_order:false,
      fetching:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetch_Domain_Level()
  }
  componentDidUpdate =(prevProps,prevState)=>{
    if(prevState.isChecked!==this.state.isChecked){
      this.fetch_Domain_Level()
    }
    if(prevProps.detail_domain_name!== this.props.detail_domain_name){
      this.fetch_Domain_Level()
    }
  }
  fetch_Domain_Level =()=>{
    this.setState({fetching:true})
    axios.post('/ework/user2/fetch_in_admin_for_path',{
      action:'add-path',
      for:'Domain-'+this.props.detail_domain_name,usertype:this.state.isChecked})
    .then( res => {
        if(res.data){
          this.setState({instructions:res.data})
        }
  this.setState({fetching:false})
    })
    .catch( err => {
            this.setState({fetching:false})
    });
  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    const divStyle={
      backgroundColor:'#ff4081',
      color:'white',
      textAlign:'center',padding:'3px',borderRadius:'20px',
    }
    const divS={
      textAlign:'center',padding:'3px',borderRadius:'20px',
    }
    if (this.state.active === position) {
        return divStyle;
      }
      return divS;
  }

  render(){
    return(
      <React.Fragment>
         {this.props.detail_domain_name &&
             <React.Fragment>
             <Dialog fullScreen open={this.props.display_details} onClose={this.props.showDivOpen} TransitionComponent={Transition}>
                 <AppBar>
                   <Toolbar>
                    <Grid style={{marginTop:'55'}}container spacing={1}>
                      <Grid item xs={2} sm={2}>
                         <IconButton edge="start" color="inherit" onClick={this.props.showDivOpen} aria-label="close">
                           <CloseIcon />
                         </IconButton>
                      </Grid>
                      <Grid item xs={8} sm={8}>
                        <div style={{textAlign:'center',fontSize:'25px'}}>
                         Selected Domain <span className="yellow-text">{this.props.detail_domain_name}(<Rating name="read-only" value={2} readOnly />)</span>
                        </div>
                      </Grid>
                      <Grid item xs={2} sm={2}/>
                    </Grid>
                   </Toolbar>
                 </AppBar>
                 <List style={{marginTop:'70px'}}>

                 <div style={{marginRight:'7px',marginLeft:'7px',borderRadius:'5px'}}>
                     <Grid container>
                         <Grid item xs={9} sm={3}>
                            <Paper style={{padding:'6px',boxShadow:'0px 0px 4px 0px rgba(186, 186, 186, 0.75)',borderRadius:'5px'}}>
                              <Grid container spacing={1}>
                                 <Grid item xs={4} sm={4}>
                                    <Paper className="go" style={this.color('Begineer')}  onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</Paper>
                                 </Grid>
                                 <Grid item xs={4} sm={4}>
                                    <Paper className="go" style={this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</Paper>
                                 </Grid>
                                 <Grid item xs={4} sm={4}>
                                      <Paper className="go" style={this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</Paper>
                                 </Grid>
                              </Grid>
                            </Paper>
                         </Grid>
                         <Hidden xsDown><Grid item sm={7}/></Hidden>
                         <Grid item xs={3} sm={2}/>
                     </Grid>
           <br /><br />


                     <TableContainer component={Paper}>
                       <Table aria-label="simple table">
                         <TableHead>
                           <TableRow>
                             <TableCell align="center"><b>LEARN</b></TableCell>
                             <TableCell align="center"><b>LINK TO REFER</b></TableCell>
                             <TableCell align="center"><b>TASK COMPLETED</b></TableCell>
                             <TableCell align="center"><b>RATING</b></TableCell>
                           </TableRow>
                         </TableHead>
                         {this.state.fetching ?
                           <div style={{textAlign:'center'}}>
                             Fetching Data .....
                           </div>
                           :
                         <TableBody>
                             {this.state.instructions.map((content,index) => (
                               <TableRow key={index}>
                                 <TableCell align="center">{content.course_to_learn}</TableCell>
                                 <TableCell align="center">
                                    <a  rel="noopener noreferrer" href={content.ref_link_for_skill} target="_blank">{content.ref_link_for_skill}</a>
                                 </TableCell>
                                 <TableCell align="center"><TaskCompleted course={content.course_to_learn} /></TableCell>
                                 <TableCell align="center"><RatingS course={content.course_to_learn} /></TableCell>
                               </TableRow>
                             ))}
                         </TableBody>
                         }
                       </Table>
                     </TableContainer>
</div>
                 </List>
                </Dialog>

             </React.Fragment>
         }
      </React.Fragment>

    )

  }
}


class TaskCompleted extends Component {
  constructor(){
    super()
    this.state={
      total:'',
      loading:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetchCompletationHistory()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props){
      this.fetchCompletationHistory()
    }
  }
  fetchCompletationHistory =()=>{
    this.setState({loading:true})
    axios.post('/ework/user2/fetchCompletationHistory',{course:this.props.course})
    .then( res => {
        if(res.data === 'No Data'){
          this.setState({total:res.data})
        }
        else{
          this.setState({total:res.data.length})
        }
        this.setState({loading:false})
    });
  }
  render() {
    if(this.state.loading)
    {
      return(
        <div className="center" style={{padding:'10px'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else
    {
    return (
      <div>
        {this.state.total}
      </div>
    );
   }
  }
}

class RatingS extends Component {
  constructor(){
    super()
    this.state={
      rating:'',
      loader:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetchRating()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props){
      this.fetchRating()
    }
  }
  fetchRating =()=>{
    this.setState({loader:true})
    axios.post('/ework/user2/fetchRating',{course:this.props.course})
    .then( res => {
      //console.log(res.data)
        if(res.data){
          this.setState({rating:res.data})
        }
        this.setState({loader:false})
    });
  }
  render() {
    const usess = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    '& > * + *': {
      marginTop: theme.spacing(1),
    },
  },
}));
  if(this.state.loader)
  {
    return(
      <div className="center" style={{padding:'10px'}}>
        <CircularProgress color="secondary" />
      </div>
    )
  }
  else{
    return (

      <div className={usess.root}>
        <Rating name="half-rating-read" defaultValue={parseFloat(this.state.rating)} precision={0.5} readOnly />
      </div>
    );
    }
  }
}
