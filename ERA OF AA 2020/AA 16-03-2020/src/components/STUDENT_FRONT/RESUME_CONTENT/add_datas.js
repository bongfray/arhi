import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';



export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      Action:'',
      name: '',
      emailid: '',
      mobileno:'',
      address: '',
      gender:'',
      faculty_id:'',
      subject_taking:'',
      current:false,
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
  })

  if(this.props.data.Action === 'FACULTY LIST'){
    this.setState({current:true})
  }
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {

    let pageTitle;
if(this.state.serial) {
  pageTitle = "EDIT DATAS";
} else {
  pageTitle ="ADD DATAS";
}

    return(
      <Paper elevation={3} style={{padding:'10px'}}>
      <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{pageTitle} </Typography><br />
      {this.props.description &&
        <Typography variant="overline" style={{textAlign:'center'}} display="block" gutterBottom>
         {this.props.description}
        </Typography>
      }
      <br />

          {this.props.data.fielddata.map((content,index)=>(
            <React.Fragment key={index}>
              <Grid container spacing={1} >
                 <Grid item xs={1} sm={1}/>
                  <Grid item xs={2} sm={2}>{content.header}</Grid>
                  <Grid item xs={8} sm={8}>
                      <TextField
                        type={content.type}
                        id="outlined-textarea"
                        label={content.placeholder}
                        name={content.name}
                        value={this.state[content.name]}
                        onChange={e => this.handleD(e, index)}
                        multiline
                        variant="filled"
                        fullWidth
                      />
                  </Grid>
                  <Grid item xs={1} sm={1}/>
              </Grid>
              <br />
              </React.Fragment>
          ))}
          <Grid container spacing={1}>
            <Grid item xs={9} sm={9} />
            <Grid item xs={1} sm={1}>
              <Button variant="outlined" onClick={this.props.cancel} color="secondary">CANCEL</Button>
            </Grid>
            <Grid item xs={2} sm={2}>
             <Button variant="contained" onClick={this.handleSubmit} style={{backgroundColor:'#455a64',color:'white'}} >SUBMIT</Button>
            </Grid>
          </Grid>
      </Paper>
    )
  }
}
