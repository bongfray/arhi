import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import {Typography,TextField} from '@material-ui/core';
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




export default class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.initialState ={
      expired_date:'',
      expired_month:'',
      expired_year:'',
      denying_reason:'',
      approving:false,
      denying:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.state=this.initialState;
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleDeny =(id,username) =>{
    if(!this.state.denying_reason)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill all the Details !!',
        alert_type:'warning',
      });
    }
    else
    {
    axios.post('/ework/user/denyrequest',{
      serial: id,
      username:username,
      denying_reason:this.state.denying_reason,
    }).then(res=>{
      this.props.modalrender();
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
    .catch( err => {
      this.setState({
        snack_open:true,
        snack_msg:'Something Went Wrong !!',
        alert_type:'error',
      });
    });
   }
  }
  handleApprove = (id,username) =>{
    if(!this.state.expired_date ||!this.state.expired_month || !this.state.expired_year)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill all the Details !!',
        alert_type:'warning',
      });
    }
    else
    {
    axios.post('/ework/user/approverequest',{
      serial: id,
      username:username,
      expired_date:this.state.expired_date,
      expired_month:this.state.expired_month,
      expired_year:this.state.expired_year
    }).then(res=>{
      this.props.modalrender();
      if(res.data)
      {
        var request = this.props.request;
        this.props.handleRequestModify({
          request: request.filter(request => request.serial !== id),
          viewdata:'',
        })
        this.setState(this.initialState)
      }
    })
   }
  }
  proceedToApprove = () =>{
    this.setState({approving:true,denying:false})
  }
  proceedToDeny = () =>{
    this.setState({denying:true,approving:false})
  }
  addExpire = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }


  render()
  {
    var dat;
    if(this.props.view_details ==="yes")
    {
      dat=
      <React.Fragment>
      <Typography variant="h6" align="center">Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!
      </Typography>
      <br />
      <div style={{float:'right'}}>
        <Button variant="contained" color="secondary" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</Button>
      </div>

      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat=
    <React.Fragment>
      <Typography variant="h6" align="center">We can't able to find any data for this request !! Kindly take decision manually !!</Typography>
      <br />
       <div style={{float:'right'}}>
          <Button variant="contained" color="secondary" onClick={this.proceedToDeny}>DENY</Button>
          &ensp;
          <Button variant="contained" color="primary" onClick={this.proceedToApprove}>APPROVE</Button>
        </div>
      <br />
      <div style={{margin:'20px 0px 0px 0px'}}>
          {this.state.denying===true &&
            <React.Fragment>
              <TextField
                id="outlined-multiline-static"
                label="Enter the Reason"
                multiline
                rows="4"
                fullWidth
                type="text" name="denying_reason" value={this.state.denying_reason} onChange={this.addExpire}
                variant="filled"
              />
              <br /><br />
              <Button variant="contained" style={{float:'right'}} color="secondary" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</Button>
            </React.Fragment>
          }

          {this.state.approving=== true &&
            <React.Fragment>
             <Typography variant="h6" align="center">Enter Expiry Date</Typography>
             <br /><br />
             <TextField fullWidth id="outlined-basic1" label="Day(dd)" variant="outlined" type="number"
              name="expired_date" value={this.state.expired_date} onChange={this.addExpire} />
             <br /><br />
             <TextField fullWidth id="outlined-basic3" label="Month(mm)" variant="outlined" type="number"
               name="expired_month" value={this.state.expired_month} onChange={this.addExpire} />
             <br /><br />
             <TextField fullWidth id="outlined-basic3" label="Year(yyyy)" variant="outlined" type="number"
               name="expired_year" value={this.state.expired_year} onChange={this.addExpire} />
               <br /><br />
              <Button variant="contained" style={{float:'right'}} color="primary" onClick={() => this.handleApprove(this.props.index_id,this.props.username)}
              >APPROVE</Button>
            </React.Fragment>
        }
    </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    return(
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      {this.props.renderDiv&&
        <Dialog
          open={true} fullWidth
          onClose={this.props.modalrender}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            {dat}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.modalrender} color="primary" autoFocus>
              CLOSE
            </Button>
          </DialogActions>
        </Dialog>
      }
    </React.Fragment>
    )
  }
}
