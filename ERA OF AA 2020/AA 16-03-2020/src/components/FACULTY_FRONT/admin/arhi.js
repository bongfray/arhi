import React, { Component } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';


import HandleSkill from './ARHI-Operation/handle-skill'
import HandleDomain from './ARHI-Operation/handle-domain'
import Action from './ARHI-Operation/action'

export default class Arhi extends Component {
  constructor(){
      super();
      this.state ={
          radio:[{value:'Resume-Skill'},
          {value:'Domain'},{value:'Action'}],
          choosed: '',
      }
  }

  render()
  {

      return(
          <React.Fragment>
              <AppBar position="static" color="default">
                <Tabs
                  value={this.state.choosed}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="scrollable"
                  scrollButtons="auto"
                  aria-label="scrollable auto tabs example"
                >
                {this.state.radio.map((content,index)=>(
                  <Tab key={index}  label={content.value} onClick={(e)=>this.setState({choosed:index})} />
                ))}
                </Tabs>
              </AppBar>
              <br />
              <div>
                <Display choosed={this.state.choosed} />
              </div>
          </React.Fragment>
      );
  }
}


 class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: '',
            select:'',
            active:'',
            open_drawer:true,
        }
    }
    handleCheck = (e) => {
        this.setState({
            select: e,
            open_drawer:false
        })

        if (this.state.active === e) {
          this.setState({active : null})
        } else {
          this.setState({active : e})
        }

    }
    componentDidUpdate=(prevProps)=>{
      if(prevProps.choosed !== this.props.choosed)
      {
        this.setState({open_drawer:true})
      }
    }

    color =(position) =>{
      if (this.state.active === position) {
          return "#ff4081";
        }
        return "";
    }

    render(){
      let choosed_option;
        if(this.props.choosed === 0)
        {
            choosed_option=
            <React.Fragment>
            {this.state.open_drawer === false &&
              <div onClick={()=>this.setState({open_drawer:true})} className="float-left"
                style={{background:'#e0e0e0',borderRadius: '0px 20px 20px 0px'}}>
                <ChevronRightIcon />
              </div>
            }
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('add-skill')}}
                      onClick={(e)=>{this.handleCheck('add-skill')}}>
                        <ListItemText primary={"Add Skill"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('add-project')}}
                      onClick={(e)=>{this.handleCheck('add-project')}}>
                        <ListItemText primary={"Projects on Skill"} />
                      </ListItem>
                      <ListItem button  style={{color:this.color('add-question')}}
                       onClick={(e)=>{this.handleCheck('add-question')}}>
                        <ListItemText primary={"Add Questions"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                  <HandleSkill select={this.state.select} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed === 1)
        {
            choosed_option=
            <React.Fragment>
            {this.state.open_drawer === false &&
              <div onClick={()=>this.setState({open_drawer:true})} className="float-left"
                style={{background:'#e0e0e0',borderRadius: '0px 20px 20px 0px'}}>
                <ChevronRightIcon />
              </div>
            }
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('add-domain')}}
                      onClick={(e)=>{this.handleCheck('add-domain')}}>
                        <ListItemText primary={"Add Domain"} />
                      </ListItem>
                      <ListItem button style={{color:this.color('achieve-domain')}}
                      onClick={(e)=>{this.handleCheck('achieve-domain')}}>
                        <ListItemText primary={"Add Path to Achieve Domain"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                    <HandleDomain select={this.state.select} />
                </main>
            </React.Fragment>
        }
        else if(this.props.choosed === 2)
        {
            choosed_option=
            <React.Fragment>
            {this.state.open_drawer === false &&
              <div onClick={()=>this.setState({open_drawer:true})} className="float-left"
                style={{background:'#e0e0e0',borderRadius: '0px 20px 20px 0px'}}>
                <ChevronRightIcon />
              </div>
            }
            <Drawer
                  variant="persistent"
                  anchor="left"
                  open={this.state.open_drawer}
                  onClose={()=>this.setState({open_drawer:false})}
                >
                <div>
                    <IconButton onClick={()=>this.setState({open_drawer:false})}>
                      {this.state.open_drawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                    </div>
                  <Divider />
                  <List>
                      <ListItem button style={{color:this.color('add-suprimity')}}
                      onClick={(e)=>{this.handleCheck('add-suprimity')}}>
                        <ListItemText primary={"Add Suprimity"} />
                      </ListItem>
                  </List>
                </Drawer>
                <main>
                    <Action select={this.state.select} />
                </main>
            </React.Fragment>
        }
        else{
            choosed_option=
                <div></div>
        }
        return(
          <React.Fragment>
             <div style={{marginTop:'15px'}}>
               {choosed_option}
             </div>
          </React.Fragment>
        )
    }
}
