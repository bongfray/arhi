import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import {Paper,InputBase,Divider,IconButton,CircularProgress} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import DataTable from './prod_table'
import ValidateUser from '../validateUser'

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      index:'',
      modal:false,
      disabled:false,
      allowed:false,
      error: null,
      products: [],
      action:'',
      loading:true,
      value_for_search:'',
      success:'none',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch(this.props.action)
  }

  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({success:'block',disabled:true,id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.setState({success:'none',disabled:false})
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
 if((index === "Skill-For-Student")){
   delroute = "/ework/user2/del_inserted_data";
  }
  else{
    delroute = "/ework/user2/del_inserted_data";
   }

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:false,
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
 if((action === "add-path"))
  {
    fetchroute = "/ework/user2/fetch_in_admin_for_path";
  }
  else
  {
    fetchroute = "/ework/user2/fetch_in_admin";
  }
  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
    for:this.props.for,
  })
  .then(response =>{
    this.setState({
     products: response.data,
     loading:false,
   })
  })
}

searchEngine =(e)=>{
  this.setState({value_for_search:e.target.value})
}

  render() {
    const {products} = this.state;
    var libraries = products,
    searchString = this.state.value_for_search.trim().toLowerCase();
    if(searchString.length > 0)
    {
      if(this.props.action === "Skill-For-Student")
      {
        libraries = libraries.filter(function(i) {
          return i.skill_name.toLowerCase().match( searchString );
        });
      }
      else if(this.props.action === "Domain-For-Student"){
        libraries = libraries.filter(function(i) {
          return i.domain_name.toLowerCase().match( searchString );
        });
      }

    }

    if(this.state.loading)
    {
      return(
        <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
          <CircularProgress color="secondary" />
        </div>
      );
    }
    else{
      return(
        <React.Fragment>
            {this.state.success === 'block' &&
              <ValidateUser  showDiv={this.showDiv}/>
             }


            {((this.props.action==="Skill-For-Student") || (this.props.action==="Domain-For-Student")) ?
               <div style={{padding:'30px'}}>
                <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
              </div>
              :
              <React.Fragment></React.Fragment>
            }

             <div style={{margin:'0px 15px 0px 15px'}}>
                <DataTable username={this.props.username} noChange={this.props.noChange} data={libraries}
                 action={this.props.action} disabled={this.state.disabled} fielddata={this.props.data.fielddata}
                 incoming={this.props.data} editProduct={this.props.editProduct}
                 deleteProduct={this.deleteProduct}/>
             </div>
            </React.Fragment>
      )
    }

  }
}



const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
