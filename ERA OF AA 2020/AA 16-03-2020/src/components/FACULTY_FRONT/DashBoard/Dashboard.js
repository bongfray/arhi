import React, { Component } from 'react';
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import Daa from './DAA'
import ContentLoader from "react-content-loader"
import {
  CircularProgressbar,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { easeQuadInOut } from "d3-ease";

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import AnimatedProgressProvider from "./AnimatedProgressProvider";
import CircularProgress from '@material-ui/core/CircularProgress';



export default class Dash extends Component {

    constructor(props) {
      super(props);
      this.state = {
        loading: true,
        home:'/ework/faculty',
        logout:'/ework/user/logout',
        login:'/ework/flogin',
        get:'/ework/user/',
        noti_route:true,
        nav_route: '/ework/user/fetchnav',
        isChecked:0,
        active:0,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchlogin()
    }
    fetchlogin = () =>{
      axios.get('/ework/user/')
      .then(response =>{
        this.setState({loading: false})
        if(!response.data.user){
          this.setState({
            redirectTo:'/ework/faculty',
          });
        }
      })
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: e,
      })
      if (this.state.active === e) {
        this.setState({active : null})
      } else {
        this.setState({active : e})
      }
    }

    color =(position) =>{
      if (this.state.active === position) {
          return "#f73378";
        }
        return "#9e9e9e";
    }

  render()
  {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="20" y="10" rx="5" ry="5" width="110" height="50" />
        <rect x="140" y="10" rx="3" ry="3" width="110" height="50" />
        <rect x="270" y="10" rx="3" ry="3" width="110" height="50" />

        <rect x="20" y="90" rx="1" ry="1" width="110" height="15" />
        <rect x="140" y="90" rx="1" ry="1" width="110" height="15" />
        <rect x="270" y="90" rx="1" ry="1" width="110" height="15" />


      </ContentLoader>
    )

    if(this.state.loading=== true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
        <Nav noti_route={this.state.noti_route} login={this.state.login}
        home={ this.state.home} nav_route={this.state.nav_route} get={this.state.get}
        logout={this.state.logout}/>
        <Grid container style={{marginTop:'8px'}}>
          <Grid item xs={2} sm={5}/>
          <Grid container item xs={8} sm={2} style={{marginTop:'5px'}} spacing={1}>
            <Grid item xs={6} sm={6}>
              <Paper style={{backgroundColor:this.color(0),textAlign:'center',color:'white',padding:'8px',cursor:'pointer'}}
              onClick={(e)=>{this.handleComp(0)}}>Weekly</Paper>
            </Grid>
            <Grid item xs={6} sm={6}>
              <Paper style={{backgroundColor:this.color(1),textAlign:'center',padding:'8px',color:'white',cursor:'pointer'}}
              onClick={(e)=>{this.handleComp(1)}}>Monthly</Paper>
            </Grid>
          </Grid>
          <Grid item xs={2} sm={5} />
        </Grid>
        <Dashh choice={this.state.isChecked}/>
      </React.Fragment>
    )
  }
}
  }
}


class Dashh extends Component {
  constructor(props) {
    super(props);
    this.state = {
      administrative:'',
      academics:'',
      research:'',
      fetching:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.countPercentage()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice !== this.props.choice)
    {
      this.countPercentage()
    }
  }
  countPercentage =() =>{
    let adroute,acaroute,resroute;
      adroute ='/ework/user/fetchAdmins_Actual';
      acaroute = '/ework/user/fetchAcad_Actual';
      resroute = '/ework/user/fetchRes_Actual';
    this.AdministrativeP(adroute);
    this.ResearchP(resroute);
    this.AcademicP(acaroute);
  }


    AdministrativeP = (adroute) =>{
        this.setState({fetching:true})
      axios.get(adroute)
      .then( response => {
        if(this.props.choice === 0)
        {
          this.setState({
            administrative:response.data.weekly_percentage,
          })
        }
        else if(this.props.choice === 1){
          this.setState({
            administrative:response.data.monthly_percentage,
          })
        }
    this.setState({fetching:false})
      })
      .catch( err => {
    this.setState({fetching:false})
      });
    }
    AcademicP = (acaroute) =>{
  this.setState({fetching:true})
      axios.get(acaroute)
      .then( response => {
        if(this.props.choice === 0)
        {
          this.setState({
            academics:response.data.weekly_percentage,
          })
        }
        else if(this.props.choice === 1){
          this.setState({
            academics:response.data.monthly_percentage,
          })
        }
  this.setState({fetching:false})
      })
      .catch( err => {
  this.setState({fetching:false})
      });
    }

    ResearchP = (resroute) =>{
  this.setState({fetching:true})
      axios.get(resroute)
      .then( response => {
        if(this.props.choice === 0)
        {
          this.setState({
            research:response.data.weekly_percentage,
          })
        }
        else if(this.props.choice === 1){
          this.setState({
            research:response.data.monthly_percentage,
          })
        }
  this.setState({fetching:false})
      })
      .catch( err => {
  this.setState({fetching:false})
      });
    }



render(){
  if(this.state.fetching)
  {
    return(
      <div style={{marginLeft:'50%',padding:'30px'}}>
        <CircularProgress color="secondary" />
      </div>
    )
  }
  else{
        return (
          <React.Fragment>
          <Grid container spacing={2} style={{padding:'15px'}}>
             <Grid item xs={12} sm={4} >
                  <Example label="Administrative">
                    <AnimatedProgressProvider
                      valueStart={0}
                      valueEnd={this.state.administrative}
                      duration={1.4}
                      easingFunction={easeQuadInOut}
                    >
                      {value => {
                        const roundedValue = Math.round(value);
                        return (
                          <CircularProgressbar
                            value={value}
                            text={`${roundedValue}%`}
                            styles={buildStyles({ pathTransition: "none" })}
                          />
                        );
                      }}
                    </AnimatedProgressProvider>
                  </Example>
              </Grid>
              <Grid item xs={12} sm={4} >
                <Example label="Academics">
                  <AnimatedProgressProvider
                    valueStart={0}
                    valueEnd={this.state.academics}
                    duration={1.4}
                    easingFunction={easeQuadInOut}
                  >
                    {value => {
                      const roundedValue = Math.round(value);
                      return (
                        <CircularProgressbar
                          value={value}
                          text={`${roundedValue}%`}
                          styles={buildStyles({ pathTransition: "none" })}
                        />
                      );
                    }}
                  </AnimatedProgressProvider>
                </Example>
              </Grid>
              <Grid item xs={12} sm={4} >
                  <Example label="Research">
                    <AnimatedProgressProvider
                      valueStart={0}
                      valueEnd={this.state.research}
                      duration={1.4}
                      easingFunction={easeQuadInOut}
                    >
                      {value => {
                        const roundedValue = Math.round(value);
                        return (
                          <CircularProgressbar
                            value={value}
                            text={`${roundedValue}%`}
                            styles={buildStyles({ pathTransition: "none" })}
                          />
                        );
                      }}
                    </AnimatedProgressProvider>
                  </Example>
              </Grid>
          </Grid>
          <div>
          {this.props.choice=== 0 && <Daa />}
           <FreeSlots choice={this.props.choice}/>

          </div>
          </React.Fragment>
          );
      }
  }

  }
  function Example(props) {
  return (
    <Paper elevation={3} style={{padding:'5px'}}>
      <div  style={{backgroundColor:'#00e5ff',textAlign:'center',fontSize:'20px'}}>{props.label}</div>
      <br />
       <div style={{marginLeft:'26%',width:'200px'}}>{props.children}</div>
    </Paper>
  );
  }


  class FreeSlots extends Component {
    constructor() {
      super()
      this.state={
        free_slot_own:'',
        free_slot_other:'',
        absent:'',
      }
      this.componentDidMount =  this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchFree()
    }
    fetchFree = () =>{
      let free_slot_other_route,absent_route;

        // free_slot_own_route ='/ework/user/fetch_own_extra_week';
        free_slot_other_route = '/ework/user/fetch_other_extra';
        absent_route = '/ework/user/fetch_absent'

      // this.FreeSlotOwn(free_slot_own_route);
      this.FreeSlotOther(free_slot_other_route);
      this.Absent(absent_route);
    }

    FreeSlotOwn = (adfroute) =>{
      axios.get(adfroute)
      .then( response => {
        this.setState({
          free_slot_own:response.data.result,
        })
      });
    }
    FreeSlotOther = (free_slot_other_route) =>{
      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();


      axios.get(free_slot_other_route)
      .then( response => {
        const reques2 = response.data.total.filter(item =>
        item.week === week && item.month === month && item.year === year);
        let free_other_total_week = reques2.length;

        const reques3 = response.data.completed.filter(item =>
        item.week === week && item.month === month && item.year === year);
        let free_other_handled_week = reques3.length;

        const reques4 = response.data.total.filter(item =>item.month === month && item.year === year);
        let free_other_total_month = reques4.length;

        const reques5 = response.data.completed.filter(item =>  item.month === month && item.year === year);
        let free_other_handled_month = reques5.length;

        if(this.props.choice === 0)
        {
          this.setState({
            total_other:free_other_total_week,
            free_slot_other:free_other_handled_week,
          })
        }
        else{
          this.setState({
            total_other:free_other_total_month,
            free_slot_other:free_other_handled_month,
          })
        }
      });
    }



    Absent = (absent_route) =>{

      var month = parseInt(Date.today().toString("M"));
      var year = parseInt(Date.today().toString("yyyy"));
      Date.prototype.getWeek = function ()
      {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };
      var today = new Date.today();
      var week =today.getWeek();


      axios.get(absent_route)
      .then( response => {

        const reques3 = response.data.filter(item =>
        item.week === week && item.month === month && item.year === year);
        let absent_week = reques3.length;

        const reques4 = response.data.filter(item =>item.month === month && item.year === year);
        let absent_month = reques4.length;


        if(this.props.choice === 0)
        {
          this.setState({
           absent:absent_week,
          })
        }
        else{
          this.setState({
            absent:absent_month,
          })
        }
      });
    }

    render()
    {
      return(
        <React.Fragment>
          <Grid container spacing={1}>
             <Grid item xs={12} sm={4}>
             <div style={{backgroundColor:'yellow',textAlign:'center'}}>Total Extra Slot Taken(By You)</div>
               <Paper elevation={3} style={{padding:'20px'}}>
                 <div>
                        No of the Extra Slot taken : {this.state.free_slot_own}
                 </div>
               </Paper>
             </Grid>

             <Grid item xs={12} sm={4}>
               <div style={{backgroundColor:'yellow',textAlign:'center'}}>Total Extra Slot Taken(For other Faculty)</div>
                 <Paper elevation={3} style={{padding:'20px'}}>
                     <Grid container spacing={1}>
                        <Grid item xs={8} sm={8}>Hanlded Slot -- Total Request - </Grid>
                        <Grid item xs={4} sm={4}>{this.state.free_slot_other} -- {this.state.total_other}</Grid>
                     </Grid>
                 </Paper>
             </Grid>

             <Grid item xs={12} sm={4}>
                <div style={{backgroundColor:'yellow',textAlign:'center'}}>Total Absent</div>
                 <Paper elevation={3} style={{padding:'20px'}}>
                   <div>
                     Total Number of  Absent -  <span style={{color:'red'}}><b>{this.state.absent}</b></span>
                   </div>
                 </Paper>
             </Grid>
        </Grid>
        </React.Fragment>
      );
    }
  }
