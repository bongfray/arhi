import React from 'react';
import Error from './error3.png'
import { Link } from 'react-router-dom';
export default class Not extends React.Component{
    render(){
        return (
            <React.Fragment>
                <div className="row">
                    <div className="error-img">
                    <img alt="Error Page"src={Error} className="error-image"/>
                    </div>
                </div>
                <div className="row fof">

                        <h1>Oops! You took a wrong turn...</h1>
                </div>
                <div className="row center">
                    <Link to="/ework"><button className="button-home">Go to Home Page</button></Link>

                </div>
            </React.Fragment>
        )
    }
}
