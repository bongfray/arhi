import React from 'react';
import axios from 'axios';
import Backdrop from '@material-ui/core/Backdrop';
import Grid from '@material-ui/core/Grid';
import DataTable from './prod_table'
import CircularProgress from '@material-ui/core/CircularProgress';
import ValidateUser from '../validateUser'



export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
      loading:true,
      value_for_search:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({modal: !this.state.modal,disabled:'disabled',id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
 if((index === "Skill-For-Student")){
   delroute = "/ework/user2/del_inserted_data";
  }
  else{
    delroute = "/ework/user2/del_inserted_data";
   }

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:'',
        success:'none',
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
 if((action === "add-path"))
  {
    fetchroute = "/ework/user2/fetch_in_admin_for_path";
  }
  else
  {
    fetchroute = "/ework/user2/fetch_in_admin";
  }

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
    for:this.props.for,
  })
  .then(response =>{
    this.setState({
     products: response.data,
     loading:false,
   })
  })
}

searchEngine =(e)=>{
  this.setState({value_for_search:e.target.value})
}

  render() {
    const {products} = this.state;
    var libraries = products,
    searchString = this.state.value_for_search.trim().toLowerCase();
    if(searchString.length > 0)
    {
      if(this.props.action === "Skill-For-Student")
      {
        libraries = libraries.filter(function(i) {
          return i.skill_name.toLowerCase().match( searchString );
        });
      }
      else if(this.props.action === "Domain-For-Student"){
        libraries = libraries.filter(function(i) {
          return i.domain_name.toLowerCase().match( searchString );
        });
      }

    }

    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      );
    }
    else{
      return(
        <div>
            <div style={{display:this.state.success}}>
              <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
            </div>

            {((this.props.action==="Skill-For-Student") || (this.props.action==="Domain-For-Student")) ?
                <div>
                    <div className="search-wrapper">
                      <Grid container spacing={1}>
                         <Grid item xs={11} sm={11}>
                            <input id="search" style={{borderTop:'1px black'}}
                            placeholder="Search" value={this.state.value_for_search}
                            onChange={this.searchEngine} />
                         </Grid>
                         <Grid item xs={1} sm={1}>
                            <i className="material-icons">search</i>
                         </Grid>
                      </Grid>
                    </div>
                </div>
              :
              <div></div>
          }

           <Grid container spacing={1} fullWidth>
                <DataTable username={this.props.username} noChange={this.props.noChange} data={libraries}
                 action={this.props.action} disabled={this.state.disabled} fielddata={this.props.data.fielddata}
                 incoming={this.props.data} editProduct={this.props.editProduct}
                 deleteProduct={this.deleteProduct}/>
             </Grid>
            </div>
      )
    }

  }
}
