import React, { Component } from 'react';
import axios from 'axios'
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddProduct from './add';
import ProductList from './prod'


export default class SkillLevel extends Component {

 constructor(props)
 {
   super(props)
   this.state ={
     username:'',
     redirectTo:'',
     option:'',
     domain_name:'',
     saved_domain:[],
     loading:true,
   }
   this.componentDidMount = this.componentDidMount.bind(this)
 }

handleOption = (e) =>{
 this.setState({option: e.target.value})
}
setSkill=(e)=>{
  if(e === null)
  {

  }
  else{
    this.setState({domain_name:e.domain_name})
  }
}

componentDidMount(){
 this.fetchSavedSkills()
}

fetchSavedSkills =() =>{
 axios.post('/ework/user2/fetch_in_admin',{action:'Domain-For-Student'})
 .then(res => {
     if(res.data)
     {
       this.setState({saved_domain:res.data,loading:false})
     }
 });
}


 render()
 {
   if(this.state.loading)
   {
     return(
       <Backdrop  open={true} >
         <CircularProgress color="secondary" />
       </Backdrop>
     )
   }
   else
   {
     const options = this.state.saved_domain.map(option => {
       const firstLetter = option.domain_name[0].toUpperCase();
       return {
         firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
         ...option,
       };
     });
   return(
     <React.Fragment>
     <Grid container spacing={1}>
             <Grid item xs={6} sm={6}>
                <Paper elevation={2} style={{padding:'20px'}}>
                    <Autocomplete
                      id="grouped-demo"
                      options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                      groupBy={option => option.firstLetter}
                      getOptionLabel={option => option.domain_name}
                      onChange={(event, value) => this.setSkill(value)}
                      style={{ width: '100%' }}
                      renderInput={params => (
                        <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
                      )}
                    />
                </Paper>
             </Grid>
             <Grid item xs={6} sm={6}>
                       <Paper elevation={2} style={{padding:'10px'}}>
                           <FormControl style={{width:'100%'}}>
                             <InputLabel id="level">Select Level</InputLabel>
                               <Select
                                 labelId="level"
                                 id="level"
                                 value={this.state.option} onChange={this.handleOption}
                               >
                               <MenuItem value="Begineer">Begineer</MenuItem>
                               <MenuItem value="Intermediate">Intermediate</MenuItem>
                               <MenuItem value="Advanced">Advanced</MenuItem>
                               </Select>
                            </FormControl>
                       </Paper>
             </Grid>
       </Grid>
       {(this.state.option && this.state.domain_name) && <div className="row">
         <Gg noChange={this.props.noChange} level={this.state.option}
         domain_name={this.state.domain_name} username={this.props.username}/>
       </div>
       }
     </React.Fragment>
     )
   }

 }
}

class Gg extends Component{
 constructor(props) {
    super(props);
    this.state = {
      redirectTo: null,
      username:'',
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      action:this.props.options,
      saved_skill:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentDidMount(){
    this.fetchSavedSkills()
  }

  fetchSavedSkills =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
    .then(res => {
        if(res.data)
        {
          this.setState({saved_skill:res.data})
        }
    });
  }


  onCreate = (e,index) => {
    this.setState({ isAddProduct: true,product: {}});
  }

  onFormSubmit(data) {
    let apiUrl;
    var addroute,editroute;
      addroute="/ework/user2/add_from_insert_in_admin";
      editroute = "/ework/user2/edit_inserted_data";

    if(this.state.isEditProduct){
      apiUrl = editroute;
    } else {
      apiUrl = addroute;
    }
    axios.post(apiUrl, {data})
        .then(response => {
          this.setState({
            response: response.data,
            isAddProduct: false,
            isEditProduct: false
          })
        })
  }

  editProduct = (productId,index)=> {
    var editProd;
      editProd ="/ework/user2/fetch_for_edit"
    axios.post(editProd,{
      id: productId,
    })
        .then(response => {
          this.setState({
            product: response.data,
            isEditProduct: true,
            isAddProduct: true,
          });
        })

 }

 updateState = () =>{
   this.setState({
     isAddProduct:false,
     isEditProduct:false,
   })
 }


  render() {
    let productForm;

           var  data1 = {
              fielddata: [
                {
                  header: "Refer Id",
                  name: "count",
                  placeholder: "blank",
                },
                {
                  header: "Course or Skill to Learn",
                  name: "course_to_learn",
                  placeholder: "Enter the Course or Skill to Learn",
                },
                {
                  header: "Any Reference Link",
                  name: "ref_link_for_skill",
                  placeholder: "Enter Any Reference Link To Learn",
                },
                {
                  header: "Action",
                  name: "no",
                  placeholder: "blank",
                }
              ],
            };
            if(this.state.isAddProduct || this.state.isEditProduct)
            {
              productForm = <AddProduct d="true"
              saved_skill={this.state.saved_skill} cancel={this.updateState}
              username={this.props.username} action="add-path" for={'Domain-'+this.props.domain_name}
              level={this.props.level}  data={data1} onFormSubmit={this.onFormSubmit}
              product={this.state.product} />
            }

      return (
       <React.Fragment>
       <div>
             {!this.state.isAddProduct &&
              <React.Fragment>
                <Grid container spacing={1}>
                  <Grid item xs={6} sm={6} />
                  <Grid item xs={6} sm={6}>
                   <Button variant="contained" color="secondary" style={{float:'right'}}  onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</Button>
                  </Grid>
                </Grid>
             </React.Fragment>
            }
            <br />
         {!this.state.isAddProduct &&
           <ProductList
            username={this.props.username}
            noChange={this.props.noChange}
            d="true" action="add-path" for={'Domain-'+this.props.domain_name}
            level={this.props.level}  data={data1}  editProduct={this.editProduct}/>
          }
         { productForm }
       </div>
      </React.Fragment>
      );

}
}
