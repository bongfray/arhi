import React, { Component } from 'react';
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


export default class ValidateUser extends Component{
  constructor()
  {
    super()
    this.state={
      display:'none',
      requests:[],
      recheck:[],
      notfound:'',
      isChecked:false,
      username:'',
      faculty_ad:false,
      year_set:false,
      disable_active:true,
      for_year:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchRequests()
  }
  fetchRequests = ()=>{
    axios.post('/ework/user2/fetch_signup_request_for_admin',{user:this.props.user})
    .then(res=>{
        this.setState({display:'block',requests:res.data})
    })
  }

  ActiveFacultyAdvisor =(e,index,username)=>
  {
    this.setState({advisor_on:true,index,year_set:true})
  }

  Active =(e,index,content)=>{
  window.M.toast({html: 'Activating...', classes:'rounded orange'});
  window.M.toast({html: 'Sending Activation Mail...', classes:'rounded orange'});
  this.setState({
        isChecked: !this.state.isChecked,index,
        username: e.target.value,
      })

      axios.post('/ework/user2/active_user',{
        content: content,advisor_on:this.state.advisor_on,for_year:this.state.for_year
      })
      .then(res=>{
            this.setState({advisor_on:false})
            window.M.toast({html: 'Activated !!', classes:'rounded green darken-2'});
            const { requests } = this.state;

                  this.setState({
                    requests: requests.filter(product => product.username !== content.username)
                 })
      })
  }
handleModal=()=>{
  this.setState({year_set:!this.state.year_set})
}
handleInput=(e)=>{
  this.setState({for_year:e.target.value,disable_active:false})
}

  render()
  {
    return(
      <React.Fragment>
      {this.state.year_set &&
        <Dialog
          open={this.state.year_set}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">In Which Year ?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
                <FormControl style={{width:'100%'}}>
                  <InputLabel id="sel_year">Year</InputLabel>
                    <Select
                      labelId="sel_year"
                      id="sel_year"
                      name="year"
                      value={this.state.year}
                      onChange={this.handleInput}
                    >
                      <MenuItem value="" disabled defaultValue>Year</MenuItem>
                      <MenuItem value="1">First Year</MenuItem>
                      <MenuItem value="2">Second Year</MenuItem>
                      <MenuItem value="3">Third Year</MenuItem>
                      <MenuItem value="4">Fourth Year</MenuItem>
                      <MenuItem value="5">Fifth Year</MenuItem>
                    </Select>
                 </FormControl>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleModal} color="primary" autoFocus>
              SUBMIT
            </Button>
          </DialogActions>
        </Dialog>
      }
        <p className="center">You Will able to see the requests only of your department and campus</p>
        <div className="row">
           <div className="col l12">
              <div className="card">
                <div className="card-title center pink white-text">Accept Request</div>
                <div className="card-content">
                  {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                  <div style={{display:this.state.display}}>
                  {this.state.requests.length === 0 ?
                  <h6 className="center">No Request Found</h6>
                  :
                  <React.Fragment>
                  <div className="row">
                    <div className="row">
                         <div className="col l1 left"><b>Index</b></div>
                          <div className="col l2 center"><b>Reg Id</b></div>
                          <div className="col l3 center"><b>Mail Id</b></div>
                          <div className="col l2 center"><b>Sem-Batch</b></div>
                          <div className="col l2 center"><b>Faculty Advisor</b></div>
                          <div className="col l2 center"><b>Action</b></div>
                    </div>
                    <hr />
                     {this.state.requests.map((content,index)=>(
                             <div className="row"  key={index}>
                              <div className="col l1 left">{index+1}</div>
                              <div className="col l2 center">{content.username}</div>
                              <div className="col l3 center">{content.mailid}</div>
                              <div className="col l2 center">{content.sem}-{content.batch}</div>
                              <div className="col l2 center">{content.faculty_adviser_id}</div>
                              <div className="col l2 center">
                                  <div className="switch">
                                    <label>
                                      <input checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content)}} type="checkbox" />
                                      <span className="lever"></span>
                                    </label>
                                  </div>
                              </div>
                            </div>
                     ))}
                   </div>
                  </React.Fragment>
                }
                </div>
                </div>
              </div>
           </div>
        </div>

      </React.Fragment>
    )
  }
}
