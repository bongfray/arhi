import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import Nav from '../../dynnav'

import StudentList from './studentList'
import ValidateUser from './validateUser'
import Manage from '../admin/Operations/SUPER_OPERATIONS/handleRerender_bluePrint'



export default class DepartmentAdmin extends Component {
  constructor()
  {
    super()
    this.state={
      radio:[{name:'radio2',value:'Validate User'},{name:'radio2',value:'Student Under You'},
      {name:'radio3',value:'Re-render BluePrint'}],
      isChecked: false,
      choosed: '',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      login:'/ework/flogin',
      noti_route:true,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In', classes:'rounded red'});
        }
        else {
          this.setState({user:response.data.user,loader:false})
        }
      })
    }
    handleChecked =(e,index,color)=>{
        this.setState({
            isChecked: !this.state.isChecked,
            choosed: e.target.value
        });
    }


  render() {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else
    {

       if(this.state.loader)
       {
         return(
           <div className="center">Loading....</div>
         )
       }
       else{
          return(
              <React.Fragment>
              <Nav noti_route={this.state.noti_route} home={this.state.home} login={this.state.login} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
             <div className="row">
                <div className="col s12 m12 l2 xl2">
                  {this.state.radio.map((content,index)=>(
                      <div key={index}>
                          <div className="col l12 s12 m12 xl12 form-signup">
                          <p>
                          <label>
                          <input className="with-gap" type='radio' id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                          <span style={{color:'green'}}><b>{content.value}</b></span>
                          </label>
                          </p>
                          </div>
                    </div>
                  ))}
                  </div>
                  <div className="col s12 m12 l10 xl10">
                     <Display user={this.state.user} choosed={this.state.choosed}/>
                  </div>
             </div>

                  <div className="row">

                  </div>
              </React.Fragment>
          );
        }
}
  }
}



class Display extends Component {
  constructor() {
    super()
    this.state={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    let content;
    if(this.props.choosed === 'Student Under You')
    {
      content = <StudentList user={this.props.user} />
    }
    else if(this.props.choosed === 'Validate User')
    {
      content =<ValidateUser user={this.props.user} />
    }
    else if(this.props.choosed === 'Re-render BluePrint')
    {
      content =<Manage choice={"student"} user={this.props.user} student={true} />
    }
    else{
      content = <div></div>
    }
    return(
      <React.Fragment>
       {content}
      </React.Fragment>
    );
  }
}
