import React, { Component } from 'react';
import axios from 'axios'
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';
import ViewStudentData from './view_single_student'


export default class StundetList extends Component{
  constructor()
  {
    super()
    this.state={
      students:'',
      details_view:false,
      data:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStudentList();
  }

  fetchStudentList=()=>{
    axios.post('/ework/user2/fetch_student_list_under_faculty_adviser',{user:this.props.user})
  .then( res => {
      if(res.data)
      {
        this.setState({students:res.data})
      }
  });
  }

closeView=()=>{
  this.setState({details_view:false})
}

showDetails=(content)=>{
  this.setState({data:content,details_view:true})
}

  render()
  {
    return(
      <React.Fragment>
      {this.state.details_view  &&
          <ViewStudentData details_view={this.state.details_view}  closeView={this.closeView} data={this.state} />
      }
      <div className="row">
         <div className="col l12">
            <div className="card">
              <div className="card-title center pink white-text">Student List</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.students.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                       <div className="col l1 left"><b>Index</b></div>
                        <div className="col l2 center"><b>Reg Id</b></div>
                        <div className="col l2 center"><b>Mail Id</b></div>
                        <div className="col l2 center"><b>Year - Sem - Batch</b></div>
                        <div className="col l3 center"><b>campus - Department</b></div>
                        <div className="col l2 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.students.map((content,index)=>(
                       <React.Fragment key={index}>
                           <div className="row" >
                            <div className="col l1 left">{index+1}</div>
                            <div className="col l2 center">{content.username}</div>
                            <div className="col l2 center">{content.mailid}</div>
                            <div className="col l2 center"> {content.year} - {content.sem} - {content.batch} </div>
                            <div className="col l3 center">{content.campus} - {content.dept}</div>
                            <div className="col l2 center">
                             <Tooltip title="View Details">
                              <VisibilityIcon onClick={()=>this.showDetails(content)} />
                             </Tooltip>
                            </div>
                          </div>
                          <hr />
                        </React.Fragment>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
      </React.Fragment>
    )
  }
}
