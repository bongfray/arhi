import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import RegistrationStopped from '../FACULTY_FRONT/stop'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Typography from '@material-ui/core/Typography';
import ContentLoader from "react-content-loader"





export default class SSignup extends Component {
	constructor() {
    super()
    this.initialState = {
		loader:true,
		status:"none",
		disable_signup:false,
		redirectTo: null,
		color:'green-text',
		enable:false,
		faculty_adviser:'',
		department:'',
    }
		this.state = this.initialState;
		this.componentDidMount = this.componentDidMount.bind(this)
		this.handleInput = this.handleInput.bind(this)
	}
	handleInput = (e) =>{
		this.setState({[e.target.name]:e.target.value})
	}
		componentDidMount(){
			axios.post('/ework/user2/stud_status')
	        .then(response => {
						if(response.data)
						{
							var stud_reg_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="SIGNUP STATUS")));
							if((stud_reg_status.length)>0)
							{
								if(stud_reg_status[0].status === true)
								{
									this.setState({status:'block'})
								}
								else
								{
									this.setState({loader:false})
									this.setState({status:'none'})
								}
							}
							else
							{
								this.setState({status:'block'})
							}
						}
						else
						{
							this.setState({status:'block'})
						}
						this.fetchDesig();
	        })
		}

		fetchDesig =() =>{
			axios.post('/ework/user/fetch_designation')
			.then(res => {
					if(res.data)
					{
							const department = res.data.filter(item =>item.action ==='Department');
						this.setState({department:department,loader:false})
					}
			});
		}


	handleSubmit=(event)=> {
		console.log(this.state)
		var verify = this.state.regid;
		var vermail = this.state.mailid;
		event.preventDefault()
		if(!(this.state.year)||!(this.state.degree)||!(this.state.name)
		||!(this.state.mailid)||!(this.state.regid)||!(this.state.phone)
		||!(this.state.password)||!(this.state.cnf_pswd)||!(this.state.campus)
		||!(this.state.dept))
		{
			window.M.toast({html: 'Enter all the Details',classes:'rounded red'});
      return false;
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
			window.M.toast({html: 'Password does not match !!',classes:'rounded red'});
	     return false;
	  }
	  else if(!verify.includes('RA'))
	  {
			window.M.toast({html: 'Enter Registration Number starting with RA !!',classes:'rounded red'});
		return false;
	  }
	  else if(verify.length!==15)
	  {
				window.M.toast({html: 'Registration Number should be of 15 digit !!',classes:'rounded red'});
		return false;
	  }
	  else if((!vermail.includes('srmuniv.edu.in'))&&(!vermail.includes('srmist.edu.in')))
	  {
			window.M.toast({html: 'Enter official SRM Mail Id Please!!',classes:'rounded red'});
	  	return false;
	  }
		else if ((this.state.phone).length!==10)
		{
			window.M.toast({html:'Enter correct format of Phone no !!',classes:'rounded red'});
			return false;
		}
		else
		{
		this.setState({disable_signup:true,enable:true})

	}
}


signedUP=(faculty_adviser)=>{
	if(!faculty_adviser)
	{
			window.M.toast({html:'Enter Faculty Adviser Name !!',classes:'red'});
	}
	else
	{
	this.closeEnable();
	window.M.toast({html:'Signing Up....',classes:'rounded orange'});
	axios.post('/ework/user2/ssignup', {
		username: this.state.regid,
		password: this.state.password,
		name: this.state.name,
		mailid: this.state.mailid,
		phone: this.state.phone,
		campus: this.state.campus,
		dept: this.state.dept,
		batch: this.state.batch,
		degree: this.state.degree,
		sem: this.state.sem,
		year: this.state.year,
		count: 4,
		faculty_adviser_id:faculty_adviser,
		suspension_status:false,
		active:false,
		render_timetable:false,
		priority:0,
		nightmode:false,
		resetPasswordExpires:'',
		resetPasswordToken:'',
	})
		.then(response => {
			if(response.status===200){
				if(response.data.emsg)
				{
				window.M.toast({html: response.data.emsg,classes:'rounded #ba68c8 purple lighten-2'});
						this.setState(this.initialState);
				}
				else if(response.data.succ)
				{
					window.M.toast({html: response.data.succ,classes:'rounded #ba68c8 purple lighten-2'});
					this.setState({
							redirectTo: '/ework/slogin'
					})
				}
			}
		}).catch(error => {
			window.M.toast({html: 'Internal Error',classes:'rounded #ec407a pink lighten-1'});
		})
	this.setState({loader:false})
  }
}


showValidation=()=>{
	this.setState({show_modal:!this.state.show_modal})
}

closeEnable=()=>{
	this.setState({enable:!this.state.enable})
}

render() {
	const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
        </ContentLoader>
      )
			if (this.state.redirectTo) {
					 return <Redirect to={{ pathname: this.state.redirectTo }} />
			 } else {
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status ==="none")
	{
		return(
			<RegistrationStopped login_path="/ework/slogin" section_name="STUDENT REGISTRATION"/>
		);
	}
	else{
	return (
	  <React.Fragment>
		{this.state.enable &&
			 <FacultyAdviser closeEnable={this.closeEnable}
			 signUP={this.signedUP} state={this.state} />
	   }

		<div className="row">

		<div className="col s2 l2 m2 xl2" />

		<div className="col l8 s12 m12 form-signup">
				<div className="center">
						<h5 className="reg">Student Registration</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">

								<div className="input-field col s6 l6 xl6 m6">
								<input id="name" type="text" className="validate" name="name" value={this.state.name} onChange={this.handleInput} required />
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s6 xl6 l6 m6">
								<input id="stud_id" type="text" className="validate" name="regid" value={this.state.regid} onChange={this.handleInput} required />
								<label htmlFor="stud_id">Registration Number</label>
								</div>
						</div>



						<div className="row">
								<div className="input-field col s5 l5 xl5 m5">
								<input id="email" type="email" className="validate" name="mailid" value={this.state.mailid} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Only official mail id is allowed</span>
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="ph_num" type="number" className="validate" name="phone" value={this.state.phone} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Enter Correct Format of Phone no</span>
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="col s3 l3 xl3 m3">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_degree">Degree</InputLabel>
												<Select
													labelId="sel_degree"
													id="sel_degree"
													name="degree"
													value={this.state.degree}
													onChange={this.handleInput}
												 >
														<MenuItem value="" disabled defaultValue>Year</MenuItem>
														<MenuItem value="B.tech">B.Tech</MenuItem>
														<MenuItem value="M.tech">M.Tech</MenuItem>
														<MenuItem value="BCA">BCA</MenuItem>
														<MenuItem value="MCA">MCA</MenuItem>
												</Select>
										 </FormControl>
								</div>

						</div>

						<div className="row">

								<div className="input-field col s6 l6 xl6 m6">
								<input onChange={this.handleInput} name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								<div>
								<HelpOutlineIcon style={{color:'green'}} onClick={this.showValidation} />
								 <Dialog
		               open={this.state.show_modal}
									 onClose={this.showValidation}
		               aria-labelledby="alert-dialog-title"
		               aria-describedby="alert-dialog-description"
		             >
		               <DialogTitle id="alert-dialog-title">Password Should Consist of </DialogTitle>
		               <DialogContent>
		                 <DialogContentText id="alert-dialog-description">
											 <Typography variant="button" display="block" gutterBottom>
											 At least 1 uppercase character.<br />
											 At least 1 lowercase character.<br />
											 At least 1 digit.<br />
											 At least 1 special character.<br />
											 Minimum 6 characters.<br />
								       </Typography>
		                 </DialogContentText>
		               </DialogContent>
		               <DialogActions>
		                 <Button onClick={this.showValidation} color="primary" autoFocus>
		                   OK
		                 </Button>
		               </DialogActions>
		             </Dialog>
								</div>
								</div>

								<div className="input-field col s6 l6 xl6 m6">
								<input onChange={this.handleInput} id="cnf_pswd" value={this.state.cnf_pswd} name="cnf_pswd" type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>

						</div>

						<div className="row">
								<div className="col s4 l4 xl4 m4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_year">Year</InputLabel>
												<Select
													labelId="sel_year"
													id="sel_year"
													name="year"
													value={this.state.year}
													onChange={this.handleInput}
												>
													<MenuItem value="" disabled defaultValue>Year</MenuItem>
													<MenuItem value="1">First Year</MenuItem>
													<MenuItem value="2">Second Year</MenuItem>
													<MenuItem value="3">Third Year</MenuItem>
													<MenuItem value="4">Fourth Year</MenuItem>
													<MenuItem value="5">Fifth Year</MenuItem>
												</Select>
										 </FormControl>
								</div>

								<div className="col s4 l4 xl4 m4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_sem">Semester</InputLabel>
												<Select
													labelId="sel_sem"
													id="sel_sem"
													name="sem"
													value={this.state.sem}
													onChange={this.handleInput}
												>
													<MenuItem value="" disabled defaultValue>Year</MenuItem>
													<MenuItem value="1">1</MenuItem>
													<MenuItem value="2">2</MenuItem>
													<MenuItem value="3">3</MenuItem>
													<MenuItem value="4">4</MenuItem>
													<MenuItem value="5">5</MenuItem>
													<MenuItem value="6">6</MenuItem>
													<MenuItem value="7">7</MenuItem>
													<MenuItem value="8">8</MenuItem>
													<MenuItem value="9">9</MenuItem>
													<MenuItem value="10">10</MenuItem>
												</Select>
										 </FormControl>
								</div>

								<div className="col s4 l4 xl4 m4">
									<FormControl style={{width:'100%'}}>
										<InputLabel id="sel_batch">Batch</InputLabel>
											<Select
												labelId="sel_batch"
												id="sel_batch"
												name="batch"
												value={this.state.batch}
												onChange={this.handleInput}
											>
											<MenuItem value="B1">B-1</MenuItem>
											<MenuItem value="B2">B-2</MenuItem>
											</Select>
									 </FormControl>
								</div>

						</div>
						<div className="row">

								<div className="col xl6 l6 m6 s6">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_campus">Campus</InputLabel>
												<Select
													labelId="sel_campus"
													id="sel_campus"
													name="campus"
													value={this.state.campus}
													onChange={this.handleInput}
												>
												<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
												<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
												<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
												<MenuItem value="NCR Campus">NCR Campus</MenuItem>
												</Select>
										 </FormControl>
								</div>
								<div className="col xl6 l6 s6 m4">
									<FormControl style={{width:'100%'}}>
										<InputLabel id="dept">Department</InputLabel>
											<Select
												labelId="dept"
												id="dept"
												value={this.state.dept} name="dept" onChange={this.handleInput}
											>
														{this.state.department.map((content,index)=>{
															return(
																<MenuItem key={index} value={content.department_name}>{content.department_name}</MenuItem>
															)
												})}
												</Select>
										</FormControl>
								</div>
						</div>
						<br/>
						<div className="row"><div className="col l4 m4 s4 xl4 left">
							<Link to='/ework/slogin' className="log">Login Instead ?</Link></div>
							<div className="col l4 s4 xl4 m4"/>
							<div className="col l4 s4 m4 xl4">
							<button disabled={this.state.disable_signup} className="waves-effect btn blue-grey darken-2 sup" style={{width:'100%'}} onClick={this.handleSubmit}>Submit</button>
							</div>
						</div>
				</form>
		</div>
		</div>

		</React.Fragment>
	);
}
	}
}
}
}



class FacultyAdviser extends Component {
	constructor()
	{
		super()
		this.state={
			adviser:'',
			selected:'',
		}
		this.componentDidMount= this.componentDidMount.bind(this)
	}
	componentDidMount()
	{
this.fetchDatas();
	}

	fetchDatas=()=>{
		axios.post('/ework/user/fetch_faculty_adviser',{datas:this.props.state})
		.then( res => {
				if(res.data)
				{
					console.log(res.data)
					this.setState({adviser:res.data})
				}
		});
	}

handleField=(e)=>{
	this.setState({selected:e.target.value})
}

  render() {
		console.log(this.props.state)
    return (
      <React.Fragment>
					<Dialog
						open={this.props.state.enable}
						aria-labelledby="alert-dialog-title"
						aria-describedby="alert-dialog-description"
					>
						<DialogTitle id="alert-dialog-title">Please Select Your Faculty Adviser</DialogTitle>
						<DialogContent>
							<DialogContentText id="alert-dialog-description">
									{this.state.adviser.length>0 ?
										<FormControl style={{width:'100%'}}>
										<InputLabel id="dept">Faculty Adviser</InputLabel>
											<Select
												labelId="adviser"
												id="adviser"
												value={this.state.selected} name="adviser" onChange={this.handleField}
											>
														{this.state.adviser.map((content,index)=>{
															return(
																<MenuItem key={index} value={content.username}>{content.username} - {content.name}</MenuItem>
															)
												})}
												</Select>
										</FormControl>
										:
										<div className="center">No Data Found !!</div>
									}
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button onClick={()=>this.props.signUP(this.state.selected)} color="primary" autoFocus>
								SUBMIT
							</Button>
						</DialogActions>
					</Dialog>
      </React.Fragment>
    );
  }
}
