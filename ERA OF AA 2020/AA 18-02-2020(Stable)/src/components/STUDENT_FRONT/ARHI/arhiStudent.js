import React, { Component } from 'react';
import Rating from '@material-ui/lab/Rating';
import axios from 'axios'
import Nav from '../../dynnav'
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class ARHI extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      noti_route:false,
      searched_domain:'',
      selected_domain:'',
      list_of_domains:[],
      detail_domain_name:'',
      display_details:false,
      user:'',
      loader:true,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
   this.getUser();
  }
  getUser =()=>
  {
    axios.get('/ework/user2/getstudent').then(response =>{
      if (response.data.user)
      {
        this.setState({user:response.data.user})
        this.fetch_domains();
      }
      else{

      }
     })
  }
  fetch_domains =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Domain-For-Student'})
    .then( response => {
        this.fetch_selected();
        if(response){
          this.setState({fetched_domain:response.data})
        }
    });
  }
  fetch_selected=()=>{
    axios.get('/ework/user2/fetch_domains')
    .then( res => {
      this.setState({loader:false})
        if(res.data){
          this.setState({list_of_domains:res.data})
        }
    });
  }
  search =(e)=>{
    this.setState({searched_domain:e.target.value})
  }

  selected =(data)=>{
    if(data === null)
    {

    }
    else{
      this.setState({
        selected_domain:data.domain_name,
      })
      axios.post('/ework/user2/entry_domain',{domain:data.domain_name,user:this.state.user})
      .then( res => {
          if(res.data ==='have'){
            window.M.toast({html: 'Already Present !!',classes:'rounded #ec407a pink lighten-1'});
          }
          else if(res.data === 'done'){
            window.M.toast({html: 'Saved !!',classes:'rounded green darken-2'});
            this.setState({searched_domain:''})
            this.fetch_selected();
          }
      });
    }
  }

  showDivOpen=()=>{
    this.setState({display_details:!this.state.display_details})
  }

  postDetail =(detail_domain_name)=>{
    this.showDivOpen();
    this.setState({detail_domain_name:detail_domain_name})
  }

  render() {
    if(this.state.loader)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress color="secondary" />
        </Backdrop>
      );
    }
    else{
    const options = this.state.fetched_domain.map(option => {
      const firstLetter = option.domain_name[0].toUpperCase();
      return {
        firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
        ...option,
      };
    });


    return (
      <React.Fragment>
        <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
           <Grid container spacing={1}>
             <Grid item xs={4} sm={4}  xl={4}/>
             <Grid item xs={4} xl={4}sm={4} >
                <Grid container item xs={12} sm={12} xl={12} md={12}>
                   <Grid item  xs={12} sm={12} xl={12} md={12}>
                     <Autocomplete
                       id="grouped-demo"
                       options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                       groupBy={option => option.firstLetter}
                       getOptionLabel={option => option.domain_name}
                       onChange={(event, value)=>this.selected(value)}
                       style={{ width: '100%' }}
                       renderInput={params => (
                         <TextField {...params} label="Search Domain" variant="outlined" fullWidth />
                       )}
                     />
                   </Grid>
                </Grid>
             </Grid>
             <Grid item xs={4} sm={4} xl={4} />
           </Grid>

           <Grid container spacing={1} style={{padding:'8px'}}>
              {this.state.list_of_domains.map((content,index)=>(
                <Grid item xs={2} sm={2} style={{borderRadius:'5px',minHeight:'60px'}} key={index} onClick={()=>this.postDetail(content.domain_name)}>
                    <Paper elevation={3} style={{padding:'2px'}}>
                        <Grid container spacing={1}>
                           <Grid item xs={10}>
                              <Typography variant="overline" display="block" style={{fontSize:'15px',textAlign:'center'}} gutterBottom>
                               {content.domain_name}
                              </Typography>
                           </Grid>
                           <Grid item xs={2} sm={2}>
                              <CloseIcon className="go" />
                           </Grid>
                        </Grid>
                    </Paper>
                </Grid>

              ))}
           </Grid>
           <Detailer display_details={this.state.display_details} showDivOpen={this.showDivOpen} detail_domain_name={this.state.detail_domain_name} />
      </React.Fragment>
    );
   }
  }
}

class Detailer extends Component {
  constructor() {
    super()
    this.state={
      isChecked:'Begineer',
      active:'Begineer',
      toggled:false,
      instructions:[],
      fetch_order:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetch_Domain_Level()
  }
  componentDidUpdate =(prevProps,prevState)=>{
    if(prevState.isChecked!==this.state.isChecked){
      this.fetch_Domain_Level()
    }
    if(prevProps.detail_domain_name!== this.props.detail_domain_name){
      this.fetch_Domain_Level()
    }
  }
  fetch_Domain_Level =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Domain-'+this.props.detail_domain_name,usertype:this.state.isChecked})
    .then( res => {
        if(res.data){
          this.setState({instructions:res.data})
        }
    });
  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "pink go white-text";
      }
      return "go";
  }

  render(){
    return(
      <React.Fragment>
         {this.props.detail_domain_name &&
             <React.Fragment>
             <Dialog fullScreen open={this.props.display_details} onClose={this.props.showDivOpen} TransitionComponent={Transition}>
                 <AppBar>
                   <Toolbar>
                    <Grid style={{marginTop:'55'}}container spacing={1}>
                      <Grid item xs={2} sm={2}>
                         <IconButton edge="start" color="inherit" onClick={this.props.showDivOpen} aria-label="close">
                           <CloseIcon />
                         </IconButton>
                      </Grid>
                      <Grid item xs={8} sm={8}>
                        <div style={{textAlign:'center',fontSize:'25px'}}>
                         Selected Domain <span className="yellow-text">{this.props.detail_domain_name}(<Rating name="read-only" value={2} readOnly />)</span>
                        </div>
                      </Grid>
                      <Grid item xs={2} sm={2}/>
                    </Grid>
                   </Toolbar>
                 </AppBar>
                 <List style={{marginTop:'70px'}}>

                 <div style={{marginRight:'7px',marginLeft:'7px',borderRadius:'5px'}}>
                     <div className="row">
                         <div className="col l3 xl3 m4 s9">
                            <div className="row div-style">
                              <div className={'col l4 xl4 s4 m4 center particular '+this.color('Begineer')} onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</div>
                              <div className={'col l4 xl4 s4 m4 center particular '+this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</div>
                              <div className={'col l4 xl4 s4 m4 center particular '+this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</div>
                            </div>
                         </div>
                         <div className="col l7 xl7 hide-on-small-only m4"/>
                         <div className="col l2 xl2 s3 m4"/>
                     </div>
                     <div className="row head">
                        <div className="center col l3 xl3 s3 m3"><b>LEARN</b></div>
                        <div className="center col l3 xl3 s3 m3"><b>LINK TO REFER</b></div>
                        <div className="center col l3 xl3 s3 m3"><b>TASK COMPLETED</b></div>
                        <div className="center col l3 xl3 s3 m3"><b>RATING</b></div>
                     </div>
                     <hr />
                     <div className="body">
                       {this.state.instructions.map((content,index)=>(
                         <React.Fragment key={index}>
                           <div className="row">
                              <div className="center col l3 xl3 s3 m3">{content.course_to_learn}</div>
                              <div className="center col l3 xl3 s3 m3">
                                <div className="hide-on-med-and-down"><a  rel="noopener noreferrer" href={content.ref_link_for_skill} target="_blank">{content.ref_link_for_skill}</a></div>
                                <div className="hide-on-large-only"><a rel="noopener noreferrer" href={content.ref_link_for_skill} target="_blank">LINK</a></div>
                              </div>
                              <div className="center col l3 xl3 s3 m3"><TaskCompleted course={content.course_to_learn} /></div>
                              <div className="center col l3 xl3 s3 m3"><RatingS course={content.course_to_learn} /></div>
                           </div>
                           <hr />
                         </React.Fragment>
                       ))}
                     </div>
                 </div>
                 {this.state.fetch_order &&
                     <CallsFromUser fetch_order={this.state.fetch_order} fetchMethod={this.fetchMethod} />
                 }
                 </List>
                </Dialog>

             </React.Fragment>
         }
      </React.Fragment>
    )
  }
}


class TaskCompleted extends Component {
  constructor(){
    super()
    this.state={
      total:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetchCompletationHistory()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props){
      this.fetchCompletationHistory()
    }
  }
  fetchCompletationHistory =()=>{
    axios.post('/ework/user2/fetchCompletationHistory',{course:this.props.course})
    .then( res => {
        if(res.data === 'No Data'){
          this.setState({total:res.data})
        }
        else{
          this.setState({total:res.data.length})
        }
    });
  }
  render() {
    return (
      <div>
        {this.state.total}
      </div>
    );
  }
}

class RatingS extends Component {
  constructor(){
    super()
    this.state={
      rating:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.fetchRating()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props){
      this.fetchRating()
    }
  }
  fetchRating =()=>{
    axios.post('/ework/user2/fetchRating',{course:this.props.course})
    .then( res => {
      console.log(res.data)
        if(res.data){
          this.setState({rating:res.data})
        }
    });
  }
  render() {
    return (
      <Rating name="read-only" value={this.state.rating} readOnly />
    );
  }
}


class CallsFromUser extends Component{
  constructor(props)
  {
    super(props)
    this.state={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render(){
    return(
      <Dialog
        open={this.props.fetch_order}
        onClose={this.props.fetchMethod}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Notification From </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Call For Project
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.fetchMethod} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
