import React, { Component } from 'react';
import { Select } from 'react-materialize';
import axios from 'axios';

export default class Action extends Component {
  constructor(){
    super()
    this.state={
      username:'',
      task:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){

  }
  handleField=(e)=>{
    this.setState({[e.target.name]:e.target.value})
  }
putSuprimity=()=>{
  if((!this.state.username)||(!this.state.task))
  {
    window.M.toast({html: 'Enter all the Details !!', classes:'rounded red darken-2'});
  }
  else{
    window.M.toast({html: 'Sending the Mail !!', classes:'rounded green lighten-1'});
    axios.post('/ework/user/assign_suprimity',{data:this.state})
    .then( res => {
        if(res.data==='ok'){
          window.M.toast({html: 'Successfully Inserted  !!', classes:'rounded green lighten-1'});
        }
        else if(res.data==='no')
        {
          window.M.toast({html: 'No User Found !!', classes:'rounded red darken-2'});
        }
        else if(res.data==='have')
        {
          window.M.toast({html: 'Already Assigned !!', classes:'rounded red darken-2'});
        }
    });
  }
}
  render() {
    return (
      <React.Fragment>
          <div className="row">
          <div className="col l12">
          <div className="input-field col l6">
          <input id="username" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleField} required />
          <label htmlFor="username">Enter Offical Id</label>
          </div>
          <div className="col l6">
                <Select value={this.state.task} name="task" onChange={this.handleField}>
                <option value="" disabled defaultValue>Task Type</option>
                <option value="add-skill">Add Skill</option>
                <option value="add-domain">Add Domain</option>
                <option value="achieve-domain">Add Path For Domain</option>
                </Select>
          </div>
          <button className="waves-effect btn #37474f blue-grey darken-3 col l2 right" onClick={this.putSuprimity}>Assign</button>
          </div>
          </div>
      </React.Fragment>
    );
  }
}
