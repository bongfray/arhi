import React, {} from 'react'
import { } from 'react-router-dom'
import M from 'materialize-css'
import { Redirect} from 'react-router-dom'
import axios from 'axios'
import DeciderPath from './decider_type'






 export default class ViewSubject extends React.Component{
      _isMounted = false;
  constructor()
  {
    super()
    this.state={
      loading:true,
      option:'',
      username:'',
      redirectTo:'',
      modal: false,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
 componentDidMount()
 {
   this._isMounted = true;
   this.getUser();
   M.AutoInit()
 }
 getUser()
 {
   axios.get('/ework/user/'
 )
    .then(response =>{
     if (this._isMounted) {
       if(response.status === 200)
       {
         this.setState({
           loading:false,
         })
      if(response.data.user)
      {

      }
      else{
        this.setState({
          redirectTo:'/ework/faculty',
        });
        window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    }
    }
    })
 }



 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }
 render()
 {
   if (this.state.redirectTo)
   {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
 }
  else
   {
   return(
     <React.Fragment>
        <DeciderPath display="none" username={this.state.username} showAction="localuser" />
     </React.Fragment>
 );
}
 }
}
