import React from 'react';
import ReactApexChart from 'react-apexcharts'
import Axios from 'axios'

class Chart extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
          resume_data:'',
        series: [{
          data: []
        }],
        options: {
          chart: {
            type: 'bar',
            height: 350
          },
          plotOptions: {
            bar: {
              vertical: true,
            }
          },
          dataLabels: {
            enabled: false
          },
          xaxis: {
            categories: [],
          }
        },


      };
      this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        Axios.post('/ework/user2/fetchall',{
            action:'Skills',
            username:this.props.prop_data.data.username,
        })
        .then(res=>{
            console.log(res.data)
            if(res.data)
            {
                var skill_n=[];
                var rating = [];
                var skills = res.data.filter(function(item){

                   skill_n.push(item.skill_name)
                   rating.push(parseInt(item.skill_rating))
                })
                console.log(rating)

                let a = this.state.series.slice();
                a[0].data = rating;
                this.setState({options:{xaxis:{categories:skill_n}},series:a})
            }
        })
    }

    render() {
        console.log(this.state)
      return (
            <React.Fragment>
                <div className="row">

                    <div className="col s3"></div>

                    <div className="chart col s6">

                        <ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={350} />

                    </div>

                    <div className="col s3"></div>

                </div>

            </React.Fragment>
);
}
}

export default Chart;
