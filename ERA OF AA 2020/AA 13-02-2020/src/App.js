import React, { Component } from 'react';
import { Route, Switch} from 'react-router-dom'
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';

import NotFound from './components/ERROR/Not-Found'
import FiveYear from './components/FACULTY_FRONT/FIVEYEAR/fiveyear'
import Decider from './components/STUDENT_FRONT/Decider'
import  Section from './components/FACULTY_FRONT/Section/Gg'
import Initial from './Navigator'
import FSignup from './components/FACULTY_FRONT/sign-up'
import SSignup from './components/STUDENT_FRONT/ssignup'
import FLoginForm from './components/FACULTY_FRONT/login-form'
import SLoginForm from './components/STUDENT_FRONT/slogin'
import FacultyHome from './components/FACULTY_FRONT/Home'
import StudentHome from './components/STUDENT_FRONT/Home_stud'
import Dash from './components/FACULTY_FRONT/DashBoard/Dashboard'
import Faculty_Profile from './components/FACULTY_FRONT/Profile/exdash'
import Sprofile from './components/STUDENT_FRONT/studentProfile'
import Con from './components/online-offline/Connection'
import Scroll from './components/Top/scroll_top'
import YesterdayTimeTable from './components/FACULTY_FRONT/TimeTable/yesterday_slot'
import TodayTimeTable from './components/FACULTY_FRONT/TimeTable/time2'
import Manage from './components/FACULTY_FRONT/Manage/manage'
import FProfileData from './components/FACULTY_FRONT/Profile/Degree'
import Master from './components/FACULTY_FRONT/master/mater'
import BluePrint from './components/FACULTY_FRONT/Schedule/Timetable'
import ResetPass from './components/Reset/reset_password'
import AdminAuth from './components/FACULTY_FRONT/admin/auth'
import Panel from './components/FACULTY_FRONT/admin/admin_operations'
import ViewSelect from './components/FACULTY_FRONT/VIEW/view_select'
import ViewOwn from './components/FACULTY_FRONT/VIEW/view_own'
import ViewSuper from './components/FACULTY_FRONT/VIEW/view_super'
import Extra from './components/FACULTY_FRONT/EXTRA/own_extra'
import DeptAdmin from './components/FACULTY_FRONT/admin/deptadmin'
import DeptAdmin_Panel from './components/FACULTY_FRONT/admin/department_admin_panel'
import Suprimity from './components/FACULTY_FRONT/suprimity'
import FacultyAdviser from './components/FACULTY_FRONT/FACULTY ADVISER/faculty_adviser'

import Check from './components/server_check'

/*------------------Student Part------------------*/
import Resume from './components/STUDENT_FRONT/resume'
import TaskManager from './components/STUDENT_FRONT/ARHI/taskManage'
import Arhi from './components/STUDENT_FRONT/ARHI/arhiStudent'
import TimeTableS from './components/STUDENT_FRONT/Timetable/slotdetails'

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">


<Switch>
        {/* <Con /> */}
      {/*  <Route path="*" component={NotFound}/>*/}
        <Route exact path="/" render={() =>
          <Initial
         />}
       />

        <Route path="/ework/faculty" render={() =>
          <FacultyHome
         />}
       />

       <Route exact path="/ework/student" render={() =>
         <StudentHome
        />}
      />

        <Route path="/ework/flogin" render={() =>
          <FLoginForm
         />}
       />

       <Route path="/ework/slogin" render={() =>
         <SLoginForm
        />}
      />
       <Route path="/ework/fsignup" component={FSignup} />
       <Route path="/ework/ssignup" component={SSignup} />

       <Route path="/ework/fprofile" component={Faculty_Profile} />
       <Route path="/ework/sprofile" component={Sprofile} />


       <Route path="/ework/partA" render={() =>
         <Section
        />} />


       <Route path="/ework/fprofdata" render={() =>
         <FProfileData
        />} />

       <Route path="/ework/dash" render={() =>
         <Dash
        />} />
        <Route path="/ework/master" component={Master} />
        <Route path="/ework/view" component={ViewSelect} />
        <Route path="/ework/view_super" component={ViewSuper} />
        <Route path="/ework/view_own" component={ViewOwn} />

         <Route path="/ework/blue_print" component={BluePrint} />
         <Route path="/ework/reset_password/:token" component={ResetPass} />


          <Route path="/ework/time_new" render={() =>
            <TodayTimeTable
           />} />
           <Route path="/ework/time_yes" render={() =>
             <YesterdayTimeTable
            />} />

            <Route path="/ework/arhi" component={Decider} />

            <Route path="/ework/admin" component={AdminAuth} />
            <Route path="/ework/admin_panel" component={Panel} />

            <Route path="/ework/manage" component={Manage} />
              <Route path="/ework/five" component={FiveYear} />
              <Route path="/ework/extra" component={Extra} />
              <Route path="/ework/suprimity_admin" component={Suprimity} />
              <Route path="/ework/faculty_adviser" component={FacultyAdviser} />




              <Route path="/ework/department_admin" component={DeptAdmin} />
              <Route path="/ework/dadmin_panel" component={DeptAdmin_Panel} />
              <Route path="/ework/check" component={Check} />

             <Route path="/ework/resume" component={Resume} />
             <Route path="/ework/task_student" component={TaskManager} />
             <Route path="/ework/arhi_student" component={Arhi} />
             <Route path="/ework/timetables" component={TimeTableS} />

</Switch>
         <Scroll/>
      </div>
    );
  }
}

export default App;
