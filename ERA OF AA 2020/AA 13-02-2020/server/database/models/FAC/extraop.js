const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const extra = new Schema({
  day_order: { type: String, unique: false, required: false },
  week: { type: String, unique: false, required: false },
  date: { type: String, unique: false, required: false },
  month: { type: String, unique: false, required: false },
  year: { type: String, unique: false, required: false },
  order_from: { type: String, unique: false, required: false },
  faculty_id: { type: String, unique: false, required: false },
  hour: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  slot: { type: String, unique: false, required: false },
  for_year: { type: String, unique: false, required: false },
  for_sem: { type: String, unique: false, required: false },
  for_batch: { type: String, unique: false, required: false },
  compense_faculty: { type: String, unique: false, required: false },
  problem_compensation_date: { type: String, unique: false, required: false },
  problem_compensation_hour: { type: String, unique: false, required: false },
  problem_compensation_day_order: { type: String, unique: false, required: false },
  accepted: { type: String, unique: false, required: false },
})



extra.plugin(autoIncrement.plugin, { model: 'Extra', field: 'serial', startAt: 1,incrementBy: 1 });

const Extra = mongoose.model('Request-For-ExtraHour', extra)
module.exports = Extra
