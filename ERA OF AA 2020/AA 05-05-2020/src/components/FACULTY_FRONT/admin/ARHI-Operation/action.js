import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import {Paper,TextField} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



export default class SetAction extends Component
{
  render()
  {
    if(this.props.select==='add-suprimity')
    {
      return(
        <Action />
      )
    }
    else {
      return(
              <div></div>
      )
    }
  }
}

 class Action extends Component {
  constructor(){
    super()
    this.state={
      username:'',
      task:'',
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
  }

  handleField=(e)=>{
    this.setState({[e.target.name]:e.target.value})
  }
putSuprimity=()=>{
  if((!this.state.username)||(!this.state.task))
  {
    this.setState({
      snack_open:true,
      snack_msg:'Enter all the Details !!',
      alert_type:'warning',
    });
  }
  else{
    this.setState({
      snack_open:true,
      snack_msg:'Sending the Mail !!',
      alert_type:'info',
    });
    axios.post('/ework/user/assign_suprimity',{data:this.state})
    .then( res => {
        if(res.data==='ok'){
          this.setState({
            snack_open:true,
            snack_msg:'Successfully Inserted  !!',
            alert_type:'success',
          });
        }
        else if(res.data==='no')
        {
          this.setState({
            snack_open:true,
            snack_msg:'No User Found !!',
            alert_type:'error',
          });
        }
        else if(res.data==='have')
        {
          this.setState({
            snack_open:true,
            snack_msg:'Already Assigned !!',
            alert_type:'info',
          });
        }
    });
  }
}
  render() {
    return (
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      <Paper elevation={2} style={{margin:'30px',padding:'40px'}}>
              <Grid container spacing={1}>
                  <Grid item xs={12} sm={6}>
                      <TextField  id="username" type="text" fullWidth className="validate" name="username" value={this.state.username}
                      onChange={this.handleField} label="Enter Offical Id" variant="outlined" />
                   </Grid>
                  <Grid item xs={12} sm={6}>
                        <FormControl variant="outlined" style={{width:'100%'}}>
                          <InputLabel id="type">Select Level</InputLabel>
                            <Select
                              labelId="type"
                              id="type"
                              value={this.state.task} name="task" onChange={this.handleField}
                              labelWidth={82}
                            >
                            <MenuItem value="add-skill">Add Skill</MenuItem>
                            <MenuItem value="add-domain">Add Domain</MenuItem>
                            <MenuItem value="achieve-domain">Add Path For Domain</MenuItem>
                            <MenuItem value="add-questions">Add Questions</MenuItem>
                            </Select>
                         </FormControl>
                  </Grid>
              </Grid>
             <br />
               <Button variant="contained" color="secondary" style={{float:'right'}}
                onClick={this.putSuprimity}>Assign</Button>

      </Paper>
    </React.Fragment>
    );
  }
}
