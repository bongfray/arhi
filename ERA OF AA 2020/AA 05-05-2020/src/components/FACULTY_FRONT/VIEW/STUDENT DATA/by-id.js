import React, { Component } from 'react';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import ViewStudentData from '../../FACULTY ADVISER/view_single_student'
import {Snackbar} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class StudentData extends Component {
  constructor()
  {
    super()
    this.state={
      red_id:'',
      details_view:false,
      data:'',
      loading:false,
      snack_open:false,
      snack_msg:'',
      alert_type:'',
    }
  }

  closeView=()=>{
    this.setState({details_view:false})
  }

  fetchUser=()=>
  {
    if(!this.state.reg_id)
    {
      this.setState({
        snack_open:true,
        snack_msg:'Enter Registration id !!',
        alert_type:'warning',
      })
    }
    else {
      this.setState({loading:true})
      axios.post('/ework/user2/check_student',{regid:this.state.reg_id.toUpperCase()})
      .then( res => {
          this.setState({loading:false})
          if(res.data === 'no')
          {
            this.setState({
              snack_open:true,
              snack_msg:'User Not Found !!',
              alert_type:'error',
            })
          }
          else
          {
            this.setState({data:res.data,loading:false,details_view:true})
          }
      });
    }
  }

setRegId=(e)=>{
  this.setState({reg_id:e.target.value})
}

  render() {
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} >
          <CircularProgress style={{color:'#ffff00'}} />
        </Backdrop>
      )
    }
    else {
    return (
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

        <Grid container>
           <Grid item xs={1} sm={2} lg={4} xl={4} md={3}/>
           <Grid item xs={10} sm={8} lg={4} xl={4} md={6}>
             <Paper elevation={3} style={{minHeight:'120px',padding:'10px'}}>
               <TextField
                 required
                 id="filled-required"
                 label="Enter Registration id "
                 value={this.state.reg_id}
                 onChange={this.setRegId}
                 variant="outlined"
                 fullWidth
               />
                <Button style={{float:'right',marginTop:'5px'}} onClick={this.fetchUser} variant="contained" color="secondary">View History</Button>
               </Paper>
           </Grid>
           <Grid item xs={1} sm={2} lg={4} xl={4} md={3}/>
        </Grid>
        <br />
        {this.state.data &&
           <ViewStudentData details_view={this.state.details_view}  closeView={this.closeView} data={this.state} />
        }
      </React.Fragment>
    );
   }
  }
}
