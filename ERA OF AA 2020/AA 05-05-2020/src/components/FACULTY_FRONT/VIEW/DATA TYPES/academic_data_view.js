import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import AcademicTable from './acadmeic_data_table.js'

export default class AdRes extends Component {
  constructor() {
    super()
    this.state={
      free_data:[],
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.fetchData(this.props.action);
  }

  componentDidUpdate =(prevProps)=>{
    if(prevProps!== this.props)
    {
      this.fetchData(this.props.action)
    }
  }

  fetchData=(route)=>{
    axios.post(route,{datas:this.props.referDatas,action:null,username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        free_data: res.data,
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let adres_data;
    if(this.state.loader)
    {
      adres_data =<Backdrop  open={true} style={{zIndex:'2040'}}>
                <CircularProgress color="secondary" />
              </Backdrop>
    }
    else{
      adres_data =
        <React.Fragment>
               {this.state.free_data.length>0 ?
                  <AcademicTable admin_action={this.props.admin_action} username={this.props.username} academic_data={this.state.free_data} />
                  :
                  <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
               }
        </React.Fragment>
    }
    return (
      <React.Fragment>
        {adres_data}
        </React.Fragment>
    );
  }
}
