import React, { Component,Fragment } from 'react'
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import {Snackbar} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

import ReqStat from './approve_stat'


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class ManageOne extends Component {
  constructor(props)
  {
    super(props)
    this.initialState ={
      action:'false',
      username:props.username,
      req_date:'',
      req_month:'',
      req_year:'',
      req_reason:'',
      day_order:'',
      expired:false,
      denying_reason:'',
      request_props:[],
      added:false,
      disabled:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.state = this.initialState;
  }

  handleDatas=(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleRequest =(e) =>{
    e.preventDefault()
    this.setState({added:true})
    if(!(this.state.req_date) || !(this.state.req_month) || !(this.state.req_year) || !(this.state.day_order) || !(this.state.req_reason))
    {
      this.setState({
        snack_open:true,
        snack_msg:'Fill all the Details !!',
        alert_type:'warning',
      });
    }
    else{
      this.setState({
        snack_open:true,
        snack_msg:'Validating....',
        alert_type:'info',
        disabled:true
      });
    axios.post('/ework/user/request_for_entry',this.state).then(response=>{
      if(response.data.handled)
      {
        this.setState({
          snack_open:true,
          snack_msg:response.data.handled,
          alert_type:'info',
        });
      }
      else if(response.data.succ)
      {
        this.setState({
          snack_open:true,
          snack_msg:response.data.succ,
          alert_type:'success',
        });
      }
      else if(response.data.noday){
        this.setState({
          snack_open:true,
          snack_msg:response.data.noday,
          alert_type:'error',
        });
      }
       this.setState(this.initialState);
    })
    .catch( err => {
      this.setState({
        snack_open:true,
        snack_msg:'Something Went Wrong !!',
        alert_type:'error',
      });
    });
   }
  }
  render()
  {
    return(
      <Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      <Grid container spacing={1}>
        <Hidden xsDown>
          <Grid item sm={1}/>
        </Hidden>
        <Grid item xs={12} sm={10}>
          <Paper variant="outlined" square style={{padding:'10px 10px 70px 10px'}}>
          <Typography variant="h6" align="center" gutterBottom>
            REQUEST FOR AN ENTRY
          </Typography><br />
          <Grid container spacing={1}>
            <Grid item xs={12} sm={2}>
              <TextField type="number" fullWidth name="day_order" id="Request-dayorder" value={this.state.day_order} onChange={this.handleDatas}
               label="Enter the DayOrder" variant="outlined" />
            </Grid>
            <Grid container spacing={1} item xs={12} sm={5}>
                 <Grid item xs={12} sm={4}>
                    <TextField name="req_date" type="number" id="day-req" fullWidth value={this.state.req_date} onChange={this.handleDatas}
                     label="Day(dd)" variant="outlined" />
                 </Grid>
                 <Grid item xs={12} sm={4}>
                   <TextField type="number" name="req_month" fullWidth value={this.state.req_month} id="month-req" onChange={this.handleDatas}
                    label="Month(mm)" variant="outlined" />
                 </Grid>
                 <Grid item xs={12} sm={4}>
                     <TextField  type="number" name="req_year" fullWidth value={this.state.req_year} id="year-req" onChange={this.handleDatas}
                      label="Year(yyyy)" variant="outlined" />
                 </Grid>
            </Grid>
            <Grid item xs={12} sm={5}>
                 <TextField
                    id="filled-textarea"
                    label="Enter the Valid Reason"
                    name="req_reason"
                    value={this.state.req_reason}
                    onChange={this.handleDatas}
                    multiline
                    fullWidth
                    variant="filled"
                  />
            </Grid>
          </Grid><br />
            <Button disabled={this.state.disabled} variant="contained" color="primary"
             onClick={this.handleRequest} style={{marginBottom:'8px',float:'right'}}>Make A Request</Button>
          </Paper>
        </Grid>
        <Hidden xsDown>
          <Grid item sm={1}/>
        </Hidden>
      </Grid>
      <br />
      <Paper variant="outlined" square style={{padding:'10px'}}>
           {this.state.added ?
             <ReqStat request_props={this.state} />
              :
             <ReqStat />
           }
      </Paper>
      </Fragment>
    )
  }
}
