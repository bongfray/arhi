import React, { Component } from 'react'
import axios from 'axios'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';



export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
			logout:'/ework/user/logout',
			get:'/ework/user/',
      count:0,
      loading:false,
      nav_route: '/ework/user/fetchnav',
      percent_ad:[],
      percent_ac:[],
      percent_re:[],
    };
		this.componentDidMount = this.componentDidMount.bind(this);
    }

fetchPercentage = () =>{
  this.setState({loading:true})
  axios.get('/ework/user/knowPercentage', {
  })
  .then(response =>{
    const administrative = response.data.filter(item => item.action=== "Administrative");
    const academic = response.data.filter(item => item.action=== "Academic");
    const research = response.data.filter(item => item.action=== "Research");
    this.setState({percent_ad:administrative,percent_ac:academic,percent_re:research,loading:false})
  })
}
    componentDidMount() {
        this.fetchPercentage();
    }
  render()
  {
    if(this.state.loading)
    {
      return(
        <div style={{marginLeft:'50%',padding:'30px'}}>
          <CircularProgress color="secondary" />
        </div>
      )
    }
    else{
        return(
          <React.Fragment>
          <Grid container spacing={1} style={{padding:'15px'}}>
            <Grid item xs={12} sm={4}>
               <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}}
               onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
                  <ExpansionPanelSummary
                  style={{borderRadius:'20px'}}
                    expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                    className="mine-collap"
                  >
                <div style={{color:'yellow',fontSize:'18px'}}>Administrative Works</div>
                      </ExpansionPanelSummary>
                    <div style={{padding:'15px'}}>
                      <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                          <TableHead>
                            <TableRow>
                              <TableCell>Your Roles</TableCell>
                              <TableCell align="center">Alloted(H/W)</TableCell>
                              <TableCell align="center">Completed(H/W)</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {this.state.percent_ad.map((row,index) => (
                              <TableRow key={index}>
                                <TableCell component="th" scope="row">
                                  {row.responsibilty_title}
                                </TableCell>
                                <TableCell align="center">{row.responsibilty_percentage}</TableCell>
                                <TableCell align="center"><Total role={row.responsibilty_title}/></TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </div>
                  </ExpansionPanel>
            </Grid>

            <Grid item xs={12} sm={4}>
             <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
                <ExpansionPanelSummary
                style={{borderRadius:'20px'}}
                  expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                  aria-controls="panel4bh-content"
                  id="panel4bh-header"
                  className="mine-collap"
                >
              <div style={{color:'yellow',fontSize:'18px'}}>Academic Work</div>
                    </ExpansionPanelSummary>
                    <div style={{padding:'15px'}}>
                      <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                          <TableHead>
                            <TableRow>
                              <TableCell>Your Roles</TableCell>
                              <TableCell align="center">Alloted(H/W)</TableCell>
                              <TableCell align="center">Completed(H/W)</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {this.state.percent_ac.map((row,index) => (
                              <TableRow key={index}>
                                <TableCell component="th" scope="row">
                                  {row.responsibilty_title}
                                </TableCell>
                                <TableCell align="center">{row.responsibilty_percentage}</TableCell>
                                <TableCell align="center"><Total role={row.responsibilty_title}/></TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </div>
                </ExpansionPanel>
            </Grid>
            <Grid item xs={12} sm={4}>

            <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={this.handleExpand} disabled={this.state.disabled_expanse}>
               <ExpansionPanelSummary
               style={{borderRadius:'20px'}}
                 expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                 aria-controls="panel4bh-content"
                 id="panel4bh-header"
                 className="mine-collap"
               >
             <div style={{color:'yellow',fontSize:'18px'}}>Research Work</div>
                   </ExpansionPanelSummary>
                     <div style={{padding:'15px'}}>
                         <TableContainer component={Paper}>
                           <Table aria-label="simple table">
                             <TableHead>
                               <TableRow>
                                 <TableCell>Your Roles</TableCell>
                                 <TableCell align="center">Alloted(H/W)</TableCell>
                                 <TableCell align="center">Completed(H/W)</TableCell>
                               </TableRow>
                             </TableHead>
                             <TableBody>
                               {this.state.percent_re.map((row,index) => (
                                 <TableRow key={index}>
                                   <TableCell component="th" scope="row">
                                     {row.responsibilty_title}
                                   </TableCell>
                                   <TableCell align="center">{row.responsibilty_percentage}</TableCell>
                                   <TableCell align="center"><Total role={row.responsibilty_title}/></TableCell>
                                 </TableRow>
                               ))}
                             </TableBody>
                           </Table>
                         </TableContainer>
                     </div>
               </ExpansionPanel>
            </Grid>
          </Grid>

          </React.Fragment>
        )
   }
  }
}



class Total extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      count:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTotal();
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.role !== this.props.role){
      this.fetchTotal();
    }
  }
  fetchTotal=()=>
  {
    axios.post('/ework/user/know_Complete_Percentage',{res:this.props.role})
    .then(response =>{
      let len= ((response.data).length)
      this.setState({count:len})
    })
  }
  render()
  {
    return(
      <React.Fragment>
         {this.state.count}
      </React.Fragment>
    );
  }
}
