import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import Nav from '../dynnav';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import InfoIcon from '@material-ui/icons/Info';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import AddDomain from './admin/ARHI-Operation/handle-domain';
import AddSkill from './admin/ARHI-Operation/handle-skill';



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class Suprimity extends Component {
  constructor(){
    super()
    this.state={
      redirectTo:null,
      forgotroute:'/ework/user/forgo',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      noti_route:true,
      disp:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    axios.get('/ework/user/')
    .then( res => {
        if(res.data.user){
          this.knowSuprimity()
        }
        else{
          this.setState({
            snack_open:true,
            snack_msg:'Not Logged In !!',
            alert_type:'error',
          })
          this.setState({redirectTo:'/ework/faculty'})
        }
    });
  }
  knowSuprimity =()=>{
    axios.post('/ework/user/knowSuprimityStatus')
    .then( res => {
        if(res.data === 'no'){
          this.setState({
            snack_open:true,
            snack_msg:'Not Authorized !!',
            alert_type:'error',
          })
          this.setState({redirectTo:'/ework/faculty'})
        }
        else{

          this.setState({proceed:res.data})
        }
    });
  }
  showInfo=()=>{
    this.setState({
      disp:!this.state.disp,
    })
  }
  render() {
    let path;
    let tilt;
    let description=
    <React.Fragment>
    <Dialog
       open={true}
       onClose={this.showInfo}
       aria-labelledby="scroll-dialog-title"
       aria-describedby="scroll-dialog-description"
     >
       <DialogTitle id="scroll-dialog-title" align="center">Insturctions</DialogTitle>
       <DialogContent >
         <DialogContentText
           id="scroll-dialog-description"
           tabIndex={-1}
         >
         In this page you have to add datas according to the responsibilty you have got through mail.
         Remember,you should not enter any kind of invalid or duplicate data.You can only edit or delete your entered datas.
         Others data can't be editable. You can only view it to understand or to know what are the things are present already.
         Please avoid to enter those existing datas.<br />
         <Typography color="secondary" variant="overline" display="block" gutterBottom>Important: </Typography>
         If you have been assign a task to show path to achieve domains, in this
         case you need to add the skills or course one should learn to be good in any domain.Like if it's Web Design and the
         level is Advanced,one should learn React Js very well.
         </DialogContentText>
       </DialogContent>
       <DialogActions>
         <Button onClick={this.showInfo} color="primary">
           GOT IT
         </Button>
       </DialogActions>
     </Dialog>
    </React.Fragment>
     ;
    if(this.state.proceed==="add-skill"){
      tilt =<span>add Skills</span>;
    }
    else if(this.state.proceed==="add-domain"){
      tilt =<span>add Domains</span>;
    }
    else if(this.state.proceed === 'achieve-domain'){
      tilt =<span>show path to achieve Domais</span>;
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
       if(this.state.proceed==="add-skill"){
         path =
         <React.Fragment>
         <Grid container spacing={1}>
           <Grid item xs={2} sm={2}>
             {this.state.disp === false &&
               <div onClick={()=>this.setState({disp:true})} style={{margin:'15px'}} >
                  <InfoIcon />
               </div>
             }
           </Grid>
           <Grid item xs={8} sm={8}>
             <Typography variant="h5" align="center">Your Role is to
               <Typography variant="button" display="block" color="secondary" gutterBottom>{tilt}</Typography>
              </Typography>
           </Grid>
         </Grid>

          {this.state.disp &&
            <React.Fragment>
              {description}
            </React.Fragment>
          }
          <div style={{padding:'15px'}}>
            <AddSkill noChange="have" select={this.state.proceed}/>
          </div>
         </React.Fragment>
       }
       else if((this.state.proceed==="add-domain") || (this.state.proceed==="achieve-domain"))
       {
        path =
        <React.Fragment>
          <Grid container spacing={1}>
            <Grid item xs={2} sm={2}>
              {this.state.disp === false &&
                <div onClick={()=>this.setState({disp:true})} style={{margin:'15px'}}>
                  <InfoIcon />
                </div>
              }
            </Grid>
            <Grid item xs={8} sm={8}>
              <Typography variant="h5" align="center">Your Role is to
                <Typography variant="button" display="block" color="secondary" gutterBottom>{tilt}</Typography>
               </Typography>
            </Grid>
          </Grid>

         {this.state.disp &&
           <React.Fragment>
             {description}
           </React.Fragment>
         }
         <div style={{padding:'15px'}}>
           <AddDomain noChange="have" select={this.state.proceed} />
         </div>
        </React.Fragment>
       }
       else{
         path = <div></div>
       }
    return (
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

       <Nav noti_route={this.state.noti_route} home={this.state.home}
       nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
        {path}
      </React.Fragment>
    );
  }
 }
}
