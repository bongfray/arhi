import React, { Component } from 'react';
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import Nav from '../../dynnav'
import Button from '@material-ui/core/Button';
import ForeignExtra from './foreign_extra'
import TimeDiv from '../TimeTable/slot2'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import {Tab,Typography} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Review from './review'

require("datejs")



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class ownExtra extends Component {
  constructor()
  {
    super()
    this.state={
      active:0,
      loading: true,
      isChecked: false,
      history:'',
      redirectTo:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      noti_route:true,
      user:'',
      value:0,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleComp = (e) =>{
    this.setState({
      isChecked: !this.state.isChecked,
      history: e.target.value,
    })
  }
  componentDidMount()
  {
    this.loggedin()
  }
  loggedin = ()=>{
    axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({user:res.data.user,loading:false})
      }
      else{

        this.setState({redirectTo:'/ework/faculty',loading:false})
      }
    })
  }

  render() {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     }
     else
     {
       if(this.state.loading)
       {
         return(
           <Backdrop  open={true} >
             <CircularProgress color="secondary" />
           </Backdrop>
         )
       }
       else {
        return (
          <React.Fragment>
                    <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div>
          <div>
            <AppBar position="static" color="default">
               <Tabs
                 value={this.state.value}
                 indicatorColor="secondary"
                 textColor="secondary"
                 variant="fullWidth"
                 aria-label="full width tabs example"
               >
                 <Tab label="Own Extra" onClick={(e)=>this.setState({value:0})}  />
                 <Tab label="Foreign Extra" onClick={(e)=>this.setState({value:1})} />
                 <Tab label="Review" onClick={(e)=>this.setState({value:2})}  />
               </Tabs>
             </AppBar>
          </div>
            <br /><br />
            <InputValue datas={this.state.value} user={this.state.user} />
          </div>
          </React.Fragment>
        );
    }
   }
  }
}



class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
      date:'',
      dayorder:'',
      hour:'',
      day_order:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  addAllot =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }
  componentDidMount()
  {
    this.fetchdayorder()
  }
  fetchdayorder =()=>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{
          this.setState({
            day_order: response.data.day_order,
          });
    });
  }
  render(){
    if(this.props.datas === 1)
    {
      return(
        <React.Fragment>
          <ForeignExtra />
        </React.Fragment>
      );
    }
    else if(this.props.datas === 2)
    {
      return(
        <React.Fragment>
          <Review user={this.props.user} />
        </React.Fragment>
      )
    }
    else
    {
      return(
        <React.Fragment>
          <OwnExtra username={this.props.user.username} day_order={this.state.day_order} />
        </React.Fragment>
      );
    }
  }
}


class  OwnExtra extends Component {
  constructor() {
    super()
    this.state={
      compense_data:[],
      modal: false,
      color:'red',
      notfound:'',
      content:'',
      datas_a:'',
      props_content:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchCompenseClasses()

  }
  fetchCompenseClasses = ()=>{
    axios.get('/ework/user/fetch_own_extra')
    .then( res => {
      if(res.data.length === 0)
      {
        this.setState({notfound:'No Data Found !!'})
      }
      else{
        this.setState({compense_data:res.data})
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
showModal=(e,content)=>{
  this.setState({modal:true,content:content,
  datas_a:{
    for_year:content.for_year,
    for_sem:content.for_sem,
    for_batch:content.for_batch,
  },
  props_content:{
    number:content.hour,
  }
})
}

color =(hour,day,month,year) =>{
	axios.post('/ework/user/fetchfrom', {
		day_order:this.props.day_order,
    hour:hour,
		date: day,
		month:month,
		year: year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
					loading:false,
				})
		}
	})
}

  render()
  {
    // console.log(this.props.day_order)
    var date = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    return(
      <React.Fragment>
      <React.Fragment>
      {this.state.compense_data.length>0 ?
      <React.Fragment>
        <TableContainer component={Paper}>
          <Table  aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center"><b>Requested On</b></TableCell>
                <TableCell align="center"><b>Requested DayOrder / Hour</b></TableCell>
                <TableCell align="center"><b>Requested Slot</b></TableCell>
                <TableCell align="center"><b>For Batch-Sem-Year</b></TableCell>
                <TableCell align="center"><b>Deadline(dd-mm-yyyy)</b></TableCell>
                <TableCell align="center"><b>Status</b></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.compense_data.map((content,index)=> (
                <TableRow key={index}>
                  <TableCell align="center">
                    {content.date}-{content.month}-{content.year}
                  </TableCell>
                  <TableCell align="center">{content.day_order} / {content.hour}</TableCell>
                  <TableCell align="center">{content.slot}</TableCell>
                  <TableCell align="center">{content.for_batch}-{content.for_sem}-{content.for_year}</TableCell>
                  <TableCell align="center">{content.date}-{content.month}-{content.year} [Before 11:59pm]</TableCell>
                  <TableCell align="center">
                    <Button variant="contained" color="primary" style={{float:'center'}} onClick={(e)=>this.showModal(e,content)}>UPLOAD</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </React.Fragment>
      :
      <Typography variant="h5" align="center">No request Found !!</Typography>
      }
      </React.Fragment>

               {this.state.modal &&
                 <Dialog
      open={this.state.modal}
      TransitionComponent={Transition}
      keepMounted
      onClose={this.selectModal}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogContent style={{width:'230px'}}>
                    <div className="row">
                       <div className="col l12">
                           <TimeDiv day_order={this.props.day_order} username={this.props.username}
                            date={date} month={month} year={year}
                            datas_a={this.state.datas_a}
                            slots={this.state.content.slot}
                            content={this.state.props_content}
                            displayModal={this.state.modal}
                            closeModal={this.selectModal}
                            cday={true}
                            color={() => this.color(this.state.content.problem_compensation_hour,date,month,year)}
                            />
                       </div>
                    </div>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={this.selectModal} color="primary">
                        Close
                      </Button>
                    </DialogActions>
                  </Dialog>
                  }
      </React.Fragment>
    );
  }
}
