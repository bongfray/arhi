import React, { Component } from "react";
import { Offline } from "react-detect-offline";
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import Slide from '@material-ui/core/Slide';
import {Typography,Hidden,Button} from '@material-ui/core';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class OFFLINE extends Component{
  constructor() {
    super()
    this.state={

    }
  }
  render()
  {
    return(
      <Offline>
        <Dialog fullScreen open={true} TransitionComponent={Transition}>
            <List>
                <div>
                   <Grid container spacing={1}>
                      <Hidden><Grid item sm={3} /></Hidden>
                      <Grid item xs={12} sm={6} style={{marginTop:'150px'}}>
                        <Typography variant="h4" align="center" style={{color:'red'}}>Oops !!</Typography><br />
                        <Typography variant="h4" align="center">No Internet Connection !!</Typography><br />
                        <Typography variant="h6" align="center">
                          It seems you are offline !! Please don't press refresh button if you are filling
                          any data. As soon as you will be online,we will redirect you automatically.In this case if you have not refreshed
                          your page,all your datas you have filled just a few minute earlier,will be as it is.
                        </Typography>
                        <br />
                        <div style={{textAlign:'center'}}>
                           <Button variant="outlined"
                           onClick={this.props.go_offline}>Go Offline</Button>
                        </div>
                      </Grid>
                      <Hidden xsDown><Grid item sm={3}/></Hidden>
                   </Grid>
                </div>
            </List>
        </Dialog>
      </Offline>
    )
  }
}
