import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import {Button,Dialog,DialogActions,DialogTitle,DialogContent} from '@material-ui/core';



export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      Action:'',
      name: '',
      emailid: '',
      mobileno:'',
      address: '',
      gender:'',
      faculty_id:'',
      subject_taking:'',
      current:false,
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
  })

  if(this.props.data.Action === 'FACULTY LIST'){
    this.setState({current:true})
  }
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {

    let pageTitle;
if(this.state.serial) {
  pageTitle = "EDIT DATAS";
} else {
  pageTitle ="ADD DATAS";
}

    return(
      <Dialog
        open={true}
        fullWidth
        onClose={this.props.cancel}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title" align="center">{pageTitle}</DialogTitle>
      <DialogContent >

      {this.props.description &&
        <Typography variant="overline" style={{textAlign:'center'}} display="block" gutterBottom>
         {this.props.description}
        </Typography>
      }

          {this.props.data.fielddata.map((content,index)=>(
            <React.Fragment key={index}>
              <Grid container spacing={1} >
                  <Grid item xs={12} sm={3}>{content.header}</Grid>
                  <Grid item xs={12} sm={9}>
                   {content.long ?
                     <TextField
                       type={content.type}
                       id={content.name}
                       label={content.placeholder}
                       name={content.name}
                       value={this.state[content.name]}
                       onChange={e => this.handleD(e, index)}
                       multiline
                       variant="outlined"
                       fullWidth
                     />
                     :
                     <TextField
                       type={content.type}
                       id={content.name}
                       label={content.placeholder}
                       name={content.name}
                       value={this.state[content.name]}
                       onChange={e => this.handleD(e, index)}
                       variant="outlined"
                       fullWidth
                     />
                   }
                  </Grid>
              </Grid>
              <br />
              </React.Fragment>
          ))}

        </DialogContent>
      <DialogActions>
        <Grid container spacing={1}>
          <Grid item xs={4} sm={6} />
          <Grid item xs={4} sm={3}>
            <Button variant="outlined" onClick={this.props.cancel} color="secondary">CANCEL</Button>
          </Grid>
          <Grid item xs={4} sm={3}>
           <Button variant="contained" onClick={this.handleSubmit} style={{backgroundColor:'#455a64',color:'white'}} >SUBMIT</Button>
          </Grid>
        </Grid>
      </DialogActions>
     </Dialog>
    )
  }
}
