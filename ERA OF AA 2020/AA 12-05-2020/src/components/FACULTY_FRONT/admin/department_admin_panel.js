import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import {Dialog,Grid,Paper} from '@material-ui/core';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Radio,Typography,Switch} from '@material-ui/core';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


import Nav from '../../dynnav';
import UsersOfDepartment from './DEPT_ADMIN/department_users';



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class DepartmentAdmin extends Component {
  constructor()
  {
    super()
    this.state={
      logout:'/ework/user/logout',
      get:'/ework/user/',
      login:'/ework/flogin',
      nav_route: '',
      noti_route:true,
      user:'',
      value:0,
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    if(this.props.user)
    {
      this.setState({
        user:this.props.user,
        loading:false,
      })
    }
    else {
      this.fetchlogin();
    }

  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user === null)
        {
          this.setState({
            redirectTo:'/ework/',
          });
        }
        else
        {
          this.setState({user:response.data.user})
        }
        this.setState({loading:false})
      })
    }


  render() {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
        if(this.state.loading)
        {
          return(
            <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
              <CircularProgress color="secondary" />
            </div>
          )
        }
      else
      {
    return(
        <React.Fragment>
        {!this.props.user &&
         <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get}
         logout={this.state.logout}/>
        }

           <AppBar position="static" color="default">
              <Tabs
                value={this.state.value}
                indicatorColor="secondary"
                textColor="primary"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Validate User" onClick={(e)=>this.setState({value:0})}  />
                <Tab label="View Users in Department" onClick={(e)=>this.setState({value:1})} />
              </Tabs>
            </AppBar>

            <Grid container>
              <Display user={this.state.user}  choosed={this.state.value}/>
            </Grid>
        </React.Fragment>
    );
  }
}
  }
}



class Display extends Component {
  constructor() {
    super()
    this.state={
      fetching:true,
      dept_list:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchList();
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps!== this.props)
    {
      this.fetchList();
    }
  }
  fetchList=()=>{
    if(this.props.choosed === 1)
    {
      axios.post('/ework/user/fetch_dept_users',{d_admin:this.props.user})
      .then( res => {
        this.setState({dept_list:res.data,fetching:false})
      });
    }
  }
      render()
      {
        if(this.props.choosed === 1)
        {
          if(this.state.fetching)
          {
            return(
              <div style={{position: 'relative',marginLeft: '50%',marginTop:'20px'}}>
                <CircularProgress color="secondary" /><br/>
                Fetching.....
              </div>
            )
          }
          else {
            return(
              <div style={{marginTop:'15px',padding:'10px'}}>
              <UsersOfDepartment list_of_user={this.state.dept_list}  />
              </div>
            )
          }
        }
        else if(this.props.choosed === 0)
        {
          return(
            <ValidateUser user={this.props.user} />
          )
        }
        else
        {
          return(
            <div></div>
          )
        }
      }
}


class ValidateUser extends Component{
  constructor()
  {
    super()
    this.state={
      display:true,
      requests:[],
      recheck:[],
      notfound:'',
      isChecked:false,
      username:'',
      faculty_ad:false,
      year_set:false,
      disable_active:true,
      for_year:'',
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchRequests()
  }
  fetchRequests = ()=>{
    axios.post('/ework/user/fetch_request',{user:this.props.user})
    .then(res=>{
        this.setState({display:false,requests:res.data})
    })
  }

  ActiveFacultyAdvisor =(e,index,username)=>
  {
    this.setState({advisor_on:true,index,year_set:true})
  }

  Active =(e,index,content)=>{
    this.setState({
      snack_open:true,
      snack_msg:'Activating...!! Sendign Mail...',
      alert_type:'warning',
    });
  this.setState({
        isChecked: !this.state.isChecked,index,
        username: e.target.value,
      })

      axios.post('/ework/user/active_user',{
        content: content,advisor_on:this.state.advisor_on,for_year:this.state.for_year
      })
      .then(res=>{
            this.setState({
              advisor_on:false
            })
            this.setState({
              snack_open:true,
              snack_msg:'Activated !!',
              alert_type:'success',
            });
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== content.username)
                 })
      })
  }
handleModal=()=>{
  this.setState({year_set:!this.state.year_set})
}
handleInput=(e)=>{
  this.setState({for_year:e.target.value})
}

  render()
  {
    return(
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

          {this.state.year_set &&
            <Dialog
              open={this.state.year_set}
              onClose={()=>this.setState({year_set:false})}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">In Which Year ?</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <FormControl style={{width:'100%'}}>
                      <InputLabel id="sel_year">Year</InputLabel>
                        <Select
                          labelId="sel_year"
                          id="sel_year"
                          name="year"
                          value={this.state.year}
                          onChange={this.handleInput}
                        >
                          <MenuItem value="" disabled defaultValue>Year</MenuItem>
                          <MenuItem value="1">First Year</MenuItem>
                          <MenuItem value="2">Second Year</MenuItem>
                          <MenuItem value="3">Third Year</MenuItem>
                          <MenuItem value="4">Fourth Year</MenuItem>
                          <MenuItem value="5">Fifth Year</MenuItem>
                        </Select>
                     </FormControl>
                </DialogContentText>
              </DialogContent>
              <DialogActions>
              <Button onClick={()=>this.setState({year_set:false})} color="primary" autoFocus>
                CLOSE
              </Button>
                <Button onClick={this.handleModal} color="primary" autoFocus>
                  SUBMIT
                </Button>
              </DialogActions>
            </Dialog>
          }
        <br />

{!(this.state.display) ?
        <Grid container spacing={1}>
           <Grid item xs={12} sm={12}>
             {this.state.requests.length === 0 ?
               <Typography variant="h6" align="center">No Request Found !!</Typography>
               :
                <div style={{padding:'20px'}}>
                   <Typography align="center">You Will able to see the requests only of your department and campus</Typography>
                   <br />
                     <TableContainer component={Paper}>
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell align="center"><b>Name</b></TableCell>
                            <TableCell align="center"><b>Official Id</b></TableCell>
                            <TableCell align="center"><b>Mail Id</b></TableCell>
                            <TableCell align="center"><b>Faculty Advisor</b></TableCell>
                            <TableCell align="center"><b>Action</b></TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {this.state.requests.map((row,index) => (
                            <TableRow key={index}>
                              <TableCell align="center">{row.name}</TableCell>
                              <TableCell align="center">{row.username}</TableCell>
                              <TableCell align="center">{row.mailid}</TableCell>
                              <TableCell align="center">
                                  <FormControl component="fieldset">
                                      <RadioGroup aria-label="gender1" value={this.state.advisor_on}
                                      onChange={(e)=>{this.ActiveFacultyAdvisor(e,index,row.username)}}>
                                       <FormControlLabel value="female" control={<Radio />} label="Choose Year" />
                                      </RadioGroup>
                                    </FormControl>
                              </TableCell>
                              <TableCell align="center">
                                <FormControlLabel
                                    control={<Switch checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,row)}}
                                     name="Active" />}
                                    label="Active"
                                  />
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                </div>
            }
           </Grid>
        </Grid>
        :
        <div style={{position: 'relative',marginLeft: '50%',marginTop:'20px'}}>
          <CircularProgress color="secondary" /><br/>
          Fetching.....
        </div>

      }

      </React.Fragment>
    )
  }
}
