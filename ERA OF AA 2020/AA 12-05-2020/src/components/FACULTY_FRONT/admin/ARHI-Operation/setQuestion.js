import React, { Component } from 'react';
import axios from 'axios'
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';


import ValidateUser from '../validateUser'
import DataTable from './question_table';




export default class SkillLevel extends Component {

 constructor(props)
 {
   super(props)
   this.state ={
     username:'',
     redirectTo:'',
     option:'',
     skill_name:'',
     saved_skill:[],
     loading:true,
   }
   this.componentDidMount = this.componentDidMount.bind(this)
 }

handleOption = (e) =>{
 this.setState({option: e.target.value})
}
setSkill=(e)=>{
  if(e === null)
  {

  }
  else{
    this.setState({skill_name:e.skill_name})
  }
}

componentDidMount(){
 this.fetchSavedSkills()
}

fetchSavedSkills =() =>{
 axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
 .then(res => {
     if(res.data)
     {
       this.setState({saved_skill:res.data,loading:false})
     }
 });
}


 render()
 {
   if(this.state.loading)
   {
     return(
       <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
         <CircularProgress color="secondary" />
       </div>
     )
   }
   else
   {
     const options = this.state.saved_skill.map(option => {
       const firstLetter = option.skill_name[0].toUpperCase();
       return {
         firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
         ...option,
       };
     });
   return(
     <React.Fragment>
     <Grid container spacing={1}>
             <Grid item xs={6} sm={6}>
                <Paper elevation={2} style={{padding:'20px'}}>
                    <Autocomplete
                      id="grouped-demo"
                      options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                      groupBy={option => option.firstLetter}
                      getOptionLabel={option => option.skill_name}
                      onChange={(event, value) => this.setSkill(value)}
                      style={{ width: '100%' }}
                      renderInput={params => (
                        <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
                      )}
                    />
                </Paper>
             </Grid>
             <Grid item xs={6} sm={6}>
                       <Paper elevation={2} style={{padding:'20px'}}>
                           <FormControl variant="outlined" style={{width:'100%'}}>
                              <InputLabel id="demo-simple-select-outlined-label">
                                Select Level
                              </InputLabel>
                              <Select
                                labelId="level"
                                id="level"
                                value={this.state.option} onChange={this.handleOption}
                                labelWidth={90}
                              >
                                <MenuItem value="Begineer">Beginner</MenuItem>
                                <MenuItem value="Intermediate">Intermediate</MenuItem>
                                <MenuItem value="Advanced">Advanced</MenuItem>
                              </Select>
                            </FormControl>
                       </Paper>
             </Grid>
       </Grid>
       {(this.state.option && this.state.skill_name) && <div className="row">
         <Gg noChange={this.props.noChange} level={this.state.option}
         skill_name={this.state.skill_name} username={this.props.username}/>
       </div>
       }
     </React.Fragment>
     )
   }

 }
}

class Gg extends Component{
 constructor(props) {
    super(props);
    this.state = {
      redirectTo: null,
      username:'',
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      action:this.props.options,
      saved_skill:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentDidMount(){
    this.fetchSavedSkills()
  }

  fetchSavedSkills =()=>{
    axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
    .then(res => {
        if(res.data)
        {
          this.setState({saved_skill:res.data})
        }
    });
  }


  onCreate = (e,index) => {
    this.setState({ isAddProduct: true,product: {}});
  }

  onFormSubmit(data) {
    let apiUrl;
    var addroute,editroute;
      addroute="/ework/user2/add_from_insert_in_admin";
      editroute = "/ework/user2/edit_inserted_data";

    if(this.state.isEditProduct){
      apiUrl = editroute;
    } else {
      apiUrl = addroute;
    }
    axios.post(apiUrl, {data})
        .then(response => {
          this.setState({
            response: response.data,
            isAddProduct: false,
            isEditProduct: false
          })
        })
  }

  editProduct = (productId,index)=> {
    var editProd;
      editProd ="/ework/user2/fetch_for_edit"
    axios.post(editProd,{
      id: productId,
    })
        .then(response => {
          this.setState({
            product: response.data,
            isEditProduct: true,
            isAddProduct: true,
          });
        })

 }

 updateState = () =>{
   this.setState({
     isAddProduct:false,
     isEditProduct:false,
   })
 }


  render() {
    let productForm;
            if(this.state.isAddProduct || this.state.isEditProduct)
            {
              productForm =  <AddProduct cancel={this.updateState} username={this.props.username}
                action="add-questions" level={this.props.level} for={'QS '+this.props.skill_name}
                onFormSubmit={this.onFormSubmit}  product={this.state.product} />
            }

      return (
       <React.Fragment>
       <div>
             {!this.state.isAddProduct &&
              <React.Fragment>
                <Grid container spacing={1}>
                  <Grid item xs={6} sm={6} />
                  <Grid item xs={6} sm={6}>
                      <Fab style={{float:'right',marginRight:'8px'}} size="small" color="primary"
                      onClick={(e) => this.onCreate(e)}
                      aria-label="add">
                           <AddIcon />
                       </Fab>
                  </Grid>
                </Grid>
             </React.Fragment>
            }
            <br />
         {!this.state.isAddProduct &&
           <React.Fragment>
           {(this.props.skill_name && this.props.level) &&
             <ProductList username={this.props.username} noChange={this.props.noChange}
             action="add-questions" level={this.props.level} for={'QS '+this.props.skill_name}
             editProduct={this.editProduct}/>
           }
           </React.Fragment>
          }
         { productForm }
       </div>
      </React.Fragment>
      );
  }
}



class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      action:'',
      usertype:'',
      username:'',
      skill_name:'',
      for:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  componentDidMount(){
    this.setState({
      action:this.props.action,
      usertype:this.props.level,
      inserted_by:this.props.username,
    })
    if(this.props.for)
    {
      this.setState({for:this.props.for})
    }
  }
  stateSet=(object)=>{
    this.setState(object);
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
      <Dialog open={true}
      keepMounted
      maxWidth={'md'}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
      onClose={this.props.cancel}>
          <DialogContent>
             <div style={{padding:'10px'}}>
                 <TextField
                   id="outlined-multiline-static"
                   label={"Enter the Question"}
                   multiline
                   fullWidth
                   name={"question"}
                   value={this.state.question}
                   onChange={e => this.handleD(e)}
                   variant="filled"
                 />
                 <br /><br/>
                 <FormGroup row>
                         <FormControlLabel style={{color:'black'}}
                           control={
                             <TextField
                               id="outlined-multiline-static"
                               label={"Enter Option 1"}
                               multiline
                               fullWidth
                               name={"option1"}
                               value={this.state.option1}
                               onChange={e => this.handleD(e)}
                               variant="filled"
                             />
                           }
                         />
                         <FormControlLabel style={{color:'black'}}
                           control={
                             <TextField
                               id="outlined-multiline-static"
                               label={"Enter Option 2"}
                               multiline
                               fullWidth
                               name={"option2"}
                               value={this.state.option2}
                               onChange={e => this.handleD(e)}
                               variant="filled"
                             />
                           }
                         />
                 </FormGroup>
                 <br /><br />
                 <FormGroup row>
                         <FormControlLabel style={{color:'black'}}
                           control={
                             <TextField
                               id="outlined-multiline-static"
                               label={"Enter Option 3"}
                               multiline
                               fullWidth
                               name={"option3"}
                               value={this.state.option3}
                               onChange={e => this.handleD(e)}
                               variant="filled"
                             />
                           }
                         />
                         <FormControlLabel style={{color:'black'}}
                           control={
                             <TextField
                               id="outlined-multiline-static"
                               label={"Enter Option 4"}
                               multiline
                               fullWidth
                               name={"option4"}
                               value={this.state.option4}
                               onChange={e => this.handleD(e)}
                               variant="filled"
                             />
                           }
                         />
                 </FormGroup>
                 <br /><br />
                 <TextField
                   id="outlined-multiline-static"
                   label={"Enter the Correct Answer"}
                   multiline
                   fullWidth
                   name={"answer"}
                   value={this.state.answer}
                   onChange={e => this.handleD(e)}
                   variant="filled"
                 />
              </div>
          </DialogContent>
          <DialogActions>
          <Button onClick={this.props.cancel} color="secondary">
            Close
          </Button>
            <Button onClick={this.handleSubmit} color="primary">
              UPLOAD
            </Button>
          </DialogActions>
      </Dialog>
    )
  }
}



class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:false,
      allowed:false,
      error: null,
      products: [],
      action:'',
      loading:true,
      value_for_search:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({modal: !this.state.modal,disabled:true,id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
   delroute = "/ework/user2/del_inserted_data";


const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:false,
        modal:false,
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
    fetchroute = "/ework/user2/fetch_in_admin_for_path";

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
    for:this.props.for,
  })
  .then(response =>{
    this.setState({
     products: response.data,
     loading:false,
   })
  })
}

  render() {
    const {products} = this.state;
    var libraries = products;

    if(this.state.loading)
    {
      return(
        <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
          <CircularProgress color="secondary" />
        </div>
      );
    }
    else{
      return(
        <div>
            {this.state.modal &&
              <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
            }

            {libraries.length>0 ?
                <React.Fragment>
                        <div style={{margin:'0px 15px 0px 15px'}}>
                            <DataTable username={this.props.username} noChange={this.props.noChange} data={libraries}
                             action={this.props.action} disabled={this.state.disabled} products={this.state.products}
                             libraries={libraries} editProduct={this.props.editProduct}
                             deleteProduct={this.deleteProduct}/>
                       </div>
                 </React.Fragment>
                 :
                 <Typography variant="h5" style={{textAlign:'center',color:'red'}} display="block" gutterBottom>
                    No Data Found !!
                 </Typography>
               }
            </div>
      )
    }
  }
}
