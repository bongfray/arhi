import React, { Component } from 'react';
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AddProduct from './add';
import ProductList from './prod'
import SkillLevel from './add_path';

export default class SkillSetting extends Component {
  constructor()
  {
    super()
    this.state={
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user)
        {
          this.setState({username:response.data.user.username})
        }
        else{
          this.setState({
            redirectTo:'/ework/',
          });
        }
      })
    }

  render() {
    let target;
      if(this.props.select === 'add-domain')
      {
        target = <AddDomain noChange={this.props.noChange} username={this.state.username}/>
      }
      else if(this.props.select === 'achieve-domain')
      {
        target = <SkillLevel noChange={this.props.noChange} username={this.state.username} />
      }
      else{
        target = <div></div>
      }
    return (
      <div>
         {target}
      </div>
    );
  }
}


class AddDomain extends Component {
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
       addroute="/ework/user2/add_from_insert_in_admin";
       editroute = "/ework/user2/edit_inserted_data";

     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/ework/user2/fetch_for_edit"


     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Refer Id",
                   name: "count",
                   placeholder: "blank",
                   type: "text",

                 },
                 {
                   header: "Domain Name",
                   name: "domain_name",
                   placeholder: "Enter the Name of the Domain",
                   type: "text",
                 },
                 {
                   header: "Action",
                   name: "no",
                   placeholder: "blank",
                   type: "text",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct)
             {
               productForm = <AddProduct
                d="true" username={this.props.username} cancel={this.updateState}
                action="Domain-For-Student" data={data1} onFormSubmit={this.onFormSubmit}
                 product={this.state.product} />
             }

          return (
            <React.Fragment>
            {!this.state.isAddProduct &&
               <React.Fragment>
                   <Grid container spacing={1}>
                      <Grid item xs={6} sm={6} />
                       <Grid item xs={6} sm={6}>
                         <Button variant="contained" color="secondary" style={{marginRight:'5px',float:'right'}}
                          onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</Button>
                       </Grid>
                   </Grid>
               </React.Fragment>
            }

              {!this.state.isAddProduct &&
                <ProductList  username={this.props.username}
                 noChange={this.props.noChange} d="true"
                action="Domain-For-Student" data={data1}  editProduct={this.editProduct}/>
              }
              { productForm }
          </React.Fragment>
          );

}
}
