import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

export default class Stop extends Component {
  render()
  {
    return(
      <React.Fragment>
        <Grid container spacing={1} style={{marginTop:'50px'}}>
           <Grid item xs={2} sm={4}/>
             <Grid item xs={8} sm={4}>
                <Paper elevation={4} style={{minHeight:'220px'}}>
                   <div style={{padding:'2px',textAlign:'center',backgroundColor:'#455a64',color:'white'}}>
                    {this.props.section_name}
                  </div>
                  <br /><br />
                   <Typography variant="h6" color="secondary" align="center">
                       REGISTRATION HAS BEEN CLOSED
                    </Typography>
                   <div>
                   <br />
                   <Link to={this.props.login_path} >
                    <Button style={{float:'right',marginRight:'5px'}}
                    variant="outlined" color="secondary">LOGIN</Button>
                   </Link>
                   </div>
                </Paper>
             </Grid>
           <Grid item xs={2} sm={4}/>
        </Grid>
      </React.Fragment>
    );
  }
}
