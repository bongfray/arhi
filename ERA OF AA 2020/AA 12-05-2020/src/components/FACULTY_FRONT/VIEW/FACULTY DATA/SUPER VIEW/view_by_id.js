import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios';
import Backdrop from '@material-ui/core/Backdrop';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import TextField from '@material-ui/core/TextField';

import DatasPass from '../../decider_type';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


require("datejs")


export default class ViewByID extends Component {
  constructor() {
    super()
    this.state ={
      details_view:false,
      username:'',
      redirectTo:'',
      send_user:'',
      loading:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    }
    this.retriveValue = this.retriveValue.bind(this)
    this.authenticate = this.authenticate.bind(this)
  }
retriveValue =(e)=>{
  this.setState({username:e.target.value})
}
  authenticate =(e)=>{
    this.setState({loading:true})
    e.preventDefault()
    axios.post('/ework/user/authenticate',{username:this.state.username})
    .then(res=>{
    if(res.data.username)
    {
      this.setState({
        details_view:true,
        send_user:res.data.username,
      });
    }
    else{
      this.setState({
        snack_open:true,
        snack_msg:res.data,
        alert_type:'error',
        details_view:false
      });
    }
    this.setState({loading:false})
    })
  }

  closeDecider=(object)=>{
    this.setState(object)
  }

  render()
  {
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress color="secondary" />&emsp;
          <div style={{color:'yellow'}}>Processing Your Request...</div>
        </Backdrop>
      )
    }
    else{
    return(
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>


         <Grid container>
           <Grid item xs={12} sm={3}/>
           <Grid item xs={12} sm={6}>
               <Paper elevation={3} style={{padding:'18px'}}>
                  <Grid container>
                   <TextField id="outlined-basic" type="text"
                   fullWidth value={this.state.user} onChange={this.retriveValue}
                    label="Type Official Id" variant="outlined" />
                  </Grid>
                  <br />
                  <Button style={{float:'right'}} variant="contained" color="secondary"
                    onClick={this.authenticate}>View</Button>
                  <br />
                  {!(this.props.admin_action)&&
                    <Grid container>
                     <span style={{color:'green'}}>DISCLAIMER: </span>
                     Please Remember in this section you can only see the User's Data only if they are not coming in your same or upper designation level.
                     One more thing keep in mind users in medium level(like HOD , Professor) have only access to view datas within their department only.
                  </Grid>
                  }
                </Paper>
           </Grid>
            <Grid item xs={12} sm={3}/>
         </Grid>
         <div className="row">
             {this.state.details_view &&
               <DatasPass admin_action={this.props.admin_action} closeDecider={this.closeDecider}
               display={this.state.details_view}
               showAction="globaluser" username={this.state.send_user}/>
             }
         </div>
      </React.Fragment>
    );
   }
  }
}
