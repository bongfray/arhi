import React, { Component } from 'react';
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import { Redirect } from 'react-router-dom'
import {Collapsible, CollapsibleItem } from 'react-materialize'

export default class TaskManage extends Component {
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
      skills:[],
      div_no:'',
      skill_name:'',
      loader:true,
      redirectTo:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
   this.getUser();
  }
  getUser=()=> {
    axios.get('/ework/user2/getstudent').then(response=>{
      if (response.data.user){
         this.fetchSkills()
      }
      else
      {
        this.setState({loader:false,redirectTo:'/ework/student'});
        window.M.toast({html: 'Not Logged In !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
        })
      }

  fetchSkills =()=>{
    axios.post('/ework/user2/fetchall',{action:'Skills'})
    .then( res => {
        if(res.data)
        {
          this.setState({skills:res.data,loader:false})
        }
    });
  }

setDivOn =(div)=>{
  this.setState({div_no:div})
}

showDetails =(skill_name)=>{
  this.setState({
    render_detail:!this.state.render_detail,
    skill_name:skill_name,
  })
}

  render(){
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="5" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="73" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="140" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="210" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="280" y="35" rx="0" ry="0" width="50" height="18" />
        <rect x="350" y="35" rx="0" ry="0" width="45" height="18" />

        <rect x="5" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="73" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="140" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="210" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="280" y="75" rx="0" ry="0" width="50" height="18" />
        <rect x="350" y="75" rx="0" ry="0" width="45" height="18" />

      </ContentLoader>
      )
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else
     {
       if(this.state.loader)
       {
         return(
           <MyLoader />
         );
       }
       else {
          return(
            <React.Fragment>
              <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
                {this.state.render_detail && <ShowDetails detail={this.showDetails} skill_name={this.state.skill_name} />}
              <h4 className="center">Task-Manager</h4>
              <div className="row" style={{padding:'10px'}}>
              {this.state.skills.map((content,index)=>(
                <React.Fragment key={index}>
                <div className="col l2 xl2 m3 s4 go"  onClick={()=>{this.showDetails(content.skill_name)}} >
                  <h6 className="center card" style={{height:'50px',paddingTop:'15px'}}>{content.skill_name}
                  </h6>
                </div>
                </React.Fragment>
              ))}
              </div>
            </React.Fragment>
          );
        }
   }
  }
}

class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_completed:'',
      total_request:'',
      message:'',
      mailid:'',
      isChecked:'Begineer',
      active:'Begineer',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "pink go white-text";
      }
      return "go";
  }
  totalSet =(object)=>{
    this.setState(object);
  }

  render() {
    return (
      <div className="notinoti">
          <div className="right go" style={{padding:'8px'}} onClick={this.props.detail}><i className="small material-icons black-text">close</i></div>
          <h5 className="center">Complete Details of  <span className="pink-text">{this.props.skill_name}</span></h5>
          <div className="row">
              <div className="col l3">
                 <div className="row div-style">
                   <div className={'col l4 center particular '+this.color('Begineer')} onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</div>
                   <div className={'col l4 center particular '+this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</div>
                   <div className={'col l4 center particular '+this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</div>
                 </div>
              </div>
              <div className="col l6"/>
              <div className="col l3">
              <div className="col l6">
                 <div className="card">
                 <div className="red white-text center">PENDING TASK</div>
                 <div className="card-content center">
                  <h6>{this.state.total_request}</h6>
                 </div>
                 </div>
              </div>
              <div className="col l6">
                 <div className="card">
                 <div className="green white-text center">COMPLETED</div>
                 <div className="card-content center">
                  <h6>{this.state.total_completed}</h6>
                 </div>
                 </div>
              </div>
              </div>
          </div>
          <div className="row">
              <TaskHistory level={this.state.isChecked} skill_name={this.props.skill_name} totalSet={this.totalSet} />
          </div>
      </div>
    );
  }
}




class TaskHistory extends Component {
  constructor()
  {
    super()
    this.state={
      tasks:[],
      completed_tasks:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTasksFromAdmin()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.level !== this.props.level) {
      this.fetchTasksFromAdmin();
    }
  }

  fetchTasksFromAdmin =()=>{
    axios.post('/ework/user2/fetch_in_admin',{usertype:this.props.level,action:'Task '+this.props.skill_name})
    .then( res => {
        if(res.data)
        {
          this.fetchCompletedHistory(res.data)
        }
    });
  }
  fetchCompletedHistory = (adminTasks) =>{
    axios.post('/ework/user2/fetchTasks',{level:this.props.level,action:'Task '+this.props.skill_name})
    .then( res => {
        if(res.data)
        {

          const tasks = adminTasks.filter(item =>{
            return !res.data.find(citem => citem.referTask === item.serial);
          });

          this.setState({
            tasks:tasks,
            completed_tasks:res.data,
            loading:false,
          })
          this.props.totalSet({
            total_request:tasks.length,
            total_completed:res.data.length,
          })
        }
    });
  }

  callEdit =(key) =>{
    const referTaskDetails = this.state.completed_tasks.filter(item => item.referTask=== key);
    this.setState({
      display_on:!this.state.display_on,
      referTaskDetails:referTaskDetails,
    })
  }
  deleteTask=(key)=>{
    window.M.toast({html: 'Deleting !!',outDuration:'9000', classes:'rounded red'});
    const { completed_tasks } = this.state;
    axios.post('/ework/user2/deleteTask',{key:key})
    .then(res=>{
      if(res.data === 'del'){
        this.setState({
            completed_tasks: completed_tasks.filter(items => items.referTask !== key)
        })
        window.M.toast({html: 'Deleted !!',outDuration:'9000', classes:'rounded red'});
        this.fetchTasksFromAdmin();
      }
    })
  }

  render() {
    if(this.state.loading)
    {
      return(
        <div className="center">
            <div className="preloader-wrapper big active">
            <div className="spinner-layer spinner-red">
                <div className="circle-clipper left">
                 <div className="circle"></div>
                </div><div className="gap-patch">
                 <div className="circle"></div>
                </div><div className="circle-clipper right">
                 <div className="circle"></div>
                </div>
            </div>
            </div>
        </div>
      )
    }
    else
    {
    return (
      <React.Fragment>
      {this.state.display_on &&  <FloatingWindow fetchTasksFromAdmin={this.fetchTasksFromAdmin} callEdit={this.callEdit} referTaskDetails={this.state.referTaskDetails} />}
      {this.props.level &&
        <React.Fragment>
          {this.state.tasks.length>0 ?
        <div>
        <div className="row">
          <div className="col l1 xl1 s1 m1 center"><b>Task-No</b></div>
          <div className="col l5 xl5 m6 s6 center"><b>Description of the Task</b></div>
          <div className="col l4 xl4 s3 m3 center"><b>Link to Refer</b></div>
           <div className="col l2 xl2 s2 m2 center"><b>Status</b></div>
        </div>
        <hr />
        {this.state.tasks.map((content,index)=>(
          <React.Fragment key={index}>
           <div className="row">
             <div className="col l1 xl1 s1 m1 center">T{content.serial}</div>
             <div className="col l5 xl5 m6 s6 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.description_of_the_task}</div>
             <div className="col l4 xl4 s3 m3 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.ref_for_task}</div>
              <div className="col l2 xl2 s2 m2 center"><Complete fetchTasksFromAdmin={this.fetchTasksFromAdmin} level={this.props.level} skill_name={this.props.skill_name} serial={content.serial} /></div>
           </div>
             <hr />
          </React.Fragment>

        ))}
        </div>
        :
        <h5 className="center">No Pending Task Found !!</h5>
      }
        <Collapsible popout>
          <CollapsibleItem
            header={
                  <h6 className="collaphead">COMPLETED TASKS</h6>
            }>
            {this.state.completed_tasks.length>0 ?
            <div className="hello">
            <div className="row">
              <div className="col l1 xl1 s1 m1 center"><b>Task-No</b></div>
              <div className="col l4 xl4 m6 s6 center"><b>Description</b></div>
              <div className="col l3xl3 s3 m3 center"><b>Links</b></div>
              <div className="col l3xl3 s3 m3 center"><b>Duration</b></div>
               <div className="col l1 xl1 s2 m2 center"><b>Action</b></div>
            </div>
            <hr />
            {this.state.completed_tasks.map((content,index)=>(
              <React.Fragment key={index}>
               <div className="row">
                 <div className="col l1 xl1 s1 m1 center">T{content.referTask}</div>
                 <div className="col l4 xl4 m6 s6 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.description}</div>
                 <div className="col l3 xl3 s3 m3 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.link}</div>
                 <div className="col l3 xl3 s3 m3 center" style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>{content.start_date}-{content.end_date}</div>
                  <div className="col l1 xl1 s2 m2">
                         <i className="material-icons small go green-text" onClick={()=>this.callEdit(content.referTask)}>edit</i>
                         <i className="material-icons small go red-text" onClick={()=>this.deleteTask(content.referTask)}>delete</i>
                  </div>
               </div>
                 <hr />
              </React.Fragment>

            ))}
            </div>
            :
              <h5 className="center">No Completed Task Found !!</h5>
            }
          </CollapsibleItem>
        </Collapsible>
        </React.Fragment>
      }

      </React.Fragment>
    );
   }
  }
}


class Complete extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display_overlay:false,
      level:'',
      skill_name:'',
      serial:'',
    }
  }

  handleChange =(level,skill_name,serial) =>{
    this.setState({
      display_overlay:!this.state.display_overlay,
      level:level,
      skill_name:skill_name,
      serial:serial,
    })
  }
  render()
  {
    return(
      <React.Fragment>
      {this.state.display_overlay &&
         <FloatingWindow fetchTasksFromAdmin={this.props.fetchTasksFromAdmin} handleChange={this.handleChange} level={this.state.level} skill_name={this.state.skill_name} serial={this.state.serial}/>
      }
        <p>
          <label>
           <input type="checkbox" className="filled-in" checked={this.state.blue_allot} value={this.state.blue_allot} onChange={() => this.handleChange(this.props.level,this.props.skill_name,this.props.serial)}/>
           <span className="black-text">Completed ?</span>
          </label>
        </p>
      </React.Fragment>
    )
  }
}

class  FloatingWindow extends Component {
  constructor()
  {
    super()
    this.state={
      start_date:'',
      end_date:'',
      link:'',
      description:'',
      level:'',
      skill_name:'',
      serial:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.putData()
  }
  putData =()=>{
    if(this.props.referTaskDetails){
      this.setState({
        start_date:this.props.referTaskDetails[0].start_date,
        end_date:this.props.referTaskDetails[0].end_date,
        description:this.props.referTaskDetails[0].description,
        link:this.props.referTaskDetails[0].link,
        level:this.props.level,
        skill_name:this.props.skill_name,
        serial:this.props.referTaskDetails[0].referTask,
      })
    }
    else{
      this.setState({
        level:this.props.level,
        skill_name:this.props.skill_name,
        serial:this.props.serial,
      })
    }
  }

  retriveData =(e)=>{
    this.setState({[e.target.name]:e.target.value})
  }
  submitTask =(e)=>{
    e.preventDefault()
    if((!this.state.start_date) || (!this.state.end_date) || (!this.state.description))
    {
        window.M.toast({html: 'Fill up the required fileds !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    }
    else{
      axios.post('/ework/user2/send_task',{
        start_date:this.state.start_date,
        end_date:this.state.end_date,
        description:this.state.description,
        level:this.state.level,
        task_name:this.state.skill_name,
        link:this.state.link,
        referTask:this.state.serial,
      })
      .then(res=>{
         if(res.data === 'done')
         {
           window.M.toast({html: 'Saved !!',outDuration:'9000', classes:'rounded green darken-1'});
           this.props.handleChange();
           this.props.fetchTasksFromAdmin();
         }
         else if(res.data === 'updated')
         {
           window.M.toast({html: 'Updated !!',outDuration:'9000', classes:'rounded green darken-2'});
           this.props.callEdit();
            this.props.fetchTasksFromAdmin();
         }
      })
    }
  }
  render()
  {
    console.log(this.props.referTaskDetails)
    return(
      <React.Fragment>
         <div className="cover_all">
           <div className="up anit">
             {this.props.callEdit ?
               <div className="right go" style={{padding:'10px'}} onClick={this.props.callEdit}>
                  <i className="material-icons small">close</i>
               </div>
               :
               <div className="right go" style={{padding:'10px'}} onClick={this.props.handleChange}>
                  <i className="material-icons small">close</i>
               </div>
             }
             <br />
             {this.props.handleChange && <h6 className="center">Details of the solution on {this.props.skill_name} in {this.props.level} Level</h6>}
             <br />
             <div style={{padding:'20px'}}>
               <label className="pure-material-textfield-outlined alignfull">
                 <textarea
                   className=""
                   type="text"
                   placeholder=" "
                   min="10"
                   max="50"
                   name="description"
                   value={this.state.description}
                   onChange={this.retriveData}
                 />
                 <span>Description of your work*</span>
               </label>
               <br />
               <div className="input-field">
                  <input type="text" id="link" value={this.state.link}  name="link" onChange={this.retriveData} />
                  <label htmlFor="link">Enter any link of your work</label>
               </div>
               <div className="row">
                  <div className="col l6 xl6">
                    <div className="input-field">
                       <input type="text" id="start_date" value={this.state.start_date} name="start_date" onChange={this.retriveData} className="validate" required />
                       <label htmlFor="start_date">Enter the Start Date *</label>
                    </div>
                  </div>
                  <div className="col l6 xl6">
                    <div className="input-field">
                       <input type="text" id="end_date" value={this.state.end_date} name="end_date" onChange={this.retriveData} className="validate" required />
                       <label htmlFor="end_date">Enter the end Date *</label>
                    </div>
                  </div>
               </div>
                <div className="row">
                  <div className="col l8 xl8">
                     <span className="red-text">This Datas will be added to your resume as your work.</span>
                  </div>
                  <div className="col l4 xl4">
                      <button type="submit" onClick={this.submitTask} className="btn btn-small right">Submit Data</button>
                  </div>
               </div>
               </div>
           </div>
         </div>
      </React.Fragment>
    )
  }
}
