import React, { Component } from 'react'
import axios from 'axios'
import AddProduct from './add_datas';
import ProductList from './show_info'
var empty = require('is-empty');

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:'',

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     console.log(data)
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/ework/user2/editt';
     } else {
       apiUrl = '/ework/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  oneEntry = (object)=>{
    this.setState(object);
  }

   render() {
     let productForm,title,description;
     var data;
             title ='Contact Details';
             data = {
               Action:'Personal_Details',
               button_grid:'col l1 m1 s1 center',
               fielddata: [
                 {
                   header: "Name",
                   name: "name",
                   placeholder: "Enter your name",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Email ID",
                   name: "emailid",
                   placeholder: "Enter your mailId",
                   type: "email",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Gender",
                   name: "gender",
                   placeholder: "Enter your gender",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "Mobile No.",
                   name: "mobileno",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Mobile No."
                 },
                 {
                   header: "Address",
                   name: "address",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Permanent Address"
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div>
  <div>
    <div>
  <div>
    {!this.state.isAddProduct && <ProductList oneEntry={this.oneEntry} username={this.props.username} title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 left">
  </div>
     <div className="col l6">
       <button disabled={this.state.disabled} className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>
  </div>
</div>
  </div>
);
}
}
