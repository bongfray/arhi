import React from 'react';
import Error from './error3.png'
import { Link } from 'react-router-dom';
import './styles.css';
class Not extends React.Component{
    render(){
        return (
            <React.Fragment>  
                <div className="row">
                    <div className="img">
                    <img src={Error} className="image"/>
                    </div>
                </div>
                <div className="row fof">
                        
                        <h1>Oops! You took a wrong turn...</h1>
                </div>
                <div className="row center">
                    <Link to="/ework"><button className="button-home">Go to Home Page</button></Link>
                    
                </div>
            </React.Fragment>
        )
    }   
}

export default Not;