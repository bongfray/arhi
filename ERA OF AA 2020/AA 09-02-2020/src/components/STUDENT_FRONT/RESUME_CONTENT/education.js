import React, { Component } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';



 export default class SelectTor extends Component {
   constructor(){
     super()
     this.state={
       degree:'',
     }
   }
   handleInput =(e)=>{
     this.setState({degree:e.target.value})
   }

  render() {
    return (
      <React.Fragment>
           <Grid container spacing={1}>
              <Grid item xs={3} sm={3}>
                  <FormControl style={{width:'100%'}}>
                    <InputLabel id="sel1_type">Select Here</InputLabel>
                      <Select
                        labelId="sel1_type"
                        id="sel1_type"
                        name="skill_level" value={this.state.degree} onChange={this.handleInput}
                      >
                      <MenuItem value="10th Standard">10th Standard</MenuItem>
                      <MenuItem value="12th Standard">12th Standard</MenuItem>
                      <MenuItem value="Graduation">Graduation</MenuItem>
                      <MenuItem value="Post Graduation">Post Graduation</MenuItem>
                      <MenuItem value="PhD">PhD</MenuItem>
                      <MenuItem value="Diploma">Diploma</MenuItem>
                      </Select>
                   </FormControl>
               </Grid>
             </Grid>
           <br />
            {this.state.degree &&  <Gg  degree={this.state.degree} username={this.props.username} /> }


      </React.Fragment>
    );
  }
}


 class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:false,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user2/editt';
     } else {
       apiUrl = '/ework/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })

   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  oneEntry = (object)=>{
    this.setState(object);
  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

   render() {
     console.log(this.state)
     let productForm,title,description;
     var data;
             title ='Education Details';
             data = {
               Action:this.props.degree,
               fielddata: [
                 {
                   header: "Status",
                   name: "status_of_education",
                   placeholder: "radio",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "College Name/School name",
                   name: "college",
                   placeholder: "Enter your college or school Name",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                  header: "Board Name",
                  name: "board",
                  placeholder: "Enter your Board Name",
                  type: "text",
                  grid:'col l2 m2 s2 center',
                },
                 {
                   header: "Start Year",
                   name: "start_year",
                   placeholder: "Enter the Start Year",
                   type: "number",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "End Year",
                   name: "end_year",
                   type: "number",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter the End Year"
                 },
                 {
                   header: "Degree",
                   name: "degree",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Degree(like B.Tech)"
                 },
                 {
                   header: "Stream",
                   name: "stream",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Stream(like Software Engieneer)"
                 },
                 {
                   header: "Percentage/CGPA",
                   name: "cgpa_percentage",
                   type: "number",
                   grid: 'col l2 m2 s2 center',
                   placeholder: "Enter Your Percentage/CGPA"
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} username={this.props.username} action={this.props.degree} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div>
    {!this.state.isAddProduct &&
      <ProductList  username={this.props.username} title={title} description={description}
      action={this.props.options} data={data}  editProduct={this.editProduct}/>
    }
    <br />
    {!this.state.isAddProduct &&
     <React.Fragment>
       <Grid container spacing={1}>
          <Grid item xs={6} sm={6}/>
          <Grid item xs={5} sm={5}>
           <Button variant="contained" style={{float:'right'}} color="secondary" onClick={(e) => this.onCreate(e,this.props.options)} disabled={this.state.disabled}>
            Add Data
            </Button>
          </Grid>
          <Grid item xs={1} sm={1}/>
      </Grid>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>

);
}
}


 class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      status:'',
      start_year:'',
      end_year:'',
      college:'',
      board:'',
      cgpa_percentage:'',
      stream:'',
      degree:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      if((!this.state.college) || (!this.state.board) || (!this.state.status) || (!this.state.cgpa_percentage) || (!this.state.end_year))
      {
        window.M.toast({html: 'Enter all the fields!!',classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
      }
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  handleChecked =(e)=>{
      this.setState({
          status:e.target.value
      });
      if(e.target.value === 'Pursuing')
      {
        this.setState({end_year:e.target.value})
      }
      else{
        this.setState({end_year:''})
      }
  }

  stateSet=(object)=>{
    this.setState(object);
  }
  status =(stat)=>{
    if(this.state.status === stat){
      return "checked";
    }
    else{
      return "";
    }
  }


  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS TO {this.props.data.Action}</h5>
}

    return(
      <Paper elevation={3} style={{padding:'10px'}}>
      <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{pageTitle} </Typography><br />
      <br />
          {this.props.data.fielddata.map((content,index)=>(
          <Grid container spacing={1} key={index}>
             <Grid item xs={1} sm={1}/>
             <Grid item xs={2} sm={2}>
                  {((this.props.data.Action==='10th Standard')||(this.props.data.Action==='12th Standard') )?
                      <React.Fragment>
                      {(content.name === 'degree' || content.name==='stream') ?
                        <div></div>
                        :
                        <div>
                           {content.header}
                        </div>
                      }
                      </React.Fragment>
                      :
                      <div>{content.header}</div>
                  }
              </Grid>
            <Grid item xs={8} sm={8}>
                  <React.Fragment>
                     {content.placeholder === "radio" ?
                     <React.Fragment>
                         <FormControl component="fieldset" style={{padding:'5px'}}>
                           <RadioGroup aria-label="gender" name="gender1" value={this.state.status}  onChange={this.handleChecked}>
                              <Grid container spacing={1}>
                                 <Grid item sm={6} xs={6}>
                                    <FormControlLabel value="Pursuing" control={<Radio />}
                                    style={{color:'black'}} label="Pursuing" />
                                 </Grid>
                                 <Grid item xs={6} sm={6}>
                                    <FormControlLabel value="Completed" control={<Radio />}
                                    style={{color:'black'}} label="Completed" />
                                 </Grid>
                               </Grid>
                           </RadioGroup>
                         </FormControl>
                     </React.Fragment>
                      :
                       <React.Fragment>
                       {content.name === 'end_year' ?
                         <div>
                          {this.state.status === "Pursuing" ?
                              <div>
                                  <TextField
                                    name={content.name}
                                    defaultValue="Pursuing"
                                    disabled
                                    variant="filled"
                                    fullWidth
                                  />
                              </div>
                             :
                             <TextField
                               type={content.type}
                               id="outlined-textarea"
                               label={content.placeholder}
                               name={content.name}
                               multiline
                               value={this.state[content.name]}
                               onChange={e => this.handleD(e, index)}
                               variant="filled"
                               fullWidth
                             />
                           }
                           </div>
                           :
                           <div>
                           {((this.props.data.Action==='10th Standard')|| (this.props.data.Action==='12th Standard') ) ?
                                 <div>
                                 {(content.name === 'degree' || content.name==='stream') ?
                                   <div></div>
                                   :
                                   <TextField
                                     type={content.type}
                                     id="outlined-textarea"
                                     label={content.placeholder}
                                     name={content.name}
                                     multiline
                                     value={this.state[content.name]}
                                     onChange={e => this.handleD(e, index)}
                                     variant="filled"
                                     fullWidth
                                   />
                                 }
                                 </div>
                                 :
                                 <TextField
                                   type={content.type}
                                   id="outlined-textarea"
                                   label={content.placeholder}
                                   name={content.name}
                                   multiline
                                   value={this.state[content.name]}
                                   onChange={e => this.handleD(e, index)}
                                   variant="filled"
                                   fullWidth
                                 />
                          }
                          </div>
                         }
                       </React.Fragment>
                     }
                  </React.Fragment>

            </Grid>

               <Grid item xs={1} sm={1}/>
          </Grid>
          ))}

          <Grid container spacing={1}>
            <Grid item xs={9} sm={9} />
            <Grid item xs={1} sm={1}>
              <Button variant="outlined" onClick={this.props.cancel} color="secondary">CANCEL</Button>
            </Grid>
            <Grid item xs={2} sm={2}>
             <Button variant="contained" onClick={this.handleSubmit} style={{backgroundColor:'#455a64',color:'white'}} >SUBMIT</Button>
            </Grid>
          </Grid>
      </Paper>
    )
  }
}


 class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      productId:'',
      render_confirm:false,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.data.Action !== this.props.data.Action) {
      this.fetch();
    }
  }

    deleteProduct = (productId,index) => {
      this.setState({render_confirm:!this.state.render_confirm,productId:productId});
  }

  confirm=()=>{
    let delUrl;
    if(this.props.data.Action === 'FACULTY LIST'){
      delUrl = '/ework/user2/del_faculty_list';
    } else {
      delUrl = '/ework/user2/del';
    }

  const { products } = this.state;
  axios.post(delUrl,{
    serial: this.state.productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          render_confirm:false,
          productId:0,
          response: response,
          products: products.filter(product => product.serial !== this.state.productId)
       })
      })
  }

fetch =() =>{
  axios.post('/ework/user2/fetchall',{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}



  render() {
    const {  products} = this.state;
      return(
        <React.Fragment>
        <Dialog
          open={this.state.render_confirm}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Cnfirmation of Deleting the Data</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              After you click agree button below, your all rating datas related to this content will be affected.
              So kindly take decision what do you really want to do .
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.deleteProduct} color="primary">
              Disagree
            </Button>
            <Button onClick={this.confirm} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>

            <Typography variant="h5" style={{textAlign:'center'}} gutterBottom>{this.props.title}</Typography><br />
            <Grid container spacing={1}>
              {products.map((product,index) => (
                <Grid item xs={4} sm={4} key={index}>
                  <Paper elevation={3} style={{padding:'12px'}}>
                    <Grid container spacing={1}>
                        <Grid xs={10} sm={10}>
                                 <div>Degree : {product.action}</div><br />
                                 <div>College Name : {product.college}</div><br />
                                 <div>Board Name : {product.board}</div><br />
                                 <div>Duration: {product.start_year} - {product.end_year}</div><br />
                                 {product.degree &&
                                 <React.Fragment>
                                   <div>Degree : {product.degree}</div><br />
                                 </React.Fragment>

                                 }
                                 {product.stream &&
                                 <React.Fragment>
                                    <div>Stream: {product.stream}</div><br />
                                 </React.Fragment>
                                 }
                                 <div>Percentage/CGPA: {product.cgpa_percentage}</div><br />
                        </Grid>
                        <Grid item xs={2} sm={2}>
                            <div style={{float:'right'}}>
                              <EditIcon className="go"  onClick={() => this.props.editProduct(product.serial,this.props.action)} />&nbsp;
                              <DeleteIcon className="go" style={{color:'red'}} onClick={() => this.deleteProduct(product.serial,this.props.action)} />
                            </div>
                        </Grid>
                    </Grid>
                    </Paper>
                </Grid>
              ))}
            </Grid>
        </React.Fragment>
      )

  }
}
