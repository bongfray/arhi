import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import ContentLoader from "react-content-loader"
import RegistrationStopped from './stop'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';

var empty = require('is-empty');

export default class Signup extends Component {
	constructor() {
    super()
    this.initialState = {
			display:'block',
			status:'none',
			loader:true,
			redirectTo: null,
			h_order:'',
			title: '',
			name: '',
			mailid: '',
			username: '',
			phone: '',
			password: '',
			cnf_pswd: '',
			campus: '',
			dept: '',
			desgn: '',
		  dob:'',
			err:'',
			show_modal:false,
			color:'green-text',
			designation:[],
			department:[],
	}
	 this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    }
		componentDidMount(){
			this.fetchrender();
		}

fetchrender =() =>{
	axios.get('/ework/user/fetch_render_status')
	.then(res=>{
		this.setState({loader:false})
		if(res.data)
		{
			if(res.data=== true)
			{
				this.setState({status:"block"})
				this.fetchDesig();
			}
			else
			{
				this.setState({status:'none'})
			}
		}
	})
}

		fetchDesig =() =>{
		  axios.post('/ework/user/fetch_designation')
		  .then(res => {
		      if(res.data)
		      {

						        const designation = res.data.filter(item =>
						        item.action ==='Designation');
										const department = res.data.filter(item =>
										item.action ==='Department');
		        this.setState({designation:designation,department:department})
		      }
		  });
		}

		handleField = (e) =>{
				this.setState({
					[e.target.name] : e.target.value,
				})
		}


					handleDesgn = (e) => {
						let order;
						if(e.target.value === "Director")
						{
							order = 1;
							this.setState({display:'none'});
						}
						else if(e.target.value === "Principle")
						{
							order = 2;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Assistant Director")
						{
							order =3;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Dean")
						{
							order =4;
							this.setState({display:'block'});
						}
						else if(e.target.value === "HOD")
						{
							order =5;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Professor")
						{
							order =6;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Assistant Professor")
						{
							order =7;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Associate Professor")
						{
							order = 8;
							this.setState({display:'block'});
						}
						this.setState({
								desgn: e.target.value,
								h_order: order,
						});
			    }


	handleSubmit(event) {
		event.preventDefault()
		if(empty(this.state.title)||empty(this.state.name)||empty(this.state.mailid)||empty(this.state.username)||empty(this.state.phone)||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.desgn))
		{
			window.M.toast({html: 'Enter all the Details',outDuration:'3000', classes:'rounded red'});
      return false;
		}
		else if(!(this.state.password.match(/[a-z]/g) && this.state.password.match(
							/[A-Z]/g) && this.state.password.match(
							/[0-9]/g) && this.state.password.match(
							/[^a-zA-Z\d]/g) && this.state.password.length >= 6))
		{
				this.setState({err:'Follow Correct Format!!',color:'red-text'})
				this.showValidation();
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
			this.setState({error:'Password does not match !!',cnf_pswd:''})
	        return false;
	  }
		else if ((this.state.phone).length!==10)
		{
			this.setState({error:'Verify your phone no !!',phone:''})
			return false;
		}
		else{
			window.M.toast({html: 'Signing UP....',outDuration:'3000', classes:'rounded violet'});
		axios.post('/ework/user/', {
			h_order: this.state.h_order,
			username: this.state.username,
			password: this.state.password,
			title: this.state.title,
			name: this.state.name,
			mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
			dept: this.state.dept,
			desgn:this.state.desgn,
			dob: this.state.dob,
		})
			.then(response => {
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, classes:'rounded #ba68c8 purple lighten-2'});
				  }
					else if(response.data.succ)
					{
						window.M.toast({html: response.data.succ, classes:'rounded #ba68c8 purple lighten-2'});
						this.setState({
								redirectTo: '/ework/flogin'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'2000', classes:'rounded red'});
			})
			this.setState({loader:false})
	}
}

showValidation=()=>{
	this.setState({show_modal:!this.state.show_modal})
}

render() {
	const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
        </ContentLoader>
      )
			if(this.state.redirectTo) {
					 return <Redirect to={{ pathname: this.state.redirectTo }} />
			 } else {
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status === "none")
	{
		return(
			<RegistrationStopped login_path="/flogin" section_name="FACULTY REGISTRATION"/>
		);
	}
	else{
	return (
		<div className="row">
		<div className="col s2 l2 m2 xl2" />
		<div className="col l8 xl8 s12 m12 form-signup">
				<div className="center">
						<h5 className="reg">REGISTRATION</h5>
				</div>
				<form className="row form-con">
						<div className="row">
								<div className="col s6 l2 xl2 m6">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="title">Salutation</InputLabel>
												<Select
													labelId="title"
													id="title"
													name="title" value={this.state.title} onChange={this.handleField}
												>
												<MenuItem value="Mr.">Mr.</MenuItem>
												<MenuItem value="Mrs.">Mrs.</MenuItem>
												<MenuItem value="Miss.">Miss.</MenuItem>
												<MenuItem value="Dr.">Dr.</MenuItem>
												</Select>
										 </FormControl>
								</div>

								<div className="input-field col s6 l6 xl6 m6">
								<input id="name" name="name" type="text" className="validate" value={this.state.name} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter your name" data-success=""></span>
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s6 l4 xl4 m6">
								<input id="fac_id" name="username" type="text" className="validate" value={this.state.username} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success=""></span>
								<label htmlFor="fac_id">Official ID</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6 l5 xl5 m6">
								<input id="email" name="mailid" type="email" className="validate" value={this.state.mailid} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success="">Email id should be Official Mail Id</span>
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s6 l4 xl4 m6">
								<input id="ph_num" type="text" name="phone" className="validate" value={this.state.phone} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success="">Phone number should be of 10 digit.</span>
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="input-field col s6 l3 xl3 m6">
								<input id="dob" type="text" name="dob" className="validate" value={this.state.dob} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success="">Format: dd/mm/yyyy</span>
								<label htmlFor="dob">D.O.B.</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6 xl6 l6 m6">
								<input onChange={this.handleField}  name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								<div>
								<HelpOutlineIcon style={{color:'green'}} onClick={this.showValidation} />
								 <Dialog
		               open={this.state.show_modal}
									 onClick={this.showValidation}
		               aria-labelledby="alert-dialog-title"
		               aria-describedby="alert-dialog-description"
		             >
		               <DialogTitle id="alert-dialog-title">Password Should Consist of </DialogTitle>
		               <DialogContent>
		                 <DialogContentText id="alert-dialog-description">
											 <Typography variant="button" display="block" gutterBottom>
											 At least 1 uppercase character.<br />
											 At least 1 lowercase character.<br />
											 At least 1 digit.<br />
											 At least 1 special character.<br />
											 Minimum 6 characters.<br />
								       </Typography>
		                 </DialogContentText>
		               </DialogContent>
		               <DialogActions>
		                 <Button onClick={this.showValidation} color="primary" autoFocus>
		                   Agree
		                 </Button>
		               </DialogActions>
		             </Dialog>
								</div>
								<span className="red-text"><p>{this.state.err}</p></span>
								</div>
								<div className="input-field col s6 xl6 m6 l6">
								<input  onChange={this.handleField} id="cnf_pswd" name="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								<span className="helper-text" data-error="Please enter data!!" data-success=""></span>
								</div>

						</div>
						<div className="row">

								<div className="col l4 s6 m6 xl4">
								<FormControl style={{width:'100%'}}>
									<InputLabel id="campus">Campus</InputLabel>
										<Select
											labelId="campus"
											id="campus"
											value={this.state.campus} name="campus" onChange={this.handleField}
										>
										<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
										<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
										<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
										<MenuItem value="NCR Campus">NCR Campus</MenuItem>
										</Select>
								 </FormControl>
								</div>
								<div className="col l4 s6 m6 xl4">
								 <FormControl style={{width:'100%'}}>
									 <InputLabel id="desgn">Designation</InputLabel>
										 <Select
											 labelId="desgn"
											 id="desgn"
											 value={this.state.desgn} onChange={this.handleDesgn}
										 >
													 {this.state.designation.map((content,index)=>{
														 return(
															 <MenuItem key={index} value={content.designation_name}>{content.designation_name}</MenuItem>
														 )
											 })}
											 </Select>
	 								 </FormControl>
								</div>
								{this.state.display === 'block' ?
								<div className="col l4 s6 m6 xl4">
								<FormControl style={{width:'100%'}}>
									<InputLabel id="dept">Department</InputLabel>
										<Select
											labelId="dept"
											id="dept"
											value={this.state.dept} name="dept" onChange={this.handleField}
										>
													{this.state.department.map((content,index)=>{
														return(
															<MenuItem key={index} value={content.department_name}>{content.department_name}</MenuItem>
														)
											})}
											</Select>
									</FormControl>
								</div>
								:
								<div className="col l4 s6 m6 xl4">
								<FormControl style={{width:'100%'}}>
									<InputLabel id="dept">Department</InputLabel>
										<Select
											labelId="dept"
											id="dept"
											value={this.state.dept} name="dept" onChange={this.handleField}
										>
												<MenuItem value="E & T">E & T</MenuItem>
												<MenuItem value="Medical">Medical</MenuItem>
											</Select>
									</FormControl>
								</div>
							}
						</div>
						<br/>
						<div className="row"><div className="col l4 m4 s4 xl4 left">
							<Link to='/ework/flogin' className="log">Login Instead ?</Link></div>
							<div className="col l4 s4 xl4 m4 center" style={{color:'red'}}>{this.state.error}</div>
							<div className="col l4 s4 m4 xl4">
							<button className="waves-effect btn blue-grey darken-2 sup" style={{width:'100%'}} onClick={this.handleSubmit}>Submit</button>
							</div>
						</div>
				</form>
		</div>
		</div>

	);
}
}
}
}
}
