
import React from 'react';

export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      val:'',
      link:'',
      action:'',
      usertype:'',
      username:'',
      skill_name:'',
      description_of_the_task:'',
      ref_for_task:'',
      course_to_learn:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


  componentDidMount(){
    this.setState({
      action:this.props.action,
      usertype:this.props.level,
      inserted_by:this.props.username,
    })
  }
  stateSet=(object)=>{
    this.setState(object);
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
    <React.Fragment>
      <div className="row">
        <div className="col s12 l12" >
          {this.props.data.fielddata.map((content,index)=>(
            <div className="col l6 s6 m6" key={index}>
            {!(content.name === 'course_to_learn') ?
            <label className="pure-material-textfield-outlined alignfull">
              <textarea
                id={content.name}
                className=""
                type={content.type}
                placeholder=" "
                min="10"
                max="50"
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              />
              <span>{content.placeholder}</span>
            </label>
            :
            <div>
              <SearchEngine stateSet={this.stateSet} field={this.props.saved_skill} />
            </div>
             }
            </div>
          ))}
        </div>
        </div>
        <div className="row">
          <div className="col l9 xl9 hide-on-mid-and-down" />
          <div className="col l3 xl3 s12">
            <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
            <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
          </div>
        </div>

      </React.Fragment>
    )
  }
}

class SearchEngine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchString: "",
      users: []
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.setState({
      users: this.props.field
    });
    this.refs.search.focus();
  }

  handleChange() {
    this.setState({
      searchString: this.refs.search.value
    });
  }

  handleLevel = (e) =>{
    this.setState({searchString:e})
    this.props.stateSet({
      course_to_learn:e,
    })
  }

  render() {
    let _users = this.state.users;
    let search = this.state.searchString.trim().toLowerCase();

    if (search.length > 0) {
      _users = _users.filter(function(user) {
        return user.skill_name.toLowerCase().match(search);
      });
    }

    return (
      <div>
        <div>
          <input
            type="text"
            value={this.state.searchString}
            ref="search"
            onChange={this.handleChange}
            placeholder="Search Skill Here"
          />
          {this.state.searchString &&
            <div>
            {_users.map((l,index) => {
              return (
                <div key={index} style={{padding:'8px'}} className="col l3 xl3 m4 s6 center" onClick={()=>{this.handleLevel(l.skill_name)}}>{l.skill_name}</div>
              );
            })}
            </div>
          }
        </div>
      </div>
    );
  }
}
