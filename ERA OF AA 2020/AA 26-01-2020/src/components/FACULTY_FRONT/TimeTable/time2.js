import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from './colordiv.js'
import Insturction from './instructionmodal'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import HandOverSlot from './handOverSlot'
require("datejs")

/*-------------------------------------------------Clock for Current Date and time------------------- */

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <span>{new Date().toDateString()}</span>
      <p>{this.state.time}</p>
      </div>
    );
  }
}



/*-----------------------------------------Automatically Chanege the day order-------------------------------*/


class DayOrder extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{
_isMounted = false;
  constructor() {
    super();
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      loading:true,
      opendir: '',
      modal: false,
      display:'block',
      saved_dayorder:'',
      redirectTo:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
  }

  getCount =() =>{
    axios.get('/ework/user/knowcount', {
    })
    .then(response =>{
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  getDayOrder = () =>{
    axios.get('/ework/user/fetchdayorder')
    .then(response =>{

    if (this._isMounted) {
        this.setState({loading:false})
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/ework/',
        });
        window.M.toast({html: 'You are not Logged In', classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
        if(response.data.day_order===0)
        {
          this.setState({saved_dayorder:'0',display:'none'})
        }
        else
        {
          this.setState({
            saved_dayorder: response.data.day_order,
          });
        }
      }
    }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
      this._isMounted = true;
    M.AutoInit();
    this.getDayOrder();
    this.getCount();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };

      render(){
        const MyLoader = () => (
          <ContentLoader
            height={160}
            width={400}
            speed={2}
            primaryColor="#f3f3f3"
            secondaryColor="#c0c0c0"
          >
            <rect x="10" y="8" rx="5" ry="5" width="80" height="30" />
            <rect x="140" y="30" rx="3" ry="3" width="250" height="120" />

            <rect x="10" y="70" rx="5" ry="5" width="80" height="30" />
            <rect x="10" y="120" rx="1" ry="1" width="80" height="15" />


          </ContentLoader>
        )
        if(this.state.loading === true)
        {
          return(
            <MyLoader />
          );
        }
        else{
        if (this.state.redirectTo) {
             return <Redirect to={{ pathname: this.state.redirectTo }} />
         } else {

        return(
          <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          {this.state.modal=== false &&
            <div onClick={this.selectModal} className="right animat bounce" style={{margin:'20px 20px 0px 0px',cursor: 'pointer'}}>
              <i className="material-icons small red-text">info_outline</i>
            </div>
          }
          <div style={{padding:'20px'}}>
            <Insturction
                displayModal={this.state.modal}
                closeModal={this.selectModal}
            />
          <div className="row">
          <div className="col l2 s6 xl2 m2">
            <Clock />
          </div>
          <div className="col l10 s6 xl10 m10"><div className="row"><div className="col l4 s4 xl4 m4" />
            <p className="left tcr" style={{fontSize:'20px'}}>Today's DayOrder</p>
            </div>
          </div>
          </div>
          <div className="row">
          <div className="col l2 s12 xl2 m12">
          <Link to="/ework/time_yes" style={{color: 'black'}} >Go to Yesterday's Dayorder</Link>
              <DayOrder day_order={this.state.saved_dayorder}/>
            <br />
          <div style={{display:this.state.display,marginTop:'70px'}}>
                <select value={this.state.opendir}  onChange={this.handledir}>
                  <option value="" disabled defaultValue>Select Here</option>
                  <option value="r_class">Regular Class</option>
                  <option value="own_ab">Absent</option>
                </select>
            </div>
        </div>
        <div className="col l10 s12 m12 xl10">
          <Content opendir={this.state.opendir} day_order={this.state.saved_dayorder} usern={this.props.username}/>
        </div>
        </div>
          </div>
          </React.Fragment>
        );
      }
    }
      }
    }




/*-----------------------------------------------This class is for dynamically allocating div ------------- */

class Content extends Component{
  constructor(){
    super();
    this.state={
      saved_dayorder: '',
      day:'',
      month:'',
      year:'',
      redirectTo:'',
      username:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.getUser();
  }
  getUser =()=>{
    axios.get('/ework/user/')
    .then( res => {
        if(res.data.user){
          this.setState({username:res.data.user.username})
        }
    });
  }
  render(){
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.props.opendir==="r_class")
    {
          return(
            <React.Fragment>
             <ColorRep usern={this.state.username} day={day} month={month} year={year} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
         <Switch  day={day} month={month} year={year} day_order={this.props.day_order} />
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select from the DropDown</div>
      );
    }

}
  }
}





class Switch extends React.Component {

    constructor ( props ) {
        super( props );

		this.state = {
			isChecked: false,
      history:'',
      absent_reason:'',
      absent_state:false,
		}
    this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      axios.post('/ework/user/absentStatus',{day:this.props.day,month:this.props.month,year:this.props.year})
      .then( res => {
          if(res.data === 'ok')
          {
              this.setState({absent_state:true})
          }
      });
    }
    handleAbsentReason =(e)=>{
      this.setState({absent_reason:e.target.value})
    }
    submitAbsent =(e)=>{
      if(!this.state.absent_reason)
      {
        window.M.toast({html: 'Enter the reason !!',classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
      axios.post('/ework/user/absent_insert',{absent_reason:this.state.absent_reason})
      .then( res => {
        if(res.data)
        {
          this.setState({absent_state:true})
        }
      });
      }

    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
    }
    render () {

        return(
          <div>
          {this.state.absent_state === false ?
          <div className="row">
          <label className="pure-material-textfield-outlined alignfull">
            <textarea
              className="area"
              type="text"
              placeholder=" "
              min="10"
              max="60"
              value={this.state.absent_reason}
              onChange={this.handleAbsentReason}
            />
            <span>Enter Reason About the Absent</span>
          </label>
          <button className="btn right" onClick={this.submitAbsent}>SUBMIT</button>
          <div className="col l8">
          <span className="green-text"><b>NOTE : </b></span><span className="red-text"><b>If you are going to
          submit that you are absent today and have submitted any data today,in that case , all the submitted
           data(only Alloted Slots) will be deleted.
          </b></span>If you want to handover some of your allotted slots to
          other faculty,first submit the reason then only you will see that part.
          </div>
          </div>
          :
          <React.Fragment>
          <div className="row">
            <div className="col l6 switch center">
                <label style={{color:'red',fontSize:'15px'}}>
                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    Want to Handover some slots to Other Faculty?
                </label>
            </div>
            <div className="col l6">
            <span className="green-text"><b>REMEMBER : </b></span>You should enter some faculty's id and the slot,
            which should not come under their alloted slot,and must be in your alloted slot.One more thing,the
            faculty should be of same designation and in same campus.Whatever request you are making will be
            render for today only. Tommorow all the request you are making today,will be invalid. So request
            the respective faculty to complete within today.
            </div>
            </div>
            <br />
            <br /><br />
            <InputValue datas={this.state.isChecked}  day_order={this.props.day_order}/>
            </React.Fragment>
          }
          </div>

        );
    }

}

class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
    };
  }
  render(){
    if(this.props.datas === true)
    {
      return(
        <React.Fragment>
           <HandOverSlot  day_order={this.props.day_order}/>
        </React.Fragment>
      );
    }
    else{
      return(
        <React.Fragment>
        </React.Fragment>
      );
    }
  }
}
