import React, { Component } from 'react'
import { } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Al2'
import Free from './Free'


export default class Modal2 extends Component{
constructor(props){
  super(props);
  this.state = {
    rit:'',
    day_order:'',
    modal: false,
    allo1:false,
    loading: true,
    no1:'',

  };
  this.closeModal = this.closeModal.bind(this);
  this.componentDidMount = this.componentDidMount.bind(this);
}
handle1 = (e, index) => {
  // alert(e.target.value);
  // alert(e.target.name);
  const key = e.target.name;
  const value = e.target.value;
  this.setState({ rit: value, index ,day_order: key});

};
componentDidMount(){
  M.AutoInit();
  this.collectw();
}

componentDidUpdate =(prevProps)=>{
  if(prevProps !== this.props)
  {
      this.collectw()
  }
}

collectw =() =>
{
	axios.post('/ework/user/fetchme', {
		slot_time: this.props.day_order+this.props.day_slot_time,
	})
	.then(response =>{
		if(response.data)
		{
				this.setState({
					allo1:true,
          loading: false,
				})
		}
	})
}

    closeModal(e) {
        e.stopPropagation();
        this.props.closeModal()
     }
     render(){
       let allotw = this.state.allo1;
       const divStyle = {
            display: this.props.displayModal ? 'block' : 'none',
            padding:'20px',
       };
       let content;

           if(allotw)
           {
             content =
             <React.Fragment>

                                        <span className="drop">
                                          <TableDisplay color={this.props.color}
                                          cday={this.props.cday}
                                          day ={this.props.day}
                                          month ={this.props.month}
                                          year ={this.props.year}
                                          time ={this.props.time}
                                          slot={this.props.slot}
                                          day_sl={this.props.day_sl}
                                          selectValue="allot" day_slot_time={this.props.day_slot_time} day_order={this.props.day_order} usern={this.props.usern} closeModal={this.props.closeModal}
                                          />
                                        </span>
             </React.Fragment>

           }
           else{
             content =
             <React.Fragment>

                                        <span className="drop">
                                          <TableDisplay color={this.props.color}
                                          cday={this.props.cday}
                                          day ={this.props.day}
                                          month ={this.props.month}
                                          year ={this.props.year}
                                          time ={this.props.time}
                                          slot={this.props.slot}
                                          day_sl={this.props.day_sl}
                                          selectValue="free" day_slot_time={this.props.day_slot_time} day_order={this.props.day_order} usern={this.props.usern} closeModal={this.props.closeModal}
                                          />
                                        </span>
             </React.Fragment>
           }
     return (
       <React.Fragment>

       <div className="mobmodal" style={divStyle}>
          <div className="">
                 <button style={{marginBottom:'30px'}} className="float_cancel btn-floating btn-small waves-effect black  right" onClick={ this.closeModal }><i className="material-icons">cancel</i></button>
             <br /><br />
                  <div>
                    <div className="root-of-time" >
                    <div className="row">
                         <div className="col l12 s12 xl12 m12">

                         <div>
                               {content}
                         </div>
                         </div>


                         </div>

        </div>
                  </div>
          </div>
       </div>

</React.Fragment>
     );
   }
}



     class TableDisplay extends Component {
       constructor() {
         super()
         this.state = {
           allot:'',
             count: 0,
             selected:'',
             problem_conduct:'',
             saved_dayorder: '',
             day_recent:'',

         }
         }

       render() {
                     if (this.props.selectValue === "allot") {
                       return(
                         <div className="">

                         <Allot
                         cday={this.props.cday}
                         slot={this.props.slot}
                         time={this.props.time}
                         day ={this.props.day}
                         month ={this.props.month}
                         year={this.props.year}
                         day_sl={this.props.day_sl}
                          color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                         </div>
                       );

                     } else if (this.props.selectValue === "free") {
                       return(
                         <Free
                         slot={this.props.slot}
                         time={this.props.time}
                         day ={this.props.day}
                         month ={this.props.month}
                         year={this.props.year}
                         day_sl={this.props.day_sl}
                         color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                       );

                     }
                     else{
                       return(
                         <div className="inter-drop center">
                           Choose from above DropDown
                         </div>
                       );
                     }
       }
     }
