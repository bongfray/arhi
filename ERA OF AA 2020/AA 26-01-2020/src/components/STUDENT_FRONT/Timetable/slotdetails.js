import React, { Component} from 'react';
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import M from 'materialize-css';
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import AddProduct from '../RESUME_CONTENT/add_datas';
import ProductList from '../RESUME_CONTENT/show_info'


export default class BluePrint extends Component {
	constructor() {
    super();
    this.state = {
			loading: true,
			datas: [
				{
					day_order:'Day Order 1',dayor:'1',
					day:[
						{id:"1.1"},{id:"1.2"},{id:"1.3"},{id:"1.4"},{id:"1.5"},{id:"1.6"},{id:"1.7"},{id:"1.8"},{id:"1.9"},{id:"1.10"}
					]

				},
				{
					day_order:'Day Order 2',dayor:'2',
					day:[
						{id:"2.1"},{id:"2.2"},{id:"2.3"},{id:"2.4"},{id:"2.5"},{id:"2.6"},{id:"2.7"},{id:"2.8"},{id:"2.9"},{id:"2.10"}
					]
				},
				{
					day_order:'Day Order 3',dayor:'3',
					day:[
						{id:"3.1"},{id:"3.2"},{id:"3.3"},{id:"3.4"},{id:"3.5"},{id:"3.6"},{id:"3.7"},{id:"3.8"},{id:"3.9"},{id:"3.10"}
					]
				},
				{
					day_order:'Day Order 4',dayor:'4',
					day:[
						{id:"4.1"},{id:"4.2"},{id:"4.3"},{id:"4.4"},{id:"4.5"},{id:"4.6"},{id:"4.7"},{id:"4.8"},{id:"4.9"},{id:"4.10"}
					]
				},
				{
					day_order:'Day Order 5',dayor:'5',
					day:[
						{id:"5.1"},{id:"5.2"},{id:"5.3"},{id:"5.4"},{id:"5.5"},{id:"5.6"},{id:"5.7"},{id:"5.8"},{id:"5.9"},{id:"5.10"}
					]
				}
			],
			username:'',
			home:'/ework/faculty',
			logout:'/ework/user/logout',
			get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

    fetchlogin = () =>{
      axios.get('/ework/user/'
	)
      .then(response =>{
				this.setState({loading: false})
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
				else{
					this.setState({username:response.data.user.username})
				}
      })
    }

    componentDidMount() {
        M.AutoInit();
        this.fetchlogin();
    }


render() {
	const MyLoader = () => (
		<ContentLoader
		  height={160}
		  width={400}
		  speed={2}
		  primaryColor="#f3f3f3"
		  secondaryColor="#c0c0c0"
		>
		  <rect x="24" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="35" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="55" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="75" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="95" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="115" rx="0" ry="0" width="38" height="7" />

		</ContentLoader>
	  )
	if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
	return (
		<React.Fragment>
		<Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>


        <div className="row">
        <table className="center">
        <thead style={{backgroundColor:'white',color:'black'}}>
        <tr>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>Hour /</span><br />Day Order</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>1</span><br />08:00 - 08:50</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>2</span><br />08:50 - 09:40</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>3</span><br />09:45 - 10:35</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>4</span><br />10:40 - 11:30</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>5</span><br />11:35 - 12:25</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>6</span><br />12:30 - 01:20</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>7</span><br />01:25 - 02:15</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>8</span><br />02:20 - 03:10</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>9</span><br />03:15 - 04:05</th>
           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>10</span><br />04:05 - 04:55</th>
        </tr>
        </thead>

        <tbody>
				{this.state.datas.map((content,index)=>{
					return(
					<tr style={{backgroundColor: 'white'}} className="" key={index}>
					  <Stat content={content} dayor={content.dayor} username = {this.state.username}/>
					</tr>
				);

				})}
        </tbody>
      </table>


	</div>



</React.Fragment>
);
}
}
}
}


class Stat extends Component{
	constructor()
	{
		super()
		this.state={
			isAddProduct: false,
			response: {},
			action:'',
			product: {},
			isEditProduct: false,
			added:'',
			disabled:'block',
			editing:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}

	componentDidMount()
	{
	}

	    onCreate = (e,action) => {
	      this.setState({ isAddProduct: action ,product: {},disabled:'none'});
	    }
	    onFormSubmit =(data) => {
	      let apiUrl;
	      if(this.state.isEditProduct)
	      {
	        apiUrl = '/ework/user/edit_blue_print';
	      }
	       else
	       {
	        apiUrl = '/ework/user/send_blue_print';
	       }
	      axios.post(apiUrl,data)
	          .then(response => {
							if(response.data.exceed)
							{
								window.M.toast({html: response.data.exceed,outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
							}
							else if(response.data.succ)
							{
								window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded green lighten-1'});
							}
							else if(response.data.emsg)
							{
								window.M.toast({html: response.data.emsg,outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
							}
							if(this.state.isEditProduct)
							{
							 this.setState({added:data.serial})
							}
							 else
							 {
								this.setState({added:this.state.isAddProduct})
							 }

							this.setState({
								response: response.data,
								isEditProduct: false,
								isAddProduct: false,
								disabled:'block',
								editing:'',
							})
	          })
	    }

	    editProduct = (productId,index)=> {
				 this.setState({editing:productId})
	      axios.post('/ework/user/show_editable_blue_print',{
	        id: productId,
	      })
	          .then(response => {
	            this.setState({
	              product: response.data,
	              isEditProduct: index,
	              isAddProduct: index,
	            });
	          })

	   }
	render()
	{
		return(
			<React.Fragment>
			<td style={{borderRight: '1px solid'}} className="center"><b>{this.props.content.day_order}{this.state.week}</b></td>
			{this.props.content.day.map((content,ind)=>{
											return(
												<td style={{borderRight: '1px solid '}} key={content.id}>
															<div className="row">
															 <ShowDetails editing={this.state.editing} creating={this.state.isAddProduct} create={this.onCreate} added={this.state.added} data={content} username={this.state.username}  action={content.id} editProduct={this.editProduct}/>
															 {((this.state.isAddProduct === content.id) || (this.state.isEditProduct === content.id)) &&
															 <Add data={content}  action={content.id} username={this.state.username} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
															 }
															</div>
												</td>
											);

											})}
			</React.Fragment>
		);
	}
}



class FacultyList extends Component{
 constructor(props) {
    super(props);
    this.state = {
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      action:this.props.options,
      disabled:'',

    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount()
{
 this.setState({
   isAddProduct: false,
   isEditProduct: false,
 })
}

  onCreate = (e,index) => {
    this.setState({ isAddProduct: true ,product: {}});
  }
  onFormSubmit(data) {
    let apiUrl;
    if(this.state.isEditProduct){
      apiUrl = '/ework/user2/edit_faculty';
    } else {
      apiUrl = '/ework/user2/add_faculty';
    }

    if(!(data.faculty_id) || !(data.subject_taking)){
      window.M.toast({html: 'Enter all the details !!', classes:'rounded #f44336 red'});
    }
    else{
    axios.post(apiUrl, {data})
        .then(response => {
          if(response.data === 'have'){
              window.M.toast({html: 'Datas are already saved !!', classes:'rounded #f44336 red'});
          }
          else if(response.data === 'done'){
              window.M.toast({html: 'Saved !!', classes:'rounded green darken-2'});
          }
          this.setState({
            response: response.data,
            isAddProduct: false,
            isEditProduct: false
          })

        })


      }

  }

  editProduct = (productId,index)=> {
    axios.post('/ework/user2/fetcheditdata_facultylist',{
      id: productId,
      username: this.props.username,
    })
        .then(response => {
          this.setState({
            product: response.data,
            isEditProduct: true,
            isAddProduct: true,
          });
        })

 }

 updateState = () =>{
   this.setState({
     isAddProduct:false,
     isEditProduct:false,
   })
 }
  render() {
    let productForm,title,description;
    var data;
            title ='CURRENT SEMESTER FACULTY LIST';
            data = {
              Action:'FACULTY LIST',
              button_grid:'col l1 m1 s1 center',
              fielddata: [
                {
                  header: "Faculty ID",
                  name: "faculty_id",
                  placeholder: "Enter Faculty ID",
                  type: "text",
                  grid:'col l5 xl5 m5 s5 center',
                },
                {
                  header: "Subject Taking",
                  name: "subject_taking",
                  placeholder: "Enter the subject,he takes",
                  type: "text",
                  grid:'col l5 m5 s5 xl5 center',
                },
              ],
            };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct description={description} cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
 <div>
   {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
   {!this.state.isAddProduct &&
    <React.Fragment>
    <div className="row">
    <div className="col l6" />
    <div className="col l6">
      <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
     </div>
   </div>
   </React.Fragment>
   }
   { productForm }
   <br/>
 </div>

);
}
}
