import React, { Component } from 'react'
import axios from 'axios'
import { Select } from 'react-materialize';

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       saved_skill:[],

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.fetchSavedSkills()
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

fetchSavedSkills =()=>{
  axios.post('/ework/user2/fetch_in_admin',{action:'Skill-For-Student'})
  .then(res => {
      if(res.data)
      {
        this.setState({saved_skill:res.data})
      }
  });
}


   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user2/editt';
     } else {
       apiUrl = '/ework/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }


   updateState = () =>{
     this.setState({
       isAddProduct:false,
       isEditProduct:false,
     })
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm,title,description;
     var data;
             title ='Skills';
             data = {
               Action:'Skills',
               button_grid:'col l2 xl2 s2 m2 center',
               fielddata: [
                 {
                   header: "Your Skills",
                   name: "skill_name",
                   placeholder: "Enter Your Skill",
                   type: "text",
                   grid:'col l3 xl3 s1 m1 center'
                 },
                 {
                   header: "Level of Your Skill",
                   name: "skill_level",
                   placeholder: "Enter Your Lavel in this Skill",
                   type: "text",
                   grid: 'col l3 xl3 s1 m1 center',
                   exception:true,
                 },
                 {
                   header: "Verification",
                   name: "skill_verification",
                   placeholder: "",
                   type: "text",
                   grid:'col l3 xl3 s1 m1 center',
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct cancel={this.updateState} saved_skill={this.state.saved_skill} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div className="card hoverable">
  <div>
    <div>
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}
    <br />
    {!this.state.isAddProduct &&
      <React.Fragment>
      <div className="row">
      <div className="right">
        <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
      </div>
      </div>
     </React.Fragment>
   }
     { productForm }
     <br/>
   </div>
   </div>
 </div>
   </div>
);
}
}


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      Action:'',
      name: '',
      emailid: '',
      mobileno:'',
      address: '',
      gender:'',

      skill_name:'',
      skill_level:'',
      skill_verification:'',
      skill_rating:'',


    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
    skill_rating:'0*',
  })
  if(this.props.data.Action === 'Skills')
  {
    this.setState({
      skill_verification:false,
    })
  }
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  stateSet=(object)=>{
    this.setState(object);
  }


  render() {
    // console.log(this.initialState.username)
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS TO YOUR PERSONAL DATA</h5>
}

    return(
      <div>
      <div className="center">{pageTitle}</div>
          {this.props.data.fielddata.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1"/>
          <div className="col l2 m2 s2">
           {content.placeholder === '' ?
             <div></div>
             :
             <div>
             {content.header}
             </div>
            }

          </div>
            <div className="col l8 s8 m8 input-field" key={index}>
            <React.Fragment>
               {content.exception ? <Select name="skill_level" value={this.state.skill_level} onChange={this.handleInput}>
                     <option value="" disabled selected>Level</option>
                     <option value="Begineer">Begineer</option>
                     <option value="Intermediate">Intermediate</option>
                     <option value="Advanced">Advanced</option>
                 </Select> :
                 <React.Fragment>
                   {content.name === "skill_name" ?
                   <SearchEngine stateSet={this.stateSet} field={this.props.saved_skill} />
                   :
                   <div>
                    {content.placeholder === '' ?
                    <div>

                    </div>
                       :
                       <div>
                           <input
                             className=""
                             type={content.type}
                             placeholder=" "
                             min="10"
                             max="60"
                             name={content.name}
                             value={this.state[content.name]}
                             onChange={e => this.handleD(e, index)}
                           />
                           <label>{content.placeholder}</label>
                       </div> }

                     </div>
                   }
                 </React.Fragment>
               }
            </React.Fragment>

            </div>

            <div className="col l1" />
          </div>
          ))}

          <div className="row">
            <div className="col l9 xl9 hide-on-mid-and-down" />
            <div className="col l3 xl3 s12">
              <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
              <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
            </div>
          </div>

      </div>
    )
  }
}


class SearchEngine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchString: "",
      users: []
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.setState({
      users: this.props.field
    });
    this.refs.search.focus();
  }

  handleChange() {
    this.setState({
      searchString: this.refs.search.value
    });
  }

  handleLevel = (e) =>{
    this.setState({searchString:e})
    this.props.stateSet({
      skill_name:e,
    })
  }

  render() {
    let _users = this.state.users;
    let search = this.state.searchString.trim().toLowerCase();

    if (search.length > 0) {
      _users = _users.filter(function(user) {
        return user.skill_name.toLowerCase().match(search);
      });
    }

    return (
      <div>
        <div>
          <input
            type="text"
            value={this.state.searchString}
            ref="search"
            onChange={this.handleChange}
            placeholder="Search Skill Here"
          />
          {this.state.searchString &&
            <div>
            {_users.map((l,index) => {
              return (
                <div key={index} style={{padding:'8px'}} className="col l3 xl3 m4 s6 center" onClick={()=>{this.handleLevel(l.skill_name)}}>{l.skill_name}</div>
              );
            })}
            </div>
          }
        </div>
      </div>
    );
  }
}

class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user2/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        if(this.props.oneEntry)
        {
          this.props.oneEntry({
            disabled:'',
          })
        }
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/ework/user2/fetchall',{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    if(response.data.length>0)
    {
      if(this.props.oneEntry)
      {
        this.props.oneEntry({
          disabled:'disabled',
        })
      }
    }
    this.setState({
     products: response.data,
   })
  })
}



  render() {
    const {  products} = this.state;
      return(
        <React.Fragment>
          <h5 className="center">{this.props.title}</h5><br />
          <div style={{marginLeft:'20px'}}>
          <div className="row">
            <div className="col s1 m1 xl1 l1 center"><b>Serial No</b></div>
            {this.props.data.fielddata.map((content,index)=>(
              <div className={content.grid} key={index}><b>{content.header}</b></div>
            ))}
            <div className={this.props.data.button_grid}><b>Action</b></div>
          </div>
              {products.map((product,index) => (
                <div className="row" key={product.serial}>
                  <div className="col s1 l1 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <React.Fragment key={index}>
                    {content.placeholder === '' ?
                          <div className={content.grid}>
                             {product[content.name] === false ?
                                <span className="red-text center">Verification Pending</span>
                                :
                                <span className="green-text center">Verified</span>
                             }
                          </div>
                       :
                      <div className={'center '+content.grid}>{product[content.name]}</div>
                    }
                    </React.Fragment>
                  ))}
                    <div className={this.props.data.button_grid}>
                    <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                    &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                </div>
              ))}
            </div>
        </React.Fragment>
      )

  }
}
