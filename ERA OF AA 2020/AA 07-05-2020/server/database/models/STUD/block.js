const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const facultylist = new Schema({
  username: { type: String, unique: false, required: false },
  name:{ type: String, unique: false, required: false },
  faculty_adviser_id:{ type: String, unique: false, required: false },
  count:{ type: Number, unique: false, required: false },
  status:{ type: Boolean, unique: false, required: false },
  received:{type: Date, default: Date.now},
  unblocked_by:{type: Array}
})



facultylist.plugin(autoIncrement.plugin, { model: 'Block', field: 'serial', startAt: 1,incrementBy: 1 });

const FacultyList = mongoose.model('Block', facultylist)
module.exports = FacultyList
