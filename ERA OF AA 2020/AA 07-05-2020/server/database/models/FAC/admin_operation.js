const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const different_res = new Schema({
  action: {type: String , unique: false, required: false},
  usertype: {type: String , unique: false, required: false},
  username: {type: String , unique: false, required: false},
  section_data_name:{ type: String, unique: false, required: false },
  section_props:{ type: String, unique: false, required: false },
  role:{ type: String, unique: false, required: false },
  description:{ type: String, unique: false, required: false },
  date_of_starting:{ type: String, unique: false, required: false },
  date_of_complete:{ type: String, unique: false, required: false },
  achivements:{ type: String, unique: false, required: false },
  designation_name:{type: String, unique: false, required: false},
  designation_order_no:{type: Number, unique: false, required: false},
  department_name:{type: String, unique: false, required: false},
  responsibilty_root: {type: String, unique: false, required: false},
  responsibilty_user: {type: String, unique: false, required: false},
  responsibilty_root_percentage: {type: Number, unique: false, required: false},
  responsibilty_title: {type: String, unique: false, required: false},
  responsibilty_percentage: {type: Number, unique: false, required: false},
  skill_name:{type: String, unique: false, required: false},
  domain_name:{type: String, unique: false, required: false},
  inserted_by: {type: String, unique: false, required: false},
  suggestion_for_learn_a_skill: {type: String, unique: false, required: false},
  ref_link_for_skill: {type: String, unique: false, required: false},
  course_to_learn: {type: String, unique: false, required: false},
  description_of_the_task:{type: String, unique: false, required: false},
  ref_for_task:{type: String, unique: false, required: false},
  for:{type: String, unique: false, required: false},
  question: {type: String, unique: false, required: false},
  option1: {type: String, unique: false, required: false},
  option2:{type: String, unique: false, required: false},
  option3:{type: String, unique: false, required: false},
  option4:{type: String, unique: false, required: false},
  answer:{type: String, unique: false, required: false},
})



different_res.plugin(autoIncrement.plugin, { model: 'AdminOperations', field: 'serial', startAt: 1,incrementBy: 1 });

const Different_res = mongoose.model('AdminOperations', different_res)
module.exports = Different_res
