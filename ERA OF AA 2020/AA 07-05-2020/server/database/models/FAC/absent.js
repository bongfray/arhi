const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

const absent = new Schema({

  official_id: { type: String, unique: false, required: false },
  reason: { type: String, unique: false, required: false },
  week: { type: String, unique: false, required: false },
  date: { type: String, unique: false, required: false },
  month: { type: String, unique: false, required: false },
  year: { type: String, unique: false, required: false },
})

var Absent = mongoose.model('Absent', absent);

module.exports = Absent
