const passport = require('passport')
const LocalStrategy = require('./localStrategy')
const User = require('../database/models/FAC/user')

passport.serializeUser((user, done) => {
	done(null, { _id: user._id,count: user.count })
})


passport.deserializeUser((id, done) => {
	User.findOne(
		{ _id: id },
		['username','count','desgn','h_order','campus','dept','suprimity','faculty_adviser',
		'faculty_adviser_year','count','name','nightmode'],
		(err, user) => {
			if(err)
			{
				done(err,false)
			}
			else{
			done(null, user)
			}
		}
	)
})

passport.use(LocalStrategy)

module.exports = passport
