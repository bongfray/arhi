import React, { } from 'react';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import Slide from '@material-ui/core/Slide';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import {DialogTitle,DialogContentText} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ListItem from '@material-ui/core/ListItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';
import {Snackbar} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

let count=0;
let visibilityChange = null;
if (typeof document.hidden !== 'undefined') {
  visibilityChange = 'visibilitychange';
} else if (typeof document.msHidden !== 'undefined') {
  visibilityChange = 'msvisibilitychange';
} else if (typeof document.webkitHidden !== 'undefined') {
  visibilityChange = 'webkitvisibilitychange';
}


export default class Instructions extends React.Component {
  constructor()
  {
    super()
    this.state={
        start_modal:true,
        test_window:false,
        snack_open:false,
        snack_msg:'',
        alert_type:'',
        valid:false,
      }

  }

  openTest=()=>{
    this.setState({valid:true})
    axios.post('/ework/user2/know_block',{skill:this.props.state.skill_name})
    .then( res => {
      console.log(res.data);
        if(res.data === 'ok')
        {
          this.setState({
            valid:false,
            snack_open:true,
            snack_msg:'Ooops !! You are not allowed to add this skill !!',
            alert_type:'error',
          })
        }
        else
        {
          this.setState({valid:false,test_window:true})
        }
    })
    .catch( err => {
      this.setState({
        valid:false,
        snack_open:true,
        snack_msg:'Something went wrong !!',
        alert_type:'error',
      })
    });

  }

  closeInstruction=()=>{
    this.props.stateSet({
      question_window:false
    })
    this.setState({test_window:false})
  }
  setStateOFThis=(object)=>{
    this.setState(object);
  }

  render() {
    if(this.state.valid)
    {
      return(
        <div className="load-me" style={{textAlign:'center'}}>
          <div class="spinner-box">
                <div class="blue-orbit leo">
                </div>

                <div class="green-orbit leo">
                </div>

                <div class="red-orbit leo">
                </div>
                <div class="white-orbit w1 leo">
                </div><div class="white-orbit w2 leo">
                </div><div class="white-orbit w3 leo">
                </div>
          </div>
          <Typography style={{color:'white'}} variant="h5" align="center">Setting Up Test Window....</Typography>
        </div>
      )
    }
    else
    {
        return (
          <React.Fragment>
          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={4000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          {this.props.state.question_window &&
            <Dialog
              open={this.props.state.question_window}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title" style={{textAlign:'center'}}>Instructions</DialogTitle>
              <DialogContent>
                1.  In order to add a skill, first we will verify you.<br/>
                2.  After clicking the agree button below we will reidirect you to a page.<br/>
                3.  Please remember if you exit from that window, the particular skill that you are going to add,
                   will be blocked forever for you .<br/>
                4.  Pressing any other buttons and switching to another tab will also be treated as a violation to
                  our policy and that skill again will be blocked for you.<br/>
                5.  We are requesting you not to leave the test window. If you click or move the cursor outside of the window that
                    will again be treated as a violation to our policy.<br />
                6.  You have to answer 5 correct question out of 10.<br />
                7.  If you give 6 wrong answer, we would not add your skill to you resume.<br />
                8.  But don't worry. You have multiple chance to take the screening test.<br />
                9.  As usual, you have to answer within some particular time, that will be initiated
                   after the test start.<br />
                10.  Remember one more thing, questions will be displayed one by one.You can
                  move to other question by answering the questions in sequencial manner.<br />
                11.  Once you answer a question, you can't go back to that question.

              </DialogContent>
              <DialogActions>
              <Button onClick={this.closeInstruction} color="primary" autoFocus>
                CLOSE
              </Button>
                <Button onClick={this.openTest} color="primary" autoFocus>
                  Agree
                </Button>
              </DialogActions>
            </Dialog>
          }

          {this.state.test_window &&
            <Questions state_data={this.state} props_data={this.props.state}
              closeInstruction={this.closeInstruction}
             stateSet={this.props.stateSet} formSubmit={this.props.formSubmit} cancel={this.props.cancel}
             />
          }
          </React.Fragment>
        )
       }
  }
}

   var alertTimerId,timer=0,spam=0;
 class Questions extends React.Component {

  constructor()
  {
    super()
    this.state={
        actions:false,
        start_modal:true,
        fetching:true,
        answer:'',
        expansion_id:0,
        isExpanded:true,
        correct:0,
        total:10,
        show_result:false,
        warn_window:false,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount() {
        document.addEventListener(visibilityChange, this.handleVisibilityChange, false);
        this.fetchQuestions();
  }

  componentDidUpdate=(prevProps)=>
  {
    if(prevProps !== this.props)
    {
      this.fetchQuestions()
    }
  }

  fetchQuestions=()=>{
    this.setState({fetching:true})
    axios.post('/ework/user2/fetch_in_admin_for_path',{
      action: "add-questions",
      usertype: this.props.props_data.skill_level,
      for:'QS '+this.props.props_data.skill_name,
    })
    .then( res => {
        if(res.data)
        {
          if(res.data.length<10)
          {
            this.props.stateSet({
              snack_open:true,
              snack_msg:'Sorry, questions not available !!',
              alert_type:'info',
            })
            this.props.closeInstruction();
          }
          else
          {
            let n =10;
            var result = new Array(n),
                len = res.data.length,
                taken = new Array(len);
              while (n--)
              {
                  var x = Math.floor(Math.random() * len);
                  result[n] = res.data[x in taken ? taken[x] : x];
                  taken[x] = --len in taken ? taken[len] : len;
              }
              if(this.props.props_data.skill_level=== 'Begineer')
              {
                timer =4*60000;
              }
              else if (this.props.props_data.skill_level ==='Intermediate') {
                timer =6*60000;
              }
              else {
                timer =9*60000;
              }
              alertTimerId = setTimeout(this.submitResult,timer);
              this.setState({questions:result,fetching:false})
          }
          this.setState({fetching:false})
        }
    });
  }

  handleVisibilityChange = () => {
    if(spam === 2){
      this.blockSkill();
    }
    else{
      spam++;
      this.setState({warn_window:true})
    }
  }

  escFunction=(event)=>{
    if(spam === 2){
      this.blockSkill();
    }
    else{
      spam++;
      this.setState({warn_window:true})
    }
  }

  blockSkill=()=>{
    axios.post('/ework/user2/block_skill',{skill:this.props.props_data.skill_name})
    .then( res => {

    });
    this.props.stateSet({
      question_window:false,
    });
    spam=0;
  }

  componentWillUnmount()    {
    document.removeEventListener(visibilityChange, this.handleVisibilityChange);
  }

  setAnswer=(answer,index)=>{
    this.verifyCorrectness(answer,index)
  }

  verifyCorrectness=(answer,index)=>{
    if(this.state.answer === answer)
    {
      count++;
    }
    if(index === 9)
    {
      this.setState({show_result:true})
    }
    else
    {
        this.setState({expansion_id:index+1,correct:count,answer:''})
    }

  }

  submitResult=()=>{
    if(count>=5)
    {
      clearTimeout(alertTimerId );
      this.props.formSubmit(this.props.props_data);
      this.props.stateSet({
        question_window:false,
      });
    }
    else
    {
      clearTimeout(alertTimerId);
      this.props.stateSet({
        question_window:false,
      });
      this.props.cancel();
    }
    count = 0;
  }



  render() {
    if(this.state.fetching)
    {
      return(
        <div style={{float:'center'}}>
        <CircularProgress />
        </div>
      )

    }
    else
    {
    return (
      <React.Fragment>
        <Dialog
        fullScreen
         open={true}
         onKeyDown={this.escFunction}
         onMouseLeave={this.escFunction}
         TransitionComponent={Transition}>
          {!(this.state.show_result) ?
            <List>
              <div style={{marginTop:'40px'}}>
                <div style={{textAlign:'center'}}>
                    <Typography variant="h4">SET OF QUESTIONS</Typography>
                </div>
              <br />
                <Clock seconds={timer} /><br/>
                {this.state.questions.map((content,index)=>{
                  return(
                    <React.Fragment>
                    {this.state.warn_window &&
                      <Dialog
                        open={true}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                      >
                        <DialogTitle id="alert-dialog-title">Warning !!</DialogTitle>
                        <DialogContent>
                          <DialogContentText id="alert-dialog-description">
                             Remember all the rules whatever we have shown to you previously.If you again violate the
                             rule this skill will be blocked for you.
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={()=>this.setState({warn_window:false})} color="primary" autoFocus>
                            Close
                          </Button>
                        </DialogActions>
                      </Dialog>
                    }
                    <Grid container spacing={1} key={index}>
                      <Grid item xs={1} sm={1}/>
                       <Grid item xs={10} sm={10}>
                          <ExpansionPanel
                          expanded={this.state.isExpanded  && this.state.expansion_id === index}
                          >
                            <ExpansionPanelSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1a-content"
                              id="panel1a-header"
                              style={{backgroundColor:'#b9f6ca'}}
                            >
                              <Typography>QUESTION {index+1}</Typography>
                            </ExpansionPanelSummary>
                                <List>
                                    <ListItem button>
                                     <b>Q.{index+1} - {content.question}</b>
                                    </ListItem>
                                    <br />
                                    <ListItem>
                                    <FormControl component="fieldset">
                                      <RadioGroup aria-label="gender" name="gender1" value={this.state.answer}
                                      onChange={(e)=>this.setState({answer:e.target.value})}>
                                        <FormControlLabel value={content.option1}
                                        control={<Radio />} style={{color:'black'}} label={content.option1} />
                                        <FormControlLabel value={content.option2}
                                        control={<Radio />}  style={{color:'black'}} label={content.option2} />
                                        <FormControlLabel value={content.option3}
                                        control={<Radio />} style={{color:'black'}} label={content.option3} />
                                        <FormControlLabel value={content.option4}
                                        control={<Radio />} style={{color:'black'}} label={content.option4} />
                                      </RadioGroup>
                                      <br />
                                      <Button variant="contained" style={{float:'right'}}
                                      color="primary" onClick={()=>this.setAnswer(content.answer,index)}>Save</Button>
                                    </FormControl>
                                  </ListItem>
                                </List>
                          </ExpansionPanel>
                          <br />
                        </Grid>
                        <Grid item xs={1} sm={1}/>
                    </Grid>
                  </React.Fragment>
                  )
                })}
              </div>
            </List>
            :
            <List style={{marginTop:'30px'}}>
              <Paper variant="outlined" square style={{padding:'50px'}}>
               <div style={{textAlign:'center'}}>You result - {count} / 10<br />
               {count>=5 ?
                 <React.Fragment>
                     <Typography style={{color:'green'}}>Congrats !! You have cleared screening test !!</Typography>
                     <br />
                     <Button variant="contained" onClick={this.submitResult}>Click here to add this course</Button>
                 </React.Fragment>
                 :
                 <React.Fragment>
                   <Typography color="secondary">Ooops !! You can't able to clear the test !! Better luck for your next screening test !!</Typography><br />
                   <Button variant="contained" onClick={this.submitResult}>Click here to exit the test window</Button>
                 </React.Fragment>
               }
               </div>
               </Paper>
            </List>
          }
          </Dialog>
      </React.Fragment>
    )
 }
}
}


class Clock extends React.Component {
  constructor(props){
    super(props)
    this.tick = this.tick.bind(this)
    this.state = {seconds: props.seconds}
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
  }

  tick(){
    if (this.state.seconds > 0) {
      this.setState({seconds: this.state.seconds - 1})
    } else {
      clearInterval(this.timer);
    }
  }
  render(){
    return(
      <Paper className="float-right" style={{color:'red'}} elevation={3}>
        {this.state.seconds} seconds left
      </Paper>
    )
  }
}
