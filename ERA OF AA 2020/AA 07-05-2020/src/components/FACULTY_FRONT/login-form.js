import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from '../forget'
import axios from 'axios'
import Nav from '../dynnav'
import InfoIcon from '@material-ui/icons/Info';
import
{Dialog,Button,Grid,Paper,DialogActions,
  Divider,DialogContent,DialogTitle,
  Snackbar,Typography,Zoom,TextField
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            all:false,
            username: '',
            password: '',
            redirectTo: null,
            forgotroute:'/ework/user/forgo',
            home:'/ework/faculty',
            logout:'/ework/user/logout',
            get:'/ework/user/',
            nav_route: '/ework/user/fetchnav',
            display_welcomenote:false,
            display_nav:true,
            block_click:false,
            snack_open:false,
            alert_type:'',
            snack_msg:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)
    }

    handleUser(e) {
        this.setState({
            username: e.target.value,
            all:false
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value,
            all:false,
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        if(!(this.state.username)||!(this.state.password)){
          this.setState({
            snack_open:true,
            snack_msg:'Enter the Details !!',
            alert_type:'warning',
            all:true,
          })
          return false;
        }
        else{
          this.setState({
            snack_open:true,
            snack_msg:'Logging In..',
            alert_type:'info',
          })

          this.setState({block_click:true})
        axios.post('/ework/user/faclogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                // console.log(response)
                if (response.status === 200) {
                if(response.data.count===0)
                {
                  this.setState({
                      redirectTo: '/ework/fprofdata'
                  })
                }
                else if((response.data.count === 2)||(response.data.count ===1))
                {
                  this.setState({
                      redirectTo: '/ework/faculty'
                  })
                }
                else if(response.data.count === 3)
                {
                  this.setState({
                    redirectTo:'/ework/dadmin_panel'
                  })
                }
                else if(response.data.count === 4)
                {
                  this.setState({
                    redirectTo:'/ework/admin_panel'
                  })
                }
                this.setState({block_click:false})
                }
            }).catch(error => {
              this.setState({block_click:false})
              if(error.response.status === 401)
              {
                this.setState({
                  snack_open:true,
                  snack_msg:error.response.data,
                  alert_type:'error',
                });
              }
              else
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Internal Error !!',
                  alert_type:'error',
                });
              }
            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }



    getUser = () => {
      axios.get('/ework/user/').then(response => {
        if (response.data.user) {
              if((response.data.user.count === 1) || (response.data.user.count === 0) || (response.data.user.count === 2))
              {
                this.setState({redirectTo:'/ework/faculty'})
              }
              else if((response.data.user.count === 3))
              {
                this.setState({redirectTo:'/ework/dadmin_panel'})
              }
              else if((response.data.user.count === 4))
              {
                this.setState({redirectTo:'/ework/admin_panel'})
              }
        }
          })
        }



        componentDidMount() {
            this.getUser();
        }

        Welcome =() =>{
          this.setState({
            display_nav:!this.state.display_nav,
            display_welcomenote:!this.state.display_welcomenote,
          })
        }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

            return (
              <React.Fragment>


              <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

              <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
              open={this.state.snack_open} autoHideDuration={2000}
              onClose={()=>this.setState({snack_open:false})}>
                <Alert onClose={()=>this.setState({snack_open:false})}
                severity={this.state.alert_type}>
                  {this.state.snack_msg}
                </Alert>
              </Snackbar>

                <Grid container>
                 <Grid item xs={1} sm={4} xl={4}>
                 {this.state.display_nav &&
                  <div onClick={this.Welcome}
                   className="animat bounce" style={{marginTop:'20px',cursor: 'pointer'}}>
                   <InfoIcon />
                  </div>
                 }
                 <Zoom in={this.state.display_welcomenote}>
                   <Dialog
                     open={this.state.display_welcomenote}
                     keepMounted
                     onClose={this.Welcome}
                     aria-labelledby="alert-dialog-slide-title"
                     aria-describedby="alert-dialog-slide-description"
                   >
                     <DialogTitle id="alert-dialog-slide-title" align="center">Remember !!</DialogTitle>
                     <DialogContent>
                       If you have signed up recently, in that case, your account is in verification stage now.
                       Kindly wait for the approve mail before proceeding for login. It may take time.
                        If you have not received any mail for a long time kindly contact SRM CARE.
                     </DialogContent>
                     <DialogActions>
                       <Button onClick={this.Welcome} color="primary">
                         Close
                       </Button>
                     </DialogActions>
                   </Dialog>
                  </Zoom>
                 </Grid>

                 <Grid item xs={10} sm={4} xl={4}>
                 <Paper elevation={3} style={{marginTop:'20px',borderRadius:'8px'}}>
                     <div className="center">
                         <Link style={{textDecoration:'none'}} to ="/ework/faculty">
                           <Typography variant="h5" color="secondary" align="center"
                           style={{padding:'8px',color: 'white',borderRadius:'10px 10px 0px 0px',
                             display: 'block',backgroundColor:'#455a64',fontFamily:'Orbitron'}}>eWork</Typography>

                         </Link>
                         <Typography variant="h6" color="secondary" align="center"
                           style={{padding:'8px',color: 'black',borderRadius:'10px 10px 0px 0px',
                             display: 'block',fontFamily:'Orbitron'}}>
                             LOG IN
                          </Typography>
                     </div>
                     <form className="form-con" onSubmit={this.handleSubmit}>
                           <Grid container spacing={1}>
                             <Grid item xs={12} sm={12}>
                                <TextField id="outlined-basic1" label="Username"
                                type="text" value={this.state.username} onChange={this.handleUser} fullWidth
                                error={this.state.all}
                                 variant="outlined" />
                                 <br /><br />
                             </Grid>
                             <Grid item xs={12} sm={12}>
                               <TextField id="outlined-basic2" label="Password"
                                type="password" className="validate" value={this.state.password} onChange={this.handlePass} fullWidth
                                error={this.state.all}
                                variant="outlined" />
                                <br /><br />
                             </Grid>
                           </Grid>
                           <Grid container spacing={3}>
                               <Grid item xs={6} sm={6}>
                                 <Link style={{textDecoration:'none'}} to="/ework/fsignup">
                                   <Button fullWidth style={{backgroundColor:'#455a64',color:'white'}}>
                                     Register
                                    </Button>
                                  </Link>
                               </Grid>
                              <Grid item xs={6} sm={6}>
                                 <Button disabled={this.state.block_click} fullWidth
                                  style={{backgroundColor:'#455a64',color:'white'}}
                                  onClick={this.handleSubmit}>Login</Button>
                              </Grid>
                           </Grid>
                           <br />

                         <Divider /><br />

                            <Modal forgot={this.state.forgotroute}/>
                     </form>
                    </Paper>
                 </Grid>
                <Grid item xs={1} sm={4} xl={4}/>
             </Grid>
             </React.Fragment>
           );
         }

    }
}

export default LoginForm
