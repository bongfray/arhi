import React, { Component } from 'react';
import axios from 'axios'
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import {Paper} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import MuiAlert from '@material-ui/lab/Alert';
 import {Snackbar,Typography,Grid,TextField,Button} from '@material-ui/core';
 import VisibilityIcon from '@material-ui/icons/Visibility';
 import SendIcon from '@material-ui/icons/Send';
 import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';


 import Dialog from '@material-ui/core/Dialog';
 import DialogActions from '@material-ui/core/DialogActions';
 import DialogContent from '@material-ui/core/DialogContent';
 import DialogContentText from '@material-ui/core/DialogContentText';
 import DialogTitle from '@material-ui/core/DialogTitle';

import ViewStudentData from './view_single_student';

 function Alert(props) {
   return <MuiAlert elevation={6} variant="filled" {...props} />;
 }



export default class StundetList extends Component{
  constructor()
  {
    super()
    this.state={
      students:'',
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStudentList();
  }

  fetchStudentList=()=>{
    axios.post('/ework/user2/fetch_student_list_under_faculty_adviser',{user:this.props.user})
  .then( res => {
      if(res.data)
      {
        this.setState({students:res.data,loading:false})
      }
  });
  }

  render()
  {
    if(this.state.loading)
    {
      return(
        <Backdrop open={true} style={{zIndex:'2024'}}>
          <CircularProgress style={{color:'yellow'}} />&emsp;
          <div style={{color:'yellow',textAlign:'center'}}>Loading....</div>
        </Backdrop>
      )
    }
    else {
      if(this.state.students.length === 0)
      {
        return(
          <Grid container>
            <Grid item xs={12} sm={12}>
                <Typography align="center" variant="h6">No Request Found !!</Typography>
            </Grid>
          </Grid>
        )
      }
      else {

      }
        return(
          <React.Fragment>
            <EnhancedTable request={this.state.students} user_session={this.props.user} />
          </React.Fragment>
        )
    }
  }
}



function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}


const headCells = [
  { id: 'Reg ID', numeric: false, disablePadding: false, label: 'Reg ID' },
  { id: 'Mail Id', numeric: true, disablePadding: false, label: 'Mail Id' },
  { id: 'Year - Sem - Batch', numeric: true, disablePadding: false, label: 'Year - Sem - Batch' },
  { id: 'Campus - Department', numeric: true, disablePadding: false, label: 'Campus - Department' },
  { id: 'Action', numeric: true, disablePadding: true, label: 'Action' },
  { id: 'Send Message', numeric: true, disablePadding: true, label: 'Send Message' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>
               {(((headCell.label === 'Information') || (headCell.label === 'Action')) && (props.incoming_action===false)) ?
                 <React.Fragment></React.Fragment>
                   :
                 <React.Fragment>{headCell.label}</React.Fragment>
               }
              </b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [submit,setSubmit] = React.useState(false)
  const [data,setData] = React.useState({
    details_view:false,
    data:'',
    snack_open:false,
    snack_msg:'',
    alert_type:'',
  });
  const [mwindow,setMwindow] = React.useState({
      open_popup: false,
      userDetails: '',
    });
    const [message, setMessage] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };



  const closeView=()=>{
    setData({details_view:false})
  }

  const showDetails=(content)=>{
    setData({data:content,details_view:true})
  }

  const sendMessage=()=>{
    var data;
    if(!message)
    {
      setData({
        snack_open:true,
        snack_msg:'Enter all the Details !!',
        alert_type:'warning',
      })
    }
    else{
        data = {
          mail_send:true,
          user:[{mailid:mwindow.userDetails.mailid}],
          message:message,
          user_session:props.user_session,
        }
        setData({
          snack_open:true,
          snack_msg:'Sending Mail....',
          alert_type:'info',
        })
        setSubmit(true)
      axios.post('/ework/user2/sendMessage_from_UserSide',data)
      .then( res => {
        setSubmit(false)
          if(res.data === 'ok')
          {
            setData({
              snack_open:true,
              snack_msg:'Done !!',
              alert_type:'success',
            })

            message('')
            setMwindow({
              open_popup:false,
            })
          }
          else if(res.data === 'no') {
            setData({
              snack_open:true,
              snack_msg:'Failed To Send !!',
              alert_type:'error',
            })
          }
      })
      .catch( err => {
        setSubmit(false)
      });
   }
  }

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.request.length - page * rowsPerPage);

if(submit)
{
  return(
    <Backdrop open={true} style={{zIndex:'2024'}}>
      <CircularProgress style={{color:'yellow'}} />&emsp;
      <div style={{color:'yellow',textAlign:'center'}}>Sending Your Data....</div>
    </Backdrop>
  )
}
else{
  return (
    <div className={classes.root}>
    {data.details_view  &&
        <ViewStudentData details_view={data.details_view}  closeView={closeView} data={data} />
    }

    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    open={data.snack_open} autoHideDuration={2000}
    onClose={()=>setData({snack_open:false})}>
      <Alert onClose={()=>setData({snack_open:false})}
      severity={data.alert_type}>
        {data.snack_msg}
      </Alert>
    </Snackbar>

    {mwindow.open_popup &&
        <Dialog open={mwindow.open_popup} onClose={()=>setMwindow({open_popup:false,userDetails:''})} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Send Message</DialogTitle>
          <DialogContent>
            <DialogContentText>
             This message will be forwarded to the particular student.
            </DialogContentText>
          <TextField
              required
              id="filled-required"
              label="Enter the message"
              value={message}
              onChange={(e)=>setMessage(e.target.value)}
              variant="filled"
              fullWidth
              multiline
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setMwindow({open_popup:false,userDetails:''})} color="primary">
              Cancel
            </Button>
            <Button disabled={message ? false: true} color="primary" onClick={sendMessage}>
              Send Message
            </Button>
          </DialogActions>
         </Dialog>
    }


      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              incoming_action={props.admin_action}
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.request.length}
            />
            <TableBody>
              {stableSort(props.request, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row._id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                    >
                     <TableCell>
                     {row.username}
                     </TableCell>
                     <TableCell align="center">
                       {row.mailid}
                      </TableCell>

                      <TableCell align="center">
                        {row.year} - {row.sem} - {row.batch}
                      </TableCell>
                      <TableCell align="center">
                        {row.campus} - {row.dept}
                      </TableCell>
                      <TableCell align="center">
                        <Tooltip title="View Details">
                         <VisibilityIcon onClick={()=>showDetails(row)} />
                        </Tooltip>
                       </TableCell>
                       <TableCell align="center">
                         <Tooltip title="Send Message To This Student">
                          <SendIcon onClick={()=>setMwindow({
                            open_popup:true,
                            userDetails:row,
                          })} />
                         </Tooltip>
                       </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.request.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}


}
