import React, { Component } from 'react';
import axios from 'axios';
import {Paper,Grid,Typography,Switch} from '@material-ui/core';

export default class Activation extends Component {
  constructor(){
      super();
      this.state={
        isChecked:false,
        isCheckedS:false,
        faculty_blueprint:false,
        student_blueprint:false,
        history:'',
      }
      this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.fetchFaculty_Registration_Status();
    this.fetchStudent_Registration_Status();
  }

  fetchFaculty_Registration_Status()
  {
    axios.post('/ework/user/fac_status')
        .then(response => {
          if(response.data)
          {
            var fac_reg_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="SIGNUP STATUS")));
            var fac_blueprint_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="BLUE PRINT STATUS")));

            if((fac_reg_status.length)>0)
            {
              this.setState({isChecked: fac_reg_status[0].status})
            }
            if((fac_blueprint_status.length)>0)
            {
                this.setState({faculty_blueprint: fac_blueprint_status[0].status,})
            }
          }
        })
  } 
  fetchStudent_Registration_Status()
  {
    axios.post('/ework/user2/stud_status')
        .then(response => {
          if(response.data)
          {
            var stud_reg_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="SIGNUP STATUS")));
            var stud_blueprint_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="BLUE PRINT STATUS")));

            if((stud_reg_status.length)>0)
            {
              this.setState({isCheckedS: stud_reg_status[0].status})
            }
            if((stud_blueprint_status.length)>0)
            {
                this.setState({student_blueprint: stud_blueprint_status[0].status,})
            }
          }
        })
  }


      handleComp= (e)=>{
        this.setState({
          isChecked: !this.state.isChecked,
          history: e.target.value,
        })
        axios.post('/ework/user/handleFaculty_Registration',{
          checked: !this.state.isChecked,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

      handleCompS=(e)=>{
        this.setState({
          isCheckedS: !this.state.isCheckedS,
          history: e.target.value,
        })
        axios.post('/ework/user2/handleStudent_Registration',{
          checked: !this.state.isCheckedS,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

      FBluePrint=(e)=>{
        this.setState({
          faculty_blueprint: !this.state.faculty_blueprint,
          history: e.target.value,
        })

        axios.post('/ework/user/handleFaculty_BluePrint',{
          checked: !this.state.faculty_blueprint,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

      SBluePrint=(e)=>{
        this.setState({
          student_blueprint: !this.state.student_blueprint,
          history: e.target.value,
        })

        axios.post('/ework/user2/handleStudent_BluePrint',{
          checked: !this.state.student_blueprint,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

  render() {
    return (
      <React.Fragment>
         <Paper elevation={3} style={{padding:'15px',margin:'0px 40px 0px 40px'}}>
            <Grid container spacing={1}>
               <Grid container item xs={12} sm={12}>
                 <Grid item xs={8} sm={6}>
                   <Typography align="center">Faculty Registration</Typography>
                 </Grid>
                 <Grid item xs={4} sm={6}>
                   <Switch
                      checked={this.state.isChecked}
                      value="fac" onChange={this.handleComp}
                      inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                 </Grid>
               </Grid>
               <br />
               <Grid container item xs={12} sm={12}>
                 <Grid item xs={8} sm={6}>
                   <Typography align="center">Student Registration</Typography>
                 </Grid>
                 <Grid item xs={4} sm={6}>
                   <Switch
                      checked={this.state.isCheckedS} value="stud" onChange={this.handleCompS}
                      inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                 </Grid>
               </Grid>
               <br />
               <Grid container item xs={12} sm={12}>
                 <Grid item xs={8} sm={6}>
                    <Typography align="center">Render Faculty Blue Print</Typography>
                 </Grid>
                 <Grid item xs={4} sm={6}>
                   <Switch align="center"
                      checked={this.state.faculty_blueprint} value="fac" onChange={this.FBluePrint}
                      inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                 </Grid>
               </Grid>
               <br />
               <Grid container item xs={12} sm={12}>
                 <Grid item xs={8} sm={6}>
                    <Typography align="center">Render Student Blue Print</Typography>
                 </Grid>
                 <Grid item xs={4} sm={6}>
                   <Switch
                      checked={this.state.student_blueprint} value="stud" onChange={this.SBluePrint}
                      inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                 </Grid>
               </Grid>
            </Grid>
         </Paper>
      </React.Fragment>
    );
  }
}
