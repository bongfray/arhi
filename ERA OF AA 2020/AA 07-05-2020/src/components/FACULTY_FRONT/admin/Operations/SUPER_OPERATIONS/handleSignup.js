import React, { Component } from 'react'
import axios from 'axios';
import {Switch,Typography,CircularProgress,Backdrop,FormControlLabel} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {InputBase,Divider,IconButton} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:false,
      requests:[],
      snack_open:false,
      alert_type:'',
      snack_msg:'',
      value_for_search:'',
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,username,name,mailid)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })
      this.setState({
        snack_open:true,
        snack_msg:'Sending Activation Mail...',
        alert_type:'info',
        display:true,
      });
      axios.post('/ework/user/active_user',{
        username: username,
        name:name,
        mailid:mailid,
      })
      .then(res=>{
        this.setState({
          snack_open:true,
          snack_msg:'Activated !!',
          alert_type:'success',
          display:false,
        });
            const { requests } = this.state;
                  this.setState({
                    requests: requests.filter(product => product.username !== username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    this.setState({display:true})
    if(this.props.choice === 'faculty')
    {
      route = '/ework/user/fetch_signup_request_for_admin';
      axios.get(route)
      .then(res=>{
          this.setState({display:false,requests:res.data})
      })
    }
    else if(this.props.choice === 'student')
    {
      axios.post('/ework/user2/fetch_signup_request_for_admin',{user:''})
      .then(res=>{
          this.setState({display:false,requests:res.data})
      })
    }
  }
  searchEngine =(e)=>{
    this.setState({value_for_search:e.target.value})
  }

  render()
  {
        if(this.state.display)
        {
          return(
            <Backdrop  open={true} style={{zIndex:'2040'}}>
              <CircularProgress style={{color:'yellow'}}/>&emsp;
              <div style={{color:'yellow'}}>Processing Your Request.....</div>
            </Backdrop>
          )
        }
        else{
          var requests;
          if(this.props.filter === 'Accepted') {
            requests = this.state.requests.filter(item=>item.active === true);
          }
          else if(this.props.filter === 'Denied')
          {
            requests = this.state.requests.filter(item=>item.active === 'denied');
          }
          else {
            requests = this.state.requests.filter(item=>(item.active === false));
          }
          var libraries = requests,
          searchString = this.state.value_for_search.trim().toLowerCase();
          libraries = libraries.filter(function(i) {
            return i.username.toLowerCase().match( searchString );
          });

        return(
          <React.Fragment>

          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          <div style={{padding:'30px'}}>
           <CustomizedInputBase value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
         </div>


          {this.props.choice &&
              <div style={{marginTop:'30px'}}>
              {libraries.length===0 ?
                <React.Fragment>
                  <Typography variant="h5" align="center">No Request Found !!</Typography>
                </React.Fragment>
                :
              <TableContainer component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">Name</TableCell>
                      <TableCell align="center">Official Id</TableCell>
                      <TableCell align="center">Mail Id</TableCell>
                      <TableCell align="center">Campus</TableCell>
                      <TableCell align="center">Department</TableCell>
                      <TableCell align="center">Designation</TableCell>
                      <TableCell align="center">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {libraries.map((row,index) => (
                      <TableRow key={index}>
                        <TableCell align="center">
                          {row.name}
                        </TableCell>
                        <TableCell align="center">{row.username}</TableCell>
                        <TableCell align="center">{row.mailid}</TableCell>
                        <TableCell align="center">{row.campus}</TableCell>
                        <TableCell align="center">{row.dept}</TableCell>
                        <TableCell align="center">{row.desgn}</TableCell>
                        <TableCell align="center">
                            <FormControlLabel
                                control={<Switch disabled={row.active ? true : row.otp_verified ? false :true}
                                   checked={this.state.isChecked} value="fac"
                                   onChange={(e)=>{this.Active(e,index,row.username,row.name,row.mailid)}} name="checkedA" />}
                                label={!row.otp_verified && "Otp Not Verified"}
                              />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
                </TableContainer>
              }
            </div>
           }
          </React.Fragment>
        ) ;
       }
  }
}



const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function CustomizedInputBase(props) {
  const classes = useStyles();
  return (
    <Paper style={{width:'100%'}} component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder="Search Only Official Id" value={props.value_for_search} onChange={props.searchEngine}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
