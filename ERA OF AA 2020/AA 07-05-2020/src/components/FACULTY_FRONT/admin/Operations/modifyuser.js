import React, { Component } from 'react'
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import {FormControl,Grid,Hidden} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';


import ValidateUser from '../validateUser'


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class ModifyUser extends Component{
  constructor(props){
      super(props);
      this.initialState={
        success:'',
        modal:false,
        disabled:'',
        allowed:false,
          done:'',
          cday:'',
          cmonth:'',
          cyear:'',
          replacable_day_order:'',
          snack_open:false,
          snack_msg:'',
          alert_type:'',
      }
      this.state = this.initialState;
  }

  componentDidUpdate =(prevProps,prevState) => {
    if (prevState.allowed !== this.state.allowed) {
      this.navigate();
    }
  }
  showDiv =(userObject) => {
    this.setState(userObject)
  }

  navigate =(e) =>{
    this.setState({success:'block',disabled:true})
    if(this.state.allowed === true)
    {
      this.setState({success:'none',disabled:false})
      if(this.props.select === "modifytoday")
      {
        this.Reset_Today_DayOrder()
      }
      else if(this.props.select === "cancel_or_declare_today_as_a_holiday")
      {
        this.handleCancel_Holiday()
      }
    }
  }


  handleChange = (e) =>{
    this.setState({
      [e.target.name]: e.target.value,
    })
  }
  /*------------------------------------Cancellation For Holiday & Also DayOrder Cancelled---------------- */
  handleCancel_Holiday = (e) =>{
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    if((this.state.cday === day) && (this.state.cmonth === month) && (this.state.cyear === year))
    {
    axios.post('/ework/user/declare_cancel_or_holiday',this.state,
    this.setState({done:''})
  )
    .then(res=>{
      this.setState({done:res.data,disabled:'',
      success:'none'})
    })
    .catch(error =>{
      this.setState({
        snack_open:true,
        snack_msg:'Something Went Wrong',
        alert_type:'error',
      })
    })
  }
  else
  {
    this.setState({
      cday:'',cmonth:'',
      cyear:'',disabled:false,
      success:'none',
      snack_open:true,
      snack_msg:'Invalid Input !!',
      alert_type:'warning',
    });
  }
  }

/*------------------------------------------------Reset Today's DayOrder---------------------------- */
Reset_Today_DayOrder = (e) =>{
    axios.post('/ework/user/handle_change_today_dayorder',{day_order:this.state.replacable_day_order})
    .then(res=>{
      if(res.data)
      {
        this.setState({
        replacable_day_order:'',
        disabled:false,
        success:'none',
        snack_open:true,
        snack_msg:res.data,
        alert_type:'info',})
      }
    })
}


  render(){
    let content;
     if(this.props.select === 'modifytoday'){
          content =
                <Grid container spacing={1}>
                   <Hidden xsDown><Grid item sm={3}/></Hidden>
                   <Grid item sm={6} xs={12}>
                        <Paper  variant="outlined" square style={{padding:'10px 10px 45px 10px'}}>
                            <FormControl fullWidth variant="outlined">
                              <InputLabel htmlFor="outlined-adornment-amount">Reset Today's Day Order to:</InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount1"
                                onChange={this.handleChange}
                                type="text" name="replacable_day_order" className="validate" value={this.state.replacable_day_order}
                                startAdornment={<InputAdornment position="start">DayOrder</InputAdornment>}
                                labelWidth={200}
                              />
                            </FormControl>
                           <br /><br />
                           <Button style={{float:'right'}} variant="contained" color="primary" disabled={this.state.replacable_day_order ? false:true} onClick={this.navigate}>Reset Day Order</Button>
                        </Paper>
                      </Grid>
                     <Hidden xsDown><Grid item sm={3}/></Hidden>
                  </Grid>
      }
      else if(this.props.select === 'cancel_or_declare_today_as_a_holiday'){
          content =
          <Grid container spacing={1}>
             <Hidden xsDown><Grid item sm={3}/></Hidden>
             <Grid item sm={6} xs={12}>
                  <Paper elevation={3} style={{padding:'20px 10px 45px 10px'}}>
                    <Grid container spacing={1}>
                       <Grid item xs={12} sm={4}>
                           <FormControl fullWidth variant="outlined">
                             <InputLabel htmlFor="outlined-adornment-amount">Today's Day</InputLabel>
                             <OutlinedInput
                               id="outlined-adornment-amount2"
                               type="Number" name="cday" className="validate" value={this.state.cday} onChange={this.handleChange}
                               labelWidth={100}
                             />
                           </FormControl>
                       </Grid>
                       <Grid item xs={12} sm={4}>
                           <FormControl fullWidth variant="outlined">
                             <InputLabel htmlFor="outlined-adornment-amount">Today's Month</InputLabel>
                             <OutlinedInput
                               id="outlined-adornment-amount3"
                               type="Number" name="cmonth" className="validate" value={this.state.cmonth} onChange={this.handleChange}
                               labelWidth={105}
                             />
                           </FormControl>
                       </Grid>
                       <Grid item xs={12} sm={4}>
                           <FormControl fullWidth variant="outlined">
                             <InputLabel htmlFor="outlined-adornment-amount">Today's Year</InputLabel>
                             <OutlinedInput
                               id="outlined-adornment-amount4"
                               type="Number" name="cyear" className="validate" value={this.state.cyear} onChange={this.handleChange}
                               labelWidth={100}
                             />
                           </FormControl>
                       </Grid>
                    </Grid>
                     <br /><br />
                     <Button style={{float:'right'}} variant="contained" color="primary"
                     disabled={(this.state.cday && this.state.cmonth && this.state.cyear)?false :true}
                     onClick={this.navigate}>Reset Day Order</Button>
                  </Paper>
                </Grid>
               <Hidden xsDown><Grid item sm={3}/></Hidden>
            </Grid>

      }
      else{
          content=
              <div></div>
      }
      return(
        <React.Fragment>
        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        open={this.state.snack_open} autoHideDuration={2000}
        onClose={()=>this.setState({snack_open:false})}>
          <Alert onClose={()=>this.setState({snack_open:false})}
          severity={this.state.alert_type}>
            {this.state.snack_msg}
          </Alert>
        </Snackbar>

        {this.state.success === 'block' &&
         <ValidateUser showDiv={this.showDiv}/>
        }
        {content}
        </React.Fragment>

      )
  }
}
