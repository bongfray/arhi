import React, { Component} from 'react'
import { } from 'react-router-dom'
import M from 'materialize-css'
import { Redirect} from 'react-router-dom'
import Nav from '../../dynnav'
import axios from 'axios'
import ContentLoader from "react-content-loader"
import Pdf from './pdf_alldata'

export default class ViewSubject extends React.Component{
      _isMounted = false;
  constructor()
  {
    super()
    this.state={
      loading:true,
      option:'',
      redirectTo:'',
      modal: false,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
 componentDidMount()
 {
   this._isMounted = true;
   this.getUser();
   M.AutoInit()
 }
 getUser()
 {
   axios.get('/ework/user/'
 )
    .then(response =>{
     if (this._isMounted) {
       if(response.status === 200)
       {
         this.setState({
           loading:false,
         })
      if(response.data.user)
      {

      }
      else{
        this.setState({
          redirectTo:'/ework/faculty',
        });
        window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    }
    }
    })
 }



 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }
 render()
 {
   const MyLoader = () => (
     <ContentLoader
       height={160}
       width={400}
       speed={2}
       primaryColor="#f3f3f3"
       secondaryColor="#c0c0c0"
     >
       <rect x="95" y="15" rx="3" ry="3" width="220" height="20" />
       <rect x="320" y="15" rx="3" ry="3" width="20" height="20" />

     </ContentLoader>
   )
   if(this.state.loading === true)
   {
     return(
       <MyLoader />
     );
   }
   else if(this.state.loading === false){

   if (this.state.redirectTo) {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
 } else {
   return(
     <React.Fragment>
     <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
     <div style={{marginTop:'15px'}} className="row">
     <div  className="col offset-l3 offset-xl3 l6 xl6 s12">
         <select value={this.state.option} onChange={this.handleOption}>
          <option value="" disabled defaultValue>Choose the Field Here...</option>
          <option value="Section">Section Datas</option>
          <option value="Academic">Academic Data</option>
          <option value="Administrative">Admininstrative Datas</option>
          <option value="Research">Research Datas</option>
          <option value="Five">Five Year Plan Data</option>
       </select>
       </div>
       <div className="col l3"><Pdf /></div>
       </div>
    <div className="row">
           <Decider data={this.state.option} />
    </div>
   </React.Fragment>
 );
}
}
 }
}

class Decider extends Component{
  render()
  {
    let body,route;
    if(this.props.data === "Section")
    {
      body = <Section />
    }
    else if(this.props.data === "Five")
    {
      body = <Five />
    }
    else if((this.props.data === "Administrative"))
    {
      route = '/ework/user/fetch_adminis_for_view';
      body = <AdRes action={route}/>
    }
    else if(this.props.data === "Research")
    {
      route = '/ework/user/fetch_research_for_view';
      body = <AdRes action={route}/>
    }
    else if(this.props.data === "Academic")
    {
      route = '/ework/user/fetch_academic_for_view';
      body = <AdRes action={route}/>
    }
    else{
      body
       = <div></div>
    }
    return(
      <React.Fragment>
      {body}
      </React.Fragment>
    )
  }
}

class Section extends Component {
  constructor() {
    super()
    this.state={
      section_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_section_for_view').then(res => {
      this.setState({
        loader:false,
        section_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let sec_data;
    if(this.state.loader === true)
    {
      sec_data = <div className="center">
        <div class="preloader-wrapper big active">
        <div className="spinner-layer spinner-red">
            <div className="circle-clipper left">
             <div className="circle"></div>
            </div><div className="gap-patch">
             <div className="circle"></div>
            </div><div className="circle-clipper right">
             <div className="circle"></div>
            </div>
        </div>
        </div>
      </div>
    }
    else{
      sec_data =       <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">
                  {this.state.section_data.length === 0 && <h5 className="red-text center" style={{marginTop:'30px'}}> No Data Found !!</h5>}
                {this.state.section_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}}>
                        <h6 className="count center">{item.action}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>Description</p>
                             <p>Your Role</p>
                             <p>Your Achivements</p>
                             <p>Duration</p>
                           </div>
                           <div className="col l6">
                             <p>{item.description}</p>
                             <p>{item.role}</p>
                             <p>{item.achivements}</p>
                             <p>{item.date_of_starting} / {item.date_of_complete}</p>
                           </div>
                        </div>
                      </div>
                      </div>

                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.section_data.length &&
                   <button style={{background:'linear-gradient(to bottom, #ff5050 0%, #ff99cc 100%)'}} onClick={this.loadMore} type="button" className="btn load-more">Load more</button>
                }
                </div>
              </React.Fragment>
    }
    return (
      <React.Fragment>
         {sec_data}
        </React.Fragment>
    );
  }
}


class AdRes extends Component {
  constructor() {
    super()
    this.state={
      free_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    this.fetchData(this.props.action);
  }

  componentDidUpdate =(prevProps)=>{
    if(prevProps.action !== this.props.action)
    {
      this.fetchData(this.props.action)
    }
  }

  fetchData=(route)=>{
    axios.post(route).then(res => {
      this.setState({
        loader:false,
        free_data: res.data,
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let adres_data;
    if(this.state.loader === true)
    {
      adres_data = <div className="center">
        <div class="preloader-wrapper big active">
        <div className="spinner-layer spinner-red">
            <div className="circle-clipper left">
             <div className="circle"></div>
            </div><div className="gap-patch">
             <div className="circle"></div>
            </div><div className="circle-clipper right">
             <div className="circle"></div>
            </div>
        </div>
        </div>
      </div>
    }
    else{
      adres_data =      <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">
                {this.state.free_data.length === 0 && <h5 className="red-text center" style={{marginTop:'30px'}}> No Data Found !!</h5>}
                {this.state.free_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment  key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}}>
                        <h6 className="count center">{item.freefield}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>About the Responsibility</p>

                             {item.freeslot && <p>Content of Submission</p>}
                             {item.covered && <p>Topics Covered</p>}
                             {item.problem_statement && <p>Problem in Slot Handle</p>}

                             <p>Submitted Date</p>
                             <p>Submitted DayOrder.Slot</p>
                           </div>
                           <div className="col l6">
                             <p>{item.freeparts}</p>
                             {item.freeslot && <p>{item.freeslot}</p>}
                             {item.covered && <p>{item.covered}</p>}
                             {item.problem_statement && <p>{item.problem_statement}</p>}

                             <p>{item.date} / {item.month} /{item.year}</p>
                             <p>{item.day_slot_time}</p>
                           </div>
                        </div>
                      </div>
                      </div>
                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.free_data.length &&
                   <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
                }
                </div>
              </React.Fragment>
    }
    return (
      <React.Fragment>
        {adres_data}
        </React.Fragment>
    );
  }
}


class Five extends Component {
  constructor() {
    super()
    this.state={
      five_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_five_for_view').then(res => {
      this.setState({
        loader:false,
        five_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let five_dat;
    if(this.state.loader === true)
    {
      five_dat = <div className="center">
        <div class="preloader-wrapper big active">
        <div className="spinner-layer spinner-red">
            <div className="circle-clipper left">
             <div className="circle"></div>
            </div><div className="gap-patch">
             <div className="circle"></div>
            </div><div className="circle-clipper right">
             <div className="circle"></div>
            </div>
        </div>
        </div>
      </div>
    }
    else{
      five_dat =       <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">
              {this.state.five_data.length === 0 && <h5 className="red-text center" style={{marginTop:'30px'}}> No Data Found !!</h5>}
                {this.state.five_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}} >
                        <h6 className="count center">{item.action}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>About the Plan</p>
                             <p>Plan Will Expire on</p>
                             <p>Completed Status</p>
                           </div>
                           <div className="col l6">
                             <p>{item.content}</p>
                             <p>Year {item.expire_year}</p>
                             {item.completed?  <p>Completed</p> : <p>Not Completed</p>}
                           </div>
                        </div>
                      </div>
                      </div>
                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.five_data.length &&
                   <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
                }
                </div>
              </React.Fragment>
    }




    return (
      <React.Fragment>
       {five_dat}
        </React.Fragment>
    );
  }
}
