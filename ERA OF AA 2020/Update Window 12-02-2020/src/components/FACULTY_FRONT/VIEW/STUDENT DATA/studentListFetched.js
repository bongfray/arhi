
import React, {Component} from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Backdrop from '@material-ui/core/Backdrop';
import Rating from '@material-ui/lab/Rating';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';
import SendIcon from '@material-ui/icons/Send';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TablePagination from '@material-ui/core/TablePagination';

import ViewStudentData from '../../FACULTY ADVISER/view_single_student'




export default class ShowStudentList extends Component {
 constructor()
 {
   super()
   this.state={
     loading:true,
     students_skill:'',
     students_domain:'',
     details_view:false,
     data:'',
     mdata:'',
     mwindow:false,
   }
   this.componentDidMount = this.componentDidMount.bind(this)
 }

 componentDidMount()
 {
   this.fetchData();
 }

 componentDidUpdate=(prevProps)=>{
   if(prevProps!== this.props)
   {
     this.fetchData();
   }
 }

 fetchData=()=>{
   if(this.props.type === 'skill_fetch')
   {
     axios.post('/ework/user2/fetch_student_on_a_skill',{skill:this.props.skill_selected})
     .then( res => {
         this.setState({students_skill:res.data,loading:false})
     });
   }
   else if(this.props.type === 'domain_fetch') {
     axios.post('/ework/user2/fetch_student_on_a_domain',{domain:this.props.domain_selected})
     .then( res => {
         this.setState({students_domain:res.data,loading:false})
     });
   }

 }

 setDomain=(e)=>{
   if(e === null)
   {

   }
   else{
     this.setState({skill_selected:e.skill_name})
   }
 }

 closeView=()=>{
   this.setState({details_view:false})
 }

 showDetails=(content)=>{
   this.setState({data:content,details_view:true})
 }

 showPopUp=(mdata)=>{
   this.setState({mwindow:true,mdata:mdata})
 }
 mWindow=()=>{
   this.setState({mwindow:!this.state.mwindow})
 }

 retriveMessage=(e)=>{
   this.setState({r_message:e.target.value})
 }
 sendMessage=()=>{
   if(!this.state.r_message)
   {
              window.M.toast({html: 'Enter the message !!',classes:'rounded red'});
   }
   else {
     axios.post('/user/user2/sendMessage_From_User',{message:this.state.r_message})
     .then( res => {
         if(res.data)
         {
            window.M.toast({html: 'Sent !!',classes:'rounded green darken-1'});
           this.mWindow();
         }
     });
   }
 }

 render() {
   if(this.state.loading)
   {
     return(
       <Backdrop  open={true} >
         <CircularProgress color="secondary" />
       </Backdrop>
     )
   }
   else {
   return (
     <React.Fragment>
       {this.state.mwindow &&
           <Dialog open={this.state.mwindow} onClose={this.mWindow} aria-labelledby="form-dialog-title">
             <DialogTitle id="form-dialog-title">Send Message</DialogTitle>
             <DialogContent>
               <DialogContentText>
                This message will be forwarded to the particular student.
               </DialogContentText>
               <TextField
                 required
                 id="filled-required"
                 label="Enter the message"
                 value={this.state.message}
                 onChange={this.retriveMessage}
                 variant="filled"
                 fullWidth
                 multiline
               />
             </DialogContent>
             <DialogActions>
               <Button onClick={this.mWindow} color="primary">
                 Cancel
               </Button>
               <Button onClick={this.sendMessage} color="primary">
                 Send Message
               </Button>
             </DialogActions>
            </Dialog>
       }
     {this.state.details_view  &&
         <ViewStudentData details_view={this.state.details_view}  closeView={this.closeView} data={this.state} />
     }

      {this.state.students_skill ?
         <TableContainer component={Paper}>
         <Table  aria-label="simple table">
           <TableHead>
             <TableRow>
               <TableCell align="left">Student Reg. Id</TableCell>
               <TableCell align="center">Searched For</TableCell>
               <TableCell align="center">Level of Student</TableCell>
               <TableCell align="center">Rating on Skill</TableCell>
               <TableCell align="center">View</TableCell>
               <TableCell align="center">Action</TableCell>
             </TableRow>
           </TableHead>
           <TableBody>
             {this.state.students_skill.map(row => (
               <TableRow key={row.username}>
                 <TableCell align="left">
                   {row.username}
                 </TableCell>
                 <TableCell align="center">{row.skill_name}</TableCell>
                 <TableCell align="center">{row.skill_level}</TableCell>
                 <TableCell align="center">
                      <Rating name="read-only" value={parseInt(row.skill_rating)} readOnly />
                 </TableCell>
                 <TableCell align="center">
                   <Tooltip title="View Details">
                    <VisibilityIcon onClick={()=>this.showDetails(row)} />
                   </Tooltip>
                 </TableCell>
                 <TableCell align="center">
                   <Tooltip title="Send Message To This Student">
                    <SendIcon onClick={()=>this.showPopUp(row)} />
                   </Tooltip>
                 </TableCell>
               </TableRow>
             ))}
           </TableBody>
         </Table>
       </TableContainer>
       :
       <TableContainer component={Paper}>
       <Table  aria-label="simple table">
         <TableHead>
           <TableRow>
             <TableCell align="left">Student Reg. Id</TableCell>
             <TableCell align="center">Searched For</TableCell>
             <TableCell align="center">View</TableCell>
             <TableCell align="center">Action</TableCell>
           </TableRow>
         </TableHead>
         <TableBody>
           {this.state.students_domain.map(row => (
             <TableRow key={row.username}>
               <TableCell align="left">
                 {row.username}
               </TableCell>
               <TableCell align="center">{row.domain_name}</TableCell>
               <TableCell align="center">
                   <Tooltip title="View Details">
                    <VisibilityIcon onClick={()=>this.showDetails(row)} />
                   </Tooltip>
               </TableCell>
               <TableCell align="center">
                 <Tooltip title="Send Message To This Student">
                  <SendIcon onClick={()=>this.showPopUp(row)} />
                 </Tooltip>
               </TableCell>
             </TableRow>
           ))}
         </TableBody>
       </Table>
     </TableContainer>
      }
     </React.Fragment>
   );
  }
 }
}
