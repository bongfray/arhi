import React, { Component} from 'react';
import axios from 'axios'
import {Link,Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


export default class BluePrint extends Component {
	constructor() {
    super();
    this.state = {
			loading: true,
			datas: [
				{
					day_order:'Day Order 1',dayor:'1',
					day:[
						{id:"1.1"},{id:"1.2"},{id:"1.3"},{id:"1.4"},{id:"1.5"},{id:"1.6"},{id:"1.7"},{id:"1.8"},{id:"1.9"},{id:"1.10"}
					]

				},
				{
					day_order:'Day Order 2',dayor:'2',
					day:[
						{id:"2.1"},{id:"2.2"},{id:"2.3"},{id:"2.4"},{id:"2.5"},{id:"2.6"},{id:"2.7"},{id:"2.8"},{id:"2.9"},{id:"2.10"}
					]
				},
				{
					day_order:'Day Order 3',dayor:'3',
					day:[
						{id:"3.1"},{id:"3.2"},{id:"3.3"},{id:"3.4"},{id:"3.5"},{id:"3.6"},{id:"3.7"},{id:"3.8"},{id:"3.9"},{id:"3.10"}
					]
				},
				{
					day_order:'Day Order 4',dayor:'4',
					day:[
						{id:"4.1"},{id:"4.2"},{id:"4.3"},{id:"4.4"},{id:"4.5"},{id:"4.6"},{id:"4.7"},{id:"4.8"},{id:"4.9"},{id:"4.10"}
					]
				},
				{
					day_order:'Day Order 5',dayor:'5',
					day:[
						{id:"5.1"},{id:"5.2"},{id:"5.3"},{id:"5.4"},{id:"5.5"},{id:"5.6"},{id:"5.7"},{id:"5.8"},{id:"5.9"},{id:"5.10"}
					]
				}
			],
			username:'',
			home:'/ework/faculty',
			logout:'/ework/user/logout',
			get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
			display:'block',
    };

		this.componentDidMount = this.componentDidMount.bind(this);
    }

    fetchlogin = () =>{
      axios.get('/ework/user/'
	)
      .then(response =>{
        if(response.data.user === null)
				{
					this.setState({loading: false})
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
				else
				{

					this.setState({username:response.data.user.username})
						this.fetchRenderStatus();
				}
      })
    }

    componentDidMount() {
			if(this.props.props_to_refer)
			{
				this.setState({username:this.props.props_to_refer.username,loading:false})
			}
			else
			{
				this.fetchlogin();
			}

    }

		fetchRenderStatus=()=>{
			axios.post('/ework/user/fac_status',{userEnable:'verify'})
					.then(response => {
						this.setState({loading: false})
						if(response.data === 'no')
						{
							this.setState({display:'none'})
						}
						else if(response.data === 'ok')
						{
							this.setState({display:'block'})
						}
						else
						{
							var fac_blueprint_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="BLUE PRINT STATUS")));
							if((fac_blueprint_status.length)>0)
							{
								if(fac_blueprint_status[0].status === true)
								{
									this.setState({display:'block'})
								}
								else {
									this.setState({display:'none'})
								}
							}
							else {
								this.setState({status:'block'})
							}
						}
					})
		}


render() {
	const MyLoader = () => (
		<ContentLoader
		  height={160}
		  width={400}
		  speed={2}
		  primaryColor="#f3f3f3"
		  secondaryColor="#c0c0c0"
		>
		  <rect x="24" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="35" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="55" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="75" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="95" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="115" rx="0" ry="0" width="38" height="7" />

		</ContentLoader>
	  )
	if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo)
	{
     return <Redirect to={{ pathname: this.state.redirectTo }} />
  }
	else
	 {
		 if(this.state.display === 'none')
		 {
			 return(
				 <Grid container spacing={1}>
				    <Grid item xs={3} sm={3}/>
						<Grid item xs={6} sm={6}>
									<Card style={{marginTop:'60px'}}>
									<CardContent style={{textAlign:'center'}}>
									<Typography gutterBottom variant="h4" style={{color:'red'}} component="h2">
										Oops !!
									</Typography>
									 <Typography variant="button" display="block" gutterBottom>
										 SUBMISSION OF ANY DATA IN THIS PAGE HAS BEEN CLOSE BY ADMIN
									 </Typography>
									 <Typography variant="overline" display="block" gutterBottom>
										 Send request for re-render this page in <Link style={{textDecoration:'none'}} to="/ework/manage">Manage Request Section</Link>
									 </Typography>
									</CardContent>
									<CardActions>
									 <Button size="small" color="primary" style={{float:'right'}}>
									   <Link style={{textDecoration:'none'}} to="/ework/faculty" className="btn small blue-grey darken-2">HOME</Link>
									 </Button>
									</CardActions>
									</Card>
						</Grid>
						<Grid item xs={3} sm={3}/>
				 </Grid>
			 );
		 }
		 else
		 {
			return (
				<React.Fragment>
				 {!(this.props.props_to_refer) &&
					 <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
				 }
		        <div className="row">
		        <table className="center">
		        <thead style={{backgroundColor:'white',color:'black'}}>
		        <tr>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>Hour /</span><br />Day Order</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>1</span><br />08:00 - 08:50</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>2</span><br />08:50 - 09:40</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>3</span><br />09:45 - 10:35</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>4</span><br />10:40 - 11:30</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>5</span><br />11:35 - 12:25</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>6</span><br />12:30 - 01:20</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>7</span><br />01:25 - 02:15</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>8</span><br />02:20 - 03:10</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>9</span><br />03:15 - 04:05</th>
		           <th className="center" style={{borderRight: '1px solid'}}><span style={{color:'red'}}>10</span><br />04:05 - 04:55</th>
		        </tr>
		        </thead>

		        <tbody>
						{this.state.datas.map((content,index)=>{
							return(
							<tr style={{backgroundColor: 'white'}} className="" key={index}>
							  <Stat content={content} dayor={content.dayor} username = {this.state.username}/>
							</tr>
						);

						})}
		        </tbody>
		      </table>


			</div>




		</React.Fragment>
		);
	 }
  }
}
}
}


class Stat extends Component{
	constructor()
	{
		super()
		this.state={
			isAddProduct: false,
			response: {},
			action:'',
			product: {},
			isEditProduct: false,
			added:'',
			disabled:'block',
			editing:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}

	componentDidMount()
	{
	}

	    onCreate = (e,action) => {
	      this.setState({ isAddProduct: action ,product: {},disabled:'none'});
	    }
	    onFormSubmit =(data) => {
				console.log(data)
	      let apiUrl;
	      if(this.state.isEditProduct)
	      {
	        apiUrl = '/ework/user/edit_blue_print';
	      }
	       else
	       {
	        apiUrl = '/ework/user/send_blue_print';
	       }
	      axios.post(apiUrl,data)
	          .then(response => {
							if(response.data.exceed)
							{
								window.M.toast({html: response.data.exceed,outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
							}
							else if(response.data.succ)
							{
								window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded green lighten-1'});
							}
							else if(response.data.emsg)
							{
								window.M.toast({html: response.data.emsg,outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
							}
							if(this.state.isEditProduct)
							{
							 this.setState({added:data.serial})
							}
							 else
							 {
								this.setState({added:this.state.isAddProduct})
							 }

							this.setState({
								response: response.data,
								isEditProduct: false,
								isAddProduct: false,
								disabled:'block',
								editing:'',
							})
	          })
	    }

	    editProduct = (productId,index)=> {
				 this.setState({editing:productId})
	      axios.post('/ework/user/show_editable_blue_print',{
	        id: productId,
	      })
	          .then(response => {
	            this.setState({
	              product: response.data,
	              isEditProduct: index,
	              isAddProduct: index,
	            });
	          })

	   }

		 close=()=>{
			 this.setState({editing:'',isAddProduct:'',isEditProduct:''})
		 }

		 displayPops=(id)=>{
		 	this.setState({popOver:true,popId:id})
		 }
		 displayPopsOff=(id)=>{
			this.setState({popOver:false,popId:id})
		 }

	render()
	{
		return(
			<React.Fragment>
			<td style={{borderRight: '1px solid'}} className="center"><b>{this.props.content.day_order}{this.state.week}</b></td>
			{this.props.content.day.map((content,ind)=>{
											return(
												<td onMouseLeave={()=>this.displayPopsOff(content.id)} onMouseEnter={()=>this.displayPops(content.id)} style={{borderRight: '1px solid '}} key={content.id}>
															 <ShowDetails editing={this.state.editing} creating={this.state.isAddProduct}
															  create={this.onCreate} added={this.state.added} data={content}
																username={this.props.username}  action={content.id}
																editProduct={this.editProduct} popOver={this.state.popOver} popId={this.state.popId}
																/>
															 {((this.state.isAddProduct === content.id) || (this.state.isEditProduct === content.id)) &&
															 <Add data={content}  action={content.id} username={this.props.username}
															 close={this.close}
															 onFormSubmit={this.onFormSubmit}  product={this.state.product} />
															 }
												</td>
											);

											})}
			</React.Fragment>
		);
	}
}




class Add extends Component {
	constructor(props) {
    super(props);
    this.initialState = {
			timing:'',
      username:'',
      action:'',
			status:false,
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (e,index) =>{
    const value = e.target.value;
    this.setState({
      [e.target.name]:value
    })
  }


componentDidMount(){
  this.setState({
    action:this.props.data.id,
		timing:this.props.data.id,
    username: this.props.username,
		status:true,
  })
}



  handleSubmit(event,index) {
      event.preventDefault();
			if(!(this.state.year) || !(this.state.sem) || !(this.state.batch) ||  !(this.state.alloted_slots))
			{
				window.M.toast({html: 'Enter all the details !!', classes:'rounded red'});
			}
			else{
				this.props.onFormSubmit(this.state);
				this.setState(this.initialState);
			}
  }

  render() {
    return(
      <React.Fragment>

			<div className="" >
						<div className="row">
							<div className="col l12">
												<input
														className=""
														type="text"
														name="alloted_slots"
														value={this.state.alloted_slots}
														onChange={this.handleD}
														placeholder="Enter Slot Name"
													/>
							</div>
							<div className="row">
								<div className="col l4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_year">Year</InputLabel>
												<Select
													labelId="sel_year"
													id="sel_year"
													name="year"
													value={this.state.year}
													onChange={this.handleD}
												>
												<MenuItem value="" disabled defaultValue>Year</MenuItem>
												<MenuItem value="1">1</MenuItem>
												<MenuItem value="2">2</MenuItem>
												<MenuItem value="3">3</MenuItem>
												<MenuItem value="4">4</MenuItem>
												<MenuItem value="5">5</MenuItem>
												</Select>
										 </FormControl>
								</div>
								<div className="col l4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_sem">Sem</InputLabel>
												<Select
													labelId="sel_sem"
													id="sel_sem"
													name="sem"
													value={this.state.sem}
													onChange={this.handleD}
												>
												<MenuItem value="" disabled defaultValue>Sem</MenuItem>
												<MenuItem value="1">1</MenuItem>
												<MenuItem value="2">2</MenuItem>
												<MenuItem value="3">3</MenuItem>
												<MenuItem value="4">4</MenuItem>
												<MenuItem value="5">5</MenuItem>
												<MenuItem value="6">6</MenuItem>
												<MenuItem value="7">7</MenuItem>
												<MenuItem value="8">8</MenuItem>
												<MenuItem value="9">9</MenuItem>
												<MenuItem value="10">10</MenuItem>
												</Select>
										 </FormControl>
								</div>
								<div className="col l4">
										<FormControl style={{width:'100%'}}>
											<InputLabel id="sel_batch">Batch</InputLabel>
												<Select
													labelId="sel_batch"
													id="sel_batch"
													name="batch"
													value={this.state.batch}
													onChange={this.handleD}
												>
												<MenuItem value="" disabled defaultValue>Batch</MenuItem>
												<MenuItem value="B1">B-1</MenuItem>
												<MenuItem value="B2">B-2</MenuItem>
												</Select>
										 </FormControl>
								</div>
							</div>
						</div>

					<div>
						<button className="btn btn-small red" onClick={this.props.close} type="submit">Cancel</button>
						<button className="btn right blue-grey darken-2" onClick={this.handleSubmit} type="submit">UPLOAD</button>
					</div>
			</div>
      </React.Fragment>
    )
  }
}



class ShowDetails extends Component {
	constructor(props) {
    super(props);
    this.state = {
      username:'',
      error: null,
      products: [],
      action:'',
			loading:true,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if ((prevProps.action !== this.props.action) || (prevProps.added !== this.props.added)) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user/del_blue_print',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =(e) =>{
	let route;
	if(e)
	{
		route = e;
	}
	else
	{
	route =this.props.action;
 }
  axios.post('/ework/user/fetch_blue_print',{
    action: route,
  })
  .then(response =>{
    this.setState({
			loading:false,
      products: response.data,
   })
  })
}

  render() {
      return(
        <React.Fragment>
				{this.state.loading === false ?
						<React.Fragment>
									{this.state.products.length!==0 ?
										<React.Fragment>
											        {this.state.products.map((content,index)=>(
																<React.Fragment>
																	{!(this.props.editing === content.serial) &&
																		<div className="row" style={{display:this.props.disabled}}>
													              <div className="col l12 s4 m4 xl12 left">
																					   <div className="center"><span className="red-text">Slot - </span>{content.alloted_slots}</div>
																						 <div className="row">
																						   <div className="col l4 center">
																							 <span className="red-text">Year : </span>{content.year}
																							 </div>
																							 <div className="col l4 center">
																							   <span className="red-text">Sem : </span>{content.sem}
																							 </div>
																							 <div className="col l4 center">
																							   <span className="red-text">B : </span>{content.batch}
																							 </div>
																						 </div>
																					 </div>
																					 {((this.props.popOver) && (this.props.popId === this.props.action)) &&
																						 <div className="popup_over right">
																							 <i className="material-icons go" onClick={() => this.props.editProduct(content.serial,this.props.action)}>edit</i>
																							 <i className="material-icons go" onClick={() => this.deleteProduct(content.serial,this.props.action)}>delete</i>
																						</div>
																					}
																				</div>
																  }
																	</React.Fragment>
											        ))}
											</React.Fragment>
									 :<div>
									{!(this.props.creating === this.props.data.id) && <div className="btn-floating btn-small right" style={{display:this.props.disabled}} onClick={(e) => this.props.create(e,this.props.data.id)}><i className="material-icons go">add</i></div> }
									</div>
								}
						</React.Fragment>
			:
			<div className="center">
				  <div class="preloader-wrapper small active">
					<div className="spinner-layer spinner-red">
							<div className="circle-clipper left">
							 <div className="circle"></div>
							</div><div className="gap-patch">
							 <div className="circle"></div>
							</div><div className="circle-clipper right">
							 <div className="circle"></div>
							</div>
					</div>
					</div>
			</div>
		}
			</React.Fragment>
      )
  }
}
