import React, { Component} from 'react'
import { } from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NavAdd from '../CRUD_In_Input/NavHandle'
import AddSection from '../CRUD_In_Input/handleSection'
import Res from '../CRUD_In_Input/handleRes'
import InsertDesignation from '../CRUD_In_Input/insertDesignation'
import InsertDept from '../CRUD_In_Input/handleDept'
import axios from 'axios'


export default class InsertUser extends Component{
    constructor(){
        super();
        this.initialState = {
            username: 'D',
            mailid: '',
            campus:'',
            dept:'',
            h_order:'0.5',
            password:'',
            deptToken:'',
            suspension_status:true,
            suprimity:false,
            faculty_adviser:false,
            faculty_adviser_year:'',
            render_timetable:false,
            task:'',
            resetPasswordToken:'',
            resetPasswordExpires:'',
            count:3,
            active:false,
        }
        this.state = this.initialState;
    }
    handleField = (e) =>{
        this.setState({
          [e.target.name] : e.target.value,
        })
    }
    InsertDeptAdmin =(event)=>
    {
          event.preventDefault()
          if(!(this.state.username) || !(this.state.dept) || !(this.state.campus) || !(this.state.mailid))
          {
            window.M.toast({html: 'Fill all the Details Please!!',classes:'rounded red lighten-1'});
          }
          else
          {
          window.M.toast({html: 'Submitting...... !!',classes:'rounded green lighten-1'});
          axios.post('/ework/user/insert_dept_admin',{data:this.state})
          .then( res => { 
              if(res.data === 'ok')
              {
                window.M.toast({html: 'Successfully Inserted  !!',classes:'rounded green lighten-1'});
              }
              else if(res.data === 'no')
              {
                window.M.toast({html: 'User Already There !!',classes:'rounded pink lighten-1'});
              }
              this.setState(this.initialState)
          });
        }
    }
    render(){
        if(this.props.select==='insertadmin')
        {
            return(
              <React.Fragment>
                <div className="row">
                    <div className="input-field col l3">
      								<input id="username" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleField} required />
      								<label htmlFor="username">Enter Offical Id</label>
    								</div>

    								<div className="input-field col l3">
      								<input id="mailid" type="email" className="validate" name="mailid" value={this.state.mailid} onChange={this.handleField} required />
      								<label htmlFor="mailid">Maild Id</label>
    								</div>
                    <div className="input-field col l3 xl3">
                        <FormControl style={{width:'100%'}}>
                           <InputLabel id="campus">Campus</InputLabel>
                           <Select
                             labelId="campus"
                             id="campus"
                             value={this.state.campus}
                             name="campus" onChange={this.handleField}
                           >
                             <MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
                             <MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
                             <MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
                             <MenuItem value="NCR Campus">NCR Campus</MenuItem>
                           </Select>
                         </FormControl>
                    </div>
                    <div className="input-field col l3 xl3">
                        <FormControl style={{width:'100%'}}>
                           <InputLabel id="campus">Campus</InputLabel>
                           <Select
                             labelId="campus"
                             id="campus"
                             value={this.state.dept}
                             name="dept" onChange={this.handleField}
                           >
                           <MenuItem value="Computer Science">Computer Science</MenuItem>
                           <MenuItem value="Information Technology">Information Technology</MenuItem>
                           <MenuItem value="Software Engineering">Software</MenuItem>
                           <MenuItem value="Mechanical Engineering">Mechanical</MenuItem>
                           </Select>
                         </FormControl>
                    </div>
                </div>
                <div className="row">
                   <button className="right btn #37474f blue-grey darken-3 col l4" onClick={this.InsertDeptAdmin}>Insert Department Admin</button>
                </div>
                </React.Fragment>
                )
        }
        else if(this.props.select==='insert_in_nav')
        {
            return(
                  <NavAdd />
                )
        }
        else if(this.props.select==='responsibilty_percentages')
        {
            return(
                  <Res />
                )
        }
        else if(this.props.select==='section_part_insert')
        {
            return(
                     <AddSection />
                )
        }
        else if(this.props.select === 'insert_designation')
        {
          return(
            <InsertDesignation />
          )
        }
        else if(this.props.select === 'insert_department')
        {
          return(
            <InsertDept/>
          )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}
