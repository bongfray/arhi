import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class Notification extends Component {
    constructor()
    {
        super()
        this.state={
            isExpanded:false,
            display:'',
            loader:true,
            order_action:'faculty',
            notification:[],
            covered:[],
            fetching:true,
            reply_message:'',
            permission:'',
            modal_open:false,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchNoti()
    }

  fetchNoti = ()=>{
    axios.get('/ework/user/fetch_notification')
    .then( res => {
      this.setState({loader:false})
        if(res.data.length === 0)
        {
          this.setState({notfound:'Canot able to find any notification for you !!'})
        }
        else{
          this.setState({notification:res.data})
        }
    });
  }
    clearOne =(id)=>{
      const { notification } = this.state;
      axios.post('/ework/user/clear_one_notification',{id:id})
          .then(response => {
            this.setState({
              response: response,
              notification: notification.filter(noti => noti.serial !== id)
           })
          })
    }
    clearAll = ()=>{
      axios.post('/ework/user/clear_all_notification')
      .then( res => {
        this.setState({
          notification: [],
       })
      });
    }
    setDisplay =() =>{
      this.props.setDisp({
        notidisp: 'none',
      });
    }
    referOther =(ref,dbname)=>{
      axios.post('/ework/user/referNotification_Fetch',{ref:ref})
      .then( res => {
          if(res.data)
          {
              this.setState({covered:res.data,fetching:false})
          }
      });
    }

    handleInput =(e)=>{
      this.setState({[e.target.name]:e.target.value})
    }

    sendReply =(no,id)=>{
      if(!(this.state.reply_message))
      {
        window.M.toast({html: 'Enter all the Details !!',outDuration:'9000', classes:'rounded yellow black-text'});
      }
      else{
      axios.post('/ework/user/send_reply_to_complaint',{serial:no,official_id:id,message:this.state.reply_message})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({reply_message:''});
          this.handleClose();
          window.M.toast({html: 'Replied !!',classes:'rounded green darken-2'});
        }
      });
     }
    }

    updatePermission=(e,content)=>{
      this.setState({permission:e.target.value})
      axios.post('/ework/user/giving_permission',{permission:e.target.value,
        hour:content.academic_hour,content:content})
      .then( res => {
        if(res.data === 'done')
        {
          window.M.toast({html: 'Succeed !!',classes:'rounded green darken-2'});
        }
      });
    }

    handleExpand =(e,index,serial) => {
      this.setState({isExpanded:!this.state.isExpanded,index});
      axios.post('/ework/user/mark_as_read',{serial:serial})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({newDisplay:'none'})
        }
      });
    };
    handleClose=()=>{
      this.setState({modal_open:!this.state.modal_open})
    }

    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

        return (
            <React.Fragment>
            <div className="notinoti">
            <div className="row blue-grey darken-2 white-text">
            <div className="col l1">
            <div className="left go" style={{marginTop:'10px',marginLeft:'10px'}} onClick={this.setDisplay}><i className="small material-icons white-text">close</i></div>
            </div>
            <div className="col l10 center"><h5>Notifications</h5></div>
            <div className="col l1" />
            </div>
            {this.state.loader === true ?
                <React.Fragment>
                  <div className="center">
                      <div className="preloader-wrapper big active">
                      <div className="spinner-layer spinner-red">
                          <div className="circle-clipper left">
                           <div className="circle"></div>
                          </div><div className="gap-patch">
                           <div className="circle"></div>
                          </div><div className="circle-clipper right">
                           <div className="circle"></div>
                          </div>
                      </div>
                      </div>
                  </div>
                </React.Fragment>
                :
            <div className="row">
            <div className="col l1" />
            <div className="col l10">
            {this.state.notfound && <h5 className="center">{this.state.notfound}</h5>}
            {this.state.notification.map((content,index)=>{
              return(
                <React.Fragment key={index}>
                <div className="row">
                <div className="col l11">
                <ExpansionPanel expanded={this.state.isExpanded} style={{borderRadius:'20px'}} onChange={(e)=>this.handleExpand(e,index,content.serial)}>
                  <ExpansionPanelSummary
                  style={{borderRadius:'20px'}}
                    expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                    className="mine-collap"
                  >
                    <div style={{color:'yellow',fontSize:'25px'}}>{content.subject}</div>
                          </ExpansionPanelSummary>
                              <div style={{padding:'15px'}}>
                              <div className="">
                                   <p>{content.details}</p>
                                   {content.permission &&
                                     <React.Fragment>
                                        <span className="pink-text">Requested hour of today - </span><span>{content.academic_hour}</span>
                                        <RadioGroup aria-label="permit" name="permit" value={this.state.permission} onChange={(e)=>this.updatePermission(e,content)}>
                                         <FormControlLabel value="allow" control={<Radio />} label="Ok" />
                                         <FormControlLabel value="deny" control={<Radio />} label="Deny" />
                                        </RadioGroup>
                                     </React.Fragment>
                                   }
                                   {content.ref &&
                                       <div>
                                          <span className="red-text">MESSAGE FROM EWORK : </span>You have been given the supreme priority for this complaint. You have to handle this complaint.
                                       </div>
                                   }
                                   <br />
                                   {content.ref &&
                                     <div style={{marginBottom:'30px'}}>
                                     <Button variant="outlined" color="primary" onClick={()=>this.referOther(content.ref,content.db_name)}>Reference</Button>
                                        <Dialog
                                          open={this.state.modal_open}
                                          TransitionComponent={Transition}
                                          keepMounted
                                          onClose={this.handleClose}
                                          aria-labelledby="alert-dialog-slide-title"
                                          aria-describedby="alert-dialog-slide-description"
                                        >
                                          <DialogTitle id="alert-dialog-slide-title">{"Use Google's location service?"}</DialogTitle>
                                          <DialogContent>
                                          {this.state.fetching === true ?
                                            <div className="center">Fetching Information....</div>
                                            :
                                               <div className="col l12">
                                                   <React.Fragment>
                                                   <div className="row">
                                                      <div className="col l6">Complaint By</div>
                                                      <div className="col l6">{this.state.covered.official_id}</div>
                                                   </div>
                                                   <div className="row">
                                                      <div className="col l6">Mail Id to Contact</div>
                                                      <div className="col l6">{this.state.covered.mailid}</div>
                                                   </div>
                                                   <div className="row">
                                                      <div className="col l6">Complaint Subject</div>
                                                      <div className="col l6">{this.state.covered.complaint_subject}</div>
                                                   </div>
                                                   <div className="row">
                                                      <div className="col l6">Complaint Details</div>
                                                      <div className="col l6">{this.state.covered.complaint}</div>
                                                   </div>

                                                       <label className="pure-material-textfield-outlined alignfull">
                                                         <textarea
                                                           className=""
                                                           type="text"
                                                           placeholder=" "
                                                           min="10"
                                                           max="60"
                                                           name="reply_message"
                                                           value={this.state.reply_message}
                                                           onChange={this.handleInput}
                                                         />
                                                         <span>Reply to the User</span>
                                                       </label>
                                                       <br />
                                                   <button className="right btn" onClick={()=>this.sendReply(this.state.covered.serial,this.state.covered.official_id)} style={{marginBottom:'5px'}}>REPLY</button>
                                                   </React.Fragment>
                                               </div>
                                             }
                                          </DialogContent>
                                          <DialogActions>
                                            <Button onClick={this.handleClose} color="primary">
                                              CLOSE
                                            </Button>
                                          </DialogActions>
                                        </Dialog>
                                    </div>
                                   }
                              </div>
                              </div>
                        </ExpansionPanel>
              </div>
              <div className="col l1">
                <button onClick={() => this.clearOne(content.serial)}
                  style={{marginTop:'10px',background:'linear-gradient(to bottom, #00ffff 0%, #cc33ff 100%'}}
                  className="left btn-floating btn-large waves-effect waves-light" waves="light" tooltip="Dismiss">
                  <i className="small material-icons">clear</i>
                </button>
                </div>
                </div>
                </React.Fragment>
            );
            })}
            </div>
            <div className="col l1" />
            </div>
          }

            <div className="row">
            {this.state.notification.length!==0 &&
<div onClick={this.clearAll} className="waves-effect tooltipped" style={{position:'fixed',bottom: 0,left: 0}} data-position="bottom" data-tooltip="Clear All"><i className="red-text large material-icons">delete_forever</i></div>
}
    <div className="div"></div>
</div>
</div>
            </React.Fragment>
        );
      }
      }
}
