import React from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      datafrom:'',
      Action:'',
      role:'',
      description:'',
      date_of_starting:'',
      date_of_complete:'',
      achivements:'',
      verified:false,
      date:0,
      month:0,
      year:0,
      verification_link:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  var date = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  this.setState({
    action:this.props.action,
    username: this.props.username,
    datafrom:'Section',
    verified:false,
    date:date,
    month:month,
    year:year,
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
      <React.Fragment>
        <Grid container spacing={1}>
          <Grid container item xs={12} sm={12} spacing={1}>
            {this.props.data.fielddata.map((content,index)=>(
              <Grid item xs={12} sm={3} key={index}>
                <TextField
                  id="outlined-multiline-static"
                  label={content.placeholder}
                  multiline
                  rows="2"
                  fullWidth
                  name={content.name}
                  value={this.state[content.name]}
                  onChange={e => this.handleD(e, index)}
                  variant="filled"
                />
              </Grid>
            ))}
          </Grid>
        </Grid>
        <Grid container spacing={1}>
           <Hidden xsDown>
             <Grid item sm={9} />
           </Hidden>
          <Grid item xs={12} sm={3}>
            <Button variant="outlined" color="secondary" onClick={this.props.cancel}>CANCEL</Button>&emsp;
            <Button variant="contained" color="primary" type="submit" onClick={this.handleSubmit}>UPLOAD</Button>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}

export default AddProduct;
