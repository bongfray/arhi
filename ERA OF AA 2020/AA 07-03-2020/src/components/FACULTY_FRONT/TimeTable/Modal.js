import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Allot from './Al2'
import Free from './Free'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default class Modal2 extends Component{
constructor(props){
  super(props);
  this.state = {
    rit:'',
    day_order:'',
    modal: false,
    allo1:false,
    loading: true,
    no1:'',

  };
  this.closeModal = this.closeModal.bind(this);
  this.componentDidMount = this.componentDidMount.bind(this);
}
handle1 = (e, index) => {
  const key = e.target.name;
  const value = e.target.value;
  this.setState({ rit: value, index ,day_order: key});

};
componentDidMount(){
  this.collectw();
}

componentDidUpdate =(prevProps)=>{
  if(prevProps !== this.props)
  {
      this.collectw()
  }
}

collectw =() =>
{
	axios.post('/ework/user/fetchAllotments', {
		day_order:this.props.day_order,
    hour:this.props.content.number,
	})
	.then(response =>{
		if(response.data)
		{
				this.setState({
					allo1:true,
          loading: false,
				})
		}
	})
}

    closeModal(e) {
        e.stopPropagation();
        this.props.closeModal()
     }
     render(){
       let allotw = this.state.allo1;
       let content;

           if(allotw)
           {
             content =
             <React.Fragment>
                     <TableDisplay color={this.props.color}
                     cday={this.props.cday}
                     date ={this.props.date}
                     month ={this.props.month}
                     year ={this.props.year}
                     content={this.props.content}
                     slot={this.props.slot}
                     selectValue="allot"
                     day_order={this.props.day_order}
                     username={this.props.username}
                     closeModal={this.props.closeModal}
                     datas_a={this.props.datas_a}
                     />
             </React.Fragment>

           }
           else
           {
             content =
             <React.Fragment>
                   <TableDisplay color={this.props.color}
                   datas_a={this.props.datas_a}
                   cday={this.props.cday}
                   date ={this.props.date}
                   month ={this.props.month}
                   year ={this.props.year}
                   slot={this.props.slot}
                   selectValue="free"
                   content={this.props.content}
                   day_order={this.props.day_order}
                   username={this.props.username} closeModal={this.props.closeModal}
                   />
             </React.Fragment>
          }
     return (
       <React.Fragment>
       <Dialog fullScreen open={this.props.displayModal} onClose={this.props.closeModal} TransitionComponent={Transition}>
           <AppBar>
             <Toolbar>
              <Grid style={{marginTop:'55'}}container spacing={1}>
                <Grid item xs={2} sm={2}>
                   <IconButton edge="start" color="inherit" onClick={this.props.closeModal} aria-label="close">
                     <CloseIcon />
                   </IconButton>
                </Grid>
                <Grid item xs={8} sm={8}>
                  <div className="center" style={{fontSize:'20px'}}>
                   {this.state.allo1 ?
                      <div>Alloted Slot - {this.props.slot}</div>
                      :
                      <div>Free Slot</div>
                   }
                  </div>
                </Grid>
                <Grid item xs={2} sm={2}/>
              </Grid>
             </Toolbar>
           </AppBar>
           <List>
           <Grid container spacing={1} style={{marginTop:'65px'}}>
                <Grid item xs={12} sm={12}>
                   {content}
                </Grid>
           </Grid>
           </List>
         </Dialog>
</React.Fragment>
     );
   }
}



class TableDisplay extends Component {
       constructor() {
         super()
         this.state = {
           allot:'',
             count: 0,
             selected:'',
             problem_conduct:'',
             saved_dayorder: '',
             day_recent:'',
          }
         }

       render() {
                     if (this.props.selectValue === "allot") {
                       return(
                             <Allot
                                 datas_a={this.props.datas_a}
                                 cday={this.props.cday}
                                 slot={this.props.slot}
                                 date ={this.props.date}
                                 month ={this.props.month}
                                 year={this.props.year}
                                 color={this.props.color}
                                 username={this.props.username }
                                 day_order={this.props.day_order}
                                 closeModal={this.props.closeModal}
                                 content={this.props.content}/>
                       );

                     } else if (this.props.selectValue === "free") {
                       return(
                         <Free
                           datas_a={this.props.datas_a}
                           slot={this.props.slot}
                           time={this.props.time}
                           date ={this.props.date}
                           month ={this.props.month}
                           year={this.props.year}
                           content={this.props.content}
                           color={this.props.color}
                           username={this.props.username }
                           day_order={this.props.day_order}
                           closeModal={this.props.closeModal}
                          />
                       );

                     }
                     else{
                       return(
                         <div style={{textAlign:'center',fontSize: '20px',marginBottom: '20px'}}>
                           Choose from above DropDown
                         </div>
                       );
                     }
       }
     }
