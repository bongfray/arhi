import React, { } from 'react'
import TextField from '@material-ui/core/TextField';
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



export default class Free extends React.Component {
  constructor() {
    super()
    this.state = {
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        covered:'',
        freefield:'',
        freeparts:'',
        disabled:false,
        snack_open:false,
        alert_type:'',
        snack_msg:'',
        verification_link:'',

    }
    this.handlecovered = this.handlecovered.bind(this)
    this.handleFreeVal = this.handleFreeVal.bind(this)
    this.handleFreeField = this.handleFreeField.bind(this)
    }

    closeModal=()=>{
      this.props.closeModal();
    }
    colorChange=()=>{
      this.props.color()
    }

        handleFreeField =(e) =>{
          this.setState({
            freefield: e.target.value,
          })

        }

        handleFreeVal =(e) =>{
          this.setState({
            [e.target.name]: e.target.value,
          })

        }


        handlecovered =(e) =>{
          e.preventDefault();
          if(!(this.state.freefield)||!(this.state.covered) || !(this.state.verification_link) || !(this.state.freeparts))
          {
            this.setState({
              snack_open:true,
              snack_msg:'Enter All the Details First',
              alert_type:'warning',
            });
          }
          else
          {
            this.setState({
              snack_open:true,
              snack_msg:'Submitting....',
              alert_type:'info',
              disabled:true
            });

          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var myDate = new Date.today();
          var week =myDate.getWeek();

          axios.post('/ework/user/free', {
            day_order:this.props.day_order,
            hour:this.props.content.number,
            order:'Academic-Administrative Works',
            freefield: this.state.freefield,
            covered: this.state.covered,
            freeparts: this.state.freeparts,
            date:this.props.date,
            month: this.props.month,
            year: this.props.year,
            week:week,
            slot:this.props.slot,
            time:this.props.time,
            verification_link:this.state.verification_link,
          })
            .then(response => {
              this.setState({disabled:false});
              if(response.status===200){
                this.closeModal();
                 if(response.data.already_have){
                   this.setState({
                     snack_open:true,
                     snack_msg:response.data.already_have,
                     alert_type:'info',
                     disabled:false
                   });
                }
                else if(response.data.suc)
                {
                  this.setState({
                    snack_open:true,
                    snack_msg:response.data.suc,
                    alert_type:'info',
                  });
                }
                else if(response.data === 'done')
                {
                  this.setState({
                    snack_open:true,
                    snack_msg:'Submitted !!',
                    alert_type:'success',
                  });
                  this.colorChange();
                }
              }
            }).catch(error => {
              this.setState({
                snack_open:true,
                snack_msg:'Internal Error',
                alert_type:'error',
                disabled:false
              });
            })
            this.setState({
              freefield:'',
              covered:'',
          })
}

        }

  updateAllotV (userObject) {
    this.setState(userObject)
  }
  freeParts=(userObject)=> {
    this.setState(userObject)
  }

render(){
  return(
    <div style={{padding:'10px'}}>

    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    open={this.state.snack_open} autoHideDuration={2000}
    onClose={()=>this.setState({snack_open:false})}>
      <Alert onClose={()=>this.setState({snack_open:false})}
      severity={this.state.alert_type}>
        {this.state.snack_msg}
      </Alert>
    </Snackbar>

    <Typography color="secondary" variant="button" display="block" gutterBottom>
      Plaese Mention on which area you are going to submit the datas :
    </Typography>
    <br />
            <Grid container spacing={1}>
                    <Grid item xs={12} sm={4}>
                        <FormControl style={{width:'100%'}}>
                          <InputLabel id="sel1_type">Select Here</InputLabel>
                            <Select
                              labelId="sel1_type"
                              id="sel1_type"
                              value={this.state.freefield}
                              onChange={this.handleFreeField}
                            >
                            <MenuItem value="Academic">Academic</MenuItem>
                            <MenuItem value="Research">Research Work</MenuItem>
                            <MenuItem value="Administrative">Administrative Work</MenuItem>
                            </Select>
                         </FormControl>
                     </Grid>
                    <Grid item xs={12} sm={4}>
                       <FreeS freefield={this.state.freefield} freeParts={this.freeParts} />
                    </Grid>
              </Grid>
              <br/>
              <Grid container spacing={1}>
                 <Grid item xs={12} sm={6}>
                   <TextField
                     id="outlined-multiline-static"
                     label="Put a short brief of your work"
                     multiline
                     rows="3"
                     fullWidth
                     name="covered"
                     value={this.state.covered}
                     onChange={this.handleFreeVal}
                     variant="filled"
                   />
                 </Grid>
                 <Grid item xs={12} sm={6}>
                   <TextField
                     id="outlined-multiline-static1"
                     label="Put a Google Drive Link(Should be of SRM IST Mail Drive)"
                     multiline
                     rows="3"
                     fullWidth
                     name="verification_link"
                     value={this.state.verification_link}
                     onChange={this.handleFreeVal}
                     variant="filled"
                   />
                 </Grid>

              </Grid>

              <br /><br />
       <Button disabled={this.state.disabled} style={{float:'right'}} variant="contained" color="primary"
        onClick={this.handlecovered}>SUBMIT</Button>

    </div>
  );
}
}


class FreeS extends React.Component {
  constructor()
  {
    super()
    this.state ={
      administrativefreefield:'',
      hfreefield:'',
      researchfreefield:'',
      root_names:[],
    }

  }


componentDidUpdate =(prevProps,prevState) => {
  if (prevProps.freefield !== this.props.freefield) {
    this.getRoles();
  }
}

getRoles =()=>{
  axios.post('/ework/user/root_percentage_fetch',{action:this.props.freefield})
  .then(res=>{
    if(this.props.freefield ==="Academic")
    {
        const reques = res.data.filter(item => item.responsibilty_title !== "Curriculum");
        this.setState({root_names:reques})
    }
    else
    {
        this.setState({root_names:res.data})
    }

  })
}

handleHFreeField =(e) =>{
  this.setState({
    hfreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}

render()
{
  if((this.props.freefield ==="Academic") || (this.props.freefield ==="Administrative") || (this.props.freefield ==="Research"))
  {
    return(
      <React.Fragment >
          <FormControl style={{width:'100%'}}>
             <InputLabel id="free_type">Please Select the Type</InputLabel>
             <Select
               labelId="free_type"
               id="free_type"
               value={this.state.hfreefield}
               onChange={this.handleHFreeField}
             >
             {this.state.root_names.map((content,index)=>{
                     return(
                                <MenuItem value={content.responsibilty_title} key={index}>{content.responsibilty_title}</MenuItem>
                     )
               })}
             </Select>
           </FormControl>
      </React.Fragment>
    );
  }
  else{
    return(
      <React.Fragment>
      </React.Fragment>
    )
  }

}
}
