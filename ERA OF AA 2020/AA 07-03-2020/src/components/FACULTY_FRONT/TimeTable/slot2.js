import React, { } from 'react'
import {Redirect } from 'react-router-dom'
import Modal2 from './Modal'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
/*---------------------------------------------------------Code for regular classes time table------------------------------------ */



export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',
      modal: false,
    };

  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  render()
  {
    if (this.state.redirectTo)
    {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else
    {
      return(
              <div className="node">
                  <Paper elevation={3} style={{padding:'5px',backgroundColor:'red',color:'white'}} onClick={ this.selectModal } >
                      <Typography style={{color:'#ffff00'}} align="center" variant="button" display="block" gutterBottom>
                        {this.props.content.time} [{this.props.content.number}]
                      </Typography>
                      <Typography align="center" variant="button" display="block" gutterBottom>
                        <b>{this.props.slots}</b>
                      </Typography>
                  </Paper>
                     <Modal2
                     datas_a={this.props.datas_a}
                     date={this.props.date}
                     month={this.props.month}
                     year={this.props.year}
                     displayModal={this.state.modal}
                     closeModal={this.selectModal}
                     username={this.props.username}
                     day_order={this.props.day_order}
                     color={this.props.color}
                     content={this.props.content}
                     slot={this.props.slots}
                     cday={this.props.cday}
                     />
             </div>
      )
  }
  }
}



/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */
