import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});




export default class Switch extends React.Component {

    constructor ( props ) {
        super( props );

		this.state = {
			isChecked: false,
      history:'',
      absent_reason:'',
      absent_state:false,
      loading:true,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
		}
    this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      axios.post('/ework/user/absentStatus',{date:this.props.date,month:this.props.month,year:this.props.year})
      .then( res => {
        this.setState({loading:false})
          if(res.data === 'ok')
          {
              this.setState({absent_state:true,})
          }
      });
    }
    handleAbsentReason =(e)=>{
      this.setState({absent_reason:e.target.value})
    }
    submitAbsent =(e)=>{
      if(!this.state.absent_reason)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter the reason !!',
          alert_type:'warning',
        });
      }
      else
      {
      axios.post('/ework/user/absent_insert',{absent_reason:this.state.absent_reason})
      .then( res => {
        if(res.data)
        {
          this.setState({absent_state:true})
        }
      });
      }

    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
    }
    render () {
      if(this.state.loading)
      {
        return(
          <Backdrop  open={true}>
             <CircularProgress color="secondary" />
          </Backdrop>
        )
      }
      else
      {
        return(
          <div>
          <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snack_open} autoHideDuration={2000}
          onClose={()=>this.setState({snack_open:false})}>
            <Alert onClose={()=>this.setState({snack_open:false})}
            severity={this.state.alert_type}>
              {this.state.snack_msg}
            </Alert>
          </Snackbar>

          {this.state.absent_state === false ?
          <div style={{marginTop:'50px'}}>
            <TextField
              id="outlined-multiline-static"
              label="Enter the reason of your absent"
              multiline
              rows="4"
              fullWidth
              value={this.state.absent_reason}
              onChange={this.handleAbsentReason}
              variant="filled"
            />
            <br /><br />
          <Button variant="contained" style={{float:'right'}} color="secondary" onClick={this.submitAbsent}>SUBMIT</Button>
          <br /><br />
          <Grid container spacing={1}>
             <span style={{color:'green'}}><b>NOTE : </b></span><span style={{color:'red'}}><b>If you are going to
             submit that you are absent today and have submitted any data today,in that case , all the submitted
              data(only Alloted Slots) will be deleted.
             </b></span>If you want to handover some of your allotted slots to
             other faculty,first submit the reason then only you will see that part.
          </Grid>
          </div>
          :
          <React.Fragment>
            <InputValue all_props={this.props}
             />
            </React.Fragment>
          }
          </div>

        );
      }
    }

}

class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
      day_set:[],
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    var year = parseInt(Date.today().toString("yyyy"));
    if(this.leapYear(year)=== true)
    {
      this.setState({
        day_set: [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
      {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
    ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
      })
    }
    else
    {
      this.setState({
        day_set: [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
      {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
    ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
      })
    }
  }

  leapYear(year)
  {
   return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
  }

  render(){

      return(
        <React.Fragment>
            <Paper elevation={3}>
            <Typography align="center" variant="h5" gutterBottom>
              Hours, you can manage on absent
            </Typography>
              <div style={{padding:'10px'}}>
                 <Grid container spacing={3}>
                      {this.props.all_props.day_seq.map((content,index)=>{
                          return(
                            <React.Fragment key={index}>
                                <RenderValiidAbsent day_set={this.state.day_set}
                                 content={content} all_props={this.props.all_props}  />
                            </React.Fragment>
                          )
                        })}
                </Grid>
              </div>
           </Paper>
        </React.Fragment>
      );
  }
}


class RenderValiidAbsent extends Component{
  constructor()
  {
    super();
    this.state= {
      alreadyhave:'',
      modal:false,
      datas_a:'',
      active:0,
      paper_color:'',
      compday:'',
      comphour:'',
      compday_order:'',
      compense_faculty:'',
      hour_set:[{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},
      {value:7},{value:8},{value:9},{value:10}],
      snack_open:false,
      alert_type:'',
      snack_msg:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.collect();
  }


  collect()
  {
    axios.post('/ework/user/fetchAllotments',{day_order: this.props.all_props.day_order,
      hour:this.props.content.number,
    }
  )
    .then(response =>
      {
      if(response.data)
      {
          let allotdata = (response.data.alloted_slots).toUpperCase();
          this.setState({
            datas_a:response.data,
            alreadyhave: allotdata,
          })
      }
      else
      {
        this.setState({
          alreadyhave: 'Free Slot',
        })
      }
    })
  }


  handleComData =(evt)=>{
    const value = evt.target.value;
    this.setState({
      [evt.target.name]: value
    });
  }

handleSubmitData=()=>{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();
  let datas_to_send;
  if(this.state.compense_faculty)
  {
      if(!this.state.compense_faculty)
      {
        this.setState({
          snack_open:true,
          snack_msg:'Enter All the Details First',
          alert_type:'warning',
        });
      }
      else
      {
        var data_set={
          username: this.props.all_props.username,
          date:this.props.all_props.date,
          month: this.props.all_props.month,
          year: this.props.all_props.year,
          week:week,
          day_order: this.props.all_props.day_order,
          content:this.props.content,
          datas_a:this.state.datas_a,
          slot:this.state.alreadyhave,
          time:this.props.content.time,
        }
        this.checkWithFaculty(this.state,data_set,this.props)
      }
  }
  else
  {
    if((!this.state.compday) || (!this.state.comphour) || (!this.state.compday_order))
    {
      this.setState({
        snack_open:true,
        snack_msg:'Enter All the Details First',
        alert_type:'warning',
      });
    }
    else
    {
       datas_to_send={
         username: this.props.all_props.username,
         freefield:"Academic",
         freeparts:'',
         date:this.props.all_props.date,
         month: this.props.all_props.month,
         year: this.props.all_props.year,
         week:week,
         day_order: this.props.all_props.day_order,
         hour:this.props.content.number,
         for_year:this.state.datas_a.year,
         for_batch: this.state.datas_a.batch,
         for_sem: this.state.datas_a.sem,
         slot:this.state.alreadyhave,
         time:this.props.content.time,
         selected:'Absent',
         problem_statement:'Absent For the Whole Day [Defined By System]',
         problem_compensation_day_order:this.state.compday_order,
         problem_compensation_date:this.state.compday,
         problem_compensation_hour:this.state.comphour,
      }
      this.sendAllot(datas_to_send);
    }
  }
}


checkWithFaculty=(state,incoming_data,all_props)=>{
  let datas_to_send;
  axios.post('/ework/user/check_possibility',{id:state.compense_faculty
    ,week:incoming_data.week,props_content:incoming_data})
  .then( res => {
    if(res.data === 'no')
    {
      this.setState({
        snack_open:true,
        snack_msg:'Invalid Request !!',
        alert_type:'error',
      });
        this.setState(this.initialState);
      return false;
    }
    else
    {
      this.setState({
        snack_open:true,
        snack_msg:'Submitting....',
        alert_type:'info',
      });
      datas_to_send={
        username: incoming_data.username,
        freefield:"Academic",
        freeparts:'',
        date:incoming_data.date,
        month: incoming_data.month,
        year: incoming_data.year,
        week:incoming_data.week,
        day_order: incoming_data.day_order,
        hour:incoming_data.content.number,
        for_year:incoming_data.datas_a.year,
        for_batch: incoming_data.datas_a.batch,
        for_sem: incoming_data.datas_a.sem,
        selected:'Problem',
        problem_statement:'Absent For the Whole Day [ Defined By System ]',
        slot:incoming_data.slot,
        time:incoming_data.time,
        compense_faculty:state.compense_faculty,
        compense_faculty_topic:'',
     }
     this.sendAllot(datas_to_send);
    }
  });
}

sendAllot=(datas)=>
{
        axios.post('/ework/user/timeallot',datas,
        this.setState({
          sending: 'disabled',
        })
      )
          .then(response => {
            this.setState({sending:''})
            if(response.status===200){
               if(response.data==='done')
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Success !!',
                  alert_type:'success',
                });
                this.renderSlot();
              }
              else if(response.data === 'no')
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Already Submitted !!',
                  alert_type:'info',
                });
                this.renderSlot();
              }
              else if(response.data === 'spam')
              {
                this.setState({
                  snack_open:true,
                  snack_msg:'Invalid Request !!',
                  alert_type:'error',
                });
              }
            }
          })
}


  renderSlot=()=>{
    this.setState({modal:!this.state.modal})
  }

  handleSet=(e)=>{
    this.setState({
      active: e,
    })
  }

  color =(position) =>{
    if (this.state.active === position) {
        return '#69F0AE';
      }
      return '#e0e0e0';
  }


  render(){

    let actual_days;
    if(this.props.day_set.length>0)
    {
        var month = parseInt(Date.today().toString("MM"));
        var today = parseInt(Date.today().toString("dd"));
        var days=[];
        const day_data= this.props.day_set.filter(item => item.value===month);
        for(var j=1;j<=day_data[0].day;j++)
        {
          days.push(j);
        }
        actual_days= days.filter(item1=>item1>today)
    }
    let time = parseFloat(new Date().toString("HH.mm"));

      return(
        <React.Fragment>
            {!(this.state.alreadyhave==='Free Slot') &&
             <Grid item xs={4} sm={2} onClick={this.renderSlot}>
              <Paper elevation={3} style={{padding:'10px'}}>
                Slot : {this.state.alreadyhave}
              </Paper>
            </Grid>
            }

            {this.state.modal &&

              <Dialog fullScreen open={this.state.modal} onClose={this.renderSlot} TransitionComponent={Transition}>
                  <AppBar>
                    <Toolbar>
                     <Grid style={{marginTop:'55'}}container spacing={1}>
                       <Grid item xs={2} sm={2}>
                          <IconButton edge="start" color="inherit" onClick={this.renderSlot} aria-label="close">
                            <CloseIcon />
                          </IconButton>
                       </Grid>
                       <Grid item xs={8} sm={8}>
                         <div className="center" style={{fontSize:'20px'}}>Slot : {this.state.alreadyhave}</div>
                       </Grid>
                       <Grid item xs={2} sm={2}/>
                     </Grid>
                    </Toolbar>
                  </AppBar>
                  <List>

                  <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                  open={this.state.snack_open} autoHideDuration={2000}
                  onClose={()=>this.setState({snack_open:false})}>
                    <Alert onClose={()=>this.setState({snack_open:false})}
                    severity={this.state.alert_type}>
                      {this.state.snack_msg}
                    </Alert>
                  </Snackbar>

                      <Grid style={{marginTop:'70px'}} container spacing={1}>
                           <Grid item sm={4} xs={1} />
                            <Grid item xs={10} sm={4}>
                                <Grid container spacing={1}>
                                    <Grid item sm={6} xs={6}>
                                      <Paper  onClick={(e)=>{this.handleSet(0)}} style={{height:'40px',textAlign:'center',backgroundColor:this.color(0)}} >HandOver to Other Faculty</Paper>
                                    </Grid>
                                    <Grid item sm={6} xs={6}>
                                      <Paper onClick={(e)=>{this.handleSet(1)}} style={{height:'40px',textAlign:'center',backgroundColor:this.color(1)}} >Want to Compensate On Other Day?</Paper>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item sm={4} xs={1} />
                      </Grid>
                      <br />
                      <Grid container spacing={1}>
                        <Grid item xs={1} sm={3}/>
                        <Grid item xs={10} sm={6}>
                         <Paper >
                            {(this.state.active===0) ?
                              <div style={{padding:'15px'}}>
                                {(!((parseFloat(this.props.content.end_time))<time)) ?
                                  <React.Fragment>
                                  <div className="input-field" >
                                    <input type="text" id="faculty_id_c" value={this.state.compense_faculty} name="compense_faculty"
                                     onChange={this.handleComData} />
                                     <label htmlFor="faculty_id_c">Faculty Id</label>
                                  </div>
                                  <br />
                                  <Grid container spacing={1}>
                                      <Grid item xs={8} sm={9}/>
                                      <Grid item xs={4} sm={3}>
                                        <Button onClick={this.handleSubmitData} variant="contained"
                                        color="secondary">Submit</Button>
                                      </Grid>
                                  </Grid>
                                  </React.Fragment>
                                  :
                                  <div style={{color:'red',textAlign:'center'}}>
                                     Slot can only be exchanged to other faculty within that hour !!
                                  </div>
                                }
                              </div>
                              :
                              <div style={{padding:'15px'}}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={4} sm={4}>
                                             <FormControl style={{width:'100%'}}>
                                                <InputLabel id="day_order">Day Order</InputLabel>
                                                <Select
                                                  labelId="day_order"
                                                  id="day_order"
                                                  name="compday_order"
                                                  value={this.state.compday_order}
                                                  onChange={this.handleComData}
                                                >
                                                   <MenuItem value="1">1</MenuItem>
                                                   <MenuItem value="2">2</MenuItem>
                                                   <MenuItem value="3">3</MenuItem>
                                                   <MenuItem value="4">4</MenuItem>
                                                   <MenuItem value="5">5</MenuItem>
                                                </Select>
                                              </FormControl>
                                        </Grid>
                                        <Grid item xs={4} sm={4}>
                                             <FormControl style={{width:'100%'}}>
                                                <InputLabel id="month">Day</InputLabel>
                                                <Select
                                                  labelId="month"
                                                  id="month"
                                                  name="compday"
                                                  value={this.state.compday}
                                                  onChange={this.handleComData}
                                                >
                                                {actual_days.map((content,index)=>{
                                                        return(
                                                          <MenuItem key={index}  value={content}>{content}</MenuItem>
                                                        )
                                                  })}
                                                </Select>
                                              </FormControl>
                                        </Grid>
                                         <Grid item xs={4} sm={4}>
                                               <FormControl style={{width:'100%'}}>
                                                  <InputLabel id="hour">Hour</InputLabel>
                                                  <Select
                                                    labelId="hour"
                                                    id="hour"
                                                    name="comphour"
                                                    value={this.state.comphour}
                                                    onChange={this.handleComData}
                                                  >
                                                  {this.state.hour_set.map((content,index)=>{
                                                          return(
                                                            <MenuItem key={index}  value={content.value}>{content.value}</MenuItem>
                                                          )
                                                    })}
                                                  </Select>
                                                </FormControl>
                                         </Grid>
                                    </Grid>
                                    <br />
                                    <Grid container spacing={1}>
                                        <Grid item xs={9} sm={9}/>
                                        <Grid item xs={3} sm={3}>
                                          <Button onClick={this.handleSubmitData} variant="contained"
                                           color="secondary">Submit</Button>
                                        </Grid>
                                    </Grid>
                              </div>
                            }
                      </Paper>
                      </Grid>
                      <Grid item xs={1} sm={3} />
                    </Grid>
                  </List>
                </Dialog>
            }
        </React.Fragment>
      );
  }
}
