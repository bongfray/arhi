import React from 'react';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CircularProgress from '@material-ui/core/CircularProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';



export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      fetching:true,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user/profile1del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/ework/user/fetchprofile1',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>
    {
      this.setState({
        fetching:false,
       products: response.data,

     })
     if(this.props.status)
     {
       if(response.data.length>0)
       {
         this.props.status();
       }
     }
  })
}
  render() {
    const { products} = this.state;
      return(
        <React.Fragment>

        {this.state.fetching ?
          <div style={{position: 'relative',marginLeft: '50%',marginTop:'5px'}}>
            <CircularProgress color="secondary" />
          </div>
          :
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Degree</TableCell>
                  <TableCell align="center">College Name</TableCell>
                  <TableCell align="center">Year of Starting</TableCell>
                  <TableCell align="center">Year of Completion</TableCell>
                  <TableCell align="center">Marks</TableCell>
                  <TableCell align="center">Verification Link</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map((row,index)=> (
                  <TableRow key={index}>
                    <TableCell color="secondary">
                         {this.props.title}
                    </TableCell>
                    {this.props.data.fielddata.map((content,ind)=>(
                      <TableCell align="center" key={ind}>
                        {content.link ?
                          <a target="_blank" rel="noopener noreferrer" style={{color:'red'}} href={row[content.name]}>Verification Link</a>
                          :
                          <Typography>{row[content.name]}</Typography>
                        }

                      </TableCell>
                    ))}
                    <TableCell align="center">
                      {row.verified ?
                        <Typography style={{color:'#388e3c'}}>Verified</Typography>
                        :
                        <div style={{textAlign:'center'}}>
                          <IconButton aria-label="edit" color="primary" onClick={() => this.props.editProduct(row.serial,this.props.action)}>
                            <EditIcon fontSize="inherit" />
                          </IconButton>
                          <IconButton aria-label="edit" color="secondary" onClick={() => this.deleteProduct(row.serial,this.props.action)}>
                            <DeleteIcon fontSize="inherit" />
                          </IconButton>
                        </div>
                      }
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        }
        </React.Fragment>

      )

  }
}
