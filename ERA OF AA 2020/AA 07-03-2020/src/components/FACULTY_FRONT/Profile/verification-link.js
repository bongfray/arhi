import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './add';
import ProductList from './prod';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';


 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       degrees:'',
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:true,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  axios.get('/ework/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else
     {
       this.setState({
         redirectTo:'/ework/faculty',
       });
     }
   })
}

   onCreate = (e,index) => {
     this.setState({isAddProduct:true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user/editprofile1';
     } else {
       apiUrl = '/ework/user/profile1';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId)=> {
     axios.post('/ework/user/fetchtoedit',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  handleDegress =(e) =>{
    this.setState({degrees:e.target.value})
  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

 upStat =()=>{
   this.setState({disabled:false})
 }
   render() {
             var data = {
               fielddata: [
                 {
                   name: "verificationlinks",
                   placeholder: "Put a Google Drive Link(Should be of SRM IST Mail Drive)",
                   type: "text",
                   xs:8,
                 },
               ],
             };

  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <Grid container spacing={1}>
       <Hidden xsDown>
       <Grid item sm={3} />
       </Hidden>
       <Grid item xs={12} sm={7}>
         <Paper elevation="3" style={{padding:'10px'}}>
               <Typography variant="h6" align="center" color="secondary" gutterBottom>
                 Give the links
               </Typography>
               <FormControl style={{width:'100%'}}>
                 <InputLabel id="sel_degree">Choose your option</InputLabel>
                   <Select
                     labelId="sel_degree"
                     id="sel_degree"
                     value={this.state.degrees}
                     onChange={this.handleDegress}
                   >
                   <MenuItem value="" disabled defaultValue>Choose your option</MenuItem>
                   <MenuItem value="For Degree">For Degree</MenuItem>
                   <MenuItem value="For Records">For Records</MenuItem>
                   <MenuItem value="For Administrative-Research Work">For Administrative-Research Work</MenuItem>
                   <MenuItem value="For Five Year">For Five Year</MenuItem>
                   </Select>
                </FormControl>
          </Paper>
      </Grid>

      <Hidden xsDown>
      <Grid item sm={3} />
      </Hidden>
  </Grid>
  <br /><br />
     {this.state.degrees &&
       <Paper variant="outlined" square style={{padding:'5px 10px 50px 10px'}}>
          <Grid container spacing={1}>
            <Grid item xs={1} sm={1} ><b>Type</b></Grid>
            <Grid item xs={1} sm={1} style={{textAlign:'center'}} ><b>Index</b></Grid>
            <Grid item xs={8} sm={8} style={{textAlign:'center',wordWrap: 'break-word'}}><b>Link</b></Grid>
            <Grid item xs={2} sm={2} style={{textAlign:'center',wordWrap: 'break-word'}}><b>Actions</b></Grid>
          </Grid>
      <Divider />


                {!this.state.isAddProduct &&
                  <Grid container spacing={1}>
                    <ProductList
                    status={this.upStat} username={this.state.username}
                    title={this.state.degrees} action={this.state.degrees}
                    data={data}  editProduct={this.editProduct}/>
                  </Grid>
                }
                {!this.state.isAddProduct &&
                 <React.Fragment>
                   <Button variant="contained" style={{float:'right'}} color="primary"
                    name={this.state.degrees} onClick={(e) => this.onCreate(e,data)}>
                      Add Degree
                    </Button>
                </React.Fragment>
                }
                {(this.state.isAddProduct || this.state.isEditProduct) &&
                   <Grid container spacing={1}>
                     <AddProduct cancel={this.updateState} username={this.state.username} action={this.state.degrees} data={data}
                      onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                   </Grid>
                }

          </Paper>
        }
</React.Fragment>
);
}
}
}
