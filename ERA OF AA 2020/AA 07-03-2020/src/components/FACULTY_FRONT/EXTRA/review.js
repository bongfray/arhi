import React, { Component } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';


export default class Review extends Component {
  constructor()
  {
    super()
    this.state={
      review_data:'',
      loader:true,
      isExpanded:true,
      expanded:0,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchIncomings();
  }
  fetchIncomings=()=>{
    axios.post('/ework/user/fetch_skill_for_fac',{action:'Review-Action'})
    .then( res => {
        if(res.data)
        {
          //console.log(res.data)
          this.setState({review_data:res.data,loader:false})
        }
    });
  }
  render() {
    if(this.state.loader)
    {
      return(
        <div>Loading...</div>
      )
    }
    else {
      return (
        <div>
          {this.state.review_data.map((content,index)=>{
            return(
              <Grid container spacing={1} key={index}>
                 <Grid item xs={1} sm={1}/>
                 <Grid item xs={10} sm={10}>
                     <ExpansionPanel expanded={this.state.isExpanded && this.state.expanded===index}
                     style={{borderRadius:'20px'}} onChange={()=>this.setState({expanded:index})}>
                       <ExpansionPanelSummary
                       style={{borderRadius:'20px'}}
                         expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                         aria-controls="panel4bh-content"
                         id="panel4bh-header"
                         className="mine-collap"
                       >
                         <div style={{color:'yellow',fontSize:'25px'}}>Refer Task No - T{content.task_id}</div>
                        </ExpansionPanelSummary>
                         <TaskH fetchIncomings={this.fetchIncomings} data={this.state.review_data}/>
                        </ExpansionPanel>
                 </Grid>
                 <Grid item xs={1} sm={1}/>
              </Grid>
            )
          })}
        </div>
      );
    }
  }
}


class TaskH extends Component {
  constructor() {
    super()
    this.state={
      fetching:true,
      action:'',
      reason:'',
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTaskHis();
  }
  componentDidUpdate=(prevProps)=>{
     if((prevProps.data.task_id !== this.props.data.task_id) ||
     (prevProps.data.from !== this.props.data.from) ||
     (prevProps.data.skill_name !== this.props.data.skill_name))
     {
       this.fetchTasks();
     }
  }

  fetchTaskHis=()=>{
    axios.post('/ework/user2/fetch_a_task',{data:this.props.data})
    .then( res => {
        if(res.data === 'no')
        {
          window.M.toast({html: 'Not Found !!', classes:'rounded red'});
        }
        else {
          this.setState({task_histoty:res.data})
        }
        this.setState({fetching:false})
    });
  }

  verify=(e,action)=>{
    this.setState({open_reason:true,action:action})
  }

  verifyIt=()=>{
    window.M.toast({html: 'Updating...', classes:'rounded orange'});
    axios.post('/ework/user2/validate_a_submitted_task',{data:this.state.task_histoty,
    props_data:this.props.data,action:this.state.action,reason:this.state.reason})
    .then( res => {
        if(res.data === 'no')
        {
          window.M.toast({html: 'Failed To Update !!', classes:'rounded red'});
        }
        else {
          this.closeCase();
        }
    });
  }

  closeCase=()=>{
    axios.post('/ework/user/mark_as_done_review',{data:this.state.task_histoty,
    props_data:this.props.data})
    .then( res => {
      this.setState({open_reason:false})
        if(res.data === 'no')
        {
          window.M.toast({html: 'Failed To Update !!', classes:'rounded red'});
        }
        else {
          this.props.fetchIncomings();
          window.M.toast({html: 'Updated !!', classes:'rounded green darken-1'});
        }
    });
  }

  render()
  {
    if(this.state.fetching)
    {
      return(
            <div>Loading...</div>
      )
    }
    else {
      return(
        <React.Fragment>

        <Dialog
          open={this.state.open_reason}
          keepMounted
          onClose={()=>this.setState({open_reason:false})}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title" style={{textAlign:'center'}}>Information</DialogTitle>
          <DialogContent>
                      {!(this.state.action) ?
                         <TextField
                           id="outlined-multiline-static"
                           label="Mention the Reason"
                           multiline
                           rows="4"
                           fullWidth
                           name="reason"
                           value={this.state.reason}
                           onChange={(e)=>this.setState({reason:e.target.value})}
                           variant="filled"
                         />
                         :
                         <div>Are you sure you want to verify this task ?</div>
                       }
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>this.setState({open_reason:false})} color="secondary">
              CLOSE
            </Button>
            <Button
            onClick={this.verifyIt} color="primary">
              {this.state.action ? <React.Fragment>AGREE</React.Fragment>:<React.Fragment>SEND</React.Fragment>}
            </Button>
          </DialogActions>
        </Dialog>

        <div style={{padding:'15px'}}>
          <Grid container spacing={1}>
            <Grid item xs={1} sm={1}><b>Task-No</b></Grid>
            <Grid item xs={4} sm={4}><b>Description</b></Grid>
            <Grid item xs={2} sm={2}><b>Links</b></Grid>
            <Grid item xs={3} sm={3}><b>Duration</b></Grid>
            <Grid item xs={2} sm={2}><b>Action</b></Grid>
          </Grid>
          <hr />

              <React.Fragment>
               <Grid container spacing={1}>
                 <Grid item xs={1} sm={1}>T{this.state.task_histoty.referTask}</Grid>
                 <Grid item xs={4} sm={4}
                   style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>
                   {this.state.task_histoty.description}
                 </Grid>
                 <Grid item xs={2} sm={2}
                 style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>
                   {this.state.task_histoty.link}
                 </Grid>
                 <Grid item xs={3} sm={3}
                 style={{whiteSpace: 'pre-line',wordWrap: 'break-word'}}>
                   {this.state.task_histoty.start_date}-{this.state.task_histoty.end_date}
                  </Grid>
                  <Grid item xs={2} sm={2}>
                  {!(this.props.prop_data) &&
                      <React.Fragment>
                       {this.state.task_histoty.verification_status === 0 ?
                         <Grid container spacing={1}>
                            <Grid item xs={6} sm={6}>
                               <Button variant="contained" color="primary" onClick={(e)=>this.verify(e,true)}>
                                  Veify
                                </Button>
                            </Grid>
                             <Grid item xs={6} sm={6}>
                               <Button variant="contained" color="secondary" onClick={(e)=>this.verify(e,false)}>
                                 Deny
                               </Button>
                             </Grid>
                         </Grid>
                         :
                         <div style={{textAlign:'center',color:'green'}}>
                           Verified
                         </div>
                       }
                      </React.Fragment>
                   }
                  </Grid>
               </Grid>
              </React.Fragment>
          </div>
        </React.Fragment>

      )
    }

  }
}
