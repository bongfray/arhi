import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios'

import Review from '../../EXTRA/review'
import Validate from '../validateUser'
import HandleActivation from './SUPER_OPERATIONS/handleActivation'
import SignUpReq from './SUPER_OPERATIONS/handleSignup'
import ViewApprove from './SUPER_OPERATIONS/viewRequests'
import BluePrintHandle from './SUPER_OPERATIONS/handleRerender_bluePrint'


export default class SuperUser extends Component{
    constructor(){
        super();
        this.state={
          showButton:'block',
          show:'none',
          display:'none',
          redirectTo:'',
          modal: false,
          isChecked:false,
          isCheckedS:false,
          history:'',
            susername:'',
            username:'',
            index_id:'',
            request:[],
            responded_request:[],
            viewdata:'',
            option:'',
            req_reject_no:'',
            req_accept_no:'',
            selectedOption:'',
            id:'',
            renderDiv:false,
            switch_value:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }


    getRequest =() =>{
      axios.get('/ework/user/getrequest')
          .then(response => {
            const reques = response.data.filter(item => item.action === "false");
            const reject = response.data.filter(item => item.action === "denied");
            const approve = response.data.filter(item => item.action === "approved");
            this.setState({display:'block',request: reques,responded_request:response.data,req_reject_no:reject.length,req_accept_no:approve.length})
          })
    }
    view_Status_of_Request = (content,index) =>{
      this.setState({
        username:content.username,
        index_id:index,
      })
      axios.post('/ework/user/approve_request',content)
          .then(response => {
            if(response.data)
            {
              this.setState({viewdata: response.data})
              this.modalrender();
            }
          })
    }
    modalrender=()=>{
      this.setState({renderDiv:!this.state.renderDiv})
    }
    openModal()
    {
        this.setState({modal: !this.state.modal})
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }
    componentDidMount()
    {
      this.openModal();
      this.getRequest();
    }


    handleOption = (e) =>{
      this.setState({option: e.target.value})
    }


    handleRequestModify =(object) =>{
      this.setState(object)
    }



    render(){
      var viewbutton;
        if(this.props.select === 'register')
        {
            viewbutton =
              <React.Fragment>
                <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                 <HandleActivation />
                </React.Fragment>
        }
        else if(this.props.select === 'signup')
        {
            viewbutton =
                    <React.Fragment>
                      <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                        <div style={{display:this.state.show}}>
                        <FormControl style={{width:'100%'}}>
                           <InputLabel id="option">Select Here</InputLabel>
                           <Select
                             labelId="option"
                             id="option"
                             value={this.state.option} onChange={this.handleOption}
                           >
                           <MenuItem value="faculty">Faculty</MenuItem>
                           <MenuItem value="student">Student</MenuItem>
                           </Select>
                         </FormControl>
                        </div>
                       <SignUpReq choice={this.state.option}/>
                    </React.Fragment>
        }
        else if(this.props.select === 'blue_print')
        {
          viewbutton =
                  <React.Fragment>
                    <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                      <div style={{display:this.state.show}}>
                      <FormControl style={{width:'100%'}}>
                         <InputLabel id="option">Select Here</InputLabel>
                         <Select
                           labelId="option"
                           id="option"
                           value={this.state.option} onChange={this.handleOption}
                         >
                         <MenuItem value="faculty">Faculty</MenuItem>
                         <MenuItem value="student">Student</MenuItem>
                         </Select>
                       </FormControl>
                      </div>
                     <BluePrintHandle choice={this.state.option}/>
                  </React.Fragment>
        }
        else if(this.props.select === 'review_projects')
        {
          viewbutton =
                  <React.Fragment>
                    <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                    <Review />
                  </React.Fragment>
        }
        else if(this.props.select === "approve_single_faculty_req")
        {
          viewbutton =
          <React.Fragment>
          <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
          {this.state.display === 'none' ? <h5 className="center">Fetching Requests....</h5> :
          <div style={{margin:'15px 8px 1px 9px',display:this.state.show}}>
          <div className="row">
          <div className="card col l4">
            <div className="head center yellow">Total No of Request Received </div>
            <div className="card-content">{this.state.responded_request.length}</div>
          </div>
          <div className="card col l4">
            <div className="head center yellow">Total No of Request Approved</div>
            <div className="card-content">{this.state.req_accept_no}</div>
          </div>
          <div className="card col l4">
            <div className="head center yellow">Total No of Request Rejected</div>
            <div className="card-content">{this.state.req_reject_no}</div>
          </div>
          </div>
          <div className="row">
            <div className="col l1 center"><b>Serial No.</b></div>
            <div className="col l1 center"><b>Day Order</b></div>
            <div className="col l2 center"><b>Official ID</b></div>
            <div className="col l1 center"><b>Requested Hour</b></div>
            <div className="col l2 center"><b>Requested Date(dd/mm/yyyy)</b></div>
            <div className="col l3 center"><b>Reason</b></div>
            <div className="col l2 center"><b>Action</b></div>
          </div><hr />
          {this.state.request.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 center">{index+1}</div>
            <div className=" col l1 center">{content.day_order}</div>
            <div className=" col l2 center">{content.username}</div>
            <div className=" col l1 center">{content.req_hour ? <div>{content.req_hour}</div>: <div>NULL</div>}</div>
            <div className=" col l2 center">{content.req_date}/{content.req_month}/{content.req_year}</div>
            <div className=" col l3 center">{content.req_reason}</div>
            </div>
            <button className="btn col l2 blue-grey darken-2 sup" onClick={() => this.view_Status_of_Request(content,content.serial)}>View Status</button>
            </div><hr />
            </React.Fragment>
          ))}
          <ViewApprove showDiv={this.showDiv} handleRequestModify={this.handleRequestModify}
           showButton={this.state.showButton} request={this.state.request}
           modalrender={this.modalrender} renderDiv={this.state.renderDiv} view_details={this.state.viewdata}
           username={this.state.username} index_id={this.state.index_id}/>
          </div>}
          </React.Fragment>
        }
        else{
          viewbutton =
          <div></div>
        }
        return(
            <div className="center">
              {viewbutton}
              <SuspendUser select={this.props.select}/>
            </div>
        )
    }
}


class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'',
            modal:false,
            disabled:'',
            allowed:false,
            suspend_faculty_username:'',
            suspend_student_username:'',
            suspend_reason_faculty:'',
            suspend_reason_student:'',
            deniedlist:[],
            current_action:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount()
    {
      this.fetch_denied_list()
    }
    componentDidUpdate =(prevProps,prevState) => {
      if (prevProps.action !== this.props.action) {
        this.fetch(this.props.action);
      }
      if (prevState.allowed !== this.state.allowed) {
        if(this.state.current_action === "remove_sespension")
        {
          this.RemoveSuspension();
        }
        else
        {
          this.Suspend();
        }
      }
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }

    fetch_denied_list =() =>{
      axios.get('/ework/user/fetch_denied_list')
      .then(res=>{
        this.setState({deniedlist: res.data})
      })
    }
    handleApprove = (id,username) =>{
      axios.post('/ework/user/approverequest',{
        serial: id,
        username:username,
      }).then(res=>{
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== id),
        })
      })
    }


    handleUsername = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    Suspend = (e)=>{
      this.setState({modal: !this.state.modal,disabled:'disabled',current_action:'suspend_user'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:''})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/ework/user/suspension_user"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/ework/user2/suspension_suser"
        }
        this.handleSuspension(route);
      }
    }
    RemoveSuspension = (e)=>{
      this.setState({modal: !this.state.modal,disabled:'disabled',current_action:'remove_sespension'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:''})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/ework/user/remove_user_suspension"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/ework/user2/remove_suser_suspension"
        }
        this.handle_Remove_Suspension(route);
      }
    }
    handleSuspension = (route) =>{
      axios.post(route,this.state)
      .then(res=>{
          window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }

    handle_Remove_Suspension = (route) =>{
      axios.post(route,this.state)
      .then(res=>{
          window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }
    render(){
      let suspend_content;
      if(this.props.select === 'suspendfac'){
            suspend_content=
                <div className="center">
                   <div className="row">
                      <div className="input-field col s12 xl6 l6 m6">
        								<input id="username" type="text" name="suspend_faculty_username" className="validate" value={this.state.suspend_faculty_username} onChange={this.handleUsername} required />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Official Id</span>
        								<label htmlFor="username">Username</label>
								      </div>
                      <div className="input-field col s12 xl6 l6 m6">
        								<input id="username" type="text" name="suspend_reason_faculty" className="validate" value={this.state.suspend_reason_faculty} onChange={this.handleUsername}/>
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Ignore if it's Removal of Suspension</span>
        								<label htmlFor="username">Reason</label>
								      </div>
                      </div>
                      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Link>
								      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5 offset-l2 offset-m2 offset-xl2 offset-s2" disabled={this.state.disabled} onClick={this.Suspend}>Suspend Faculty</Link>
                </div>
        }
        else if(this.props.select === 'suspendstu'){
            suspend_content =
                <div className="center">
                   <div className="row">
                      <div className="input-field col s12 l6 xl6 m6">
        								<input id="username" type="text" className="validate" name="suspend_student_username" value={this.state.suspend_student_username} onChange={this.handleUsername} required />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Student Registation Number Only</span>
        								<label htmlFor="username">Username</label>
								      </div>
                      <div className="input-field col s12 l6 xl6 m6">
                        <input id="username" type="text" className="validate" name="suspend_reason_student" value={this.state.suspend_reason_student} onChange={this.handleUsername} />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Ignore if it's Removal of Suspension</span>
                        <label htmlFor="username">Reason</label>
                      </div>
                      </div>
                      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Link>
								      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5 offset-l2 offset-m2 offset-xl2 offset-s2" disabled={this.state.disabled} onClick={this.Suspend}>Suspend Student</Link>
                </div>
        }
        else if(this.props.select === 'suspend_request'){
            suspend_content =
                <div style={{marginTop:'17px',marginRight:'10px'}}>
                <div className="row">
                  <div className="col l1 center"><b>Serial No.</b></div>
                  <div className="col l1 center"><b>Day Order</b></div>
                  <div className="col l2 center"><b>Official ID</b></div>
                  <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
                  <div className="col l3 center"><b>Reason</b></div>
                  <div className="col l2 center"><b>Action</b></div>
                </div><hr />
                {this.state.deniedlist.map((content,index)=>(
                  <React.Fragment key={index}>
                  <div className="row">
                  <div>
                  <div className="col l1 center"><b>{index+1}</b></div>
                  <div className="col l1 center"><b>{content.day_order}</b></div>
                  <div className="col l2 center"><b>{content.username}</b></div>
                  <div className="col l3 center"><b>{content.day}/{content.month}/{content.year}</b></div>
                  <div className="col l3 center"><b>{content.req_reason}</b></div>
                  </div>
                  <button className="btn col l2 " onClick={() => this.handleApprove(content.serial,content.username)}>Approve</button>
                  </div>
                  </React.Fragment>
                ))}
                </div>
        }
        else{
            suspend_content=
                <div></div>
        }
        return(
          <React.Fragment>
          <div style={{display:this.state.success}}>
            <Validate displayModal={this.state.modal} showDiv={this.showDiv}/>
          </div>
          <div style={{}}>
            {suspend_content}
          </div>
          </React.Fragment>
        );
    }
}
