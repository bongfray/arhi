import React, { Component } from 'react';
import axios from 'axios'

export default class Activation extends Component {
  constructor(){
      super();
      this.state={
        isChecked:false,
        isCheckedS:false,
        faculty_blueprint:false,
        student_blueprint:false,
        history:'',
      }
      this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.fetchFaculty_Registration_Status();
    this.fetchStudent_Registration_Status();
  }

  fetchFaculty_Registration_Status()
  {
    axios.post('/ework/user/fac_status')
        .then(response => {
          if(response.data)
          {
            var fac_reg_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="SIGNUP STATUS")));
            var fac_blueprint_status = response.data.filter(item => ((item.usertype==="fac") && (item.order==="BLUE PRINT STATUS")));

            if((fac_reg_status.length)>0)
            {
              this.setState({isChecked: fac_reg_status[0].status})
            }
            if((fac_blueprint_status.length)>0)
            {
                this.setState({faculty_blueprint: fac_blueprint_status[0].status,})
            }
          }
        })
  }
  fetchStudent_Registration_Status()
  {
    axios.post('/ework/user2/stud_status')
        .then(response => {
          if(response.data)
          {
            var stud_reg_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="SIGNUP STATUS")));
            var stud_blueprint_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="BLUE PRINT STATUS")));

            if((stud_reg_status.length)>0)
            {
              this.setState({isCheckedS: stud_reg_status[0].status})
            }
            if((stud_blueprint_status.length)>0)
            {
                this.setState({student_blueprint: stud_blueprint_status[0].status,})
            }
          }
        })
  }


      handleComp= (e)=>{
        this.setState({
          isChecked: !this.state.isChecked,
          history: e.target.value,
        })
        axios.post('/ework/user/handleFaculty_Registration',{
          checked: !this.state.isChecked,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

      handleCompS=(e)=>{
        this.setState({
          isCheckedS: !this.state.isCheckedS,
          history: e.target.value,
        })
        axios.post('/ework/user2/handleStudent_Registration',{
          checked: !this.state.isCheckedS,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

      FBluePrint=(e)=>{
        this.setState({
          faculty_blueprint: !this.state.faculty_blueprint,
          history: e.target.value,
        })

        axios.post('/ework/user/handleFaculty_BluePrint',{
          checked: !this.state.faculty_blueprint,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

      SBluePrint=(e)=>{
        this.setState({
          student_blueprint: !this.state.student_blueprint,
          history: e.target.value,
        })

        axios.post('/ework/user2/handleStudent_BluePrint',{
          checked: !this.state.student_blueprint,
          history: e.target.value,
        })
        .then(res=>{

        })
      }

  render() {
    return (
      <React.Fragment>
            <div className="row form-signup" style={{padding:'15px',display:this.state.show}}>
            <div className=" col s12 l12 m12" >

            <div className="row">
            <div className="col l8 s8 m8">
               Faculty Registration
            </div>
            <div className="col l4 s4 m4">
            <div className="switch">
              <label>
                Off
                <input  checked={this.state.isChecked} value="fac" onChange={this.handleComp} type="checkbox" />
                <span className="lever"></span>
                On
              </label>
            </div>
            </div>
            </div>

            <div className="row">
              <div className="col l8">
                 Student Registration
              </div>
              <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.isCheckedS} value="stud" onChange={this.handleCompS} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
              </div>
            </div>


            <div className="row">
              <div className="col l8">
                 Render Faculty Blue Print
              </div>
              <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.faculty_blueprint} value="fac" onChange={this.FBluePrint} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col l8">
                 Render Student Blue Print
              </div>
              <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.student_blueprint} value="stud" onChange={this.SBluePrint} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
              </div>
            </div>

            </div>
            </div>
      </React.Fragment>
    );
  }
}
