import React, { Component} from 'react'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';

import NavAdd from '../CRUD_In_Input/NavHandle'
import AddSection from '../CRUD_In_Input/handleSection'
import Res from '../CRUD_In_Input/handleRes'
import InsertDesignation from '../CRUD_In_Input/insertDesignation'
import InsertDept from '../CRUD_In_Input/handleDept'



export default class InsertUser extends Component{
    constructor(){
        super();
        this.initialState = {
            username: 'D',
            mailid: '',
            campus:'',
            dept:'',
            h_order:'0.5',
            password:'',
            deptToken:'',
            suspension_status:true,
            resetPasswordToken:'',
            resetPasswordExpires:'',
            count:3,
            active:false,
            department:'',
        }
        this.state = this.initialState;
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount()
    {
        axios.post('/ework/user/fetch_designation')
        .then(res => {
            if(res.data)
            {
                      const department = res.data.filter(item =>
                      item.action ==='Department');
              this.setState({department:department})
            }
        });
    }
    handleField = (e) =>{
        this.setState({
          [e.target.name] : e.target.value,
        })
    }
    InsertDeptAdmin =(event)=>
    {
          event.preventDefault()
          if(!(this.state.username) || !(this.state.dept) || !(this.state.campus) || !(this.state.mailid))
          {
            window.M.toast({html: 'Fill all the Details Please!!',classes:'rounded red lighten-1'});
          }
          else
          {
          window.M.toast({html: 'Submitting...... !!',classes:'rounded green lighten-1'});
          axios.post('/ework/user/insert_dept_admin',{data:this.state})
          .then( res => {
              if(res.data === 'ok')
              {
                window.M.toast({html: 'Successfully Inserted  !!',classes:'rounded green lighten-1'});
              }
              else if(res.data === 'no')
              {
                window.M.toast({html: 'User Already There !!',classes:'rounded pink lighten-1'});
              }
              this.setState(this.initialState)
          });
        }
    }
    render(){
        if(this.props.select==='insertadmin')
        {
          if(this.state.dept_fetching)
          {
            return(
              <div style={{float:'center'}}>
                 <CircularProgress color="secondary" />
              </div>
            )
          }
          else{
            return(
              <Paper elevation={3} style={{padding:'17px'}}>
                    <Grid container spacing={1}>
                        <Grid item xs={3} sm={3}>
                            <TextField
                              type="text"
                              id="Enter Offical Id"
                              label="Enter Offical Id"
                              multiline
                              fullWidth
                              name="username" value={this.state.username} onChange={this.handleField}
                              variant="filled"
                            />
        								</Grid>
        								<Grid item xs={3} sm={3}>
                            <TextField
                              type="email"
                              id="Enter Offical Mail Id"
                              label="Enter Offical Mail Id"
                              multiline
                              fullWidth
                              name="mailid" value={this.state.mailid} onChange={this.handleField}
                              variant="filled"
                            />
        								</Grid>
                        <Grid item xs={3} sm={3}>
                            <FormControl style={{width:'100%'}}>
                               <InputLabel id="campus">Campus</InputLabel>
                               <Select
                                 labelId="campus"
                                 id="campus"
                                 value={this.state.campus}
                                 name="campus" onChange={this.handleField}
                               >
                                 <MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
                                 <MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
                                 <MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
                                 <MenuItem value="NCR Campus">NCR Campus</MenuItem>
                               </Select>
                             </FormControl>
                        </Grid>
                        <Grid item xs={3} sm={3}>
                        <FormControl style={{width:'100%'}}>
                          <InputLabel id="dept">Department</InputLabel>
                            <Select
                              labelId="dept"
                              id="dept"
                              value={this.state.dept}
                              name="dept" onChange={this.handleField}
                            >
                                  {this.state.department.map((content,index)=>{
                                    return(
                                      <MenuItem key={index} value={content.department_name}>{content.department_name}</MenuItem>
                                    )
                              })}
                              </Select>
                          </FormControl>
                        </Grid>
                    </Grid>
                    <br /><br />
                    <Grid container spacing={1}>
                       <Button
                       variant="contained" color="secondary"
                       style={{float:'right',padding:'10px'}}
                       onClick={this.InsertDeptAdmin}>Insert Department Admin</Button>
                    </Grid>
                </Paper>
                )
            }
        }
        else if(this.props.select==='insert_in_nav')
        {
            return(
                  <NavAdd />
                )
        }
        else if(this.props.select==='responsibilty_percentages')
        {
            return(
                  <Res />
                )
        }
        else if(this.props.select==='section_part_insert')
        {
            return(
                  <AddSection />
                )
        }
        else if(this.props.select === 'insert_designation')
        {
          return(
            <InsertDesignation />
          )
        }
        else if(this.props.select === 'insert_department')
        {
          return(
            <InsertDept/>
          )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}
