import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import axios from 'axios';

export default class Action extends Component {
  constructor(){
    super()
    this.state={
      username:'',
      task:'',
    }
  }

  handleField=(e)=>{
    this.setState({[e.target.name]:e.target.value})
  }
putSuprimity=()=>{
  if((!this.state.username)||(!this.state.task))
  {
    window.M.toast({html: 'Enter all the Details !!', classes:'rounded red darken-2'});
  }
  else{
    window.M.toast({html: 'Sending the Mail !!', classes:'rounded green lighten-1'});
    axios.post('/ework/user/assign_suprimity',{data:this.state})
    .then( res => {
        if(res.data==='ok'){
          window.M.toast({html: 'Successfully Inserted  !!', classes:'rounded green lighten-1'});
        }
        else if(res.data==='no')
        {
          window.M.toast({html: 'No User Found !!', classes:'rounded red darken-2'});
        }
        else if(res.data==='have')
        {
          window.M.toast({html: 'Already Assigned !!', classes:'rounded red darken-2'});
        }
    });
  }
}
  render() {
    return (
      <Paper elevation={2} style={{padding:'15px'}}>
              <Grid container spacing={1}>
                  <Grid item xs={6} sm={6}>
                      <div className="input-field">
                      <input id="username" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleField} required />
                      <label htmlFor="username">Enter Offical Id</label>
                      </div>
                   </Grid>
                  <Grid item xs={6} sm={6}>
                        <FormControl style={{width:'100%'}}>
                          <InputLabel id="type">Select Level</InputLabel>
                            <Select
                              labelId="type"
                              id="type"
                              value={this.state.task} name="task" onChange={this.handleField}
                            >
                            <MenuItem value="add-skill">Add Skill</MenuItem>
                            <MenuItem value="add-domain">Add Domain</MenuItem>
                            <MenuItem value="achieve-domain">Add Path For Domain</MenuItem>
                            </Select>
                         </FormControl>
                  </Grid>
              </Grid>
             <Grid container spacing={1}>
               <Button variant="contained" color="secondary" style={{float:'right'}}
                onClick={this.putSuprimity}>Assign</Button>
            </Grid>
      </Paper>
    );
  }
}
