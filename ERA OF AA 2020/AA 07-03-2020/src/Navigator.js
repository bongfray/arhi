import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export default class Nstart extends Component {
  render() {
    return (
      <Grid container spacing={1}>
         <Grid item xs={2} sm={3}/>
         <Grid item xs={8} sm={6}>
           <Paper elevation="3" style={{padding:'10px',borderRadius:'10px',marginTop:'170px'}}>
            <div>
              <Typography variant="h4" align="center" gutterBottom>
                eWork
              </Typography><br />
              <Grid container spacing={1} style={{marginBottom:'35px'}}>
                <Grid item xs={5} sm={5}>
                  <Link to="/ework/flogin">
                      <Paper style={{color:'white',padding:'10px',textAlign:'center',background:'linear-gradient(to bottom left, #ff0066 0%, #00ff99 100%)'}}>
                        Faculty
                      </Paper>
                  </Link>

                </Grid>
                <Grid item xs={2} sm={2}/>
                <Grid item xs={5} sm={5}>
                    <Link to="/ework/slogin">
                      <Paper style={{color:'black',padding:'10px',textAlign:'center',background: 'linear-gradient(to bottom left, #66ffff 0%, #ffff99 100%)'}}>
                        Student
                      </Paper>
                    </Link>
                </Grid>
              </Grid>
            </div>
          </Paper>
         </Grid>
         <Grid item xs={2} sm={3}/>
      </Grid>
    );

  }
}
