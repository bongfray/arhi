import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import Collap from './Gg'

export default class Section extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      option:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}

componentDidMount(){
  M.AutoInit()
}


  render()
  {
    return(
      <React.Fragment>
      <div className="row">
      <div className="col l3" />
        <div className="col l6">
            <select name="title" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled selected>Title</option>
            <option value="thesis_projects">Thesis & Projects Supervision</option>
            <option value="training_workshop">Training & Workshop Attended or Held</option>
            <option value="paper_published_accepted">Paper Published & Accepted</option>
            </select>
        </div>
        <div className="col l3" />
        </div>
          <Collap options={this.state.option} />
      </React.Fragment>

    )
  }
}
