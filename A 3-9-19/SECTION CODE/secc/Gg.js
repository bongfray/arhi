import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import AddProduct from './add';
import ProductList from './prod'

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{
  console.log(this.state.action)
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }



handleMe =() =>{
  axios.post('/user/reset')
  .then(response => {
    if(response.data.succ)
    {
        window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded green lighten-1'});
    }
    else if(response.data.notallowed)
    {
        window.M.toast({html: response.data.notallowed,outDuration:'9000', classes:'rounded  red lighten-1'});
    }
  })

}
   onFormSubmit(data) {
     let apiUrl;

     if(this.state.isEditProduct){
       apiUrl = '/user/editt';
     } else {
       apiUrl = '/user/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetcheditdata',{
       id: productId
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm,title,description;
     var data;
     if(this.props.options === "thesis_projects")
     {
             title ='Thesis & Project Supervision';
             description ='Please provide information about graduate thesis/project supervised';
             data = {
               Action:'thesis',
               fielddata: [
                 {
                   header: "Title of Completed Thesis/Project",
                   name: "title_thesis_project",
                   placeholder: "Enter the Title of Thesis / Project",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Description of Thesis/Project",
                   name: "description_of_thesis_project_completed",
                   placeholder: "Description of the Thesis / Project",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Credits",
                   name: "credits_for_thesis_project",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter Details about the Credits"
                 },
                 {
                   header: "Date of Completion",
                   name: "date_of_thesis_project_completion",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter the Date of Completion"
                 },

               ],
             };

        if(this.state.isAddProduct || this.state.isEditProduct) {

          productForm = <AddProduct action={this.props.options} data={data} onFormSubmit={this.onFormSubmit} id={this.state.id} product={this.state.product} />
        }
}
else if(this.props.options === "training_workshop")
{
  title ='Training/Workshop Attended';
  description ='Please Provide the Details about the training workshop seminar attended participated';
  data = {
    Action:'training',
    fielddata: [
      {
        header: "Date",
        name: "date_of_training_held",
        placeholder: "Enter the Date of helding",
        type: "text",
        grid: 2
      },
      {
        header: "Details About Workshop or Seminar",
        name: "details_workshop_seminar",
        placeholder: "Details About Workshop or Seminar",
        type: "text",
        grid: 2
      },
      {
        header: "Location",
        name: "location_of_workshop_seminar",
        type: "text",
        grid: 2,
        placeholder: "Location of Workshop/Seminer Held"
      },
      {
        header: "Duration of the Event",
        name: "duration_of_workshop_seminar",
        type: "text",
        grid: 2,
        placeholder: "Enter the Duration of the Event"
      },
      {
        header: "Your Role",
        name: "role_workshop_seminar",
        type: "text",
        grid: 2,
        placeholder: "Your Role in Workshop or Seminer"
      },

    ],
  };

if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct action={this.props.options} data={data} onFormSubmit={this.onFormSubmit} id={this.state.id} product={this.state.product} />
}
}
else{
  return(
  productForm = <div></div>
)
}


return (
  <div className="App">
  <Collapsible popout>
    <CollapsibleItem
      header={
            <h5 className="collaphead">{title}</h5>
      }expanded>
  <div>
    {!this.state.isAddProduct && <ProductList title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 left">
  <span>Kindly Reset after Deleting all the Datas  </span><a className="go" onClick={this.handleMe}>RESET</a>
  </div>
     <div className="col l6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Row</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>
  </CollapsibleItem>
</Collapsible>
  </div>
);
}
}
