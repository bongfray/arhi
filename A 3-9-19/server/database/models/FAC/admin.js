const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


// var connection = mongoose.createConnection("mongodb://localhost:27017/eWork");
//
// autoIncrement.initialize(connection);


const adminSchema = new Schema({
title_thesis_project: { type: String, unique: false, required: false },
description_of_thesis_project_completed: { type: String, unique: false, required: false },
credits_for_thesis_project: { type: String, unique: false, required: false },
date_of_thesis_project_completion: { type: String, unique: false, required: false },
})

adminSchema.plugin(autoIncrement.plugin, { model: 'Admin', field: 'serial', startAt: 1,incrementBy: 1 });



var counter = mongoose.model('Admin', adminSchema);




module.exports = counter
