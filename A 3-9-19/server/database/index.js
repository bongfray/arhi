const mongoose = require('mongoose')
mongoose.Promise = global.Promise
var autoIncrement = require('mongoose-auto-increment');
const uri = 'mongodb://localhost:27017/eWork'

mongoose.connect(uri,{ useNewUrlParser: true }).then(
    () => {

        console.log('Connected to Mongo');

    },
    err => {

         console.log('error connecting to Mongo: ')
         console.log(err);

        }
  );
  var connection = mongoose.createConnection("mongodb://localhost:27017/eWork",{ useNewUrlParser: true});

  autoIncrement.initialize(connection);

module.exports = mongoose.connection
