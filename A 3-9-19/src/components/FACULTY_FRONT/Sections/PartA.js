import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import newId from './id'
import Par from "./tab";
import '../style.css'
import Nav from '../dynnav'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'

class PartA extends Component {
  constructor(){
    super()
    this.componentDidMount();
  }
  state = {
    width: "600px",
    height: "30px",
    home:'/faculty',
    logout:'/user/logout',
    get:'/user/',
    content:[
      {
        val:'Profile',
        link:'/fprofile',
      },
      {
        val:'DashBoard',
        link:'/dash',
      },
      {
        val:'Sections',
        link:'/partA',
      },
      {
        val:'TimeTable',
        link:'/time_new',
      },
      {
        val:'Master TimeTable',
        link:'/master',
      }
    ],
  };

  componentDidMount() {
    M.AutoInit();
    this.id = newId();
    let collapsible = document.querySelectorAll(".collapsible");
    M.Collapsible.init(collapsible, {});
  }

  render() {
    var data1 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Course Code/Course Info",
          inputfield: true,
          length: 20,
          name: "code",
          placeholder: "",
          content: "text",
          grid: "six"
        },
        {
          header: "No. of Students",
          inputfield: true,
          length: 10,
          name: "totalstud",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data2 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Course Code/Course Info",
          inputfield: true,
          length: 20,
          name: "code1",
          placeholder: "",
          content: "text",
          grid: "six"
        },
        {
          header: "No. of Students",
          inputfield: true,
          length: 10,
          name: "totalstud1",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data3 = {
      fielddata: [
        {
          id: "",
          header: "No.",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          id: "",
          header: "Title of Completed Thesis/Project",
          inputfield: true,
          length: 10,
          name: "toctp",
          placeholder: "",
          content: "text",
          grid: "four"
        },
        {
          id: "",
          header: "Credits",
          inputfield: true,
          length: 10,
          name: "credits",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          id: "",
          header: "Date of Completion",
          inputfield: true,
          length: 7,
          name: "dateoc",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data4 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "one"
        },
        {
          header: "Date",
          inputfield: true,
          length: 10,
          name: "trdate",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Workshop/Seminar",
          inputfield: true,
          length: 10,
          name: "wksem",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Location",
          inputfield: true,
          length: 7,
          name: "loca",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Duration",
          inputfield: true,
          length: 10,
          name: "duration",
          placeholder: "",
          content: "text",
          grid: "one"
        },
        {
          header: "Your Role",
          inputfield: true,
          length: 7,
          name: "role",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data5 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "one"
        },
        {
          header: "Date",
          inputfield: true,
          length: 10,
          name: "pdate",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Paper Title",
          inputfield: true,
          length: 10,
          name: "ptitle",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Name of Publication",
          inputfield: true,
          length: 7,
          name: "nopub",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Volume No",
          inputfield: true,
          length: 7,
          name: "volno",
          content: "text",
          grid: "one",
          placeholder: ""
        },
        {
          header: "Impact Factor",
          inputfield: true,
          length: 10,
          name: "impfac",
          placeholder: "",
          content: "text",
          grid: "two"
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };

    var data6 = {
          fielddata: [
            {
              header: "No. ",
              inputfield: true,
              length: 10,
              name: "date_created",
              placeholder: "",
              content: "text",
              grid : "two",
            },
            {
              header: "Date",
              inputfield: true,
              length: 10,
              name: "date",
              placeholder: "",
              content: "text",
              grid : "two",
            },
            {
              header: "Conference Title",
              inputfield: true,
              length: 10,
              name: "conft",
              content: "text",
              grid : "two",
              placeholder: ""
            },
            {
              header: "Location",
              inputfield: true,
              length: 7,
              name: "loc",
              content: "text",
              grid : "two",
              placeholder: ""
            },
            {
              header: "Mention Your Role",
              inputfield: true,
              length: 7,
              name: "role",
              content: "text",
              grid : "two",
              placeholder: ""
            }
          ],
          instances: [
            {
            date_created: "",
            id: ''
          }
          ]
        };
    var data7 = {

          fielddata: [

            {
              id: '',
              header: "No.",
              inputfield: true,
              length: 10,
              name: "date_created",
              placeholder: "",
              content: "text",
              grid : "two",
            },
            {
              id: '',
              header: "Research Area",
              inputfield: true,
              length: 10,
              name: "rarea",
              placeholder: "",
              content: "text",
              grid : "two",
            },
            {
              id: '',
              header: "Project/Research Title",
              inputfield: true,
              length: 10,
              name: "p/rtitle",
              content: "text",
              grid : "two",
              placeholder: ""
            },
            {
              id: '',
              header: "Description",
              inputfield: true,
              length: 7,
              name: "descrip",
              content: "text",
              grid : "two",
              placeholder: ""
            },
            {
              id: '',
              header: "Mention Your Role",
              inputfield: true,
              length: 7,
              name: "role",
              content: "text",
              grid : "two",
              placeholder: ""
            },
          ],
          instances: [
            {
            date_created: '',
            id: ''
          }
          ]
        };
        var data8 = {
              fielddata: [
                {
                  header: "No. ",
                  inputfield: true,
                  length: 10,
                  name: "date_created",
                  placeholder: "",
                  content: "text",
                  grid : "two",
                },
                {
                  header: "Date",
                  inputfield: true,
                  length: 10,
                  name: "dedate",
                  placeholder: "",
                  content: "text",
                  grid : "two",
                },
                {
                  header: "Activity Details",
                  inputfield: true,
                  length: 10,
                  name: "dactivity",
                  content: "text",
                  grid : "three",
                  placeholder: ""
                },
                {
                  header: "Mention your role",
                  inputfield: true,
                  length: 7,
                  name: "drole",
                  content: "text",
                  grid : "three",
                  placeholder: ""
                }
              ],
              instances: [
                {
                date_created: "",
                id: ''
              }
              ]
            };
            var data9 = {
                  fielddata: [
                    {
                      header: "No. ",
                      inputfield: true,
                      length: 10,
                      name: "date_created",
                      placeholder: "",
                      content: "text",
                      grid : "two",
                    },
                    {
                      header: "Date",
                      inputfield: true,
                      length: 10,
                      name: "depcommitydate",
                      placeholder: "",
                      content: "text",
                      grid : "two",
                    },
                    {
                      header: "Commity Details",
                      inputfield: true,
                      length: 10,
                      name: "conft",
                      content: "text",
                      grid : "two",
                      placeholder: ""
                    },
                    {
                      header: "Mention your Position and Role",
                      inputfield: true,
                      length: 7,
                      name: "loc",
                      content: "text",
                      grid : "two",
                      placeholder: ""
                    },
                    {
                      header: "Number of the Metting Attended",
                      inputfield: true,
                      length: 7,
                      name: "role",
                      content: "text",
                      grid : "two",
                      placeholder: ""
                    }
                  ],
                  instances: [
                    {
                    date_created: "",
                    id: ''
                  }
                  ]
                };
                var data10 = {
                      fielddata: [
                        {
                          header: "No. ",
                          inputfield: true,
                          length: 10,
                          name: "date_created",
                          placeholder: "",
                          content: "text",
                          grid : "two",
                        },
                        {
                          header: "Date",
                          inputfield: true,
                          length: 10,
                          name: "guidingdate",
                          placeholder: "",
                          content: "text",
                          grid : "two",
                        },
                        {
                          header: "Details of the Student(Academic only like sem,year,reg no)",
                          inputfield: true,
                          length: 10,
                          name: "conft",
                          content: "text",
                          grid : "two",
                          placeholder: ""
                        },
                        {
                          header: "Avg no of advising Hours per sem",
                          inputfield: true,
                          length: 7,
                          name: "loc",
                          content: "text",
                          grid : "two",
                          placeholder: ""
                        },
                        {
                          header: "Description of Advising Activity",
                          inputfield: true,
                          length: 7,
                          name: "role",
                          content: "text",
                          grid : "two",
                          placeholder: ""
                        }
                      ],
                      instances: [
                        {
                        date_created: "",
                        id: ''
                      }
                      ]
                    };
                    var data11 = {
                          fielddata: [
                            {
                              header: "No. ",
                              inputfield: true,
                              length: 10,
                              name: "date_created",
                              placeholder: "",
                              content: "text",
                              grid : "two",
                            },
                            {
                              header: "Date",
                              inputfield: true,
                              length: 10,
                              name: "date",
                              placeholder: "",
                              content: "text",
                              grid : "two",
                            },
                            {
                              header: "Conference Title",
                              inputfield: true,
                              length: 10,
                              name: "conft",
                              content: "text",
                              grid : "two",
                              placeholder: ""
                            },
                            {
                              header: "Location",
                              inputfield: true,
                              length: 7,
                              name: "loc",
                              content: "text",
                              grid : "two",
                              placeholder: ""
                            },
                            {
                              header: "Mention Your Role",
                              inputfield: true,
                              length: 7,
                              name: "role",
                              content: "text",
                              grid : "two",
                              placeholder: ""
                            }
                          ],
                          instances: [
                            {
                            date_created: "",
                            id: ''
                          }
                          ]
                        };

    const loggedIn = this.props.loggedIn;
      return (
        <React.Fragment>
        <Nav home={this.state.home} content={this.state.content} get={this.state.get} logout={this.state.logout}/>
        <div className="grid-of-parts">
          <Collapsible popout>
            <CollapsibleItem
              header={
                    <h5 className="collaphead">Thesis & Project Supervision</h5>
              }expanded>
            <div className="row">
              <div className="col s1">
                <h5 className="right td ft">A .</h5>
              </div>
              <div className="col s11 td">
                <h5 className="ft">
                  Please provide information about graduate thesis/project
                  supervised
                </h5>
              </div>
            </div>
            <br />
            <br />
            <div className="row">
              <Par data={data3} />
            </div>
            </CollapsibleItem>
          </Collapsible>

          {/*----------------------------------------------------------------------------------------------------Thesis / Project*/}

          <Collapsible popout>
            <CollapsibleItem
              header={
                    <h5 className="collaphead">
                      Training/Workshop Attended
                    </h5>
              }>
              <div className="row">
                <div className="col s1">
                  <h5 className="right td ft">A .</h5>
                </div>
                <div className="col s11 td">
                  <h5 className="ft">
                    Please Provide the Details about the
                    training/workshop/seminar attended/participated
                  </h5>
                </div>
              </div>
              <br />
              <br />
              <div className="row">
                <Par data={data4} />
              </div>
            </CollapsibleItem>
          </Collapsible>

          {/*----------------------------------------------------------------------------------------------------------Training/Workshop/Seminar*/}

          <Collapsible popout>
            <CollapsibleItem
              header={
                    <h5 className="collaphead">
                    Paper Published/ Accepted
                    </h5>
              }>
              <div className="row">
                <div className="col s1">
                  <h5 className="right td ft">A .</h5>
                </div>
                <div className="col s11 td">
                  <h5 className="ft">
                    Please Provide information about published and accepted paper in journal/conference/book chapter/newspaper article
                  </h5>
                </div>
              </div>
              <br />
              <br />
              <div className="row">
                <Par data={data5} />
              </div>
            </CollapsibleItem>
          </Collapsible>

          {/*-------------------------------------------------------------------------------------------------Paper, Journal etc*/}

          <Collapsible popout>
                <CollapsibleItem header={
                    <h5 className="collaphead">Professional Development Activities</h5>
                  }>
   <div className="row">
      <div className="col s1"><h5 className="right td ft">A .</h5>
      </div>
   <div className="col s11 td">
   <h5 className="ft">Please Provide info about the International /National Conference Attended / Participated </h5>
   </div>
 </div>
<br />
<br />
 <div className="row">
   <Par data={data6} />
 </div>
</CollapsibleItem>
</Collapsible>

{/*--------------------------------------------------------------------------------------------End of Profrssional Development Activities */}

<Collapsible popout>
<CollapsibleItem header={
      <h5 className="collaphead">Research and Projects Works</h5>
    }>
    <div className="row">
    <div className="col s1"><h5 className="right td ft">A .</h5>
    </div>
    <div className="col s11 td">
    <h5 className="ft">Please Provide your Research and Project Details </h5>
    </div>
    </div>
<br />
<br />
<div className="row">
<Par data={data7} />
</div>
</CollapsibleItem>
</Collapsible>

{/*----------------------------------------------------------------------------------------------------End of Research and Project works*/}


<Collapsible popout>
  <CollapsibleItem header={
      <h5 className="collaphead">Department Activities</h5>
    }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Please Provide the Details about your Department Activities(including events Like Seminer, Workshop etc) </h5>
</div>
</div>
<br />
<br />
<div className="row">
<Par data={data8} />
</div>
</CollapsibleItem>
</Collapsible>

{/*----------------------------------------------------------------------------------------------------------End of Department Activities*/}

<Collapsible popout>
  <CollapsibleItem header={
      <h5 className="collaphead">Department Committes Membership</h5>
    }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Please Provide about your Participation in Dept. Committes</h5>
</div>
</div>
<br />
<br />
<div className="row">
<Par data={data9} />
</div>
</CollapsibleItem>
</Collapsible>

{/*-------------------------------------------------------------------------------------------------End of Department Commities meetings*/}


<Collapsible popout>
  <CollapsibleItem header={
      <h5 className="collaphead">Advising and Counselling Details</h5>
    }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Mention your Participation in Advising and Counselling Students(for example guiding the Students in their academic and so on)</h5>
</div>
</div>
<br />
<br />
<div className="row">
<Par data={data10} />
</div>
</CollapsibleItem>
</Collapsible>

{/* -----------------------------------------------------------------------------------------------End of Advising & Counselling Details*/}


<Collapsible popout>
  <CollapsibleItem header={
      <h5 className="collaphead">Professional Development Activities</h5>
    }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Please Provide info about the International /National Conference Attended / Participated </h5>
</div>
</div>
<br />
<br />
<div className="row">
<Par data={data11} />
</div>
</CollapsibleItem>
</Collapsible>


        </div>
        </React.Fragment>
      );
  }
}

export default PartA;
