import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: []
    }
  }

  componentDidMount() {
    axios.get('/user/fetchall')
        .then(response => {
          this.setState({
           products: response.data,
         })
        })
  }


  deleteProduct = (productId) => {
    console.log("DEL")
  const { products } = this.state;



  axios.post('/user/del',{
    serial: productId
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}

  render() {
    const { error, products} = this.state;


      return(
        <div>
        <div className="row">
          <div className="col s1">
            <h5 className="right td ft">A .</h5>
          </div>
          <div className="col s11 td">
            <h5 className="ft">
              Please provide information about graduate thesis/project
              supervised
            </h5>
          </div>
        </div>
        <br />
          <table>
            <thead className="col s12">
            <tr>
            <th className="col s2">Serial No</th>
            {this.props.data.fielddata.map((content,index)=>(
              <th className="col s2 center" key={index}>{content.header}</th>
            ))}
            <th className="col s2 center"> Action</th>
            </tr>
            </thead>
            <tbody>
              {products.map((product,index) => (
                <tr key={product.serial}>
                  <td className="center">{index}</td>
                  <td className="center">{product.title_thesis_project}</td>
                  <td className="center">{product.description_of_thesis_project_completed}</td>
                  <td className="center">{product.credits_for_thesis_project}</td>
                  <td className="center">{product.date_of_thesis_project_completion}</td>
                    <td className="center"><button className="btn-small btnalign green" onClick={() => this.props.editProduct(product.serial)}>Edit</button>
                    &nbsp;<button className="btn-small btnalign red" onClick={() => this.deleteProduct(product.serial)}>Delete</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )

  }
}
