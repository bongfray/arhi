import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import ProductList from './prod';
import AddProduct from './add';



 class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       modalon: false,
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false
     }
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }

   onCreate = () => {

     this.setState({ isAddProduct: true ,product: {}});
   }



handleMe =() =>{
  axios.post('/user/reset')
  .then(response => {
    if(response.data.succ)
    {
        window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded green lighten-1'});
    }
    else if(response.data.notallowed)
    {
        window.M.toast({html: response.data.notallowed,outDuration:'9000', classes:'rounded  red lighten-1'});
    }
  })

}
   onFormSubmit(data) {
     console.log(data)
     let apiUrl;

     if(this.state.isEditProduct){
       apiUrl = '/user/editt';
     } else {
       apiUrl = '/user/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }




   editProduct = productId => {
     axios.post('/user/fetcheditdata',{
       id: productId
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

   render() {
     var data1 = {
       fielddata: [
         {
           header: "Title of Completed Thesis/Project",
           name: "title_thesis_project",
           placeholder: "Enter the Title of Thesis / Project",
           type: "text",
           grid: 2
         },
         {
           header: "Description of Thesis/Project",
           name: "description_of_thesis_project_completed",
           placeholder: "Description of the Thesis / Project",
           type: "text",
           grid: 2
         },
         {
           header: "Credits",
           name: "credits_for_thesis_project",
           type: "text",
           grid: 2,
           placeholder: "Enter Details about the Credits"
         },
         {
           header: "Date of Completion",
           name: "date_of_thesis_project_completion",
           type: "text",
           grid: 2,
           placeholder: "Enter the Date of Completion"
         },

       ],
     };
     let productForm;
if(this.state.isAddProduct || this.state.isEditProduct) {
  productForm = <AddProduct data={data1} modal={this.state.modalon} onFormSubmit={this.onFormSubmit} id={this.state.id} product={this.state.product} />
}
     return (
       <div className="App">
       <Collapsible popout>
         <CollapsibleItem
           header={
                 <h5 className="collaphead">Thesis & Project Supervision</h5>
           }expanded>
       <div>
         {!this.state.isAddProduct && <ProductList data={data1}  editProduct={this.editProduct}/>}<br />
         {!this.state.isAddProduct &&
          <React.Fragment>
          <div className="row">
          <div className="col l6 left">
       <span>Kindly Reset after Deleting all the Datas  </span><a className="go" onClick={this.handleMe}>RESET</a>
       </div>
          <div className="col l6">
            <button className="btn right blue-grey darken-2 sup subm" onClick={() => this.onCreate()}>Add Row</button>
            </div>
         </div>
         </React.Fragment>
       }
         { productForm }
         <br/>
       </div>
       </CollapsibleItem>
     </Collapsible>

       </div>
     );
   }
}
export default Gg;
