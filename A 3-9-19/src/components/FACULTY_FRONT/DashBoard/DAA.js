import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'

var empty = require('is-empty');

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
      total_adminispercentage:null,
      total_academicpercentage:null,
      total_researchpercentage:null,
      ad_total_role_res:null,
      ad_total_clerical :null,
      ad_total_planning :null,
      academic_total_curricular:null,
      academic_total_cocurricular:null,
      academic_total_extracurricular:null,
      academic_total_evaluation_placementwork:null,
      research_total_publication:null,
      research_total_ipr_patents:null,
      research_total_funded_sponsored_projectes:null,
      research_total_tech_dev_consultancy:null,
      research_total_product_development:null,
      research_total_research_center_establish:null,
      research_total_research_guidnce:null,
			logout:'/user/logout',
			get:'/user/',
			content:[
				{
					val:'Profile',
					link:'/fprofile',
				},
				{
					val:'DashBoard',
					link:'/dash',
				},
				{
					val:'Sections',
					link:'/partA',
				},
				{
					val:'TimeTable',
					link:'/time_new',
				},
				{
					val:'Master TimeTable',
					link:'/master',
				}
			]
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

		getPercentage =() =>{
			axios.get('/user/getPercentage', {
			})
			.then(response =>{
				if(response.data){

					this.setState({
            total_adminispercentage:response.data.total_adminispercentage,
            total_academicpercentage:response.data.total_academicpercentage,
            total_researchpercentage:response.data.total_researchpercentage,
            ad_total_role_res:response.data.ad_total_role_res,
            ad_total_clerical:response.data.ad_total_clerical,
            ad_total_planning:response.data.ad_total_planning,
            academic_total_curricular:response.data.academic_total_curricular,
            academic_total_cocurricular:response.data.academic_total_cocurricular,
            academic_total_extracurricular:response.data.academic_total_extracurricular,
            academic_total_evaluation_placementwork:response.data.academic_total_evaluation_placementwork,
            research_total_publication:response.data.research_total_publication,
            research_total_ipr_patents:response.data.research_total_ipr_patents,
            research_total_funded_sponsored_projectes:response.data.research_total_funded_sponsored_projectes,
            research_total_tech_dev_consultancy:response.data.research_total_tech_dev_consultancy,
            research_total_product_development:response.data.research_total_product_development,
            research_total_research_center_establish:response.data.research_total_research_center_establish,
            research_total_research_guidnce:response.data.research_total_research_guidnce,
            ad_complete_role_res:response.data.ad_complete_role_res,
            ad_complete_clerical:response.data.ad_complete_clerical,
            ad_complete_planning:response.data.ad_complete_planning,
            academic_complete_curricular:response.data.academic_complete_curricular,
            academic_complete_cocurricular:response.data.academic_complete_cocurricular,
            academic_complete_extracurricular:response.data.academic_complete_extracurricular,
            academic_complete_evaluation_placementwork:response.data.academic_complete_evaluation_placementwork,
            research_complete_publication:response.data.research_complete_publication,
            research_complete_ipr_patents:response.data.research_complete_ipr_patents,
            research_complete_funded_sponsored_projectes:response.data.research_complete_funded_sponsored_projectes,
            research_complete_tech_dev_consultancy:response.data.research_complete_tech_dev_consultancy,
            research_complete_product_development:response.data.research_complete_product_development,
            research_complete_research_center_establish:response.data.research_complete_research_center_establish,
            research_complete_research_guidnce:response.data.research_complete_research_guidnce,
					});
				}
			})
		}

    fetchlogin = () =>{
      axios.get('/user/fetchdayorder', {
      })
      .then(response =>{
        if(response.data === "Not"){
          this.setState({
            redirectTo:'/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }

    componentDidMount() {
        M.AutoInit();
        this.fetchlogin();
				this.getPercentage();
    }
  render()
  {
    return(
      <React.Fragment>
      <div className="row">
        <div className="col l4">
        <ul className="collapsible">
        <li>
          <div className="center collapsible-header pink" style={{color:'white'}}>Administrative Works</div>
          <div className="collapsible-body">
          <div className="row">
            <div className="col l11 left">Total Percentage Alloted for Administrative Work: </div>
            <div className="col l1">{this.state.total_adminispercentage}</div>
          </div>
          <table>
            <thead>
              <tr>
                  <th>Different Works</th>
                  <th>Completed (H/W)</th>
                  <th>Alloted (H/W)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Roles & Responsibility</td>
                <td className="center">{this.state.ad_complete_role_res}</td>
                <td className="center">{this.state.ad_total_role_res}</td>
              </tr>
              <tr>
                <td>Clerical Work</td>
                <td className="center">{this.state.ad_complete_clerical}</td>
                <td className="center">{this.state.ad_total_clerical}</td>
              </tr>
              <tr>
                <td>Planning & Strategy</td>
                <td className="center">{this.state.ad_complete_planning}</td>
                <td className="center">{this.state.ad_total_planning}</td>
              </tr>
            </tbody>
          </table>
          </div>
          </li>
        </ul>
        </div>

        <div className="col l4">
        <ul className="collapsible">
        <li>
           <div className="center collapsible-header pink" style={{color:'white'}}>Academic Work</div>
           <div className="collapsible-body">
           <div className="row">
             <div className="col l11 left">Total Percentage Alloted for Academic Work: </div>
             <div className="col l1">{this.state.total_academicpercentage}</div>
           </div>

           <table>
             <thead>
               <tr>
                   <th>Different Works</th>
                   <th>Completed (H/W)</th>
                   <th>Alloted (H/W)</th>
               </tr>
             </thead>
             <tbody>
               <tr>
                 <td>Curricular</td>
                 <td className="center">{this.state.academic_complete_curricular}</td>
                 <td className="center">{this.state.academic_total_curricular}</td>
               </tr>
               <tr>
                 <td>Co-Curricular</td>
                 <td className="center">{this.state.academic_complete_cocurricular}</td>
                 <td className="center">{this.state.academic_total_cocurricular}</td>
               </tr>
               <tr>
                 <td>Extra Curricular</td>
                 <td className="center">{this.state.academic_complete_extracurricular}</td>
                 <td className="center">{this.state.academic_total_extracurricular}</td>
               </tr>
               <tr>
                 <td>Preparation & Evaluation & Placement Related Works</td>
                 <td className="center">{this.state.academic_complete_evaluation_placementwork}</td>
                 <td className="center">{this.state.academic_total_evaluation_placementwork}</td>
               </tr>
             </tbody>
           </table>
           </div>
           </li>
           </ul>
        </div>
        <div className="col l4">
        <ul className="collapsible">
        <li>
           <div className="center collapsible-header pink" style={{color:'white'}}>Research Work</div>
           <div className="collapsible-body">
           <div className="row">
             <div className="col l12 left">Total Percentage Alloted for Research Work: <span>{this.state.total_researchpercentage}</span></div>
           </div>
           <table>
             <thead>
               <tr>
                   <th>Different Works</th>
                   <th>Completed (H/W)</th>
                   <th>Alloted</th>
               </tr>
             </thead>
             <tbody>
               <tr>
                 <td>Publications</td>
                 <td className="center">{this.state.research_complete_publication}</td>
                 <td className="center">{this.state.research_total_publication}</td>
               </tr>
               <tr>
                 <td>IPR & Patents</td>
                 <td className="center">{this.state.research_complete_ipr_patents}</td>
                 <td className="center">{this.state.research_total_ipr_patents}</td>
               </tr>
               <tr>
                 <td>Funded Projects or Sponsored Research</td>
                 <td className="center">{this.state.research_complete_funded_sponsored_projectes}</td>
                 <td className="center">{this.state.research_total_funded_sponsored_projectes}</td>
               </tr>
               <tr>
                 <td>Technology Devpmt. or Consultancy</td>
                 <td className="center">{this.state.research_complete_tech_dev_consultancy}</td>
                 <td className="center">{this.state.research_total_tech_dev_consultancy}</td>
               </tr>
               <tr>
                 <td>Product Development</td>
                 <td className="center">{this.state.research_complete_product_development}</td>
                 <td className="center">{this.state.research_total_product_development}</td>
               </tr>
               <tr>
                 <td>Research Center Establishment</td>
                 <td className="center">{this.state.research_complete_research_center_establish}</td>
                 <td className="center">{this.state.research_total_research_center_establish}</td>
               </tr>
               <tr>
                 <td>Research Guidance</td>
                 <td className="center">{this.state.research_complete_research_guidnce}</td>
                 <td className="center">{this.state.research_total_research_guidnce}</td>
               </tr>
             </tbody>
           </table>
           </div>
           </li>
           </ul>
        </div>
      </div>

      </React.Fragment>
    )
  }
}
