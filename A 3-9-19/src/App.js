import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'

import Decider from './components/STUDENT_FRONT/Decider'

import  Test from './components/FACULTY_FRONT/NEWSEC/Gg'

import Initial from './Navigator'
import FSignup from './components/FACULTY_FRONT/sign-up'
import SSignup from './components/STUDENT_FRONT/ssignup'
import FLoginForm from './components/FACULTY_FRONT/login-form'
import SLoginForm from './components/STUDENT_FRONT/slogin'
import PartA from './components/FACULTY_FRONT/Sections/PartA'
import Faculty_Home from './components/FACULTY_FRONT/Home'
import Student_Home from './components/STUDENT_FRONT/Home_stud'
import Dash from './components/FACULTY_FRONT/DashBoard/Dashboard'

import Faculty_Profile from './components/FACULTY_FRONT/Profile/exdash'
import Sprofile from './components/STUDENT_FRONT/studentProfile'

import Con from './components/online-offline/Connection'
import DashProf from './components/FACULTY_FRONT/Profile/DashProf'
import Home2 from './components/Home2'
import Scroll from './components/Top/scroll_top'
import Yesterday_TimeTable from './components/FACULTY_FRONT/TimeTable/yesterday_slot'
import Today_TimeTable from './components/FACULTY_FRONT/TimeTable/time2'


import FProfileData from './components/FACULTY_FRONT/Profile/profile'
import Master from './components/FACULTY_FRONT/master/mater'


import BluePrint from './components/FACULTY_FRONT/Schedule/Timetable'
import Color from './components/FACULTY_FRONT/TimeTable/colordiv'
import ResetPass from './components/Reset/reset_password'
import NotFound from './components/default'
import NaVM from './nav-modal'


import AdminAuth from './components/FACULTY_FRONT/admin/auth'
import Panel from './components/FACULTY_FRONT/admin/admin_operations'


import './style.css'
require('disable-react-devtools');

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">



        {/*<Con />*/} {/*This is for checking the connectivity */}

        <Route exact path="/" component={Initial} />

        <Route path="/faculty" render={() =>
          <Faculty_Home
         />}
       />

       <Route exact path="/student" render={() =>
         <Student_Home
        />}
      />

        <Route path="/flogin" render={() =>
          <FLoginForm
         />}
       />

       <Route path="/slogin" render={() =>
         <SLoginForm
        />}
      />
       <Route path="/fsignup" component={FSignup} />
       <Route path="/ssignup" component={SSignup} />

       <Route path="/fprofile" component={Faculty_Profile} />
       <Route path="/sprofile" component={Sprofile} />


       <Route path="/partA" render={() =>
         <PartA
        />} />


       <Route path="/fprofdata" render={() =>
         <FProfileData
        />} />



       <Route path="/home" component={Home2} />

       <Route path="/Dash" render={() =>
         <Dash
        />} />
        <Route path="/master" component={Master} />


         <Route path="/blue_print" component={BluePrint} />
         <Route path="/reset_password/:token" component={ResetPass} />


          <Route path="/time_new" render={() =>
            <Today_TimeTable
           />} />
           <Route path="/time_yes" render={() =>
             <Yesterday_TimeTable
            />} />

            <Route path="/arhi" component={Decider} />

            <Route path="/no" component={NotFound} />




            <Route path="/admin" component={AdminAuth} />
            <Route path="/admin_panel" component={Panel} />




            <Route path="/test" component={Test} />

         {/*<Scroll/>*/}
      </div>
    );
  }
}

export default App;
