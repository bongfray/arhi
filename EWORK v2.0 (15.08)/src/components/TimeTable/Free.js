import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import axios from 'axios'
var empty = require('is-empty');

export default class Free extends React.Component {
  constructor() {
    super()
    this.state = {
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        freeslot:'',
        freefield:'',

    }
    this.handleFreeSlot = this.handleFreeSlot.bind(this)
    this.handleFreeVal = this.handleFreeVal.bind(this)
    this.handleFreeField = this.handleFreeField.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)


    }

        handleFreeField =(e) =>{
          this.setState({
            freefield: e.target.value,
          })

        }

        handleFreeVal =(e) =>{
          this.setState({
            freeslot: e.target.value,
          })

        }


        handleFreeSlot =(e) =>{
          if(empty(this.state.freefield)||empty(this.state.freeslot))
          {
            window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
            this.setState({
              freefield:'',
              freeslot:'',
            })
          }
          else {
          // alert(this.props.day_order)
          // alert(this.state.selected)
          e.preventDefault()
          // alert(this.state.problem_conduct)
          // alert(this.props.usern)

          axios.post('/user/timefree', {
            usern: this.props.usern,
            order:this.props.day_order+this.props.day_slot_time+'Free',
            day_slot_time: this.props.day_order+this.props.day_slot_time,
            freefield: this.state.freefield,
            freeslot: this.state.freeslot,
          })
            .then(response => {

              if(response.status===200){
                if(response.data.newmsg)
                {
                  alert(response.data.newmsg);
                  this.setState({
                    freeslot:'',
                    freefield:'',
                  })
                }
                else if(response.data.msg)
                {
                  alert(response.data.msg);
                  this.setState({
                    freeslot:'',
                    freefield:'',
                  })
                }
                else if(response.data.succ)
                {
                  alert('Success');
                }
              }
            }).catch(error => {
              window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
            })
            this.setState({
              freefield:'',
              freeslot:'',
          })
}

        }

  componentDidMount()
  {

  }

  updateAllotV (userObject) {
    this.setState(userObject)
  }

render(){
  return(
    <div><h6 className="free-head">Free Slot is for different Works. Kindly fill the Details..</h6><br />
    <span className="quali">Plaese Mention on which area you are going to work :</span><br />
              <div className="input-field inline">
                <input id="email_inline" type="text" value={this.state.freefield} onChange={this.handleFreeField} required/>
                <label for="email_inline">Fill it correctly</label>
              </div>
    <label className="pure-material-textfield-outlined alignfull">
      <textarea
        className="area"
        type="text"
        placeholder=" "
        min="10"
        max="60"
        value={this.state.freeslot}
        onChange={this.handleFreeVal}
      />
      <span>Detail About Your work</span>
    </label>
    <div className="right btn-of-submit-at-time"><Link to ="#" className="btn" onClick={this.handleFreeSlot}>SUBMIT</Link></div>
    </div>
  );
}
}
