import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import Allot2 from './slot2'
import '../style.css'

export default class Color extends Component {
	constructor(props) {
    super(props)
    this.state = {
					day:[
						{id:".1",time:"8:00-8:50",color:''},
						{id:".2",time:"8:50-9:40",color:''},
						{id:".3",time:"9:45-10:35",color:''},
						{id:".4",time:"10:40-11:30",color:''},
						{id:".5",time:"11:35-12:25",color:''},
						{id:".6",time:"12:30-1:20",color:''},
						{id:".7",time:"1:25-2:15",color:''},
						{id:".8",time:"2:20-3:10",color:''},
						{id:".9",time:"3:15-4:05",color:''},
						{id:".10",time:"4:05-4:55",color:''}
					],
    }
		this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {

    }


render() {
	
	return(
		<div className="rocontent" style={{marginTop:'90px'}}>
	{this.state.day.map((content,index)=>{

			return(
				<div>
				<Allotment usern={this.props.usern} day_order={this.props.day_order} time={content.time} day_slot_time={content.id} />
				</div>
			);
})}
</div>
);
}
}


class Allotment extends Component{
	constructor(props)
	{
		super(props)
		this.state ={
			color:'red',

		}
this.componentDidMount = this.componentDidMount.bind(this)
	}

	componentDidMount()
	{

		axios.post('/user/fetchfrom', {
			slot_time: this.props.day_order+this.props.day_slot_time,
		})
		.then(response =>{
			if(response.data)
			{
			console.log(response.data)
			this.setState({
				color:'green'
			})
		}
		})
	}

	render()
	{
		if(this.state.color === "red")
		{

			return(
				<div>
				<div className="col l4 s12" style={{color: 'red'}}  key={this.props.time}>
						<div style={{backgroundColor:'white'}} className="card hoverable each_time">
							<p className="center">{this.props.time}</p>
							<Allot2 usern={this.props.usern} day_order={this.props.day_order} day_slot_time={this.props.day_slot_time}/>
						</div>
				</div>
				</div>
			);
		}
		else if(this.state.color === "green")
		{
			return(
				<div>
				<div className="col l4 s12" style={{color: this.state.color}}  key={this.props.time}>
						<div style={{backgroundColor:'white'}} className="card hoverable each_time">
							<p className="center">{this.props.time}</p>
							<p className="center"><b>COMPLETED</b></p>
						</div>
				</div>
				</div>
			);
		}


	}
}
