const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const dayorder = new Schema({
  day_order: { type: Number, unique: false, required: false },
  time: { type: Number, unique: false, required: false },
})


const DayOrder = mongoose.model('common_dayorder', dayorder)
module.exports = DayOrder
