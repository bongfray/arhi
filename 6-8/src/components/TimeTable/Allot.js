import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import axios from 'axios'

export default class Allot extends React.Component {
  allotTimer;
  constructor() {
    super()
    this.state = {
      allot:'',
        count: 0,
        selected:'',
        problem_conduct:'',
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        problem_statement:'',

    }
    this.handleAlloted = this.handleAlloted.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateAllotV = this.updateAllotV.bind(this)
    this.getDayOrder = this.getDayOrder.bind(this)
    this.updateProblem = this.updateProblem.bind(this);


    }


    getDayOrder(){
      axios.post('/user/fetchfromtimetable', {
        day_slot_time: this.props.day_slot_time,
      })
      .then(response =>{
        if(response.data.alloted_slots){
          // alert(response.data.alloted_slots)
          this.setState({
            saved_slots: response.data.alloted_slots,
            saved_dayorder: response.data.day_order,
          });

        }
        else if(response.data.message)
        {
          this.setState({
            saved_slots: response.data.message,
            saved_dayorder: response.data.day_order
          })
        }
      });
    }

  handleAlloted =(e) =>{
    // alert(this.props.day_order)
    // alert(this.state.selected)
    e.preventDefault()
    // alert(this.state.problem_conduct)
    // alert(this.props.usern)
    axios.post('/user/timeallot', {
      usern: this.props.usern,
      day_slot_time: this.state.saved_dayorder+this.props.day_slot_time,
      selected: this.state.selected,
      order:this.state.saved_dayorder+this.props.day_slot_time+'Alloted',
      saved_slots: this.state.saved_slots,
      problem_statement: this.state.problem_statement,
    })
      .then(response => {
        if(response.status===200){
          if(response.data.e_msg)
          {
            alert(response.data.e_msg);
          }
          else if(response.data.msg)
          {
            alert(response.data.msg);
          }
          else if(response.data.succ)
          {
            alert('Success');
          }
        }
      }).catch(error => {
        window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
      })
      this.setState({
        selected: '',
    })
  }

  componentDidMount()
  {
    this.getDayOrder();


  }

componentWillUnmount(){
  clearInterval(this.allotTimer);
}

updateProblem(userObject)
{
  this.setState(userObject)
}
  updateAllotV (userObject) {
    this.setState(userObject)
  }

render(){
  return(
    <div>
    <h5 className="center">{this.state.saved_slots}</h5>
    <span style={{color:'red'}}>Reminder: </span><span>This slot is booked at the beginig of Semester.You are not allowed to edit it.</span>
    <p>
      <label>
      <input type='radio' id='radio-1' name='myRadio' value='Class_completed' onChange={(e) => this.setState({ selected: e.target.value })} />
        <span style={{color:'green'}}><b>Class Completed</b></span>
      </label>
   </p>
   <Check updateAllotV={this.updateAllotV} updateProblem={this.updateProblem} />
   <div className="right btn-of-submit-at-time"><Link to ="#" className="btn" onClick={this.handleAlloted}>SUBMIT</Link></div>
   </div>
  );
}
}


{/*-----------------------------------------------------------Check box--------------------------- */}




class Check extends React.Component{

  constructor() {
    super();
    this.state = {isChecked: false};
    this.handleChecked = this.handleChecked.bind(this);

  }



  handleChecked =(e) =>{
    // alert(this)
    this.setState({isChecked: true});
    this.props.updateAllotV({
      selected: e.target.value,
    });
  }

  render(){
    return <div>
            <p>
              <label>
              <input type='radio' id='radio-2' name='myRadio' value='problem_conduct' checked={this.state.checked} onChange={this.handleChecked} />
                <span style={{color:'red'}}><b>Problem With  Class Completion</b></span>
              </label>
           </p>
       <p><CheckDisp check={this.state.isChecked} updateProblem={this.props.updateProblem}/></p>
    </div>
  }
}



{/*-------for check radio button----------------------------------- */}

 class CheckDisp extends Component{
   constructor(){
     super();
     this.state = ({
       problem_conduct:'',
     });
     this.componentDidMount = this.componentDidMount.bind(this);
   }
   componentDidMount(){

   }
   handle1Res= (e) =>{
     this.setState({
       problem_conduct:e.target.value,
     });
     this.props.updateProblem({
       problem_statement: e.target.value,
     });
   }

   render(){
     if(this.props.check === true)
     {
       return(
         <div>
         <input value={this.state.problem_conduct} onChange={this.handle1Res}  />
         </div>
       );
     }
     else if(this.props.check === false)
     {
       return(
         <div>

         </div>
       );
     }
   }
 }


const TableRow = ({ children }) => (
  <div className="row">{children}</div>
);
