import React, { Component } from 'react'
import axios from 'axios'
import { } from 'react-router-dom';
import SunB from './Sunburst';
import data from './data'
import M from 'materialize-css';
import {} from 'materialize-css'
//var empty = require('is-empty');

class Dash extends Component {
	notifi = () => window.M.toast({html: 'Welcome to DashBoard', outDuration:'1000', inDuration:'900', displayLength:'1800'});

    componentDidMount() {
      this.getDash()
        this.notifi()
        M.AutoInit()
      }
      constructor(){
        super()
        this.getDash = this.getDash.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
      }


      getDash(){
        axios.get('/user/Dash').then(response =>{
          if(response.status===200)
          {

          }

        })
      }


render() {
  const loggedIn = this.props.loggedIn;
	if(this.props.loggedIn)
	{
		return (

			<div className="row">
			<div className="col l12">
			<SunB data={data}
			width="700"
			height="700"
			count_member="size"
			labelFunc={(node)=>node.data.name}
			_debug={true} />
			</div>

			</div>

		);
	}
	else{
		return(
			<div className="error-div">
			 <div className="row">
			 <div className="col s4">
			 <i className="large material-icons right error-icon">warning</i>
			 </div>
			 <div className="col s6">
			 <h3 className="left error-msg">Sorry !! You are not logged in</h3>
			 </div>
			 <div className="col s2">
			 </div>
			 </div>
			 <div className="row">
			 <h5 className="reminder">Please Log In to Access this Site</h5>
			 </div>
			</div>
		);
	}

}
}

export default Dash
