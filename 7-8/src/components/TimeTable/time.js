import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'
import TimeSlot from './slot'
import axios from 'axios'




{/*-------------------------------------------------Clock for Current Date and time------------------- */}

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <div className="curr_date">{new Date().toDateString()}</div>
      <p className="curr_time">{this.state.time}</p>
      </div>
    );
  }
}



{/*-------------------------------------------------------Automatically Chanege the day order------------------------------------------------*/}


class Day_Order extends Component{

  intervalID_new;

  constructor() {
    super();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.getCurrentDayOrder = this.getCurrentDayOrder.bind(this);
    this.getNext = this.getNext.bind(this);
    this.state = {
      day_order: '',
    }
  }

getCurrentDayOrder(){
  axios.get('/user/fetchdayorder', {
  })
  .then(response =>{
    if(response.data.day_order)
    {
      let hour = (new Date().getHours())*3600000;
      let minutes = (new Date().getMinutes())*60000;
      let second =(new Date().getSeconds())*1000;
      let time_ren = hour + minutes + second;
      this.intervalID_new = setTimeout((this.getNext.bind(this)),(time_ren - response.data.time));
      this.setState({
        day_order: response.data.day_order,
      });
    }

  });
}
getNext(){
  this.getCurrentDayOrder();
}


  componentDidMount(){
    this.getCurrentDayOrder();
  }

  componentWillUnmount(){
    clearInterval(this.intervalID_new);
  }


  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.state.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{

  constructor() {
    super();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.dayorder = this.dayorder.bind(this);
    this.state = {
      opendir: ''
    }
  }


  dayorder()
  {
    axios.get('/user/fetchdayorder', {
    })
    .then(response =>{
      if(response.data.day_order)
      {

      }

    });
  }

  componentDidMount(){
    M.AutoInit();

  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };
  render(){
    // alert(this.props.day_order)
    // if(this.props.loggedIn)
    // {
    return(
      <div className="time-div">

      <div className="row">
      <div className="col l2">
          <span className="col s2">
          <b className="center day-order-day">DETAILS</b>
          </span>
        <div className="date_pick">
          <Clock />
        </div>
        <br />
      <div className="dynamic_dayorder">
          <Day_Order />
      </div>
        <br />
      <div className="pick_curent">
            <select value={this.state.opendir}  onChange={this.handledir}>
              <option value="" disabled selected>Choose</option>
              <option value="r_class">Regular Class</option>
              <option value="own_ab">Absent</option>
              <option value="class_cancel">Class Cancelled</option>
              <option value="college_cancel">College Cancelled</option>
            </select>
        </div>
    </div>
    <div className="col l10">
      <Content opendir={this.state.opendir} usern={this.props.username}/>
    </div>
    </div>
      </div>
    );
  // }
  //   else{
  //     return(
  //       <div className="error-div">
  //        <div className="row">
  //        <div className="col s4">
  //        <i className="large material-icons right error-icon">warning</i>
  //        </div>
  //        <div className="col s6">
  //        <h3 className="left error-msg">Sorry !! You are not logged in</h3>
  //        </div>
  //        <div className="col s2">
  //        </div>
  //        </div>
  //        <div className="row">
  //        <h5 className="reminder">Please Log In to Access this Site</h5>
  //        </div>
  //       </div>
  //     );
  //
  //   }
  }
}




{/*-----------------------------------------------This class is for dynamically allocating div ------------- */}

class Content extends Component{
  constructor(){
    super();
    this.state={
      datt : [
        {con:".1",head:"8:8:50"},{con:".2",head:"08:50 - 09:40"},{con:".3",head:"09:45 - 10:35"},{con:".4",head:"10:40 - 11:30"},{con:".5",head:"11:35 - 12:25"},{con:".6",head:"12:30 - 01:20"},{con:".7",head:"01:25 - 02:15"},{con:".8",head:"02:20 - 03:10"},{con:".9",head:"03:15 - 04:05"},{con:".10",head:"04:05 - 04:55"}
      ],
  };
  }

  render(){
    // let {dayorder}= this.props.dayorder;
    if(this.props.opendir==="r_class")
    {
          return(
            <div>
             <TimeSlot datt={this.state.datt} usern={this.props.usern} />
            </div>
          );

    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"

          />
          <span>Kindly Provide Valid Reason for Your Absence</span>
        </label>
        </div>
      );
    }
    else if(this.props.opendir === "class_cancel")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Kindly Provide Valid Reason of Class Cancellation</span>
        </label>
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select Something from the DropDown</div>
      );
    }

  }
}
