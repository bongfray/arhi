import React, { Component } from 'react';
import Login from './Login';
import Signup from './Signup';
import Navbar from './Navbar';
import './style.css';
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import { BrowserRouter, Route } from 'react-router-dom';
import ReactD3 from './Dashboard';


class App extends Component {
  
  
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          
          {/* <Navbar/>
          <Route exact path='/' component={Login} />
          <Route exact path='/Signup' component={Signup}/>
          <Route exact path='/Das' component={Dashboard}/>
           */}
           <ReactD3 />

        </div>
      </BrowserRouter>
    );
  }
}


export default App;
