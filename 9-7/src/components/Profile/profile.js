import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import M from "materialize-css";
//import { } from 'materialize-css';
import "../style.css";
var empty = require("is-empty");

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      name: "",
      id: "",
      username: "",
      phone: "",
      campus: "",
      dept: "",
      desgn: "",
      ust_year: "",
      ufn_year: "",
      marksug: "",
      pst_year: "",
      pfn_year: "",
      markspg: ""
    };
    this.handleTitle = this.handleTitle.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleId = this.handleId.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePhone = this.handlePhone.bind(this);
    this.handleCampus = this.handleCampus.bind(this);
    this.handleDept = this.handleDept.bind(this);
    this.handleDesgn = this.handleDesgn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleuMarks = this.handleuMarks.bind(this);
    this.handleuStYear = this.handleuStYear.bind(this);
    this.handleuFnYear = this.handleuFnYear.bind(this);
  }

  handleTitle = e => {
    this.setState({
      title: e.target.value
    });
  };
  handleName = e => {
    this.setState({
      name: e.target.value
    });
  };
  handleId = e => {
    this.setState({
      id: e.target.value
    });
  };
  handleEmail = e => {
    this.setState({
      username: e.target.value
    });
  };
  handlePhone = e => {
    this.setState({
      phone: e.target.value
    });
  };
  handleCampus = e => {
    this.setState({
      campus: e.target.value
    });
  };
  handleDept = e => {
    this.setState({
      dept: e.target.value
    });
  };
  handleDesgn = e => {
    this.setState({
      desgn: e.target.value
    });
  };
  handleuStYear = e => {
    this.setState({
      ust_year: e.target.value
    });
  };
  handleuFnYear = e => {
    this.setState({
      ufn_year: e.target.value
    });
  };
  handleuMarks = e => {
    this.setState({
      marksug: e.target.value
    });
  };
  handlepStYear = e => {
    this.setState({
      pst_year: e.target.value
    });
  };
  handlepFnYear = e => {
    this.setState({
      pfn_year: e.target.value
    });
  };
  handlepMarks = e => {
    this.setState({
      markspg: e.target.value
    });
  };
  handleSubmit(event) {
    //console.log('sign-up handleSubmit, username: ')
    //console.log(this.state.username)
    event.preventDefault();
    if (
      empty(this.state.title) ||
      empty(this.state.name) ||
      empty(this.state.id) ||
      empty(this.state.username) ||
      empty(this.state.phone) ||
      empty(this.state.campus) ||
      empty(this.state.dept) ||
      empty(this.state.desgn)
    ) {
      window.M.toast({
        html: "Enter all the Details",
        outDuration: "850",
        inDuration: "800",
        displayLength: "1500"
      });

      return false;
    } else if (this.state.password !== this.state.cnf_pswd) {
      window.M.toast({
        html: "Password does not match",
        outDuration: "850",
        inDuration: "800",
        displayLength: "1500"
      });
      return false;
      // The form won't submit
    } else if (this.state.phone.length !== 10) {
      window.M.toast({
        html: "Enter correct format of Phone no",
        outDuration: "850",
        inDuration: "800",
        displayLength: "1500"
      });
    } else {
      window.M.toast({
        html: "UPDATED",
        outDuration: "850",
        inDuration: "800",
        displayLength: "1500"
      });
    }
  }
  notifi = () =>
    window.M.toast({
      html: "Enter Details",
      outDuration: "850",
      inDuration: "800",
      displayLength: "1500"
    });

  componentDidMount() {
    this.notifi();
    M.AutoInit();
  }

  render() {
    return (
      <div className="row">
        <div className="col s2" />

        <div className="col s8 form-signup">
          <div className="ew center">
            <h5 className="reg">PROFILE DATA</h5>
          </div>
          <form className="row form-con">
            <div className="input-field row">
              <div className="input-field col s2">
                <select value={this.state.title} onChange={this.handleTitle}>
                  <option value="" disabled selected>
                    Title
                  </option>
                  <option value="Mr.">Mr.</option>
                  <option value="Mrs.">Mrs.</option>
                  <option value="Miss.">Miss.</option>
                  <option value="Dr.">Dr.</option>
                </select>
              </div>

              <div className="input-field col s6">
                <input
                  id="name"
                  type="text"
                  className="validate"
                  value={this.state.name}
                  onChange={this.handleName}
                  required
                />
                <label htmlFor="name">Name</label>
              </div>

              <div className="input-field col s4">
                <input
                  id="fac_id"
                  type="text"
                  className="validate"
                  value={this.state.id}
                  onChange={this.handleId}
                  required
                />
                <label htmlFor="fac_id">ID</label>
              </div>
            </div>

            <div className="input-field row">
              <div className="input-field col s4">
                <select value={this.state.campus} onChange={this.handleCampus}>
                  <option value="" disabled selected>
                    Campus
                  </option>
                  <option value="ktr">Kattankulathur Campus</option>
                  <option value="rpr">Ramapuram Campus</option>
                  <option value="vpl">Vadapalani Campus</option>
                  <option value="ncr">NCR Campus</option>
                </select>
              </div>
              <div className="input-field col s4">
                <select value={this.state.dept} onChange={this.handleDept}>
                  <option value="" disabled selected>
                    Department
                  </option>
                  <option value="cse">Computer Science</option>
                  <option value="it">Information Technology</option>
                  <option value="swe">Software</option>
                  <option value="mech">Mechanical</option>
                </select>
              </div>
              <div className="input-field col s4">
                <select value={this.state.desgn} onChange={this.handleDesgn}>
                  <option value="" disabled selected>
                    Designation
                  </option>
                  <option value="hod">HOD</option>
                  <option value="prf">Professor</option>
                  <option value="aprf">Associate Professor</option>
                  <option value="asprf">Assistant Professor</option>
                </select>
              </div>
            </div>

            <div className="input-field row">
              <div className="input-field col s8">
                <input
                  id="email"
                  type="email"
                  className="validate"
                  value={this.state.username}
                  onChange={this.handleEmail}
                  required
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="input-field col s4">
                <input
                  id="ph_num"
                  type="text"
                  className="validate"
                  value={this.state.phone}
                  onChange={this.handlePhone}
                  required
                />
                <label htmlFor="ph_num">Phone Number</label>
              </div>
            </div>
            <div className="input-field row">
              <div className="col s3">
                <h6 className="labdeg">U.G Degree:</h6>
              </div>
              <div className="input-field col s3">
                <input
                  id="ust_year"
                  type="number"
                  className="validate"
                  value={this.state.ust_year}
                  onChange={this.handleuStYear}
                  required
                />
                <label htmlFor="ust_year">Starting Year</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="ufn_year"
                  type="number"
                  className="validate"
                  value={this.state.ufn_year}
                  onChange={this.handleuFnYear}
                  required
                />
                <label htmlFor="ufn_year">Year of Completion</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="marksug"
                  type="text"
                  className="validate"
                  value={this.state.marksug}
                  onChange={this.handleuMarks}
                  required
                />
                <label htmlFor="marksug">Percentage/GPA</label>
              </div>
            </div>
            <div className="input-field row">
              <div className="col s3 labdeg">
                <h6 className="labdeg">P.G Degree:</h6>
              </div>
              <div className="input-field col s3">
                <input
                  id="pst_year"
                  type="number"
                  className="validate"
                  value={this.state.pst_year}
                  onChange={this.handlepStYear}
                  required
                />
                <label htmlFor="pst_year">Starting Year</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="pfn_year"
                  type="number"
                  className="validate"
                  value={this.state.pfn_year}
                  onChange={this.handlepFnYear}
                  required
                />
                <label htmlFor="pfn_year">Year of Completion</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="markspg"
                  type="text"
                  className="validate"
                  value={this.state.markspg}
                  onChange={this.handlepMarks}
                  required
                />
                <label htmlFor="markspg">Percentage/GPA</label>
              </div>
            </div>
            <div>
              <div className="col s3" />
              <div className="col s5" />
              <Link
                to="#"
                className="waves-effect waves-light btn blue-grey darken-2 col s4 sup"
                onClick={this.handleSubmit}
              >
                Submit
              </Link>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Signup;
