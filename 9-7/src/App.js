import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import Signup from './components/sign-up'
import LoginForm from './components/login-form'
import Navbar from './components/navbar'
import PartB from './components/Sections/par'
import PartA from './components/Sections/PartA'
import Home from './components/Home'
import Dash from './components/DashBoard/dash/global-data/Dashboard'
import Profile from './components/Profile/profile'
import Con from './components/online-offline/Connection'
import DashProf from './components/Profile/DashProf'
import Home2 from './components/Home2'
import './style.css'
require('disable-react-devtools');

class App extends Component {
  constructor() {
    super()
    this.state = {
      loggedIn: false,
      username: null,
      logins: null
    }

    this.getUser = this.getUser.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }


  componentDidMount() {
    this.getUser()
  }

  updateUser (userObject) {
    this.setState(userObject)
  }

  getUser() {
    axios.get('/user/').then(response => {
      console.log('Get user response: ')
      console.log(response.data)
      if (response.data.user) {
        console.log('Get User: There is a user saved in the server session: ')
        console.log(response.data.user);
        this.setState({
          loggedIn: true,
          username: response.data.user.username,
          logins: response.data.user.use

        })
      } else {
        console.log('Get user: no user');
        this.setState({
          loggedIn: false,
          username: null,
          logins: null

        })
      }
    })
  }


  render() {
    //const redir = this.props.logins;
    return (
       <div className="App">

        <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn} logins={this.state.logins}/>


        {/*<Con />*/} {/*This is for checking the connectivity */}


        {/* greet user if logged in: */}

        {/*this.state.loggedIn &&
          <p className="center">Welcome , {this.state.username}!</p>

        */}



        {/*this.state.loggedIn &&

          window.location.assign(API_URL+'/signup')
        */}
    {  /*  {this.state.loggedIn ?
        (

          window.location.assign(API_URL+'/signup')
        )
        :
        (
        window.location.assign(API_URL+'/')
        )
      }*/}


        <Route path="/login" render={() =>
          <LoginForm
         updateUser={this.updateUser}
         />}
       />

       <Route path="/signup" component={Signup} />



      <Route path="/partB" render={() =>
        <PartB
       updateUser={this.updateUser} loggedIn={this.state.loggedIn}
       />} />
       <Route path="/partA" render={() =>
         <PartA loggedIn={this.state.loggedIn}
        />} />
       <Route exact path="/" component={Home} />
       <Route path="/profile" component={Profile} />
       <Route path="/dashprof" component={DashProf} />
       <Route path="/home" component={Home2} />
       <Route path="/Dash" render={() =>
         <Dash
        updateUser={this.updateUser} loggedIn={this.state.loggedIn}
        />} />
      </div>
    );
  }
}

export default App;
