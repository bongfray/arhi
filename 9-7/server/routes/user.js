const express = require('express')
const router = express.Router()
const User = require('../database/models/user')
const passport = require('../passport')

router.post('/', (req, res) => {
    //console.log('user signup');

    const { username, password,title,name,id,phone,campus,dept,desgn,count} = req.body
    User.findOne({ username: username }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            msg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
              username: username,
              password: password,
              title: title,
              name: name,
              id: id,
              phone: phone,
              campus: campus,
              dept: dept,
              desgn: desgn,
              count: count
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})

router.post(
    '/login',(req, res, next) => {

      const { username, password} = req.body

      User.findOne({ username: username }, function(err, objs){

          if (objs.count === "0")
          {
              var counter = 0;
              User.updateOne({ username: username }, {count: counter+1},(err, user) => {

              })
              console.log(counter); // this prints "Renato", as it should

          }
          else if (objs.count === "1")
          {
              var coun = 0;
              User.updateOne({ username: username }, {count: coun+2},(err, user) => {

              })
              console.log(counter); // this prints "Renato", as it should

          }
      });


      // User.findOne({ username: username },(err, res) => {
      //   if(res.count === 0)
      //   {
      //     var count=0;
      //     User.updateOne({ username: username }, {count: count+1},(err, user) => {
      //
      //     })
      //   }
      //   else if(res.count>=2)
      //   {
      //     var count=0;
      //     User.updateOne({ username: username }, {count: count+3},(err, user) => {
      //
      //     })
      //   }
      // })

        console.log('routes/user.js, login, req.body: ');
        console.log(req.body)
        next()
    },
    passport.authenticate('local'),
    (req, res) => {
        //console.log('logged in', req.user);
        var userInfo = {
            use: req.user.desgn,
            username: req.user.username,
            count: req.user.count
        };
        res.send(userInfo);
    },


)

router.get('/', (req, res, next) => {
    //console.log('===== user!!======')

    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})


router.get('/login', function(req, res) {

    var name = '';
    const { username} = req.user;
    User.findOne({ username: username }, function(err, objs){

        if (objs.count)
        {
            name = objs.count;
            //console.log(name); // this prints "Renato", as it should
            res.send(objs.count);
        }
    });


        // res.render('index', {
        //     title: name,
        //     year: date.getFullYear()
        // });
    });











// router.get('/login', (req, res) => {
//
//
//   //console.log("Hello");
//   //console.log(req.user);
//   const { username} = req.user;
//   User.findOne({ username: username }, (err, res) => {
//     console.log(res.count);
//
//       // console.log(data);
//       // res.json({dir: 1});
//     })
//
//
//
// // res.send(emsg);
// })



router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })



})

router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out' })
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

module.exports = router
