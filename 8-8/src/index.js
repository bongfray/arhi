import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { disableReactDevTools } from '@fvilers/disable-react-devtools';
import { BrowserRouter } from 'react-router-dom' //don't need to specify localhost url in axios http address

//style
import './index.css';

if (process.env.NODE_ENV === 'production') {
  disableReactDevTools();
}


ReactDOM.render(
	<BrowserRouter>
		<App />
	</BrowserRouter>,
	document.getElementById('root')
)
