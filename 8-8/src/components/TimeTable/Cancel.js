import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import axios from 'axios'

export default class Cancel extends React.Component {
  allotTimer;
  constructor() {
    super()
    this.state = {
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        c_cancelled: '',
        compensation:'false',
    }
    this.handleMissComp = this.handleMissComp.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.getDayOrder = this.getDayOrder.bind(this)
    }

    getDayOrder(){
      axios.post('/user/fetchfromtimetable', {
        day_slot_time: this.props.day_slot_time,
      })
      .then(response =>{
        if(response.data.alloted_slots){
          // alert(response.data.alloted_slots)
          this.setState({
            saved_slots: response.data.alloted_slots,
            saved_dayorder: response.data.day_order,
          });

        }
        else if(response.data.message)
        {
          this.setState({
            saved_slots: response.data.message,
            saved_dayorder: response.data.day_order
          })
        }
      });
    }


    handleCancel =(e) =>{
      this.setState({
          c_cancelled: e.target.value,
      })
    }


    handleCancelSlot =(e) =>{
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      // alert(this.state.compensation)
      // alert(this.state.c_cancelled


      axios.post('/user/timecancel', {
        usern: this.props.usern,
        order:this.state.saved_dayorder+this.props.day_slot_time+'Cancel',
        day_slot_time: this.state.saved_dayorder+this.props.day_slot_time,
        compensation_status: this.state.compensation,
        c_cancelled: this.state.c_cancelled,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.msg)
            {
              alert(response.data.msg);
            }
            else if(response.data.exist)
            {
              alert(response.data.exist);
            }
            else if(response.data.succ)
            {
              alert('Success');
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          selected: '',
      })


    }

    handleMissComp = (e) =>{
      this.setState({
        compensation: e.target.value,
      })
    }

  componentDidMount()
  {
    this.getDayOrder();


  }

componentWillUnmount(){
  clearInterval(this.allotTimer);
}

  updateAllotV (userObject) {
    // alert(userObject)
    this.setState(userObject)
  }

render(){
  return(
    <div>
    <label className="pure-material-textfield-outlined alignfull">
      <textarea
        className="area"
        type="text"
        placeholder=" "
        min="10"
        max="60"
        value={this.state.c_cancelled}
        onChange={this.handleCancel}
      />
      <span>Enter the reason</span>
    </label><br />
    <p className="need">
     <label>
       <input type="checkbox" value="true" onChange={this.handleMissComp} />
       <span style={{color:'black'}}>Would you like to Get a extra slot in this week ? </span>
     </label>
   </p>
   <div className="right btn-of-submit-at-time"><Link className="btn" to="#" onClick={this.handleCancelSlot} >SUBMIT</Link>
   </div>
    </div>
  );
}
}
