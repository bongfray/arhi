import React, { Component } from 'react';
import axios from 'axios'
// import {API_URL} from '../utils/apiUrl'
import { Link } from 'react-router-dom';
import M from 'materialize-css';
import Nav from '../dynnav'
import '../style.css'
var empty = require('is-empty');


class Timetable extends Component {
	constructor() {
    super()
    this.state = {
			logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/newd',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
			datas: [
				{
					day_order:'Day Order 1',
					day:[
						{id:"1.1"},{id:"1.2"},{id:"1.3"},{id:"1.4"},{id:"1.5"},{id:"1.6"},{id:"1.7"},{id:"1.8"},{id:"1.9"},{id:"1.10"}
					]

				},
				{
					day_order:'Day Order 2',
					day:[
						{id:"2.1"},{id:"2.2"},{id:"2.3"},{id:"2.4"},{id:"2.5"},{id:"2.6"},{id:"2.7"},{id:"2.8"},{id:"2.9"},{id:"2.10"}
					]
				},
				{
					day_order:'Day Order 3',
					day:[
						{id:"3.1"},{id:"3.2"},{id:"3.3"},{id:"3.4"},{id:"3.5"},{id:"3.6"},{id:"3.7"},{id:"3.8"},{id:"3.9"},{id:"3.10"}
					]
				},
				{
					day_order:'Day Order 4',
					day:[
						{id:"4.1"},{id:"4.2"},{id:"4.3"},{id:"4.4"},{id:"4.5"},{id:"4.6"},{id:"4.7"},{id:"4.8"},{id:"4.9"},{id:"4.10"}
					]
				},
				{
					day_order:'Day Order 5',
					day:[
						{id:"5.1"},{id:"5.2"},{id:"5.3"},{id:"5.4"},{id:"5.5"},{id:"5.6"},{id:"5.7"},{id:"5.8"},{id:"5.9"},{id:"5.10"}
					]
				}
			],
			blue_allot:'',
			key_ind:'',
    }
		this.handleBlue_Allot = this.handleBlue_Allot.bind(this)
    this.handleBlue_AllotChange = this.handleBlue_AllotChange.bind(this)
		this.componentDidMount = this.componentDidMount.bind(this)

    }
		handleBlue_AllotChange =(e,index) =>{
			// alert(e.target.name)
				const value = e.target.value;
     this.setState({ blue_allot: value, index,key_ind: e.target.name});

    }
		handleBlue_Allot =(e) =>{
			e.preventDefault()

			axios.post('/user/send_blue_print', {
				alloted_slots: this.state.blue_allot,
				timing: this.state.key_ind,
				})
				 .then(response => {
					 if(response.status === 200)
					 {
						 if(response.data.exceed)
						 {
							 window.M.toast({html: response.data.exceed,outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
						 }
						 else if(response.data.succ)
						 {
							 window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded green lighten-1'});
						 }
						 else if(response.data.emsg)
						 {
							 window.M.toast({html: response.data.emsg,outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
						 }
					 }
				 }).catch(error => {
					 	window.M.toast({html: 'Internal Error',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
				 })
				 this.setState({
					 alloted_slots:'',
					 timing:'',
				 })


		}

  notifi = () => window.M.toast({html: 'Enter Details', outDuration:'850', inDuration:'800', displayLength:'1500'});

    componentDidMount() {
        this.notifi();
        M.AutoInit();
    }


render() {
	return (
		<React.Fragment>
		<Nav content={this.state.content} get={this.state.get} logout={this.state.logout}/>
        <div className="row tcontent">
        <table className="striped centered">
        <thead>
          <tr>
              <th style={{border: '1px solid'}}><h6><b>Hour /<br/>Day Order</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>08:00 - 08:50<br/>1</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>08:50 - 09:40<br/>2</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>09:45 - 10:35<br/>3</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>10:40 - 11:30<br/>4</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>11:35 - 12:25<br/>5</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>12:30 - 01:20<br/>6</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>01:25 - 02:15<br/>7</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>02:20 - 03:10<br/>8</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>03:15 - 04:05<br/>9</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>04:05 - 04:55<br/>10</b></h6></th>
          </tr>
		  <br/>

        </thead>

        <tbody>

				{this.state.datas.map((content,index)=>{
					return(

					<tr style={{backgroundColor: 'white'}} className="card hoverable" key={index}>
					<td style={{borderRight: '1px solid',bordeBottom:'1px solid'}}><b>{content.day_order}</b></td>
			    {content.day.map((content,ind)=>{
													return(
									            <td style={{borderRight: '1px solid '}} key={content.id}>
															<label className="pure-material-textfield-outlined alignfull">
					                      <textarea
					                        className="area"
					                        type="text"
					                        placeholder=" "
					                        min="10"
					                        max="60"
																	name={content.id}
					                        value={ this.state.ind && this.state.blue_allot}
																	onChange={e => this.handleBlue_AllotChange(e, content.id)}
					                      />
					                      <span>Fill Details</span>
					                    </label>
															  <div className="right btn-of-submit-at-time"><Link to ="#" className="btn" onClick={this.handleBlue_Allot}>SUBMIT</Link></div>
									            </td>

													);

													})}
					</tr>
				);

				})}
        </tbody>

      </table>


	</div>


</React.Fragment>
	)
}
}

export default Timetable
