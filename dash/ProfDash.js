import React, { Component } from 'react';
import Sunburst from './global-data/Sunburst';
import data from './global-data/data';
import Logout from './Logout.png';
import Slogo from './Slogo.png'

class ProfDash extends Component{
    render(){
        return(
            <div>
            <nav>
                <div className="nav-wrapper blue-grey darken-2">
                <a href="#" class="brand-logo right"><img src={Logout} alt="Logout" className="logout" title="Logout" /></a>
                <ul id="nav-mobile" class="left hide-on-med-and-down">
                    <li><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></li>
                    <li><a href="/">Home</a></li>
                    <li><a href="/">Section 1</a></li>
                    <li><a href="/">Section 2</a></li>
                </ul>
                </div>
            </nav>
            <Sunburst data={data}
            width="700"
            height="700"
            count_member="size"
            labelFunc={(node)=>node.data.name}
            _debug={true} />
            </div>
        )
    }
}

export default ProfDash;