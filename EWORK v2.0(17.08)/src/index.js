import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { disableReactDevTools } from '@fvilers/disable-react-devtools';
import { BrowserRouter,Route, Switch } from 'react-router-dom' //don't need to specify localhost url in axios http address
import NotFound from './components/default'
import './index.css';

if (process.env.NODE_ENV === 'production') {
  disableReactDevTools();
}


ReactDOM.render(
	<BrowserRouter>
    <Switch>
    	<App />
    </Switch>
	</BrowserRouter>,
	document.getElementById('root')
)
