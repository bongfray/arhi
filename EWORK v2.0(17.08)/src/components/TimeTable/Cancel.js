import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import axios from 'axios'
var empty = require('is-empty');

export default class Cancel extends React.Component {
  constructor() {
    super()
    this.state = {
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        c_cancelled: '',
        compensation:'false',
    }
    this.handleMissComp = this.handleMissComp.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    }

    closeModal=()=>{
      this.props.closeModal();
    }
    colorChange=()=>{
      this.props.color()
    }

    handleCancel =(e) =>{
      this.setState({
          c_cancelled: e.target.value,
      })
    }


    handleCancelSlot =(e) =>{
      if(empty(this.state.c_cancelled))
      {
        window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else {
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      // alert(this.state.compensation)
      // alert(this.state.c_cancelled


      axios.post('/user/timecancel', {
        usern: this.props.usern,
        order:this.props.day_order+this.props.day_slot_time+'Cancel',
        day_slot_time: this.props.day_order+this.props.day_slot_time,
        compensation_status: this.state.compensation,
        c_cancelled: this.state.c_cancelled,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.msg)
            {
              alert(response.data.msg);
              this.setState({
                c_cancelled:'',
              });
              this.closeModal();
            }
            else if(response.data.exist)
            {
              alert(response.data.exist);
              this.setState({
                c_cancelled:'',
              });
            }
            else if(response.data.succ)
            {
              alert('Success');
              this.closeModal();
              this.colorChange();
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          c_cancelled:'',
      })
}

    }

    handleMissComp = (e) =>{
      this.setState({
        compensation: e.target.value,
      })
    }

  componentDidMount()
  {
  }


  updateAllotV (userObject) {
    // alert(userObject)
    this.setState(userObject)
  }

render(){
  return(
    <div>
    <label className="pure-material-textfield-outlined alignfull">
      <textarea
        className="area"
        type="text"
        placeholder=" "
        min="10"
        max="60"
        value={this.state.c_cancelled}
        onChange={this.handleCancel}
      />
      <span>Enter the reason</span>
    </label><br />
    <p className="need">
     <label>
       <input type="checkbox" value="true" onChange={this.handleMissComp} />
       <span style={{color:'black'}}>Would you like to Get a extra slot for your work completion in this week ? </span>
     </label>
   </p>
   <div className="right btn-of-submit-at-time"><Link className="btn" to="#" onClick={this.handleCancelSlot} >SUBMIT</Link>
   </div>
    </div>
  );
}
}
