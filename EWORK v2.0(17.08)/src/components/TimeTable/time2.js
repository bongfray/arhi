import React, { Component, Fragment  } from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import {Collapsible, CollapsibleItem, Modal} from 'react-materialize'
import M from 'materialize-css'
import TimeSlot from './slot'
import axios from 'axios'
import ColorRep from './colordiv.js'
import Insturction from './instructionmodal'
import Time from './time2'
import '../style.css'


{/*-------------------------------------------------Clock for Current Date and time------------------- */}

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <span>{new Date().toDateString()}</span>
      <p>{this.state.time}</p>
      </div>
    );
  }
}



{/*-------------------------------------------------------Automatically Chanege the day order------------------------------------------------*/}


class Day_Order extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{

  constructor() {
    super();
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      opendir: '',
      modal: false,
      display:'block',
      saved_dayorder:'',
      redirectTo:'',
    }
  }

  getCount =() =>{
    axios.get('/user/knowcount', {
    })
    .then(response =>{
      if(response.data.count ==="1")
      {
          this.selectModal();
      }
    })
  }

  getDayOrder = () =>{
    axios.get('/user/fetchdayorder', {
    })
    .then(response =>{
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else if(response.data.day_order === 0){
        this.setState({
          display:'none',
          saved_dayorder:'NO',
        });
      }
      else if(response.data.day_order){
        this.setState({
          saved_dayorder: response.data.day_order,
        });
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
    M.AutoInit();
    this.getDayOrder();
    this.getCount();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };

      render(){
        if (this.state.redirectTo) {
             return <Redirect to={{ pathname: this.state.redirectTo }} />
         } else {

        return(
          <div className="time-div">
            <Insturction
                displayModal={this.state.modal}
                closeModal={this.selectModal}
            />
          <div className="row">
          <div className="col l2">
            <Clock />
          </div>
          <div className="col l10"><div className="row"><p className="col l4"></p>
            <p className="left day-order-day col l8" style={{fontSize:'20px'}}>Today's DayOrder</p>
            </div>
          </div>
          </div>
          <div className="row">
          <div className="col l2">
          <Link to="/time_yes" style={{color: 'black'}} >Yesterday's Dayorder</Link>
              <Day_Order day_order={this.state.saved_dayorder}/>
            <br />
          <div className="status_of_day" style={{display:this.state.display}}>
                <select value={this.state.opendir}  onChange={this.handledir}>
                  <option value="" disabled selected>Select Here</option>
                  <option value="r_class">Regular Class</option>
                  <option value="own_ab">Absent</option>
                  <option value="college_cancel">College Cancelled</option>
                </select>
            </div>
        </div>
        <div className="col l10">
          <Content opendir={this.state.opendir} day_order={this.state.saved_dayorder} usern={this.props.username}/>
        </div>
        </div>
          </div>
        );
      }
      }
    }




{/*-----------------------------------------------This class is for dynamically allocating div ------------- */}

class Content extends Component{
  constructor(){
    super();
    this.state={
      saved_dayorder: '',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }

  render(){
    console.log(this.props.day_order)
    if(this.props.opendir==="r_class")
    {
          return(
            <React.Fragment>
             <ColorRep usern={this.props.usern} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
         <Switch />
        </div>
      );
    }
    else if(this.props.opendir === "college_cancel")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Kindly Provide the Reason of College Cancellation</span>
        </label>
        <a href="#" className="btn right">Submit</a>
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select from the DropDown</div>
      );
    }

  }
}





class Switch extends React.Component {

    constructor ( props ) {
        super( props );

		this.state = {
			isChecked: false,
      history:'',
		}
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
    }
    render () {

        return(
          <div>

            <div className="switch center">
                <label style={{color:'red',fontSize:'15px'}}>
                    Want to compensate on other day ?
                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    Want to Handover this class to Other Faculty?
                </label>
            </div>
            <br />
            <br /><br />
            <InputValue datas={this.state.isChecked}/>
          </div>
        );
    }

}

class InputValue extends Component{
  render(){
    if(this.props.datas === true)
    {
      return(
        <div>
        <div className="input-field">
           <input id="fac" type="text" />
           <label for="fac">Name of the Faculty</label>
        </div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Provide the Details of the Classes here</span>
        </label>
          <a className="btn">Submit</a>
        </div>
      );
    }
    else{
      return(
        <div>
        <div className="input-field">
           <input id="fac1" type="text" />
           <label for="fac1">Mention the Date</label>
        </div>
        <div className="input-field">
           <input id="fac2" type="text" />
           <label for="fac2">Mention the DayOrder where you want to compensate this</label>
        </div>
        <div className="input-field">
           <input id="fac3" type="text" />
           <label for="fac3">Mention the slot</label>
        </div>
          <a className="btn">Submit</a>
        </div>
      );
    }
  }
}
