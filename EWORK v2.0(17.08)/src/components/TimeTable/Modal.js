import React, { Component } from 'react'
import '../style.css'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Button, Modal} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Allot'
import Free from './Free'
import Cancel from './Cancel'


export default class Modal2 extends Component{
constructor(props){
  super(props);
  this.state = {
    rit:'',
    day_order:'',
    modal: false,

  };
  this.closeModal = this.closeModal.bind(this);
}
handle1 = (e, index) => {
  // alert(e.target.value);
  // alert(e.target.name);
  const key = e.target.name;
  const value = e.target.value;
  this.setState({ rit: value, index ,day_order: key, index});

};
componentDidMount(){
  M.AutoInit();

}

    closeModal(e) {
        e.stopPropagation()
        this.props.closeModal()
     }
     render(){
       const divStyle = {
            display: this.props.displayModal ? 'block' : 'none',
       };
     return (
       <div className="">

       <div
         className="modal mobmodal"
         style={divStyle} >
          <div
             className="modal-content"
             onClick={ e => e.stopPropagation() } >
             <div
                 onClick={ this.closeModal }>
  <a className="float_cancel btn-floating btn-small waves-effect black  right" onClick={ this.closeModal }><i className="material-icons">cancel</i></a>
             </div>
             <br />

                  <div class="mymodal">
                    <div className="root-of-time" >
                    <div className="row">
                         <div className="col l12 ">
                         <div className="ca">
                         <span className="">
                         <div className="center day-order-day">{this.state.day_order}</div>
                         </span>
                         </div><br /><br />
                           <div>
                             <select name={this.state.day_order} onChange={e => this.handle1(e, this.state.day_order)}>
                                 <option defaultValue="" disabled selected>Choose</option>
                                 <option value="allot">Alloted Work/Class</option>
                                 <option value="free">Free Slot</option>
                                 <option value="slot_aborted">Slot Aborted</option>
                             </select>
                           </div>
                           <span className="drop">

                             <TableDisplay color={this.props.color}
                             selectValue={this.state.rit} day_slot_time={this.props.day_slot_time} day_order={this.props.day_order} usern={this.props.usern} closeModal={this.props.closeModal}
                             />
                           </span>
                         </div>


                         </div>

        </div>
                  </div>
          </div>
       </div>
</div>
     );
   }
}



     class TableDisplay extends Component {
       constructor() {
         super()
         this.state = {
           allot:'',
             count: 0,
             selected:'',
             problem_conduct:'',
             saved_dayorder: '',
             day_recent:'',

         }
         }

       render() {


                     if (this.props.selectValue === "allot") {
                       return(
                         <div className="">

                         <Allot color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                         </div>
                       );

                     } else if (this.props.selectValue === "free") {
                       return(
                         <Free color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                       );

                     }
                     else if (this.props.selectValue === "slot_aborted") {
                      return(
                        <Cancel color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                      );

                    }
                     else{
                       return(
                         <div className="inter-drop center">
                           Choose from above DropDown
                         </div>
                       );
                     }
       }
     }
