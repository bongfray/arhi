import React, { Component,Fragment } from 'react'
import axios from 'axios'
import { } from 'react-router-dom';
import SunB from './Sunburst';
import data from './data'
import M from 'materialize-css';
import {} from 'materialize-css'
import Nav from '../dynnav'
//var empty = require('is-empty');

class Dash extends Component {
	notifi = () => window.M.toast({html: 'Welcome to DashBoard', outDuration:'1000', inDuration:'900', displayLength:'1800'});

    componentDidMount() {
      this.getDash()
        this.notifi()
        M.AutoInit()
      }
      constructor(){
        super()
				this.state ={
					logout:'/user/logout',
					get:'/user/',
					content:[
						{
							val:'Profile',
							link:'/newd',
						},
						{
							val:'DashBoard',
							link:'/dash',
						},
						{
							val:'Sections',
							link:'/partA',
						},
						{
							val:'TimeTable',
							link:'/time_new',
						},
						{
							val:'Master TimeTable',
							link:'/master',
						}
					],
				}
        this.getDash = this.getDash.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
      }


      getDash(){
        axios.get('/user/Dash').then(response =>{
          if(response.status===200)
          {

          }

        })
      }


render() {

		return (
			<React.Fragment>
			 <Nav content={this.state.content} get={this.state.get} logout={this.state.logout}/>
			<div className="row">
			<div className="col l12">
			<SunB data={data}
			width="700"
			height="700"
			count_member="size"
			labelFunc={(node)=>node.data.name}
			_debug={true} />
			</div>

			</div>
</React.Fragment>
		);



}
}

export default Dash
