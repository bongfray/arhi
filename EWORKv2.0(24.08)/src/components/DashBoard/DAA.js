import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'

var empty = require('is-empty');

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
      total_adminispercentage:null,
      total_academicpercentage:null,
      total_researchpercentage:null,
      ad_total_role_res:null,
      ad_total_clerical :null,
      ad_total_planning :null,
      academic_total_curricular:null,
      academic_total_cocurricular:null,
      academic_total_extracurricular:null,
      academic_total_evaluation_placementwork:null,
      research_total_publication:null,
      research_total_ipr_patents:null,
      research_total_funded_sponsored_projectes:null,
      research_total_tech_dev_consultancy:null,
      research_total_product_development:null,
      research_total_research_center_establish:null,
      research_total_research_guidnce:null,
			logout:'/user/logout',
			get:'/user/',
			content:[
				{
					val:'Profile',
					link:'/newd',
				},
				{
					val:'DashBoard',
					link:'/dash',
				},
				{
					val:'Sections',
					link:'/partA',
				},
				{
					val:'TimeTable',
					link:'/time_new',
				},
				{
					val:'Master TimeTable',
					link:'/master',
				}
			]
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

		getPercentage =() =>{
			axios.get('/user/getPercentage', {
			})
			.then(response =>{
				if(response.data){
					this.setState({
            total_adminispercentage:response.data.total_adminispercentage,
            total_academicpercentage:response.data.total_academicpercentage,
            total_researchpercentage:response.data.total_researchpercentage,
            ad_total_role_res:response.data.ad_total_role_res,
            ad_total_clerical:response.data.ad_total_clerical,
            ad_total_planning:response.data.ad_total_planning,
            academic_total_curricular:response.data.academic_total_curricular,
            academic_total_cocurricular:response.data.academic_total_cocurricular,
            academic_total_extracurricular:response.data.academic_total_extracurricular,
            academic_total_evaluation_placementwork:response.data.academic_total_evaluation_placementwork,
            research_total_publication:response.data.research_total_publication,
            research_total_ipr_patents:response.data.research_total_ipr_patents,
            research_total_funded_sponsored_projectes:response.data.research_total_funded_sponsored_projectes,
            research_total_tech_dev_consultancy:response.data.research_total_tech_dev_consultancy,
            research_total_product_development:response.data.research_total_product_development,
            research_total_research_center_establish:response.data.research_total_research_center_establish,
            research_total_research_guidnce:response.data.research_total_research_guidnce,
					});
				}
			})
		}

    fetchlogin = () =>{
      axios.get('/user/fetchdayorder', {
      })
      .then(response =>{
        if(response.data === "Not"){
          this.setState({
            redirectTo:'/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }

    componentDidMount() {
        M.AutoInit();
        this.fetchlogin();
				this.getPercentage();
    }
  render()
  {
    return(
      <React.Fragment>
      <div className="row card hoverable masterprog">
        <div className="col l1 center">
            Administrative<br /> Work Effort<br />(Hours)
        </div>
        <div className="col l3 center" style={{borderRight: '1px solid',borderLeft:'1px solid'}}>
           <span>Total Percentages : </span>{this.state.total_adminispercentage}% <span style={{fontSize:'18px',color:'red'}}>/</span> {this.state.total_adminispercentage}%
        </div>
        <div className="col l1 center">
            Academic <br/>Work Effort<br />(Hours)
        </div>
        <div className="col l3 center" style={{borderRight: '1px solid',borderLeft:'1px solid'}}>
            <span>Total Percentages :</span>{this.state.total_academicpercentage}% <span style={{fontSize:'18px',color:'red'}}>/</span> {this.state.total_academicpercentage}%
            <br />
            <span>Curricular : </span><br />
            <span>Co-Curricular : </span><br />
            <span>Extra-Curricular : </span><br />
            <span>Preparation/ Evaluation : </span><br />
        </div>
        <div className="col l1 center">
            Research<br /> Work Effort<br />(Hours)
        </div>
        <div className="col l3 center" style={{borderRight: '1px solid',borderLeft:'1px solid'}}>
           <span>Total Percentages : </span>{this.state.total_researchpercentage}% <span style={{fontSize:'18px',color:'red'}}>/</span> {this.state.total_researchpercentage}%
        </div>
      </div>
      </React.Fragment>
    )
  }
}
