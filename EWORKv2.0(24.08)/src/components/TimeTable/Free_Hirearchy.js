import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Select,actionHandler} from 'react-materialize'
import axios from 'axios'
import M from 'materialize-css'
var empty = require('is-empty');

export default class FreeS extends React.Component {
  constructor()
  {
    super()
    this.state ={
      administrativefreefield:'',
      academicfreefield:'',
      researchfreefield:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

componentDidMount()
{
  let selects = document.querySelectorAll('select');
  M.FormSelect.init(selects, {});
}

handleAcademicFreeField =(e) =>{
  this.setState({
    academicfreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}

handleAdministrativeFreeField =(e) =>{
  this.setState({
    administrativefreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}
handleResearchFreeField=(e) =>{
  this.setState({
    researchfreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}

render()
{
  console.log(this.state.academicfreefield)
  if(this.props.freefield ==="Academic")
  {
    return(
      <React.Fragment>
      <Select value={this.state.academicfreefield} onChange={this.handleAcademicFreeField}>
      <option value="" disabled selected>Select Here</option>
      <option value="CoCurricular">CoCurricular</option>
      <option value="ExtraCurricular">ExtraCurricular</option>
      <option value="Evaluation">Evaluation</option>
      <option value="Evaluation">Placement Related Work</option>
    </Select>
      </React.Fragment>
    );
  }
  else if(this.props.freefield ==="Administrative_Work")
  {
    return(
      <React.Fragment>
        <Select value={this.state.administrativefreefield} onChange={this.handleAdministrativeFreeField}>
          <option value="" disabled selected>Select Here</option>
          <option value="Roles_Responsibilities">Roles & Responsibilities</option>
          <option value="Clerical_Work">Clerical Work</option>
          <option value="Planning_Strategy">Planning / Strategy</option>
          <option value="Accounts_Finance">Accounts & Finance</option>
          <option value="DataEntry_Analysis">Data Entry & Analysis</option>
          <option value="Administrative_Other">Other</option>
        </Select>
      </React.Fragment>

    );
  }
  else if(this.props.freefield ==="Research_Work")
  {
    return(
      <React.Fragment>
        <Select value={this.state.researchfreefield} onChange={this.handleResearchFreeField}>
          <option value="" disabled selected>Select Here</option>
          <option value="Publications">Publications</option>
          <option value="IPR_Patents">IPR & Parents</option>
          <option value="funded_sponsered_project">Funded / Sponsered Project</option>
          <option value="tech_dev_consultancy">Technology Devt. & Consultancy</option>
          <option value="prod_dev">Product Development</option>
          <option value="res_center_establish">Research Center Establishment</option>
          <option value="res_guidence">Research Guidance</option>
        </Select>
      </React.Fragment>

    );
  }
  else{
    return(
      <React.Fragment>
      </React.Fragment>
    )
  }

}
}
