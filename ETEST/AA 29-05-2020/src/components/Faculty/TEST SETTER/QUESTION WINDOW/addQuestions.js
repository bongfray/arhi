import React, { Component } from 'react';
import {AppBar,Tabs,Tab} from '@material-ui/core';

import AddQuestions from './New Questions/add_questions'
import CurrentWindow from './Current Test/current_window'

export default class Questions extends Component {
  constructor()
  {
    super()
    this.state={
      value:0,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render() {

    return (
      <React.Fragment>
        <AppBar position="static" color="default">
         <Tabs
           value={this.state.value}
           indicatorColor="primary"
           textColor="primary"
           variant="fullWidth"
           aria-label="full width tabs example"
         >
           <Tab label="Setup Questions for Current Test" onClick={(e)=>this.setState({value:0})}  />
           <Tab label="Add Questions" onClick={(e)=>this.setState({value:1})} />
         </Tabs>
        </AppBar><br />
        {this.state.value === 0 ?
          <CurrentWindow props_data={this.props.props_data} fetch={this.props.fetch} refer_data={this.props.refer_data} />
          :
          <AddQuestions username={this.props.username} fetch={this.props.fetch}  props_data={this.props.props_data}  />
        }
      </React.Fragment>
    );
  }
}
