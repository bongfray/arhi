import React, { Component } from 'react';
import axios from 'axios';
import {
  Select,MenuItem,FormControl,Paper,InputLabel,Grid,TextField,
} from '@material-ui/core';

import Autocomplete from '@material-ui/lab/Autocomplete';


import MultipleChoice from './Type/option_type';
import SAQ from './Type/saq_type';
import TF from './Type/true_false';
import Image from './Type/image-based';
import MTF from './Type/match-following';
import Coding from './Type/coding_type';
import Weq from './Type/write_equation';

export default class SetUpWindow extends Component {
  constructor()
  {
    super()
    this.state={
      question_type:'',
      username:'',
      loading:true,
      skill_name:'',
      level:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.get('/etest/user/fetch_skills')
    .then( res => {
      console.log(res.data);
        this.setState({saved_skill:res.data,loading:false})
    })
    .catch( err => {
        this.setState({loading:false})
    });
  }
  setSkill=(e)=>{
    if(e === null)
    {

    }
    else
    {
      this.setState({skill_name:e.skill_name})
    }
  }

  render() {
    if(this.state.loading)
    {
      return(
        <div style={{textAlign:'center'}}>Loading....</div>
      )
    }
    else
    {
      const options = this.state.saved_skill.map(option => {
        const firstLetter = option.skill_name[0].toUpperCase();
        return {
          firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
          ...option,
        };
      });
      if(options.length  === 0)
      {
        return(
          <div>Oops !! You can not add questions...</div>
        )
      }
      else
      {
    return (
      <React.Fragment>
          <Grid container>
            <Grid item xs={4} sm={4} style={{padding:'4px'}}>
               <Paper elevation={2} style={{padding:'20px'}}>
                   <Autocomplete
                     id="grouped-demo"
                     options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                     groupBy={option => option.firstLetter}
                     getOptionLabel={option => option.skill_name}
                     onChange={(event, value) => this.setSkill(value)}
                     style={{ width: '100%' }}
                     renderInput={params => (
                       <TextField {...params} label="Select Skill" variant="outlined" fullWidth />
                     )}
                   />
               </Paper>
            </Grid>
            <Grid item xs={4} sm={4} style={{padding:'4px'}}>
                      <Paper elevation={2} style={{padding:'20px'}}>
                       <FormControl variant="outlined" style={{width:'100%'}}>
                          <InputLabel id="demo-simple-select-outlined-label">
                            Select Level
                          </InputLabel>
                          <Select
                            labelId="level"
                            id="level"
                            value={this.state.level} onChange={(e)=>this.setState({level:e.target.value})}
                            labelWidth={90}
                          >
                            <MenuItem value="Begineer">Beginner</MenuItem>
                            <MenuItem value="Intermediate">Intermediate</MenuItem>
                            <MenuItem value="Advanced">Advanced</MenuItem>
                          </Select>
                        </FormControl>
                      </Paper>
            </Grid>
            <Grid item xs={4} sm={4} style={{padding:'4px'}}>
            {(this.state.level && this.state.skill_name) &&
              <Paper elevation={3} style={{padding:'5px'}}>
                 <FormControl style={{width:'100%'}}>
                   <InputLabel id="type">Type of questions</InputLabel>
                     <Select
                       labelId="type"
                       id="type"
                       name="type" value={this.state.question_type} onChange={(e)=>this.setState({question_type:e.target.value})}
                     >
                     <MenuItem value="Choose From Options">Choose From Options</MenuItem>
                     <MenuItem value="Write Answers">Write Answers</MenuItem>
                     <MenuItem value="True-False">True-False</MenuItem>
                     <MenuItem value="Match the Following">Match the Following</MenuItem>
                     <MenuItem value="Write Equation">Write Equation</MenuItem>
                     <MenuItem value="Image Based">Image Based</MenuItem>
                     <MenuItem value="Coding Questions">Coding Questions</MenuItem>
                     </Select>
                  </FormControl>
               </Paper>
               }
            </Grid>
      </Grid>

       {this.state.question_type === 'Choose From Options' && <MultipleChoice state={this.state} props={this.props} />}
       {this.state.question_type === 'Write Answers' && <SAQ state={this.state} props={this.props} />}
       {this.state.question_type === 'True-False' && <TF state={this.state} props={this.props} />}
       {this.state.question_type === 'Match the Following' && <MTF state={this.state} props={this.props} />}
       {this.state.question_type === 'Write Equation' && <Weq state={this.state} props={this.props} />}
       {this.state.question_type === 'Image Based' && <Image state={this.state} props={this.props} />}
       {this.state.question_type === 'Coding Questions' && <Coding state={this.state} props={this.props} />}

      </React.Fragment>
    );
   }
  }
  }
}
