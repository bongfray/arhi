import React, { Component ,useEffect } from 'react';
import axios from 'axios';
import SearchIcon from '@material-ui/icons/Search';
import DeleteIcon from '@material-ui/icons/Delete';

import {  makeStyles } from '@material-ui/core/styles';
import {Paper,CardContent,Grid,InputBase,Card,Divider,CardHeader,Checkbox,Dialog,DialogTitle,DialogActions,DialogContent,
  IconButton,CardActionArea,TablePagination,Button,Typography,
} from '@material-ui/core';

export default class GenerateQuestions extends Component {
  constructor()
  {
    super()
    this.state={
      fetching: true,
      generateset:[],
      finalarray:[],
      q_count:0,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
      this.fetchQuestions();
  }

  fetchQuestions=()=>{
    axios.post('/etest/user/fetch_questions',
    {action:'question_set',course_id:this.props.props.props_data.state.course_id})
    .then( res => {
      let questionSet = [];
        this.props.state.data.setdata.map(content=>{
          let questions=[];
           questions =  res.data.filter(item=>((item.question_type ===  this.props.state.data.question_type)
           && (item.mark === parseInt(content.mark_for_combination))
            && (item.level ===  this.props.state.data.level)));
             let n =0 ;
              n=n + parseInt(content.no_of_question);
              let com_type = content.mark_for_combination;
           questionSet.push({total:n,questions:questions,com_type:com_type});
        })
        this.setState({generateset:questionSet,fetching:false,})
    })
    .catch( err => {

    });
  }
  saveSetup=()=>{
    this.setState({fetching:true})
    axios.post('/etest/user/save_testsetup',{data:this.state,props_data:this.props.props.props_data,
      refer_data:this.props.props.refer_data})
    .then( res => {
      this.setState({fetching:false})
    })
    .catch( err => {
      this.setState({fetching:false})
    })
  }
  render() {
    if(this.state.fetching)
    {
      return (
        <div>
          Loading...
        </div>
      );
    }
    else {
      return (
        <React.Fragment>
        <Grid container spacing={1}>
          {this.state.generateset.map((content,index)=>{
            return(
              <Grid item xs={12} sm={12} lg={12} xl={12} md={12} key={index}>
                <EnhancedTable data={content.questions} q_count={content.total} com_type={content.com_type} generateset={this.props.props.refer_data.questions}
                 state={this.props.state} props={this.props.props} fetch={this.props.fetch} />
              </Grid>
            )
          })}
        </Grid>
        </React.Fragment>
      );
    }
  }
}

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: '100%',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

 function EnhancedTable(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [preview, setPreview] = React.useState(false);
  const [fetching, setFetching] = React.useState(true);
  const [q_set, setQ] = React.useState([]);
  const [checkedItems, setChecked] = React.useState(new Map());
  const [rowsPerPage, setRowsPerPage] = React.useState(6);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };



  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const setData=(e,data,index)=>{
      const item = parseInt(data.serial);
      const isChecked = e.target.checked;
      let setup = checkedItems.set(item, isChecked)
      setChecked(setup);
      let dataset=q_set.concat(data.serial)
      setQ(dataset);
  }
  const deleteQ =(e,row,index,action)=>{
    if(action ===  true)
    {
      const new_set = q_set.filter(item=>item !== row);
      const updateChecked = typeof (checkedItems.get(parseInt(row))) === "undefined" ? true : false;
      let setup = checkedItems.set(parseInt(row), updateChecked);
      setChecked(setup);
      setPreview(false)
      setQ(new_set);
    }
    else {
      const new_set = q_set.filter(item=>item !== row.serial);
      const updateChecked = typeof (checkedItems.get(parseInt(row.serial))) === "undefined" ? true : false;
      let setup = checkedItems.set(parseInt(row.serial), updateChecked);
      setChecked(setup);
      setPreview(false)
      setQ(new_set);
    }
  }
  const saveQuestion=()=>{
    axios.post('/etest/user/save_choice_setup',{part:props.state.refer_part,question_set:q_set,props_data:props.props.props_data,
      refer_data:props.props.refer_data})
      .then( res => {
          if(res.data === 'ok')
          {
            props.fetch();
            setPreview(false)
          }
      });
  }
  if(props.props.refer_data.questions.length>0)
  {
    let filterdata;
    props.props.refer_data.questions.map(content=>{
        if(content.part ===  props.state.refer_part)
        {
          filterdata =content.question_set;
          filterdata.map(row=>{
            const item = parseInt(row);
            let setup = checkedItems.set(item, true)
            setChecked(setup);
            setQ(filterdata);
          })
        }
    })
  }


  return (
    <div className={classes.root}>
     {preview &&
       <Dialog
       fullWidth
       maxWidth={'lg'}
         open={preview}
         keepMounted
         onClose={()=>setPreview(false)}
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title" align="center">Questions You Added</DialogTitle>
          <DialogContent>
            <Button variant="outlined" color="primary" onClick={saveQuestion} disabled={(q_set.length ===  props.q_count) ? false : true}
            >Save Questions</Button><br /><br />
              <Paper elevation={3} className={classes.paper} style={{padding:'10px'}}>
                  <Grid container spacing={1}>
                      {q_set.map((row, index) => {
                          return (
                            <Grid item xs={2} sm={2} key={index}>
                            <Card>
                              <CardActionArea>
                              <CardHeader
                                  action={
                                    <IconButton onClick={e => deleteQ(e,row,index,true)}>
                                      <DeleteIcon />
                                    </IconButton>
                                      }
                                />
                                <CardContent>
                                  Question No - {row}
                                </CardContent>
                              </CardActionArea>
                            </Card>
                         </Grid>
                          );
                        })}
                  </Grid>
              </Paper>
          </DialogContent>
        <DialogActions>
        <Button onClick={()=>setPreview(false)} color="primary">
          CLOSE
        </Button>
        </DialogActions>
      </Dialog>
     }
      <Paper elevation={3} className={classes.paper} style={{padding:'10px'}}>
        <Typography align="center" variant="h6">{props.com_type} Marker Questions</Typography>
        <Grid container spacing={1}>
          <Grid item xs={6} sm={6}>
              <Paper style={{width:'100%'}} component="form" className={classes.root}>
                <InputBase
                  fullWidth
                  className={classes.input}
                  placeholder="Search Only Certain Keywords in a Question"
                  inputProps={{ 'aria-label': 'search' }}
                />
                <Divider className={classes.divider} orientation="vertical" />
                <IconButton className={classes.iconButton} aria-label="search">
                  <SearchIcon />
                </IconButton>
              </Paper>
          </Grid>
          <Grid item xs={4} sm={4}>
            <Paper elevation={3} style={{padding:'11px'}}>
              <Typography align="center">Choosed Question Count - {q_set.length} / {props.q_count}</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2} sm={2}>
            <Button onClick={()=>setPreview(true)} disabled={(q_set.length ===  props.q_count) ? false : true}
             variant="outlined" style={{float:'right'}} color="secondary">Preview</Button>
          </Grid>
        </Grid>
        <br />
        <Divider />
        <br />
          <Grid container spacing={1}>
              {props.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <Grid item xs={4} sm={4} key={index}>
                    <Card>
                      <CardActionArea>
                      <CardHeader
                          action={
                            <React.Fragment>
                             {(!!checkedItems.get(row.serial)) ?
                               <IconButton onClick={e => deleteQ(e,row,index,false)}>
                                 <DeleteIcon />
                               </IconButton>
                               :
                               <Checkbox
                               checked={!!checkedItems.get(row.serial)} disabled={(q_set.length ===  props.q_count) ? true : false}
                                 onChange={e => setData(e,row,index)}
                                 inputProps={{ 'aria-label': 'primary checkbox' }}
                                />
                             }
                            </React.Fragment>
                              }
                        />
                        <CardContent>
                          {row.question && <React.Fragment><b>Question</b>  -  {row.question}<br /><br /></React.Fragment>}
                          {(row.options.length>0 && !(row.optionBs.length>0)) &&
                            <React.Fragment>
                            <Typography align="center" >Options</Typography><br />
                              <Grid container spacing={1}>
                                {row.options.map((content,index)=>{
                                  return(
                                    <Grid item xs={6} sm={6} key={index}><b>Option {index+1}</b> - {content}</Grid>
                                  )
                                })}
                              </Grid><br />
                            </React.Fragment>
                          }

                          {(row.options.length>0 && row.optionBs.length>0 && row.answers.length>0 )&&
                            <React.Fragment>
                              <Grid container spacing={1}>
                                <Grid item xs={6} sm={6}>
                                  <Typography align="center"><b>Part - A</b></Typography>
                                  {row.options.map((content,index)=>{
                                    return(
                                      <Typography align="center" key={index}>{content}</Typography>
                                    )
                                  })}
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <Typography align="center"><b>Part - B</b></Typography>
                                  {row.optionBs.map((content,index)=>{
                                    return(
                                      <Typography align="center" key={index}>{content}</Typography>
                                    )
                                  })}
                                </Grid>
                              </Grid>
                              <hr />
                              <Typography align="center"><b>Answers</b></Typography>
                              {row.answers.map((content,index)=>{
                                return(
                                <Grid container spacing={1} key={index}>
                                  <Grid item xs={6} sm={6}>Part A{index+1}</Grid>
                                  <Grid item xs={6} sm={6}>{content}</Grid>
                                  </Grid>
                                )
                              })}
                              <br />
                            </React.Fragment>
                          }
                          {row.answer && <React.Fragment><b>Answer</b> - {row.answer} </React.Fragment>}
                        </CardContent>
                      </CardActionArea>
                    </Card>
                 </Grid>
                  );
                })}
          </Grid>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
