import React from 'react'
import { addStyles, EditableMathField } from 'react-mathquill'
import
{Dialog,Button,Grid,DialogActions,
  Divider,DialogContent,DialogTitle,
  Snackbar,Zoom,TextField
} from '@material-ui/core';

addStyles()
 
export default class Math extends React.Component {
  constructor(props) {
    super(props)
 
    this.state = {
      latex: '',
    }
    this.handleSubmit = this.handleSubmit.bind(this)

  }
  handleSubmit(event) {
    event.preventDefault()
    var s = this.state.latex
    console.log(s)
}
  
 
  render() {
    return (
        <React.Fragment>

      <EditableMathField
        latex={this.state.latex} // Initial latex value for the input field
        onChange={mathField => {
            this.setState({ latex: mathField.latex() })
        }}
      />
      <Button fullWidth
                                  style={{backgroundColor:'#009688',color:'white'}}
                                  onClick={this.handleSubmit}>Login</Button>
        </React.Fragment>
      
    )
  }
}