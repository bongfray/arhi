import React, { Component } from 'react';
import Etest from './etestfront.png';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import {Grid,Zoom} from '@material-ui/core';

export default class Nstart extends Component {
    render() {
        const checked = true;
        return (
            <Grid container>
                <Grid item xs={1} sm={3}></Grid>
                <Grid item xs={10} sm={6}>
                    <Zoom
                    in={true}
                    {...(checked ? { timeout: 700 } : {})}
                    >
                        <Paper elevation={3} style={{padding:'20px',borderRadius:'10px',marginTop:'170px'}}>
                            <Grid container justify="center">
                                <img src={Etest} alt="" width="135px"/>
                            </Grid>
                            <Grid container spacing={1} style={{marginBottom:'10px',marginTop:'20px'}}>
                                <Grid item xs={5} sm={5}>
                                    <Link to="/faculty" style={{textDecoration:'none'}}>
                                        <Paper style={{color:'white',padding:'10px',textAlign:'center',background:'linear-gradient(to bottom left, #ff0066 0%, #00ff99 100%)'}}>
                                        Faculty
                                        </Paper>
                                    </Link>
                                </Grid>
                                <Grid item xs={2} sm={2}></Grid>
                                <Grid item xs={5} sm={5}>
                                    <Link to="/student" style={{textDecoration:'none'}}>
                                        <Paper style={{color:'black',padding:'10px',textAlign:'center',background: 'linear-gradient(to bottom left, #66ffff 0%, #ffff99 100%)'}}>
                                        Student
                                        </Paper>
                                    </Link>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Zoom>
                </Grid>
                <Grid item xs={1} sm={3}></Grid>
            </Grid>
            
        )
    }
}
