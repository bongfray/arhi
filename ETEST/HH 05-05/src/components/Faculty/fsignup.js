import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Select from '@material-ui/core/Select';
import Divider from '@material-ui/core/Divider'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class FSignup extends Component {
    constructor(){
        super();
        this.state={
            open:false,
            title:'',
            name:'',
            username:'',
            mailid:'',
            phone:'',
            password:'',
            cnf_password:'',
            campus:'',
            dept:'',
            desgn:'',
        }
    }
    handleClickOpen = () => {
        this.setState({
            open:true
        })
    }

    handleClose= () => {
        this.setState({
            open:false
        })
    }

    handleField = (e) =>{
        this.setState({
            [e.target.name] : e.target.value,
        })
    }
    render() {
        return (
            <div>
                <Button fullWidth style={{backgroundColor:'#009688',color:'white'}} onClick={this.handleClickOpen}>
                    Register
                </Button>
                <Dialog fullWidth={true} maxWidth={'lg'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title" align="center">Faculty Registration</DialogTitle>
                    <Divider/>
                        <DialogContent>
                            <form className="form-con">
                                <Grid container spacing={2}>
                                        <Grid item xs={6} sm={2}>
                                                <FormControl style={{width:'100%'}}>
                                                    <InputLabel id="title">Salutation</InputLabel>
                                                        <Select
                                                            labelId="title"
                                                            id="title"
                                                            name="title" value={this.state.title} onChange={this.handleField}
                                                        >
                                                        <MenuItem value="Mr.">Mr.</MenuItem>
                                                        <MenuItem value="Mrs.">Mrs.</MenuItem>
                                                        <MenuItem value="Miss.">Miss.</MenuItem>
                                                        <MenuItem value="Dr.">Dr.</MenuItem>
                                                        </Select>
                                                </FormControl>
                                        </Grid>
								        <Grid item xs={6} sm={6}>
                                            <TextField id="name" name="name" type="text" fullWidth
                                            value={this.state.name} onChange={this.handleField} label="Name" />
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
                                            <TextField id="fac_id" name="fac_id" type="text" fullWidth
                                            value={this.state.username} onChange={this.handleField} label="Official ID" />
                                        </Grid>
                                        <Grid item xs={12} sm={8}>
                                            <TextField id="email" name="email" type="text" fullWidth
                                            value={this.state.mailid} onChange={this.handleField} label="Official Mail ID" />
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
                                            <TextField id="phone" name="phone" type="text" fullWidth
                                            value={this.state.phone} onChange={this.handleField} label="Mobile No." />
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="campus">Campus</InputLabel>
													<Select
														labelId="campus"
														id="campus"
														value={this.state.campus} name="campus" onChange={this.handleField}
													>
													<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
													<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
													<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
													<MenuItem value="NCR Campus">NCR Campus</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={4}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="dept">Department</InputLabel>
													<Select
														labelId="dept"
														id="dept"
														value={this.state.dept} name="dept" onChange={this.handleField}
													>
													<MenuItem value="cse">Computer Science</MenuItem>
													<MenuItem value="mech">Mechanical</MenuItem>
													<MenuItem value="eee">Electrical</MenuItem>
													<MenuItem value="swe">Software</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={4}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="desgn">Designation</InputLabel>
													<Select
														labelId="desgn"
														id="desgn"
														value={this.state.desgn} name="desgn" onChange={this.handleField}
													>
													<MenuItem value="prof">Professor</MenuItem>
													<MenuItem value="asso_prof">Associate Professor</MenuItem>
													<MenuItem value="assi_prof">Assistant Professor</MenuItem>
													<MenuItem value="hod">Head Of Department</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField id="pswd" name="pswd" type="password" fullWidth
                                            value={this.state.password} onChange={this.handleField} label="Password" />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField id="c_pswd" name="c_pswd" type="text" fullWidth
                                            value={this.state.cnf_password} onChange={this.handleField} label="Confirm Password" />
                                        </Grid>

                               </Grid>
                            </form>
                        </DialogContent>
                        <Divider/>
                        <DialogActions>
                            
                        <Button onClick={this.handleClose} color="primary" style={{backgroundColor:'red',color:'white'}}>
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary" style={{backgroundColor:'#009688',color:'white'}}>
                            Submit
                        </Button>
                        </DialogActions>
                    </Dialog>
                
            </div>
        )
    }
}
