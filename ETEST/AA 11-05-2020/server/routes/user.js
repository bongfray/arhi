const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const User = require('../database/models/Faculty/user')
const passport = require('../passport/fac_pass')
const crypto = require('crypto')
require("datejs")

console.log('Connected to Server !!')

/**-------------Signup---------- */
router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

router.post('/signup', (req, res) => {
    User.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
                title: req.body.title,
                name: req.body.name,
                username: req.body.username,
                mailid: req.body.mailid,
                phone: req.body.phone,
                campus: req.body.campus,
                dept: req.body.dept,
                desgn: req.body.desgn,
                password: req.body.password,
              })
            newUser.save((err, savedUser) => {
              if(err)
              {

              }
              else if (savedUser) {
                var succ= {
                    succ: "Successful SignedUP"
                  };
                  res.send(succ);
              }
            })
        }
    })
})
module.exports = router
