import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Select from '@material-ui/core/Select';
import Divider from '@material-ui/core/Divider'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class FSignup extends Component {
    constructor(){
        super();
        this.state={
            open:false,
            name:'',
            username:'',
            mailid:'',
            phone:'',
            campus:'',
            degree:'',
            dept:'',
            year:'',
            sem:'',
            batch:'',
            batch_no:'',
            password:'',
            cnf_pswd:'',
            
        }
    }
    handleClickOpen = () => {
        this.setState({
            open:true
        })
    }

    handleClose= () => {
        this.setState({
            open:false
        })
    }

    handleField = (e) =>{
        this.setState({
            [e.target.name] : e.target.value,
        })
    }
    render() {
        return (
            <div>
                <Button fullWidth style={{backgroundColor:'#009688',color:'white'}} onClick={this.handleClickOpen}>
                    Register
                </Button>
                <Dialog fullWidth={true} maxWidth={'lg'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title" align="center">Faculty Registration</DialogTitle>
                    <Divider/>
                        <DialogContent>
                            <form className="form-con">
                                <Grid container spacing={2}>
                                        
								        <Grid item xs={12} sm={6}>
                                            <TextField id="name" name="name" type="text" fullWidth
                                            value={this.state.name} onChange={this.handleField} label="Name" />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField id="reg_no" name="reg_no" type="text" fullWidth
                                            value={this.state.username} onChange={this.handleField} label="Registration No." />
                                        </Grid>
                                        <Grid item xs={12} sm={5}>
                                            <TextField id="email" name="email" type="text" fullWidth
                                            value={this.state.mailid} onChange={this.handleField} label="Official Mail ID" />
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
                                            <TextField id="phone" name="phone" type="text" fullWidth
                                            value={this.state.phone} onChange={this.handleField} label="Mobile No." />
                                        </Grid>
                                        <Grid item xs={12} sm={3}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="campus">Campus</InputLabel>
													<Select
														labelId="campus"
														id="campus"
														value={this.state.campus} name="campus" onChange={this.handleField}
													>
													<MenuItem value="Kattankulathur Campus">Kattankulathur Campus</MenuItem>
													<MenuItem value="Ramapuram Campus">Ramapuram Campus</MenuItem>
													<MenuItem value="Vadapalani Campus">Vadapalani Campus</MenuItem>
													<MenuItem value="NCR Campus">NCR Campus</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={6}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="degree">Degree</InputLabel>
													<Select
														labelId="degree"
														id="degree"
														value={this.state.degree} name="degree" onChange={this.handleField}
													>
													<MenuItem value="btech">B.Tech</MenuItem>
													<MenuItem value="bca">BCA</MenuItem>
													<MenuItem value="mtech">M.Tech</MenuItem>
													<MenuItem value="mba">MBA</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={6}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="dept">Department</InputLabel>
													<Select
														labelId="dept"
														id="dept"
														value={this.state.dept} name="dept" onChange={this.handleField}
													>
													<MenuItem value="cse">Computer Science</MenuItem>
													<MenuItem value="mech">Mechanical</MenuItem>
													<MenuItem value="eee">Electrical</MenuItem>
													<MenuItem value="swe">Software</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={3}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="year">Year</InputLabel>
													<Select
														labelId="year"
														id="year"
														value={this.state.year} name="year" onChange={this.handleField}
													>
													<MenuItem value="first">1</MenuItem>
													<MenuItem value="Second">2</MenuItem>
													<MenuItem value="Third">3</MenuItem>
													<MenuItem value="Fourth">4</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={3}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="sem">Semester</InputLabel>
													<Select
														labelId="sem"
														id="sem"
														value={this.state.sem} name="sem" onChange={this.handleField}
													>
													<MenuItem value="first">1</MenuItem>
													<MenuItem value="second">2</MenuItem>
													<MenuItem value="third">3</MenuItem>
													<MenuItem value="fourth">4</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={3}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="batch">Batch</InputLabel>
													<Select
														labelId="batch"
														id="batch"
														value={this.state.batch} name="batch" onChange={this.handleField}
													>
													<MenuItem value="A">A</MenuItem>
													<MenuItem value="B">B</MenuItem>
													<MenuItem value="C">C</MenuItem>
													<MenuItem value="D">D</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={3}>
											<FormControl style={{width:'100%'}}>
												<InputLabel id="batch_no">Batch No.</InputLabel>
													<Select
														labelId="batch_no"
														id="batch_no"
														value={this.state.batch_no} name="batch_no" onChange={this.handleField}
													>
													<MenuItem value="A1">A1</MenuItem>
													<MenuItem value="B1">B1</MenuItem>
													<MenuItem value="C3">C3</MenuItem>
													<MenuItem value="D3">D3</MenuItem>
													</Select>
											 </FormControl>
									    </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField id="pswd" name="pswd" type="password" fullWidth
                                            value={this.state.password} onChange={this.handleField} label="Password" />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField id="c_pswd" name="c_pswd" type="text" fullWidth
                                            value={this.state.cnf_password} onChange={this.handleField} label="Confirm Password" />
                                        </Grid>

                               </Grid>
                            </form>
                        </DialogContent>
                        <Divider/>
                        <DialogActions>
                            
                        <Button onClick={this.handleClose} color="primary" style={{backgroundColor:'red',color:'white'}}>
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary" style={{backgroundColor:'#009688',color:'white'}}>
                            Submit
                        </Button>
                        </DialogActions>
                    </Dialog>
                
            </div>
        )
    }
}
