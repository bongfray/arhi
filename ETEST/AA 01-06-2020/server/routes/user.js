const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const passport = require('../passport/fac_pass')
const crypto = require('crypto')

const User = require('../database/models/Faculty/user')
const TestHistory = require('../database/models/Faculty/test_history')
const Skills = require('../database/models/Faculty/skills')
const TestRef = require('../database/models/Faculty/test_ref')
const Question = require('../database/models/Faculty/question')

require("datejs")

console.log('Connected to Server !!')

/**-------------Signup---------- */
router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})
router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

router.post(
    '/faclogin',(req, res, next) => {
      passport.authenticate('local', function(err, user, info) {
        if (err) {
           return next(err);
         }
        if (!user) {
            res.status(401);
            res.send(info.message);
            return;
        }
        req.logIn(user, function(err) {
          if (err) {
            return next(err);
          }
          var userInfo = {
              username: req.user.username,
          };
          res.send(userInfo);
        });
      })(req, res, next);
    }
)

router.post('/signup', (req, res) => {
    User.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
                title: req.body.title,
                name: req.body.name,
                username: req.body.username,
                mailid: req.body.mailid,
                phone: req.body.phone,
                campus: req.body.campus,
                dept: req.body.dept,
                desgn: req.body.desgn,
                password: req.body.password,
              })
            newUser.save((err, savedUser) => {
              if(err)
              {

              }
              else if (savedUser) {
                var succ= {
                    succ: "Successful SignedUP"
                  };
                  res.send(succ);
              }
            })
        }
    })
})

router.post('/add_test', function(req,res) {
          var trans = new TestHistory(req.body.data);
          trans.save((err, savedUser) => {
            var succ= {
              succ: "Done"
            };
            res.send(succ);
          })
});

router.post('/edit_test', function(req,res) {
  TestHistory.updateOne({$and: [{ serial: req.body.data.serial},
    {username: req.user.username}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })

});



router.post('/fetch_test',function(req,res) {
  if(req.user)
  {
    TestHistory.find({$and: [{type:req.body.type},{layout:req.body.layout},{username: req.user.username}]}, function(err, docs){
      if(err)
      {
        res.status(500).send('error')
      }
      else
      {
        res.send(docs)
      }
  });
}
else {
  let data =[]
  res.send(data)
}
})
router.post('/fetch_test_to_edit',function(req,res) {
  TestHistory.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
    if(err)
    {
      res.status(500).send('error')
    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/delete_test',function(req,res) {
  TestHistory.deleteOne({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
});


router.post('/add_questions', function(req,res) {
  console.log(req.body);
          var trans = new Question(req.body.data);
          trans.save((err, savedUser) => {
            var succ= {
              succ: "Done"
            };
            res.send(succ);
          })
});

router.post('/edit_questions', function(req,res) {
  Question.updateOne({$and: [{ serial: req.body.data.serial},
    {username: req.user.username}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })

});



router.post('/fetch_questions',function(req,res) {
  if(req.user)
  {
    console.log(req.body);
    let query;
    if(req.body.admin_action === true)
    {
      query = {$and: [{skill_name:req.body.skill_name},{level:req.body.level},{question_type:req.body.question_type}]}
    }
    else if(req.body.action  ===  'question_set')
    {
      query = {$and: [{course_id:req.body.course_id},{username: req.user.username}]}
    }
    else {
      query = {$and: [{skill_name:req.body.skill_name},{level:req.body.level},{question_type:req.body.question_type},
        {username: req.user.username}]}
    }
    Question.find(query, function(err, docs){
      if(err)
      {
        res.status(500).send('error')
      }
      else
      {
        res.send(docs)
      }
  });
}
else {
  let data =[]
  res.send(data)
}
})

router.post('/fetch_part_of_questions',function(req,res) {
  if(req.user)
  {
    console.log(req.body);
    let query;
    if(req.body.admin_action === true)
    {
      query = {$and: [{skill_name:req.body.skill_name},{level:req.body.level},{question_type:req.body.question_type}]}
    }
    else if(req.body.action  ===  'question_set')
    {
      query = {$and: [{course_id:req.body.course_id}]}
    }
    else {
      query = {$and: [{skill_name:req.body.skill_name},{level:req.body.level},{question_type:req.body.question_type},
        {username: req.user.username}]}
    }
    Question.find(query, function(err, docs){
      if(err)
      {
        res.status(500).send('error')
      }
      else
      {
        res.send(docs)
      }
  });
}
else {
  let data =[]
  res.send(data)
}
})

router.post('/fetch_questions_to_edit',function(req,res) {
  Question.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
    if(err)
    {
      res.status(500).send('error')
    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/delete_questions',function(req,res) {
  Question.deleteOne({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
});

router.get('/fetch_skills',function(req,res) {
  Skills.find({},function(err,data){
    if(err)
    {
      res.status(500).send('error')
    }
    else {
      res.send(data)
    }
  })
});


router.post('/check_availability',function(req,res) {
  Question.find({$and:[{course_id:req.body.props_data.state.course_id},
    {level:req.body.data.level},{question_type:req.body.data.question_type}]},function(err,data){
    if(err)
    {
      res.status(500).send('error')
    }
    else {
      let count = 0;
       Array.apply(null, {length:parseInt(req.body.data.q_combination)}).map((content,index)=>{
        let dataset = data.filter(item=>(item.mark === parseInt(req.body.data['mark_for_combination'+(index+1)])))
        if((parseInt(req.body.data['no_of_question'+(index+1)]))>(dataset.length))
        {

        }
        else {
          count = count+1;
        }
     })

     if(count === (parseInt(req.body.data.q_combination)))
     {
       res.send('ok')
     }
     else {
           res.send('no')
     }
    }
  })
});

router.post('/add_combination',function(req,res) {
  TestHistory.updateOne({$and:[{_id:req.body.refer_data._id},{username:req.body.props_data.state.username},
    {type:req.body.refer_data.type}]},{$set:{combination:req.body.data.combination,question_setup:req.body.question_setup}},function(err,data){
    if(err)
    {
      res.status(500).send('error')
    }
    else {
      if(data.nModified === 1)
      {
        res.send('ok')
      }
      else {
        res.send('no')
      }
    }
  })
});


router.post('/delete_combination',function(req,res) {
  TestHistory.findOne({$and:[{_id:req.body.refer_data._id},{username:req.body.props_data.state.username},
    {type:req.body.refer_data.type}]},function(err,data){
    if(err)
    {
      res.status(500).send('error')
    }
    else {
       let combination = data.combination.filter((item, j) => req.body.index !== j);
       TestHistory.updateOne({$and:[{_id:req.body.refer_data._id},{username:req.body.props_data.state.username},
         {type:req.body.refer_data.type}]},{$set:{combination:combination}},function(err1,data1){
         if(err1)
         {
           res.status(500).send('error')
         }
         else {
           if(data1.nModified === 1)
           {
             res.send('ok')
           }
           else {
             res.send('no')
           }
         }
       })
    }
  })
});

router.post('/save_random_testsetup',function(req,res) {
  TestHistory.updateOne({$and:[{_id:req.body.refer_data._id},{username:req.body.props_data.state.username},
    {type:req.body.refer_data.type}]},{$set:{questions:req.body.data.generateset}},function(err,data){
    if(err)
    {
      res.status(500).send('error')
    }
    else {
      if(data.nModified === 1)
      {
        var datsets = new TestRef({
          exam_details:req.body.refer_data,
          questions:req.body.data.finalarray,
        })
        datsets.save((err1,done)=>{
          if(err1)
          {
            res.status(500).send('error')
          }
          else
          {
            res.send('ok')
          }
        })

      }
      else {
        res.send('no')
      }
    }
  })
});


router.post('/save_choice_setup',function(req,res) {
  TestHistory.updateOne({$and:[{_id:req.body.refer_data._id},{username:req.body.props_data.state.username},
    {type:req.body.refer_data.type}]},{questions:req.body.questions},function(err,data){
    if(err)
    {
      res.status(500).send('error')
    }
    else {
      if(data.nModified === 1)
      {
        TestRef.findOne({exam_id:req.body.refer_data.serial},(error,found)=>{
          if(error)
          {

          }
          else if(found)
          {
            TestRef.updateOne({exam_id:req.body.refer_data.serial},
              {questions:req.body.questions},function(err2,data2){
              if(err)
              {
                res.status(500).send('error')
              }
              else {
                if(data2.nModified === 1)
                {
                  res.send('ok')
                }
                else {
                  res.send('no')
                }
              }
            })
          }
          else {
            var datsets = new TestRef({
              exam_id:req.body.refer_data.serial,
              exam_details:req.body.refer_data,
              questions:req.body.questions,
            })
            datsets.save((err1,done)=>{
              if(err1)
              {
                //console.log('error here');
                res.status(500).send('error')
              }
              else
              {
                res.send('ok')
              }
            })
          }
        })
      }
      else {
        res.send('no')
      }
    }
  })
});


module.exports = router
