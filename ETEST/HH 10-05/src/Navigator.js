import React, { Component } from 'react';
import Etest from './etestfront.png';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import {Grid,Zoom} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import Typography from '@material-ui/core/Typography'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';


export default class Nstart extends Component {
    constructor(){
        super();
        this.state={
            show_modal:false,
        }
    }
    showValidation=()=>{
		this.setState({show_modal:!this.state.show_modal})
	}
    render() {
        const checked = true;
        return (
            <Grid container>
                <Grid item xs={1} sm={3}></Grid>
                <Grid item xs={10} sm={6}>
                    <Zoom
                    in={true}
                    {...(checked ? { timeout: 700 } : {})}
                    >
                        <Paper elevation={3} style={{padding:'20px',borderRadius:'10px',marginTop:'170px'}}>
                            <Grid container justify="center">
                                <img src={Etest} alt="" width="135px"/>
                            </Grid>
                            <Grid container spacing={1} style={{marginBottom:'10px',marginTop:'20px'}}>
                                <Grid item xs={5} sm={5}>
                                    <Link to="/faculty" style={{textDecoration:'none'}}>
                                        <Paper style={{color:'white',padding:'10px',textAlign:'center',background:'linear-gradient(to bottom left, #ff0066 0%, #00ff99 100%)'}}>
                                        Faculty
                                        </Paper>
                                    </Link>
                                </Grid>
                                <Grid item xs={2} sm={2}>
                                    <Grid container justify="center" alignItems="center">
                                        <InfoIcon style={{color:'#009688',cursor:'pointer'}} onClick={this.showValidation}/>
                                        <React.Fragment>
												<Dialog style={{userSelect:'none'}}
                                                fullWidth={true} maxWidth={'sm'}
                                                open={this.state.show_modal}
                                                onClick={this.showValidation}
                                                aria-labelledby="alert-dialog-title"
                                                aria-describedby="alert-dialog-description"
                                                >
                                                <DialogTitle id="alert-dialog-title">
                                                    <Grid container>
                                                        <Grid item xs={12} sm={12} xl={12}>
                                                            <Grid container justify="center" style={{padding:'8px',color: 'white',borderRadius:'10px 10px 0px 0px',display: 'block',backgroundColor:'#009688'}}>            
                                                                <Typography variant="h5" color="secondary" align="center" style={{color: 'white',borderRadius:'10px 10px 0px 0px',display: 'block',backgroundColor:'#009688'}}>About eTest</Typography>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </DialogTitle>
                                                <DialogContent>
                                                    <DialogContentText id="alert-dialog-description">
                                                        <Typography variant="button" display="block" gutterBottom>
                                                        </Typography>
                                                    </DialogContentText>
                                                </DialogContent>
                                                <DialogActions>
                                                    <Grid container justify="center">
                                                        <CloseIcon onClick={this.showValidation} style={{color:'red',cursor:'pointer',border:'1px solid red',borderRadius:'15px'}}/>
                                                    </Grid>
                                                </DialogActions>
                                                </Dialog>
                                            </React.Fragment>
                                    </Grid>
                                </Grid>
                                <Grid item xs={5} sm={5}>
                                    <Link to="/student" style={{textDecoration:'none'}}>
                                        <Paper style={{color:'black',padding:'10px',textAlign:'center',background: 'linear-gradient(to bottom left, #66ffff 0%, #ffff99 100%)'}}>
                                        Student
                                        </Paper>
                                    </Link>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Zoom>
                </Grid>
                <Grid item xs={1} sm={3}></Grid>
            </Grid>
            
        )
    }
}
