import React, { Component } from 'react';
import Modal from '../forget'
import { Redirect } from 'react-router-dom'
import Register from './fsignup'
import
{Dialog,Button,Grid,DialogActions,
  Divider,DialogContent,DialogTitle,
  Snackbar,Zoom,TextField
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import Navbar from '../dynnav';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class Fcred extends Component {
    constructor(){
        super();
        this.state={
            username:'',
            password:'',
            snack_open:false,
            alert_type:'',
            snack_msg:'',
            redirect:null,

        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)
    }
    componentDidMount()
    {
      axios.get('/user/')
      .then( res => {

      });
    }
    handleUser(e) {
        this.setState({
            username: e.target.value,
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value,
        })
    }
    handleSubmit(event) {
        event.preventDefault()
        if(!(this.state.username)||!(this.state.password)){
          this.setState({
            snack_open:true,
            snack_msg:'Enter all the Details !!',
            alert_type:'warning',
            all:true,
          })
          return false;
        }
        else{
          axios.post('/user/flogin',{
              username: this.state.username,
              password: this.state.password,
          })
          .then(response => {
                if (response.status === 200) {
                  if(response.data === 'block')
                  {
                    this.setState({
                        snack_open:true,
                        snack_msg:'Secondary User Not Allowed !!',
                        alert_type:'error',
                      })
                      return false
                  }
                    else if(response.data === 'U P')
                    {
                        this.setState({
                            snack_open:true,
                            snack_msg:'User not Found !!',
                            alert_type:'warning',
                          })
                          return false
                    }
                    else if(response.data === 'P P')
                    {
                        this.setState({
                            snack_open:true,
                            snack_msg:'Password does not match !!',
                            alert_type:'warning',
                          })
                          return false
                    }
                    else{
                        this.setState({user:response.data})
                        this.setState({
                            snack_open:true,
                            snack_msg:'Logging In !!',
                            alert_type:'success',
                            all:true,
                            redirect:'/dashboard',
                          })
                    }
     
                }
            }).catch(error => {
                this.setState({
                    snack_open:true,
                    snack_msg:'Wrong Credentials !!',
                    alert_type:'error',
                  })
            })
            this.setState({
               username: '',
               password: '',
             })
        }
    }

    
    render() {
        const checked = true;
        if (this.state.redirect) {
            return <Redirect to={{ pathname: this.state.redirect }} />
        } else {
        return (
            <React.Fragment>
                <Snackbar
                open={this.state.snack_open} autoHideDuration={2000}
                onClose={()=>this.setState({snack_open:false})}>
                <Alert onClose={()=>this.setState({snack_open:false})}
                severity={this.state.alert_type}>
                  {this.state.snack_msg}
                </Alert>
                </Snackbar>
                <Navbar />
                <Grid container>
                    <Grid item xs={1} sm={4} xl={4}></Grid>
                    <Grid item xs={10} sm={4} xl={4}>
                    <Zoom
                    in={true}
                    {...(checked ? { timeout: 700 } : {})}
                    >
                    <Paper elevation={3} style={{marginTop:'100px',borderRadius:'8px'}}>
                        <div className="center">
                            <Link style={{textDecoration:'none'}} to ="/">
                            <Typography variant="h5" color="secondary" align="center"
                            style={{padding:'8px',color: 'white',borderRadius:'10px 10px 0px 0px',
                                display: 'block',backgroundColor:'#009688'}}>eTest</Typography>

                            </Link>
                            <Typography variant="h6" color="secondary" align="center"
                            style={{padding:'8px',color: '#7b7b7b',borderRadius:'10px 10px 0px 0px',
                                display: 'block'}}>
                                FACULTY LOG IN
                            </Typography>
                        </div>
                        <form className="form-con">
                           <Grid container spacing={1}>
                             <Grid item xs={12} sm={12}>
                                <TextField id="outlined-basic1" label="Username"
                                type="text" value={this.state.username} onChange={this.handleUser} fullWidth
                                 variant="outlined" />
                                 <br /><br />
                             </Grid>
                             <Grid item xs={12} sm={12}>
                               <TextField id="outlined-basic2" label="Password"
                                type="password" className="validate" value={this.state.password} onChange={this.handlePass} fullWidth
                                variant="outlined" />
                                <br /><br />
                             </Grid>
                           </Grid>
                           <Grid container spacing={3}>
                               <Grid item xs={6} sm={6}>
                                    <Register />


                               </Grid>
                              <Grid item xs={6} sm={6}>
                                 <Button fullWidth
                                  style={{backgroundColor:'#009688',color:'white'}}
                                  onClick={this.handleSubmit}>Login</Button>
                              </Grid>
                           </Grid>
                           <br />

                         <Divider /><br />

                         <Modal/>

                     </form>
                    </Paper>
                    </Zoom>
                    </Grid>
                    <Grid item xs={1} sm={4} xl={4}></Grid>
                </Grid>


            </React.Fragment>
        )
    }
}
}
