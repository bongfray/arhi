const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const FUser = require('../database/models/Faculty/user')
const fpassport = require('../passport/fac_pass')

const SUser = require('../database/models/Student/user')
const spassport = require('../passport/stud_pass')

const crypto = require('crypto')
require("datejs")

console.log('Connected to Server !!')

/**-------------Signup---------- */
router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

// Faculty sign up
router.post('/fsignup', (req, res) => {
    FUser.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new FUser({
                title: req.body.title,
                name: req.body.name,
                username: req.body.username,
                mailid: req.body.mailid,
                phone: req.body.phone,
                campus: req.body.campus,
                dept: req.body.dept,
                desgn: req.body.desgn,
                password: req.body.password,
              })
            newUser.save((err, savedUser) => {
              if(err)
              {

              }
              else if (savedUser) {
                var succ= {
                    succ: "Successfully SignedUP"
                  };
                  res.send(succ);
              }
            })
        }
    })
})

router.post(
  '/flogin',(req, res) => {
    const { username} = req.body;
    FUser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        // console.log(err)
      }
      else if(!objs)
      {
        res.send('U P')
      }
      else if(objs){
        if(bcrypt.compareSync(req.body.password, objs.password)) 
        {
          sess = req.session;
          sess.username = req.body.username;
          sess.password = objs.password;
          res.send('ok')
        }
        else{
          res.send('P P')
        }
      }
    });

  }
)

// Student signup
router.post('/ssignup', (req, res) => {
  SUser.findOne({regid: req.body.regid}, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if (user)
      {
        var emsg = {
          emsg: "User already taken",
        };
          res.send(emsg);
      }
      else {
          const newUser = new SUser({
              name: req.body.name,
              regid: req.body.regid,
              mailid: req.body.mailid,
              phone: req.body.phone,
              campus: req.body.campus,
              degree: req.body.degree,
              dept: req.body.dept,
              year: req.body.year,
              sem: req.body.sem,
              batch: req.body.batch,
              batch_no: req.body.batch_no,
              password: req.body.password,
            })
          newUser.save((err, savedUser) => {
            if(err)
            {

            }
            else if (savedUser) {
              var succ= {
                  succ: "Successfully SignedUP"
                };
                res.send(succ);
            }
          })
      }
  })
})
module.exports = router
