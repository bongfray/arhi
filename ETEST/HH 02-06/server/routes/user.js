const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const FUser = require('../database/models/Faculty/user')
const fpassport = require('../passport/fac_pass')

// questions import
const Mcq = require('../database/models/Faculty/mcq')
const TrueFalse = require('../database/models/Faculty/truefalse')
const OneWord = require('../database/models/Faculty/oneword')
const Match = require('../database/models/Faculty/match')
const ShortLong = require('../database/models/Faculty/shortlong')

const SUser = require('../database/models/Student/user')
const spassport = require('../passport/stud_pass')

const crypto = require('crypto')
require("datejs")

console.log('Connected to Server !!')

/**-------------Signup---------- */
router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

// Faculty sign up
router.post('/fsignup', (req, res) => {
    FUser.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new FUser({
                title: req.body.title,
                name: req.body.name,
                username: req.body.username,
                mailid: req.body.mailid,
                phone: req.body.phone,
                campus: req.body.campus,
                dept: req.body.dept,
                desgn: req.body.desgn,
                password: req.body.password,
              })
            newUser.save((err, savedUser) => {
              if(err)
              {

              }
              else if (savedUser) {
                var succ= {
                    succ: "Successfully SignedUP"
                  };
                  res.send(succ);
              }
            })
        }
    })
})

// Questions add

router.post('/mcq_upload',(req,res)=>{
  const newMcq = new Mcq({
    question: req.body.question,
    option1: req.body.option1,
    option2: req.body.option2,
    option3: req.body.option3,
    option4: req.body.option4,
    answer: req.body.answer,
  })
  newMcq.save((err,savedMcq)=>{
    if(err){
      console.log(err)
    }
    else if(savedMcq){
      var succ= {
        succ: "Successfully Uploaded"
      };
      res.send(succ);
    }
  })
})

router.post('/truefalse_upload',(req,res)=>{
  const newTrueFalse = new TrueFalse({
    question: req.body.question,
    option1: req.body.option1,
    option2: req.body.option2,
    answer: req.body.answer,
  })
  newTrueFalse.save((err,savedTrueFalse)=>{
    if(err){
      console.log(err)
    }
    else if(savedTrueFalse){
      var succ= {
        succ: "Successfully Uploaded"
      };
      res.send(succ);
    }
  })
})

router.post('/oneword_upload',(req,res)=>{
  const newOneWord = new OneWord({
    question: req.body.question,
    answer: req.body.answer,
  })
  newOneWord.save((err,savedOneWord)=>{
    if(err){
      console.log(err)
    }
    else if(savedOneWord){
      var succ= {
        succ: "Successfully Uploaded"
      };
      res.send(succ);
    }
  })
})

router.post('/match_upload',(req,res)=>{
  const newMatch = new Match({
    left1: req.body.left1,
    left2: req.body.left2,
    left3: req.body.left3,
    left4: req.body.left4,
    left5: req.body.left5,  
    right1: req.body.right1,
    right2: req.body.right2,
    right3: req.body.right3,
    right4: req.body.right4,
    right5: req.body.right5,
    answer1: req.body.answer1,
    answer2: req.body.answer2,
    answer3: req.body.answer3,
    answer4: req.body.answer4,
    answer5: req.body.answer5,
  })
  newMatch.save((err,savedMatch)=>{
    if(err){
      console.log(err)
    }
    else if(savedMatch){
      var succ= {
        succ: "Successfully Uploaded"
      };
      res.send(succ);
    }
  })
})

router.post('/shortlong_upload',(req,res)=>{
  const newShortLong = new ShortLong({
    question: req.body.question,
  })
  newShortLong.save((err,savedShortLong)=>{
    if(err){
      console.log(err)
    }
    else if(savedShortLong){
      var succ= {
        succ: "Successfully Uploaded"
      };
      res.send(succ);
    }
  })
})
router.post(
  '/flogin',(req, res) => {
    const { username} = req.body;
    FUser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        // console.log(err)
      }
      else if(!objs)
      {
        res.send('U P')
      }
      else if(objs){
        if(bcrypt.compareSync(req.body.password, objs.password)) 
        {
          sess = req.session;
          sess.username = req.body.username;
          sess.password = objs.password;
          res.send('ok')
        }
        else{
          res.send('P P')
        }
      }
    });

  }
)

// Student signup
router.post('/ssignup', (req, res) => {
  SUser.findOne({regid: req.body.regid}, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if (user)
      {
        var emsg = {
          emsg: "User already taken",
        };
          res.send(emsg);
      }
      else {
          const newUser = new SUser({
              name: req.body.name,
              regid: req.body.regid,
              mailid: req.body.mailid,
              phone: req.body.phone,
              campus: req.body.campus,
              degree: req.body.degree,
              dept: req.body.dept,
              year: req.body.year,
              sem: req.body.sem,
              batch: req.body.batch,
              batch_no: req.body.batch_no,
              password: req.body.password,
            })
          newUser.save((err, savedUser) => {
            if(err)
            {

            }
            else if (savedUser) {
              var succ= {
                  succ: "Successfully SignedUP"
                };
                res.send(succ);
            }
          })
      }
  })
})
module.exports = router
