const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

const match = new Schema({

  left1: { type: String, unique: false, required: false },
  left2: { type: String, unique: false, required: false },
  left3: { type: String, unique: false, required: false },
  left4: { type: String, unique: false, required: false },
  left5: { type: String, unique: false, required: false },
  right1: { type: String, unique: false, required: false },
  right2: { type: String, unique: false, required: false },
  right3: { type: String, unique: false, required: false },
  right4: { type: String, unique: false, required: false },
  right5: { type: String, unique: false, required: false },
  answer1: { type: String, unique: false, required: false },
  answer2: { type: String, unique: false, required: false },
  answer3: { type: String, unique: false, required: false },
  answer4: { type: String, unique: false, required: false },
  answer5: { type: String, unique: false, required: false },
  

})

var Match = mongoose.model('Match', match);

module.exports = Match
