const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

const shortlong = new Schema({
  question: { type: String, unique: false, required: false },  
})

var ShortLong = mongoose.model('ShortLong', shortlong);

module.exports = ShortLong
