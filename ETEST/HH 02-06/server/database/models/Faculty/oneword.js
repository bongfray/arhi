const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

const oneword = new Schema({

  question: { type: String, unique: false, required: false },
  answer: { type: String, unique: false, required: false },
})

var OneWord = mongoose.model('OneWord', oneword);

module.exports = OneWord
