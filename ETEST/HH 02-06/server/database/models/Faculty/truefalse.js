const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

const truefalse = new Schema({

  question: { type: String, unique: false, required: false },
  option1: { type: String, unique: false, required: false },
  option2: { type: String, unique: false, required: false },
  answer: { type: String, unique: false, required: false },
})

var TrueFalse = mongoose.model('TrueFalse', truefalse);

module.exports = TrueFalse
