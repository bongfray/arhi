const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const userSchema = new Schema({
name: { type: String, unique: false, required: false },
regid: { type: String, unique: false, required: false },
mailid: { type: String, unique: false, required: false },
phone: { type: String, unique: false, required: false },
campus: { type: String, unique: false, required: false },
dept: { type: String, unique: false, required: false },
degree: { type: String, unique: false, required: false },
year: { type: String, unique: false, required: false },
sem: { type: String, unique: false, required: false },
batch: { type: String, unique: false, required: false },
batch_no: { type: String, unique: false, required: false },
password: { type: String, unique: false, required: false },
})


userSchema.methods = {
	checkPassword: function (inputPassword) {
		return bcrypt.compareSync(inputPassword, this.password)
	},
	hashPassword: plainTextPassword => {
		return bcrypt.hashSync(plainTextPassword, 10)
	}
}



userSchema.pre('save', function (next) {
	if (!this.password) {
		next()
	} else {
		this.password = this.hashPassword(this.password)
		next()
	}
})



const Stud_User = mongoose.model('Stud_User', userSchema)
module.exports = Stud_User
