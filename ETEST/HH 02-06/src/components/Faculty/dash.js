import React, { Component } from 'react'
import
{Dialog,Button,Grid,DialogActions,
  Divider,DialogContent,DialogTitle,
  Snackbar,Zoom,TextField
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

export default class Dashboard extends Component {
    constructor(){
        super()
        this.state={
            course_id:'15SE303',
            course_name:'Random Engg.',
            total_stud:28,
            open:false,
        }
    }
    handleOpen=()=>{
        this.setState({open:!this.state.open})
    }
    render() {
        return (
            <React.Fragment>
            <Grid container spacing={1} style={{padding:'20px'}}>
                <Grid item xs={12} sm={2} xl={2}>
                <Paper elevation={3} onClick={this.handleOpen} style={{marginTop:'100px',borderRadius:'8px',height:'150px',padding:'15px',cursor:'pointer'}}>
                    <Typography variant="h5">Course ID: {this.state.course_id}</Typography>
                    <Typography variant="h6" color="secondary">Course Name: {this.state.course_name}</Typography>
                    <Typography variant="h6" color="secondary">Total Students: {this.state.total_stud}</Typography>
                    
                </Paper>
                </Grid>
                <Dialog fullScreen open={this.state.open} onClose={this.handleOpen} TransitionComponent={Transition}>
                    <AppBar position="relative" style={{backgroundColor:'#009688'}}>
                    <Toolbar>
                        <Grid container spacing={1}>
                            <Grid item xs={1} sm={1} xl={1}>
                                <IconButton edge="start" color="inherit" onClick={this.handleOpen} aria-label="close">
                                <CloseIcon />
                                </IconButton>
                            </Grid>
                            <Grid item xs={11} sm={11} xl={11}>

                            <Typography variant="h6" align="center" style={{paddingTop:'8px'}}>
                                {this.state.course_id} : {this.state.course_name}
                            </Typography>
                            </Grid>
                        </Grid>
                    </Toolbar>
                    </AppBar>
                </Dialog>
            </Grid>
            </React.Fragment>
        )
    }
}
