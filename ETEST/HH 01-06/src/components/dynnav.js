import React, { Component } from 'react';
import Slogo from './Slogo.png'

import {Slide} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Hidden from '@material-ui/core/Hidden';


export default class Navbar extends Component {
    render() {
        const checked=true;
        return (
            <React.Fragment>
                <Slide direction="down"
                    in={true}
                    {...(checked ? { timeout: 700 } : {})}
                >
                    <AppBar position="fixed" style={{backgroundColor:'#009688'}}>
                        <Toolbar>
                            <Grid container spacing={1}>
                                <Grid item xs={3} sm={2}>
                                    <Hidden xsDown>
                                    <img src={Slogo} className="srm"
                                    alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/>
                                    </Hidden>
                                </Grid>
                                    <Hidden smUp>
                                        <Grid item xs={6} sm={8}>
                                        <div style={{textAlign:'center',marginTop:'5px',
                                        fontFamily: 'Cinzel',
                                        fontSize: '30px'}}>
                                            SRM CARE
                                        </div>
                                        </Grid>
                                    </Hidden>
                                    <Hidden xsDown>
                                        <Grid item xs={6} sm={8}>
                                        <div style={{textAlign:'center',marginTop:'5px',fontFamily: 'Cinzel',fontSize: '30px'}}>
                                            SRM Centre for Applied Research in Education
                                            </div>
                                        </Grid>
                                        </Hidden>
                                <Grid item xs={3} sm={2}/>
                            </Grid>
                        </Toolbar>
                    </AppBar>
                </Slide>
            </React.Fragment>
        )
    }
}
