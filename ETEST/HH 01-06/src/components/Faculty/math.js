import React from 'react'
import { addStyles, EditableMathField } from 'react-mathquill'
import
{Dialog,Button,Grid,DialogActions,
  Divider,DialogContent,DialogTitle,
  Snackbar,Zoom,TextField
} from '@material-ui/core';
import ReactFileReader from 'react-file-reader';
import axios from 'axios'
import MuiAlert from '@material-ui/lab/Alert';
import CSVReader from 'react-csv-reader'



addStyles()
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

export default class Math extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      latex: '',
      rec:'',
      snack_open:false,
      alert_type:'',
      snack_msg:'',
      flag:'',
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleF=this.handleF.bind(this)

  }
  handleSubmit(event) {
    event.preventDefault()
    var s = this.state.latex
    console.log(s)
}
    papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header =>
      header
        .toLowerCase()
        .replace(/\W/g, '_')
  }

  handleF(data,fileInfo){

      this.setState({
          rec:data,
      })
    for(var i=0;i<this.state.rec.length;i++){
        axios.post('/user/mcq_upload', {
            question: this.state.rec[i].question,
            option1: this.state.rec[i].option1,
            option2: this.state.rec[i].option2,
            option3: this.state.rec[i].option3,
            option4: this.state.rec[i].option4,
            answer: this.state.rec[i].answer,
        })
        .then(response => {
            if(response.status===200){
                if(response.data.emsg)
                {
                    this.setState({
                        snack_open:true,
                        snack_msg:response.data.emsg,
                        alert_type:'info',
                    })
              }
                else if(response.data==='ok')
                {
                    this.setState({
                            verify_modal:true,
                    })
                }
                else if(response.data.succ)
                {
                    this.setState({
                        snack_open:true,
                        snack_msg:"Successfully Uploaded ",
                        alert_type:'success',
                    });
                }
            }
        }).catch(error => {
        this.setState({
              snack_open:true,
              snack_msg:'Something Went Wrong !!',
              alert_type:'error',
          })
        })
    }
  }


  render() {
    return (
        <React.Fragment>
        <Snackbar
		        open={this.state.snack_open} autoHideDuration={2000}
		        onClose={()=>this.setState({snack_open:false})}>
			    <Alert onClose={()=>this.setState({snack_open:false})}
			    severity={this.state.alert_type}>
				{this.state.snack_msg}
			    </Alert>
		        </Snackbar>

      <EditableMathField
        latex={this.state.latex} // Initial latex value for the input field
        onChange={mathField => {
            this.setState({ latex: mathField.latex() })
        }}
      />
        <Button fullWidth
        style={{backgroundColor:'#009688',color:'white'}}
        onClick={this.handleSubmit}>Login</Button>

        <CSVReader parserOptions={this.papaparseOptions} onFileLoaded={this.handleF} />

        </React.Fragment>

    )
  }
}
