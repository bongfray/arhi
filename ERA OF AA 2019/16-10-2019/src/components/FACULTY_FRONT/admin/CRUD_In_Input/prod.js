import React from 'react';
import axios from 'axios';
import ValidateUser from '../validateUser'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level != this.props.level)) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({modal: !this.state.modal,disabled:'disabled',id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
  if(index === "faculty")
  {
    delroute = "/user/delnav";
  }
  else if(index === "student")
 {
  delroute = "/user2/delnav";
 }
 else if(index === "Section"){
  delroute = "/user/del_section";
 }
else if((index === "Administrative") || (index === "Academic") || (index === "Research")){
  delroute = "/user/del_responsibility";
 }
 else if((index==="Designation") || (index === "Department"))
 {
   delroute = "/user/del_designation";
 }
const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:'',
        success:'none',
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
  if(action === "faculty")
  {
    fetchroute = "/user/fetchnav";
  }
  else if(action === "student")
  {
    fetchroute = "/user2/fetch_student_nav"
  }
  else if(action === "Section"){
    fetchroute = "/user/fetch_section";
  }
  else if((action === "Administrative") || (action === "Academic") || (action === "Research"))
  {
    fetchroute = "/user/fetch_responsibility";
  }
  else if((action === "Designation") || (action === "Department"))
  {
    fetchroute = "/user/fetch_designation";
  }

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { error, products} = this.state;
      return(
        <div>
          <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
            <div className="row">
                <div className="col s1 l1 m1 xl1 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className={content.div} key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s2 l2 xl2 m2 center"><b>Action</b></div>
            </div>
            <hr />
            <div>
              {products.map((product,index) => (
                <div className="row" key={product.serial}>
                  <div className="col l1 xl1 m1 s1 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className={content.div} key={index}>{product[content.name]}</div>
                  ))}
                    <div className="center col s2 m2 l2 xl2 hide-on-small-only"><button className="btn-small green" onClick={() => this.props.editProduct(product.serial,this.props.action)}>Edit</button>
                    &nbsp;<button className="btn-small red" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </div>
                  <div className="col s2  hide-on-med-and-up">
                  <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                  &nbsp;<i className="material-icons go" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                </div>
              ))}
            </div>
        </div>
      )

  }
}
