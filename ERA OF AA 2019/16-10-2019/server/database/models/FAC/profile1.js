const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const profSchema = new Schema({

  username: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  Collage_Name:{ type: String, unique: false, required: false },
  End_Year:{ type: String, unique: false, required: false },
  Start_Year:{ type: String, unique: false, required: false },
  Marks_Grade:{ type: String, unique: false, required: false },
})





profSchema.plugin(autoIncrement.plugin, { model: 'degrees', field: 'serial', startAt: 1,incrementBy: 1 });



var Prof = mongoose.model('degrees', profSchema);

module.exports = Prof
