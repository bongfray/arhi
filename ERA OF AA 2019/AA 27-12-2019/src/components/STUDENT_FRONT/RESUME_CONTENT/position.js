import React, { Component } from 'react'
import axios from 'axios'
import AddProduct from './add_datas';
import ProductList from './show_info'


 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:'',

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/user2/editt';
     } else {
       apiUrl = '/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }
   render() {
     console.log(this.state)
     let productForm,title,description;
     var data;
             title ='Position of Responsibility';
             description = 'Example: Project leader in Project A or of Team X'
             data = {
               Action:'Position of Responsibility',
               button_grid:'col l1 m1 s1 center',
               fielddata: [
                 {
                   header: "Description",
                   name: "description",
                   placeholder: "Short Note About your work",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Start Date",
                   name: "start_date",
                   placeholder: "Enter the Start Date",
                   type: "text",
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "End Date",
                   name: "end_date",
                   placeholder: "Enter the End Date(Type Current for Ongoing Work)",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct description={description} cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6" />
     <div className="col l6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
      </div>
    </div>
    </React.Fragment>
    }
    { productForm }
    <br/>
  </div>

);
}
}
