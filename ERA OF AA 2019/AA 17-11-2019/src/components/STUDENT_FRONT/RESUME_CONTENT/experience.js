import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import AddProduct from './add_datas';
import ProductList from './show_info'
var empty = require('is-empty');

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
   }

   editProduct = (productId,index)=> {


  }
   render() {
     let productForm,title,description;
     var data;
             title ='Thesis & Projects Supervised';
             description ='Please provide information about graduate thesis/project supervised';
             data = {
               Action:'thesis',
               fielddata: [
                 {
                   header: "Title of Completed Thesis/Project",
                   name: "title_thesis_project",
                   placeholder: "Enter the Title of Thesis / Project",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Description of Thesis/Project",
                   name: "description_of_thesis_project_completed",
                   placeholder: "Description of the Thesis / Project",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Credits",
                   name: "credits_for_thesis_project",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter Details about the Credits"
                 },
                 {
                   header: "Date of Completion",
                   name: "date_of_thesis_project_completion",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter the Date of Completion"
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div className="card">
  <div>
    <div>
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 left">
  </div>
     <div className="col l6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>
  </div>
</div>
  </div>
);
}
}
