import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import AddProduct from './add_datas';
import ProductList from './show_info'
var empty = require('is-empty');

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{
  console.log(this.state.action)
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }



handleMe =() =>{
  axios.post('/user/reset')
  .then(response => {
    if(response.data.succ)
    {
        window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded green lighten-1'});
    }
    else if(response.data.notallowed)
    {
        window.M.toast({html: response.data.notallowed,outDuration:'9000', classes:'rounded  red lighten-1'});
    }
  })

}
   onFormSubmit(data) {
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/user/editt';
     } else {
       apiUrl = '/user/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm,title,description;
     var data;
     if(this.props.options === "thesis_projects_supervised")
     {
             title ='Thesis & Projects Supervised';
             description ='Please provide information about graduate thesis/project supervised';
             data = {
               Action:'thesis',
               fielddata: [
                 {
                   header: "Title of Completed Thesis/Project",
                   name: "title_thesis_project",
                   placeholder: "Enter the Title of Thesis / Project",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Description of Thesis/Project",
                   name: "description_of_thesis_project_completed",
                   placeholder: "Description of the Thesis / Project",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Credits",
                   name: "credits_for_thesis_project",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter Details about the Credits"
                 },
                 {
                   header: "Date of Completion",
                   name: "date_of_thesis_project_completion",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter the Date of Completion"
                 },

               ],
             };
}
else if(this.props.options === "training_workshop_attended")
{
  title ='Training or Workshop or Seminer or Conference Attended';
  description ='Please Provide the Details about the training workshop seminar attended ';
  data = {
    fielddata: [
      {
        header: "Date",
        name: "date_of_training_attended",
        placeholder: "Enter the Date of Attending",
        type: "text",
        grid: 2
      },
      {
        header: "Title of  Workshop or Seminar",
        name: "title_workshop_seminar_attended",
        placeholder: "Title of the Workshop or Seminar",
        type: "text",
        grid: 2
      },
      {
        header: "Details About Workshop or Seminar",
        name: "details_workshop_seminar_attended",
        placeholder: "Details About Workshop or Seminar",
        type: "text",
        grid: 2
      },
      {
        header: "Location",
        name: "location_of_workshop_seminar_attended",
        type: "text",
        grid: 2,
        placeholder: "Location of Workshop/Seminer Held"
      },
      {
        header: "Duration of the Event",
        name: "duration_of_workshop_seminar_attended",
        type: "text",
        grid: 2,
        placeholder: "Enter the Duration of the Event"
      },

    ],
  };
}
else if(this.props.options === "training_workshop_held")
{
  title ='Training or Workshop or Seminer or Conference Held';
  description ='Please Provide the Details about the training workshop seminar held by you';
  data = {
    fielddata: [
      {
        header: "Date",
        name: "date_of_training_held",
        placeholder: "Enter the Date of helding",
        type: "text",
        grid: 2
      },
      {
        header: "Details About Workshop or Seminar",
        name: "details_workshop_seminar_held",
        placeholder: "Details About Workshop or Seminar",
        type: "text",
        grid: 2
      },
      {
        header: "Location",
        name: "location_of_workshop_seminar_held",
        type: "text",
        grid: 2,
        placeholder: "Location of Workshop/Seminer Held"
      },
      {
        header: "Duration of the Event",
        name: "duration_of_workshop_seminar_held",
        type: "text",
        grid: 2,
        placeholder: "Enter the Duration of the Event"
      },
      {
        header: "Your Role",
        name: "role_workshop_seminar_held",
        type: "text",
        grid: 2,
        placeholder: "Your Role in the Event"
      },

    ],
  };
}
else if(this.props.options === "paper_published_accepted")
{
  title ='Paper Published & Accepted';
  description ='Please Provide the Details about the Paper Published as well as Accepted(Fill Details only if your Paper is Accepted & Published)';
  data = {
    fielddata: [
      {
        header: "Date of Publication",
        name: "date_of_paper_publication",
        placeholder: "Enter the Date of Publication & Accepted",
        type: "text",
        grid: 2
      },
      {
        header: "Paper Title",
        name: "paper_title",
        placeholder: "Enter the Paper Title",
        type: "text",
        grid: 2
      },
      {
        header: "Volume No.",
        name: "volume_no_of_publiction",
        type: "text",
        grid: 2,
        placeholder: "Enter the Volume No"
      },
      {
        header: "Impact of Your Publication",
        name: "impact_of_publication",
        type: "text",
        grid: 2,
        placeholder: "Impact of Your Publication"
      },
      {
        header: "Accepted By",
        name: "paper_accepted_by",
        type: "text",
        grid: 2,
        placeholder: "Your Paper is Accepted By"
      },

    ],
  };
}
else if(this.props.options === "research_projects_done_by_you")
{
  title ='Research or Projects Done by You';
  description ='Please Provide your Research and Project Details';
  data = {
    fielddata: [
      {
        header: "Research or Project Area/ Domain",
        name: "research_project_area",
        placeholder: "Research or Project Area/ Domain",
        type: "text",
        grid: 2
      },
      {
        header: "Project/Research Title",
        name: "project_research_title",
        placeholder: "Project/Research Title",
        type: "text",
        grid: 2
      },
      {
        header: "Description of Projects or Research",
        name: "description_project_research",
        type: "text",
        grid: 2,
        placeholder: "Description of Projects or Research"
      },
      {
        header: "Duration of your Work",
        name: "duration_of_project_research_tobe_done",
        type: "text",
        grid: 2,
        placeholder: "Enter the Duration of the Event"
      },
      {
        header: "Any Achivements",
        name: "achivements_from_project_research_work",
        type: "text",
        grid: 2,
        placeholder: "Any Achivements"
      },

    ],
  };
}
else if(this.props.options === "contribution_to_university")
{
  title ='Contribution to University';
  description ='Please provide the details about any type of contribution to this University';
  data = {
    fielddata: [
      {
        header: "Contribution Field",
        name: "contribution_field",
        placeholder: "Contribution Field",
        type: "text",
        grid: 2
      },
      {
        header: "Details of your Contribution",
        name: "details_of_contribution",
        placeholder: "Details of your Contribution",
        type: "text",
        grid: 2
      },
      {
        header: "Results",
        name: "result_of_contribution",
        type: "text",
        grid: 2,
        placeholder: "Result of your Contribution"
      },
      {
        header: "Achivements for Contribution",
        name: "achivements_for_your_contribution",
        type: "text",
        grid: 2,
        placeholder: "Achivements for your Contribution"
      },

    ],
  };
}
else if(this.props.options === "department_activities")
{
  title ='Department Activities';
  description ='Please Provide the Details about your Department Activities';
  data = {
    fielddata: [
      {
        header: "Field of Activity",
        name: "field_of_activity",
        placeholder: "Enter the Field of Activity",
        type: "text",
        grid: 2
      },
      {
        header: "Details About your Activity",
        name: "details_of_dept_activity",
        placeholder: "Details About your Activity",
        type: "text",
        grid: 2
      },
      {
        header: "Result of Your Activity",
        name: "result_from_activity",
        type: "text",
        grid: 2,
        placeholder: "Result of Your Activity"
      },
      {
        header: "Achivements form the Activity",
        name: "achivements_from_the_activity",
        type: "text",
        grid: 2,
        placeholder: "Achivements form the Activity"
      },

    ],
  };
}
else if(this.props.options === "department_committe_membership")
{
  title ='Department Committes Membership';
  description ='Please Provide Details about your Participation in Dept. Committes';
  data = {
    fielddata: [
      {
        header: "Date of Meeting",
        name: "date_of_meeting",
        placeholder: "Enter the Date of Meeting",
        type: "text",
        grid: 2
      },
      {
        header: "Commity Details",
        name: "commity_details",
        placeholder: "Commity Details",
        type: "text",
        grid: 2
      },
      {
        header: "Your Position and Role",
        name: "position_role",
        type: "text",
        grid: 2,
        placeholder: "Mention your Position and Role"
      },
      {
        header: "Number of the Metting Attended",
        name: "number_of_meeting_attended",
        type: "text",
        grid: 2,
        placeholder: "Number of the Metting Attended"
      }

    ],
  };
}
else if(this.props.options === "advising_counselling_details")
{
  title ='Advising and Counselling Details';
  description ='Mention your Participation in Advising and Counselling Students(for example guiding the Students in their academic and so on)';
  data = {
    fielddata: [
      {
        header: "Date",
        name: "date_of_counselling",
        placeholder: "Enter the Date of Event",
        type: "text",
        grid: 2
      },
      {
        header: "Details of the Student(Academic only like sem,year,reg no)",
        name: "details_of_student",
        placeholder: "Details of the Student(Academic only like sem,year,reg no)",
        type: "text",
        grid: 2
      },
      {
        header: "Total No of advising Hours per sem",
        name: "no_of_advising_hour",
        type: "text",
        grid: 2,
        placeholder: "No of advising Hours per sem"
      },
      {
        header: "Description of Advising Activity",
        name: "description_of_activity",
        type: "text",
        grid: 2,
        placeholder: "Description of Advising Activity"
      },
    ],
  };
}

else{
  return(
  productForm = <div></div>
)
}

if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div className="App">
  <Collapsible popout>
    <CollapsibleItem
      header={
            <h5 className="collaphead">{title}</h5>
      }expanded>
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 left">
  <span>Kindly Reset after Deleting all the Datas  </span><a className="go" onClick={this.handleMe}>RESET</a>
  </div>
     <div className="col l6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
    <br/>
  </div>
  </CollapsibleItem>
</Collapsible>
  </div>
);
}
}
