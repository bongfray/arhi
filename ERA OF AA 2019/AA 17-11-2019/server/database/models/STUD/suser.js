const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const student = new Schema({
suspension_status: { type:Boolean, unique: true, required: false },
username: { type: String, unique: true, required: false },
password: { type: String, unique: false, required: false },
name: { type: String, unique: false, required: false },
mailid: { type: String, unique: false, required: false },
phone: { type: String, unique: false, required: false },
campus: { type: String, unique: false, required: false },
dept: { type: String, unique: false, required: false },
dob: { type: String, unique: false, required: false },
degree: { type: String, unique: false, required: false },
st_year: { type: String, unique: false, required: false },
year: { type: String, unique: false, required: false },
resetPasswordToken: { type: String, unique: false, required: false },
resetPasswordExpires: { type: String, unique: false, required: false },
count: { type: Number, unique: false, required: false },
})


student.methods = {
	checkPassword: function (inputPassword) {
		return bcrypt.compareSync(inputPassword, this.password)
	},
	hashPassword: plainTextPassword => {
		return bcrypt.hashSync(plainTextPassword, 10)
	}
}



student.pre('save', function (next) {
	if (!this.password) {
		next()
	} else {

		this.password = this.hashPassword(this.password)
		next()
	}
})



const Suser = mongoose.model('user_s', student)
module.exports = Suser
