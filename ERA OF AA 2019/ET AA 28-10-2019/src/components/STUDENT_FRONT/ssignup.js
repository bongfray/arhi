import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import Registration_Stopped from '../FACULTY_FRONT/stop'
import {Select} from 'react-materialize'
import ContentLoader from "react-content-loader"
import M from 'materialize-css';
import {} from 'materialize-css'

var empty = require('is-empty');



class SSignup extends Component {
	constructor() {
    super()
    this.state = {
		loader:false,
		error:'',
		status:"none",
		redirectTo: null,
		name: '',
		mailid: '',
		regid: '',
		phone: '',
		password: '',
		cnf_pswd: '',
		campus: '',
		dept: '',
		degree: '',
		startyear: '',
		fnyear: '',
		dob:'',
		count: 4,
    }
		this.componentDidMount = this.componentDidMount.bind(this)
		this.handleInput = this.handleInput.bind(this)
	}
	handleInput = (e) =>{
		this.setState({[e.target.name]:e.target.value})
	}
		componentDidMount(){
			M.AutoInit()
			axios.get('/user2/fetch_srender_status',this.setState({loader:true}))
			.then(res=>{
				this.setState({loader:false})
				if(res.data === true)
				{
				this.setState({status:"block"})
			  }
			})
		}


	handleSubmit=(event)=> {
		var verify = this.state.regid;
		var vermail = this.state.mailid;
		event.preventDefault()
		if(empty(this.state.fnyear)||empty(this.state.startyear)||empty(this.state.degree)||empty(this.state.name)||empty(this.state.mailid)||empty(this.state.regid)||empty(this.state.phone)||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.dept))
		{
			window.M.toast({html: 'Enter all the Details',outDuration:'1000', classes:'rounded red'});
      return false;
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
			this.setState({
				error:'Password does not match !!',
				cnf_pswd:'',
			})
	    
	     return false;

	  }
	  else if(!verify.includes('RA'))
	  {
		this.setState({
			error:'Enter Registration Number starting with RA !!',
			regid:'',
		})
		return false;
	  }
	  else if(verify.length!==15)
	  {
		this.setState({
			error:'Registration Number should be of 15 digit !!',
			regid:'',
		})
		return false;
	  }
	  else if((!vermail.includes('srmuniv.edu.in'))&&(!vermail.includes('srmist.edu.in')))
	  {
		this.setState({
			error:'Enter official SRM Mail Id Please!!',
			mailid:'',
		})
		return false;
	  }
		else if ((this.state.phone).length!==10)
		{
			this.setState({
				error:'Enter correct format of Phone no !!',
				phone:'',
			})
			return false;
		}
		else{
			console.log(this.state)
		axios.post('/user2/ssignup', {
			regid: this.state.regid,
			password: this.state.password,
      name: this.state.name,
      mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
	    dept: this.state.dept,
			dob: this.state.dob,
			degree: this.state.degree,
			st_year: this.state.st_year,
			fn_year: this.state.fn_year,
			count: this.state.count,
		})
			.then(response => {
				console.log(response)
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, outDuration:'9000', classes:'rounded #ba68c8 purple lighten-2'});
				  }
					else if(response.data.succ)
					{
						alert(response.data.succ);
						this.setState({
								redirectTo: '/slogin'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
			})
			this.setState({
			name: '',
			mailid: '',
			regid: '',
			phone: '',
			password: '',
			cnf_pswd: '',
			campus: '',
			dept: '',
			dob:''
		})
	}
}


render() {
	const MyLoader = () => (
        <ContentLoader 
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />   
        </ContentLoader>
      )
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status ==="none")
	{
		return(
			<Registration_Stopped login_path="/slogin" section_name="STUDENT REGISTRATION"/>
		);
	}
	else{
	if (this.state.redirectTo) {
			 return <Redirect to={{ pathname: this.state.redirectTo }} />
	 } else {
	return (
		<div className="row">

		<div className="col s2 l2 m2 xl2" />

		<div className="col l8 s12 m12 form-signup">
				<div className="center">
						<h5 className="reg">Student Registration</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">

								<div className="input-field col s6 l6 xl6 m6">
								<input id="name" type="text" className="validate" name="name" value={this.state.name} onChange={this.handleInput} required />
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s6 xl6 l6 m6">
								<input id="stud_id" type="text" className="validate" name="regid" value={this.state.regid} onChange={this.handleInput} required />
								<label htmlFor="stud_id">Registration Number</label>
								</div>
						</div>



						<div className="input-field row">

								<div className="input-field col s5 l5 xl5 m5">
								<input id="email" type="email" className="validate" name="mailid" value={this.state.mailid} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Only official mail id is allowed</span>
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="ph_num" type="number" className="validate" name="phone" value={this.state.phone} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Enter Correct Format of Phone no</span>
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="input-field col s3 l3 xl3 m3">
								<input id="dob" type="text" className="validate" name="dob" value={this.state.dob} onChange={this.handleInput} required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Format: dd-mm-yyyy</span>
								<label htmlFor="dob">Date of Birth</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6 l6 xl6 m6">
								<input onChange={this.handleInput} name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
								<span className="helper-text" data-error="Please enter the data" data-success="">Try to use some strong password</span>
								<label htmlFor="pswd">Password</label>
								</div>

								<div className="input-field col s6 l6 xl6 m6">
								<input onChange={this.handleInput} id="cnf_pswd" value={this.state.cnf_pswd} name="cnf_pswd" type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>

						</div>

						<div className="row">

								<div className="col s4 l4 xl4 m4">
										<Select name="degree" value={this.state.degree} onChange={this.handleInput}>
										<option value="" disabled defaultValue>Degree</option>
										<option value="btech">B.Tech</option>
										<option value="mtech">M.Tech</option>
										<option value="bca">BCA</option>
										<option value="mca">MCA</option>
										</Select>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="st_year" name="startyear" type="number" className="validate" value={this.state.startyear} onChange={this.handleInput} required />
								<label htmlFor="st_year">Starting Year (yyyy)</label>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="fn_year" type="number" name="fnyear" className="validate" value={this.state.fnyear} onChange={this.handleInput} required />
								<label htmlFor="fn_year">Completion Year (yyyy)</label>
								</div>

						</div>
						<div className="row">

								<div className="col xl6 l6 m6 s6">
										<Select name="campus" value={this.state.campus} onChange={this.handleInput}>
										<option value="" disabled defaultValue>Campus</option>
										<option value="Kattankulathur Campus">Kattankulathur Campus</option>
										<option value="Ramapuram Campus">Ramapuram Campus</option>
										<option value="Vadapalani Campus">Vadapalani Campus</option>
										<option value="NCR Campus">NCR Campus</option>
										</Select>
								</div>
								<div className="col xl6 l6 s6 m4">
										<Select name="dept" value={this.state.dept} onChange={this.handleInput}>
										<option value="" disabled defaultValue>Department</option>
										<option value="Computer Science">Computer Science</option>
										<option value="Information Technology">Information Technology</option>
										<option value="Software Engineering">Software</option>
										<option value="Mechanical Engineering">Mechanical</option>
										</Select>
								</div>
						</div>
						<br/>
						<div className="row"><div className="col l4 m4 s4 xl4 left">
							<Link to='/slogin' className="log">Login Instead ?</Link></div>
							<div className="col l4 s4 xl4 m4 center" style={{color:'red'}}>{this.state.error}</div>
							<div className="col l4 s4 m4 xl4">
							<button className="waves-effect btn blue-grey darken-2 sup" style={{width:'100%'}} onClick={this.handleSubmit}>Submit</button>
							</div>
						</div>
				</form>
		</div>
		</div>

	);
}
	}
}
}
}

export default SSignup;
