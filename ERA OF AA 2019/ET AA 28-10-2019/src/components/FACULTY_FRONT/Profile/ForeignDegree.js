import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import {} from 'materialize-css'
import AddProduct from './add';
import ProductList from './prod'
var empty = require('is-empty');

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       degrees:'',
       redirectTo: null,
       logout:'/user/logout',
       home:'/faculty',
       login:'/flogin',
       get:'/user/',
       nav_route: '/user/fetchnav',
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  axios.get('/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/faculty',
       });
       window.M.toast({html: 'You are not Logged In',outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
     }
   })
}

   onCreate = (e,index) => {
     this.setState({isAddProduct:true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'1200', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/user/editprofile1';
     } else {
       apiUrl = '/user/profile1';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId)=> {
     axios.post('/user/fetchtoedit',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  handleDegress =(e) =>{
    console.log(e.target.value)
    this.setState({degrees:e.target.value})
  }
   render() {
             var data = {
               fielddata: [
                 {
                   name: "Collage_Name",
                   placeholder: "Enter the Collage Name",
                   type: "text",
                   grid: 2
                 },
                 {
                   name: "Start_Year",
                   placeholder: "Starting Year",
                   type: "number",
                   grid: 2
                 },
                 {
                   name: "End_Year",
                   type: "number",
                   grid: 2,
                   placeholder: "Ending Year"
                 },
                 {
                   name: "Marks_Grade",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter Your Grade/ Mark"
                 },

               ],
             };

  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
    <div className="row">
  <div className="col l4" />
  <div className="col l4" style={{marginTop:'10px'}}>
    <Link to="/"><h5 className="prof-heading center black-text">ADD YOUR DEGREES</h5></Link>
  </div>
  <div className="col l4"/>
  </div>
  <div className="row">
     <div className="red-text col l3 xl3 hide-on-mid-and-down" />
       <div className=" col l7 xl7 s12 m12 form-signup">
          <div className="red-text">Choose the Name Of The Degree....</div>
           <div className="">
               <select value={this.state.degrees} onChange={this.handleDegress}>
                  <option value="" disabled defaultValue>Choose your option</option>
                  <option value="UG">UG</option>
                  <option value="PG">PG</option>
                  <option value="PHD">PHD</option>
                  <option value="POST-DOCTORATE">POST DOCTORATE</option>
               </select>
           </div>
      </div>
      <div className="col l3 xl3 hide-on-mid-and-down" />
  </div><br /><br />
          <div className="row">
            <div className="col l1 s2 m2 xl1"><b>Degree</b></div>
            <div className="col l1 s1 m1 xl1 center"><b>No.</b></div>
            <div className="col l2  s2 m2 xl2 center"><b>College Name</b></div>
            <div className="col l2 s2 xl2 m2 center"><b>Year of Starting</b></div>
            <div className="col l2 s2 xl2 m2 center"><b>Year of Completion</b></div>
            <div className="col l2 s2 xl2 m2 center"><b>Marks</b></div>
            <div className="col l2 xl2 s1 m1 center"><b>Actions</b></div>
          </div>

      <hr />
      <div className="form-signup">
          <div className="row ">
            {!this.state.isAddProduct && <ProductList username={this.state.username} title={this.state.degrees} action={this.state.degrees} data={data}  editProduct={this.editProduct}/>}
            {!this.state.isAddProduct &&
             <React.Fragment>
               <button className="btn right blue-grey darken-2 subm" name={this.state.degrees} onClick={(e) => this.onCreate(e,data)}>Add Degree</button>
            </React.Fragment>
          }
          {(this.state.isAddProduct || this.state.isEditProduct) &&
            <AddProduct username={this.state.username} action={this.state.degrees} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
          }
          </div>
          </div>
</React.Fragment>
);
}
}
}
