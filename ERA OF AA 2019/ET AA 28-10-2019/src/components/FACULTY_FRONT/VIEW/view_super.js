import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import { Select } from 'react-materialize'
import ContentLoader from "react-content-loader"
require("datejs")




export default class BasicView extends Component {
      _isMounted = false;
  constructor() {
    super()
    this.state ={
      start:'',
      isChecked: false,
      history: '',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      agreed:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this._isMounted = true;
    this.getUser();
    M.AutoInit()
  }

    getUser()
    {
      axios.get('/user/'
    )
       .then(response =>{
        if (this._isMounted) {
          if(response.status === 200)
          {
            this.setState({
              loading:false,
            })
         if(response.data.user)
         {

         }
         else{
           this.setState({
             redirectTo:'/faculty',
           });
           window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
         }
       }
       }
       })
    }


  handleComp = (e) =>{
    window.M.toast({html: 'Validating..',outDuration:'3000', classes:'rounded pink'});
    axios.get('/user/validate_list_view')
    .then( response => {
      // console.log(response.data)
        if(response.data.permission === 1)
        {
          window.M.toast({html: 'Accepted!!',outDuration:'3000', classes:'rounded green'});
          this.setState({
            isChecked: !this.state.isChecked,
            start:response.data.data_send,
          })
        }
        else{
          window.M.toast({html: 'Denied Request !!',outDuration:'3000', classes:'rounded red'});
          this.setState({
            isChecked:false,
            redirectTo:'/view',
          })
        }
    });
  }
  render() {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="130" y="10" rx="3" ry="3" width="135" height="10" />
        <rect x="100" y="30" rx="3" ry="3" width="200" height="100" />

      </ContentLoader>
    )
    if(this.state.loading === true)
    {
      return(
        <MyLoader />
      );
    }
    else if(this.state.loading === false){

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

                            <div className="switch center"  style={{marginTop:'18px'}}>
                                <label style={{color:'red',fontSize:'15px'}}>
                                    Already Having Official Id
                                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                                    <span className="lever"></span>
                                    Don't have any Official Id !!
                                </label>
                            </div>
                            <br />
                            <Switch datas={this.state.isChecked} start={this.state.start}/>
      </React.Fragment>
    );
  }
}
  }
}


class Switch extends Component{
  constructor(props)
  {
    super(props)
    this.state={

    }
  }
  render()
  {
    let render_content;
    if(this.props.datas === true)
    {
      render_content =
      <ListShow  start={this.props.start}/>
    }
    else{
      render_content =
      <ViewByID />
    }
    return(
      <React.Fragment>
        {render_content}
      </React.Fragment>
    );
  }
}


class ListShow extends Component {
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      username:'',
      dept:'',
      desgn:'',
      designation:'',
      desgn_list:[],
      department:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/user/fetch_view_list',{start:this.props.start})
    .then( res => {
      // console.log(res.data)
        if(res.data)
        {
          this.setState({desgn_list:res.data});
          this.fetchDepartment();
        }
    });
  }
  fetchDepartment=()=>{
    axios.get('/user/fetch_department')
    .then( res => {
      // console.log(res.data)
        if(res.data)
        {
          this.setState({department:res.data})
        }
    });
  }
  handleDesgn =(e)=>{
    this.setState({desgn:e.target.value})
  }
  handleDept = (e)=>{
    this.setState({dept:e.target.value})
  }
  showDiv =(username)=>{
    if(username)
    {
      this.setState({display:'block',username:username})
    }
  }

  render() {
    return (
      <div>
       <div className="row">
          <div className="form-signup col l4">
              <div className="row">
                 <div className="col l6">
                     <h6 className="center pink-text">Select the Designation</h6>
                     <Select l="12" xl="12" s="12" m="6" value={this.state.desgn} onChange={this.handleDesgn}>
                     <option value="" disabled defaultValue>Select Here</option>
                     {this.state.desgn_list.map((content,index)=>{
                       return(
                         <option value={content.designation_name} key={index}>{content.designation_name}</option>
                     )
                     })}
                   </Select>
                 </div>
                 <div className="col l6">
                    <h6 className="center pink-text">Select the Department</h6>
                    <Select l="12" xl="12" s="12" m="6" value={this.state.dept} onChange={this.handleDept}>
                    <option value="" disabled defaultValue>Select Here</option>
                    {this.state.department.map((content,index)=>{
                      return(
                        <option value={content.department_name} key={index}>{content.department_name}</option>
                    )
                    })}
                  </Select>
                 </div>
              </div>
          </div>
          <div className="col l8 form-signup">
                              <FetchUser dept={this.state.dept} desgn={this.state.desgn} showDiv={this.showDiv} />
          </div>
        </div>
        <div className="row">
            <ShowDiv display={this.state.display} username={this.state.username}/>
        </div>
      </div>
    );
  }
}


class FetchUser extends Component {
  constructor(props) {
    super(props)
    this.state={
      users:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser();
  }
  componentDidUpdate =(prevProps)=>{
    if((prevProps.dept!==this.props.dept))
    {
      this.fetchUser();
    }
  }
  fetchUser = ()=>{
    axios.post('/user/fetchUser',{department:this.props.dept,designation:this.props.desgn})
    .then( res => {
      // console.log(res.data)
        this.setState({users:res.data})
    });
  }
  render()
  {
    return(
      <React.Fragment>
              <table>
                  <thead>
                  <tr>
                     <th>Serial No.</th>
                     <th>Name</th>
                     <th>Official Id</th>
                     <th>Mail Id</th>
                     <th>Contact No</th>
                     <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  {this.state.users.map((content,index)=>{
                    return(
                      <tr key={index}>
                       <td>{index+1}</td>
                       <td>{content.name}</td>
                       <td>{content.username}</td>
                       <td>{content.mailid}</td>
                       <td>{content.phone}</td>
                       <td><div className="btn #fafafa grey lighten-5 pink-text" onClick={() => this.props.showDiv(content.username)}>View</div></td>
                      </tr>
                  )
                  })}

                  </tbody>
        </table>
      </React.Fragment>
    )
  }
}



class ViewByID extends Component {
  constructor() {
    super()
    this.state ={
      display:'none',
      username:'',
      redirectTo:'',
      agreed:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }

retriveValue =(e)=>{
  this.setState({[e.target.name]:e.target.value})
}
  Authenticate =(e)=>{
    e.preventDefault()
    axios.post('/user/authenticate',this.state)
    .then(res=>{
    if(res.data === "Allowed")
    {
      this.setState({
        display:'block',
      })
    }
    else{
      this.setState({display:'bloack'})
    }
        window.M.toast({html: res.data ,outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
    })
  }

  render()
  {
    return(
      <React.Fragment>
         <div className="row">
         <div className="col l3 xl3" />
           <div className="col l6 s12 m12 xl6">
               <div className="form-signup">
                  <div className="row">
                      <form onSubmit={this.Authenticate} >
                          <div className="input-field">
                             <input type="text" name="username" value={this.state.username} onChange={this.retriveValue} id="id" className="validate" required />
                             <label htmlFor="id">Input Official ID</label>
                          </div>
                          <button className="btn col l2 pink right" >View</button>
                      </form>
                  </div>
                  <br />
                  <div className="row">
                     <span className="green-text">DISCLAIMER: </span>Please Remember in this section you can only see the User's Data only if they are not coming in your same or upper designation level.
                     One more thing keep in mind users in medium level(like HOD , Professor) have only access to view datas within their department only.
                  </div>
                </div>
           </div>
         <div className="col l3 s12 m12 xl3" />
         </div>
         <div className="row">
                <div style={{display:this.state.display}}>
                   <ShowDiv display={this.state.display} username={this.state.username}/>
                </div>
         </div>
      </React.Fragment>
    );
  }
}



 class ShowDiv extends Component {
   constructor(props) {
     super(props)
     this.state={
         isChecked: false,
         choosed:'',
         agreed:[{name:'radio1',value:'Academic'},{name:'radio2',value:'Administrative'},{name:'radio3',value:'Research'},{name:'radio4',value:'Degrees'},{name:'radio5',value:'Section Datas'},],
     }
     this.componentDidMount = this.componentDidMount.bind(this)
   }
   componentDidMount()
   {

   }
   handleChecked =(e,index)=>{
       this.setState({
         isChecked: !this.state.isChecked,
         choosed: e.target.value
       });
   }
   render()
   {
     return(
       <React.Fragment>
           <div className="card" style={{display:this.props.display}}>
            <div className="row">
               <div className="col l2">
                  <div className="" style={{fontSize:'20px'}}>
                     You are seeing the profile of <span className="pink-text">{this.props.username}</span>
                  </div>
                 {this.state.agreed.map((content,index)=>(
                     <div className="row" key={index}>
                         <div className="col l10 s10 m10 form-signup">
                         <p>
                         <label>
                         <input type='radio' className="with-gap" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                         <span style={{color:'green'}}><b>{content.value}</b></span>
                         </label>
                         </p>
                         </div>
                   </div>
                 ))}
            </div>
            <div className="col l10" style={{display:this.props.display}}>
              <ShowData choosed={this.state.choosed} username={this.props.username}/>
            </div>
          </div>
        </div>
       </React.Fragment>
     )
   }
 }





class  ShowData extends Component {
  constructor(props) {
    super(props)
    this.state ={
      option:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleOption = (e) =>{
    this.setState({option:e.target.value})
  }
  componentDidMount()
  {
    this.getSectionData();
  }
  getSectionData()
  {
    axios.post('/user/fetch_section',{action:'Section'}).then(response =>{
      this.setState({
       SectionProps: response.data,
     })
    })
  }

  render()
  {
    let content;
    if(this.props.choosed === "Section Datas")
    {
      content =
        <React.Fragment >
          <div className="row">
          <div className="col l4" style={{fontSize:'20px',marginTop:'20px'}}>Select The Section Field From Here</div>
          <div className="col l7">
            <Select l="12" xl="12" s="12" m="6" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled defaultValue>Select Here</option>
            {this.state.SectionProps.map((content,index)=>{
              return(
                <option key={index} value={content.section_data_name}>{content.section_data_name}</option>
            )
            })}
          </Select>
          </div>
          </div>
          <SectionDataRender username={this.props.username} section_ref={this.state.option}/>
      </React.Fragment>
    }
    else{
      content =
      <div></div>
    }
    return(
      <div>
      {content}
      </div>
    )
  }
}


class SectionDataRender extends Component {
  constructor(props)
  {
    super(props)
    this.state={
      section_data:[],
    }
    this.componentDidMount= this.componentDidMount.bind(this)
    this.componentDidUpdate = this.componentDidUpdate.bind(this)
  }
  componentDidMount()
  {
    this.fetchSec();
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.section_ref!==this.props.section_ref)
    {
      this.fetchSec();
    }
    if(prevProps.username!==this.props.username)
    {
      this.fetchSec();
    }
  }
  fetchSec()
  {
    axios.post('/user/viewSection',{username:this.props.username,section_ref:this.props.section_ref})
    .then( res => {
        this.setState({section_data:res.data})
    });
  }
  render() {
    return (
      <div>
            <table>
                <thead>
                <tr>
                   <th className="center">Serial No</th>
                   <th className="center">Description of The Person's Work</th>
                   <th className="center">The Person's Role</th>
                   <th className="center">Person's Achivements</th>
                   <th className="center">Duration(start date-end date)</th>
                </tr>
                </thead>

                <tbody>
                {this.state.section_data.map((content,index)=>{
                  return(
                    <tr key={index}>
                     <td className="center">{index+1}</td>
                     <td className="center">{content.description}</td>
                     <td className="center">{content.role}</td>
                     <td className="center">{content.achivements}</td>
                     <td className="center">{content.date_of_starting}-{content.date_of_complete}</td>
                    </tr>
                )
                })}

                </tbody>
      </table>
      </div>
    );
  }
}
