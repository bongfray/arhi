
import React from 'react';
class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      datafrom:'',
      expired: false,
      completed: false,
      expire_year:'',
      value:'',
      username: props.username,
      serial:'',
      action:'',
      content:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      content: value,
    })

  }


componentDidMount(){
  var year = Date.today().toString("yyyy");
  var num = parseInt(year);
  this.setState({
    action:this.props.data.index,
    username: this.props.username,
    expire_year: num+5,
    expired:false,
    completed:false,
    datafrom:'Five',
  })
}



  handleSubmit(event,index) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
      <div>
      <div>
      <div className="row">
        <div className="col s12 l12" >
          <form onSubmit={this.handleSubmit}>
              <input
                className=""
                type="text"
                min="10"
                max="60"
                name={this.props.data.index}
                value={this.state.content}
                onChange={e => this.handleD(e,this.props.data.index)}
                placeholder="Enter Here..."
              />
            <div>
              <button className="btn right blue-grey darken-2 sup " type="submit">UPLOAD</button>
            </div>
          </form>
        </div>
      </div>
      </div>

      </div>
    )
  }
}

export default AddProduct;
