import React, { Component } from 'react';
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import ForeignExtra from './foreign_extra'
require("datejs")


export default class ownExtra extends Component {
  constructor()
  {
    super()
    this.state={
      isChecked: false,
      history:'',
      redirectTo:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleComp = (e) =>{
    this.setState({
      isChecked: !this.state.isChecked,
      history: e.target.value,
    })
  }
  componentDidMount()
  {

  }
  render() {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div>

        <div className="switch center">
            <label style={{color:'red',fontSize:'15px'}}>
                Extra Slot History
                <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                <span className="lever"></span>
                Request From Other Faculty to Handle Extra Slot
            </label>
        </div>
        <br />
        <br /><br />
        <InputValue datas={this.state.isChecked}/>
      </div>
      </React.Fragment>
    );
  }
}



class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
      date:'',
      dayorder:'',
      hour:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  addAllot =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }
  componentDidMount()
  {

  }
  handleCompensate = (e)=>{
    e.preventDefault();
    axios.post('/user/absent_compendte',{date:this.state.date,dayorder:this.state.dayorder,hour:this.state.hour})
    .then( res => {

    });
  }
  render(){
    if(this.props.datas === true)
    {
      return(
        <React.Fragment>
          <ForeignExtra />
        </React.Fragment>
      );
    }
    else{
      return(
        <React.Fragment>
        </React.Fragment>
      );
    }
  }
}
