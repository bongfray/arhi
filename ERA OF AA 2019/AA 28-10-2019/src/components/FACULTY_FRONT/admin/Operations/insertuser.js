import React, { Component} from 'react'
import { } from 'react-router-dom'
import NavAdd from '../CRUD_In_Input/NavHandle'
import AddSection from '../CRUD_In_Input/handleSection'
import Res from '../CRUD_In_Input/handleRes'
import InsertDesignation from '../CRUD_In_Input/insertDesignation'
import InsertDept from '../CRUD_In_Input/handleDept'
export default class InsertUser extends Component{
    constructor(){
        super();
        this.state = {
            username: '',
            pswd: ''
        }
    }
    handleInsert = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleSubmit(event) {
      event.preventDefault()

    }
    render(){
        if(this.props.select==='insertadmin')
        {
            return(
                <div className="row">
                <div className="col l3"/>
                <div className="col l6">
                <div className="input-field col s6">
								<input id="username" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleInsert} required />
								<label htmlFor="username">Username</label>
								</div>

								<div className="input-field col s6">
								<input id="password" type="password" className="validate" name="pswd" value={this.state.pswd} onChange={this.handleInsert} required />
								<label htmlFor="password">Password</label>
								</div>
								<button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Insert Admin</button>
                </div>
                <div className="col l3"/>
                </div>
                )
        }
        else if(this.props.select==='insert_in_nav')
        {
            return(
                  <NavAdd />
                )
        }
        else if(this.props.select==='responsibilty_percentages')
        {
            return(
                  <div><Res /></div>
                )
        }
        else if(this.props.select==='section_part_insert')
        {
            return(
                  <React.Fragment>
                     <div><AddSection /></div>
                  </React.Fragment>
                )
        }
        else if(this.props.select === 'insert_designation')
        {
          return(
            <div><InsertDesignation /></div>
          )
        }
        else if(this.props.select === 'insert_department')
        {
          return(
            <div><InsertDept/></div>
          )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}
