import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import ContentLoader from "react-content-loader"
import RegistrationStopped from './stop'
import {Select,Modal,Button,Icon} from 'react-materialize'
import {} from 'materialize-css'

var empty = require('is-empty');

export default class Signup extends Component {
	_isMounted = false;
	constructor() {
    super()
    this.initialState = {
			display:'block',
			status:'none',
			loader:true,
			redirectTo: null,
			suspension_status:false,
			h_order:'',
			title: '',
			name: '',
			mailid: '',
			username: '',
			phone: '',
			password: '',
			cnf_pswd: '',
			campus: '',
			dept: '',
			desgn: '',
		  dob:'',
		  count: 0,
			err:'',
			color:'green-text',
	}
	this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
		this.textInput = React.createRef();
    this.focusPasswordInput = this.focusPasswordInput.bind(this);
    }
		componentDidMount(){
			this._isMounted = true;
			axios.get('/user/fetch_render_status')
			.then(res=>{
			if (this._isMounted) {
				if(res)
				{
					if(res.data=== true)
					{
						this.setState({status:"block",loader:false})
					}
					else{
						this.setState({loader:false,status:'none'})
					}
			  }
				else
				{
					this.setState({loader:false,status:'none'})
				}
			}
			})
		}

		componentWillUnmount =()=> {
			this._isMounted = false;
		}

		handleField = (e) =>{
				this.setState({
					[e.target.name] : e.target.value,
				})
		}


					handleDesgn = (e) => {
						let order;
						if(e.target.value === "Director")
						{
							order = 1;
							this.setState({display:'none'});
						}
						else if(e.target.value === "Principle")
						{
							order = 2;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Assistant Director")
						{
							order =3;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Dean")
						{
							order =4;
							this.setState({display:'block'});
						}
						else if(e.target.value === "HOD")
						{
							order =5;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Professor")
						{
							order =6;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Assistant Professor")
						{
							order =7;
							this.setState({display:'block'});
						}
						else if(e.target.value === "Associate Professor")
						{
							order =8;
							this.setState({display:'block'});
						}
						this.setState({
								desgn: e.target.value,
								h_order: order,
						});
			    }

					focusPasswordInput() {
				    this.textInput.current.focus();
				  }
	handleSubmit(event) {
		event.preventDefault()
		if(empty(this.state.title)||empty(this.state.name)||empty(this.state.mailid)||empty(this.state.username)||empty(this.state.phone)||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.desgn))
		{
			window.M.toast({html: 'Enter all the Details',outDuration:'3000', classes:'rounded red'});
      return false;
		}
		// else if(!(this.state.password.match(/[a-z]/g) && this.state.password.match(
		// 					/[A-Z]/g) && this.state.password.match(
		// 					/[0-9]/g) && this.state.password.match(
		// 					/[^a-zA-Z\d]/g) && this.state.password.length >= 6))
		// {
		// 		this.setState({err:'Follow Correct Format!!',color:'red-text'})
		// 		this.focusPasswordInput();
		// }
		else if(this.state.password !==this.state.cnf_pswd)
		{
			this.setState({error:'Password does not match !!',cnf_pswd:''})
	        return false;
	  }
		else if ((this.state.phone).length!==10)
		{
			this.setState({error:'Verify your phone no !!',phone:''})
			return false;
		}
		else{
			window.M.toast({html: 'Signing UP....',outDuration:'3000', classes:'rounded violet'});
		axios.post('/user/', {
			suspension_status:this.state.suspension_status,
			h_order: this.state.h_order,
			username: this.state.username,
			password: this.state.password,
			title: this.state.title,
      name: this.state.name,
      mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
	    dept: this.state.dept,
	    desgn:this.state.desgn,
			dob: this.state.dob,
			count: this.state.count,
		})
			.then(response => {
				console.log(response)
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, outDuration:'4000', classes:'rounded #ba68c8 purple lighten-2'});
				    }
					else if(response.data.succ)
					{
						window.M.toast({html: response.data.succ, outDuration:'2000', classes:'rounded #ba68c8 purple lighten-2'});
						this.setState({
								redirectTo: '/flogin'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'2000', classes:'rounded red'});
			})
			this.setState(this.initialState)
	}
}
render() {
	const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="90" y="25" rx="0" ry="0" width="210" height="190" />
        </ContentLoader>
      )
	if(this.state.loader === true)
	{
		return(
			<MyLoader />
		);
	}
	else
	{
	if(this.state.status === "none")
	{
		return(
			<RegistrationStopped login_path="/flogin" section_name="FACULTY REGISTRATION"/>
		);
	}
	else{
	if(this.state.redirectTo) {
			 return <Redirect to={{ pathname: this.state.redirectTo }} />
	 } else {
	return (
		<div className="row">
		<div className="col s2 l2 m2 xl2" />
		<div className="col l8 xl8 s12 m12 form-signup">
				<div className="center">
						<h5 className="reg">REGISTRATION</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">
								<div className="col s2 l2 xl2 m2">
										<Select name="title" value={this.state.title} onChange={this.handleField}>
										<option value="" disabled defaultValue>Title</option>
										<option value="Mr.">Mr.</option>
										<option value="Mrs.">Mrs.</option>
										<option value="Miss.">Miss.</option>
										<option value="Dr.">Dr.</option>
										</Select>
								</div>

								<div className="input-field col s6 l6 xl6 m6">
								<input id="name" name="name" type="text" className="validate" value={this.state.name} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter your name" data-success=""></span>
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="fac_id" name="username" type="text" className="validate" value={this.state.username} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success=""></span>
								<label htmlFor="fac_id">Official ID</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s5 l5 xl5 m5">
								<input id="email" name="mailid" type="email" className="validate" value={this.state.mailid} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success="">Email id should be Official Mail Id</span>
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4 l4 xl4 m4">
								<input id="ph_num" type="text" name="phone" className="validate" value={this.state.phone} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success="">Phone number should be of 10 digit.</span>
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="input-field col s3 l3 xl3 m3">
								<input id="dob" type="text" name="dob" className="validate" value={this.state.dob} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success="">Format: dd/mm/yyyy</span>
								<label htmlFor="dob">D.O.B.</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6 xl6 l6 m6">
								<input ref={this.textInput}  onChange={this.handleField}  name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								<div>
								<Button
								 href="#modal1" className="modal-trigger N/A transparent" floating small waves="light" icon={<Icon className={this.state.color} tiny> help_outline </Icon>} />
								<Modal id="modal1">
								   <h5 className="center">Required Field in Password</h5>
									 <p className="center">
									 At least 1 uppercase character.<br />
          				 At least 1 lowercase character.<br />
									 At least 1 digit.<br />
									 At least 1 special character.<br />
									 Minimum 6 characters.<br />
									 </p>
								</Modal>
								</div>
								<span className="red-text"><p>{this.state.err}</p></span>
								</div>
								<div className="input-field col s6 xl6 m6 l6">
								<input  onChange={this.handleField} id="cnf_pswd" name="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								<span className="helper-text" data-error="Please enter data!!" data-success=""></span>
								</div>

						</div>
						<div className="row">

								<div className="col l4 s4 m4 xl4">
										<Select className="col l4 s6 m6 xl6" value={this.state.campus} name="campus" onChange={this.handleField}>
										<option value="" disabled defaultValue>Campus</option>
										<option value="Kattankulathur Campus">Kattankulathur Campus</option>
										<option value="Ramapuram Campus">Ramapuram Campus</option>
										<option value="Vadapalani Campus">Vadapalani Campus</option>
										<option value="NCR Campus">NCR Campus</option>
										</Select>
								</div>
								<div className="col l4 s4 m4 xl4">
										<Select value={this.state.desgn} onChange={this.handleDesgn}>
										<option value="" disabled defaultValue>Designation</option>
										<option value="Director">Director</option>
										<option value="Principle">Principle</option>
										<option value="Assistant Director">Assistant Director</option>
										<option value="Dean">Dean</option>
										<option value="HOD">HOD</option>
										<option value="Professor">Professor</option>
										<option value="Associate Professor">Associate Professor</option>
										<option value="Assistant Professor">Assistant Professor</option>
										</Select>
								</div>
								<div className="col l4 s4 m4 xl4" style={{display:this.state.display}}>
										<Select value={this.state.dept} name="dept" onChange={this.handleField}>
										<option value="" disabled defaultValue>Department</option>
										<option value="Computer Science">Computer Science</option>
										<option value="Information Technology">Information Technology</option>
										<option value="Software Engineering">Software</option>
										<option value="Mechanical Engineering">Mechanical</option>
										</Select>
								</div>
						</div>
						<br/>
						<div className="row"><div className="col l4 m4 s4 xl4 left">
							<Link to='/flogin' className="log">Login Instead ?</Link></div>
							<div className="col l4 s4 xl4 m4 center" style={{color:'red'}}>{this.state.error}</div>
							<div className="col l4 s4 m4 xl4">
							<button className="waves-effect btn blue-grey darken-2 sup" style={{width:'100%'}} onClick={this.handleSubmit}>Submit</button>
							</div>
						</div>
				</form>
		</div>
		</div>

	);
}
}
}
}
}
