import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from './colordiv.js'
import Insturction from './instructionmodal'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import { Button , Modal } from 'react-materialize'
require("datejs")

/*-------------------------------------------------Clock for Current Date and time------------------- */

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <span>{new Date().toDateString()}</span>
      <p>{this.state.time}</p>
      </div>
    );
  }
}



/*-----------------------------------------Automatically Chanege the day order-------------------------------*/


class DayOrder extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{
_isMounted = false;
  constructor() {
    super();
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      loading:true,
      opendir: '',
      modal: false,
      display:'block',
      saved_dayorder:'',
      redirectTo:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
  }

  getCount =() =>{
    axios.get('/user/knowcount', {
    })
    .then(response =>{
      console.log(response.data.count)
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  getDayOrder = () =>{
    axios.get('/user/fetchdayorder')
    .then(response =>{

    if (this._isMounted) {
        this.setState({loading:false})
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
        if(response.data.day_order===0)
        {
          this.setState({saved_dayorder:'0',display:'none'})
        }
        else
        {
          this.setState({
            saved_dayorder: response.data.day_order,
          });
        }
      }
    }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
      this._isMounted = true;
    M.AutoInit();
    this.getDayOrder();
    this.getCount();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };

      render(){
        const MyLoader = () => (
          <ContentLoader
            height={160}
            width={400}
            speed={2}
            primaryColor="#f3f3f3"
            secondaryColor="#c0c0c0"
          >
            <rect x="10" y="8" rx="5" ry="5" width="80" height="30" />
            <rect x="140" y="30" rx="3" ry="3" width="250" height="120" />

            <rect x="10" y="70" rx="5" ry="5" width="80" height="30" />
            <rect x="10" y="120" rx="1" ry="1" width="80" height="15" />


          </ContentLoader>
        )
        if(this.state.loading === true)
        {
          return(
            <MyLoader />
          );
        }
        else{
        if (this.state.redirectTo) {
             return <Redirect to={{ pathname: this.state.redirectTo }} />
         } else {

        return(
          <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div style={{padding:'20px'}}>
            <Insturction
                displayModal={this.state.modal}
                closeModal={this.selectModal}
            />
          <div className="row">
          <div className="col l2 s6 xl2 m2">
            <Clock />
          </div>
          <div className="col l10 s6 xl10 m10"><div className="row"><div className="col l4 s4 xl4 m4" />
            <p className="left tcr" style={{fontSize:'20px'}}>Today's DayOrder</p>
            </div>
          </div>
          </div>
          <div className="row">
          <div className="col l2 s12 xl2 m12">
          <Link to="/time_yes" style={{color: 'black'}} >Go to Yesterday's Dayorder</Link>
              <DayOrder day_order={this.state.saved_dayorder}/>
            <br />
          <div style={{display:this.state.display,marginTop:'70px'}}>
                <select value={this.state.opendir}  onChange={this.handledir}>
                  <option value="" disabled defaultValue>Select Here</option>
                  <option value="r_class">Regular Class</option>
                  <option value="own_ab">Absent</option>
                </select>
            </div>
        </div>
        <div className="col l10 s12 m12 xl10">
          <Content opendir={this.state.opendir} day_order={this.state.saved_dayorder} usern={this.props.username}/>
        </div>
        </div>
          </div>
          </React.Fragment>
        );
      }
    }
      }
    }




/*-----------------------------------------------This class is for dynamically allocating div ------------- */

class Content extends Component{
  constructor(){
    super();
    this.state={
      saved_dayorder: '',
      day:'',
      month:'',
      year:'',
      redirectTo:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
  }
  render(){
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.props.opendir==="r_class")
    {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
          return(
            <React.Fragment>
             <ColorRep usern={this.props.usern} day={day} month={month} year={year} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
         <Switch day_order={this.props.day_order} />
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select from the DropDown</div>
      );
    }

}
  }
}





class Switch extends React.Component {

    constructor ( props ) {
        super( props );

		this.state = {
			isChecked: false,
      history:'',
      absent_reason:'',
      absent_state:false,
		}
    this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      axios.get('/user/knowabsent')
      .then( res => {
          if(res.data === 'ok')
          {
              this.setState({absent_state:true})
          }
      });
    }
    handleAbsentReason =(e)=>{
      this.setState({absent_reason:e.target.value})
    }
    submitAbsent =(e)=>{
      e.preventDefault()
      axios.post('/user/absent_insert',{absent_reason:this.state.absent_reason})
      .then( res => {
        if(res.data)
        {
          this.setState({absent_state:true})
        }
      });
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
    }
    render () {

        return(
          <div>
          {this.state.absent_state === false ?
          <div className="row">
          <label className="pure-material-textfield-outlined alignfull">
            <textarea
              className="area"
              type="text"
              placeholder=" "
              min="10"
              max="60"
              value={this.state.absent_reason}
              onChange={this.handleAbsentReason}
            />
            <span>Enter Reason About the Absent</span>
          </label>
          <button className="btn right" onClick={this.submitAbsent}>SUBMIT</button>
          <div className="col l6">
          <span className="green-text"><b>NOTE : </b></span>If you want to handover some of your allotted slots to
          other faculty,first submit the reason then only you will see that part.
          </div>
          </div>
          :
          <React.Fragment>
          <div className="row">
            <div className="col l6 switch center">
                <label style={{color:'red',fontSize:'15px'}}>
                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    Want to Handover some slots to Other Faculty?
                </label>
            </div>
            <div className="col l6">
            <span className="green-text"><b>REMEMBER : </b></span>You should enter some faculty's id and the slot,
            which should not come under their alloted slot,and must be in your alloted slot.One more thing,the
            faculty should be of same designation and in same campus.Whatever request you are making will be
            render for today only. Tommorow all the request you are making today,will be invalid. So request
            the respective faculty to complete within today.
            </div>
            </div>
            <br />
            <br /><br />
            <InputValue datas={this.state.isChecked}  day_order={this.props.day_order}/>
            </React.Fragment>
          }
          </div>

        );
    }

}

class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
    };
  }
  render(){
    if(this.props.datas === true)
    {
      return(
        <React.Fragment>
           <HandOverSlot  day_order={this.props.day_order}/>
        </React.Fragment>
      );
    }
    else{
      return(
        <React.Fragment>
        </React.Fragment>
      );
    }
  }
}


class HandOverSlot extends Component {
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  axios.get('/user/')
  .then( res => {
      if(res.data)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/faculty'})
      }
  });
}
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
     addroute="/user/add_handover_slots";
     editroute = "";
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })

   }
   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Faculty ID",
                   name: "faculty_id",
                   placeholder: "Enter the Faculty ID",
                   type: "text",
                   grid: 2,
                   div:"col l3 xl3 s2 m2 center",
                 },
                 {
                   header: "Mention Hour",
                   name: "hour",
                   placeholder: "Mention the Hour",
                   type: "text",
                   grid: 2,
                   div:"col l3 xl3 s2 m2 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <Add action="handover_slots"  day_order={this.props.day_order} data={data1} order_from={this.state.username} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div>
    {!this.state.isAddProduct && <ShowAndEdit action="handover_slots" data={data1} />}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</React.Fragment>
);
}
}
}




class Add extends Component{
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      val:'',
      link:'',
      action:'',
      order_from:'',
      faculty_id:'',
      hour:'',
      status:false,
      day_order:'',
      slot:'',
      expired:'',
      day:'',
      month:'',
      year:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


  componentDidMount(){
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    this.setState({
      action:this.props.action,
      order_from:this.props.order_from,
      status:false,
      day:day,
      month:month,
      year:year
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      if((this.state.faculty_id)  && (this.state.hour ))
      {
        axios.post('/user/check_possibility',{id:this.state.faculty_id,hour:this.state.hour,day_order:this.props.day_order})
        .then( res => {
          console.log(res.data)
          if(res.data === 'no')
          {
            window.M.toast({html: 'Kindly Read all the rules !!',outDuration:'9000', classes:'rounded  red lighten-1'});
            return false;
          }
          else
          {
                this.setState({slot:res.data})
                this.props.onFormSubmit(this.state);
                this.setState(this.initialState);
          }
        });

      }
      else{
        window.M.toast({html: 'Enter all the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
      }
  }

  render() {
    return(
    <React.Fragment>
      <div className="row">
        <div className="col s12 l12" >
          <form onSubmit={this.handleSubmit}>
          {this.props.data.fielddata.map((content,index)=>(
            <div className="col l6 s6 m6" key={index}>
            <div className="input-field">
              <input
                id={content.name}
                className=""
                type={content.type}
                placeholder=" "
                min="10"
                max="60"
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              />
              <label htmlFor={content.name}>{content.placeholder}</label>
            </div>
            </div>
          ))}
            <div>
              <button className="btn right blue-grey darken-2 sup " type="submit">UPLOAD</button>
            </div>
          </form>
        </div>
      </div>
      </React.Fragment>
    )
  }
}


class ShowAndEdit extends Component{
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
      covered:[],
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch(this.props.action)
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
  }

  deleteProduct = (productId,index) => {
      this.deleteOperation(productId,index);
}
deleteOperation= (productId,index) =>{
  var delroute;
    delroute = '/user/del_handover_slots';

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  var today= Date.parse("today").toString("dd");
  let fetchroute;

    fetchroute = "/user/fetch_handover_slots";

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
    expire_date:today,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
showTopics =(id,slot)=>{
  axios.post('/user/fetchCovered',{faculty_id:id,slot:slot})
  .then( res => {
    console.log(res.data)
      this.setState({covered:res.data})
  });
}
  render() {

    const {products} = this.state;
      return(
        <div>
            <div className="row">
                <div className="col s2 l2 m2 xl2 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className={content.div} key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s2 l2 m1 xl2 center"><b>Status</b></div>
                <div className="col s2 l2 xl2 m2 center"><b>Action</b></div>
            </div>
            <hr />
            <div>
              {products.map((product,index) => (
                <div className="row" key={product.serial}>
                  <div className="col l2 xl2 m1 s1 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className={content.div} key={index}>{product[content.name]}</div>
                  ))}
                  <div className="col l2 xl2 m1 s1 center">{product.status === true ?
                  <div className="center green-text">Completed</div> : <div className="center red-text">Pending</div>
                  }</div>
                  {product.status === false ?
                    <React.Fragment>
                    <div className="center col s2 m2 l2 xl2 hide-on-small-only">
                    <button className="btn-small red" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </div>
                  <div className="col s2  hide-on-med-and-up">
                    <i className="material-icons go" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                  </React.Fragment>
                  :
                  <div>
                  <Button href="#modal1" onClick={() => this.showTopics(product.faculty_id,product.slot)} className="modal-trigger right btn-small" title="Click when completed">Coverd Topic</Button>
                  <Modal id="modal1">
                               <div className="col l12">
                                 {this.state.covered.map((content,index) => (
                                   <React.Fragment>
                                   <div className="row">
                                      <div className="col l6">Slot Handled</div>
                                      <div className="col l6">{content.slot}</div>
                                   </div>
                                   <div className="row">
                                      <div className="col l6">Topics Covered</div>
                                      <div className="col l6">{content.handledtopic}</div>
                                   </div>
                                   <div className="row">
                                      <div className="col l6">Handled By</div>
                                      <div className="col l6">{content.handled_by}</div>
                                   </div>
                                   </React.Fragment>
                                 )
                               )}
                               </div>
                 </Modal>
                  </div>
                }
                </div>
              ))}
            </div>
        </div>
      )

  }
}
