import React, {  } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
var empty = require('is-empty');

export default class Cancel extends React.Component {
  constructor() {
    super()
    this.state = {
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        c_cancelled: '',
        compensation:'false',
    }
    this.handleMissComp = this.handleMissComp.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    }

    closeModal=()=>{
      this.props.closeModal();
    }
    colorChange=()=>{
      this.props.color()
    }

    handleCancel =(e) =>{
      this.setState({
          c_cancelled: e.target.value,
      })
    }


    handleCancelSlot =(e) =>{
      if(empty(this.state.c_cancelled))
      {
        window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else {
      e.preventDefault()
      Date.prototype.getWeek = function () {
    	    var onejan = new Date(this.getFullYear(), 0, 1);
    	    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    	};

    	var myDate = new Date.today();
    	var week =myDate.getWeek();


      axios.post('/user/timecancel', {
        usern: this.props.usern,
        order:this.props.day_order+this.props.day_slot_time+'Cancel',
        day_slot_time: this.props.day_order+this.props.day_slot_time,
        compensation_status: this.state.compensation,
        c_cancelled: this.state.c_cancelled,
        date:this.props.day,
        month: this.props.month,
        year: this.props.year,
        week:week,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.succ)
            {
              window.M.toast({html: 'Success !!', outDuration:'1200', inDuration:'1200', classes:'rounded #ec407a green lighten-1'});
              this.closeModal();
              this.colorChange();
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          c_cancelled:'',
      })
}

    }

    handleMissComp = (e) =>{
      this.setState({
        compensation: e.target.value,
      })
    }

  componentDidMount()
  {
  }


  updateAllotV (userObject) {
    // alert(userObject)
    this.setState(userObject)
  }

render(){
  return(
    <div>
    <label className="pure-material-textfield-outlined alignfull">
      <textarea
        className="area"
        type="text"
        placeholder=" "
        min="10"
        max="60"
        value={this.state.c_cancelled}
        onChange={this.handleCancel}
      />
      <span>Enter Details About the Problem </span>
    </label><br />
    <p className="need">
     <label>
       <input type="checkbox" value="true" onChange={this.handleMissComp} />
       <span style={{color:'black'}}>Would you like to Get a extra slot for your work completion in this month ? </span>
     </label>
   </p>
   <div className="right btn-of-submit-at-time"><Link className="btn" to="#" onClick={this.handleCancelSlot} >SUBMIT</Link>
   </div>
    </div>
  );
}
}
