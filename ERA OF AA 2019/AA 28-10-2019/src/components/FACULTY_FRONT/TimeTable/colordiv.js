import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Allot2 from './slot2'

export default class Color extends Component {
	constructor(props) {
    super(props)
    this.state = {
					day:[
						{id:".1",time:"8:00-8:50",number:1},
						{id:".2",time:"8:50-9:40",number:2},
						{id:".3",time:"9:45-10:35",number:3},
						{id:".4",time:"10:40-11:30",number:4},
						{id:".5",time:"11:35-12:25",number:5},
						{id:".6",time:"12:30-1:20",number:6},
						{id:".7",time:"1:25-2:15",number:7},
						{id:".8",time:"2:20-3:10",number:8},
						{id:".9",time:"3:15-4:05",number:9},
						{id:".10",time:"4:05-4:55",number:10}
					],
    }
		this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {

    }


render() {
	return(
		<div className="rocontent" style={{marginTop:'80px'}}>
	{this.state.day.map((content,index)=>{
			return(
				<div key={index}>
				<Allotment usern={this.props.usern} day={this.props.day} month={this.props.month} year={this.props.year}
				 day_order={this.props.day_order} time={content.time} num={content.number} day_slot_time={content.id} />
				</div>
			);
})}
</div>
);
}
}


class Allotment extends Component{
	constructor(props)
	{
		super(props)
		this.state ={
			loading: true,
			color:'red',
			alreadyhave:'',
			day:'',
			month:'',
			year:'',
			block:false,

		}
this.componentDidMount = this.componentDidMount.bind(this)
	}
collect()
{
	axios.post('/user/fetchme', {
		slot_time: this.props.day_order+this.props.day_slot_time,
	}
)
	.then(response =>{
		if(response.data)
		{
			  let allotdata = (response.data.alloted_slots).toUpperCase();
				this.setState({
					alreadyhave: allotdata,
					loading:false,
				})
		}
		else{
			this.setState({
				alreadyhave: 'Free Slot',
				loading:false,
			})
		}
	})
}
color =() =>{
	axios.post('/user/fetchfrom', {
		slot_time: this.props.day_order+this.props.day_slot_time,
		day: this.props.day,
		month: this.props.month,
		year: this.props.year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
					loading:false,
				})
		}
	})

}

block =()=>{
	axios.post('/user/knowabsent',{	slot_time: this.props.day_order+this.props.day_slot_time})
	.then(res => {
		if(res.data === 'no')
		{
			this.setState({
				block:'block',
			})
		}
		else if(res.data === 'yes')
		{
			this.setState({
				block:'none',
			})
		}
		else{
			this.setState({
				block:'block',
			})
		}
	});
}
	componentDidMount()
	{
		this.collect();
		this.color();
		this.block();
	}

	render()
	{
		if(this.state.loading === true)
		{
			return(
				<div><h6 className="center col l4 s12 xl4 m4">Loading Content...</h6></div>
			)
		}
		else
		{
		if(this.state.color === "red")
		{

			return(
				<div>
				<div className="col l4 s12" style={{color: 'red',display:this.state.block}} key={this.props.time}>
							<Allot2 color={this.color} num={this.props.num} day={this.props.day} month={this.props.month} year ={this.props.year}
							 usern={this.props.usern} time={this.props.time} slots={this.state.alreadyhave} day_order={this.props.day_order} day_slot_time={this.props.day_slot_time}/>
				</div>
				</div>
			);
		}
		else if(this.state.color === "green")
		{
			return(
				<div>
				<div className="col l4 s12 xl4 m4" style={{color: this.state.color}}  key={this.props.time}>
						<div style={{backgroundColor:'white'}} className="card hoverable each_time">
						  <p className="left blue-text" style={{marginLeft:'5px'}}>{this.props.num}</p>
							<p className="center">{this.props.time}</p>
							<p className="center"><b>COMPLETED</b></p>
						</div>
				</div>
				</div>
			);
		}
}

	}
}
