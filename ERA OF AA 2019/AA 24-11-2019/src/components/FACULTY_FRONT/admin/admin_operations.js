import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Nav from '../../dynnav'
import Load from './loader_simple'

import Display from './Operations/display'


export default class Ap extends Component{
  constructor() {
    super()
    this.state={
      isChecked:0,
      active:0,
      logout:'/user/logout',
      get:'/user/',
      nav_route: '',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
      this.fetchlogin();
  }
  fetchlogin = () =>{
      axios.get('/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
  handleComp = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }
  toggleDiv =()=>{
    this.setState({toggled:!this.state.toggled})
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "col l3 xl3 s6 m3 #00695c teal darken-3 white-text center go active-pressed";
      }
      return "col l3 xl3 s6 m3 center go";
  }

  render()
  {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
    return(
        <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div className="center">
               {this.state.toggled ===  false && <React.Fragment><i className="dig small material-icons go hide-on-med-and-down" onClick={this.toggleDiv}>arrow_drop_down_circle</i>
               <i className="dig small material-icons go hide-on-large-only" style={{marginTop:'30px'}} onClick={this.toggleDiv}>arrow_drop_down_circle</i>
               </React.Fragment>}
               {this.state.toggled ===  true && <div className="card dig center">
               <div className="row" style={{marginTop:'15px'}}>
                      <div className="col l2 xl2 hide-on-mid-and-down"/>
                      <div className="col l8 xl s12 m12">
                         <ul className="row">
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(0)}} className={this.color(0)}><b>System</b></li>
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(1)}} className={this.color(1)}><b>Admin Operations</b></li>
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(2)}} className={this.color(2)}><b>ARHI</b></li>
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(3)}} className={this.color(3)}><b>Complaints/Report</b></li>
                         </ul>
                       </div>
                       <div className="col l2 xl2 hide-on-mid-and-down"/>
               </div>
               <i class="small material-icons go" onClick={this.toggleDiv}>arrow_upward</i>
               </div>
               }
          </div>
          <div className="hide-on-med-and-down" style={{marginTop:'17px'}}>
              <System choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>
          <div className="hide-on-large-only" style={{marginTop:'60px'}}>
              <System choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>

       </React.Fragment>
    )
  }
  }
}


class System extends Component {
  constructor() {
    super()
    this.state={
      activated_user:'',
      suspended_user:'',
      suser:'',
      user:'',
      total_user:'',
      d_user:'',
      admin_user:'',
      fetch_system:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser()
  }
  fetchUser()
  {
    axios.get('/user/know_no_of_user')
    .then( res => {
      const activated_user = (res.data.filter(item => item.active === true)).length;
      const suspended_user = (res.data.filter(item => item.suspension_status === true)).length;
      const user = (res.data.filter(item => item.count=== 0 || item.count === 1 || item.count === 2)).length;
      const admin_user = (res.data.filter(item => item.h_order=== 0)).length;
      const d_user = (res.data.filter(item => item.h_order=== 0.5)).length;
        this.setState({activated_user:activated_user,suspended_user:suspended_user,admin_user:admin_user,user:user,d_user:d_user})
        this.fetchStudent(res.data.length);
    });

  }
  fetchStudent(user)
  {
    axios.get('/user2/know_no_of_suser')
    .then( res => {
        let total =res.data.length;
        this.setState({total_user:user+total,suser:res.data.length,fetch_system:false})
    });
  }
  render()
  {
    if(this.props.choice === 1)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Admin />
        }
        </React.Fragment>
      )
    }
    else if(this.props.choice === 3)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Complaints />
        }
        </React.Fragment>
      )
    }
    else{
      return(
        <React.Fragment>
        <h5 className="center con teal-text" style={{marginTop:'50px'}}>Welcome To EWork Admin Panel</h5><br /><br />
        {this.state.fetch_system ?
          <Load />
          :
         <div className="row">
            <div className="col l2">
               <div className="card">
               <div className="pink white-text center">TOTAL USER</div>
               <div className="card-content center">
                <h5>{this.state.total_user}</h5>
               </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
               <div className="pink white-text center">FACULTY</div>
               <div className="card-content center">
                <h5>{this.state.user}</h5>
               </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
               <div className="pink white-text center">STUDENT</div>
               <div className="card-content center">
                <h5>{this.state.suser}</h5>
               </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
                 <div className="pink white-text center">DEPARTMNET ADMIN</div>
                 <div className="card-content center">
                   <h5>{this.state.d_user}</h5>
                 </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
                 <div className="pink white-text center">ADMIN USER</div>
                 <div className="card-content center">
                   <h5>{this.state.admin_user}</h5>
                 </div>

               </div>
            </div>
            <div className="col l2">
                <div className="card">
                  <div className="pink white-text center">SUSPENDED USER</div>
                  <div className="card-content center">
                    <h5>{this.state.suspended_user}</h5>
                  </div>
                </div>
            </div>
         </div>
        }
        </React.Fragment>
      )
    }
  }
}


class Admin extends Component{
constructor(){
    super();
    this.state ={
        radio:[{name:'radio1',color:'blue',value:'Insert'},{name:'radio2',color:'black',value:'Update'},{name:'radio3',color:'orange',value:'Super'},{name:'radio4',color:'red',value:'Delete'},{name:'radio5',color:'yellow',value:'Modify'},{name:'radio6',color:'aqua',value:'View'}],
        isChecked: false,
        choosed: '',
        active:'',
    }
}

handleChecked =(e,index,color)=>{
    this.setState({
        isChecked: !this.state.isChecked,
        choosed: e.target.value
    });
    if (this.state.active === index) {
      this.setState({active : null})
    } else {
      this.setState({active : index})
    }
}
color =(position) =>{
  if (this.state.active === position) {
      return "col l10 s10 m10 form-signup #18ffff cyan accent-2";
    }
    return "col l10 s10 m10 form-signup black-text";
}

render()
{

    return(
        <React.Fragment>
       <div className="row">
            {this.state.radio.map((content,index)=>(
                <div className="col s6 l2 m2" key={index}>
                    <div className={this.color(index)}>
                    <p>
                    <label>
                    <input type='radio' className="yellow" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                    <span style={{letterSpacing: '.1em'}} className="black-text"><b>{content.value}</b></span>
                    </label>
                    </p>
                    </div>
              </div>
            ))}
       </div>

            <div className="row">
                <div className="col s12 m12 l12">
                   <Display choosed={this.state.choosed}/>
                </div>
            </div>
        </React.Fragment>
    );
}
}



class Complaints extends Component {
  constructor() {
    super()
    this.state={
      complaint:[],
      complaint_details:'',
      complaint_subject:'',
      official_id:'',
      serial_no:0,
      reply:false,
      forward:false,
      sendto:'',
      message_for_receiver:'',
      active:'',
      complaint_fetching:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchComplaint();
  }


  color =(position) =>{
    if (this.state.active === position) {
        return "row particular #e3f2fd blue lighten-5 black-text";
      }
      return "row particular";
  }


  fetchComplaint =()=>{
    axios.get('/user/fetch_complaint')
    .then( res => {
        if(res.data){
          this.setState({complaint:res.data,complaint_fetching:false})
        }
    });
  }

    sendData =(index,official_id,complaint_subject,complaint,serial)=>{
      if (this.state.active === index) {
        this.setState({active : null})
      } else {
        this.setState({active : index})
      }
      this.setState({official_id:official_id,complaint_subject:complaint_subject,complaint_details:complaint,serial_no:serial})
    }
    onReply =(e)=>{
      if(e === 0)
      {
        this.setState({reply:true,forward:false})
      }
      else
      {
        this.setState({forward:true,reply:false})
      }
    }

    handleInput =(e)=>{
      this.setState({[e.target.name]:e.target.value})
    }

    sendReply =(no,id)=>{
      const { complaint } = this.state;
      if(!(this.state.reply_message))
      {
        window.M.toast({html: 'Enter all the Details !!',outDuration:'9000', classes:'rounded yellow black-text'});
      }
      else{
      axios.post('/user/send_reply_to_complaint',{serial:no,official_id:id,message:this.state.reply_message})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({reply:false,message:false,complaint: complaint.filter(product => product.serial !== no),reply_message:''})
          window.M.toast({html: 'Replied !!',outDuration:'9000', classes:'rounded green darken-2'});
        }
      });
     }
    }
    forwardMsg =() =>{
    const { complaint } = this.state;
    if(!(this.state.sendto) || !(this.state.message_for_receiver))
    {
      window.M.toast({html: 'Enter all the Details !!',outDuration:'9000', classes:'rounded yellow black-text'});
    }
    else{
      axios.post('/user/forward_complaint',{data:this.state})
      .then( res => {
        if(res.data === 'ok')
        {
          this.setState({forward:false,message:false,complaint: complaint.filter(product => product.serial !== this.state.serial_no),sendto:'',message_for_receiver:''})
          window.M.toast({html: 'FORWARDED !!',outDuration:'9000', classes:'rounded green darken-2'});
        }
      });
      }
    }

    deleteOne =(id)=>{
      const { complaint } = this.state;
      axios.post('/user/delete_id',{serial:id})
      .then( res => {
          if(res.data)
          {
            this.setState({complaint: complaint.filter(product => product.serial !== id)})
          }
      });
    }

  render()
  {
    return(
      <React.Fragment>
      {this.state.complaint_fetching === true ?
        <Load />
        :
        <React.Fragment>
      {this.state.complaint.length===0 ?
        <h4 className="center" style={{marginTop:'120px'}}>Can't able to find any complaint !!</h4>
        :
      <div className="row">
         <div className="col l3 xl3 s12 m4">
            <div className="heading">
                <div className="row">
                  <div className="col l12">
                     <h6 className="ework-color center"><b>Complaints or Report</b></h6>
                  </div>
                </div>
            </div>
            <hr />
              {this.state.complaint.map((content,index)=>{
                return(
                  <div key={index} className={this.color(index)} onClick={() => this.sendData(index,content.official_id,content.complaint_subject,content.complaint,content.serial)}>
                    <div className="col l8 xl8 s8 m8">
                      <h6 className="dotted">{content.complaint_subject}</h6>
                      <label className="dotted hide-on-med-and-down">{content.complaint}</label>
                    </div>
                    <div className="col l4 xl4 s4 m4">
                       <i style={{marginTop:'20px'}} onClick={()=>this.deleteOne(content.serial)} className="small right material-icons go">delete</i>
                    </div>
                  </div>
                );
              })}
         </div>
         {this.state.official_id &&
         <div className="col l9 xl9 s12 m12">
            <div className="form-signup">
                 <React.Fragment>
                    <div className="row">
                      <div className="col l6 s12 m6 xl6">
                             <h6 className="center pink white-text" style={{padding:'8px'}}>COMPLAINT DETAILS</h6>
                              <div className="row">
                                 <div className="col l6 xl6 s6 m6"><b>Complaint From : </b></div><div className="col l6 xl6 s6 m6">{this.state.official_id}</div>
                              </div>
                              <div className="row">
                                 <div className="col l6 xl6 s6 m6"><b>Complaint Subject : </b></div><div className=" col l6 xl6 s6 m6">{this.state.complaint_subject}</div>
                              </div>
                              <div className="row">
                                 <div className="col l6 xl6 xl6 s6 m6"><b>Complaint Details : </b></div>
                              </div>
                              <div className="row" style={{wordWrap: 'break-word',padding:'20px'}}>
                                 {this.state.complaint_details}
                              </div>
                              <div className="row">
                              <div className="col l6 xl6 s6 m6"><span className="styled-btn" onClick={()=>this.onReply(0)}>Reply</span></div>
                              <div className="col l6 xl6 s6 m6"><span className="styled-btn right" onClick={()=>this.onReply(1)}>Forward</span></div>
                              </div>
                      </div>
                      <div className="col l1 xl1 m1 hide-on-small-only cr" />
                      <div className="col l5 xl5 s12 m6">
                          {this.state.reply &&
                             <React.Fragment>
                                <div className="row">
                                      <h6 className="center pink white-text" style={{padding:'8px'}}>REPLY TO USER</h6>
                                      <div className="row">
                                        <div className="col l6 xl6 s6 m6">
                                          <label className="">To : </label><span>{this.state.official_id}</span>
                                        </div>
                                      </div>
                                      <div className="row">
                                        <div className="col l12 xl12 s12 m12">
                                          <label className="pure-material-textfield-outlined alignfull">
                                            <textarea
                                              className=""
                                              type="text"
                                              placeholder=" "
                                              min="10"
                                              max="60"
                                              name="reply_message"
                                              value={this.state.reply_message}
                                              onChange={this.handleInput}
                                            />
                                            <span>Enter the Message</span>
                                          </label>
                                        </div>
                                      </div>
                                      <button className="right btn" onClick={()=>this.sendReply(this.state.serial_no,this.state.official_id)} style={{marginBottom:'5px'}}>REPLY</button>
                                </div>
                                <span className="center red-text">Note : On your reply this complaint will be closed !!</span>
                             </React.Fragment>
                          }
                          {this.state.forward &&
                             <React.Fragment>
                                <div className="row">
                                <h6 className="center pink white-text" style={{padding:'8px'}}>FORWARD MESSAGE</h6>
                                   <div className="input-field">
                                      <input id="forward-to" name="sendto" value={this.state.sendto} className="validate" onChange={this.handleInput} type="text" required/>
                                      <label htmlFor="forward-to">Send To</label>
                                       <span className="helper-text" data-error="Please enter the data !!" data-success="">Should be Official Id</span>
                                   </div>
                                </div>
                                <div style={{padding:'3px'}}>
                                  <div className="row">
                                     <label className="">Complaint From : </label><span>{this.state.official_id}</span>
                                  </div>
                                  <div className="row">
                                     <label className="">Complaint Subject : </label><span>{this.state.complaint_subject}</span>
                                  </div>
                                </div>
                                <div className="row">
                                   <div className="input-field">
                                      <input id="mm" name="message_for_receiver" className="validate" value={this.state.message_for_receiver} onChange={this.handleInput} type="text" required/>
                                      <label htmlFor="mm">Any Message For Receiver</label>
                                   </div>
                                </div>
                                <div className="row">
                                <button className="btn right" onClick={this.forwardMsg}>Forward</button>
                                </div>
                                <div className="row">
                                  <span className="red-text">Remember - </span> Once you forward this complaint, this complaint
                                  will be inactive for the admin user. The person who will receive this message can only answer or delete it.
                                </div>
                             </React.Fragment>
                          }
                      </div>
                    </div>
                 </React.Fragment>

            </div>
         </div>
           }
      </div>
        }
        </React.Fragment>
      }
      </React.Fragment>
    );
  }
}
