const express = require('express')
const route = express.Router()
const bcrypt = require('bcryptjs');
const Suser = require('../database/models/STUD/suser')
const passport = require('../passport/stud_pass')
const NavS = require('../database/models/STUD/navstud')
const AdminOrder = require('../database/models/FAC/admin')
const Admin_Operation = require('../database/models/FAC/admin_operation')
const Resume = require('../database/models/STUD/resume_schema')
const Task = require('../database/models/STUD/task')
const Arhi = require('../database/models/STUD/path')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')
console.log("Student Server")

route.get('/fetch_srender_status', function(req, res) {
  AdminOrder.findOne({usertype:'stud'}, function(err, result){
    if(err)
    {

    }
    else if(result)
    {
      res.send(result.registration_status)
    }
  })
})




route.post('/ssignup', (req, res) => {
console.log("Stud REG");
    const { regid, password,name,mailid,phone,campus,dept,dob,count,degree,st_year,year} = req.body
    Suser.findOne({ username: regid }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          console.log("Way to signup")
            const newUser = new Suser({
              suspension_status:false,
              username: regid,
              password: password,
              name: name,
              mailid: mailid,
              phone: phone,
              campus: campus,
              dept: dept,
              dob: dob,
              degree: degree,
              st_year: st_year,
              year: year,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              count: count,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})


route.post(
    '/slogin',(req, res, next) => {
      const { username } = req.body;
      Suser.findOne({ username: username }, function(err, objs){
        if(err)
        {
          console.log(err)
        }
        else if(objs)
        {
         if (objs.count === "0")
          {
              var counter = 0;
              Suser.updateOne({ username: username }, {count: counter+1},(err, user) => {
              })
          }
          else if (objs.count === "1")
          {
              var coun = 0;
              Suser.updateOne({ username: username }, {count: coun+2},(err, user) => {
              })
          }
        }
      });
        next()
    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)

route.get('/getstudent', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

route.post('/slogout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

route.post('/sprofile', (req, res) => {

    const {up_username,up_phone,up_name,up_dob} = req.body;
    Suser.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)

route.get('/getstudentprofile', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    Suser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});

    route.post('/sforgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       Suser.findOne(
         {mailid: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'Invalid User'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           console.log(token);
           Suser.updateOne({mailid:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 900000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com',
            to: mailid,
            subject: 'Reset Password:  eWorks',
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 15min</p>'
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })

        })



        route.post('/addnav', function(req,res) {
                  var trans = new NavS(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });


/*-------------------------------------------------------------Fetch Request From Nav Bar---------------------- */
        route.get('/fetch_snav',function(req,res) {
          NavS.find({ }, function(err, docs){
          if(err)
          {
            console.log(err)
          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/send_task', function(req,res) {
        Task.findOne({referTask:req.body.referTask},(err,found)=>{

          if(err)
          {

          }
          else if(found)
          {
            Task.updateOne({referTask:req.body.referTask},{$set:{description:req.body.description,link:req.body.link,start_date:req.body.start_date,end_date:req.body.end_date}},(err,done)=>{
              if(err)
              {

              }
              else if(done)
              {
                res.send('updated');
              }
            })
          }
          else{
              var respon = new Task({
                username:req.user.username,
                task_name:'Task '+req.body.task_name,
                level:req.body.level,
                description:req.body.description,
                link:req.body.link,
                start_date:req.body.start_date,
                end_date:req.body.end_date,
                referTask:req.body.referTask,
              });
              respon.save((err, savedUser) => {
                if(err)
                {

                }
                else if(savedUser){
                  res.send('done');
                  }
              })
          }

        })
      });

      route.post('/fetchTasks',function(req,res) {
        console.log(req.body)
        Task.find({$and:[{level: req.body.level},{task_name:req.body.action},{username:req.user.username}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send(docs)
        }
        });
      })

      route.post('/deleteTask',function(req,res) {
        Task.deleteOne({$and:[{referTask: req.body.key},{username:req.user.username}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send('del')
        }
        });
      })



/*-------------------------------------------------------------Fetch Request From Admin Panel---------------------- */
        route.post('/fetch_student_nav',function(req,res) {
          NavS.find({action: req.body.action}, function(err, docs){
          if(err)
          {
            console.log(err)
          }
          else
          {
            res.send(docs)
          }
        });
        })


        route.post('/editnav', function(req,res) {
          NavS.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
            var succ= {
              succ: "Updated"
            };
            res.send(succ);
          })

        });

        route.post('/edit_existing_nav',function(req,res) {
          NavS.findOne({serial: req.body.id}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/delnav',function(req,res) {
          NavS.remove({serial:  req.body.serial },function(err,succ){
            res.send("OK")
          });
        })




        route.post('/handleStudent_Registration',function(req,res) {

        AdminOrder.findOne({usertype: req.body.history}, function(err, docs){
          if(err)
          {

          }
          else if(docs)
          {
            AdminOrder.updateOne({usertype:req.body.history},{registration_status: req.body.checked},function(err,done){
              res.send("Done")
            })
          }
          else{
            var trans = new AdminOrder({
              usertype:req.body.history,
              registration_status: req.body.checked
            }
            );
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
          }
        })
        })



        route.get('/stud_reg_status', (req, res, next) => {
          AdminOrder.findOne({usertype:'stud'},function(err,result){
            if(err)
            {

            }
            else if(result)
            {
              res.send(result.registration_status)
            }
          })
        })



        /*----------------------------------------RESUME CODE0--------------------------------------------------------- */
        route.post('/addmm', function(req,res) {
                  var trans = new Resume(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });

          route.post('/editt', function(req,res) {
            Resume.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })

          });


      route.post('/fetchall',function(req,res) {
        console.log(req.body)
          Resume.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
            if(err)
            {

            }
            else
            {
              res.send(docs)
            }
        });
      })

      route.post('/fetcheditdata',function(req,res) {
        Resume.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
       });
      })

      route.post('/del',function(req,res) {
        Resume.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
          res.send("OK")
        });
      });



      route.post('/deletesuser',function(req,res){
        Suser.findOne({username:req.body.username},function(err,found){
          if(err)
          {

          }
          else if(found)
          {
            Suser.deleteOne({username:found.username},function(err,done)
            {
            })
            Resume.deleteMany({username:found.username},function(err,done)
            {
            })
            res.send("Deleted")
          }
        })
      })
      route.post('/update_student_password',function(req,res){
        Suser.findOne({username:req.body.student_username},function(err,found){
          if(err)
          {

          }
          else if(found)
          {
            let hash = bcrypt.hashSync(req.body.student_password, 10);
            Suser.updateOne({username:req.body.student_username},{password: hash} ,(err, done) => {
              if(err)
              {

              }
              else if(done)
              {
                res.send("Updated");
              }
            })
          }
          else
          {
            res.send("User Not Found !!")
          }
        })
      })


      route.post('/suspension_suser',function(req,res){
        console.log(req.body)
        Suser.findOne({username:req.body.suspend_student_username},function(err,found){
          if(err)
          {
            res.status(404).send("error!!")
          }
          else if(found)
          {
            AdminOrder.findOne({$and:[{victim_username:req.body.suspend_student_username},{order:'Suspend Suser'}]},function(err,obj){
              if(err)
              {

              }
              else if(obj)
              {
                res.send("Already Suspended !!")
              }
              else{
                var suspend = new AdminOrder({order:'Suspend Suser',victim_username:req.body.suspend_student_username,reason:req.body.suspend_reason_student,suspend_status:true});
                suspend.save((err, savedUser) => {
                  if(err)
                  {

                  }
                  else if(savedUser)
                  {
                    Suser.updateOne({username:req.body.suspend_student_username},{suspension_status: true},function(err,done){
                      if(err)
                      {

                      }
                      else if(done)
                      {
                        res.send("Suspended !!")
                      }
                    })
                  }
                })
              }
            })
          }
          else{
            res.send("User Not Found !!")
          }
        })
      })
      route.post('/remove_suser_suspension',function(req,res){
        console.log(req.body)
        Suser.findOne({username:req.body.suspend_student_username},function(err,found){
          if(err)
          {
            res.status(404).send("error!!")
          }
          else if(found)
          {
            AdminOrder.findOne({$and:[{victim_username:req.body.suspend_student_username},{order:'Suspend Suser'}]},function(err,obj){
              if(err)
              {

              }
              else if(obj)
              {
                Suser.updateOne({username:req.body.suspend_student_username},{suspension_status: false},function(err,done){
                  if(err)
                  {

                  }
                  else if(done)
                  {
                    obj.remove()
                    res.send("Removed Suspension !!")
                  }
                })
              }
              else{
                res.send("User Not Found !!")
              }
            })
          }
          else{
            res.send("User Not Found !!")
          }
        })
      })









      route.get('/know_no_of_suser', function(req, res) {
        Suser.find({ }, function(err, result){
          if(err)
          {

          }
          else if(result)
          {
            res.send(result)
          }
        })
      })

      route.post('/fetch_in_admin',function(req,res) {
        Admin_Operation.find({$and:[{action: req.body.action},{usertype:req.body.usertype}]}, function(err, docs){
        if(err)
        {

        }
        else
        {
        res.send(docs)
        }
        });
      })

      route.post('/del_inserted_data',function(req,res) {
      Admin_Operation.remove({serial:  req.body.serial },function(err,succ){
      res.send("OK")
      });
      })

      route.post('/add_from_insert_in_admin', function(req,res) {
        console.log(req.body.data)
            var respon = new Admin_Operation(req.body.data);
            respon.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
    });

    route.post('/edit_inserted_data', function(req,res) {
    Admin_Operation.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
    })

    });

    route.post('/fetch_for_edit',function(req,res) {
    Admin_Operation.findOne({serial: req.body.id}, function(err, docs){
    if(err)
    {

    }
    else
    {
    res.send(docs)
    }
    });
    })

    route.post('/entry_domain', function(req, res) {
      Arhi.findOne({$and:[{domain_name:req.body.domain},{username:req.user.username},{action:'Fav-Domain'}]},(err,found)=>{
        if(err){

        }
        else if(found){
          res.send('have')
        }
        else{
          var trans = new Arhi({
            action:'Fav-Domain',
            username:req.user.username,
            domain_name:req.body.domain,
          });
          trans.save((err, saved) => {
            if(err){

            }
            else if(saved){
                res.send('done');
            }
          })
        }
      })

    })

    route.get('/fetch_domains', function(req, res) {
      Arhi.find({$and:[{username:req.user.username},{action:'Fav-Domain'}]},(err,found)=>{
        if(err)
        {

        }
        else if(found){
          res.send(found);
        }
      })
    })






module.exports = route
