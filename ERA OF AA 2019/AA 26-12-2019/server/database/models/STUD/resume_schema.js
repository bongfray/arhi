const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const resumeSchema = new Schema({
username: { type: String, unique: false, required: false },
action: { type: String, unique: false, required: false },

name: { type: String, unique: false, required: false },
emailid: { type: String, unique: false, required: false },
mobileno: { type: Number, unique: false, required: false },
address: { type: String, unique: false, required: false },
gender: { type: String, unique: false, required: false },

skill_name:{ type: String, unique: false, required: false },
skill_level:{ type: String, unique: false, required: false },
skill_verification:{ type: Boolean, unique: false, required: false },


status:{ type: String, unique: false, required: false },
start_year:{ type: Number, unique: false, required: false },
end_year:{ type: String, unique: false, required: false },
collage_or_board:{ type: String, unique: false, required: false },
cgpa_percentage:{ type: Number, unique: false, required: false },
stream:{ type: String, unique: false, required: false },
degree:{ type: String, unique: false, required: false },

})

resumeSchema.plugin(autoIncrement.plugin, { model: 'Resume_Data', field: 'serial', startAt: 1,incrementBy: 1 });



var counter = mongoose.model('resume_data', resumeSchema);




module.exports = counter
