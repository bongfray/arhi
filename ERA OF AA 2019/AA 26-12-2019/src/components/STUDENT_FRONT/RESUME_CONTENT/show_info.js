import React from 'react';
import axios from 'axios'

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user2/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        if(this.props.oneEntry)
        {
          this.props.oneEntry({
            disabled:'',
          })
        }
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user2/fetchall',{
    action: this.props.data.Action,
    username: this.props.username,
  })
  .then(response =>{
    if(response.data.length>0)
    {
      if(this.props.oneEntry)
      {
        this.props.oneEntry({
          disabled:'disabled',
        })
      }
    }
    this.setState({
     products: response.data,
   })
  })
}



  render() {
    const { products} = this.state;
      return(
        <React.Fragment>
          <h5 className="center">{this.props.title}</h5><br />
          <div style={{marginLeft:'20px'}}>
          <div className="row">
            <div className="col s1 m1 xl1 l1 center"><b>Serial No</b></div>
            {this.props.data.fielddata.map((content,index)=>(
              <div className={content.grid} key={index}><b>{content.header}</b></div>
            ))}
            <div className={this.props.data.button_grid}><b>Action</b></div>
          </div>
              {products.map((product,index) => (
                <div className="row" key={index}>
                  <div className="col s1 l1 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,ind)=>(
                    <React.Fragment key={ind}>
                      <div className={'center '+content.grid}>{product[content.name]}</div>

                    </React.Fragment>
                  ))}
                    <div className={this.props.data.button_grid}>
                    <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                    &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                </div>
              ))}
            </div>
        </React.Fragment>
      )

  }
}
