
import React from 'react';


class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      Action:'',
      name: '',
      emailid: '',
      mobileno:'',
      address: '',
      gender:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username,
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  handleInput = (e) =>{
    this.setState({[e.target.name]:e.target.value})
  }

  render() {
    // console.log(this.initialState.username)
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS TO YOUR PERSONAL DATA</h5>
}

    return(
      <div>
      <div className="center">{pageTitle}</div>

          {this.props.data.fielddata.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1"/>
          <div className="col l2 m2 s2">{content.header}</div>
            <div className="col l8 s8 m8 input-field" key={index}>
                           <input
                             className=""
                             type={content.type}
                             placeholder=" "
                             id={content.name}
                             name={content.name}
                             value={this.state[content.name]}
                             onChange={e => this.handleD(e, index)}
                           />
                           <label htmlFor={content.name}>{content.placeholder}</label>
            </div>

            <div className="col l1" />
          </div>
          ))}
          <div className="row">
            <div className="col l9 xl9 hide-on-mid-and-down" />
            <div className="col l3 xl3 s12">
              <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
              <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
            </div>
          </div>
      </div>
    )
  }
}

export default AddProduct;
