import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from '../forget'
import axios from 'axios'
import M from 'materialize-css';
import { } from 'materialize-css';
import Nav from '../dynnav'
var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redir: '',
            redirectTo: null,
            forgotroute:'/user2/sforgo',
            home:'/student',
            logout:'/user2/logout',
            get:'/user2/getstudent',
            nav_route:'/user2/fetch_snav',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'Enter all the details !!',outDuration:'1000', classes:'rounded #f44336 red'});
          this.setState({
              cnf_pswd:''
          })
          return false;
        }
        else{
        axios.post('/user2/slogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                console.log(response)
                if (response.status === 200) {
                  this.setState({
                    redirectTo:'/student'
                  })
                }
            }).catch(error => {
                window.M.toast({html: 'Check your Login Credentials',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});

            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }



    getUser = () => {
      axios.get('/user2/getstudent').then(response => {
        if (response.data.user) {
            this.setState({redirectTo:'/student'});
            window.M.toast({html: 'Sorry already loggedIn !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
          })
        }



        componentDidMount() {
            this.getUser();
            M.AutoInit();
        }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
            return (
              <React.Fragment>
              <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
              <div className="row">

             <div className="col s4">
             </div>

             <div className="col l4 s12 m12 form-signup">
                 <div className="ew center">
                     <Link to ="/student"><h5 className="blue-grey darken-2 highlight">eWORK</h5></Link>
                     <h5 className="highlight black-text">LOG IN</h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="text" type="text" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <span className="helper-text" data-error="Please enter the data" data-success="">Registration Number Only</span>
                         <label htmlFor="text">Username</label>
                     </div>
                     <div className="input-field inline col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <span className="helper-text" data-error="Please enter the data" data-success=""></span>
                         <label htmlFor="password">Password</label>
                     </div>
                     </div>
                     <div className="row">
                     <Link to="/ssignup" className=" btn blue-grey darken-2 col s5 sup">Register</Link>
                     <div className="col s2"></div>
                     <button className=" btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <hr/>
                     <div className="row">
                     <Modal forgot={this.state.forgotroute}/>
                     </div>

                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
             </React.Fragment>
           );
         }

    }
}

export default LoginForm
