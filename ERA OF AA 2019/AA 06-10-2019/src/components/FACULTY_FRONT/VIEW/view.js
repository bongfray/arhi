import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
require("datejs")


export default class View extends Component {
  constructor() {
    super()
    this.state ={
      display:'none',
      username:'',
      redirectTo:'',
      isChecked: false,
      choosed: '',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      agreed:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    M.AutoInit()
  }

retriveValue =(e)=>{
  this.setState({[e.target.name]:e.target.value})
}
  Authenticate =(e)=>{
    e.preventDefault()
    axios.post('/user/authenticate',this.state)
    .then(res=>{
    if(res.data === "Allowed")
    {
      this.setState({
        display:'block',
        agreed:[{name:'radio1',value:'Academic'},{name:'radio2',value:'Administrative'},{name:'radio3',value:'Research'},{name:'radio4',value:'Degrees'},{name:'radio5',value:'Section Datas'},],
      })
    }
    else{
      this.setState({display:'bloack'})
    }
        window.M.toast({html: res.data ,outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
    })
  }


  handleChecked =(e,index)=>{
      this.setState({
          isChecked: !this.state.isChecked,
          choosed: e.target.value
      });
  }

  render()
  {
    return(
      <React.Fragment>
        <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
         <div className="row">
           <div className="col l3"/>
           <div className="col l6">
           <div className="form-signup">
           <div className="row">
             <form onSubmit={this.Authenticate} >
                  <div className="input-field">
                     <input type="text" name="username" value={this.state.username} onChange={this.retriveValue} id="id" className="validate" required />
                     <label htmlFor="id">Input Official ID</label>
                  </div>
                  <button className="btn col l2 pink right" >View</button>
              </form>
              </div>
              <br />
              <div className="row">
                 <span className="green-text">DECLAMAIR:  </span>Please Remember in this section you can only see the User's Data only if they are not coming in your same or upper designation level.
                 One more thing keep in mind users in medium level(like HOD , Professor) have only access to view datas within their department only.
              </div>
            </div>
           </div>
           <div className="col l3"/>
         </div>
         <div className="row form-signup" style={{display:this.state.display}}>
         <div className="row">
            <div className="col l1">

            </div>
            <div className="col l12" style={{fontSize:'20px',marginTop:'-10px'}}>
               <i className="small material-icons" style={{marginTop:'10px'}}>account_circle</i>&emsp;You are seeing the profile of <span className="pink-text">{this.state.username}</span>
            </div>
         </div>
           {this.state.agreed.map((content,index)=>{
             return(
               <div className="col s6 l2 m2" key={index}>
                   <div className="col l10 s10 m10 form-signup">
                   <p>
                   <label>
                   <input type='radio' className="with-gap" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index)}}/>
                   <span className="black-text"><b>{content.value}</b></span>
                   </label>
                   </p>
                   </div>
             </div>
             )
           })}
         </div>
         <div className="row">
           <ShowData choosed={this.state.choosed}/>
         </div>
      </React.Fragment>
    );
  }
}

class  ShowData extends Component {
  constructor(props) {
    super(props)
    this.state ={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    return(
      <React.Fragment>
        <div className="form-signup center">
           {this.props.choosed}
        </div>
      </React.Fragment>
    )
  }
}
