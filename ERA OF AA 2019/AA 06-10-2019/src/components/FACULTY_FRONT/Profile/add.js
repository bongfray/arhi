
import React from 'react';
class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      username:'',
      ug_clg_name:'',
      pg_clg_name:'',
      phd_clg_name:'',
      ug_start_year:'',
      pg_start_year:'',
      phd_start_year:'',
      ug_end_year:'',
      pg_end_year:'',
      phd_end_year:'',
      marks_ug:'',
      marks_pg:'',
      marks_phd:'',
      other_degree_name:'',
      other_degree_info:'',
      other_degree_duration:'',
      other_degree_grade:'',
      certificate:'',
      honors_awards:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.action,
    username: this.props.username
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    // console.log(this.initialState.username)
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="proftitle center">EDIT DATAS OF {this.props.action}</h5>
} else {
  pageTitle = <h5 className="proftitle center">ADD DATAS TO {this.props.action}</h5>
}

    return(
      <React.Fragment>
      {pageTitle}
      <div className="row">
          <form onSubmit={this.handleSubmit}>
          {this.props.data.fielddata.map((content,index)=>(
            <div className="col l3 s3 m3 xl3" key={index}>
            <label className="pure-material-textfield-outlined alignfull">
              <textarea
                className=""
                type={content.type}
                placeholder=" "
                min="10"
                max="60"
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              />
              <span>{content.placeholder}</span>
            </label>
            </div>
          ))}
            <div className="row">
              <button className="btn right blue-grey darken-2 sup" style={{marginRight:'10px'}} type="submit">UPLOAD</button>
            </div>
          </form>
      </div>
      </React.Fragment>
    )
  }
}

export default AddProduct;
