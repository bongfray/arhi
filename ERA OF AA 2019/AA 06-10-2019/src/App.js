import React, { Component } from 'react';
import { Route} from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import FiveYear from './components/FACULTY_FRONT/FIVEYEAR/fiveyear'
import Decider from './components/STUDENT_FRONT/Decider'
import  Section from './components/FACULTY_FRONT/Section/sec'
import Initial from './Navigator'
import FSignup from './components/FACULTY_FRONT/sign-up'
import SSignup from './components/STUDENT_FRONT/ssignup'
import FLoginForm from './components/FACULTY_FRONT/login-form'
import SLoginForm from './components/STUDENT_FRONT/slogin'
import Faculty_Home from './components/FACULTY_FRONT/Home'
import Student_Home from './components/STUDENT_FRONT/Home_stud'
import Dash from './components/FACULTY_FRONT/DashBoard/Dashboard'
import Faculty_Profile from './components/FACULTY_FRONT/Profile/exdash'
import Sprofile from './components/STUDENT_FRONT/studentProfile'
import Con from './components/online-offline/Connection'
import Scroll from './components/Top/scroll_top'
import Yesterday_TimeTable from './components/FACULTY_FRONT/TimeTable/yesterday_slot'
import Today_TimeTable from './components/FACULTY_FRONT/TimeTable/time2'
import Manage from './components/FACULTY_FRONT/Manage/manage'
import FProfileData from './components/FACULTY_FRONT/Profile/Degree'
import Master from './components/FACULTY_FRONT/master/mater'
import BluePrint from './components/FACULTY_FRONT/Schedule/Timetable'
import ResetPass from './components/Reset/reset_password'
import AdminAuth from './components/FACULTY_FRONT/admin/auth'
import Panel from './components/FACULTY_FRONT/admin/admin_operations'
import View from './components/FACULTY_FRONT/VIEW/view'

/*------------------Student Part------------------*/
import Resume from './components/STUDENT_FRONT/resume'

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">



        {/*<Con />*/} {/*This is for checking the connectivity */}

        <Route exact path="/" component={Initial} />

        <Route path="/faculty" render={() =>
          <Faculty_Home
         />}
       />

       <Route exact path="/student" render={() =>
         <Student_Home
        />}
      />

        <Route path="/flogin" render={() =>
          <FLoginForm
         />}
       />

       <Route path="/slogin" render={() =>
         <SLoginForm
        />}
      />
       <Route path="/fsignup" component={FSignup} />
       <Route path="/ssignup" component={SSignup} />

       <Route path="/fprofile" component={Faculty_Profile} />
       <Route path="/sprofile" component={Sprofile} />


       <Route path="/partA" render={() =>
         <Section
        />} />


       <Route path="/fprofdata" render={() =>
         <FProfileData
        />} />

       <Route path="/Dash" render={() =>
         <Dash
        />} />
        <Route path="/master" component={Master} />
        <Route path="/view" component={View} />

         <Route path="/blue_print" component={BluePrint} />
         <Route path="/reset_password/:token" component={ResetPass} />


          <Route path="/time_new" render={() =>
            <Today_TimeTable
           />} />
           <Route path="/time_yes" render={() =>
             <Yesterday_TimeTable
            />} />

            <Route path="/arhi" component={Decider} />

            <Route path="/admin" component={AdminAuth} />
            <Route path="/admin_panel" component={Panel} />

            <Route path="/manage" component={Manage} />
              <Route path="/five" component={FiveYear} />



              <Route path="/resume" component={Resume} />
         <Scroll/>
      </div>
    );
  }
}

export default App;
