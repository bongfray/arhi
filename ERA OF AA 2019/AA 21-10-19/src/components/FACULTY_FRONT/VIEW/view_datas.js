import React from 'react'
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import axios from 'axios'


export default class Feed extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      visible: 2,
      error: false
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount() {
    axios.get('/user/fetchnav').then(res => {
      this.setState({
        items: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render() {
    return (
      <div className="card hoverable black-text">
        <div className="tiles" aria-live="polite">
          {this.state.items.slice(0, this.state.visible).map((item, index) => {
              return (
                <div className="card col l4" key={index}>
                  <span className="count">{index+1}</span>
                  <h2>{item.val}</h2>
                </div>
              );
            })}
          </div>
          {this.state.visible < this.state.items.length &&
             <button onClick={this.loadMore} type="button" className="btn load-more">Load more</button>
          }
        </div>
    );
  }
}
