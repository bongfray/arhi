const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

//Schema
const dayorderhis = new Schema({
  cancel_status: { type: Boolean, unique: false, required: false },
  day_order: { type: Number, unique: false, required: false },
  day_order_status: { type: String, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  day: { type: Number, unique: false, required: false },
  month: { type: Number, unique: false, required: false },
  year: { type: Number, unique: false, required: false },
  username: { type: String, unique: false, required: false },
  reason_of_cancel: { type: String, unique: false, required: false },
})


const DayHis = mongoose.model('DayOrder-History',dayorderhis)
module.exports = DayHis
