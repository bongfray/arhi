import React, { Component,Fragment } from 'react'
import axios from 'axios'
import { } from 'react-router-dom'
import ValidateUser from '../validateUser'


export default class ModifyUser extends Component{
  constructor(){
      super();
      this.initialState={
        success:'',
        modal:false,
        disabled:'',
        allowed:false,
          done:'',
          cday:'',
          cmonth:'',
          cyear:'',
          hday:'',
          hmonth:'',
          hyear:'',
          replacable_day_order:'',
          sday: '',
          smonth: '',
          syear: '',
          eday: '',
          emonth: '',
          eyear: ''
      }
      this.state = this.initialState;
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.action !== this.props.action) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.navigate();
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }

  navigate =(e) =>{
    this.setState({modal: !this.state.modal,disabled:'disabled'})
    if(this.state.allowed === true)
    {
      if(this.props.select === "start-semester")
      {
        this.Start_Of_Semester();
      }
      else if(this.props.select === "end-semester")
      {
        this.End_Of_Semester();
      }
      else if(this.props.select === "modifytoday")
      {
        this.Reset_Today_DayOrder()
      }
      else if(this.props.select === "cancel_or_declare_today_as_a_holiday")
      {
        this.handleCancel_Holiday()
      }
      else if(this.props.select === "holiday")
      {
        this.Declare_Advance_Holiday()
      }
    }
  }


  handleChange = (e) =>{
    this.setState({
      [e.target.name]: e.target.value,
    })
  }
  /*------------------------------------Cancellation For Holiday & Also DayOrder Cancelled---------------- */
  handleCancel_Holiday = (e) =>{
    this.setState(this.initialState)
    axios.post('/user/declare_cancel_or_holiday',this.state,
    this.setState({done:''})
  )
    .then(res=>{
      this.setState({done:res.data,disabled:'',
      success:'none'})
    })
    .catch(error =>{
      window.M.toast({html: 'Error!!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    })
  }


Start_Of_Semester =(e) =>{
  
}
End_Of_Semester = (e) =>{
  
}
Reset_Today_DayOrder = (e) =>{
  
}

Declare_Advance_Holiday = (e) =>{

}

  render(){
    let content;
      if(this.props.select === 'start-semester'){
          content =
              <div className="center">

                              <div className="input-field col s4">
              <input id="sday" type="text" name="sday" className="validate" value={this.state.sday} onChange={this.handleChange} required />
              <label htmlFor="sday">Day (dd)</label>
              </div>
                              <div className="input-field col s4">
              <input id="smonth" type="text" name="smonth" className="validate" value={this.state.smonth} onChange={this.handleChange} required />
              <label htmlFor="smonth">Month (mm)</label>
              </div>
                              <div className="input-field col s4">
              <input id="syear" type="text" name="syear" className="validate" value={this.state.syear} onChange={this.handleChange} required />
              <label htmlFor="syear">Year (yyyy)</label>
              </div>
              <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>Start Semester</button>
              </div>
      }
      else if(this.props.select === 'end-semester'){
          content =
              <div className="center">

                              <div className="input-field col s4">
              <input id="eday" type="text" name="eday" className="validate" value={this.state.eday} onChange={this.handleChange} required />
              <label htmlFor="eday">Day (dd)</label>
              </div>
                              <div className="input-field col s4">
              <input id="emonth" type="text" name="emonth" className="validate" value={this.state.emonth} onChange={this.handleChange} required />
              <label htmlFor="emonth">Month (mm)</label>
              </div>
                              <div className="input-field col s4">
              <input id="eyear" type="text" name="eyear" className="validate" value={this.state.eyear} onChange={this.handleChange} required />
              <label htmlFor="eyear">Year (yyyy)</label>
              </div>
              <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>End Semester</button>
              </div>

          
      }
      else if(this.props.select === 'modifytoday'){
          content =
              <div className="center">

                              <div className="input-field col s12">
              <input id="dayorder" type="text" name="replacable_day_order" className="validate" value={this.state.replacable_day_order} onChange={this.handleChange} required />
              <label htmlFor="dayorder">Reset Today's Day Order to:</label>
              </div>
              <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>Reset Day Order</button>
              </div>

          
      }
      else if(this.props.select === 'cancel_or_declare_today_as_a_holiday'){
          content =
            <div className="form-signup">
                <div className="row">
                <div className="input-field col s4">
                <input id="cday" type="Number" name="cday" className="validate" value={this.state.cday} onChange={this.handleChange} required />
                <label htmlFor="cday">Day (dd)</label>
                </div>
                <div className="input-field col s4">
                <input id="cmonth" type="Number" name="cmonth" className="validate" value={this.state.cmonth} onChange={this.handleChange} required />
                <label htmlFor="cmonth">Month (mm)</label>
                </div>
                <div className="input-field col s4">
                <input id="cyear" type="Number" name="cyear" className="validate" value={this.state.cyear} onChange={this.handleChange} required />
                <label htmlFor="cyear">Year (yyyy)</label>
                </div>
                <div className="col l3">{this.state.done}</div>
                <button className="waves-effect right btn #37474f blue-grey darken-3 col s3 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>SUBMIT</button>
                </div>
          </div>
          
      }
      else if(this.props.select === 'holiday'){
          content =
            <div className="form-signup">
                <div className="row">
                <div className="input-field col s4">
                <input id="hday" type="Number" name="hday" className="validate" value={this.state.hday} onChange={this.handleChange} required />
                <label htmlFor="hday">Day (dd)</label>
                </div>
                <div className="input-field col s4">
                <input id="hmonth" type="Number" name="hmonth" className="validate" value={this.state.hmonth} onChange={this.handleChange} required />
                <label htmlFor="hmonth">Month (mm)</label>
                </div>
                <div className="input-field col s4">
                <input id="hyear" type="Number" name="hyear" className="validate" value={this.state.hyear} onChange={this.handleChange} required />
                <label htmlFor="hyear">Year (yyyy)</label>
                </div>
                <div className="col l3">{this.state.done}</div>
                <button className="waves-effect right btn #37474f blue-grey darken-3 col s3 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>SUBMIT</button>
                </div>
          </div>
      }
      else{
          content=
              <div></div>
      }
      return(
        <React.Fragment>
        <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
        {content}
        </React.Fragment>
        
      )
  }
}
