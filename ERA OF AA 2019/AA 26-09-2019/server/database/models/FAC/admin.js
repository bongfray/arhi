const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


// var connection = mongoose.createConnection("mongodb://localhost:27017/eWork");
//
// autoIncrement.initialize(connection);


const adminSchema = new Schema({
username: { type: String, unique: false, required: false },
action: { type: String, unique: false, required: false },
title_thesis_project: { type: String, unique: false, required: false },
description_of_thesis_project_completed: { type: String, unique: false, required: false },
credits_for_thesis_project: { type: String, unique: false, required: false },
date_of_thesis_project_completion: { type: String, unique: false, required: false },

date_of_training_attended:{ type: String, unique: false, required: false },
title_workshop_seminar_attended:{ type: String, unique: false, required: false },
details_workshop_seminar_attended:{ type: String, unique: false, required: false },
location_of_workshop_seminar_attended:{ type: String, unique: false, required: false },
duration_of_workshop_seminar_attended:{ type: String, unique: false, required: false },

date_of_training_held:{ type: String, unique: false, required: false },
details_workshop_seminar_held:{ type: String, unique: false, required: false },
location_of_workshop_seminar_held:{ type: String, unique: false, required: false },
duration_of_workshop_seminar_held:{ type: String, unique: false, required: false },
role_workshop_seminar_held:{ type: String, unique: false, required: false },

date_of_paper_publication:{ type: String, unique: false, required: false },
paper_title:{ type: String, unique: false, required: false },
volume_no_of_publiction:{ type: String, unique: false, required: false },
impact_of_publication:{ type: String, unique: false, required: false },
paper_accepted_by:{ type: String, unique: false, required: false },

research_project_area:{ type: String, unique: false, required: false },
project_research_title:{ type: String, unique: false, required: false },
description_project_research:{ type: String, unique: false, required: false },
duration_of_project_research_tobe_done:{ type: String, unique: false, required: false },
achivements_from_project_research_work:{ type: String, unique: false, required: false },

contribution_field:{ type: String, unique: false, required: false },
details_of_contribution:{ type: String, unique: false, required: false },
result_of_contribution:{ type: String, unique: false, required: false },
achivements_for_your_contribution:{ type: String, unique: false, required: false },

field_of_activity:{ type: String, unique: false, required: false },
details_of_dept_activity:{ type: String, unique: false, required: false },
result_from_activity:{ type: String, unique: false, required: false },
achivements_from_the_activity:{ type: String, unique: false, required: false },

date_of_meeting:{ type: String, unique: false, required: false },
commity_details:{ type: String, unique: false, required: false },
position_role:{ type: String, unique: false, required: false },
number_of_meeting_attended:{ type: String, unique: false, required: false },

date_of_counselling:{ type: String, unique: false, required: false },
details_of_student:{ type: String, unique: false, required: false },
no_of_advising_hour:{ type: String, unique: false, required: false },
description_of_activity:{ type: String, unique: false, required: false },
})

adminSchema.plugin(autoIncrement.plugin, { model: 'Admin', field: 'serial', startAt: 1,incrementBy: 1 });



var counter = mongoose.model('Admin', adminSchema);




module.exports = counter
