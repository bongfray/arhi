import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'


export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            username: '',
            deniedlist:[],
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount()
    {
      this.fetch_denied_list()
    }

    fetch_denied_list =() =>{
      axios.get('/user/fetch_denied_list')
      .then(res=>{
        this.setState({deniedlist: res.data})
      })
    }
    handleApprove = (id,username) =>{
      axios.post('/user/approverequest',{
        serial: id,
        username:username,
      }).then(res=>{
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== id),
        })
      })
    }


    handleUsername = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handleSuspend(event){

    }
    handleRemove(event){

    }
    render(){
        if(this.props.select === 'suspendadmin'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Admin</Link>
                </div>

            )
        }
        else if(this.props.select === 'suspendfac'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Faculty</Link>
                </div>

            )
        }
        else if(this.props.select === 'suspendstu'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Student</Link>
                </div>

            )
        }
        else if(this.props.select === 'suspend_request'){
            return(
                <div>
                {this.state.deniedlist.map((content,index)=>(
                  <React.Fragment key={index}>
                  <div className="row">
                  <div>
                  <div className=" col l2 center">{content.username}</div>
                  <div className=" col l1 center">{content.day}</div>
                  <div className=" col l1 center">{content.month}</div>
                  <div className=" col l2 center">{content.year}</div>
                  <div className=" col l1 center">{content.day_order}</div>
                  <div className=" col l3 center">{content.req_reason}</div>
                  </div>
                  <button className="btn col l2 " onClick={() => this.handleApprove(content.serial,content.username)}>Approve</button>
                  </div>
                  </React.Fragment>
                ))}
                </div>
            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}