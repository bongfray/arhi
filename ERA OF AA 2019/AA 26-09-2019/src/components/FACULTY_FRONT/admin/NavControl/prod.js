import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch(this.props.action)
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch(this.props.action);
    }
  }


  deleteProduct = (productId,index) => {
    var delroute;
    if(index === "faculty")
    {
      delroute = "/user/delnav";
    }
    else if(index === "student")
   {
    delroute = "/user2/delnav";
   }
  const { products } = this.state;
  axios.post(delroute,{
    serial: productId,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =(action) =>{
  let fetchroute;
  if(action === "faculty")
  {
    fetchroute = "/user/fetchnav";
  }
  else if(action === "student")
  {
    fetchroute = "/user2/fetch_student_nav"
  }

  axios.post(fetchroute,{
    action: this.props.action,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { error, products} = this.state;
      return(
        <div>
            <div className="row">
                <div className="col s3 l3 m3 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className="col s3 l3 m3 center" key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s3 l3 m3 center"><b>Action</b></div>
            </div>
            <div>
              {products.map((product,index) => (
                <div className="row" key={product.serial}>
                  <div className="col l3 m3 s3 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className="center col s3 m3 l3" key={index}>{product[content.name]}</div>
                  ))}
                    <div className="center col s3 m3 l3 hide-on-small-only"><button className="btn-small green" onClick={() => this.props.editProduct(product.serial,this.props.action)}>Edit</button>
                    &nbsp;<button className="btn-small red" onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </div>
                  <div className="col s3 hide-on-med-and-up">
                  <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                  &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                </div>
              ))}
            </div>
        </div>
      )

  }
}
