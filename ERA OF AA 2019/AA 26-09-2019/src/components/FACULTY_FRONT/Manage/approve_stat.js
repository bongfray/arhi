import React, { Component, Fragment  } from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import {Collapsible, CollapsibleItem, Modal} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from '../TimeTable/colordiv.js'
import Time from '../TimeTable/time2'
import Add from '../TimeTable/addrow'
import Nav from '../dynnav'
import '../style.css'
require("datejs")

/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{

  constructor() {
    super();
    this.componentWillMount = this.componentWillMount.bind(this);
    this.state = {
      approved:[],
      viewdata:[],
      available:'no',
    }
  }


  componentWillMount(){
    M.AutoInit();
    this.fetchApprove()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.request_props !== this.props.request_props) {
      this.fetchApprove();
    }
  }

  fetchApprove = () =>{
    axios.get('/user/fetch_requested_list').then(res=>{
      this.setState({
        approved: res.data,
      })
    })
  }


  handleViewRequest = (content,index) =>{
    this.componentDidUpdate(index)
    this.setState({
      available:'yes',
      viewdata: content,
    })
  }


      render(){
        let status_of_req;

              return(
          <React.Fragment>
          <h6 className="center pink white-text" style={{height:'45px',paddingTop:'12px'}}><b>STATUS OF YOUR REQUEST</b></h6><br /><br />
          <div className="row">
            <div className="col l1 center">Serial No.</div>
            <div className="col l2 center">Day Order</div>
            <div className="col l2 center">Official ID</div>
            <div className="col l2 center">Date(dd/mm/yyyy)</div>
            <div className="col l3 center">Reason</div>
            <div className="col l2 center">Action</div>
          </div>
          <div className="col l12">
          {this.state.approved.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 center">{index+1}</div>
            <div className=" col l2 center">{content.day_order}</div>
            <div className=" col l2 center">{content.username}</div>
            <div className=" col l2 center">{content.day}/{content.month}/{content.year}</div>
            <div className=" col l3 center">{content.req_reason}</div>
            </div>
            <StatusReq content={content} serial={content.serial} />
            </div>
            </React.Fragment>
          ))}
          </div>
          </React.Fragment>

      )
      }
    }

class StatusReq extends Component{
  constructor(props)
  {
    super(props)
    this.state ={
      status:'',
      viewdata:[],
      available:'no',
    }
    this.componentWillMount = this.componentWillMount.bind(this)
  }
  componentWillMount()
  {
    this.fetchStatus_Of_Request()
  }


  handleViewRequest = (content,index) =>{
    this.setState({
      available:'yes',
      viewdata: content,
    })
  }

  fetchStatus_Of_Request= () =>{
    axios.post('/user/fetch_requested_status',{
      serial: this.props.serial,
    }).then(res=>{
      this.setState({status: res.data})
    })
  }
  render()
  {
    let btn;
    if(this.state.status === "approved")
    {
      btn =
      <button className="btn col l2 right blue-grey darken-2 sup"  onClick={() => this.handleViewRequest(this.props.content,this.props.serial)}>Upload Data</button>
    }
    else if(this.state.status === "denied")
    {
      btn = <div className="col l2 center" style={{color:'red',borderLeft:'1px solid red',borderRight:'1px solid red',borderTop:'1px solid red',borderBottom:'1px solid red'}}>Denied</div>
    }
    else if(this.state.status === "pending")
    {
      btn = <div className="col l2 center" style={{color:'red',borderLeft:'1px solid red',borderRight:'1px solid red',borderTop:'1px solid red',borderBottom:'1px solid red'}}>Pending</div>
    }

    return(
      <React.Fragment>
      <div>{btn}</div>
      <div className="col l12 form-signup">
       <Content available={this.state.available} view={this.state.viewdata}/>
     </div>
      </React.Fragment>
    )
  }
}


{/*-----------------------------------------------This class is for dynamically allocating div ------------- */}

class Content extends Component{
  constructor(props){
    super(props);
    this.state={
      username: props.view.username,
      day_order: props.view.day_order,
      day:props.view.day,
      month:props.view.month,
      year:props.view.year,
      redirectTo:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
  }
  render(){
    console.log(this.props.view)
    let output;
    if(this.props.available === "yes") {
      output =
      <React.Fragment>
       <ColorRep usern={this.props.view.username} day={this.props.view.day} month={this.props.view.month} year={this.props.view.year} day_order={this.props.view.day_order}/>
      </React.Fragment>
    }
    else
    {
      <div></div>
    }
          return(
            <div>
            {output}
            </div>
          );
  }
}
