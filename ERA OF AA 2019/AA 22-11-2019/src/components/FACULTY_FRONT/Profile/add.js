
import React from 'react';
class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      username:'',
      Collage_Name:'',
      End_Year:'',
      Start_Year:'',
      Marks_Grade:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.action,
    username: this.props.username
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="proftitle center">EDIT DATAS OF {this.props.action}</h5>
} else {
  pageTitle = <h5 className="proftitle center">ADD DATAS TO {this.props.action}</h5>
}

    return(
      <React.Fragment>
      {pageTitle}
      <div className="row">
          {this.props.data.fielddata.map((content,index)=>(
            <div className="input-field col l3 s12 m6 xl3" key={index}>
              <input
                type={content.type}
                id={content.name}
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              />
            <label htmlFor={content.name}>{content.placeholder}</label>
            </div>
          ))}
      </div>
      <div className="row">
        <div className="col l9 xl9 hide-on-mid-and-down" />
        <div className="col l3 xl3 s12">
          <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
          <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
        </div>
      </div>
      </React.Fragment>
    )
  }
}

export default AddProduct;
