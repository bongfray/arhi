import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import {Collapsible, CollapsibleItem,Select} from 'react-materialize'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import AddProduct from './add';
import ProductList from './prod'
var empty = require('is-empty');

export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      SectionProps:[],
      loading:true,
      username:'',
      redirectTo:'',
      option:'',
      home:'/faculty',
      logout:'/user/logout',
      login:'/flogin',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}
getUser()
{
  axios.get('/user/')
   .then(response =>{
     this.setState({loading: false})
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
       this.getSectionData()
     }
     else{
       this.setState({
         redirectTo:'/faculty',
       });
       window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
     }
   })
}
getSectionData()
{
  axios.post('/user/fetch_section',{action:'Section'}).then(response =>{
    console.log(response.data)
    this.setState({
     SectionProps: response.data,
   })
  })
}

componentDidMount(){
  this.getUser()
}


   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/user/editt';
     } else {
       apiUrl = '/user/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Description of Your Deeds",
          name: "description",
          placeholder: "Description of your work",
          type: "text",
          grid: "col l3 xl3 s2 m1 center"
        },
        {
          header: "Your Role",
          name: "role",
          placeholder: "Enter Your Role",
          type: "text",
          grid: "col l2 s2 m1 xl2 center"
        },

        {
          header: "Your Achivements",
          name: "achivements",
          type: "text",
          grid: "col l3 s2 m1 xl3 center",
          placeholder: "Enter Details about your Achivements"
        },
        {
          header: "Date of Starting",
          name: "date_of_starting",
          type: "number",
          grid: "col m1 s2 l1 xl1 center",
          placeholder: "Enter the Date of Starting"
        },
        {
          header: "Date of Completing",
          name: "date_of_complete",
          type: "number",
          grid: "col s2 m1 l1 xl1 center",
          placeholder: "Enter the Date of Completion"
        },

      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct username={this.state.username} action={this.state.option} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }
    let section_datas;
    if(this.state.SectionProps)
    {
      section_datas =
        <React.Fragment >
            <Select l="12" xl="12" s="12" m="6" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled defaultValue>Select Here</option>
            {this.state.SectionProps.map((content,index)=>{
              return(
                <option key={index} value={content.section_data_name}>{content.section_data_name}</option>
            )
            })}
          </Select>
      </React.Fragment>
    }
    else
    {
      section_datas =
        <React.Fragment >
            <Select l="12" xl="12" s="12" m="6" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled defaultValue>Fetching..</option>
            </Select>
      </React.Fragment>
    }

    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
       <rect x="130" y="20" rx="3" ry="3" width="120" height="20" />
        <rect x="35" y="60" rx="3" ry="3" width="330" height="30" />
      </ContentLoader>
    )
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading ===  true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    return(
      <React.Fragment>
      <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <h5 className="center prof-heading black-text">This  Part  is  For  Enlisting  Your  Achivements</h5>
      <div className="row">
      <div className="col l1 xl1 hide-on-mid-and-down" />
      <div className="col l10 s12 m12 xl10">
      <div className="row form-signup">
      <div className="col l6 xl6 m12 s12">
        <p style={{fontSize:'22px'}}>Kindly Select On Which Area You Are Going to Upload You Datas</p>
      </div>
        <div className="col l6 s12 xl6 m12">
        {section_datas}
        </div>
        </div>
        </div>
        <div className="col l1 xl1 hide-on-mid-and-down" />
        </div>
        <div className="App">
        <Collapsible popout>
          <CollapsibleItem
            header={
                  <h5 className="collaphead">{this.state.option}</h5>
            }expanded>
        <div>
          {!this.state.isAddProduct && <ProductList username={this.props.username} title={this.state.option}  action={this.state.option} data={data}  editProduct={this.editProduct}/>}<br />
          {!this.state.isAddProduct &&
           <React.Fragment>
           <div className="row">
           <div className="col l6 s12 xl6 m12 left" />
           <div className="col l6 s6 xl6 m6">
             <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
             </div>
          </div>
          </React.Fragment>
        }
          { productForm }
          <br/>
        </div>
        </CollapsibleItem>
      </Collapsible>
        </div>
      </React.Fragment>

    )
}
}
  }
}
