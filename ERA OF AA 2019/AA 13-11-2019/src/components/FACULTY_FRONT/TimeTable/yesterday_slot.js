import React,{ Component} from 'react'
import { } from 'react-router-dom'
import {Link , Redirect} from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from './colordiv.js'
import Nav from '../../dynnav'
require("datejs")


class DayOrder extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class YesSimple extends Component{
  constructor(props) {
    super(props);
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.state = {
      opendir: '',
      display:'block',
      saved_dayorder:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
  }

  getDayOrder = () =>{
    var yesterday = Date.parse("yesterday").toString("dd");
    var yestermonth = Date.parse("yesterday").toString("M");
    var yesteryear = Date.parse("yesterday").toString("yyyy");
    axios.post('/user/fetch_yesterday_dayorder', {day:yesterday,month:yestermonth,year:yesteryear
    })
    .then(response =>{
       if((response.data.initial ===0)  || (response.data.initial ===1)  || (response.data.stock===0))
        {
          this.setState({saved_dayorder:0,display:'none'})
        }
      else
      {
        if(response.data.initial)
        {
          this.setState({saved_dayorder:response.data.initial})
        }
        else{
          this.setState({saved_dayorder:response.data.stock})
        }
      }
    });
  }



  componentDidMount(){
    M.AutoInit();
    this.getDayOrder();
  }

  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };
  render(){
    var yesterday = Date.parse("yesterday").toString("dd");
    var yestermonth = Date.parse("yesterday").toString("M");
    var yesteryear = Date.parse("yesterday").toString("yyyy");
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div className="">
      <div className="row">
      <div className="col l2">
      </div>
      <div className="col l10">
        <Link to="time_new" className="right" style={{color:'black'}}>Go to Current DayOrder</Link><br />
        <p className="center tcr" style={{fontSize:'20px'}}>Yesterday's DayOrder</p>
      </div>
      </div>
      <div className="row">
      <div className="col l2">
          <DayOrder day_order={this.state.saved_dayorder}/>
        <br />
        <div className="status_of_day" style={{display:this.state.display}}>
              <select value={this.state.opendir}  onChange={this.handledir}>
                <option value="" disabled defaultValue>Select Here</option>
                <option value="r_class">Regular Class</option>
              </select>
          </div>
    </div>
    <div className="col l10">
      <Content opendir={this.state.opendir} day={yesterday} month={yestermonth} year={yesteryear} day_order={this.state.saved_dayorder} usern={this.props.username}/>
    </div>
    </div>
      </div>
      </React.Fragment>
    );
  }
  }
}


class Content extends Component{
  constructor(){
    super();
    this.state={
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }

  render(){
    if(this.props.opendir==="r_class")
    {
          return(
            <React.Fragment>
             <ColorRep usern={this.props.usern} day={this.props.day} month={this.props.month} year={this.props.year} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else{
      return(
        <React.Fragment>
        {this.props.day_order ===0 ? <div className="def-reg center">No DayOrder Today</div> : <div className="def-reg center">Please Select from the DropDown</div>

       }
        </React.Fragment>
      );
    }

  }
}
