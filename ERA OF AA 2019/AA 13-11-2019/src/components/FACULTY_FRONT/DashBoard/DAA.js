import React, { Component } from 'react'
import axios from 'axios'
import M from 'materialize-css';
import {} from 'materialize-css'

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
			logout:'/user/logout',
			get:'/user/',
      count:0,
      nav_route: '/user/fetchnav',
      percent_ad:[],
      percent_ac:[],
      percent_re:[],
      total_percent_ad:'',
      total_percent_ac:'',
      total_percent_re:'',
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

fetchPercentage = () =>{
  axios.get('/user/knowPercentage', {
  })
  .then(response =>{
    const administrative = response.data.filter(item => item.action=== "Administrative");
    const academic = response.data.filter(item => item.action=== "Academic");
    const research = response.data.filter(item => item.action=== "Research");
    this.setState({percent_ad:administrative,percent_ac:academic,percent_re:research})
  })
}
fetchRootPercentageAd = () =>{
  axios.get('/user/knowRootPercentageAd', {
  })
  .then(response =>{
    this.setState({total_percent_ad:response.data.responsibilty_root_percentage})
  })
}
fetchRootPercentageAcademic = () =>{
  axios.get('/user/knowRootPercentageAc', {
  })
  .then(response =>{
    this.setState({total_percent_ac:response.data.responsibilty_root_percentage})
  })
}
fetchRootPercentageResearch = () =>{
  axios.get('/user/knowRootPercentageRe', {
  })
  .then(response =>{
    this.setState({total_percent_re:response.data.responsibilty_root_percentage})
  })
}

    componentDidMount() {
        M.AutoInit();
        this.fetchRootPercentageAd();
        this.fetchRootPercentageAcademic();
        this.fetchRootPercentageResearch();
        this.fetchPercentage();
    }
  render()
  {
    return(
      <React.Fragment>
      <div className="row">
        <div className="col l4 xl4 s12 m12">
        <ul className="collapsible">
        <li>
          <div className="center collapsible-header pink" style={{color:'white'}}>Administrative Works</div>
          <div className="collapsible-body">
          <div className="row">
            <div className="col l11 left">Total Percentage Alloted for Administrative Work: </div>
            <div className="col l1">{this.state.total_percent_ad}</div>
          </div>
          <table>
            <thead>
              <tr>
                  <th>Different Works</th>
                  <th className="center">Alloted (H/W)</th>
                  <th className="center">Completed (H/W)</th>
              </tr>
            </thead>
            <tbody>
            {this.state.percent_ad.map((content,index)=>{
              return(
              <tr key={index}>
                <td className="">{content.responsibilty_title}</td>
                <td className="center">{content.responsibilty_percentage}</td>
                <td className="center"><Total role={content.responsibilty_title}/></td>
              </tr>
            )
            })}
            </tbody>
          </table>
          </div>
          </li>
        </ul>
        </div>

        <div className="col l4 xl4 s12 m12">
        <ul className="collapsible">
        <li>
           <div className="center collapsible-header pink" style={{color:'white'}}>Academic Work</div>
           <div className="collapsible-body">
           <div className="row">
             <div className="col l11 left">Total Percentage Alloted for Academic Work: </div>
             <div className="col l1">{this.state.total_percent_ac}</div>
           </div>

           <table>
             <thead>
               <tr>
                   <th>Different Works</th>
                   <th className="center">Alloted (H/W)</th>
                   <th className="center">Completed (H/W)</th>
               </tr>
             </thead>
             <tbody>
             {this.state.percent_ac.map((content,index)=>{
               return(
               <tr key={index}>
                 <td className="">{content.responsibilty_title}</td>
                 <td className="center">{content.responsibilty_percentage}</td>
                 <td className="center"><Total role={content.responsibilty_title}/></td>
               </tr>
             )
             })}
             </tbody>
           </table>
           </div>
           </li>
           </ul>
        </div>
        <div className="col l4 xl4 s12 m12">
        <ul className="collapsible">
        <li>
           <div className="center collapsible-header pink" style={{color:'white'}}>Research Work</div>
           <div className="collapsible-body">
           <div className="row">
             <div className="col l12 left">Total Percentage Alloted for Research Work: <span>{this.state.total_percent_re}</span></div>
           </div>
           <table>
           <thead>
             <tr>
                 <th>Different Works</th>
                 <th className="center">Alloted (H/W)</th>
                 <th className="center">Completed (H/W)</th>
             </tr>
           </thead>
           <tbody>
           {this.state.percent_re.map((content,index)=>{
             return(
             <tr key={index}>
               <td className="">{content.responsibilty_title}</td>
               <td className="center">{content.responsibilty_percentage}</td>
               <td className="center"><Total role={content.responsibilty_title}/></td>
             </tr>
           )
           })}
           </tbody>
           </table>
           </div>
           </li>
           </ul>
        </div>
      </div>

      </React.Fragment>
    )
  }
}



class Total extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      count:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTotal();
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.role !== this.props.role){
      this.fetchTotal();
    }
  }
  fetchTotal=()=>
  {
    axios.post('/user/know_Complete_Percentage',{res:this.props.role})
    .then(response =>{
      let len= ((response.data).length)
      this.setState({count:len})
    })
  }
  render()
  {
    return(
      <React.Fragment>
         {this.state.count}
      </React.Fragment>
    );
  }
}
