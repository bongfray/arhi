import React, { Component } from 'react'
import axios from 'axios'
import {Collapsible, CollapsibleItem, Button } from 'react-materialize'
import { Redirect } from 'react-router-dom'

export default class Notification extends Component {
    constructor()
    {
        super()
        this.state={
            display:'',
            loader:true,
            order_action:'faculty',
            notification:[],
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchNoti()
    }

  fetchNoti = ()=>{
    axios.get('/user/fetch_notification')
    .then( res => {
      this.setState({loader:false})
        if(res.data.length === 0)
        {
          this.setState({notfound:'Canot able to find any notification for you !!'})
        }
        else{
          this.setState({notification:res.data})
        }
    });
  }
    clearOne =(id)=>{
      const { notification } = this.state;
      axios.post('/user/clear_one_notification',{id:id})
          .then(response => {
            this.setState({
              response: response,
              notification: notification.filter(noti => noti.serial !== id)
           })
          })
    }
    clearAll = ()=>{
      axios.post('/user/clear_all_notification')
      .then( res => {
        this.setState({
          notification: [],
       })
      });
    }
    setDisplay =() =>{
      this.props.setDisp({
        notidisp: 'none',
      });
    }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

        return (
            <React.Fragment>
            <div className="notinoti">
            <div className="row blue-grey darken-2 white-text">
            <div className="col l1">
            <div className="left go" style={{marginTop:'10px',marginLeft:'10px'}} onClick={this.setDisplay}><i className="small material-icons white-text">close</i></div>
            </div>
            <div className="col l10 center"><h5>Notifications</h5></div>
            <div className="col l1" />
            </div>
            {this.state.loader === true ?
                <React.Fragment>
                  <div className="center">
                      <div class="preloader-wrapper big active">
                      <div className="spinner-layer spinner-red">
                          <div className="circle-clipper left">
                           <div className="circle"></div>
                          </div><div className="gap-patch">
                           <div className="circle"></div>
                          </div><div className="circle-clipper right">
                           <div className="circle"></div>
                          </div>
                      </div>
                      </div>
                  </div>
                </React.Fragment>
                :
            <div className="row">
            <div className="col l1" />
            <div className="col l10">
            {this.state.notfound && <h5 className="center">{this.state.notfound}</h5>}
            {this.state.notification.map((content,index)=>{
              return(
                <React.Fragment key={index}>
                <div className="row">
                <div className="col l11">
                <Collapsible popout>
                  <CollapsibleItem
                    header={
                          <h6 className="collaphead">{content.subject}</h6>
                    }>
                    <div className="">
                         <p>{content.details}</p>
                    </div>
                </CollapsibleItem>
              </Collapsible>
              </div>
              <div className="col l1">
                <Button onClick={() => this.clearOne(content.serial)} style={{marginTop:'10px',background:'linear-gradient(to bottom, #00ffff 0%, #cc33ff 100%'}} className="left btn-floating btn-large waves-effect waves-light" waves="light" tooltip="Dismiss"><i className="small material-icons">clear</i></Button>
                </div>
                </div>
                </React.Fragment>
            );
            })}
            </div>
            <div className="col l1" />
            </div>
          }

            <div className="row">
            {this.state.notification.length!==0 &&
<div href="#" onClick={this.clearAll} className="waves-effect tooltipped" style={{position:'fixed',bottom: 0,left: 0}} data-position="bottom" data-tooltip="Clear All"><i className="red-text large material-icons">clear_all</i></div>
}
    <div className="div"></div>
</div>
</div>
            </React.Fragment>
        );
      }
      }
}
