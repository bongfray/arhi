import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from './colordiv.js'
import Insturction from './instructionmodal'
import Add from './addrow'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
require("datejs")

{/*-------------------------------------------------Clock for Current Date and time------------------- */}

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <span>{new Date().toDateString()}</span>
      <p>{this.state.time}</p>
      </div>
    );
  }
}



{/*-------------------------------------------------------Automatically Chanege the day order------------------------------------------------*/}


class Day_Order extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class Simple extends Component{

  constructor() {
    super();
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      loading:false,
      opendir: '',
      modal: false,
      display:'block',
      saved_dayorder:'',
      redirectTo:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
  }

  getCount =() =>{
    axios.get('/user/knowcount', {
    })
    .then(response =>{
      if(response.data.count ==="1")
      {
          this.selectModal();
      }
    })
  }

  getDayOrder = () =>{
    axios.get('/user/fetchdayorder',
    this.setState({loading:true})
    )
    .then(response =>{
      this.setState({loading:false})
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
      }
      else
      {
        if(response.data.day_order===0)
        {
          this.setState({saved_dayorder:'0',display:'none'})
        }
        else
        {
          this.setState({
            saved_dayorder: response.data.day_order,
          });
        }
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
    M.AutoInit();
    this.getDayOrder();
    this.getCount();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };

      render(){
        const MyLoader = () => (
          <ContentLoader 
            height={160}
            width={400}
            speed={2}
            primaryColor="#f3f3f3"
            secondaryColor="#c0c0c0"
          >
            <rect x="10" y="8" rx="5" ry="5" width="80" height="30" />  
            <rect x="140" y="30" rx="3" ry="3" width="250" height="120" />  
            
            <rect x="10" y="70" rx="5" ry="5" width="80" height="30" />  
            <rect x="10" y="120" rx="1" ry="1" width="80" height="15" />  
      
      
          </ContentLoader>
        )
        if(this.state.loading === true)
        {
          return(
            <MyLoader />
          );
        }
        else{
        if (this.state.redirectTo) {
             return <Redirect to={{ pathname: this.state.redirectTo }} />
         } else {

        return(
          <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div style={{padding:'20px'}}>
            <Insturction
                displayModal={this.state.modal}
                closeModal={this.selectModal}
            />
          <div className="row">
          <div className="col l2">
            <Clock />
          </div>
          <div className="col l10"><div className="row"><p className="col l4"></p>
            <p className="left tcr col l8" style={{fontSize:'20px'}}>Today's DayOrder</p>
            </div>
          </div>
          </div>
          <div className="row">
          <div className="col l2">
          <Link to="/time_yes" style={{color: 'black'}} >Yesterday's Dayorder</Link>
              <Day_Order day_order={this.state.saved_dayorder}/>
            <br />
          <div className="status_of_day" style={{display:this.state.display}}>
                <select value={this.state.opendir}  onChange={this.handledir}>
                  <option value="" disabled defaultValue>Select Here</option>
                  <option value="r_class">Regular Class</option>
                  <option value="own_ab">Absent</option>
                  <option value="college_cancel">College Cancelled</option>
                </select>
            </div>
        </div>
        <div className="col l10">
          <Content opendir={this.state.opendir} day_order={this.state.saved_dayorder} usern={this.props.username}/>
        </div>
        </div>
          </div>
          </React.Fragment>
        );
      }
    }
      }
    }




{/*-----------------------------------------------This class is for dynamically allocating div ------------- */}

class Content extends Component{
  constructor(){
    super();
    this.state={
      saved_dayorder: '',
      college_cancel_reason:'',
      day:'',
      month:'',
      year:'',
      redirectTo:'',
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
  }

handleCollegeCancel = (e) =>{
  e.preventDefault();
  var day = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");

  axios.post('/user/college_cancel', {
    college_cancel_reason:this.state.college_cancel_reason,
    day:day,
    month:month,
    year:year,
    day_order: this.props.day_order,
  })
  .then(response =>{
    if(response.status === 200)
    {
      if(response.data ==="have")
      {
        window.M.toast({html: 'Thanks for Sharing !!',outDuration:'4000', classes:'rounded #ec407a pink lighten-1'});
        this.setState({
          college_cancel_reason:'',
          redirectTo:'/',
        })
      }
      else if(response.data === "ok")
      {
        window.M.toast({html: 'Submitted !!',outDuration:'4000', classes:'rounded #ec407a pink lighten-1'});
        this.setState({
          college_cancel_reason:'',
          redirectTo:'/',
        })
      }
    }
  })

}
Res = (e) =>{
  this.setState({
    college_cancel_reason: e.target.value,
  })
}

  render(){
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.props.opendir==="r_class")
    {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
          return(
            <React.Fragment>
             <ColorRep usern={this.props.usern} day={day} month={month} year={year} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
         <Switch />
        </div>
      );
    }
    else if(this.props.opendir === "college_cancel")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
            value={this.state.college_cancel_reason}
            onChange={this.Res}
          />
          <span>Kindly Provide the Reason of College Cancellation</span>
        </label>
        <a href="#" className="waves-effect btn col l2 s4 blue-grey darken-2 sup right" onClick={this.handleCollegeCancel}>Submit</a>
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select from the DropDown</div>
      );
    }

}
  }
}





class Switch extends React.Component {

    constructor ( props ) {
        super( props );

		this.state = {
			isChecked: false,
      history:'',
		}
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
    }
    render () {

        return(
          <div>

            <div className="switch center">
                <label style={{color:'red',fontSize:'15px'}}>
                    Want to compensate on other day ?
                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    Want to Handover this class to Other Faculty?
                </label>
            </div>
            <br />
            <br /><br />
            <InputValue datas={this.state.isChecked}/>
          </div>
        );
    }

}

class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {

    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  addAllot =(e) =>{
    e.preventDefault();
  }
  componentDidMount()
  {

  }
  render(){
    if(this.props.datas === true)
    {
      return(
        <React.Fragment>
         <Add />
        </React.Fragment>
      );
    }
    else{
      return(
        <React.Fragment>
        <div className="row">
        <div className="col l4 input-field">
           <input id="fac1" type="text" required/>
           <label for="fac1">Mention the Date</label>
        </div>
        <div className="col l4 input-field">
           <input id="fac2" type="text" required/>
           <label for="fac2">Mention the DayOrder</label>
        </div>
        <div className="col l4 input-field">
           <input id="fac3" type="text" required/>
           <label for="fac3">Mention the slot</label>
        </div>
        </div>
        <a className="btn right green" onClick={this.addAllot}>Add Row</a>
        <a className="btn">Submit</a>
        </React.Fragment>
      );
    }
  }
}


const TableRow = ({ children }) => (
  <div className="row">{children}</div>
);
