import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';
import axios from 'axios'
import M from 'materialize-css';
import { } from 'materialize-css';
var empty = require('is-empty');


export default class Modal extends Component {
  constructor() {
      super()
      this.state = {
          mailid: '',
          official_id:'',
          redirectTo: null,
          loading: '',
      }
  }
  handleForgo=(e)=> {
      this.setState({
          [e.target.name]: e.target.value
      })
  }
  handleSubmit=(event)=> {
      event.preventDefault()
      if(empty(this.state.mailid) || empty(this.state.official_id)){
        window.M.toast({html: 'Enter the Details!!',classes:'rounded #f44336 red'});
        return false;
      }
      else{
      axios.post(this.props.forgot, {
              mailid: this.state.mailid,
              username:this.state.official_id,
          },
          this.setState({
            loading: "Sending Mail...",
          })
        )
          .then(response => {
              if (response.status === 200)
              {
                      if(response.data.success)
                        {
                          this.setState({
                           loading: '',
                           redirectTo: '/'
                          })
                         window.M.toast({html: 'Check your mail to reset password !!',outDuration:'6000', classes:'rounded #ffeb3b yellow black-text text-darken-2'});
                        }
                      else if(response.data.nodata)
                        {
                          this.setState({loading:response.data.nodata})
                        }
              }

          }).catch(error => {
            window.M.toast({html: 'Error !!!'});
          })
        }
  }
  componentDidMount() {
    const options ={
      inDuration: 250,
      outDuration: 250,
      opacity: 0.5,
      dismissable: false,
      startingTop: "94%",
      endingTop: "%"
    };
    M.Modal.init(this.Modal,options);
}
    render()
    {
      if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
        return(
          <div>
          <div className="btn-flat col s6 l6 xl6 m6 offset-s3 offset-l3 offset-m3 offset-xl3 but N/A transparent modal-trigger go" data-target="modal1">
          Forgot password?
          </div>
          <div ref={Modal => {
            this.Modal =Modal;
          }}
           id="modal1" className="modal">
      <div className="modal-content">
      <h4 className="forgohead center">Forgot Password</h4>
      <br />
      <p className="ppo">Please enter the details :</p>

      <div className="row">
      <div className="input-field col s6 l6 xl6 m6">
        <input id="officialid" type="text" name="official_id" value={this.state.official_id} onChange={this.handleForgo} className="validate" required/>
        <span className="helper-text" data-error="Please enter the officialid !!" data-success="">Your Official Id or Registration Number</span>
        <label htmlFor="officialid">Official Id</label>
      </div>
      <div className="input-field col s6 l6 xl6 m6">
        <input id="mail" type="email" name="mailid" value={this.state.mailid} onChange={this.handleForgo} className="validate" required/>
        <span className="helper-text" data-error="Please enter the mailid !!" data-success=""></span>
        <label htmlFor="mail">Mail Id</label>
      </div>
      <button className="waves-effect btn blue-grey darken-2 col s4 sup right" onClick={this.handleSubmit}>Send Mail</button>
      </div>
      <h6 className="center" style={{color:'#DF127F '}}>{this.state.loading}</h6>
     </div>
     <div className="modal-footer">
      <a href="#!" className="modal-close waves-effect waves-green btn-flat">Close</a>
      </div>
     </div>
     </div>
        )
    }

    }
}
