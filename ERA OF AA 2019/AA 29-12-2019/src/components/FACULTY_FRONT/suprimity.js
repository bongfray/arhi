import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import Nav from '../dynnav'

export default class Suprimity extends Component {
  constructor(){
    super()
    this.state={
      redirectTo:null,
      forgotroute:'/user/forgo',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
    axios.get('/user/')
    .then( res => {
        if(res.data.user){
          this.knowSuprimity()
        }
        else{
          this.setState({redirectTo:'/faculty'})
          window.M.toast({html: 'Not Logged In !!', classes:'rounded #f44336 red'});
        }
    });
  }
  knowSuprimity =()=>{
    axios.post('/user/knowSuprimityStatus')
    .then( res => {
        if(res.data === 'no'){
          this.setState({redirectTo:'/faculty'})
          window.M.toast({html: 'Not Authorized !!', classes:'rounded #f44336 red'});
        }
        else{
          this.setState({proceed:res.data})
        }
    });
  }
  render() {
    let path;
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
       if(this.state.proceed==="Add Skill"){
         path = <AddData type={this.state.proceed}/>
       }
       else if(this.state.proceed==="Add Domain"){
        path =  <AddData type={this.state.proceed} />
       }
       else if(this.state.proceed==="Add Path"){
         path = <AddPath />
       }
       else{
         path = <div></div>
       }
    return (
      <React.Fragment>
        {path}
      </React.Fragment>
    );
  }
 }
}

class AddData extends Component {
  constructor() {
    super()
    this.state={

    }
  }
  render(){
    return(
      <div>ff</div>
    )
  }
}


class AddPath extends Component {
  constructor() {
    super()
    this.state={

    }
  }
  render(){
    return(
      <div>ff</div>
    )
  }
}
