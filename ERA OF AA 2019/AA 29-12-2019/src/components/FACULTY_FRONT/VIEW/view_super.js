import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import { Select } from 'react-materialize'
import ContentLoader from "react-content-loader"
import Pdf from './pdf_alldata'
require("datejs")




export default class BasicView extends Component {
      _isMounted = false;
  constructor() {
    super()
    this.state ={
      start:'',
      isChecked: false,
      history: '',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      agreed:[],
      loading:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this._isMounted = true;
    this.getUser();
    M.AutoInit()
  }

    getUser()
    {
      axios.get('/user/'
    )
       .then(response =>{
        if (this._isMounted) {
          if(response.status === 200)
          {
            this.setState({
              loading:false,
            })
         if(response.data.user)
         {

         }
         else{
           this.setState({
             redirectTo:'/faculty',
           });
           window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
         }
       }
       }
       })
    }


  handleComp = (e) =>{
    window.M.toast({html: 'Validating..',outDuration:'3000', classes:'rounded pink'});
    axios.get('/user/validate_list_view')
    .then( response => {
      // console.log(response.data)
        if(response.data.permission === 1)
        {
          window.M.toast({html: 'Accepted!!',outDuration:'3000', classes:'rounded green'});
          this.setState({
            isChecked: !this.state.isChecked,
            start:response.data.data_send,
          })
        }
        else{
          window.M.toast({html: 'Denied Request !!',outDuration:'3000', classes:'rounded red'});
          this.setState({
            isChecked:false,
            redirectTo:'/view',
          })
        }
    });
  }
  render() {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="130" y="10" rx="3" ry="3" width="135" height="10" />
        <rect x="100" y="30" rx="3" ry="3" width="200" height="100" />

      </ContentLoader>
    )
    if(this.state.loading === true)
    {
      return(
        <MyLoader />
      );
    }
    else if(this.state.loading === false){

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

                            <div className="switch center"  style={{marginTop:'18px'}}>
                                <label style={{color:'red',fontSize:'15px'}}>
                                    Already Having Official Id
                                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                                    <span className="lever"></span>
                                    Don't have any Official Id !!
                                </label>
                            </div>
                            <br />
                            <Switch datas={this.state.isChecked} start={this.state.start}/>
      </React.Fragment>
    );
  }
}
  }
}


class Switch extends Component{
  constructor(props)
  {
    super(props)
    this.state={

    }
  }
  render()
  {
    let render_content;
    if(this.props.datas === true)
    {
      render_content =
      <ListShow  start={this.props.start}/>
    }
    else{
      render_content =
      <ViewByID />
    }
    return(
      <React.Fragment>
        {render_content}
      </React.Fragment>
    );
  }
}


class ListShow extends Component {
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      username:'',
      dept:'',
      desgn:'',
      designation:'',
      desgn_list:[],
      department:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/user/fetch_view_list',{start:this.props.start})
    .then( res => {
      // console.log(res.data)
        if(res.data)
        {
          this.setState({desgn_list:res.data});
          this.fetchDepartment();
        }
    });
  }
  fetchDepartment=()=>{
    axios.get('/user/fetch_department')
    .then( res => {
      // console.log(res.data)
        if(res.data)
        {
          this.setState({department:res.data})
        }
    });
  }
  handleDesgn =(e)=>{
    this.setState({desgn:e.target.value})
  }
  handleDept = (e)=>{
    this.setState({dept:e.target.value})
  }
  showDiv =(username)=>{
    if(username)
    {
      this.setState({display:'block',username:username})
    }
  }

  render() {
    return (
      <div>
       <div className="row">
          <div className="card col l5">
              <div className="row">
                 <div className="col l6">
                     <h6 className="center pink-text">Select the Designation</h6>
                     <Select l="12" xl="12" s="12" m="6" value={this.state.desgn} onChange={this.handleDesgn}>
                     <option value="" disabled defaultValue>Select Here</option>
                     {this.state.desgn_list.map((content,index)=>{
                       return(
                         <option value={content.designation_name} key={index}>{content.designation_name}</option>
                     )
                     })}
                   </Select>
                 </div>
                 <div className="col l6">
                    <h6 className="center pink-text">Select the Department</h6>
                    <Select l="12" xl="12" s="12" m="6" value={this.state.dept} onChange={this.handleDept}>
                    <option value="" disabled defaultValue>Select Here</option>
                    {this.state.department.map((content,index)=>{
                      return(
                        <option value={content.department_name} key={index}>{content.department_name}</option>
                    )
                    })}
                  </Select>
                 </div>
              </div>
          </div>
          <div className="col l7">
            <div className="card">
                <FetchUser dept={this.state.dept} desgn={this.state.desgn} showDiv={this.showDiv} />
         </div>

          </div>
          </div>
          <div className="row">
              <div>
               <ViewSubject display={this.state.display} username={this.state.username}/>
               </div>
          </div>
      </div>
    );
  }
}


class FetchUser extends Component {
  constructor(props) {
    super(props)
    this.state={
      users:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser();
  }
  componentDidUpdate =(prevProps)=>{
    if((prevProps.dept!==this.props.dept) || (prevProps.desgn!==this.props.desgn))
    {
      this.fetchUser();
    }
  }
  fetchUser = ()=>{
    axios.post('/user/fetchUser',{department:this.props.dept,designation:this.props.desgn})
    .then( res => {
      // console.log(res.data)
        this.setState({users:res.data})
    });
  }
  render()
  {
    return(
      <React.Fragment>
              <table>
                  <thead>
                  <tr>
                     <th>Serial No.</th>
                     <th>Name</th>
                     <th>Official Id</th>
                     <th>Mail Id</th>
                     <th>Contact No</th>
                     <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                  {this.state.users.map((content,index)=>{
                    return(
                      <tr key={index}>
                       <td>{index+1}</td>
                       <td>{content.name}</td>
                       <td>{content.username}</td>
                       <td>{content.mailid}</td>
                       <td>{content.phone}</td>
                       <td><div className="btn #fafafa grey lighten-5 pink-text" onClick={() => this.props.showDiv(content.username)}>View</div></td>
                      </tr>
                  )
                  })}

                  </tbody>
        </table>
      </React.Fragment>
    )
  }
}



class ViewByID extends Component {
  constructor() {
    super()
    this.state ={
      display:'none',
      username:'',
      redirectTo:'',
      send_user:'',
    }
    this.retriveValue = this.retriveValue.bind(this)
    this.authenticate = this.authenticate.bind(this)
  }
retriveValue =(e)=>{
  this.setState({username:e.target.value})
}
  authenticate =(e)=>{
    e.preventDefault()
    axios.post('/user/authenticate',{username:this.state.username})
    .then(res=>{
      console.log(res.data)
    if(res.data.username)
    {
      this.setState({
        display:'block',
        send_user:res.data.username
      })
      window.M.toast({html: res.data.allowed ,outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
    }
    else{
      this.setState({display:'none'})
      window.M.toast({html: res.data ,outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
    }

    })
  }

  render()
  {
    return(
      <React.Fragment>
         <div className="row">
         <div className="col l3 xl3" />
           <div className="col l6 s12 m12 xl6">
               <div className="form-signup">
                  <div className="row">
                          <div className="input-field">
                             <input type="text" value={this.state.user} onChange={this.retriveValue} id="id" className="validate" required />
                             <label htmlFor="id">Input Official ID</label>
                          </div>
                          <button className="btn col l2 pink right" onClick={this.authenticate} >View</button>
                  </div>
                  <br />
                  <div className="row">
                     <span className="green-text">DISCLAIMER: </span>Please Remember in this section you can only see the User's Data only if they are not coming in your same or upper designation level.
                     One more thing keep in mind users in medium level(like HOD , Professor) have only access to view datas within their department only.
                  </div>
                </div>
           </div>
         <div className="col l3 s12 m12 xl3" />
         </div>
         <div className="row">
             <div style={{display:this.state.display}}>
              <ViewSubject display={this.state.display} username={this.state.send_user}/>
              </div>
         </div>
      </React.Fragment>
    );
  }
}




class ViewSubject extends React.Component{
  constructor()
  {
    super()
    this.state={
      loading:true,
      option:'',
      redirectTo:'',
      modal: false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }

 handleOption = (e)=>{
   this.setState({option:e.target.value})
 }
 render()
 {
   console.log(this.props.display)


   if (this.state.redirectTo) {
     return <Redirect to={{ pathname: this.state.redirectTo }} />
 } else {
   return(
     <React.Fragment>
     <div style={{marginTop:'15px',display:this.props.display}} className="row form-signup">
     <div className="col l4" style={{fontSize:'20px'}}>
       You are seeing the profile of <span className="pink-text">{this.props.username}</span>
    </div>
     <div  className="col l5">
         <Select l="12" value={this.state.option} onChange={this.handleOption}>
          <option value="" disabled defaultValue>Choose the Field Here...</option>
          <option value="Section">Section Datas</option>
          <option value="Academic">Academic Data</option>
          <option value="Administrative">Admininstrative Datas</option>
          <option value="Research">Research Datas</option>
          <option value="Five">Five Year Plan Data</option>
       </Select>
       </div>
       <div className="col l3"><Pdf username={this.props.username}/></div>
       </div>
    <div className="row">
           <Decider data={this.state.option} username={this.props.username}/>
    </div>
   </React.Fragment>
 );
}

 }
}

class Decider extends Component{
  render()
  {
    let body,route;
    if(this.props.data === "Section")
    {
      body = <Section username={this.props.username}/>
    }
    else if(this.props.data === "Five")
    {
      body = <Five username={this.props.username}/>
    }
    else if((this.props.data === "Administrative"))
    {
      route = '/user/fetch_adminis_for_view';
      body = <AdRes action={route} username={this.props.username} />
    }
    else if(this.props.data === "Research")
    {
      route = '/user/fetch_research_for_view';
      body = <AdRes action={route} username={this.props.username} />
    }
    else if(this.props.data === "Academic")
    {
      route = '/user/fetch_academic_for_view';
      body = <AdRes action={route} username={this.props.username} />
    }
    else{
      body
       = <div></div>
    }
    return(
      <React.Fragment>
      {body}
      </React.Fragment>
    )
  }
}

class Section extends Component {
  constructor() {
    super()
    this.state={
      section_data:[],
      visible: 2,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    axios.post('/user/fetch_section_for_view',{username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        section_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let sec_data;
    if(this.state.loader === true)
    {
      sec_data = <h5 className="center"> Fetching Datas....</h5>
    }
    else{
      sec_data =       <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">

                {this.state.section_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}}>
                        <h6 className="count center">{item.action}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>Description</p>
                             <p>Your Role</p>
                             <p>Your Achivements</p>
                             <p>Duration</p>
                           </div>
                           <div className="col l6">
                             <p>{item.description}</p>
                             <p>{item.role}</p>
                             <p>{item.achivements}</p>
                             <p>{item.date_of_starting} / {item.date_of_complete}</p>
                           </div>
                        </div>
                      </div>
                      </div>

                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.section_data.length &&
                   <button style={{background:'linear-gradient(to bottom, #ff5050 0%, #ff99cc 100%)'}} onClick={this.loadMore} type="button" className="btn load-more">Load more</button>
                }
                </div>
              </React.Fragment>
    }
    return (
      <React.Fragment>
         {sec_data}
        </React.Fragment>
    );
  }
}


class AdRes extends Component {
  constructor() {
    super()
    this.state={
      free_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    this.fetchData(this.props.action);
  }

  componentDidUpdate =(prevProps)=>{
    if(prevProps.action !== this.props.action)
    {
      this.fetchData(this.props.action)
    }
  }

  fetchData=(route)=>{
    axios.post(route,{username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        free_data: res.data,
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let adres_data;
    if(this.state.loader === true)
    {
      adres_data = <h5 className="center"> Fetching Datas....</h5>
    }
    else{
      adres_data =      <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">

                {this.state.free_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment  key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}}>
                        <h6 className="count center">{item.freefield}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>About the Responsibility</p>

                             {item.freeslot && <p>Content of Submission</p>}
                             {item.covered && <p>Topics Covered</p>}
                             {item.problem_statement && <p>Problem in Slot Handle</p>}

                             <p>Submitted Date</p>
                             <p>Submitted DayOrder.Slot</p>
                           </div>
                           <div className="col l6">
                             <p>{item.freeparts}</p>
                             {item.freeslot && <p>{item.freeslot}</p>}
                             {item.covered && <p>{item.covered}</p>}
                             {item.problem_statement && <p>{item.problem_statement}</p>}

                             <p>{item.date} / {item.month} /{item.year}</p>
                             <p>{item.day_slot_time}</p>
                           </div>
                        </div>
                      </div>
                      </div>
                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.free_data.length &&
                   <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
                }
                </div>
              </React.Fragment>
    }
    return (
      <React.Fragment>
        {adres_data}
        </React.Fragment>
    );
  }
}


class Five extends Component {
  constructor() {
    super()
    this.state={
      five_data:[],
      visible: 3,
      error: false,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  componentDidMount()
  {
    axios.post('/user/fetch_five_for_view',{username:this.props.username}).then(res => {
      this.setState({
        loader:false,
        five_data: res.data
      });
    }).catch(error => {
      this.setState({
        error: true
      });
    });
  }

  render()
  {
    let five_dat;
    if(this.state.loader === true)
    {
      five_dat = <h5 className="center"> Fetching Datas....</h5>
    }
    else{
      five_dat =       <React.Fragment>
              <div className="tiles row" style={{marginLeft:'10px',marginRight:'10px'}} aria-live="polite">

                {this.state.five_data.slice(0, this.state.visible).map((item, index) => {
                    return (
                      <React.Fragment key={index}>
                      <div>
                      <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}} >
                        <h6 className="count center">{item.action}</h6><hr />
                        <div className="row">
                           <div className="col l6">
                             <p>About the Plan</p>
                             <p>Plan Will Expire on</p>
                             <p>Completed Status</p>
                           </div>
                           <div className="col l6">
                             <p>{item.content}</p>
                             <p>Year {item.expire_year}</p>
                             {item.completed?  <p>Completed</p> : <p>Not Completed</p>}
                           </div>
                        </div>
                      </div>
                      </div>
                      </React.Fragment>
                    );
                  })}

                </div>
                <br />
                <div className="row center">
                {this.state.visible < this.state.five_data.length &&
                   <button onClick={this.loadMore} type="button" className="btn load-more pink">Load more</button>
                }
                </div>
              </React.Fragment>
    }




    return (
      <React.Fragment>
       {five_dat}
        </React.Fragment>
    );
  }
}

//
//  class ShowDiv extends Component {
//    constructor(props) {
//      super(props)
//      this.state={
//          isChecked: false,
//          isCheckedS: false,
//          choosed:'',
//          agreed:[{name:'radio1',value:'Academic'},{name:'radio2',value:'Administrative'},{name:'radio3',value:'Research'},{name:'radio4',value:'Degrees'},{name:'radio5',value:'Section Datas'},{name:'radio6',value:'Five Year Plan'},],
//      }
//      this.componentDidMount = this.componentDidMount.bind(this)
//    }
//    componentDidMount()
//    {
//
//    }
//    handleChecked =(e,index)=>{
//        this.setState({
//          isChecked: !this.state.isChecked,
//          choosed: e.target.value
//        });
//    }
//    handleComp=()=>{
//      this.setState({
//        isCheckedS: !this.state.isCheckedS,
//      });
//    }
//    render()
//    {
//      return(
//        <React.Fragment>
//            <div className="card" style={{display:this.props.display}}>
//             <div className="row">
//                <div className="col l12">
//                   <div className="center card-title yellow" style={{fontSize:'20px'}}>
//                      You are seeing the profile of <span className="pink-text">{this.props.username}</span>
//                   </div>
//                   <div className="row" style={{marginTop:'20px'}}>
//                   <div className="center col l8">
//                   <h6>Download OverAll Report</h6>
//                   </div>
//                   <div className="col l4">
//                   <button className="btn blue-grey darken-2 tooltipped left" data-position="bottom" data-tooltip="Download the Report"> <i class="medium material-icons">file_download</i></button></div>
//                   </div>
//
//                                               <div className="switch center"  style={{marginTop:'18px'}}>
//                                                   <label style={{color:'red',fontSize:'15px'}}>
//                                                       <input  checked={ this.state.isCheckedS } value="split" onChange={ this.handleComp} type="checkbox" />
//                                                       <span className="lever"></span>
//                                                        Get Report For Separate Request
//                                                   </label>
//                                               </div>
//                     {this.state.isCheckedS === true && <div className="row" >
//                  {this.state.agreed.map((content,index)=>(
//                          <div className="col l6 xl6 s10 m10 form-signup" key={index}>
//                          <p>
//                          <label>
//                          <input type='radio' className="with-gap" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
//                          <span style={{color:'green'}}><b>{content.value}</b></span>
//                          </label>
//                          </p>
//                          </div>
//                  ))}
//                  </div>
//                }
//             </div>
//             <div className="col l10" style={{display:this.props.display}}>
//               <ShowData choosed={this.state.choosed} username={this.props.username}/>
//             </div>
//           </div>
//         </div>
//        </React.Fragment>
//      )
//    }
//  }
//
//
//
//
//
// class  ShowData extends Component {
//   constructor(props) {
//     super(props)
//     this.state ={
//       option:'',
//     }
//     this.componentDidMount = this.componentDidMount.bind(this)
//   }
//   componentDidMount()
//   {
//
//   }
//
//   render()
//   {
//     return(
//       <div></div>
//     )
//   }
// }
