import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import Slogo from './Slogo.png'
import M from 'materialize-css'
import Notification from './notification'
import ContentLoader from "react-content-loader"
export default class Navbar extends Component {
  constructor()
  {
    super()
    this.state ={
      redirectTo:'',
      show:'none',
      loading:true,
      loggedIn:false,
      username:'',
      nav:[],
      notidisp:'none',
      newDisplay:'none',
      count_noti:'',
      showSidebar:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.logout = this.logout.bind(this)
  }
  getUser = () =>{
    axios.get(this.props.get).then(response => {
      if (response.data.user) {
        this.fetch();
        this.newNoti();
        this.setState({
          loggedIn: true,
          username: response.data.user.username,

        })
      } else {
        this.setState({
          loading:false,
          loggedIn: false,
          username: null,
        })
      }
    })
  }


  fetch()
  {
    axios.get(this.props.nav_route).then(response =>{
      this.setState({
       nav: response.data,
     })
    })
  }

  componentDidMount = () => {
    M.AutoInit();
    this.getUser();
}

newNoti = () =>{
  axios.get('/user/fetch_new_notification')
  .then( res => {
    this.setState({loading:false})
    if(res.data === 'no_u')
    {
      this.setState({newDisplay:'none'})
    }
      else if(res.data.length === 0)
      {
        this.setState({newDisplay:'none'})
      }
      else{
        this.setState({newDisplay:'block',count_noti:res.data.length})
      }
  });
}

renderSlide =() =>{
  this.setState({showSidebar:!this.state.showSidebar})
}


    logout(event) {
        event.preventDefault()
          window.M.toast({html: 'Logging Out....!!',classes:'pink rounded'});
        axios.post(this.props.logout).then(response => {
          if (response.status === 200) {
            this.setState({
              loggedIn: false,
              username: null,
              redirectTo:'/',
            })
            window.M.toast({html: 'Logged Out....!!',classes:'green rounded'});
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }


      notiPush =() =>{
        this.setState({notidisp:'block'})
        axios.post('/user/mark_as_read')
        .then( res => {
          if(res.data === 'ok')
          {
            this.setState({newDisplay:'none'})
          }
        });
      }
      setDisp = (object) =>{
        this.setState(object);
      }
    render() {
      let output;
      const MyLoader = () => (
        <ContentLoader
          height={15}
          width={400}
          speed={2}
          primaryColor="#E5E8E8"
          secondaryColor="#F8F9F9"
        >
          <rect x="10" y="3" rx="3" ry="3" width="350" height="4" />
          <rect x="15" y="10" rx="3" ry="3" width="380" height="4" />
        </ContentLoader>
      )
  if(this.state.loading === true)
  {
    output = <MyLoader />
  }
  else
  {
 if(this.state.loggedIn === false)
{
  output=
    <div className="navbar-fixed">
  <nav>
    <div className="nav-wrapper blue-grey darken-2">
      <a href="#!" className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a>
      <div className="row">
      <div className="col s2 m2 xl2 l2" />
      <div className="col s8 m8 xl8 l8">
       <div className="row">
       <div className="col l1 xl1"/>
       <div className="col l11 xl11">
         <a href="http://care.srmuniv.ac.in" style={{paddingLeft:'50px'}} className="con nav-cen hide-on-med-and-down">SRM Centre for Applied Research in Education</a>
       </div>
      </div>
      </div>
      <div className="col s2 xl2 m2 l2">
      </div>
      </div>
    </div>
  </nav>
</div>
}
else if(this.state.loggedIn === true)
{
        output=
          <div className="navbar-fixed">
                     <nav>
                     {this.state.showSidebar=== true &&
                       <div className="cover">
                       <div className="notinoti N/A transparent" onClick={this.renderSlide}>
                       <i onClick={this.renderSlide} style={{marginLeft:'10px'}} className="go medium material-icons">close</i>
                       <div className="row">
                         <div className="col s6 m6 white">
                           <Link to={this.props.home}>HOME</Link><br />
                           {this.state.nav.map((content,index)=>{
                             return(
                               <React.Fragment key={index}>
                               <Link to={content.link}>{content.val}</Link><br />
                               </React.Fragment>
                             )
                           })}
                           <hr />
                           <Link to="#" className="center" onClick={this.logout}>
                           LOG OUT
                           </Link>
                         </div>
                       </div>
                       </div>
                       </div>
                     }
                          <div className="nav-wrapper blue-grey darken-2">
                          <div className="row">
                            <div className="col l2 s2 xl2 m2">

                            <div className="left hide-on-large-only">
                                  {this.state.showSidebar=== false && <i onClick={this.renderSlide} className="go medium material-icons">dehaze</i> }
                            </div>
                                 <div className="brand-logo"><a href="http://srmuniv.ac.in"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a></div>
                            </div>
                            <div className="col l7 s7 xl7 m7 hide-on-med-and-down ">
                                 <div className="center con" style={{marginLeft:'120px'}}><a href="http://care.srmist.edu.in">SRM CARE</a></div>
                            </div>
                            <div className="col l3 s4 xl3 m4 push-s7 push-m7">
                                <ul className="right">
                                  {this.props.home && <li title="Home">
                                    <Link to={this.props.home}><i className=" hide-on-med-and-down material-icons">home</i></Link>
                                  </li>}
                                  <li title="Log Out">
                                      <Link to="#" className="center" onClick={this.logout}>
                                      <i className="hide-on-med-and-down material-icons right go">exit_to_app</i>
                                      </Link>
                                  </li>
                                  <li title="notification" onClick={this.notiPush}>
                                        <Link to="#" className="center">
                                          <span style={{display:this.state.newDisplay}} className="notification-number">{this.state.count_noti}</span>
                                          <i className="material-icons center">notifications</i>
                                        </Link>
                                  </li>
                                  {this.props.nav_route && <li title="menu" className="hide-on-med-and-down droppp">
                                    <i className="material-icons men" >more_vert</i>
                                    <ul>
                                      {this.state.nav.map((content,index)=>{
                                        return(
                                          <li key={content.val}><Link to={content.link}>{content.val}</Link></li>
                                        )
                                      })}
                                    </ul>
                                    </li>}
                                </ul>
                            </div>
                      </div>
                          </div>
                      </nav>
                      </div>
      }
    }
    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <React.Fragment>
        {this.state.notidisp === 'block' ? <Notification setDisp={this.setDisp}/>
        : <React.Fragment> {output} </React.Fragment>
        }
        </React.Fragment>
      )
    }

    }
}
