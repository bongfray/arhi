const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise

//Schema
const timeSchema = new Schema({
  username: { type: String, unique: false, required: false },
  day_order: { type: Number, unique: false, required: false },
  day_slot_time: { type: String, unique: false, required: false },
  order: { type: String, unique: false, required: false },
  selected: { type: String, unique: false, required: false },
  freefield: { type: String, unique: false, required: false },
  freeslot: { type: String, unique: false, required: false },
  freeparts: { type: String, unique: false, required: false },
  compensation_status: { type: String, unique: false, required: false },
  c_cancelled: { type: String, unique: false, required: false },
  problem_statement: { type: String, unique: false, required: false },
  covered: { type: String, unique: false, required: false },
  time: { type: String, unique: false, required: false },
  day_sl: { type: String, unique: false, required: false },
  slot: { type: String, unique: false, required: false },
  date: { type: Number, unique: false, required: false },
  month: { type: Number, unique: false, required: false },
  year: { type: Number, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  problem_compensation_date: { type: Number, unique: false, required: false },
  problem_compensation_month: { type: Number, unique: false, required: false },
  problem_compensation_year: { type: Number, unique: false, required: false },
  problem_compensation_hour: { type: Number, unique: false, required: false },

})
const Time_table = mongoose.model('TimeTable', timeSchema)
module.exports = Time_table
