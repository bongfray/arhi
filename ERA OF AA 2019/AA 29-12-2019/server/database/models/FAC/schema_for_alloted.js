const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const blueprint = new Schema({
  username: {type: String , unique: false, required: false},
  alloted_slots: { type: String, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  timing: {type: String, unique: false , required: false},
  complete_slots_status: {type: String, unique: false , required: false},
  sum_submit: {type: Number, unique: false , required: false},
})


blueprint.plugin(autoIncrement.plugin, { model: 'AllotedSlot-Details', field: 'serial', startAt: 1,incrementBy: 1 });

const BluePrint = mongoose.model('AllotedSlot-Details', blueprint)
module.exports = BluePrint
