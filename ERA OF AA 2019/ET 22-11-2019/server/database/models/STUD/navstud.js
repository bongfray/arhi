const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const nav = new Schema({
  val: {type: String , unique: false, required: false},
  link: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
})



nav.plugin(autoIncrement.plugin, { model: 'NavStudent', field: 'serial', startAt: 1,incrementBy: 1 });

const NavS = mongoose.model('student_nav', nav)
module.exports = NavS
