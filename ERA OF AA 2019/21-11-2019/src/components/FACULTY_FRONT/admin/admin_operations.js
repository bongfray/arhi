import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Nav from '../../dynnav'

import Display from './Operations/display'


export default class Ap extends Component{
  constructor() {
    super()
    this.state={
      isChecked:0,
      active:0,
      logout:'/user/logout',
      get:'/user/',
      nav_route: '',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
      this.fetchlogin();
  }
  fetchlogin = () =>{
      axios.get('/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
  handleComp = (e) =>{
    this.setState({
      isChecked: e,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }
  toggleDiv =()=>{
    this.setState({toggled:!this.state.toggled})
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "col l3 xl3 s6 m3 #00695c teal darken-3 white-text center go active-pressed";
      }
      return "col l3 xl3 s6 m3 center go";
  }

  render()
  {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
    return(
        <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div className="center">
               {this.state.toggled ===  false && <React.Fragment><i className="dig small material-icons go hide-on-med-and-down" onClick={this.toggleDiv}>arrow_drop_down_circle</i>
               <i className="dig small material-icons go hide-on-large-only" style={{marginTop:'30px'}} onClick={this.toggleDiv}>arrow_drop_down_circle</i>
               </React.Fragment>}
               {this.state.toggled ===  true && <div className="card dig center">
               <div className="row" style={{marginTop:'15px'}}>
                      <div className="col l2 xl2 hide-on-mid-and-down"/>
                      <div className="col l8 xl s12 m12">
                         <ul className="row">
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(0)}} className={this.color(0)}><b>System</b></li>
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(1)}} className={this.color(1)}><b>Admin Operations</b></li>
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(2)}} className={this.color(2)}><b>ARHI</b></li>
                           <li style={{  fontSize:'15px',height:'30px'}} onClick={(e)=>{this.handleComp(3)}} className={this.color(3)}><b>Complaints/Report</b></li>
                         </ul>
                       </div>
                       <div className="col l2 xl2 hide-on-mid-and-down"/>
               </div>
               <i class="small material-icons go" onClick={this.toggleDiv}>arrow_upward</i>
               </div>
               }
          </div>
          <div className="hide-on-med-and-down" style={{marginTop:'17px'}}>
              <System choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>
          <div className="hide-on-large-only" style={{marginTop:'60px'}}>
              <System choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>

       </React.Fragment>
    )
  }
  }
}


class System extends Component {
  constructor() {
    super()
    this.state={
      activated_user:'',
      suspended_user:'',
      suser:'',
      user:'',
      total_user:'',
      d_user:'',
      admin_user:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchUser()
  }
  fetchUser()
  {
    axios.get('/user/know_no_of_user')
    .then( res => {
      const activated_user = (res.data.filter(item => item.active === true)).length;
      const suspended_user = (res.data.filter(item => item.suspension_status === true)).length;
      const user = (res.data.filter(item => item.count=== 0 || item.count === 1 || item.count === 2)).length;
      const admin_user = (res.data.filter(item => item.h_order=== 0)).length;
      const d_user = (res.data.filter(item => item.h_order=== 0.5)).length;
        this.setState({activated_user:activated_user,suspended_user:suspended_user,admin_user:admin_user,user:user,d_user:d_user})
        this.fetchStudent(res.data.length);
    });

  }
  fetchStudent(user)
  {
    axios.get('/user2/know_no_of_suser')
    .then( res => {
        let total =res.data.length;
        this.setState({total_user:user+total,suser:res.data.length})
    });
  }
  render()
  {
    if(this.props.choice === 1)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Admin />
        }
        </React.Fragment>
      )
    }
    else if(this.props.choice === 3)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Complaints />
        }
        </React.Fragment>
      )
    }
    else{
      return(
        <React.Fragment>
        <h5 className="center con teal-text" style={{marginTop:'50px'}}>Welcome To EWork Admin Portal</h5><br /><br />
         <div className="row">
            <div className="col l2">
               <div className="card">
               <div className="pink white-text center">TOTAL USER</div>
               <div className="card-content center">
                <h5>{this.state.total_user}</h5>
               </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
               <div className="pink white-text center">FACULTY</div>
               <div className="card-content center">
                <h5>{this.state.user}</h5>
               </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
               <div className="pink white-text center">STUDENT</div>
               <div className="card-content center">
                <h5>{this.state.suser}</h5>
               </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
                 <div className="pink white-text center">DEPARTMNET ADMIN</div>
                 <div className="card-content center">
                   <h5>{this.state.d_user}</h5>
                 </div>
               </div>
            </div>
            <div className="col l2">
               <div className="card">
                 <div className="pink white-text center">ADMIN USER</div>
                 <div className="card-content center">
                   <h5>{this.state.admin_user}</h5>
                 </div>

               </div>
            </div>
            <div className="col l2">
                <div className="card">
                  <div className="pink white-text center">SUSPENDED USER</div>
                  <div className="card-content center">
                    <h5>{this.state.suspended_user}</h5>
                  </div>
                </div>
            </div>
         </div>

        </React.Fragment>
      )
    }
  }
}


class Admin extends Component{
constructor(){
    super();
    this.state ={
        radio:[{name:'radio1',color:'blue',value:'Insert'},{name:'radio2',color:'black',value:'Update'},{name:'radio3',color:'orange',value:'Super'},{name:'radio4',color:'red',value:'Delete'},{name:'radio5',color:'yellow',value:'Modify'},{name:'radio6',color:'aqua',value:'View'}],
        isChecked: false,
        choosed: '',
        active:'',
    }
}

handleChecked =(e,index,color)=>{
    this.setState({
        isChecked: !this.state.isChecked,
        choosed: e.target.value
    });
    if (this.state.active === index) {
      this.setState({active : null})
    } else {
      this.setState({active : index})
    }
}
color =(position) =>{
  if (this.state.active === position) {
      return "col l10 s10 m10 form-signup #18ffff cyan accent-2";
    }
    return "col l10 s10 m10 form-signup black-text";
}

render()
{

    return(
        <React.Fragment>
       <div className="row">
            {this.state.radio.map((content,index)=>(
                <div className="col s6 l2 m2" key={index}>
                    <div className={this.color(index)}>
                    <p>
                    <label>
                    <input type='radio' className="yellow" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                    <span style={{letterSpacing: '.1em'}} className="black-text"><b>{content.value}</b></span>
                    </label>
                    </p>
                    </div>
              </div>
            ))}
       </div>

            <div className="row">
                <div className="col s12 m12 l12">
                   <Display choosed={this.state.choosed}/>
                </div>
            </div>
        </React.Fragment>
    );
}
}



class Complaints extends Component {
  constructor() {
    super()
    this.state={
      complaint:[],
      popupVisible: false,
      complaint_details:'',
      complaint_subject:'',
      official_id:'',
      serial_no:0,
      reply:false,
      forward:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.handleClick = this.handleClick.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
  }
  componentDidMount()
  {
    this.fetchComplaint();
  }


  fetchComplaint =()=>{
    axios.get('/user/fetch_complaint')
    .then( res => {
        if(res.data){
          this.setState({complaint:res.data})
        }
    });
  }


    handleClick() {
      if (!this.state.popupVisible) {
        document.addEventListener('click', this.handleOutsideClick, false);
      } else {
        document.removeEventListener('click', this.handleOutsideClick, false);
      }

      this.setState(prevState => ({
         popupVisible: !prevState.popupVisible,
      }));
    }

    handleOutsideClick(e) {
      if (this.node.contains(e.target)) {
        return;
      }
      this.handleClick();
    }

    sendData =(id,complaint_subject,complaint,serial)=>{
      this.setState({official_id:id,complaint_subject:complaint_subject,complaint_details:complaint,serial_no:serial})
    }
    onReply =(e)=>{
      if(e === 0)
      {
              this.setState({reply:true,forward:false})
      }
      else
      {
          this.setState({forward:true,reply:false})
      }
    }

    handleInput =(e)=>{
      this.setState({[e.target.name]:e.target.value})
    }

    sendReply =(no,id)=>{
      axios.post('/user/send_reply_to_complaint',{serial:no,official_id:id,message:this.state.reply_message})
      .then( res => {
        if(res.data === 'done')
        {
          this.setState({reply:false})
          window.M.toast({html: 'Replied!!',outDuration:'9000', classes:'rounded green darken-2'});
        }
      });
    }

  render()
  {
    console.log(this.state.detail_view)
    return(
      <React.Fragment>
      <div className="row">
         <div className="col l3 xl3 s4 m4">
            <div className="heading">
                <div className="row">
                  <div className="col l8">
                     <h4 className="ework-color">eWork</h4>
                     <label className="red-text">Complaints or Report</label>
                  </div>
                  <div className="col l4">
                   <div className="" ref={node => { this.node = node; }}>
                    <i className="small right material-icons go" onClick={this.handleClick}>more_vert</i>
                    {this.state.popupVisible && <ul className="card over-off">
                      <li className="center">Mark all as read</li>
                      <li className="center">Delete all</li>
                    </ul>
                     }
                     </div>
                  </div>
                </div>
            </div>

              {this.state.complaint.map((content,index)=>{
                return(
                  <div key={index} className="row" onClick={() => this.sendData(content.official_id,content.complaint_subject,content.complaint,content.serial)}>
                    <div className="col l8">
                      <h5>{content.complaint_subject}</h5>
                      <label className="dotted">{content.complaint}</label>
                    </div>
                    <div className="col l4">
                       <i style={{marginTop:'20px'}} className="small right material-icons go">delete</i>
                    </div>
                  </div>
                )
              })}
         </div>
         <div className="col l9 xl9 s8 m8">
            <div className="form-signup">
              {this.state.id &&
                 <React.Fragment>
                    <div className="row">
                      <div className="col l5">
                             <h6 className="center pink white-text" style={{padding:'8px'}}>COMPLAINT DETAILS</h6>
                              <div className="row">
                                 <label className="">Complaint From : </label><span>{this.state.official_id}</span>
                              </div>
                              <div className="row">
                                 <label className="">Complaint Subject : </label><span>{this.state.complaint_subject}</span>
                              </div>
                              <div className="row">
                                 <label className="">Complaint Details : </label><span  style={{wordWrap: 'break-word'}}>{this.state.complaint_details}</span>
                              </div>
                              <div className="row">
                              <div className="col l6"><span className="styled-btn" onClick={()=>this.onReply(0)}>Reply</span></div>
                              <div className="col l6"><span className="styled-btn right" onClick={()=>this.onReply(1)}>Forward</span></div>
                              </div>
                      </div>
                      <div className="col l1">
                          <div class="outer">
                            <div class="inner"></div>
                          </div>
                      </div>
                      <div className="col l6">
                          {this.state.reply &&
                             <React.Fragment>
                                <div className="row">
                                      <h6 className="center pink white-text" style={{padding:'8px'}}>REPLY TO USER</h6>
                                      <div className="row">
                                        <div className="col l6">
                                          <label className="">To : </label><span>{this.state.official_id}</span>
                                        </div>
                                      </div>
                                      <div className="row">
                                        <div className="col l12">
                                          <label className="pure-material-textfield-outlined alignfull">
                                            <textarea
                                              className=""
                                              type="text"
                                              placeholder=" "
                                              min="10"
                                              max="60"
                                              name="reply_message"
                                              value={this.state.reply_message}
                                              onChange={this.handleInput}
                                            />
                                            <span>Enter the Message</span>
                                          </label>
                                        </div>
                                      </div>
                                      <button className="right btn" onClick={()=>this.sendReply(this.state.serial_no,this.state.official_id)} style={{marginBottom:'5px'}}>REPLY</button>
                                </div>
                             </React.Fragment>
                          }
                          {this.state.forward &&
                             <React.Fragment>
                                <div className="row">
                                <h6 className="center pink white-text" style={{padding:'8px'}}>FORWARD MESSAGE</h6>
                                   <div className="input-field">
                                      <input id="forward-to" name="sendto" value={this.state.sendto} onChange={this.handleInput} type="text" />
                                      <label htmlFor="forward-to">Send To(Should be Official Id)</label>
                                   </div>
                                </div>
                                <div style={{padding:'3px'}}>
                                  <div className="row">
                                     <label className="">Complaint From : </label><span>{this.state.official_id}</span>
                                  </div>
                                  <div className="row">
                                     <label className="">Complaint Subject : </label><span>{this.state.complaint_subject}</span>
                                  </div>
                                  <div className="row">
                                     <label>Complaint Details : </label><span className="dotted">{this.state.complaint_details}</span>
                                  </div>
                                </div>
                                <div className="row">
                                   <div className="input-field">
                                      <input id="mm" name="message_for_receiver" value={this.state.message_for_receiver} onChange={this.handleInput} type="text" />
                                      <label htmlFor="mm">Any Message For Receiver</label>
                                   </div>
                                </div>
                                <button className="btn right" onClick={this.forwardMsg}>Forward</button>
                             </React.Fragment>
                          }
                      </div>
                    </div>
                 </React.Fragment>
              }
            </div>
         </div>
      </div>
      </React.Fragment>
    );
  }
}




class ShowMessage extends Component {
  constructor()
  {
    super()
    this.state ={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  componentDidUpdate =(prevProps) =>{
    if(prevProps.data!==this.props.data)
    {

    }
  }
  render() {
    return (
      <div>
        {this.props.detail_view.map((content,index)=>{
          return(
            <React.Fragment>
            <div className="row">
               <label className="">Complaint From : </label><span>{content.id}</span>
            </div>
            <div className="row">
               <label className="">Complaint Subject : </label><span>{content.complaint_subject}</span>
            </div>
            <div className="row">
               <label className="">Complaint Details : </label><span>{content.complaint}</span>
            </div>
            </React.Fragment>
          );
        })}
      </div>
    );
  }
}
