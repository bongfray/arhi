import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import Collap from './navin'
export default class Section extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      redirectTo:'',
      option:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}

componentDidMount(){
  M.AutoInit()
}


  render()
  {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <div className="row">
        <div className="col l2 s4 m4">
            <select name="title" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled selected>Select Here...</option>
            <option value="faculty">Faculty Nav</option>
            <option value="student">Student Nav</option>
            <option value="department_admin">Department Admin Nav</option>
            </select>
        </div>
        <div className="col l5 m4 s4" />
        <div className="col l5 ms s4" />
        </div>
          <Collap options={this.state.option} username={this.state.username}/>
      </React.Fragment>

    )
  }
  }
}
