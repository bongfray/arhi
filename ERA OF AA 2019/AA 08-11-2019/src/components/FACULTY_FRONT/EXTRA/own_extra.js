import React, { Component } from 'react';
import { } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import ForeignExtra from './foreign_extra'
import TimeDiv from '../TimeTable/slot2'
require("datejs")


export default class ownExtra extends Component {
  constructor()
  {
    super()
    this.state={
      isChecked: false,
      history:'',
      redirectTo:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      username:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  handleComp = (e) =>{
    this.setState({
      isChecked: !this.state.isChecked,
      history: e.target.value,
    })
  }
  componentDidMount()
  {
    this.loggedin()
  } 
  loggedin = ()=>{
    axios.get('/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.username})
      }
      else{
        this.setState({redirectTo:'/faculty'})
         window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }
  render() {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return (
      <React.Fragment>
                <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <div>

        <div className="switch center" style={{marginTop:'30px'}}>
            <label style={{color:'red',fontSize:'15px'}}>
                Extra Slot History
                <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                <span className="lever"></span>
                Request From Other Faculty to Handle Extra Slot
            </label>
        </div>
        <br />
        <br /><br />
        <InputValue datas={this.state.isChecked} username={this.state.username} />
      </div>
      </React.Fragment>
    );
  }
  }
}



class InputValue extends Component{
  constructor()
  {
    super();
    this.state= {
      date:'',
      dayorder:'',
      hour:'',
      day_order:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  addAllot =(e) =>{
    this.setState({[e.target.name]:e.target.value})
  }
  componentDidMount()
  {
    this.fetchdayorder()
  }
  fetchdayorder =()=>{
    axios.get('/user/fetchdayorder')
    .then(response =>{
          this.setState({
            day_order: response.data.day_order,
          });
    });
  }
  render(){
    if(this.props.datas === true)
    {
      return(
        <React.Fragment>
          <ForeignExtra />
        </React.Fragment>
      );
    }
    else{
      return(
        <React.Fragment>
          <OwnExtra username={this.props.username} day_order={this.state.day_order} />
        </React.Fragment>
      );
    }
  }
}


class  OwnExtra extends Component {
  constructor() {
    super()
    this.state={
      compense_data:[],
      modal: false,
      color:'red',
      notfound:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    M.AutoInit()
    this.fetchCompenseClasses()

  }
  fetchCompenseClasses = ()=>{
    axios.get('/user/fetchcompense')
    .then( res => {
      if(res.data.length === 0)
      {
        this.setState({notfound:'No Data Found !!'})
      }
      else{
        this.setState({compense_data:res.data})
      }
    });
  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}

color =(hour,day,month,year) =>{
	axios.post('/user/fetchfrom', {
		slot_time: this.props.day_order+'.'+hour,
		day: day,
		month:month,
		year: year,
	}
)
	.then(response =>{
		if(response.data)
		{
				this.setState({
					color:'green',
					loading:false,
				})
		}
	})

}

  render()
  {
    // console.log(this.props.day_order)
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    return(
      <React.Fragment>
        <div className="row card" style={{marginLeft:'10px',marginRight:'10px'}}>
         <div className="card-title center" style={{background:'linear-gradient(to bottom, #33ccff 0%, #99ffcc 100%'}}>Slots You want to compensate</div>
         <div className="card-content row">
         <h5 className="center">{this.state.notfound}</h5>
         {this.state.compense_data.map((content,index)=>{
           return(
             <div className="col l4"  style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%'}}>
             <p className="center">You missed this slot on {content.date}-{month}-{year}</p>
             {this.state.color === 'red'?
             <div className="red-text">
             <TimeDiv day_order={this.props.day_order} usern={this.props.username} day={day} month={month} year={year}
              num={content.problem_compensation_hour} slots={content.slot} time={content.time}
              day_slot_time={content.day_sl}
              displayModal={this.state.modal}
              closeModal={this.selectModal}
              cday={content.date}
              color={() => this.color(content.problem_compensation_hour,day,month,year)}
              />
              </div>
              :
              <div className="card green-text"> COMPLETED</div>
           }
              </div>
           )
         })}
         </div>
        </div>

        <ul className="collapsible"  style={{marginLeft:'10px',marginRight:'10px'}}>
          <li>
            <div className="collapsible-header">Click Here to Enter Data, If Any Extra Slot Taken by You</div>
            <div className="collapsible-body">
            </div>
          </li>
       </ul>
      </React.Fragment>
    );
  }
}
