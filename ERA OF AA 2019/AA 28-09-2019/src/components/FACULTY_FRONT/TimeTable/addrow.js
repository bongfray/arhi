import React from "react";

export default class Table extends React.Component {
  state = {
    data: [
      1
    ]
  };
  appendChild = () => {
    let { data } = this.state;
    data.push(data.length);
    this.setState({data});
  };
  render() {
    return (
      <table>
        <thead className="row">
          <th className="col l4">Id of the Faculty</th>
          <th className="col l4">Slot Name</th>
          <th className="col l4">Slot Timing</th>
        </thead>
        <tbody>
          {this.state.data.map(id => (
            <Row id = {id} append={this.appendChild}/>
          ))}
        </tbody>
      </table>
    );
  }
}

const Row = ({ id,append }) => (
  <div>
  <tr className="row">
    <td className="col l4">
      <input type="text" id={`select-type-${id}`} placeholder="Enter Details" />
    </td>
    <td className="col l4">
      <input type="text" id={`select-position-${id}`} placeholder="Enter Details" />
    </td>
    <td className="col l4">
      <input type="text" id={`select-position-${id}`} placeholder="Enter Details" />
    </td>
  </tr><br />
  <a className="btn" onClick={append}>Add Row</a>
  </div>
);
