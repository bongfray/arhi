import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom';
import {} from 'materialize-css'
var empty = require('is-empty');

export default class Stop extends Component {
  render()
  {
    return(
      <React.Fragment>
        <div className="row" style={{marginTop:'50px'}}>
           <div className="col l3 s2 m3"/>
           <div className="col l6 s8 m6 card" style={{height:'200px'}}>
               <div className="card-title center blue-grey darken-2 white-text sup" style={{marginTop:'5px'}}>{this.props.section_name}</div>
               <div className="card-content">
               <div style={{paddingTop:'30px'}}><p className="center">REGISTRAION HAS BEEN CLOSED</p></div><br />
               <Link to={this.props.login_path} className="btn small col s2 l2 m2 right blue-grey darken-2 sup">LOGIN</Link>
               </div>
           </div>
           <div className="col l3 s8 m3"/>
        </div>
      </React.Fragment>
    );
  }
}
