import React, { Component } from 'react';
import axios from 'axios'
import M from 'materialize-css';
var empty = require('is-empty');


export default class Status extends Component {
	constructor() {
    super()
    this.state = {
      content:'PENDING',
      back:'red',
    }
		this.color = this.color.bind(this)
		this.componentDidMount = this.componentDidMount.bind(this)

    }

    color =() =>{


			Date.prototype.getWeek = function () {
					var onejan = new Date(this.getFullYear(), 0, 1);
					return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
			};

			var myDate = new Date.today();
			var week =myDate.getWeek();


    	axios.post('/user/fetchmaster', {
    		slot_time: this.props.slott,
				week: week,
    	})
    	.then(response =>{
    		if(response.data)
    		{
    				this.setState({
    					content:'COMPLETED',
              back:'green',
    				})
    		}
    	})

    }



    componentDidMount() {
        M.AutoInit();
        this.color();
    }


render() {
	return (
    <React.Fragment>
        <div className="card hoverable center go" style={{color:this.state.back}}>{this.state.content}</div>
    </React.Fragment>
	)
}
}
