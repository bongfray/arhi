import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import Slogo from './Slogo.png'
import M from 'materialize-css'

export default class Navbar extends Component {
  constructor()
  {
    super()
    this.state ={
      loggedIn:false,
      username:'',
      nav:[],
    }
    this.logout = this.logout.bind(this)
  }
  getUser = () =>{
    axios.get(this.props.get).then(response => {
      if (response.data.user) {
        this.setState({
          loggedIn: true,
          username: response.data.user.username,

        })
      } else {
        this.setState({
          loggedIn: false,
          username: null,

        })
      }
    })
  }


  fetch(){
    axios.get(this.props.nav_route).then(response =>{
      this.setState({
       nav: response.data,
     })
    })
  }

  componentDidMount = () => {
    M.AutoInit();
    this.getUser();
    this.fetch();
    let elems=document.querySelectorAll('.dropdown-button');
    M.Dropdown.init(elems,{inDuration : 300 ,outDuration :225, leftOrigin: true});
    M.Sidenav.init(elems, {
    inDuration: 350,
    outDuration: 350,
    edge: 'left'
    });
}


    logout(event) {
        event.preventDefault()
        axios.post(this.props.logout).then(response => {
          if (response.status === 200) {
            this.setState({
              loggedIn: false,
              username: null
            })
            window.location.assign(this.props.home);
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }
    render() {
 if(this.state.loggedIn === false)
{
  return(
    <div className="navbar-fixed">
  <nav>
    <div className="nav-wrapper blue-grey darken-2">
      <a href="#!" className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a>
      <div className="row">
      <div className="col s2 m2 xl2 l2" />
      <div className="col s8 m8 xl8 l8">
       <div className="row">
       <div className="col l1 xl1"/>
       <div className="col l11 xl11">
         <a href="http://care.srmuniv.ac.in" style={{paddingLeft:'50px'}} className="con nav-cen hide-on-med-and-down">SRM Centre for Applied Research in Education</a>
       </div>
      </div>
      </div>
      <div className="col s2 xl2 m2 l2">
      <ul className="right hide-on-large-only">
        <li><Link to='/slogin' className="right">LOGIN</Link></li>
      </ul>
      </div>
      </div>
    </div>
  </nav>
</div>
  );
}
else if(this.state.loggedIn === true)
{
        return (
          <div className="navbar-fixed">
                      <ul id="slide-out" className="sidenav">
                        <li><Link className="waves-effect" to={this.props.home}>Home</Link></li>
                        {this.state.nav.map((content,index)=>{
                          return(
                            <li key={content.val}><Link to={content.link} className="center">{content.val}</Link></li>
                          )
                        })}
                        <li><Link to="#" className="" onClick={this.logout}>Logout</Link></li>
                     </ul>
                     <nav>
                          <div className="nav-wrapper blue-grey darken-2">
                          <div className="row">
                            <div className="col l2 s2 xl2 m2">
                                 <a href="#" data-target="slide-out" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                                 <div className="brand-logo"><a href="http://srmuniv.ac.in"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a></div>
                            </div>
                            <div className="col l7 s7 xl7 m7">
                                 <div className="hide-on-med-and-down center con"><a href="http://care.srmist.edu.in">SRM CARE</a></div>
                            </div>
                            <div className="col l3 s3 xl3 m3">
                                <ul className="right">
                                  <li title="Home">
                                    <Link to={this.props.home}><i className=" hide-on-med-and-down material-icons">home</i></Link>
                                  </li>
                                  <li title="Log Out">
                                      <Link to="#" className="center" onClick={this.logout}>
                                      <i className="material-icons right">exit_to_app</i>
                                      </Link>
                                  </li>
                                  <li title="notification">
                                      <Link to="#">
                                      <i className="hide-on-med-and-down material-icons center">notifications</i>
                                      </Link>
                                  </li>
                                  <li title="menu" className="droppp">
                                    <i className="hide-on-med-and-down material-icons men" >more_vert</i>
                                    <ul >
                                      {this.state.nav.map((content,index)=>{
                                        return(
                                          <li key={content.val}><Link to={content.link}>{content.val}</Link></li>
                                        )
                                      })}
                                    </ul>
                                    </li>
                                </ul>
                            </div>
                      </div>
                          </div>
                      </nav>
                      </div>
        );
      }

    }
}
