const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const requested = new Schema({
  action: { type: String, unique: false, required: false },
  username:{ type: String, unique: false, required: false },
  day:{ type: Number, unique: false, required: false },
  month:{ type: Number, unique: false, required: false },
  year:{ type: Number, unique: false, required: false },
  req_reason:{ type: String, unique: false, required: false },
  day_order:{ type: Number, unique: false, required: false },
})



requested.plugin(autoIncrement.plugin, { model: 'Request', field: 'serial', startAt: 1,incrementBy: 1 });

const Request = mongoose.model('Request', requested)
module.exports = Request
