import React, { Component,Fragment } from 'react'
import axios from 'axios'
import { } from 'react-router-dom'
import ValidateUser from '../validateUser'


export default class ModifyUser extends Component{
  constructor(props){
      super(props);
      this.initialState={
        success:'',
        modal:false,
        disabled:'',
        allowed:false,
          done:'',
          cday:'',
          cmonth:'',
          cyear:'',
          hday:'',
          hmonth:'',
          hyear:'',
          replacable_day_order:'',
          sday: '',
          smonth: '',
          syear: '',
          fetched_start:'none',
          modify_start:'',
          fetched_end:'none',
          modify_end:'',
          eday: '',
          emonth: '',
          eyear: '',
      }
      this.state = this.initialState;
      this.componentDidMount = this.componentDidMount.bind(this)
      this.fetch_startDate = this.fetch_startDate.bind(this)
  }
  componentDidMount()
  {
    this.fetch_startDate();
    this.fetch_endDate();
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.action !== this.props.action) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.navigate();
    }
  }
  showDiv =(userObject) => {
    this.setState(userObject)
  }

  navigate =(e) =>{
    this.setState({modal: !this.state.modal,disabled:'disabled'})
    if(this.state.allowed === true)
    {
      if(this.props.select === "start-semester")
      {
        this.Start_Of_Semester();
      }
      else if(this.props.select === "end-semester")
      {
        this.End_Of_Semester();
      }
      else if(this.props.select === "modifytoday")
      {
        this.Reset_Today_DayOrder()
      }
      else if(this.props.select === "cancel_or_declare_today_as_a_holiday")
      {
        this.handleCancel_Holiday()
      }
      else if(this.props.select === "holiday")
      {
        this.Declare_Advance_Holiday()
      }
    }
  }


  handleChange = (e) =>{
    this.setState({
      [e.target.name]: e.target.value,
    })
  }
  /*------------------------------------Cancellation For Holiday & Also DayOrder Cancelled---------------- */
  handleCancel_Holiday = (e) =>{
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    if((this.state.cday === day) && (this.state.cmonth === month) && (this.state.cyear === year))
    {
    axios.post('/user/declare_cancel_or_holiday',this.state,
    this.setState({done:''})
  )
    .then(res=>{
      this.setState({done:res.data,disabled:'',
      success:'none'})
    })
    .catch(error =>{
      window.M.toast({html: 'Error!!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    })
  }
  else
  {
    this.setState({cday:'',cmonth:'',cyear:'',disabled:'',
    success:'none'})
    window.M.toast({html: 'Invalid Input !!',outDuration:'1000', classes:'rounded #f44336 red'});
  }
  }
  /*-------------------------End of Cancel or Holiday Making----------- */

/*-------------------------------------------------Start on Operation of Start Of Sem-------------------------------- */
  fetch_startDate = () =>{

    axios.get('/user/fetch_start_date')
    .then(res=>{
      if(res.data)
      {
        this.setState({fetched_start_date:res.data,modify_start:'none',fetched_start:'block',sday:res.data.startday,smonth:res.data.startmonth,syear:res.data.startyear})
      }
      else{
        this.setState({modify_start:'block',fetched_start:'none'})
      }
    })
  }

  changeStart_Of_sem =() =>{
    this.setState({fetched_start:'none',modify_start:'block'})
  }

  deleteStart_Of_Sem =() =>{
    axios.post('/user/delete_start_of_sem')
    .then(res=>{
      if(res.data)
      {
        this.setState({modify_start:'block',fetched_start:'none',sday:'',smonth:'',syear:''});
        window.M.toast({html: res.data,outDuration:'1000', classes:'rounded pink'});
      }
    })
  }

Start_Of_Semester =(e) =>{
  if((this.state.sday) && (this.state.syear) && (this.state.smonth))
  {
    let intM = parseInt(this.state.smonth);
    axios.post('/user/handle_start_of_semester',{order:'Start_Of_Semester',startday:this.state.sday,startmonth:intM,startyear:this.state.syear})
    .then(res=>{
      if(res.data)
      {
        this.setState({disabled:'',
        success:'none',fetched_start:'block',modify_start:'none',sday:this.state.sday,smonth:this.state.smonth,syear:this.state.syear})
        window.M.toast({html: res.data,outDuration:'1000', classes:'rounded pink'});
      }
    })
  }
  else
  {
    window.M.toast({html: 'Enter all the Details First !!',outDuration:'1000', classes:'rounded pink'});
  }
}
/*----------------End Start of Sem-------------- */


/*----------------------------------------------------------------End Sem Handle----------------------------- */
fetch_endDate = () =>{

  axios.get('/user/fetch_end_date')
  .then(res=>{
    if(res.data)
    {
      this.setState({modify_end:'none',fetched_end:'block',eday:res.data.endday,emonth:res.data.endmonth,eyear:res.data.endyear})
    }
    else{
      this.setState({modify_end:'block',fetched_end:'none'})
    }
  })
}

changeEnd_Of_sem =() =>{
  this.setState({fetched_end:'none',modify_end:'block'})
}

deleteEnd_Of_Sem =() =>{
  axios.post('/user/delete_end_of_sem')
  .then(res=>{
    if(res.data)
    {
      this.setState({modify_end:'block',fetched_end:'none',eday:'',emonth:'',eyear:''});
      window.M.toast({html: res.data,outDuration:'1000', classes:'rounded pink'});
    }
  })
}

End_Of_Semester =(e) =>{
if((this.state.eday) && (this.state.eyear) && (this.state.emonth))
{
  let intN = parseInt(this.state.emonth);
  axios.post('/user/handle_start_of_semester',{order:'End_Of_Semester',endday:this.state.eday,endmonth:intN,endyear:this.state.eyear})
  .then(res=>{
    if(res.data)
    {
      this.setState({disabled:'',
      success:'none',fetched_end:'block',modify_end:'none',eday:this.state.eday,emonth:this.state.emonth,eyear:this.state.eyear})
      window.M.toast({html: res.data,outDuration:'1000', classes:'rounded pink'});
    }
  })
}
else
{
  window.M.toast({html: 'Enter all the Details First !!',outDuration:'1000', classes:'rounded pink'});
}
}





/*End of it------------------------- */


/*------------------------------------------------Reset Today's DayOrder---------------------------- */
Reset_Today_DayOrder = (e) =>{
  if(this.state.replacable_day_order)
  {
    axios.post('/user/handle_change_today_dayorder',{day_order:this.state.replacable_day_order})
    .then(res=>{
      if(res.data)
      {
        this.setState({replacable_day_order:'',disabled:'',
        success:'none'})
        window.M.toast({html: res.data,outDuration:'1000', classes:'rounded pink'});
      }
    })
  }
  else
  {
    window.M.toast({html: 'Enter all the Details First !!',outDuration:'1000', classes:'rounded pink'});
  }
}

Declare_Advance_Holiday = (e) =>{

}

  render(){
    let content;
      if(this.props.select === 'start-semester'){

          content =
          <React.Fragment>
                    <div className="row" style={{display:this.state.fetched_start}}>
            <div className="col l4">Start of the Semester Date is :-</div>
            <div className="col l4" onClick={this.changeStart_Of_sem}>{this.state.sday} / {this.state.smonth} /{this.state.syear}(dd/mm/yyyy) </div>
            <div className="col l4">
            <i className="material-icons go" onClick={this.changeStart_Of_sem}>edit</i>
                  &nbsp;<i className="material-icons go" onClick={this.deleteStart_Of_Sem}>delete</i>
            </div>
          </div>
              <div className="center" style={{display: this.state.modify_start}}>
                  <div className="input-field col s4">
              <input id="sday" type="text" name="sday" className="validate" value={this.state.sday} onChange={this.handleChange} required />
              <label htmlFor="sday">Day (dd)</label>
              </div>
                              <div className="input-field col s4">
              <input id="smonth" type="text" name="smonth" className="validate" value={this.state.smonth} onChange={this.handleChange} required />
              <label htmlFor="smonth">Month (mm)</label>
              </div>
                              <div className="input-field col s4">
              <input id="syear" type="text" name="syear" className="validate" value={this.state.syear} onChange={this.handleChange} required />
              <label htmlFor="syear">Year (yyyy)</label>
              </div>
              <div className="row">
                <div className="col l6 center" />
                <div className="col l6">
                <button className="waves-effect btn #37474f blue-grey darken-3 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>Start Semester</button>
                </div>
              </div>
              </div>
              </React.Fragment>
      }
      else if(this.props.select === 'end-semester'){
          content =
          <React.Fragment>
                    <div className="row" style={{display:this.state.fetched_end}}>
            <div className="col l4">End of the Semester Date is :-</div>
            <div className="col l4" onClick={this.changeEnd_Of_sem}>{this.state.eday} / {this.state.emonth} /{this.state.eyear}(dd/mm/yyyy) </div>
            <div className="col l4">
            <i className="material-icons go" onClick={this.changeEnd_Of_sem}>edit</i>
                  &nbsp;<i className="material-icons go" onClick={this.deleteEnd_Of_Sem}>delete</i>
            </div>
          </div>
              <div className="center" style={{display: this.state.modify_end}}>
                  <div className="input-field col s4">
              <input id="eday" type="text" name="eday" className="validate" value={this.state.eday} onChange={this.handleChange} required />
              <label htmlFor="eday">Day (dd)</label>
              </div>
                              <div className="input-field col s4">
              <input id="emonth" type="text" name="emonth" className="validate" value={this.state.emonth} onChange={this.handleChange} required />
              <label htmlFor="emonth">Month (mm)</label>
              </div>
                              <div className="input-field col s4">
              <input id="eyear" type="text" name="eyear" className="validate" value={this.state.eyear} onChange={this.handleChange} required />
              <label htmlFor="eyear">Year (yyyy)</label>
              </div>
              <div className="row">
                <div className="col l6 center" />
                <div className="col l6">
                <button className="waves-effect btn #37474f blue-grey darken-3 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>End Semester</button>
                </div>
              </div>
              </div>
              </React.Fragment>

          
      }
      else if(this.props.select === 'modifytoday'){
          content =
              <div className="center">

              <div className="input-field col s12">
              <input id="dayorder" type="text" name="replacable_day_order" className="validate" value={this.state.replacable_day_order} onChange={this.handleChange} required />
              <label htmlFor="dayorder">Reset Today's Day Order to:</label>
              </div>
              <div className="row">
                 <div className="col l6 center" />
                 <div className="col l6">
                    <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>Reset Day Order</button>
                 </div>
              </div>
              </div>

          
      }
      else if(this.props.select === 'cancel_or_declare_today_as_a_holiday'){
          content =
            <div className="form-signup">
                <div className="row">
                <div className="input-field col s4">
                <input id="cday" type="Number" name="cday" className="validate" value={this.state.cday} onChange={this.handleChange} required />
                <label htmlFor="cday">Day (dd)</label>
                </div>
                <div className="input-field col s4">
                <input id="cmonth" type="Number" name="cmonth" className="validate" value={this.state.cmonth} onChange={this.handleChange} required />
                <label htmlFor="cmonth">Month (mm)</label>
                </div>
                <div className="input-field col s4">
                <input id="cyear" type="Number" name="cyear" className="validate" value={this.state.cyear} onChange={this.handleChange} required />
                <label htmlFor="cyear">Year (yyyy)</label>
                </div>
                <div className="col l3">{this.state.done}</div>
                <button className="waves-effect right btn #37474f blue-grey darken-3 col s3 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>SUBMIT</button>
                </div>
          </div>
          
      }
      else if(this.props.select === 'holiday'){
          content =
            <div className="form-signup">
                <div className="row">
                <div className="input-field col s4">
                <input id="hday" type="Number" name="hday" className="validate" value={this.state.hday} onChange={this.handleChange} required />
                <label htmlFor="hday">Day (dd)</label>
                </div>
                <div className="input-field col s4">
                <input id="hmonth" type="Number" name="hmonth" className="validate" value={this.state.hmonth} onChange={this.handleChange} required />
                <label htmlFor="hmonth">Month (mm)</label>
                </div>
                <div className="input-field col s4">
                <input id="hyear" type="Number" name="hyear" className="validate" value={this.state.hyear} onChange={this.handleChange} required />
                <label htmlFor="hyear">Year (yyyy)</label>
                </div>
                <div className="col l3">{this.state.done}</div>
                <button className="waves-effect right btn #37474f blue-grey darken-3 col s3 offset-s3" disabled={this.state.disabled} onClick={this.navigate}>SUBMIT</button>
                </div>
          </div>
      }
      else{
          content=
              <div></div>
      }
      return(
        <React.Fragment>
        <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
        {content}
        </React.Fragment>
        
      )
  }
}
