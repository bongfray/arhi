import React from 'react';
import axios from 'axios';
import ValidateUser from '../validateUser'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.action !== this.props.action) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({modal: !this.state.modal,disabled:'disabled',id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
  if(index === "faculty")
  {
    delroute = "/user/delnav";
  }
  else if(index === "student")
 {
  delroute = "/user2/delnav";
 }
 else if(index === "Section"){
  delroute = "/user/del_section";
 }
const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:'',
        success:'none',
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
  if(action === "faculty")
  {
    fetchroute = "/user/fetchnav";
  }
  else if(action === "student")
  {
    fetchroute = "/user2/fetch_student_nav"
  }
  else if(action === "Section"){
    fetchroute = "/user/fetch_section";
  }

  axios.post(fetchroute,{
    action: this.props.action,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { error, products} = this.state;
      return(
        <div>
          <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
            <div className="row">
                <div className="col s3 l3 m3 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className={content.div} key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s3 l3 m3 center"><b>Action</b></div>
            </div>
            <div>
              {products.map((product,index) => (
                <div className="row" key={product.serial}>
                  <div className="col l3 m3 s3 center">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className={content.div} key={index}>{product[content.name]}</div>
                  ))}
                    <div className="center col s3 m3 l3 hide-on-small-only"><button className="btn-small green" onClick={() => this.props.editProduct(product.serial,this.props.action)}>Edit</button>
                    &nbsp;<button className="btn-small red" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </div>
                  <div className="col s3 hide-on-med-and-up">
                  <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                  &nbsp;<i className="material-icons go" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                </div>
              ))}
            </div>
        </div>
      )

  }
}
