import React, { Component } from 'react'
import { } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Al2'
import Free from './Free'
import Cancel from './Cancel'


export default class Modal2 extends Component{
constructor(props){
  super(props);
  this.state = {
    rit:'',
    day_order:'',
    modal: false,
    allo1:false,
    loading: false,
    no1:'',

  };
  this.closeModal = this.closeModal.bind(this);
  this.componentDidMount = this.componentDidMount.bind(this);
}
handle1 = (e, index) => {
  // alert(e.target.value);
  // alert(e.target.name);
  const key = e.target.name;
  const value = e.target.value;
  this.setState({ rit: value, index ,day_order: key, index});

};
componentDidMount(){
  M.AutoInit();
  this.collectw();
}


collectw =() =>
{
	axios.post('/user/fetchme', {
		slot_time: this.props.day_order+this.props.day_slot_time,
	},
  this.setState({
    loading: true,
  }))
	.then(response =>{
		if(response.data)
		{
				this.setState({
					allo1:true,
          loading: false,
				})
		}
	})
}

    closeModal(e) {
        e.stopPropagation()
        this.props.closeModal()
     }
     render(){
       let loading = this.state.loading;
       let allotw = this.state.allo1;
       const divStyle = {
            display: this.props.displayModal ? 'block' : 'none',
       };
       let content;

           if(allotw)
           {
             content =
                                          <React.Fragment>
                                                <option defaultValue="" disabled selected>Choose</option>
                                                <option value="allot">Alloted Work/Class</option>
                                          </React.Fragment>

           }
           else{
             content =

                    <React.Fragment>
                               <option defaultValue="" disabled selected>Choose</option>
                               <option value="free">Free Slot</option>
                               <option value="slot_aborted">Any Problem ?</option>
                    </React.Fragment>

           }
     return (
       <React.Fragment>

       <div className="modal mobmodal" style={divStyle}>
          <div className="modal-content">
                 <a className="float_cancel btn-floating btn-small waves-effect black  right" onClick={ this.closeModal }><i className="material-icons">cancel</i></a>
             <br />
                  <div class="mymodal">
                    <div className="root-of-time" >
                    <div className="row">
                         <div className="col l12">
 
                         <div>
                               <select name={this.state.day_order} onChange={e => this.handle1(e, this.state.day_order)}>
                               {content}
                               </select>
                         </div>


                           <span className="drop">
                             <TableDisplay color={this.props.color}
                             day ={this.props.day}
                             month ={this.props.month}
                             year ={this.props.year}
                             selectValue={this.state.rit} day_slot_time={this.props.day_slot_time} day_order={this.props.day_order} usern={this.props.usern} closeModal={this.props.closeModal}
                             />
                           </span>
                         </div>


                         </div>

        </div>
                  </div>
          </div>
       </div>
</React.Fragment>
     );
   }
}



     class TableDisplay extends Component {
       constructor() {
         super()
         this.state = {
           allot:'',
             count: 0,
             selected:'',
             problem_conduct:'',
             saved_dayorder: '',
             day_recent:'',

         }
         }

       render() {
                     if (this.props.selectValue === "allot") {
                       return(
                         <div className="">

                         <Allot
                         day ={this.props.day}
                         month ={this.props.month}
                         year={this.props.year}
                          color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                         </div>
                       );

                     } else if (this.props.selectValue === "free") {
                       return(
                         <Free
                         day ={this.props.day}
                         month ={this.props.month}
                         year={this.props.year}
                         color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                       );

                     }
                     else if (this.props.selectValue === "slot_aborted") {
                      return(
                        <Cancel
                        day ={this.props.day}
                        month ={this.props.month}
                        year={this.props.year}
                        color={this.props.color} usern={this.props.usern } day_order={this.props.day_order} closeModal={this.props.closeModal} day_slot_time ={this.props.day_slot_time}/>
                      );

                    }
                     else{
                       return(
                         <div className="inter-drop center">
                           Choose from above DropDown
                         </div>
                       );
                     }
       }
     }
