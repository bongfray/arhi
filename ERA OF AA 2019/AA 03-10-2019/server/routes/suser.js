const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express')
const route = express.Router()
const bcrypt = require('bcryptjs');
const Suser = require('../database/models/STUD/suser')
const passport = require('../passport/stud_pass')
const NavS = require('../database/models/STUD/navstud')
const AdminOrder = require('../database/models/FAC/admin_instruction')
const Resume = require('../database/models/STUD/resume_schema')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')

console.log("Hi Student Here")





route.get('/fetch_srender_status', function(req, res) {
  AdminOrder.findOne({usertype:'stud'}, function(err, result){
    if(err)
    {

    }
    else if(result)
    {
      res.send(result.registration_status)
    }
  })
})



route.post('/ssignup', (req, res) => {
console.log("Stud REG");
    const { regid, password,name,mailid,phone,campus,dept,dob,count,degree,st_year,fn_year} = req.body
    Suser.findOne({ username: regid }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          console.log("Way to signup")
            const newUser = new Suser({
              username: regid,
              password: password,
              name: name,
              mailid: mailid,
              phone: phone,
              campus: campus,
              dept: dept,
              dob: dob,
              degree: degree,
              st_year: st_year,
              fn_year: fn_year,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              count: count,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})


route.post(
    '/slogin',(req, res, next) => {

console.log("In Student dB");
      const { username, password} = req.body;


      Suser.findOne({ username: username }, function(err, objs){
        if(err)
        {
          return;
        }
        else if(objs)
        {
         if (objs.count === "0")
          {
              var counter = 0;
              Suser.updateOne({ username: username }, {count: counter+1},(err, user) => {

              })

          }
          else if (objs.count === "1")
          {
              var coun = 0;
              Suser.updateOne({ username: username }, {count: coun+2},(err, user) => {

              })
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)


route.get('/getstudent', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})



route.post('/slogout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})





route.post('/sprofile', (req, res) => {

    const {id,up_username,up_phone,up_name,up_dob} = req.body;
    // var password = req.body.new_password;
    Suser.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          // let exist_password = req.body.current_password;
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            // console.log("Successful Authen");
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            // console.log(hash);
            Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              // console.log("Successful Hash");
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          Suser.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)



route.get('/getstudentprofile', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    Suser.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});




    route.post('/sforgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       Suser.findOne(
         {mailid: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'Invalid User'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           console.log(token);
           Suser.updateOne({mailid:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 900000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Reset Password:  eWorks', // Subject line
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 15min</p>'
          };
          // console.log("Way to enter.......");
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })

        })



        route.post('/addnav', function(req,res) {
                  var trans = new NavS(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });


/*-------------------------------------------------------------Fetch Request From Nav Bar---------------------- */
        route.get('/fetch_snav',function(req,res) {
          NavS.find({ }, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
        });
        })



/*-------------------------------------------------------------Fetch Request From Admin Panel---------------------- */
        route.post('/fetch_student_nav',function(req,res) {
          NavS.find({action: req.body.action}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
        });
        })


        route.post('/editnav', function(req,res) {
          NavS.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
            var succ= {
              succ: "Updated"
            };
            res.send(succ);
          })

        });

        route.post('/edit_existing_nav',function(req,res) {
          NavS.findOne({serial: req.body.id}, function(err, docs){
            // console.log(docs)
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
        });
        })



        route.post('/delnav',function(req,res) {
          NavS.remove({serial:  req.body.serial },function(err,succ){
            res.send("OK")
          });
        })




        route.post('/handleStudent_Registration',function(req,res) {

        AdminOrder.findOne({usertype: req.body.history}, function(err, docs){
          if(err)
          {

          }
          else if(docs)
          {
            AdminOrder.updateOne({usertype:req.body.history},{registration_status: req.body.checked},function(err,done){
              res.send("Done")
            })
          }
          else{
            var trans = new AdminOrder({
              usertype:req.body.history,
              registration_status: req.body.checked
            }
            );
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
          }
        })
        })



        route.get('/stud_reg_status', (req, res, next) => {
          AdminOrder.findOne({usertype:'stud'},function(err,result){
            if(err)
            {

            }
            else if(result)
            {
              res.send(result.registration_status)
            }
          })
        })



        /*----------------------------------------RESUME CODE0--------------------------------------------------------- */
        route.post('/addmm', function(req,res) {
                  var trans = new Resume(req.body.data);
                  trans.save((err, savedUser) => {
                    var succ= {
                      succ: "Done"
                    };
                    res.send(succ);
                  })
        });

          route.post('/editt', function(req,res) {
            Resume.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })

          });


      route.post('/fetchall',function(req,res) {
        console.log(req.body)
          Resume.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
            if(err)
            {

            }
            else
            {
              res.send(docs)
            }
        });
      })

      route.post('/fetcheditdata',function(req,res) {
        Resume.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
       });
      })

      route.post('/del',function(req,res) {
        Resume.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
          res.send("OK")
        });
      });



      route.post('/deletesuser',function(req,res){
        Suser.findOne({username:req.body.username},function(err,found){
          if(err)
          {
      
          }
          else if(found)
          {
            Suser.deleteOne({username:found.username},function(err,done)
            {
            })
            Resume.deleteMany({username:found.username},function(err,done)
            {
            })
            res.send("Deleted")
          }
        })
      })

module.exports = route
