import React from 'react'
import { Link } from 'react-router-dom'
const Insta = props =>{
    const divStyle =
    {
      display: props.displayModal ? 'block' : 'none',
      marginTop:'170px',
    };
     return(
       <div
         className="modal instructionmodal"
         style={divStyle}>
         <div className="modal-content">
           <h4 className="center">eWork</h4><br /><br />
           <div className="mcont row"  style={{marginBottom:'35px'}}>
             <Link to="/flogin" className="col s5 waves-effect btn" style={{ background: 'linear-gradient(to bottom left, #ff0066 0%, #00ff99 100%)'}}>Faculty</Link>
             <div className="col s2"></div>
             <Link to="/slogin" className="col s5 waves-effect btn black-text" style={{background: 'linear-gradient(to bottom left, #66ffff 0%, #ffff99 100%)'}}>Student</Link>
           </div>
         </div>
       </div>
     );
}
export default Insta;
