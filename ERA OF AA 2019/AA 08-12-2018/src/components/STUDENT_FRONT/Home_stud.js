import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import {Link } from 'react-router-dom'
import './css_home.css'
import Nav from '../dynnav'
import M from 'materialize-css'

class FrontPage extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/user2/slogout',
      get:'/user2/getstudent',
      home:'/student',
      nav_route:'/user2/fetch_snav',
      theme:true,
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/user2/getstudent').then(response => {
      if (response.data.user) {
          this.setState({display:'disabled'})
      }
        })
      }


      changeTheme = () => {
  this.setState({
    theme : !this.state.theme
  });
}


    componentDidMount() {
        this.getUser();
        M.AutoInit();
        let elems = document.querySelectorAll(".modal-trigger");
        M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });

    }
    render(){
        return(
          <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div className={this.state.theme ? "normal" : "night"} >
          <div className="wrapper bdy">
        <div className="floor">
          <button onClick={this.changeTheme} className="btn">{this.state.theme ? "Night Mode" : "Normal Mode"}</button>
        </div>
        <div className="shelf-wrapper">
            <div className="shelf1">
                <div id="slantbook"></div>
                <div id="flatbook"></div>

            </div>
            <div className="shelf2">
                <div id="flatbook3"></div>
                <div className="penstand">
                    <div id="pen"></div>
                </div>

            </div>
            <div className="shelfknob">
                <div className="knob" id="knob1"></div>
                <div className="knob" id="knob2"></div>

            </div>

        </div>
        <div className="window">
            {this.state.theme=== false ? <span id="moon"></span> :
            <div id="sun"></div>}
        </div><br />
        <Link to="/slogin">
     <div className="right directions">
        <div id="Awesome" className="anim750">
  <div className="sticky anim750">
    <div className="front circle_wrapper anim750">
      <div className="circle anim750"></div>
    </div>
  </div>

  <h6>Login & SignUP</h6>

  <div className="sticky anim750">
    <div className="back circle_wrapper anim750">
      <div className="circle anim750"></div>
    </div>
  </div>
</div>
</div>
</Link>

        <div className="container">
            <div className="cup"></div>
            <div className="table">
                <span className="handle" id="handle1"></span>
                <span className="handle" id="handle2"></span>
                <div id="margintop">
                    <div className="tableleg" id="table0"></div>

                </div>
            </div>
            {this.state.theme === false && <div className="lamp">
                <div id="lampjoint"></div>
                <div className="lamphead"></div>
            </div>}

                <div style={{marginTop:'132px'}}>
                    <div class = "screen" >
                     <div className="name_e center">eWork</div>
                    
                    </div>
                 <div class = "keyboard">
                    <div class = "keyboard2">
                    </div>
                 </div>
               </div>



            <div className="semicircle"></div>
        </div>

    </div>
    </div>
            </React.Fragment>
        );
    }
}

export default FrontPage;
