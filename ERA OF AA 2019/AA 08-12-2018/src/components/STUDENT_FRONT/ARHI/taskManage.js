import React, { Component } from 'react';
import axios from 'axios'
import Nav from '../../dynnav'

export default class TaskManage extends Component {
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/user2/slogout',
      get:'/user2/getstudent',
      home:'/student',
      nav_route:'/user2/fetch_snav',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  getUser=()=> {
    axios.get('/user2/getstudent').then(response=>{
      if (response.data.user){
          this.setState({display:'disabled'})
      }
        })
      }
  componentDidMount()
  {
   this.getUser();
  }
  render(){
    return(
      <React.Fragment>
        <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
        Task
      </React.Fragment>
    );
  }
}
