import React, { Component } from 'react';
import { Redirect} from 'react-router-dom'
import NaVM from './nav-modal'
export default class Nstart extends Component {
  constructor()
  {
    super()
    this.state = {
      redirectTo:'',
      modal: false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  openModal =() =>
  {
      this.setState({modal: !this.state.modal})
  } 
  componentDidMount()
  {
    this.openModal();
  }
  render() {
    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
    return (
       <div className="Navigator">
       <NaVM
           displayModal={this.state.modal}
           closeModal={this.openModal}
       />
       </div>
    );
  }
  }
}
