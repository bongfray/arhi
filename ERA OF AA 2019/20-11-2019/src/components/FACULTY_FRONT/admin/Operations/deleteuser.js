import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import ValidateUser from '../validateUser'

export default class DeleteUser extends Component{
    constructor(){
        super();
        this.state={
          result:'',
          modal:false,
          allowed:false,
          disabled:'',
          username: '',
          success:'',
        }
    }
    handleDelete = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    componentDidUpdate =(prevProps,prevState) => {
      if (prevProps.action !== this.props.action) {
        this.fetch(this.props.action);
      }
      if (prevState.allowed !== this.state.allowed) {
        this.delete()
      }
    }

    showDiv =(userObject) => {
      this.setState(userObject)
    }

    delete = (e)=>{
      if(!this.state.username)
      {
        window.M.toast({html: 'Username Cannot be empty!!',classes:'rounded blue-grey darken-3'});
      }
      else{
      this.setState({modal: !this.state.modal,disabled:'disabled'})
      if(this.state.allowed === true)
      {
        if(this.props.select === "deleteadmin")
        {
          this.deleteAdmin();
        }
        else if(this.props.select === "deletefac")
        {
          this.deleteUser();
        }
        else if(this.props.select === "deletestu")
        {
          this.deleteSUser()
        }
      }
    }
    }
    deleteUser = (e) =>{
      axios.post('/user/deleteuser',{username:this.state.username})
      .then(res=>{
        this.setState({username:'',disabled:'',
        success:'none',})
        window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }

    deleteAdmin = (e) =>{
      axios.post('/user/deleteadmin',{username:this.state.username})
      .then(res=>{
        this.setState({username:'',disabled:'',
        success:'none',})
        window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }

    deleteSUser = (e) =>{
      axios.post('/user2/deletesuser',{username:this.state.username})
      .then(res=>{
        this.setState({username:'',disabled:'',
        success:'none',})
        window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }

    render(){
      let deleteoperation;
        if(this.props.select === 'deleteadmin'){
          deleteoperation =	<button className="waves-effect btn #37474f blue-grey darken-3" disabled={this.state.disabled} onClick={this.delete}>Delete Admin</button>
        }
        else if(this.props.select === 'deletefac'){
          deleteoperation = <button className="waves-effect btn #37474f blue-grey darken-3" disabled={this.state.disabled} onClick={this.delete}>Delete Faculty</button>
        }
        else if(this.props.select === 'deletestu'){
           deleteoperation =   <button className="waves-effect btn #37474f blue-grey darken-3" disabled={this.state.disabled} onClick={this.delete}>Delete Student</button>
        }
        else{
            return(
                <div></div>
            )
        }
        return(
            <div className="">
            <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
            <div className="input-field col s12">
              <input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleDelete} required />
              <label htmlFor="username">Enter Username to Delete</label>
            </div>
            <div className="row">
              <div className="col l3 xl3 hide-on-mid-and-down" />
              <div className="col l5 s6 m6 xl5" />
              <div className="col l4 s6 m6 xl4 right">
                 {deleteoperation}
              </div>
            </div>

            </div>

            )
    }

}
