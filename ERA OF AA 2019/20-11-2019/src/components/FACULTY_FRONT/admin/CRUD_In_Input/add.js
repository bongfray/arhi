
import React from 'react';

class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      val:'',
      link:'',
      action:'',
      usertype:'',
      username:'',
      section_data_name:'',
      responsibilty_root:'',
      responsibilty_root_percentage:'',
      responsibilty_title:'',
      responsibilty_percentage:'',
      designation_name:'',
      designation_order_no:'',
      department_name:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


  componentDidMount(){
    this.setState({
      action:this.props.action,
      usertype:this.props.level,
      inserted_by:this.props.username,
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
    <React.Fragment>
      <div className="row">
        <div className="col s12 l12" >
          {this.props.data.fielddata.map((content,index)=>(
            <div className="col l6 s6 m6" key={index}>
            <div className="input-field">
              <input
                id={content.name}
                className=""
                type={content.type}
                placeholder=" "
                min="10"
                max="60"
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              />
              <label htmlFor={content.name}>{content.placeholder}</label>
            </div>
            </div>
          ))}
        </div>
        </div>
        <div className="row">
          <div className="col l9 xl9 hide-on-mid-and-down" />
          <div className="col l3 xl3 s12">
            <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
            <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
          </div>
        </div>

      </React.Fragment>
    )
  }
}

export default AddProduct;
