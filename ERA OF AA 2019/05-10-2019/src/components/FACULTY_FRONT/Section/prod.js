import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetchall',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { error, products} = this.state;
      return(
        <div>
        <div className="row">
          <div className="col s12 l12 xl12 m12 td">
            <h5 className="proftitle">
            {this.props.description}
            </h5>
          </div>
        </div>
        <br />
          <table>
            <thead className="col s12 l12 m12 xl12">
            <tr>
            <th className="col s1 l1 xl1 m1">Serial No</th>
            {this.props.data.fielddata.map((content,index)=>(
              <th className="col s2 l2 xl2 m2 center" key={index}>{content.header}</th>
            ))}
            <th className="col s1 l1 xl1 m1 center"> Action</th>
            </tr>
            </thead>
            <tbody>
              {products.map((product,index) => (
                <tr key={product.serial}>
                  <td className="center">{index+1}</td>
                  {this.props.data.fielddata.map((content,index)=>(
                    <td className="center" key={index}>{product[content.name]}</td>
                  ))}
                    <td className="center hide-on-small-only"><button className="btn-small green" onClick={() => this.props.editProduct(product.serial,this.props.action)}>Edit</button>
                    &nbsp;<button className="btn-small red" onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </td>
                  <td className="center hide-on-med-and-up">
                  <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                  &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )

  }
}
