import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Select} from 'react-materialize'
import M from 'materialize-css'

export default class FreeS extends React.Component {
  constructor()
  {
    super()
    this.state ={
      administrativefreefield:'',
      hfreefield:'',
      researchfreefield:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

componentDidMount()
{
  let selects = document.querySelectorAll('select');
  M.FormSelect.init(selects, {});
}

handleHFreeField =(e) =>{
  this.setState({
    hfreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}

render()
{
  if(this.props.freefield ==="Academic")
  {
    return(
      <React.Fragment >
      <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
      <option value="" disabled selected>Pick Up the Role</option>
      <option value="academic_total_cocurricular">CoCurricular</option>
      <option value="academic_total_extracurricular">ExtraCurricular</option>
      <option value="academic_total_evaluation_placementwork">Evaluation</option>
      <option value="academic_total_evaluation_placementwork">Placement Related Work</option>
    </Select>
      </React.Fragment>
    );
  }
  else if(this.props.freefield ==="Administrative_Work")
  {
    return(
      <React.Fragment>
        <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
          <option value="" disabled selected>Pick Up the Role</option>
          <option value="ad_total_role_res" >Roles & Responsibilities</option>
          <option value="ad_total_clerical">Clerical Work</option>
          <option value="ad_total_planning">Planning / Strategy</option>
          <option value="other">Other</option>
        </Select>
      </React.Fragment>

    );
  }
  else if(this.props.freefield ==="Research_Work")
  {
    return(
      <React.Fragment>
        <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
          <option value="" disabled selected>Pick Up the Role</option>
          <option value="research_total_publication">Publications</option>
          <option value="research_total_ipr_patents">IPR & Parents</option>
          <option value="research_total_funded_sponsored_projectes">Funded / Sponsered Project</option>
          <option value="research_total_tech_dev_consultancy" >Technology Devt. & Consultancy</option>
          <option value="research_total_product_development">Product Development</option>
          <option value="research_total_research_center_establish">Research Center Establishment</option>
          <option value="research_total_research_guidnce">Research Guidance</option>
        </Select>
      </React.Fragment>

    );
  }
  else{
    return(
      <React.Fragment>
      </React.Fragment>
    )
  }

}
}
