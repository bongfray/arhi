## EWork




# About
It's About Official Toll

## Built With

Mongo, Express, ReactJS, Node.js, Javascript.


### Prerequisites

- [Node.js](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)
- [Nodemon](https://github.com/remy/nodemon)
- [create-react-app](https://github.com/facebook/create-react-app)



### Next Steps
- [ ] Add redux
- [ ] Add SCSS

## Author
ArHi
