const passport = require('passport')
const LocalStrategy = require('./localStrategy')
const User = require('../database/models/FAC/user')

passport.serializeUser((user, done) => {
	done(null, { _id: user._id,count: user.count })
})


passport.deserializeUser((id, done) => {
	console.log('DeserializeUser called')
	User.findOne(
		{ _id: id },
		['username','count'],
		(err, user) => {
			if(err)
			{
				done(err,false)
			}
			else{
				console.log(user)
			done(null, user)
			}
		}
	)
})

passport.use(LocalStrategy)

module.exports = passport
