
import React from 'react';

class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      val:'',
      link:'',
      action:'',
      usertype:'',
      section_data_name:'',
      responsibilty_root:'',
      responsibilty_root_percentage:'',
      responsibilty_title:'',
      responsibilty_percentage:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


  componentDidMount(){
    this.setState({
      action:this.props.action,
      usertype:this.props.level,
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    return(
    <React.Fragment>
      <div className="row">
        <div className="col s12 l12" >
          <form onSubmit={this.handleSubmit}>
          {this.props.data.fielddata.map((content,index)=>(
            <div className="col l6 s6 m6" key={index}>
            <div className="input-field">
              <input
                id={content.name}
                className=""
                type={content.type}
                placeholder=" "
                min="10"
                max="60"
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              />
              <label htmlFor={content.name}>{content.placeholder}</label>
            </div>
            </div>
          ))}
            <div>
              <button className="btn right blue-grey darken-2 sup " type="submit">UPLOAD</button>
            </div>
          </form>
        </div>
      </div>
      </React.Fragment>
    )
  }
}

export default AddProduct;
