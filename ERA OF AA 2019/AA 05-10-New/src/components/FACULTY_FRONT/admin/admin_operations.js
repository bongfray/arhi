import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Nav from '../../dynnav'

import Display from './Operations/display'

export default class Admin extends Component{
constructor(){
    super();
    this.state ={
        radio:[{name:'radio1',color:'blue',value:'Insert'},{name:'radio2',color:'black',value:'Update'},{name:'radio3',color:'orange',value:'Super'},{name:'radio4',color:'red',value:'Delete'},{name:'radio5',color:'yellow',value:'Modify'},{name:'radio6',color:'aqua',value:'Suspend'}],
        isChecked: false,
        choosed: '',
        home:'/admin_panel',
        logout:'/user/logout',
        get:'/user/',
        nav_route: '/user/fetchadmin_nav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
}
componentDidMount()
{
    this.fetchlogin();
    this.fetchUser();
}
fetchUser =() =>{
  axios.get('/user/active')
  .then(res=>{
    console.log(res.data)
  })
}
fetchlogin = () =>{
    axios.get('/user/fetchdayorder'
  )
    .then(response =>{
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }
handleChecked =(e,index,color)=>{
  console.log(color)
    this.setState({
      color:color,
        isChecked: !this.state.isChecked,
        choosed: e.target.value
    });
}
render()
{
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
    return(
        <React.Fragment>
       <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
       <div className="row">
            {this.state.radio.map((content,index)=>(
                <div className="col s6 l2 m2" key={index}>
                    <div className="col l10 s10 m10 form-signup">
                    <p>
                    <label>
                    <input type='radio' id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                    <span style={{color:'green'}}><b>{content.value}</b></span>
                    </label>
                    </p>
                    </div>
              </div>
            ))}
       </div>

            <div className="row">
                <div className="col s12 m12 l12">
                   <Display choosed={this.state.choosed}/>
                </div>
            </div>
        </React.Fragment>
    );
}
}
}
