import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import ValidateUser from '../validateUser'

export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'',
            modal:false,
            disabled:'',
            allowed:false,
            suspend_faculty_username:'',
            suspend_student_username:'',
            suspend_reason_faculty:'',
            suspend_reason_student:'',
            deniedlist:[],
            current_action:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount()
    {
      this.fetch_denied_list()
    }
    componentDidUpdate =(prevProps,prevState) => {
      if (prevProps.action !== this.props.action) {
        this.fetch(this.props.action);
      }
      if (prevState.allowed !== this.state.allowed) {
        if(this.state.current_action === "remove_sespension")
        {
          this.RemoveSuspension();
        }
        else
        {
          this.Suspend();
        }
      }
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }

    fetch_denied_list =() =>{
      axios.get('/user/fetch_denied_list')
      .then(res=>{
        this.setState({deniedlist: res.data})
      })
    }
    handleApprove = (id,username) =>{
      axios.post('/user/approverequest',{
        serial: id,
        username:username,
      }).then(res=>{
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== id),
        })
      })
    }


    handleUsername = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    Suspend = (e)=>{
      this.setState({modal: !this.state.modal,disabled:'disabled',current_action:'suspend_user'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:''})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/user/suspension_user"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/user2/suspension_suser"
        }
        this.handleSuspension(route);
      }
    }
    RemoveSuspension = (e)=>{
      this.setState({modal: !this.state.modal,disabled:'disabled',current_action:'remove_sespension'})
      if(this.state.allowed === true)
      {
        this.setState({success:'none',disabled:''})
        let route;
        if(this.props.select === "suspendfac")
        {
          route ="/user/remove_user_suspension"
        }
        else if(this.props.select === "suspendstu")
        {
          route ="/user2/remove_suser_suspension"
        }
        this.handle_Remove_Suspension(route);
      }
    }
    handleSuspension = (route) =>{
      axios.post(route,this.state)
      .then(res=>{
          window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }

    handle_Remove_Suspension = (route) =>{
      axios.post(route,this.state)
      .then(res=>{
          window.M.toast({html: res.data,classes:'rounded #ec407a pink lighten-1'});
      })
    }
    render(){
      let suspend_content;
      if(this.props.select === 'suspendfac'){
            suspend_content=
                <div className="center">
                   <div className="row">
                      <div className="input-field col s12 xl6 l6 m6">
        								<input id="username" type="text" name="suspend_faculty_username" className="validate" value={this.state.suspend_faculty_username} onChange={this.handleUsername} required />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Official Id</span>
        								<label htmlFor="username">Username</label>
								      </div>
                      <div className="input-field col s12 xl6 l6 m6">
        								<input id="username" type="text" name="suspend_reason_faculty" className="validate" value={this.state.suspend_reason_faculty} onChange={this.handleUsername}/>
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Ignore if it's Removal of Suspension</span>
        								<label htmlFor="username">Reason</label>
								      </div>
                      </div>
                      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Link>
								      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5 offset-l2 offset-m2 offset-xl2 offset-s2" disabled={this.state.disabled} onClick={this.Suspend}>Suspend Faculty</Link>
                </div>
        }
        else if(this.props.select === 'suspendstu'){
            suspend_content =
                <div className="center">
                   <div className="row">
                      <div className="input-field col s12 l6 xl6 m6">
        								<input id="username" type="text" className="validate" name="suspend_student_username" value={this.state.suspend_student_username} onChange={this.handleUsername} required />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Student Registation Number Only</span>
        								<label htmlFor="username">Username</label>
								      </div>
                      <div className="input-field col s12 l6 xl6 m6">
                        <input id="username" type="text" className="validate" name="suspend_reason_student" value={this.state.suspend_reason_student} onChange={this.handleUsername} />
                        <span className="helper-text" data-error="Please enter data!!" data-success="">Ignore if it's Removal of Suspension</span>
                        <label htmlFor="username">Reason</label>
                      </div>
                      </div>
                      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5" disabled={this.state.disabled} onClick={this.RemoveSuspension}>Remove Suspension</Link>
								      <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col l5 xl5 m5 s5 offset-l2 offset-m2 offset-xl2 offset-s2" disabled={this.state.disabled} onClick={this.Suspend}>Suspend Student</Link>
                </div>
        }
        else if(this.props.select === 'suspend_request'){
            suspend_content =
                <div style={{marginTop:'17px',marginRight:'10px'}}>
                <div className="row">
                  <div className="col l1 center"><b>Serial No.</b></div>
                  <div className="col l1 center"><b>Day Order</b></div>
                  <div className="col l2 center"><b>Official ID</b></div>
                  <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
                  <div className="col l3 center"><b>Reason</b></div>
                  <div className="col l2 center"><b>Action</b></div>
                </div><hr />
                {this.state.deniedlist.map((content,index)=>(
                  <React.Fragment key={index}>
                  <div className="row">
                  <div>
                  <div className="col l1 center"><b>{index+1}</b></div>
                  <div className="col l1 center"><b>{content.day_order}</b></div>
                  <div className="col l2 center"><b>{content.username}</b></div>
                  <div className="col l3 center"><b>{content.day}/{content.month}/{content.year}</b></div>
                  <div className="col l3 center"><b>{content.req_reason}</b></div>
                  </div>
                  <button className="btn col l2 " onClick={() => this.handleApprove(content.serial,content.username)}>Approve</button>
                  </div>
                  </React.Fragment>
                ))}
                </div>
        }
        else{
            suspend_content=
                <div></div>
        }
        return(
          <React.Fragment>
          <div style={{display:this.state.success}}>
            <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
          </div>
          <div style={{}}>
            {suspend_content}
          </div>
          </React.Fragment>
        );
    }
}
