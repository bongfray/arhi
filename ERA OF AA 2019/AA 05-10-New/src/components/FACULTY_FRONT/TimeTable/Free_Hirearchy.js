import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Select} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'

export default class FreeS extends React.Component {
  constructor()
  {
    super()
    this.state ={
      administrativefreefield:'',
      hfreefield:'',
      researchfreefield:'',
      root_names:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

componentDidMount()
{
  let selects = document.querySelectorAll('select');
  M.FormSelect.init(selects, {});
  this.getRoles();
}
componentDidUpdate =(prevProps,prevState) => {
  if (prevProps.freefield !== this.props.freefield) {
    this.getRoles();
  }
}

getRoles =()=>{
  console.log(this.props.freefield)
  axios.post('/user/root_percentage_fetch',{action:this.props.freefield})
  .then(res=>{
    this.setState({root_names:res.data})
  })
}

handleHFreeField =(e) =>{
  this.setState({
    hfreefield: e.target.value,
  });
  this.props.freeParts({
      freeparts:e.target.value,
  })
}

render()
{
  if(this.props.freefield ==="Academic")
  {
    return(
      <React.Fragment >
      <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
      {this.state.root_names.map((content,index)=>{
        return(
          <option value={content.responsibilty_title} key={index}>{content.responsibilty_title}</option>
      )
      })}
    </Select>
      </React.Fragment>
    );
  }
  else if(this.props.freefield ==="Administrative")
  {
    return(
      <React.Fragment>
        <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
        {this.state.root_names.map((content,index)=>{
          return(
            <option value={content.responsibilty_title} key={index}>{content.responsibilty_title}</option>
        )
        })}
        </Select>
      </React.Fragment>

    );
  }
  else if(this.props.freefield ==="Research")
  {
    return(
      <React.Fragment>
        <Select l="12" xl="12" s="12" m="6" value={this.state.hfreefield} onChange={this.handleHFreeField}>
        {this.state.root_names.map((content,index)=>{
          return(
            <option value={content.responsibilty_title} key={index}>{content.responsibilty_title}</option>
        )
        })}
        </Select>
      </React.Fragment>

    );
  }
  else{
    return(
      <React.Fragment>
      </React.Fragment>
    )
  }

}
}
