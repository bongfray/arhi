import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'

var empty = require('is-empty');

export default class Allot extends React.Component {
  constructor() {
    super()
    this.state = {
        sending:'SUBMIT',
        selected:'',
        covered_topic:null,
        problem_statement:null,
        compday:null,
        compmonth:null,
        compyear:null,
        compdayorder:null,
        compslot:null,
        allot:'',
        count: 0,
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        date:'',
        year:'',
        month:'',
    }
  }


  handleChecked =(e) =>{
    this.setState({
      selected: e.target.value,
    })
  }

  updateAllotV =(userObject) => {
    this.setState(userObject)
  }


  closeModal=()=>{
    this.props.closeModal();
  }
  colorChange=()=>{
    this.props.color()
  }

handleAlloted =(e) =>{
  e.preventDefault();
  if(empty(this.state.selected))
  {
    window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
  }
  else
  {
    if(this.state.compday || this.state.compmonth || this.state.compyear)
    {
      if(empty(this.state.compday) && empty(this.state.compmonth) && empty(this.state.compyear) && empty(this.state.compdayorder) && empty(this.state.compslot))
      {
        window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else if( !((this.state.compday).length<=2) && !((this.state.compmonth).length<=2) && ((this.state.compday).length!==4) && ((this.state.compdayorder).length!==1))
      {
        window.M.toast({html: 'Please Follow the Correct Format of Inputting..',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else{
        e.preventDefault()

        Date.prototype.getWeek = function () {
            var onejan = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
        };

        var myDate = new Date.today();
        var week =myDate.getWeek();

      axios.post('/user/timeallot', {
        day_slot_time: this.props.day_order+this.props.day_slot_time,
        day_order: this.props.day_order,
        selected: this.state.selected,
        order:this.props.day_order+this.props.day_slot_time+'Alloted',
        saved_slots: this.state.saved_slots,
        problem_statement: this.state.problem_statement,
        compday: this.state.compday,
        compmonth: this.state.compmonth,
        compyear: this.state.compyear,
        compdayorder:this.state.compdayorder,
        compslot: this.state.compslot,
        covered: this.state.covered_topic,
        date:this.props.day,
        month: this.props.month,
        year: this.props.year,
        week:week,
      },
      this.setState({
        sending: 'Uploading Datas',
      })
    )
        .then(response => {
          if(response.status===200){
             if(response.data.succ)
            {
              window.M.toast({html: 'Success !!', outDuration:'1200', inDuration:'1200', classes:'rounded #ec407a green lighten-1'});
              this.closeModal();
              this.colorChange();
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }
    }
    else
    {
    Date.prototype.getWeek = function () {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };

    var myDate = new Date.today();
    var week =myDate.getWeek();

  axios.post('/user/timeallot', {
    usern: this.props.usern,
    day_slot_time: this.props.day_order+this.props.day_slot_time,
    day_order: this.props.day_order,
    selected: this.state.selected,
    order:this.props.day_order+this.props.day_slot_time+'Alloted',
    saved_slots: this.state.saved_slots,
    problem_statement: this.state.problem_statement,
    compday: this.state.compday,
    compmonth: this.state.compmonth,
    compyear: this.state.compyear,
    compdayorder:this.state.compdayorder,
    compslot: this.state.compslot,
    covered: this.state.covered_topic,
    date:this.props.day,
    month: this.props.month,
    year: this.props.year,
    week:week,
  },this.setState({
    sending: 'Uploading Datas',
  }))
    .then(response => {
      if(response.status===200){
        if(response.data.succ)
        {
          window.M.toast({html: 'Success !!', outDuration:'1200', inDuration:'1200', classes:'rounded #ec407a green lighten-1'});
          this.closeModal();
          this.colorChange();
        }
      }
    }).catch(error => {
      window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
    })
  }
 }
}



render(){
// console.log(this.state);
  return(
    <div>
    <span style={{color:'red'}}>Reminder: </span><span>This slot is booked at the beginig of Semester.You are not allowed to edit it.</span>
    <div className="row">
      <div className="col l6 xl6 s12 m12">
      <p>
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Completed' onChange={this.handleChecked} />
          <span style={{color:'green'}} className="center"><b>Class/Work Completed</b></span>
        </label>
     </p>
      </div>
      <div className="col l6 xl6 m12 s12">
      <p>
        <label>
        <input type='radio' id='radio-1' name='myRadio' value='Problem' onChange={this.handleChecked} />
          <span style={{color:'green'}} className="center"><b>Problem With Work/Class Completion(like Slot Cancelled)</b></span>
        </label>
     </p>
      </div>
    </div>
    <DivOpen  selected={this.state.selected} updateAllotV={this.updateAllotV}/>
  <button className="waves-effect btn col l2 s4 xl2 m2 blue-grey darken-2 sup right" onClick={this.handleAlloted}>{this.state.sending}</button>
   </div>
  );
}
}





{/*-------for check radio button----------------------------------- */}

 class DivOpen extends Component{
   constructor(){
     super();
     this.state ={
       covered_topic:'',
       problem_statement:'',
     }
     this.componentDidMount = this.componentDidMount.bind(this);
   }
   componentDidMount(){

   }
handleAllotData= (evt) =>{
     const value = evt.target.value;
     this.setState({
       [evt.target.name]: value
     });
     this.props.updateAllotV({
       [evt.target.name]: value,
     });
   }



   render(){

     if(this.props.selected ==="Completed")
     {
       return(
         <div className="input-field">
         <input id="covered" type="text" name="covered_topic" value={this.state.covered_topic}  onChange={this.handleAllotData}/>
         <label htmlFor="covered">Enter the the things covered in this alloted slot.</label>
         </div>
       );
     }
     else if(this.props.selected === "Problem")
     {
       return(
         <React.Fragment>
         <div className="input-field">
         <input id="prob" type="text" name="problem_statement" value={this.state.problem_statement} onChange={this.handleAllotData}/>
           <label htmlFor="prob">Enter the reason</label>
         </div>
         <div className="">
          <Switch updateAllotV={this.props.updateAllotV}/>
         </div>
         </React.Fragment>
       );
     }
     else
     {
       return(
       <div></div>
     );
     }
   }
 }




  class Switch extends React.Component {

      constructor ( props ) {
          super( props );
  		this.state = {
  			isChecked: false,
  		}
      }
      statusOfCompensation= (e) =>{
        this.setState({
          isChecked: !this.state.isChecked,
        })
      }
      render () {

          return(
            <div>

              <div className="switch center">
                  <label style={{color:'red',fontSize:'15px'}}>
                      <input  checked={ this.state.isChecked } onChange={ this.statusOfCompensation} type="checkbox" />
                      <span className="lever"></span>
                      Want to compensate this on other day ?
                  </label>
              </div>
              <br />
              <InputValue datas={this.state.isChecked} updateAllotV={this.props.updateAllotV}/>
            </div>
          );
      }

  }

  class InputValue extends Component{
    constructor()
    {
      super()
      this.state ={
        compdate:'',
        compdayorder:'',
        compslot:'',
      }
    }

    handleComData =(evt)=>{
      const value = evt.target.value;
      this.setState({
        [evt.target.name]: value
      });
      this.props.updateAllotV({
        [evt.target.name]: value,
      });
    }


    render(){
      if(this.props.datas === true)
      {
        return(
          <React.Fragment>
          <div className="row">
          <div className="col l4 s12 m12 xl4">
          <div className="input-field">
             <input id="fac1" name="compday" type="number" onChange={this.handleComData} required/>
             <label htmlFor="fac1">Mention the Date(like : "June 1" then type 1)</label>
          </div>
          </div>
          <div className="col l4 s12 m12 xl4">
          <div className="input-field">
             <input id="fac2" name="compmonth" type="number" onChange={this.handleComData} required/>
             <label htmlFor="fac2">Mention the Month(like: "June" means 6)</label>
          </div>
          </div>
          <div className="col l4 s12 m12 xl4">
          <div className="input-field">
             <input id="fac3" name="compyear" type="number" onChange={this.handleComData} required/>
             <label htmlFor="fac3">Mention the Year(like 1994)</label>
          </div>
          </div>
          </div>
          <div className="row">
           <div className="col l6 s12 m12 xl6">
               <div className="input-field">
                  <input id="fac4" name="compdayorder" type="number" onChange={this.handleComData} required/>
                  <label htmlFor="fac4">Enter the DayOrder(For DayOrder 1, type only 1)</label>
               </div>
           </div>
           <div className="col l6 s12 m12 xl6">
               <div className="input-field">
                  <input id="fac6" type="text" name="compslot" onChange={this.handleComData} required/>
                  <label htmlFor="fac6">Mention the slot</label>
               </div>
           </div>
          </div>
          </React.Fragment>
        );
      }
      else {
        return(
          <React.Fragment></React.Fragment>
        )

      }
    }
  }





 const TableRow = ({ children }) => (
   <div className="row">{children}</div>
 );
