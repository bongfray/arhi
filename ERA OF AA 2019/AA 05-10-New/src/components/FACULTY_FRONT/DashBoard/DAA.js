import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
			logout:'/user/logout',
			get:'/user/',
      count:0,
      nav_route: '/user/fetchnav',
      percent_ad:[],
      percent_ac:[],
      percent_re:[],
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }

fetchPercentageAd = () =>{
  axios.get('/user/knowPercentageAd', {
  })
  .then(response =>{
    this.setState({percent_ad:response.data})
  })
}
fetchPercentageAcademic = () =>{
  axios.get('/user/knowPercentageAc', {
  })
  .then(response =>{
    this.setState({percent_ac:response.data})
  })
}
fetchPercentageResearch = () =>{
  axios.get('/user/knowPercentageRe', {
  })
  .then(response =>{
    this.setState({percent_re:response.data})
  })
}

fetch_Complete_PercentageAd = () =>{
  axios.get('/user/know_Complete_PercentageAd', {
  })
  .then(response =>{
    this.setState({percent_ad:response.data})
  })
}
fetch_Complete_PercentageAcademic = () =>{
  axios.get('/user/know_Complete_PercentageAc', {
  })
  .then(response =>{
    this.setState({percent_ac:response.data})
  })
}
fetch_Complete_PercentageResearch = () =>{
  axios.get('/user/know_Complete_PercentageRe', {
  })
  .then(response =>{
    this.setState({percent_re:response.data})
  })
}

    componentDidMount() {
        M.AutoInit();
        this.fetchPercentageAd();
        this.fetchPercentageAcademic();
        this.fetchPercentageResearch();
    }
  render()
  {
    return(
      <React.Fragment>
      <div className="row">
        <div className="col l4">
        <ul className="collapsible">
        <li>
          <div className="center collapsible-header pink" style={{color:'white'}}>Administrative Works</div>
          <div className="collapsible-body">
          <div className="row">
            <div className="col l11 left">Total Percentage Alloted for Administrative Work: </div>
            <div className="col l1">{this.state.total_adminispercentage}</div>
          </div>
          <table>
            <thead>
              <tr>
                  <th>Different Works</th>
                  <th className="center">Alloted (H/W)</th>
                  <th className="center">Completed (H/W)</th>
              </tr>
            </thead>
            <tbody>
            {this.state.percent_ad.map((content,index)=>{
              return(
              <tr key={index}>
                <td className="">{content.responsibilty_title}</td>
                <td className="center">{content.responsibilty_percentage}</td>
                <td className="center"><Total role={content.responsibilty_title}/></td>
              </tr>
            )
            })}
            </tbody>
          </table>
          </div>
          </li>
        </ul>
        </div>

        <div className="col l4">
        <ul className="collapsible">
        <li>
           <div className="center collapsible-header pink" style={{color:'white'}}>Academic Work</div>
           <div className="collapsible-body">
           <div className="row">
             <div className="col l11 left">Total Percentage Alloted for Academic Work: </div>
             <div className="col l1">{this.state.total_academicpercentage}</div>
           </div>

           <table>
             <thead>
               <tr>
                   <th>Different Works</th>
                   <th className="center">Alloted (H/W)</th>
                   <th className="center">Completed (H/W)</th>
               </tr>
             </thead>
             <tbody>
             {this.state.percent_ac.map((content,index)=>{
               return(
               <tr key={index}>
                 <td className="">{content.responsibilty_title}</td>
                 <td className="center">{content.responsibilty_percentage}</td>
                 <td className="center"><Total role={content.responsibilty_title}/></td>
               </tr>
             )
             })}
             </tbody>
           </table>
           </div>
           </li>
           </ul>
        </div>
        <div className="col l4">
        <ul className="collapsible">
        <li>
           <div className="center collapsible-header pink" style={{color:'white'}}>Research Work</div>
           <div className="collapsible-body">
           <div className="row">
             <div className="col l12 left">Total Percentage Alloted for Research Work: <span>{this.state.total_researchpercentage}</span></div>
           </div>
           <table>
           <thead>
             <tr>
                 <th>Different Works</th>
                 <th className="center">Alloted (H/W)</th>
                 <th className="center">Completed (H/W)</th>
             </tr>
           </thead>
           <tbody>
           {this.state.percent_re.map((content,index)=>{
             return(
             <tr key={index}>
               <td className="">{content.responsibilty_title}</td>
               <td className="center">{content.responsibilty_percentage}</td>
               <td className="center"><Total role={content.responsibilty_title}/></td>
             </tr>
           )
           })}
           </tbody>
           </table>
           </div>
           </li>
           </ul>
        </div>
      </div>

      </React.Fragment>
    )
  }
}



class Total extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      count:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTotal();
  }
  componentDidUpdate =(prevProps,prevState) => {
    if (prevProps.role !== this.props.role){
      this.fetchTotal();
    }
  }
  fetchTotal=()=>
  {
    axios.post('/user/know_Complete_Percentage',{res:this.props.role})
    .then(response =>{
      let len= ((response.data).length)
      this.setState({count:len})
    })
  }
  render()
  {
    return(
      <React.Fragment>
         {this.state.count}
      </React.Fragment>
    );
  }
}
