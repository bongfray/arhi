import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Nav from '../../dynnav'

import Display from './Operations/display'


export default class Ap extends Component{
  constructor() {
    super()
    this.state={
      isChecked:false,
      logout:'/user/logout',
      get:'/user/',
      nav_route: '',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
      this.fetchlogin();
  }
  fetchlogin = () =>{
      axios.get('/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
  handleComp = (e) =>{
    this.setState({
      isChecked: !this.state.isChecked,
    })
  }
  toggleDiv =()=>{
    this.setState({toggled:!this.state.toggled})
  }
  render()
  {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
    return(
        <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          <div className="center">
               {this.state.toggled ===  false && <React.Fragment><i className="dig small material-icons go hide-on-med-and-down" onClick={this.toggleDiv}>arrow_drop_down_circle</i>
               <i className="dig small material-icons go hide-on-large-only" style={{marginTop:'30px'}} onClick={this.toggleDiv}>arrow_drop_down_circle</i>
               </React.Fragment>}
               {this.state.toggled ===  true && <div className="card dig center">
               <div className="row" style={{marginTop:'25px'}}>
                   <div className="switch center">
                       <label style={{color:'red',fontSize:'15px'}}>
                           eWork
                           <input  checked={ this.state.isChecked } value="month" onChange={ this.handleComp} type="checkbox" />
                           <span className="lever"></span>
                           AdminOperation
                       </label>
                   </div>
               </div>
               <i class="small material-icons go" onClick={this.toggleDiv}>arrow_drop_up</i>
               </div>
               }
          </div>
          <div className="hide-on-med-and-down" style={{marginTop:'17px'}}>
              <System choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>
          <div className="hide-on-large-only" style={{marginTop:'60px'}}>
              <System choice={this.state.isChecked} toggled={this.state.toggled} />
          </div>

       </React.Fragment>
    )
  }
  }
}


class System extends Component {
  constructor() {
    super()
    this.state={

    }
  }
  render()
  {
    if(this.props.choice === true)
    {
      return(
        <React.Fragment>
        {this.props.toggled === false &&
            <Admin />
        }
        </React.Fragment>
      )
    }
    else{
      return(
        <div>EWork</div>
      )
    }
  }
}


class Admin extends Component{
constructor(){
    super();
    this.state ={
        radio:[{name:'radio1',color:'blue',value:'Insert'},{name:'radio2',color:'black',value:'Update'},{name:'radio3',color:'orange',value:'Super'},{name:'radio4',color:'red',value:'Delete'},{name:'radio5',color:'yellow',value:'Modify'},{name:'radio6',color:'aqua',value:'Suspend'}],
        isChecked: false,
        choosed: '',
    }
}

handleChecked =(e,index,color)=>{
    this.setState({
        isChecked: !this.state.isChecked,
        choosed: e.target.value
    });
}
render()
{

    return(
        <React.Fragment>
       <div className="row">
            {this.state.radio.map((content,index)=>(
                <div className="col s6 l2 m2" key={index}>
                    <div className="col l10 s10 m10 form-signup">
                    <p>
                    <label>
                    <input type='radio' id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                    <span style={{color:'green'}}><b>{content.value}</b></span>
                    </label>
                    </p>
                    </div>
              </div>
            ))}
       </div>

            <div className="row">
                <div className="col s12 m12 l12">
                   <Display choosed={this.state.choosed}/>
                </div>
            </div>
        </React.Fragment>
    );
}
}
