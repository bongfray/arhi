const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const notification = new Schema({

  for: { type: String, unique: false, required: false },
  subject: { type: String, unique: false, required: false },
  details: { type: String, unique: false, required: false },
  new: { type: Boolean, unique: false, required: false, default:true },
})


notification.plugin(autoIncrement.plugin, { model: 'Notification', field: 'serial', startAt: 1,incrementBy: 1 });

const noti = mongoose.model('Notification', notification)
module.exports = noti
