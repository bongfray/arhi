import React, { Component} from 'react'
import { } from 'react-router-dom'
import {Link, Redirect } from 'react-router-dom'
import M from 'materialize-css'
import axios from 'axios'
import $ from 'jquery'
import Nav from '../../dynnav'
import { Select } from 'react-materialize'
import ContentLoader from "react-content-loader"
require("datejs")


export default class View extends Component {
  constructor() {
    super()
    this.state ={
      isChecked: false,
      display:'none',
      username:'',
      redirectTo:'',
      choosed: '',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      agreed:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    M.AutoInit()
  }

retriveValue =(e)=>{
  this.setState({[e.target.name]:e.target.value})
}
  Authenticate =(e)=>{
    e.preventDefault()
    axios.post('/user/authenticate',this.state)
    .then(res=>{
    if(res.data === "Allowed")
    {
      this.setState({
        display:'block',
        agreed:[{name:'radio1',value:'Academic'},{name:'radio2',value:'Administrative'},{name:'radio3',value:'Research'},{name:'radio4',value:'Degrees'},{name:'radio5',value:'Section Datas'},],
      })
    }
    else{
      this.setState({display:'bloack'})
    }
        window.M.toast({html: res.data ,outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
    })
  }


  handleChecked =(e,index)=>{
      this.setState({
        isChecked: !this.state.isChecked,
          choosed: e.target.value
      });
  }

  render()
  {
    return(
      <React.Fragment>
        <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
         <div className="row">
           <div className="col l6 s12 m12 xl6">
               <div className="form-signup">
                  <div className="row">
                      <form onSubmit={this.Authenticate} >
                          <div className="input-field">
                             <input type="text" name="username" value={this.state.username} onChange={this.retriveValue} id="id" className="validate" required />
                             <label htmlFor="id">Input Official ID</label>
                          </div>
                          <button className="btn col l2 pink right" >View</button>
                      </form>
                  </div>
                  <br />
                  <div className="row">
                     <span className="green-text">DECLAMAIR:  </span>Please Remember in this section you can only see the User's Data only if they are not coming in your same or upper designation level.
                     One more thing keep in mind users in medium level(like HOD , Professor) have only access to view datas within their department only.
                  </div>
                </div>
           </div>
         <div className="col l6 s12 m12 xl6" style={{display:this.state.display}}>
             <div className="form-signup">
                 <div className="row">
                    <div className="col l12 s12 m12 xl12 center" style={{fontSize:'20px'}}>
                       You are seeing the profile of <span className="pink-text">{this.state.username}</span>
                    </div>
                 </div>
                 <div className="row">
                   {this.state.agreed.map((content,index)=>(
                       <div className="col s6 l6 m6" key={index}>
                           <div className="col l10 s10 m10 form-signup">
                           <p>
                           <label>
                           <input type='radio' className="with-gap" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                           <span style={{color:'green'}}><b>{content.value}</b></span>
                           </label>
                           </p>
                           </div>
                     </div>
                   ))}
                 </div>
              </div>
            </div>
         </div>
         <div className="row" style={{display:this.state.display}}>
           <ShowData choosed={this.state.choosed}/>
         </div>
      </React.Fragment>
    );
  }
}

class  ShowData extends Component {
  constructor(props) {
    super(props)
    this.state ={
      option:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  hnadleOption = (e) =>{
    this.setState({option:e.target.value})
  }
  componentDidMount()
  {
    M.AutoInit()
  }
  render()
  {
    let content;
    if(this.props.choosed === "Section Datas")
    {
      content =
      <React.Fragment>
        <div className="form-signup center">
           {this.props.choosed}
           <React.Fragment>
           <div className="row">
           <div className="col l1 hide-on-mid-and-down" />
           <div className="col l10 s12 m12 xl10">
           <div className="row form-signup">
           <div className="col l6 xl6 m12 s12">
             <p style={{fontSize:'22px'}}>Select Catagory : </p>
           </div>
             <div className="col l6 s12 xl6 m12">
                 <Select l="9" xl="9" s="12" m="12" name="title" value={this.state.option} onChange={this.handleOption}>
                 <option value="" disabled defaultValue>Select Here...</option>
                 <option value="thesis_projects_supervised">Thesis & Projects Supervised</option>
                 <option value="training_workshop_attended">Training or Workshop or Seminer or Conference Attended</option>
                 <option value="training_workshop_held">Training or Workshop or Seminer or Conference Held</option>
                 <option value="paper_published_accepted">Paper Published & Accepted</option>
                 <option value="research_projects_done_by_you">Research or Projects Done by You</option>
                 <option value="contribution_to_university">Contribution to University</option>
                 <option value="department_activities">Department Activities</option>
                 <option value="department_committe_membership">Department Committes Membership</option>
                 <option value="advising_counselling_details">Advising and Counselling Details</option>
                 </Select>
             </div>
             </div>
             </div>
             <div className="col l1 hide-on-mid-and-down" />
             </div>
            <Section section_props={this.state.option}/>
           </React.Fragment>
        </div>
      </React.Fragment>
    }
    else{
      content =
      <div></div>
    }
    return(
      <div>
      {content}
      </div>
    )
  }
}


class Section extends React.Component {
      constructor() {
        super();
        this.state = {
          todos: [],
          currentPage: 1,
          todosPerPage: 3,
          upperPageBound: 3,
          lowerPageBound: 0,
          isPrevBtnActive: 'disabled',
          isNextBtnActive: '',
          pageBound: 3
        };
        this.componentDidMount = this.componentDidMount.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.btnDecrementClick = this.btnDecrementClick.bind(this);
        this.btnIncrementClick = this.btnIncrementClick.bind(this);
        this.btnNextClick = this.btnNextClick.bind(this);
        this.btnPrevClick = this.btnPrevClick.bind(this);
        this.setPrevAndNextBtnClass = this.setPrevAndNextBtnClass.bind(this);
      }
      fetchSec = ()=>{
        axios.post('/user/datas_for_section',{section_props:this.props.section_props})
        .then(res=>{
          this.setState({todos:res.data})
        })
      }
      componentDidMount()
      {
        this.fetchSec();
      }

      componentDidUpdate(prevProps) {
        if(prevProps.section_props!==this.props.prevProps)
        {
          this.fetchSec();
        }
            $("ul li.active").removeClass('active');
            $('ul li#'+this.state.currentPage).addClass('active');
      }
      handleClick(event) {
        let listid = Number(event.target.id);
        this.setState({
          currentPage: listid
        });
        $("ul li.active").removeClass('active');
        $('ul li#'+listid).addClass('active');
        this.setPrevAndNextBtnClass(listid);
      }
      setPrevAndNextBtnClass(listid) {
        let totalPage = Math.ceil(this.state.todos.length / this.state.todosPerPage);
        this.setState({isNextBtnActive: 'disabled'});
        this.setState({isPrevBtnActive: 'disabled'});
        if(totalPage === listid && totalPage > 1){
            this.setState({isPrevBtnActive: ''});
        }
        else if(listid === 1 && totalPage > 1){
            this.setState({isNextBtnActive: ''});
        }
        else if(totalPage > 1){
            this.setState({isNextBtnActive: ''});
            this.setState({isPrevBtnActive: ''});
        }
    }
      btnIncrementClick() {
          this.setState({upperPageBound: this.state.upperPageBound + this.state.pageBound});
          this.setState({lowerPageBound: this.state.lowerPageBound + this.state.pageBound});
          let listid = this.state.upperPageBound + 1;
          this.setState({ currentPage: listid});
          this.setPrevAndNextBtnClass(listid);
    }
      btnDecrementClick() {
        this.setState({upperPageBound: this.state.upperPageBound - this.state.pageBound});
        this.setState({lowerPageBound: this.state.lowerPageBound - this.state.pageBound});
        let listid = this.state.upperPageBound - this.state.pageBound;
        this.setState({ currentPage: listid});
        this.setPrevAndNextBtnClass(listid);
    }
    btnPrevClick() {
        if((this.state.currentPage -1)%this.state.pageBound === 0 ){
            this.setState({upperPageBound: this.state.upperPageBound - this.state.pageBound});
            this.setState({lowerPageBound: this.state.lowerPageBound - this.state.pageBound});
        }
        let listid = this.state.currentPage - 1;
        this.setState({ currentPage : listid});
        this.setPrevAndNextBtnClass(listid);
    }
    btnNextClick() {
        if((this.state.currentPage +1) > this.state.upperPageBound ){
            this.setState({upperPageBound: this.state.upperPageBound + this.state.pageBound});
            this.setState({lowerPageBound: this.state.lowerPageBound + this.state.pageBound});
        }
        let listid = this.state.currentPage + 1;
        this.setState({ currentPage : listid});
        this.setPrevAndNextBtnClass(listid);
    }
      render() {
        alert(this.props.section_props)
        const { todos, currentPage, todosPerPage,upperPageBound,lowerPageBound,isPrevBtnActive,isNextBtnActive } = this.state;
        // Logic for displaying current todos
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);

        const renderTodos = currentTodos.map((todo, index) => {
          return <li key={index}>{todo.credits_for_thesis_project}</li>;
        });

        // Logic for displaying page numbers
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
          pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            if(number === 1 && currentPage === 1){
                return(
                    <li key={number} className='active' id={number}><a href='#' id={number} onClick={this.handleClick}>{number}</a></li>
                )
            }
            else if((number < upperPageBound + 1) && number > lowerPageBound){
                return(
                    <li key={number} id={number}><a href='#' id={number} onClick={this.handleClick}>{number}</a></li>
                )
            }
        });
        let pageIncrementBtn = null;
        if(pageNumbers.length > upperPageBound){
            pageIncrementBtn = <li className=''><a href='#' onClick={this.btnIncrementClick}> &hellip; </a></li>
        }
        let pageDecrementBtn = null;
        if(lowerPageBound >= 1){
            pageDecrementBtn = <li className=''><a href='#' onClick={this.btnDecrementClick}> &hellip; </a></li>
        }
        let renderPrevBtn = null;
        if(isPrevBtnActive === 'disabled') {
            renderPrevBtn = <li className={isPrevBtnActive}><span id="btnPrev"> Prev </span></li>
        }
        else{
            renderPrevBtn = <li className={isPrevBtnActive}><a href='#' id="btnPrev" onClick={this.btnPrevClick}> Prev </a></li>
        }
        let renderNextBtn = null;
        if(isNextBtnActive === 'disabled') {
            renderNextBtn = <li className={isNextBtnActive}><span id="btnNext"> Next </span></li>
        }
        else{
            renderNextBtn = <li className={isNextBtnActive}><a href='#' id="btnNext" onClick={this.btnNextClick}> Next </a></li>
        }
        return (
          <div>
            <ul>
              {renderTodos}
            </ul>
            <ul className="pagination">
              {renderPrevBtn}
              {pageDecrementBtn}
              {renderPageNumbers}
              {pageIncrementBtn}
              {renderNextBtn}
            </ul>
          </div>
        );
      }
    }
