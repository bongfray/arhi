import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import ContentLoader from "react-content-loader"
import Nav from '../../dynnav'
import AddProduct from './add'
import ProductList from './prod'
class FiveYear extends Component{
    constructor(){
        super();
        this.state={
          home:'/faculty',
          logout:'/user/logout',
          get:'/user/',
          nav_route: '/user/fetchnav',
          username:'',
          redirectTo:'',
          isAddProduct: false,
          response: {},
          action:'',
          product: {},
          isEditProduct: false,
          five:[{year:'Year - I',parts:[{name:'Administrative',index:'YearIAdministrative'},{name:'Academic',index:'YearIAcademic'},{name:'Research',index:'YearIResearch'}]},
          {year:'Year - II',parts:[{name:'Administrative',index:'YearIIAdministrative'},{name:'Academic',index:'YearIIAcademic'},{name:'Research',index:'YearIIResearch'}]},
          {year: 'Year - III',parts:[{name:'Administrative',index:'YearIIIAdministrative'},{name:'Academic',index:'YearIIIAcademic'},{name:'Research',index:'YearIIIResearch'}]},
          {year:'Year - IV',parts:[{name:'Administrative',index:'YearIVAdministrative'},{name:'Academic',index:'YearIVAcademic'},{name:'Research',index:'YearIVResearch'}]},
          {year:'Year - V',parts:[{name:'Administrative',index:'YearVAdministrative'},{name:'Academic',index:'YearVAcademic'},{name:'Research',index:'YearVResearch'}]}]
        }
    }
    componentDidMount(){
        M.AutoInit();
        axios.get('/user/',
        this.setState({loading: true})
      )
         .then(response =>{
           this.setState({loading: false})
           if(response.data.user)
           {
             this.setState({username: response.data.user.username})
           }
           else{
             this.setState({
               redirectTo:'/faculty',
             });
             window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
           }
         })
    }


    onCreate = (e,index) => {
      this.setState({ isAddProduct: index ,product: {}});
    }
    onFormSubmit =(data) => {
      console.log(data)
      let apiUrl;
      if(this.state.isEditProduct){
        apiUrl = '/user/edit_five_year_plan';
      } else {
        apiUrl = '/user/add_five_year_plan';
      }
      axios.post(apiUrl,data)
          .then(response => {
            this.setState({
              response: response.data,
              isAddProduct: false,
              isEditProduct: false
            })
          })
    }

    editProduct = (productId,index)=> {
      axios.post('/user/fetch_five_year_existing_data',{
        id: productId,
      })
          .then(response => {
            this.setState({
              product: response.data,
              isEditProduct: index,
              isAddProduct: index,
            });
          })

   }


    render(){
      const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="17" y="10" rx="0" ry="0" width="90" height="90" />
          <rect x="109" y="10" rx="0" ry="0" width="90" height="90" />
          <rect x="201" y="10" rx="0" ry="0" width="90" height="90" />
          <rect x="293" y="10" rx="0" ry="0" width="90" height="90" />

          <rect x="17" y="102" rx="0" ry="0" width="90" height="200" />


        </ContentLoader>
      )
      if(this.state.loading === true)
      {
        return(
          <MyLoader />
        );
      }
      else{
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return(
            <React.Fragment>
            <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
              <div><h6 className="center prof-heading black-text">Your Plan For Next Five Year</h6></div>
                <div className="row">
                {this.state.five.map((content,index)=>(
                  <div key={index} className="col l4 xl4 s12 m12 card">
                      <p className="center #64ffda teal accent-2 black-text" style={{height: '45px',paddingTop: '12px'}}>{content.year}</p>
                      {content.parts.map((items,no)=>(
                        <ul className="collapsible" key={no}>
                            <li className="active">
                                <div className="center collapsible-header #757575 grey darken-1">{items.name}</div>
                                <div className="collapsible-body">
                                    <div className="row">
                                    {!this.state.isAddProduct && <ProductList data={items} username={this.state.username}  action={items.index} editProduct={this.editProduct}/>}
                                    {!this.state.isAddProduct &&
                                      <div className="btn-floating btn-small right" onClick={(e) => this.onCreate(e,items.index)}><i className="material-icons go">add</i></div>
                                     }

                                     {((this.state.isAddProduct === items.index) || (this.state.isEditProduct === items.index)) &&
                                     <AddProduct data={items}  action={items.index} username={this.state.username} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                                     }
                                    </div>
                                </div>
                            </li>
                      </ul>
                      ))}
                  </div>
                ))}
                </div>
            </React.Fragment>
        )
      }
    }
    }
}

export default FiveYear;
