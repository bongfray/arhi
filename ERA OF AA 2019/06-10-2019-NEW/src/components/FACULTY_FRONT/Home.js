import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Nav from '../dynnav'
import ContentLoader from "react-content-loader"
import M from 'materialize-css'
import Fhome from '../fhome2.png'

export default class FrontPage extends Component{
  _isMounted = false;
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      loader:false,
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      order_action:'faculty',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/user/',
    this.setState({loader: true})
  ).then(response => {
    if (this._isMounted) {
    this.setState({loader:false})
      if (response.data.user) {
          this.setState({display:"disabled"})
      }
    }
        })

      }


    componentDidMount() {
      this._isMounted = true;
        this.getUser();
        M.AutoInit();
        let elems = document.querySelectorAll(".modal-trigger");
        M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });

    }
    componentWillUnmount =()=> {
      this.setState({loader:false})
      this._isMounted = false;
    }

    render(){
      const MyLoader = () => (
        <ContentLoader
          height={160}
          width={400}
          speed={2}
          primaryColor="#f3f3f3"
          secondaryColor="#c0c0c0"
        >
          <rect x="10" y="25" rx="0" ry="0" width="190" height="130" />
          <rect x="240" y="25" rx="0" ry="0" width="150" height="130" />
        </ContentLoader>
      )
      if(this.state.loader=== true)
      {
        return(
          <MyLoader />
        );
      }
      else{
        return(
          <React.Fragment>
          <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
            <div className="row">
                <div className="col l6 m12 s12">
                    <div className="fstyle">
                          <center><h4  className="ework_name">E-Work</h4></center><br /><br />
                          <p className="fpara">
                            Ework is a simplified analytics tool.
                            It is a tool to keep trace and record of each and everyday routine of staff members of the institute.
                            E-Work is a tool designed by CARE which majorly focuses on the encouragement of the well deserved ones.
                          </p><br/><br/>
                          <div className="row">
                                  <div className="col l1 m1 hide-on-down" />
                                  <div className="col l4 s12 m12">
                                     <Link to="/flogin" style={{width:'100%'}} disabled={this.state.display} className="left waves-effect btn #03a9f4 light-blue"><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></Link>
                                  </div>
                                  <div className="col l2 m2 hide-on-down" />
                                  <div className="col l4 s12 m12">
                                      <Link to="#" style={{width:'100%'}} className="waves-effect btn #c0ca33 lime darken-1 right"><i className="material-icons right">desktop_mac</i><b>About</b></Link>
                                  </div>
                                  <div className="col l1 m1 hide-on-down" />
                           </div>

                           <a className=" col s3 offset-l9 offset-s5 offset-m5 fprivacy modal-trigger" href="#modal1">
                             Privacy Policy
                           </a>
                            <div id="modal1" className="modal modal-fixed-footer modal-fixed">
                              <div className="modal-content">
                                  <h4 className="center black-text">Privacy Policy</h4>
                                  <p className="center black-text">This section will be updated soon !!</p>
                              </div>
                              <div className="modal-footer">
                                  <a href="#!" className="modal-close btn-flat"><i className="material-icons right">close</i>Close</a>
                              </div>
                            </div>

                     </div>
                 </div>
                <div className="col l6 hide-on-med-and-down center">
                    <img className="center imgf" src={Fhome} alt=""/>
                </div>
            </div>
            </React.Fragment>
        );
      }
    }
}
