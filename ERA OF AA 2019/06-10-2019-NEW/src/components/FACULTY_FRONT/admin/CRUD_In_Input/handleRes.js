import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import {} from 'materialize-css'
import M from 'materialize-css'
import AddProduct from './add';
import ProductList from './prod'
var empty = require('is-empty');

export default class Section extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      redirectTo:'',
      option:'',
      level:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}
handleLevel = (e) =>{
  this.setState({level: e.target.value})
}

componentDidMount(){
  M.AutoInit()
}


  render()
  {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <div className="row">
        <div className="col l4 xl4 s4 m4">
          <div className="row">
            <div className="col l6 xl6 s6 m6" style={{marginTop:'15px',color:'red'}}><b>Select Root Responsibility - </b></div>
            <div className="col l6 xl6 s6 m6">
              <select name="title" value={this.state.option} onChange={this.handleOption}>
              <option value="" disabled selected>Select Here...</option>
              <option value="Administrative">Administrative</option>
              <option value="Academic">Academic</option>
              <option value="Research">Research</option>
              </select>
            </div>
            </div>
        </div>
        <div className="col l1 xl1"/>
        <div className="col l4 xl4 m4 s4">
        <div className="row">
          <div className="col l6 xl6 s6 m6" style={{marginTop:'15px',color:'red'}}><b>Select Sub-Responsibility Under Root - </b></div>
          <div className="col l6 xl6 s6 m6">
        <select value={this.state.level} onChange={this.handleLevel}>
        <option value="" disabled selected>Designation</option>
        <option value="Director">Director</option>
        <option value="Principle">Principle</option>
        <option value="Assistant Director">Assistant Director</option>
        <option value="Dean">Dean</option>
        <option value="HOD">HOD</option>
        <option value="Professor">Professor</option>
        <option value="Associate Professor">Associate Professor</option>
        <option value="Assistant Professor">Assistant Professor</option>
        </select>
        </div>
        </div>
        </div>
        <div className="col l1 xl1 s4" />
        </div>
        <div className="row">
          <Gg options={this.state.option} level={this.state.level} username={this.state.username}/>
        </div>
      </React.Fragment>

    )
  }
  }
}

 class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{

}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
       addroute="/user/add_responsibility";
       editroute = "/user/edit_responsibility"
     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/user/edit_existing_responsibility"
     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm,title,description;

            var  data1 = {
               fielddata: [
                 {
                   header: "Overall Percentage for Root Responsibility",
                   name: "responsibilty_root_percentage",
                   placeholder: "Enter the Percentage of Root Responsibility",
                   type: "number",
                   grid: 2,
                   div:"col l3 s3 m3 xl3 center",
                 },
                 {
                   header: "Sub-Responsibility Under Root",
                   name: "responsibilty_title",
                   placeholder: "Enter the Title of Sub Responsibility",
                   type: "text",
                   grid: 2,
                   div:"col l3 s3 m3 xl3 center",
                 },
                 {
                   header: "Perentage For Sub-responsibilty",
                   name: "responsibilty_percentage",
                   placeholder: "Enter the Percentage for Sub-Responsibility",
                   type: "text",
                   grid: 2,
                   div:"col l3 s3 m3 xl3 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <AddProduct action={this.props.options} level={this.props.level}  data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div>
    {!this.state.isAddProduct && <ProductList action={this.props.options} level={this.props.level}  data={data1}  editProduct={this.editProduct}/>}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</React.Fragment>
);
}
}
}
