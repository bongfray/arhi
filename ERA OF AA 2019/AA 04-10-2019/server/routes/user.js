const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const io = require('socket.io')
const Nav = require('../database/models/FAC/nav_stat')
const User = require('../database/models/FAC/user')
const Admin = require('../database/models/FAC/admin')
const Prof = require('../database/models/FAC/profile1')
const DayOrder = require('../database/models/FAC/dayorder')
const DayHis = require('../database/models/FAC/dayorderhistory')
const Timetable = require('../database/models/FAC/time_table')
const BluePrint = require('../database/models/FAC/blueprint')
const Request = require('../database/models/FAC/req')
const AdminOrder = require('../database/models/FAC/admin_instruction')
const InsertIntoRes = require('../database/models/FAC/insert_responsibility')
const Five = require('../database/models/FAC/fiveyarplan')
const passport = require('../passport/fac_pass')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')
var server = require('http').createServer(express);
var socket = io.listen(server);


router.get('/active',function(req,res){
  AdminOrder.findOne({active:{"$exists": true}},function(err,yes){
    if(err)
    {

    }
    else if(yes)
    {
      res.send(yes.active)
    }
  })
})

var active =0;
socket.on('connect', function() {
  active++;
  const active = new AdminOrder({
    active: active,
  });
  active.save(() => {
  });
 });
/*------------------------------------------------------------------------Changing Day Orders------------------- */

let timer;

function nextGo(time)
{
    console.log("Way to fetch next DayOrder.....")
    timer = setTimeout(() =>
    {
    myFunc()
  },time);
}

DayOrder.findOne({day_order : {"$exists" : true} }, function(err, user){
  if(err)
  {
    res.status(404).send("Internal Error!!")
  }
  else if(user)
  {
    console.log("Server Started!!!");
    let hour = (new Date().getHours())*3600000;
    let minutes = (new Date().getMinutes())*60000;
    let second =(new Date().getSeconds())*1000;
    let total = hour+minutes+second;
    let max = (86398000-total);
    console.log(max);
    nextGo(max);
  }
 else
 {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  var day = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour + minutes + second;
  let time = (86398000-total);
  var day_order;
  var day_order_status;
  if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
  {
    day_order = 0;
    day_order_status = "weekend";
  }
  else
  {
    day_order = 1;
    day_order_status= "online";
  }
    const dayyy = new DayOrder({
      day_order: day_order,
      time: time,
      week: week,
      day_order_status:day_order_status,
      day:day,
      month:month,
      year:year,
      cancel_status: false,
    });
    dayyy.save(() => {
    });

    DayHis.findOne({day_order : {"$exists" : true}}, function(err, user){
      const dayhistory = new DayHis({
        day_order: day_order,
        day:day,
        month:month,
        year:year,
        day_order_status:day_order_status,
        week:week,
        cancel_status: false,
      });
      dayhistory.save(() => {
      });
    });
    nextGo(time);
 }
})

function myFunc()
{
    console.log("Updating Dayorder....")
      DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err)
        {
          res.status(404).send("Internal Error !!")
        }
        else if(user)
        {
          var yesday = Date.parse("yesterday").toString("dd");
          var day = Date.today().toString("dd");
          var month = Date.today().toString("M");
          var year = Date.today().toString("yyyy");
          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var today = new Date.today();
          var week =today.getWeek();

          if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
          {
            DayHis.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, user){
              const dayhistory = new DayHis({
                day_order: user.day_order,
                day:day,
                month:month,
                year:year,
                day_order_status:'weekend',
                week:week,
                cancel_status: user.cancel_status,
              });
              dayhistory.save((err, savedUser) => {
              });
            })
              nextGo(86398000);
          }
          else
          {

            var yesday = Date.parse("yesterday").toString("dd");
            var day = Date.today().toString("dd");
            var month = Date.today().toString("M");
            var year = Date.today().toString("yyyy");
            let prev = user.day_order;
            let recent = prev + 1;
            let next;
            let day_order_status;
            let hour = (new Date().getHours())*3600000;
            let minutes = (new Date().getMinutes())*60000;
            let second =(new Date().getSeconds())*1000;
            let total = hour + minutes + second;
            let max = (86398000-total);
            if(prev === 5)
            {
              next=1;
            }
            else
            {
              next = recent;
            }
              DayHis.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, prev_dayorder_history){
                if(err)
                {
                  res.status(404).send("Internal Error !!")
                }
                else if(!prev_dayorder_history)
                {
                  day_order_status = "online";
                  let renew_cancel_status = false;
                  update_without_complexity(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else if(prev_dayorder_history)
                {
                  day_order_status = "online";
                  let renew_cancel_status = false;
                 if(prev_dayorder_history.day_order_status === "weekend")
                {
                  updatedy(prev_dayorder_history.day_order,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else if(prev_dayorder_history.cancel_status === true)
                {
                  updatedy(prev_dayorder_history.day_order,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else
                {
                  update_without_complexity(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
              }

              })
            }
        }

      });

}

 function updatedy(prev_dayorder,day_order_status,day,month,year,max,week,cancel_status)
 {
   DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: prev_dayorder,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(prev_dayorder,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function update_without_complexity(prev_dayorder,today,day_order_status,day,month,year,max,week,cancel_status)
 {
   DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: today,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(today,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function dayorder_history_insert(day_order,day,month,year,week,day_order_status,cancel_status)
 {
   DayHis.findOne({day_order : {"$exists" : true}}, function(err, user){
     const dayhistory = new DayHis({
       day_order: day_order,
       day:day,
       month:month,
       year:year,
       day_order_status:day_order_status,
       week:week,
       cancel_status:cancel_status,
     });
     dayhistory.save(() => {
     });
     return;
   })
 }

function nextcall()
{
  logout();
    clearInterval(timer);
  DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
    nextGo(user.time);
  })
}

function logout()
{
  const url = 'mongodb://localhost:27017/eWork'
  MongoClient.connect(url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    }, function(err, client) {
    assert.equal(null, err);
    var cursor = client.collection('sessions').remove( { } );
  });
}
clearInterval(timer);




/* ---------------------------------------------Fetch Signup Page Render Status-------------------------*/
router.get('/fetch_render_status', function(req, res) {
  AdminOrder.findOne({usertype:'fac'}, function(err, result){
    if(err)
    {

    }
    else if(result)
    {
      res.send(result.registration_status)
    }
  })
})

/* -------------------------------------- College Cancellation Handle----------------------------------------------------------*/

router.post('/college_cancel', function(req, res) {
  const {reason,day,month,year,day_order} = req.body;
  DayHis.findOne({$and: [{day:day},{month:month},{year:year},{username:req.user.username}]}, function(err, user){
    if(user)
    {
      res.send("have");
      return;
    }
    else{
    const dayhistory = new DayHis({
      day_order_status:'cancel',
      username: req.user.username,
      day_order: day_order,
      day:day,
      month:month,
      year:year,
      reason_of_cancel:reason,
    });
    dayhistory.save((err, savedUser) => {
    });

    res.send("ok")
  }
  })
})

/* -----------------------------------------Fetching Request From Nav Bar ------------------------*/
  router.get('/fetchnav',function(req,res) {
    Nav.find({ }, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })
/*--------------------------------------To Set the color of the 10 Cards------------------ */

router.post('/fetchfrom', function(req, res) {
  console.log(req.body)
  const {slot_time,day,month,year} = req.body;
  Timetable.findOne({$and: [{day_slot_time : slot_time},{username: req.user.username},{date:day},{month:month},{year:year}]}, function(err, passed){
    res.send(passed);
  })
  });

/*------------------------To Render Slots in 10 Cards in Timetable---------------------------*/

  router.post('/fetchme', function(req, res) {
    BluePrint.findOne({$and: [{timing : req.body.slot_time},{username: req.user.username}]}, function(err, passed){
      res.send(passed);
    })
    });

/*---------------------------------------------------Today's Day Order Fetch----------------------------------- */
  router.get('/fetchdayorder', function(req, res) {
    if(!req.user)
    {
      res.send("Not")
    }
    else
    {
      var day = Date.parse("yesterday").toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      DayHis.findOne({$and: [{username: req.user.username},{day:day},{month:month},{year:year},{day_order_status:"cancel"}]}, function(err, user){
        if(err)
        {

        }
        else if(user)
        {
          var cancel ={
            cancel_day: user.day_order,
          }
          res.send(cancel);
        }
        else
        {
          DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
            if(err){

            }
            else if(user)
            {
                    var day_order ={
                      day_order: user.day_order,
                    }
                    res.send(day_order);

            }
          });
        }

        })
    }
    });
/*--------------------------------------------------YesterDay's DayOrder Fetch----------------------------------------------*/
router.post('/fetch_yesterday_dayorder', function(req, res) {
  DayHis.findOne({$and: [{username: req.user.username},{day:req.body.day},{month:req.body.month},{year:req.body.year}]}, function(err, started){
    if(err)
    {

    }
    else if(started)
    {
      var stock ={
        stock: user.day_order,
      }
      res.send(stock);
    }
    else
    {
      DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err){

        }
        else if(user)
        {
                var initial ={
                  initial: user.day_order,
                }
                res.send(initial);
        }
      });
    }

    })
  });


  /*----------------------------------------End---------------------------- */

    router.post('/fetchfromtimetable', function(req, res) {
      if(!req.user)
      {
        res.send("Not")
      }
      else
      {
      const {day_slot_time} = req.body;
                  BluePrint.findOne({$and: [{timing: day_slot_time},{username: req.user.username}]}, function(err, passed){
                    if(passed)
                    {
                      var alloted_slots ={
                        alloted_slots: passed.alloted_slots,
                      }
                      res.send(alloted_slots);
                    }
                    else{
                      var message ={
                        message: "There is no Saved Data for This Slot",
                      }
                      res.send(message);
                    }
                  })
          }
      });


router.post('/', (req, res) => {

    const { username, password,title,name,mailid,phone,campus,dept,desgn,dob,count} = req.body
    User.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
              suspension_status:req.body.suspension_status,
              username: username,
              password: password,
              title: title,
              name: name,
              mailid: mailid,
              phone: phone,
              campus: campus,
              dept: dept,
              desgn: desgn,
              dob: dob,
              count: count,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              blue_allot:0,
              total_adminispercentage:req.body.total_adminispercentage,
              total_academicpercentage:req.body.total_academicpercentage,
              total_researchpercentage:req.body.total_researchpercentage,
              ad_total_role_res:req.body.ad_total_role_res,
              ad_total_clerical:req.body.ad_total_clerical,
              ad_total_planning:req.body.ad_total_planning,
              academic_total_curricular:req.body.academic_total_curricular,
              academic_total_cocurricular:req.body.academic_total_cocurricular,
              academic_total_extracurricular:req.body.academic_total_extracurricular,
              academic_total_evaluation_placementwork:req.body.academic_total_evaluation_placementwork,
              research_total_publication:req.body.research_total_publication,
              research_total_ipr_patents:req.body.research_total_ipr_patents,
              research_total_funded_sponsored_projectes:req.body.research_total_funded_sponsored_projectes,
              research_total_tech_dev_consultancy:req.body.research_total_tech_dev_consultancy,
              research_total_product_development:req.body.research_total_product_development,
              research_total_research_center_establish:req.body.research_total_research_center_establish,
              research_total_research_guidnce:req.body.research_total_research_guidnce,
              ad_complete_role_res:0,
              ad_complete_clerical:0,
              ad_complete_planning:0,
              academic_complete_curricular:0,
              academic_complete_cocurricular:0,
              academic_complete_extracurricular:0,
              academic_complete_evaluation_placementwork:0,
              research_complete_publication:0,
              research_complete_ipr_patents:0,
              research_complete_funded_sponsored_projectes:0,
              research_complete_tech_dev_consultancy:0,
              research_complete_product_development:0,
              research_complete_research_center_establish:0,
              research_complete_research_guidnce:0,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})



router.post('/newd', (req, res) => {

    const {id,up_username,up_phone,up_name,up_dob} = req.body;
    console.log(req.body)
    User.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            // console.log("Successful Authen");
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            // console.log(hash);
            User.updateOne({username:req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              // console.log("Successful Hash");
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          User.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)

/*---------------------------------------------reseting password by mail ------------------------------- */
router.post('/reset_from_mail', (req, res) => {
  //console.log('user signup');

  const {token_come,password} = req.body;
  User.findOne({ resetPasswordToken: token_come }, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if(user) {
        console.log(user)
        let hash = bcrypt.hashSync(password, 10);
          User.updateOne({resetPasswordToken: token_come},{password: hash} ,(err, user) => {
            var succ= {
              succ: "Password Updated!!"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "failed"
          };
          res.send(fail);
        }
      })
    })


/*End of Signup------------------------------------------------------------------------------ */



/* -------------------------------Degree & Certificates--------------------------------------------------------------*/
router.post('/profile1', (req, res) => {
  console.log(req.body)
          var tran = new Prof(req.body.data);
          tran.save((err, savedUser) => {
            var succ= {
              succ: "Successfully Submitted"
            };
            res.send(succ);
          })
})



router.post('/editprofile1', function(req,res) {
  Prof.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Edited"
    };
    res.send(succ);
  })

});


router.post('/fetchprofile1',function(req,res) {
Prof.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/fetchtoedit',function(req,res) {
Prof.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
// console.log(docs)
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/profile1del',function(req,res) {
  Prof.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
})


/*End of PROFILE1-------------------------------------------------------------------------------- */

router.post(
    '/faclogin',(req, res, next) => {
      console.log(req.body)
      const { username, password} = req.body;
      User.findOne({ username: username }, function(err, objs){
        if(err)
        {
          console.log(err)
        }
        else if(objs)
        {
         if (objs.count === 0)
          {
              User.updateOne({ username: username }, { $inc: { count: 1 }},(err, user) => {
              })

          }
          else if (objs.count === 1)
          {
              User.updateOne({ username: username }, { count: 2 },(err, user) => {

              })
          }
          else if (objs.count === 4)
          {
              User.updateOne({ username: username }, { count: 4 },(err, user) => {

              })
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)



router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})


router.get('/knowcount', function(req, res) {
    const { username} = req.user;
    if(req.user)
    {
    User.findOne({ username: username }, function(err, objs){

        if (objs)
        {
            res.send(objs);
        }
    });
  }
  else{
    return;
  }
    });


router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })
})


router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

router.get('/dash2', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    User.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});

    router.post('/timeallot', (req, res) => {
        const { day_slot_time, selected,day_order, usern,order, saved_slots,problem_statement,cday,cmonth,cyear,compdayorder,compslot,covered,date,month,year,week} = req.body;
              Timetable.findOne({$and: [{day_slot_time: day_slot_time},{username: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
                  if (err)
                  {
                      return;
                  }
                  else if(objs)
                  {

                  }
                  else if (!objs) {
                            const newTime = new Timetable({
                              date: date,
                              month: month,
                              year: year,
                              username: req.user.username,
                              day_order: day_order,
                              day_slot_time:day_slot_time,
                              order: order,
                              selected:selected,
                              problem_statement: req.body.problem_statement,
                              problem_compensation_date: req.body.compday,
                              problem_compensation_month: req.body.compmonth,
                              problem_compensation_year: req.body.compyear,
                              problem_compensation_dayorder: req.body.compdayorder,
                              problem_compensation_slot: req.body.compslot,
                              covered: req.body.covered,
                              week:week,
                            })
                            newTime.save((err, savedUser) => {
                              var succ= {
                                succ: "Success"
                              };
                              res.send(succ);
                            });
                            User.findOne({username:req.user.username}, (err, user) => {
                              User.updateOne({ username: req.user.username }, {academic_complete_curricular:user.academic_complete_curricular+1},(err, user) => {
                              })
                           })
                          }
            });

  })


    router.post('/timefree', (req, res) => {
        const { day_slot_time,freeslot,freefield, order,date,month,year,week } = req.body
        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{username: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
            if (err)
            {

            }
            else if (objs)
            {

            }
            else if(!objs) {
                if((req.body.freeparts==="ad_total_clerical"))
                {

                  User.findOne({$and: [{username: req.user.username},{ad_total_clerical : {"$exists" : true}}]}, (err, user) => {

                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.ad_complete_clerical===user.ad_total_clerical))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { ad_complete_clerical: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ ad_complete_clerical: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })
                }
                else if(req.body.freeparts==="ad_total_role_res")
                {
                  User.findOne({$and: [{username: req.user.username},{ad_total_clerical : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.ad_complete_role_res===user.ad_total_role_res))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { ad_complete_role_res: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ ad_complete_role_res: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })
                }
                else if(req.body.freeparts==="ad_total_planning")
                {
                  User.findOne({$and: [{username: req.user.username},{ad_total_clerical : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {

                      if(user.week === req.body.week)
                      {
                      if((user.ad_complete_planning===user.ad_total_planning))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { ad_complete_planning: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ ad_complete_planning: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })
                }
                else if(req.body.freeparts==="academic_total_cocurricular")
                {
                  User.findOne({$and: [{username: req.user.username},{academic_total_cocurricular : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.academic_complete_cocurricular===user.academic_total_cocurricular))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { academic_complete_cocurricular: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ academic_complete_cocurricular: 1 },(err, user) => {
                     })
                     updateweek()
                   }


                   }
                  })

                }
                else if(req.body.freeparts==="academic_total_extracurricular")
                {
                  User.findOne({$and: [{username: req.user.username},{academic_total_extracurricular : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.academic_total_extracurricular===user.academic_complete_extracurricular))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { academic_complete_extracurricular: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ academic_complete_extracurricular: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })

                }
                else if(req.body.freeparts==="academic_total_evaluation_placementwork")
                {
                  User.findOne({$and: [{username: req.user.username},{academic_total_evaluation_placementwork : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.academic_complete_evaluation_placementwork===user.academic_total_evaluation_placementwork))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { academic_complete_evaluation_placementwork: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ academic_complete_evaluation_placementwork: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })
                }
                else if(req.body.freeparts==="research_total_publication")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_publication : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_total_publication===user.research_complete_publication))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_publication: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_publication: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })
                }
                else if(req.body.freeparts==="research_total_ipr_patents")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_ipr_patents : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {


                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_ipr_patents===user.research_total_ipr_patents))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_ipr_patents: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_ipr_patents: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })

                }
                else if(req.body.freeparts==="research_total_funded_sponsored_projectes")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_funded_sponsored_projectes : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {


                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_funded_sponsored_projectes===user.research_total_funded_sponsored_projectes))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_funded_sponsored_projectes: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_funded_sponsored_projectes: 1 },(err, user) => {
                     })
                     updateweek()
                   }


                   }
                  })

                }
                else if(req.body.freeparts==="research_total_tech_dev_consultancy")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_tech_dev_consultancy : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_tech_dev_consultancy===user.research_total_tech_dev_consultancy))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_tech_dev_consultancy: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_tech_dev_consultancy: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })

                }
                else if(req.body.freeparts==="research_total_product_development")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_product_development : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_total_product_development===user.research_complete_product_development))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_product_development: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_product_development: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })
                }
                else if(req.body.freeparts==="research_total_research_center_establish")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_research_center_establish: {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_research_center_establish===user.research_total_research_center_establish))
                      {
                        var suc= {
                          suc: "Field,you are going to submit has already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_research_center_establish: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ research_complete_research_center_establish: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })

                }
                else if(req.body.freeparts==="research_total_research_guidnce")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_research_guidnce : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                     if(user.week === req.body.week)
                      {
                        if((user.research_complete_research_guidnce===user.research_total_research_guidnce))
                        {

                          var suc= {
                            suc: "Field,you are going to submit has already reached to it's max!!"
                          };
                          res.send(suc);
                        }
                        else{
                          send()
                          User.updateOne({ username: req.user.username },{ $inc: { research_complete_research_guidnce: 1 }},(err, user) => {
                          })
                        }
                     }
                    else
                    {
                      send()
                      User.updateOne({ username: req.user.username },{ research_complete_research_guidnce: 1 },(err, user) => {
                      })
                    }
                   }
                  })

                }

                function updateweek()
                {
                  User.updateOne({ username: req.user.username },{ week: req.body.week },(err, user) => {
                  })
                }
                function ressend()
                {
                  var not ={
                    not:"This Field is not alloted for you. Kindly Check Dashboard !!",
                  };
                  res.send(not);
                }
                function send()
                {
                const newTime = new Timetable({
                  date: date,
                  month: month,
                  year: year,
                  username: req.user.username,
                  day_slot_time:day_slot_time,
                  order: order,
                  freefield: freefield,
                  freeslot: freeslot,
                  freeparts: req.body.freeparts,
                  week:week,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                });
              }


            }
        })
    })



    router.post('/timecancel', (req, res) => {
        const { day_slot_time,usern, order, compensation_status,c_cancelled,date,month,year,week } = req.body
        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{username: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
            if (err)
            {

            }
            else if(objs)
            {

            }
            else if(!objs)
            {
                const newTime = new Timetable({
                  date: date,
                  month: month,
                  year: year,
                  username: req.user.username,
                  day_slot_time:day_slot_time,
                  order: order,
                  c_cancelled: c_cancelled,
                  compensation_status: compensation_status,
                  week:week,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                })
          }
      })
    })


    router.post('/forgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       User.findOne(
         {mailid: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'INVALID USER !!'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           // console.log(token);
           User.updateOne({mailid:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 1800000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Reset Password:  eWork', // Subject line
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 30min from now.</p>'
          };
          // console.log("Way to enter.......");
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              // console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })
    })


        router.get('/reset_password', function(req, res) {
          User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
            if (!user) {
              console.log("Expired");
              var expire ={
                expire:"Link is Expired"
              }
              res.send(expire);
            }
            else if(user){
              res.send(user.resetPasswordToken)
            }

          });
        });


/*------------------------Blue Print Send-------------------------------------------------- */
router.post('/send_blue_print', (req, res) => {
    const {alloted_slots,timing} = req.body
    User.findOne({ username: req.user.username}, (err, user) => {
      if(err)
      {

      }
      else if(user)
      {
        if(user.blue_allot === user.academic_total_curricular)
        {
          var exceed = {
            exceed: "No of the maximum input reached!!",
          };
            res.send(exceed);
        }
        else
        {

        BluePrint.findOne({$and: [{timing: timing},{username: req.user.username}]}, (err, objs) => {
          if(err)
          {

          }
          else if(objs)
          {
            var emsg = {
              emsg: "We have datas saved on the following Time !!",
            };
              res.send(emsg);
          }
          else{
            const allottime = new BluePrint({
              alloted_slots: alloted_slots,
              timing: timing,
              username: req.user.username,

            })
            allottime.save((err, savedUser) => {
              var succ= {
                succ: "Success"
              };
              res.send(succ);
            })
            User.updateOne({ username: req.user.username},{blue_allot: user.blue_allot+1}, (err, user) => {
            })
          }
        })
      }
      }
    })
  })


/*--------------------------------Getting Percentages of Individual ------------------------------------------------- */
router.get('/getPercentage', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       res.send(user);
     }
   })
  }
})

/*----------------------------------------------------------Master Table------------------------------------------ */

router.post('/fetchmaster', function(req, res) {
  const {slot_time} = req.body;
  Timetable.findOne({$and: [{day_slot_time : slot_time},{username: req.user.username},{week:req.body.week}]}, function(err, passed){
    res.send(passed);
  })
  });

/*--------------------------------------------CRUD CODE--------------------------------------------- */
  router.post('/addmm', function(req,res) {
            var trans = new Admin(req.body.data);
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
  });

function resetAdmincount()
{
  Admin.nextCount(function(err, count) {

  Admin.resetCount(function(err, nextCount) {
    return;
  });

});
}

    router.post('/editt', function(req,res) {
      Admin.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
        var succ= {
          succ: "Successful SignedUP"
        };
        res.send(succ);
      })

    });


router.post('/fetchall',function(req,res) {
    Admin.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
  });
})

router.post('/fetcheditdata',function(req,res) {
  Admin.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/del',function(req,res) {
  Admin.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
});

router.post('/reset',function(req,res) {
  Admin.findOne({serial : {"$exists" : true}}, function(err, passed){
    if(err)
    {
      console.log(err)
    }
    else if(passed)
    {
      var notallowed ={
        notallowed:'Sorry Already Fields are there!!'
      }
      res.send(notallowed)
    }
    else if(!passed)
    {
      resetAdmincount()
      var succ={
        succ:'RESET DONE...'
      }
      res.send(succ)
    }
  })
})

/*------------------------------------------------Code For Manage Page----------------------------------- */
router.post('/request_for_entry',function(req,res) {
  DayHis.findOne({$and: [{day:req.body.day},{month: req.body.month},{year: req.body.year},{day_order:req.body.day_order},{day_order_status: 'online'}]}, function(err, present){
    if(err)
    {
      console.log(err)
    }
    else if(present)
    {
      Request.findOne({$and: [{username:req.user.username},{day:req.body.day},{month: req.body.month},{year: req.body.year}]}, function(err, passed){
        if(err)
        {
          console.log(err)
        }
        else if(passed)
        {
          var handled;
          if(passed.action === "approved")
          {
          handled= {
            handled: "Request Already Approved !!"
          };
          res.send(handled);
          }
          else
          {
            handled= {
              handled: "Admin Has Not Approved Till Now!!"
            };
            res.send(handled);
          }
        }
        else{
          var requests = new Request(req.body);
          requests.save((err, savedUser) => {
            var succ= {
              succ: "We have sent your request!!"
            };
            res.send(succ);
          })
        }
      })
    }
    else
    {
      var noday=
      {
        noday:"Invalid Request!!"
      }
      res.send(noday)
    }
  })
})


router.get('/fetch_requested_list',function(req,res) {
  Request.find({username: req.user.username}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
  else
  {
    res.send(docs)
  }
 });
})


router.post('/fetch_requested_status',function(req,res) {
  Request.findOne({$and:[{username: req.user.username},{serial:req.body.serial}]}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
  else if(docs)
  {
    if(docs.action ==="approved")
    {
      res.send("approved")
    }
    else if(docs.action ==="denied")
    {
      res.send("denied")
    }
    else if(docs.action ==="false")
    {
      res.send("pending")
    }
  }
  else
  {

  }
});
})


/*----------------------------------------Five Year Plan--------------------------------------------- */


function knowstat(user,body)
{
  var year = Date.today().toString("yyyy");
  var year_int = parseInt(year);
  Five.find({$and: [{username:user},{expired:false},{expire_year:year_int}]},function(err,docs){
    if(docs)
    {
      Five.updateMany({$and :[{username:user},{expire_year: year_int}]},{expired: true},function(err,done){
        if(done)
        {
        console.log("done")
        }
        return;
      })
    }
  })
}
    router.post('/add_five_year_plan', function(req,res) {
              var fives = new Five(req.body);
              fives.save((err, savedUser) => {
                console.log("save")
                res.send("Saved");
              })
    });




          router.post('/edit_five_year_plan', function(req,res) {
            Five.updateOne({$and :[{ serial: req.body.serial},{username: req.user.username}]},req.body, (err, user) => {
              var succ= {
                succ: "Updated"
              };
              res.send(succ);
            })

          });


      router.post('/fetch_five_year_plan',function(req,res) {
        knowstat(req.user.username,req.body);
        Five.find({$and: [{action: req.body.action},{username: req.user.username},{expired:false}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
      });
      })


      router.post('/fetch_five_year_existing_data',function(req,res) {
        console.log(req.body)
        Five.findOne({$and:[{serial: req.body.id},{username:req.user.username}]}, function(err, docs){
          // console.log(docs)
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
      });
      })

      router.post('/del_five_year_plan',function(req,res) {
        Five.remove({$and: [{serial:  req.body.serial },{username:req.user.username}]},function(err,succ){
          res.send("OK")
        });
      })

      /*-------------------------------------Declare a Holiday---------------------------------------------- */

      router.post('/declare_cancel_or_holiday',function(req,res) {
        var next_dayorder;
        console.log(req.body)
        DayOrder.findOne({$and: [{day:req.body.cday},{month: req.body.cmonth},{year:req.body.cyear},{cancel_status: false}]}, function(err, result){
           if(err)
           {
             return;
           }
           else if(result)
           {
             if(result.day_order === 1)
             {
               next_dayorder = 5;
             }
             else
             {
               next_dayorder = result.day_order - 1;
             }
                DayOrder.updateOne({day_order: result.day_order},{ $set: { day_order: next_dayorder,cancel_status: true}} ,(err, newdata) => {
                })
                DayHis.updateOne({day_order: result.day_order},{ $set: { day_order: next_dayorder,cancel_status: true}} ,(err, newdata) => {
                })

            res.send("Updated!!")
           }
           else if(!result)
           {
             res.send("It's Already Updated!!")
           }
        })

      })














/*----------------------------------------------------------Start of Admin Operation----------------------------- */

/*----------------------------------------------Admin Operation--------------------------------------------------------*/
router.post('/auth', (req, res) => {
  const {id,username,password} = req.body;

   if(req.body.id === "2019")
   {
     User.findOne({username:username}, (err, user) => {
       if(user)
       {
         var existadmin ={
           existadmin:'You already have an account !!',
         }
         res.send(existadmin);
       }
       else
       {
         const newadmin = new User({
           suspension_status:false,
           username:username,
           password: password,
           count: 4,
           resetPasswordExpires:'',
           resetPasswordToken:'',
         })
         newadmin.save((err, savedUser) => {
           var succ= {
             succ: "Successful SignedUP"
           };
           res.send(succ);
         })
       }
     })
  }
  else
  {
    var wrong ={
      wrong:'Passcode is Wrong !!'
    }
    res.send(wrong)
  }
  })







    router.post('/addnav', function(req,res) {
              var trans = new Nav(req.body.data);
              trans.save((err, savedUser) => {
                var succ= {
                  succ: "Done"
                };
                res.send(succ);
              })
    });


      router.post('/editnav', function(req,res) {
        Nav.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
          var succ= {
            succ: "Updated"
          };
          res.send(succ);
        })

      });


  router.post('/fetchnav',function(req,res) {
    Nav.find({action: req.body.action}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })


  router.post('/edit_existing_nav',function(req,res) {
    Nav.findOne({serial: req.body.id}, function(err, docs){
      // console.log(docs)
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

  router.post('/delnav',function(req,res) {
    Nav.remove({serial:  req.body.serial },function(err,succ){
      res.send("OK")
    });
  })


  /*---------------------------------------CRUD FOR SECTION CODE______________________________________________________________ */
  router.post('/add_section', function(req,res) {
    var trans = new InsertIntoRes(req.body.data);
    trans.save((err, savedUser) => {
      var succ= {
        succ: "Done"
      };
      res.send(succ);
    })
});


router.post('/edit_section', function(req,res) {
InsertIntoRes.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
var succ= {
  succ: "Updated"
};
res.send(succ);
})

});


router.post('/fetch_section',function(req,res) {
InsertIntoRes.find({action: req.body.action}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})


router.post('/edit_existing_section',function(req,res) {
InsertIntoRes.findOne({serial: req.body.id}, function(err, docs){
// console.log(docs)
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/del_section',function(req,res) {
InsertIntoRes.remove({serial:  req.body.serial },function(err,succ){
res.send("OK")
});
})


/*--------------------------------------CRUD SECTION ENDS*/




  router.get('/getrequest',function(req,res) {
    Request.find({action: "false"}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

    router.get('/fetch_denied_list',function(req,res) {
      Request.find({action: "denied"}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
    });
    })


  router.post('/approve_request',function(req,res) {
    console.log(req.body)
    Timetable.findOne({$and:[{usern:req.body.username},{date:req.body.day},{month: req.body.month},{year: req.body.year}]}, function(err, docs){
    if(err)
    {

    }
    else if(docs)
    {

      res.send("yes")
    }
    else
    {
      res.send("no")
    }
  });
  })



    router.post('/denyrequest',function(req,res) {
      Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},{action: "denied"},function(err,done){
          res.send("Done")
        })
      }
    });
    })



    router.post('/approverequest',function(req,res) {

    Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},{action: "approved"},function(err,done){
          res.send("Done")
        })
      }
    });
    })


    router.post('/handleFaculty_Registration',function(req,res) {

    AdminOrder.findOne({usertype: req.body.history}, function(err, docs){
      if(err)
      {

      }
      else if(docs)
      {
        AdminOrder.updateOne({usertype:req.body.history},{registration_status: req.body.checked},function(err,done){
          res.send("Done")
        })
      }
      else{
        var trans = new AdminOrder({
          usertype:req.body.history,
          registration_status: req.body.checked
        }
        );
        trans.save((err, savedUser) => {
          var succ= {
            succ: "Done"
          };
          res.send(succ);
        })
      }
    })
    })



    router.get('/fac_reg_status', (req, res, next) => {
      AdminOrder.findOne({usertype:'fac'},function(err,result){
        if(err)
        {

        }
        else if(result)
        {
          res.send(result.registration_status)
        }
      })
    })

    /*----------------------------------Admin Operation for opening a set of dayorder availbale to everyone----------------- */

    router.post('/show_a_span_of_dayorder_to_all_user',function(req,res) {
      // console.log(req.body.startday)
      // console.log(req.body.endday)
      // var arr1 = new Array();
      // DayHis.find({$and:[{ day: { $gt: req.body.startday} },{ day: { $lt: req.body.endday}},{day_order_status:'online'}]},function(err,matched){
      //   if(err)
      //   {
      //     console.log(err)
      //   }
      //   else if(matched)
      //   {
      //     res.send("Can't Proceed With this Request!! Because requested DayOrder Range Having DayOrder Saved in it!! We can't take this request !! Kindly verify and put datas accordingly")
      //   }
      //   else
      //   {
      //
      //   }
      // })
    })


    /*-----Validate Admin---------------------------------------- */
    router.post('/checkadmin',function(req,res){
      User.findOne({username:req.user.username},function(err,found){
        if(err)
        {

        }
        else if(found)
        {
          if(bcrypt.compareSync(req.body.password, found.password)) {
            var succ ={
              succ:"Matched"
            }
            res.send(succ)
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!found)
        {

        }
      })
    })
 /*----------------------------------------Delete User ------------------------------------------------------ */

 router.post('/deleteadmin',function(req,res){
   User.findOne({username:req.body.username},function(err,found){
     if(err)
     {

     }
     else if(found)
     {
       User.deleteOne({username:found.username},function(err,done)
       {
         if(err)
         {

         }
         else if(done)
         {
          res.send("Deleted")
         }
       })
     }
   })
 })


 router.post('/deleteuser',function(req,res){
  User.findOne({username:req.body.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      User.deleteOne({username:found.username},function(err,done)
      {
      })
      Request.deleteMany({username:found.username},function(err,done)
      {
      })
      Admin.deleteMany({username:found.username},function(err,done)
      {
      })
      Timetable.deleteMany({username:found.username},function(err,done)
      {
      })
      Five.deleteMany({username:found.username},function(err,done)
      {
      })
      Prof.deleteMany({username:found.username},function(err,done)
      {
      })
      BluePrint.deleteMany({username:found.username},function(err,done)
      {
      })
      res.send("Deleted")
    }
  })
})


router.post('/handle_change_today_dayorder',function(req,res){
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      DayOrder.findOne({$and:[{day:day},{month:month},{year:year}]}, function (err, doc){
        if(err)
        {

        }
        else if(doc)
        {
          doc.day_order = req.body.day_order;
          doc.save();
          res.send("DayOrder Updated!!")
        }
        else{
          res.send("Something went wrong !!")
        }
      })
      DayHis.findOne({$and:[{day:day},{month:month},{year:year}]}, function (err, doc){
        if(err)
        {

        }
        else if(doc)
        {
        doc.day_order = req.body.day_order;
        doc.save();
        }
      })
    }
  })
})


router.post('/handle_start_of_semester',function(req,res){
  console.log(req.body.startday)
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      AdminOrder.findOne({order:'Start_Of_Semester'},function(err,foud)
      {
        if(err)
        {

        }
        else if(foud)
        {
          AdminOrder.update({order:'Start_Of_Semester'},{$set:{startday:req.body.startday,startmonth:req.body.startmonth,startyear:req.body.startyear}},function(err,data){
            res.send("Date is Updated !!")
          })
        }
        else
        {
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Saved Into the DB !!");
          })
        }
      })
    }
  })
})

router.post('/handle_end_of_semester',function(req,res){
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      AdminOrder.findOne({order:'End_Of_Semester'},function(err,foud)
      {
        if(err)
        {

        }
        else if(foud)
        {
          AdminOrder.update({order:'End_Of_Semester'},{$set:{endday:req.body.endday,endmonth:req.body.endmonth,endyear:req.body.endyear}},function(err,data){
            res.send("Date is Updated !!")
          })
        }
        else
        {
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Saved Into the DB !!");
          })
        }
      })
    }
  })
})

router.get('/fetch_start_date',function(req,res){
      AdminOrder.findOne({order:'Start_Of_Semester'},function(err,found)
      {
        if(err)
        {

        }
        else if(found)
        {
          res.send(found)
        }
        else
        {

        }
      })
})


router.get('/fetch_end_date',function(req,res){
  AdminOrder.findOne({order:'End_Of_Semester'},function(err,found)
  {
    if(err)
    {

    }
    else if(found)
    {
      res.send(found)
    }
    else
    {

    }
  })
})
router.post('/delete_start_of_sem',function(req,res){
  AdminOrder.remove({order:'Start_Of_Semester'},function(err,succ){
    res.send("Deleted !!")
  });
})

router.post('/delete_start_of_sem',function(req,res){
  AdminOrder.remove({order:'End_Of_Semester'},function(err,succ){
    res.send("Deleted !!")
  });
})

/*-----------------------------------------------Update Operations---------------------------------------------------------------*/
router.post('/update_faculty_password',function(req,res){
  console.log(req.body)
  User.findOne({username:req.body.faculty_username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      let hash = bcrypt.hashSync(req.body.faculty_password, 10);
      User.updateOne({username:req.body.faculty_username},{password: hash} ,(err, done) => {
        if(err)
        {

        }
        else if(done)
        {
          res.send("Updated");
        }
      })
    }
    else
    {
      res.send("User Not Found !!")
    }
  })
})
module.exports = router
