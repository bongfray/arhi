const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');


const adminorder = new Schema({
  usertype: {type: String , unique: false, required: false},
  order:{type: String , unique: false, required: false},
  action:{type: String , unique: false, required: false},
  endday:{type: Number , unique: false, required: false},
  endmonth:{type: Number , unique: false, required: false},
  endyear:{type: Number , unique: false, required: false},
  startday:{type: Number , unique: false, required: false},
  startmonth:{type: Number , unique: false, required: false},
  startyear:{type: Number , unique: false, required: false},
  registration_status:{type: Boolean, unique: false, required: false},
  active: {type: Number, unique: false, required: false},
  victim_username: {type: String, unique: false, required: false},
  reason: {type: String, unique: false, required: false},
  suspend_status: {type: Boolean, unique: false, required: false},
  designation_name:{type: String, unique: false, required: false},
  designation_order_no:{type: Number, unique: false, required: false},
})

adminorder.plugin(autoIncrement.plugin, { model: 'Admin', field: 'serial', startAt: 1,incrementBy: 1 });



var Admin = mongoose.model('Admin-Order', adminorder);

module.exports = Admin
