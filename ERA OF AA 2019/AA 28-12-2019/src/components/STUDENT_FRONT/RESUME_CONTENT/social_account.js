import React, { Component } from 'react'
import axios from 'axios'
import AddProduct from './add_datas';
import ProductList from './show_info'


 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,
       disabled:'',

     }
     this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentDidMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/user2/editt';
     } else {
       apiUrl = '/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }

  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }
   render() {
     console.log(this.state)
     let productForm,title,description;
     var data;
             title ='Social Media Account(like Linkdin)';
             data = {
               Action:'Social Media Account',
               button_grid:'col l1 m1 s1 center',
               fielddata: [
                 {
                   header: "Blog Link",
                   name: "blog_link",
                   placeholder: "Enter Your Blog link",
                   type: "text",
                   grid:'col l2 m2 s2 center',
                 },
                 {
                   header: "Git Hub Profile",
                   name: "github_link",
                   placeholder: "Enter Your Git Hub link",
                   type: "text",
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "Play Store Developer A/c (Public Link):",
                   name: "playstore_link",
                   placeholder: "Enter the Link of Play Store Developer A/c (Public Link)",
                   type: "text",
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "Linkedln Link",
                   name: "linkedln_link",
                   placeholder: "Enter the Linkedln Link",
                   type: "text",
                   grid:'col l1 m1 s1 center',
                 },
                 {
                   header: "Hacker Rank Link",
                   name: "hackerrank_link",
                   placeholder: "Enter the Hacker Rank Link",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "Hacker Earth Link",
                   name: "hackerearth_link",
                   placeholder: "Enter the Hacker Earth Link",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
                 {
                   header: "Other Link",
                   name: "other_link",
                   placeholder: "Enter the Other Link",
                   type: "text",
                   grid: 'col l2 m2 s2 center',
                 },
               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct description={description} cancel={this.updateState} username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} action={this.props.options} data={data}  editProduct={this.editProduct}/>}<br />
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6" />
     <div className="col l6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
      </div>
    </div>
    </React.Fragment>
    }
    { productForm }
    <br/>
  </div>

);
}
}
