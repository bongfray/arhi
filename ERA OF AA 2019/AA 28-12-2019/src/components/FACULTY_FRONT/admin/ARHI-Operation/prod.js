import React from 'react';
import axios from 'axios';
import Load from '../loader_simple'
import ValidateUser from '../validateUser'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      success:'',
      index:'',
      modal:false,
      disabled:'',
      allowed:false,
      error: null,
      products: [],
      action:'',
      loading:true,
      value_for_search:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch(this.props.action)
  }
  openModal()
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidUpdate =(prevProps,prevState) => {
    if ((prevProps.action !== this.props.action) ||(prevProps.level !== this.props.level)) {
      this.fetch(this.props.action);
    }
    if (prevState.allowed !== this.state.allowed) {
      this.openModal();
      this.deleteProduct(this.state.id,this.state.index)
    }
  }

  showDiv =(userObject) => {
    this.setState(userObject)
  }
  deleteProduct = (productId,index) => {
    this.setState({modal: !this.state.modal,disabled:'disabled',id:productId,index:index})
    if(this.state.allowed === true)
    {
      this.deleteOperation(productId,index);
    }
}
deleteOperation= (productId,index) =>{
  var delroute;
 if((index === "Skill-For-Student")){
   delroute = "/user2/del_inserted_data";
  }
  else{
    delroute = "/user2/del_inserted_data";
   }

const { products } = this.state;
axios.post(delroute,{
  serial: productId,
})
    .then(response => {
      this.setState({
        disabled:'',
        success:'none',
        response: response,
        products: products.filter(product => product.serial !== productId)
     })
    })
}


fetch =(action) =>{
  let fetchroute;
 if((action === "Skill-For-Student"))
  {
    fetchroute = "/user2/fetch_in_admin";
  }
  else
  {
    fetchroute = "/user2/fetch_in_admin";
  }

  axios.post(fetchroute,{
    action: this.props.action,
    usertype: this.props.level,
  })
  .then(response =>{
    this.setState({
    loading:false,
     products: response.data,
   })
  })
}

searchEngine =(e)=>{
  this.setState({value_for_search:e.target.value})
}

  render() {
        const {products} = this.state;

    var libraries = products,
    searchString = this.state.value_for_search.trim().toLowerCase();
    if(searchString.length > 0)
    {
      libraries = libraries.filter(function(i) {
        return i.skill_name.toLowerCase().match( searchString );
      });
    }



    if(this.state.loading)
    {
      return(
        <div>
         <Load />
        </div>
      );
    }
    else{
      return(
        <div>
          <div style={{display:this.state.success}}>
        <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
        </div>
            {!(this.props.action ==="Skill-For-Student") ? <div className="row">
                <div className="col s1 l1 m1 xl1 center"><b>Serial No</b></div>
                {this.props.data.fielddata.map((content,index)=>(
                  <div className={content.div} key={index}><b>{content.header}</b></div>
                ))}
                <div className="col s2 l2 xl2 m2 center"><b>Action</b></div>
            </div>
            :
            <div>
                <div className="search-wrapper">
                  <div className="row">
                     <div className="col l11">
                        <input id="search" style={{borderTop:'1px black'}} placeholder="Search" value={this.state.value_for_search} onChange={this.searchEngine}/>
                     </div>
                     <div className="col l1">
                        <i className="material-icons">search</i>
                     </div>
                  </div>
                </div>
            </div>
          }
            <hr />




            {this.props.action==="Skill-For-Student" ?
                <div className="row">
                {libraries.map((product,index) => (
                     <div key={product.serial} className="col l3 xl3 m4 s6 card" style={{padding:'10px'}}>
                         <div className="row">
                             <div className="col l8 xl8 m8 s6 center">
                               {this.props.data.fielddata.map((content,index)=>(
                                 <div className={content.div} key={index} style={{whiteSpace: 'pre-line'}}>{product[content.name]}</div>
                               ))}
                             </div>
                             <div className="col s6 l4 xl4 m4 center">
                             <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                             &nbsp;<i className="material-icons go" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                             </div>
                         </div>
                     </div>
                  ))}
                </div>
                :
                 <React.Fragment>
                 {libraries.map((product,index) => (
                   <React.Fragment key={index}>
                   <div className="row">
                    <div className="col l1 xl1 m1 s1 center">{index+1}</div>
                    {this.props.data.fielddata.map((content,index)=>(
                      <div className={content.div} key={index} style={{whiteSpace: 'pre-line'}}>{product[content.name]}</div>
                    ))}
                    <div className="col s2 l2 xl2 m2 center">
                    <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                    &nbsp;<i className="material-icons go" disabled={this.state.disabled} onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                    </div>
                  </div>
                  <hr />
                  </React.Fragment>
              ))}
              </React.Fragment>
              }
              </div>
      )
    }

  }
}
