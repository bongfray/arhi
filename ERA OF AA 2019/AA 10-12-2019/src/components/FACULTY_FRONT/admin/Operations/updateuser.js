import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import ValidateUser from '../validateUser'

export default class UpdateUser extends Component{
    constructor(){
        super();
        this.initialState={
            modal:false,
            allowed:false,
            disabled:'',
            faculty_username:'',
            faculty_password:'',
            student_username:'',
            student_password:'',
            startday:'',
            endday:'',
            startmonth:'',
            endmonth:'',
            startyear:'',
            endyear:'',
            value:'',
            response:'',
        }
        this.state = this.initialState;
        this.componentDidMount = this.componentDidMount.bind(this)
    }

handleAddAccess_To_DayOrder = (e) =>{
  this.setState({
    [e.target.name]:e.target.value,
  })
}
handleUpdate = (e) =>{
    this.setState({
        [e.target.name]: e.target.value
    })
}

componentDidMount()
{

}

componentDidUpdate =(prevProps,prevState) => {
  if (prevProps.action !== this.props.action) {
    this.fetch(this.props.action);
  }
  if (prevState.allowed !== this.state.allowed) {
    this.update()
  }
}

showDiv =(userObject) => {
  this.setState(userObject)
}

    update = (e)=>{

      this.setState({modal: !this.state.modal,disabled:'disabled'})
      if(this.state.allowed === true)
      {
        var route;
        this.setState({success:'none',disabled:''})
        if(this.props.select === "facpwd")
        {
          route = "/user/update_faculty_password";

        }
        else if(this.props.select === "studpwd")
        {
          route ="/user2/update_student_password";
        }
        this.UpdateHere(route);
      }
    }


  handleAllUser_DayOrder_Add = (e) =>{
    e.preventDefault();
    axios.post('/user/show_a_span_of_dayorder_to_all_user',this.state)
    .then(response=>{
      this.setState({response: response.data})
    })
  }

  UpdateHere = (route)=>{
    axios.post(route,this.state)
    .then(res=>{
      this.setState({disabled:'',dept_admin_password:'',dept__admin_username:'',faculty_password:'',faculty_username:'',student_password:'',student_username:''})
      window.M.toast({html: res.data,outDuration:'1000', classes:'rounded #ec407a pink lighten-1'});
    })
  }
    render(){
      var button;
      if(this.props.select === 'facpwd'){
          button =
          <div>
          <div className="input-field col s6">
          <input id="uusername" type="text" className="validate" name="faculty_username" value={this.state.faculty_username} onChange={this.handleUpdate} required />
          <label htmlFor="uusername">Username</label>
          </div>

          <div className="input-field col s6">
          <input id="upassword" type="password" className="validate" name="faculty_password" value={this.state.faculty_password} onChange={this.handleUpdate} required />
          <label htmlFor="upassword">Password</label>
          </div>
          <button className="waves-effect btn #37474f blue-grey darken-3 col l2 s2 xl2 offset-l8 offset-xl8 offset-s8" disabled={this.state.disabled} onClick={this.update}>Faculty Reset</button>
          </div>
        }
        else if(this.props.select === 'studpwd'){
          button =
          <div>
          <div className="input-field col s6">
          <input id="uusername" type="text" className="validate" name="student_username" value={this.state.student_username} onChange={this.handleUpdate} required />
          <label htmlFor="uusername">Username</label>
          </div>

          <div className="input-field col s6">
          <input id="upassword" type="password" className="validate" name="student_password" value={this.state.student_password} onChange={this.handleUpdate} required />
          <label htmlFor="upassword">Password</label>
          </div>
          <button className="waves-effect btn #37474f blue-grey darken-3 col l2 s2 xl2 offset-l8 offset-xl8 offset-s8" disabled={this.state.disabled} onClick={this.update}>Student Reset</button>
          </div>
        }
        else if(this.props.select === 'all_user_dayorder_entry')
        {
          button =
          <div style={{marginLeft:'10px',marginRight:'10px'}}>
           <div className="row">
             <div className="col l5 form-signup">
             Start Date
               <div className="row">
                <div className="col l4 input-field">
                <input type="text" id="sday" name="startday" value={this.state.startday} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="sday">Day(dd)</label>
                </div>
                <div className="col l4 input-field">
                 <input type="text" id="smonth" name="startmonth" value={this.state.startmonth} onChange={this.handleAddAccess_To_DayOrder}/>
                 <label htmlFor="smonth">Month(mm)</label>
                </div>
                <div className="col l4 input-field">
                <input type="text" id="syear" name="startyear" value={this.state.startyear} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="syear">Year(yyyy)</label>
                 </div>
               </div>
             </div>
             <div className="col l1" />
             <div className="col l6 form-signup">
             End Date
               <div className="row">
                <div className="col l4 input-field">
                <input type="text" id="eday" name="endday" value={this.state.endday} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="eday">Day(dd)</label>
                </div>
                <div className="col l4 input-field">
                 <input type="text" id="emonth" name="endmonth" value={this.state.endmonth} onChange={this.handleAddAccess_To_DayOrder}/>
                 <label htmlFor="emonth">Month(mm)</label>
                </div>
                <div className="col l4 input-field">
                <input type="text" id="eyear" name="endyear" value={this.state.endyear} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="eyear">Year(yyyy)</label>
                 </div>
               </div>
              </div>
           </div>
           <button className="btn col l2 right blue-grey darken-2 sup" style={{marginBottom:'5px'}} onClick={this.handleAllUser_DayOrder_Add}>SUBMIT</button>
<br />
              <div className="center">{this.state.response}</div>
          </div>
        }
        else{
            return(
                <div></div>
            )
        }
        return(
            <div className="center">
            <div style={{display:this.state.success}}>
              <ValidateUser displayModal={this.state.modal} showDiv={this.showDiv}/>
            </div>
            {button}
            </div>
        )
    }
}
