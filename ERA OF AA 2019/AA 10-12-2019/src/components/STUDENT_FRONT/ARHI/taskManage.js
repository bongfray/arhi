import React, { Component } from 'react';
import axios from 'axios'
import Nav from '../../dynnav'

export default class TaskManage extends Component {
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
      logout:'/user2/slogout',
      get:'/user2/getstudent',
      home:'/student',
      nav_route:'/user2/fetch_snav',
      skills:[],
      div_no:'',
      skill_name:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
   this.getUser();
   this.fetchSkills()
  }
  getUser=()=> {
    axios.get('/user2/getstudent').then(response=>{
      if (response.data.user){
          this.setState({display:'disabled'})
      }
      else{

      }
        })
      }

  fetchSkills =()=>{
    axios.post('/user2/fetchall',{action:'Skills'})
    .then( res => {
        if(res.data)
        {
          this.setState({skills:res.data})
        }
    });
  }

setDivOn =(div)=>{
  this.setState({div_no:div})
}

showDetails =(skill_name)=>{
  this.setState({
    render_detail:!this.state.render_detail,
    skill_name:skill_name,
  })
}

  render(){
    return(
      <React.Fragment>
        <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
          {this.state.render_detail && <ShowDetails detail={this.showDetails} skill_name={this.state.skill_name} />}
        <h4 className="center">Task-Manager</h4>
        <div className="row" style={{padding:'10px'}}>
        {this.state.skills.map((content,index)=>(
          <React.Fragment key={index}>
          <div className="col l2 xl2 m3 s4 go"  onClick={()=>{this.showDetails(content.skill_name)}} >
            <h6 className="center card" style={{height:'50px',paddingTop:'15px'}}>{content.skill_name}
            </h6>
          </div>
          </React.Fragment>
        ))}
        </div>
      </React.Fragment>
    );
  }
}

class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_accepted:'',
      total_request:'',
      message:'',
      mailid:'',
      isChecked:'',
      active:'',
      toggled:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleLevel = (e) =>{
    this.setState({
      isChecked: e,
      toggled:!this.state.toggled,
    })
    if (this.state.active === e) {
      this.setState({active : null})
    } else {
      this.setState({active : e})
    }
  }

  color =(position) =>{
    if (this.state.active === position) {
        return "pink go white-text";
      }
      return "go";
  }
  totalSet =(object)=>{
    this.setState(object);
  }

  render() {
    return (
      <div className="notinoti">
          <div className="right go" style={{padding:'8px'}} onClick={this.props.detail}><i className="small material-icons black-text">close</i></div>
          <h5 className="center">Complete Details of  <span className="pink-text">{this.props.skill_name}</span></h5>
          <div className="row">
              <div className="col l3">
                 <div className="row div-style">
                   <div className={'col l4 center particular '+this.color('Begineer')} onClick={(e)=>{this.handleLevel('Begineer')}}>Begineer</div>
                   <div className={'col l4 center particular '+this.color('Intermediate')} onClick={(e)=>{this.handleLevel('Intermediate')}}>Intermediate</div>
                   <div className={'col l4 center particular '+this.color('Advanced')} onClick={(e)=>{this.handleLevel('Advanced')}}>Advanced</div>
                 </div>
              </div>
              <div className="col l6"/>
              <div className="col l3">
              <div className="col l6">
                 <div className="card">
                 <div className="pink white-text center">TOTAL TASK</div>
                 <div className="card-content center">
                  <h6>{this.state.total_request}</h6>
                 </div>
                 </div>
              </div>
              <div className="col l6">
                 <div className="card">
                 <div className="pink white-text center">COMPLETED</div>
                 <div className="card-content center">
                  <h6>{this.state.total_accepted}</h6>
                 </div>
                 </div>
              </div>
              </div>
          </div>
          <div className="row">
              <TaskHistory level={this.state.isChecked} action={this.props.skill_name} totalSet={this.totalSet} />
          </div>
      </div>
    );
  }
}




class TaskHistory extends Component {
  constructor()
  {
    super()
    this.state={
      tasks:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchTasks()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.level !== this.props.level) {
      this.fetchTasks();
    }
  }

  fetchTasks =()=>{
    axios.post('/user2/fetch_in_admin',{usertype:this.props.level,action:'Task '+this.props.action})
    .then( res => {
        if(res.data)
        {
          this.setState({tasks:res.data})
          this.props.totalSet({
            total_request:res.data.length,
          })
        }
    });
  }
  render() {
    return (
      <React.Fragment>
      {this.props.level &&
        <React.Fragment>
        <div className="row">
          <div className="col l1 xl1 s1 m1 center"><b>No</b></div>
          <div className="col l5 xl5 m6 s6 center"><b>Description of the Task</b></div>
          <div className="col l4 xl4 s3 m3 center"><b>Link to Refer</b></div>
           <div className="col l2 xl2 s2 m2 center"><b>Status</b></div>
        </div>
        <hr />
        {this.state.tasks.map((content,index)=>(
          <React.Fragment key={index}>
           <div className="row">
             <div className="col l1 xl1 s1 m1 center">{index+1}</div>
             <div className="col l5 xl5 m6 s6 left" style={{whiteSpace: 'pre-line'}}>{content.description_of_the_task}</div>
             <div className="col l4 xl4 s3 m3 center" style={{whiteSpace: 'pre-line'}}>{content.ref_for_task}</div>
              <div className="col l2 xl2 s2 m2 center"><Complete level={this.props.level} usertype={this.props.action} serial={content.serial} /></div>
           </div>
             <hr />
          </React.Fragment>

        ))}
        </React.Fragment>
      }
      </React.Fragment>
    );
  }
}


class Complete extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      id:'',
      blue_allot:false,
    }
  }

  handleBlue_AllotChange =(e,index) =>{
      const value = e.target.name;
   this.setState({ blue_allot: !this.state.blue_allot, index,id:value});
   axios.post('/user/update_status_fiveyear',{status:!this.state.blue_allot,key:value})
   .then( res => {
   });

  }
  render()
  {
    return(
      <React.Fragment>
        <p>
          <label>
           <input type="checkbox" className="filled-in" checked={this.state.blue_allot} value={this.state.blue_allot} onChange={e => this.handleBlue_AllotChange(e, this.props.content.serial)}/>
           <span className="black-text">Completed ?</span>
          </label>
        </p>
      </React.Fragment>
    )
  }
}
