import React from 'react';
import styled from '@react-pdf/styled-components'
import Image from './Slogo.png'
import { PDFDownloadLink } from '@react-pdf/renderer';
import { Page, Text, View, Document, StyleSheet, Font } from '@react-pdf/renderer';
import {Table, TableBody, TableCell, TableHeader,DataTableCell} from '@david.kucsai/react-pdf-table';
import Axios from 'axios';




export default class gene extends React.Component {
  constructor()
  {
    super()
    this.state={
      username:'',
    }
    this.componentDidUpdate = this.componentDidUpdate.bind(this)
  }
  componentDidUpdate(prevProps)
  {
    if(prevProps.username!== this.props.username)
    {
      this.setUser()
    }
  }
  setUser = () =>{
    this.setState({username:this.props.username})
  }

    render() {
      // console.log('user is '+this.props.username)
        return (
            <div>
            <PDFDownloadLink
          document={<MyDocument username={this.state.username}/>}
          fileName="report.pdf"
        >
        <button className="btn blue-grey darken-2 tooltipped left" data-position="bottom" data-tooltip="Download the Report"> <i className="medium material-icons">file_download</i></button>
        </PDFDownloadLink>

            </div>
        )
    }
}

const Body = styled.Page`
  padding: 35px;
`;

const Header = styled.Text`
  color: grey;
  font-size: 12px;
  text-align: center;
  margin-bottom: 20px;
`;

const Title = styled.Text`
  font-size: 16px;
  text-align: center;
  margin-top: -30px;
`;
const SubTitle = styled.Text`
  font-size: 14px;
  text-align: center;
  margin-bottom: 20px;
  margin-top: 5px;
`;
const Sutitle = styled.Text`
  font-size: 12px;
  text-align: center;
  margin-bottom: 20px;
  margin-top: -15px;
`;
const STitle = styled.Text`
  font-size: 18px;
  text-align: center;
  margin-bottom: 10px;
  margin-top: 20px;
`;
const SQTitle = styled.Text`
  font-size: 14px;
  text-align: left;
  margin-bottom: 5px;
  margin-top: 10px;
`;
const Subtitle1 = styled.Text`
margin-top: 5px
font-size: 14px;
text-align: center;
`;
const Subtitle = styled.Text`
font-size: 14px;
text-align: center;
margin-bottom: 10px;
`;
const Topic = styled.Text`
font-size: 12px;
text-align: left;
margin-bottom: 10px;
`;
const Content = styled.Text`
font-size: 10px;
text-align: left;
margin-bottom: 10px;

`;

const Author = styled.Text`
  font-size: 12px;
  text-align: center;
  margin-bottom: 40px;
`;

const Paragraph = styled.Text`
  margin: 12px;
  font-size: 14px;
  text-align: justify;
  font-family: 'Times-Roman';
`;

const styles = StyleSheet.create({
    page: {
        backgroundColor: '#FFF',
        padding: 10,
        fontSize: 10,
        borderLeft:'1pt solid #000',
      },
    viewBox: {
      border:'1pt solid #000',
    },
  });
const Picture = styled.Image`
height: 45px;
width:65px;
`;
const Footer = styled.Text`
  left: 0px;
  right: 0px;
  color: grey;
  bottom: 30px;
  font-size: 12px;
  position: absolute;
  text-align: center;
`;


 class MyDocument extends React.Component {
   constructor(){
     super();
     this.state={
       content: "",
       name: '',
       id: '',
       desgn:'',
       dept: '',
       campus: '',
       mob: '',
       mail: '',
       loading:true,
       profile_data:[],
       degree_data:[],
       administrative_data:[],
     }
     this.componentDidMount = this.componentDidMount.bind(this)
   }
   componentDidMount()
   {
     this.fetchOption()
   }

   fetchOption = () =>{
     this.profileFetch()
     this.degreeFetch()
     this.sectionFetch()
     this.administrativeFetch()
   }

   componentDidUpdate = (prevProps) =>
   {
     if(prevProps.username!== this.props.username)
     {
       this.fetchOption()
     }
   }
   profileFetch = () =>{
     Axios.post('/user/dash2',{username:this.props.username})
     .then(res=>{
       this.setState({profile_data:res.data})
     })
   }
   degreeFetch = () =>{
     Axios.post('/user/fetch_degree_for_pdf',{username:this.props.username})
     .then(res=>{
       this.setState({degree_data:res.data})
     })
   }
   sectionFetch = () =>{
     Axios.post('/user/fetch_section_for_view',{username:this.props.username})
     .then(res=>{
       this.setState({section:res.data})
     })
   }
   administrativeFetch =()=>{
     Axios.post('/user/fetch_adminis_for_view',{username:this.props.username}).then(res => {
       console.log(res.data)
       this.setState({
         administrative_data: res.data,
       });
     })
   }
    render() {
      let content,section,administrative_data;
      if(this.state.degree_data)
      {
        content =
        <Table
                      data={this.state.degree_data}
                  >
                      <TableHeader textAlign={"center"} >
                          <TableCell weighting={0.3}>
                              Name Of the Degree
                          </TableCell>
                          <TableCell>
                              College Name
                          </TableCell>
                          <TableCell>
                              Start Year
                          </TableCell>
                          <TableCell>
                              End Year
                          </TableCell>
                          <TableCell>
                              Grade or Marks
                          </TableCell>
                      </TableHeader>
                      <TableBody textAlign={"center"}>
                          <DataTableCell weighting={0.3} getContent={(r) => r.action}/>
                          <DataTableCell getContent={(r) => r.Collage_Name}/>
                          <DataTableCell getContent={(r) => r.Start_Year}/>
                          <DataTableCell getContent={(r) => r.End_Year}/>
                          <DataTableCell getContent={(r) => r.Marks_Grade}/>
                      </TableBody>
                  </Table>
      }
      if(this.state.section)
      {
        section =
        <Table
                      data={this.state.section}
                  >
                      <TableHeader textAlign={"center"} >
                          <TableCell weighting={0.3}>
                              Topic
                          </TableCell>
                          <TableCell>
                              Description
                          </TableCell>
                          <TableCell>
                              Role / Contribution
                          </TableCell>
                          <TableCell>
                              Achivements
                          </TableCell>
                          <TableCell>
                              Date of Starting
                          </TableCell>
                          <TableCell>
                              Date of Ending
                          </TableCell>
                      </TableHeader>
                      <TableBody textAlign={"center"}>
                          <DataTableCell weighting={0.3} getContent={(r) => r.action}/>
                          <DataTableCell getContent={(r) => r.description}/>
                          <DataTableCell getContent={(r) => r.role}/>
                          <DataTableCell getContent={(r) => r.achivements}/>
                          <DataTableCell getContent={(r) => r.date_of_starting}/>
                          <DataTableCell getContent={(r) => r.date_of_complete}/>
                      </TableBody>
                  </Table>
      }
      if(this.state.administrative_data)
      {
      administrative_data =
        <Table
                      data={this.state.administrative_data}
                  >
                      <TableHeader textAlign={"center"} >
                          <TableCell weighting={0.3}>
                              About the Responsibility
                          </TableCell>
                          <TableCell>
                              Content of Submission
                          </TableCell>
                          <TableCell>
                              Submitted DayOrder.Slot
                          </TableCell>
                      </TableHeader>
                      <TableBody textAlign={"center"}>
                          <DataTableCell weighting={0.3} getContent={(r) => r.freeparts}/>
                          <DataTableCell getContent={(r) => r.freeslot}/>
                          <DataTableCell getContent={(r) => r.day_slot_time}/>
                      </TableBody>
                  </Table>
      }

        return (
  <Document>
  <Page wrap size="A4" style={{ padding: '20px',borderTop:'1pt solid #000'} }>
  <View>


    <Picture src={Image} />
      <Title>SRM INSTITUTE OF SCIENCE AND TECHNOLOGY</Title>
      <SubTitle>PROFILE REPORT</SubTitle>
      <Sutitle>(For Faculty Members)</Sutitle>

      <Topic>1.     Name : {this.state.profile_data.name}</Topic>
      <Topic>2.     Official ID : {this.state.profile_data.username}</Topic>
      <Topic>3.     Campus : {this.state.profile_data.campus}</Topic>
      <Topic>4.     Department : {this.state.profile_data.dept}</Topic>
      <Topic>5.     Designation : {this.state.profile_data.desgn}</Topic>
      <Topic>6.     Mobile No. : {this.state.mob}  E-mail : {this.state.profile_data.mailid}</Topic>

      <STitle>QUALIFICATION</STitle>
      <SQTitle>Degrees :</SQTitle>
      {this.state.degree_data && content}

      <STitle>Achivements</STitle>
      <SQTitle>Section Datas :</SQTitle>
      {this.state.section && section}

      <STitle></STitle>
      <SQTitle>Administrative Datas :</SQTitle>
      {this.state.administrative_data && administrative_data}

      <Content>

      </Content>





      </View>

      <Footer render={({ pageNumber}) => (
        `${pageNumber}                                                                                                                        ~generated by eWork`
      )} fixed />
      </Page>
  </Document>

        )

    }
}
