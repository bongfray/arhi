import React, { Component } from 'react';
import NaVM from './nav-modal'
import './App.css'
export default class Nstart extends Component {
  constructor()
  {
    super()
    this.state = {
      modal: false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  openModal =() =>
  {
      this.setState({modal: !this.state.modal})
  }
  componentDidMount()
  {
    this.openModal();
  }
  render() {
    return (
       <div className="Navigator">
       <NaVM
           displayModal={this.state.modal}
           closeModal={this.openModal}
       />
       </div>
    );
  }
}
