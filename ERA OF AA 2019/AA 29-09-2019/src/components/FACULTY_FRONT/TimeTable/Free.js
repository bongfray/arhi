import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import M from 'materialize-css'
import Fd from './Free_Hirearchy'
var empty = require('is-empty');

export default class Free extends React.Component {
  constructor() {
    super()
    this.state = {
        compensation:'false',
        saved_dayorder: '',
        datapass: '',
        saved_slots:'',
        freeslot:'',
        freefield:'',
        freeparts:'',

    }
    this.handleFreeSlot = this.handleFreeSlot.bind(this)
    this.handleFreeVal = this.handleFreeVal.bind(this)
    this.handleFreeField = this.handleFreeField.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    }

    closeModal=()=>{
      this.props.closeModal();
    }
    colorChange=()=>{
      this.props.color()
    }

        handleFreeField =(e) =>{
          this.setState({
            freefield: e.target.value,
          })

        }

        handleFreeVal =(e) =>{
          this.setState({
            freeslot: e.target.value,
          })

        }


        handleFreeSlot =(e) =>{
          if(empty(this.state.freefield)||empty(this.state.freeslot))
          {
            window.M.toast({html: 'Enter All the Details First',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
            this.setState({
              freefield:'',
              freeslot:'',
            })
          }
          else {
          // alert(this.props.day_order)
          // alert(this.state.selected)
          e.preventDefault()

          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var myDate = new Date.today();
          var week =myDate.getWeek();

          axios.post('/user/timefree', {
            order:this.props.day_order+this.props.day_slot_time+'Free',
            day_slot_time: this.props.day_order+this.props.day_slot_time,
            freefield: this.state.freefield,
            freeslot: this.state.freeslot,
            freeparts: this.state.freeparts,
            date:this.props.day,
            month: this.props.month,
            year: this.props.year,
            week:week,
          })
            .then(response => {

              if(response.status===200){
                if(response.data.not)
                {
                  this.closeModal();
                  window.M.toast({html: response.data.not, outDuration:'1200', inDuration:'1200', classes:'rounded pink lighten-1'});
                }
                else if(response.data.suc)
                {
                  this.closeModal();
                  window.M.toast({html: response.data.suc, outDuration:'1200', inDuration:'1200', classes:'rounded pink lighten-1'});
                }
                else if(response.data.succ)
                {
                  window.M.toast({html: 'Success !!', outDuration:'1200', inDuration:'1200', classes:'rounded #ec407a green lighten-1'});
                  this.closeModal();
                  this.colorChange();
                }
              }
            }).catch(error => {
              window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
            })
            this.setState({
              freefield:'',
              freeslot:'',
          })
}

        }

  componentDidMount()
  {
    let selects = document.querySelectorAll('select');
    M.FormSelect.init(selects, {});
  }

  updateAllotV (userObject) {
    this.setState(userObject)
  }
  freeParts=(userObject)=> {
    this.setState(userObject)
  }

render(){
  return(
    <div>
    <div className="row"><div className="col l4 quali">Plaese Mention on which area you are going to work :
              <span><select value={this.state.freefield} onChange={this.handleFreeField}>
                <option value="" disabled seleted>Select Here</option>
                <option value="Academic">Academic</option>
                <option value="Research_Work">Research Work</option>
                <option value="Administrative_Work">Administrative Work</option>
              </select></span></div>
              <div className="col l3">
                 <Fd freefield={this.state.freefield} freeParts={this.freeParts} />
              </div>
              </div>
    <label className="pure-material-textfield-outlined alignfull">
      <textarea
        className="area"
        type="text"
        placeholder=" "
        min="10"
        max="60"
        value={this.state.freeslot}
        onChange={this.handleFreeVal}
      />
      <span>Detail About Your work</span>
    </label>
    <button className="waves-effect btn col l2 s4 blue-grey darken-2 sup right" onClick={this.handleFreeSlot}>SUBMIT</button>

    </div>
  );
}
}
