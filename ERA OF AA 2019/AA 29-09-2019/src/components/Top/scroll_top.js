import React, { } from 'react'
class ScrollButton extends React.Component {
  constructor() {
    super();

    this.state = {
        intervalId: 0
    };
  }

  scrollStep() {
    if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
  }

  scrollToTop() {
    let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
    this.setState({ intervalId: intervalId });
  }

  render () {
      return(
        <div>
        <div className='up go' onClick={ () => { this.scrollToTop();}}> <i className="material-icons aroo go">arrow_upward</i>
       </div>
        </div>
      );
   }
}

export default class ScrollApp extends React.Component {
  render () {
    return <div>
              <ScrollButton scrollStepInPx="100" delayInMs="26.66"/>
           </div>
  }
}
