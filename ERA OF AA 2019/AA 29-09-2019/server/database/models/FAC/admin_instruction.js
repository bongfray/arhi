const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const adminorder = new Schema({
  usertype: {type: String , unique: false, required: false},
  order:{type: String , unique: false, required: false},
  endday:{type: Number , unique: false, required: false},
  endmonth:{type: Number , unique: false, required: false},
  endyear:{type: Number , unique: false, required: false},
  startday:{type: Number , unique: false, required: false},
  startmonth:{type: Number , unique: false, required: false},
  startyear:{type: Number , unique: false, required: false},
  registration_status:{type: Boolean, unique: false, required: false},
})


const AdminOrder = mongoose.model('admin_order', adminorder)
module.exports = AdminOrder
