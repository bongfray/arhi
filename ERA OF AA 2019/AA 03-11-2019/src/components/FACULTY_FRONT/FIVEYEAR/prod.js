import React from 'react';
import axios from 'axios';
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      username:'',
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/del_five_year_plan',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetch_five_year_plan',{
    action: this.props.action
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}

  render() {
      return(
        <div>
        <div className="row">
          <div className="col l1 s1 m1 xl1 center"><b>No.</b></div>
          <div className="col l6 s4 m4 xl6 center"><b>Plan</b></div>
          <div className="col l2 s4 m4 xl2 center"><b>Status</b></div>
          <div className="col l3 s3 m3 right"><b>Action</b></div>
        </div>
        <div>
        {this.state.products.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1 s1 m1 center">{index+1}</div>
          <div className="col l6 s4 m4 xl6 center go">{content.content}</div>
          <div className="col l2 s4 m4 xl2 center" style={{marginTop:'-12px'}}>
          <Complete content={content}/>
          </div>
          <div className="col l3 s3 m3 right">
          <i className="material-icons go" onClick={() => this.props.editProduct(content.serial,this.props.action)}>edit</i>
          &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(content.serial,this.props.action)}>delete</i>
          </div>
          </div>
        ))}
        </div>
        </div>
      )

  }
}


class Complete extends React.Component{
  constructor(props)
  {
    super(props)
    this.state={
      id:'',
      blue_allot:false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    axios.post('/user/fetch_complete_fiveyear',{serial:this.props.content.serial})
    .then( res => {
      if(res.data)
      {
        this.setState({blue_allot:res.data})
      }
    });
  }
  handleBlue_AllotChange =(e,index) =>{
      const value = e.target.name;
   this.setState({ blue_allot: !this.state.blue_allot, index,id:value});
   axios.post('/user/update_status_fiveyear',{status:!this.state.blue_allot,key:value})
   .then( res => {

   });

  }
  render()
  {
    return(
      <React.Fragment>
        <p>
          <label>
           <input type="checkbox" className="filled-in" name={this.props.content.serial} checked={this.state.blue_allot} value={this.state.blue_allot} onChange={e => this.handleBlue_AllotChange(e, this.props.content.serial)}/>
           <span className="black-text">Completed ?</span>
          </label>
        </p>
      </React.Fragment>
    )
  }
}
