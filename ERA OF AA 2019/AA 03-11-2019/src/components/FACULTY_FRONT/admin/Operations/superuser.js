import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import { Select } from 'react-materialize'
import axios from 'axios'
import Validate from '../validateUser'
export default class SuperUser extends Component{
    constructor(){
        super();
        this.state={
          showButton:'block',
          show:'none',
          redirectTo:'',
          modal: false,
          isChecked:false,
          isCheckedS:false,
          history:'',
            susername:'',
            username:'',
            index_id:'',
            request:[],
            viewdata:'',
            option:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    handleComp= (e)=>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
      axios.post('/user/handleFaculty_Registration',{
        checked: !this.state.isChecked,
        history: e.target.value,
      })
      .then(res=>{

      })
    }

    handleCompS=(e)=>{
      this.setState({
        isCheckedS: !this.state.isCheckedS,
        history: e.target.value,
      })

      axios.post('/user2/handleStudent_Registration',{
        checked: !this.state.isCheckedS,
        history: e.target.value,
      })
      .then(res=>{

      })
    }

    getRequest =() =>{
      axios.get('/user/getrequest')
          .then(response => {
            this.setState({request: response.data})
          })
    }
    view_Status_of_Request = (content,index) =>{
      this.setState({
        username:content.username,
        index_id:index,
      })
      axios.post('/user/approve_request',content)
          .then(response => {
            if(response.data)
            {
              this.setState({viewdata: response.data})
            }
          })
    }
    openModal()
    {
        this.setState({modal: !this.state.modal})
    }
    showDiv =(userObject) => {
      this.setState(userObject)
    }
    componentDidMount()
    {
      this.openModal();
      this.getRequest();
      this.fetchFaculty_Registration_Status();
      this.fetchStudent_Registration_Status();
    }
    fetchFaculty_Registration_Status()
    {
      axios.get('/user/fac_reg_status')
          .then(response => {
            if(response.data)
            {
              this.setState({isChecked: response.data})
            }
          })
    }
    fetchStudent_Registration_Status()
    {
      axios.get('/user2/stud_reg_status')
          .then(response => {
            if(response.data)
            {
              this.setState({isCheckedS: response.data})
            }
          })
    }

    handleOption = (e) =>{
      this.setState({option: e.target.value})
    }


    handleRequestModify =(object) =>{
      this.setState(object)
    }
    render(){
      console.log(this.state.history)
      var viewbutton;
        if(this.props.select === 'register'){
            viewbutton =
              <React.Fragment>
                <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                <div className="row form-signup" style={{padding:'15px',display:this.state.show}}>
                <div className=" col s12 l12 m12" >

                <div className="row">
                <div className="col l8 s8 m8">
                   Faculty Registration
                </div>
                <div className="col l4 s4 m4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.isChecked} value="fac" onChange={this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
                </div>
                </div>

                <div className="row">
                <div className="col l8">
                   Student Registration
                </div>
                <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input  checked={this.state.isCheckedS} value="stud" onChange={this.handleCompS} type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
                </div>
                </div>
                </div>
                </div>
                </React.Fragment>
        }
        else if(this.props.select === 'signup'){
            viewbutton = <React.Fragment>
            <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
                    <div style={{display:this.state.show}}>
                        <Select name="" value={this.state.option} onChange={this.handleOption}>
                        <option value="" disabled selected>Select Here...</option>
                        <option value="faculty">Faculty</option>
                        <option value="student">Student</option>
                        </Select>
                    </div>
                       <SignUpReq choice={this.state.option}/>
                    </React.Fragment>
        }
        else if(this.props.select === 'facprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Studnet Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'facreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Student Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === "approve_single_faculty_req")
        {
          viewbutton =
          <React.Fragment>
          <Validate displayModal={this.state.modal} closeModal={this.openModal} showDiv={this.showDiv}/>
          <div style={{margin:'15px 8px 1px 9px',display:this.state.show}}>
          <div className="row">
            <div className="col l1 center"><b>Serial No.</b></div>
            <div className="col l1 center"><b>Day Order</b></div>
            <div className="col l2 center"><b>Official ID</b></div>
            <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
            <div className="col l3 center"><b>Reason</b></div>
            <div className="col l2 center"><b>Action</b></div>
          </div><hr />
          {this.state.request.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 center">{index+1}</div>
            <div className=" col l1 center">{content.day_order}</div>
            <div className=" col l2 center">{content.username}</div>
            <div className=" col l3 center">{content.day}/{content.month}/{content.year}</div>
            <div className=" col l3 center">{content.req_reason}</div>
            </div>
            <button className="btn col l2 blue-grey darken-2 sup" onClick={() => this.view_Status_of_Request(content,content.serial)}>View Status</button>
            </div><hr />
            </React.Fragment>
          ))}
          <ViewApprove showDiv={this.showDiv} handleRequestModify={this.handleRequestModify} showButton={this.state.showButton} request={this.state.request} view_details={this.state.viewdata} username={this.state.username} index_id={this.state.index_id}/>
          </div>
          </React.Fragment>
        }
        else{
          viewbutton =
          <div></div>
        }
        return(
            <div className="center">
              {viewbutton}
            </div>
        )
    }
}


class SignUpReq extends Component{
  constructor(props)
  {
    super(props)
    this.state={
      display:'none',
      requests:[],
    }
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchReq()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice!== this.props.choice)
    {
      this.fetchReq()
    }
  }
  Active =(e,index,username)=>{
  this.setState({
        isChecked: !this.state.isChecked,
        username: e.target.value,
      })

      axios.post('/user/active_user',{
        username: username,
      })
      .then(res=>{
            window.M.toast({html: 'Activated !!',outDuration:'9000', classes:'rounded green darken-2'});
            const { requests } = this.state;

                  this.setState({
                    requests: requests.filter(product => product.username !== username)
                 })
      })
  }
  fetchReq =()=>{
    let route;
    if(this.props.choice === 'faculty')
    {
      route = '/user/fetch_signup_request_for_admin';
    }
    else if(this.props.choice === 'student')
    {
      route = '/user/fetch_student_request';
    }
    axios.get(route)
    .then(res=>{
        this.setState({display:'block',requests:res.data})
    })
  }
  render()
  {
    return(
      <React.Fragment>
            {this.props.choice &&
      <div className="row">
         <div className="col l12 xl12">
            <div className="card">
              <div className="card-title center pink white-text">Accept Request</div>
              <div className="card-content">
                {this.state.display  === 'none' && <h6>Fetching Requests.....</h6>}
                <div style={{display:this.state.display}}>
                {this.state.requests.length === 0 ?
                <h6 className="center">No Request Found</h6>
                :
                <React.Fragment>
                <div className="row">
                  <div className="row">
                        <div className="col l2 center"><b>Name</b></div>
                        <div className="col l1 center"><b>Official Id</b></div>
                        <div className="col l2 center"><b>Mail Id</b></div>
                        <div className="col l2 center"><b>Campus</b></div>
                        <div className="col l2 center"><b>Department</b></div>
                        <div className="col l2 center"><b>Designation</b></div>
                        <div className="col l1 center"><b>Action</b></div>
                  </div>
                  <hr />
                   {this.state.requests.map((content,index)=>(
                           <div className="row"  key={index}>
                            <div className="col l2 center">{content.name}</div>
                            <div className="col l1 center">{content.username}</div>
                            <div className="col l2 center">{content.mailid}</div>
                            <div className="col l2 center">{content.campus}</div>
                            <div className="col l2 center">{content.dept}</div>
                            <div className="col l2 center">{content.desgn}</div>
                            <div className="col l1 center">
                                <div className="switch">
                                  <label>
                                    <input  checked={this.state.isChecked} value="fac" onChange={(e)=>{this.Active(e,index,content.username)}} type="checkbox" />
                                    <span className="lever"></span>
                                  </label>
                                </div>
                            </div>
                          </div>
                   ))}
                 </div>
                </React.Fragment>
              }
              </div>
              </div>
            </div>
         </div>
         </div>
       }
      </React.Fragment>
    ) ;
  }
}

class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleDeny =(id,username) =>{
    this.props.showDiv({modal:true,showButton:'none'})
    axios.post('/user/denyrequest',{
      serial: id,
      username:username,
    }).then(res=>{
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  handleApprove = (id,username) =>{
    this.props.showDiv({modal:true,showButton:'none'})
    axios.post('/user/approverequest',{
      serial: id,
      username:username,
    }).then(res=>{
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  render()
  {
    var dat;
    if(this.props.view_details ==="yes")
    {
      dat= <React.Fragment>
      <h6>Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!</h6>
      <div className="row">
      <button className="btn col l2 right red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      </div>
      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat= <React.Fragment>
      <h6>We can't able to find any data for this request !! Kindly take decison manually !!</h6>
      <div className="row">
      <div className="col l7" />
      <div className="col l5">
      <div className="row">
      <div className="col l5" />
      <button className="btn col l3 sup red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      <div className="col l1" />
      <button className="btn col l3 blue-grey darken-2 sup" onClick={() => this.handleApprove(this.props.index_id,this.props.username)}>APPROVE</button>
      </div>
      </div>
      </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    return(
      <div className="form-signup" style={{display:this.props.showButton}}>
      {dat}
      </div>
    )
  }
}
