import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from '../forget'
import axios from 'axios'
import M from 'materialize-css';
import { } from 'materialize-css';
import Nav from '../dynnav'

var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            error:'',
            username: '',
            password: '',
            redir: '',
            redirectTo: null,
            forgotroute:'/user/forgo',
            home:'/faculty',
            logout:'/user/logout',
            get:'/user/',
            nav_route: '/user/fetchnav',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'First Enter Details!!',outDuration:'1000',activationPercent:0.999, classes:'rounded #f44336 red'});
          return false;
        }
        else{
        axios.post('/user/faclogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                // console.log(response)
                if (response.status === 200) {
                if(response.data.count===0)
                {
                  this.setState({
                      redirectTo: '/fprofdata'
                  })
                }
                else if((response.data.count === 2)||(response.data.count ===1))
                {
                  this.setState({
                      redirectTo: '/faculty'
                  })
                }
                else if(response.data.count === 3)
                {
                  this.setState({
                    redirectTo:'/dadmin_panel'
                  })
                }
                else if(response.data.count === 4)
                {
                  this.setState({
                    redirectTo:'/admin_panel'
                  })
                }
                }
            }).catch(error => {
                window.M.toast({html: 'WRONG CREDENTIALS!!',outDuration:'1000',activationPercent:0.999, classes:'rounded #f44336 red'});
            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }



    getUser = () => {
      axios.get('/user/').then(response => {
        if (response.data.user) {
              if((response.data.user.count === 1) || (response.data.user.count === 0) || (response.data.user.count === 2))
              {
                this.setState({redirectTo:'/faculty'})
              }
              else if((response.data.user.count === 3))
              {
                this.setState({redirectTo:'/dadmin_panel'})
              }
              else if((response.data.user.count === 4))
              {
                this.setState({redirectTo:'/admin_panel'})
              }
        }
          })
        }



        componentDidMount() {
            this.getUser();
            M.AutoInit();
        }

    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

            return (
              <React.Fragment>
              <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
              <div className="row">

             <div className="col s4">
             </div>

             <div className="col l4 s12 m12 form-signup">
                 <div className="ew center">
                     <Link to ="/faculty"><h5 className="blue-grey darken-2 highlight">eWORK</h5></Link>
                     <h5 className="highlight black-text">LOG IN</h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="email" type="text" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <label htmlFor="email">Username</label>
                         <span className="helper-text" data-error="Please enter username !!" data-success="">Official Id or College Id</span>
                     </div>
                     <div className="input-field col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <label htmlFor="password">Password</label>
                         <span className="helper-text" data-error="Please enter password !!" data-success=""></span>
                     </div>
                     </div>
                     <div className="row">
                     <Link to="/fsignup" className=" btn blue-grey darken-2 col s5 sup">Register</Link>
                     <div className="col s2"></div>
                     <button className=" btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <hr/>
                     <div className="row">
                     <Modal forgot={this.state.forgotroute}/>
                     </div>
                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
             </React.Fragment>
           );
         }

    }
}

export default LoginForm
