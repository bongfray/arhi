const passport = require('passport')
const LocalStrategy = require('./studentpass')
const SUser = require('../database/models/STUD/suser')

passport.serializeUser((user, done) => {
	console.log('*** serializeUser called, user: ')
	console.log(user)
	console.log('---------')
	done(null, { _id: user._id })
})


passport.deserializeUser((id, done) => {
	console.log('DeserializeUser called')
	SUser.findOne(
		{ _id: id },
		'username',
		(err, user) => {
			console.log('*** Deserialize user, user:')
			console.log(user)
			console.log('--------------')
			done(null, user)
		}
	)
})

//  Use Strategies
passport.use(LocalStrategy)

module.exports = passport
