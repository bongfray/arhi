import React, { Component } from 'react';
import {} from 'materialize-css'
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import Daa from './DAA'
import ContentLoader from "react-content-loader"
import {
  CircularProgressbar,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./AnimatedProgressProvider";



export default class Dash extends Component {

    constructor(props) {
      super(props);
      this.state = {
        loading: true,
        home:'/faculty',
        logout:'/user/logout',
        login:'/flogin',
        get:'/user/',
        nav_route: '/user/fetchnav',
        isChecked:false,
      }
      this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchlogin()
    }
    fetchlogin = () =>{
      axios.get('/user/fetchdayorder')
      .then(response =>{
        this.setState({loading: false})
        if(response.data === "Not"){
          this.setState({
            redirectTo:'/faculty',
          });
          window.M.toast({html: 'You are not Logged In',outDuration:'1500', classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
      })
    }
  render()
  {
    const MyLoader = () => (
      <ContentLoader
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
        <rect x="20" y="10" rx="5" ry="5" width="110" height="50" />
        <rect x="140" y="10" rx="3" ry="3" width="110" height="50" />
        <rect x="270" y="10" rx="3" ry="3" width="110" height="50" />

        <rect x="20" y="90" rx="1" ry="1" width="110" height="15" />
        <rect x="140" y="90" rx="1" ry="1" width="110" height="15" />
        <rect x="270" y="90" rx="1" ry="1" width="110" height="15" />


      </ContentLoader>
    )

    if(this.state.loading=== true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav login={this.state.login} home={ this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

      <div className="row" style={{marginTop:'20px'}}>
          <div className="switch center">
              <label style={{color:'red',fontSize:'15px'}}>
                  Viewing Weekly Percentages(Current Week)
                  <input  checked={ this.state.isChecked } value="month" onChange={ this.handleComp} type="checkbox" />
                  <span className="lever"></span>
                  View Monthly Percentages(Current Month)
              </label>
          </div>
      </div>
      <Dashh choice={this.state.isChecked}/>
      </React.Fragment>
    )
  }
}
  }
}


class Dashh extends Component {

  constructor(props) {
    super(props);
    this.state = {
      administrative:'',
      academics:'',
      research:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    this.countPercentage()
  }
  componentDidUpdate =(prevProps)=>{
    if(prevProps.choice !== this.props.choice)
    {
      this.countPercentage()
    }
  }
  countPercentage =() =>{
    let adroute,acaroute,resroute;
    if(this.props.choice === false)
    {
      adroute ='/user/fetchAdmins_Actual';
      acaroute = '/user/fetchAcad_Actual';
      resroute = '/user/fetchRes_Actual'
    }
    else if(this.props.choice === true)
    {
      adroute ='/user/fetchAdmins_Actual_month';
      acaroute = '/user/fetchAcad_Actual_month';
      resroute = '/user/fetchRes_Actual_month'
    }
    this.AdministrativeP(adroute);
    this.ResearchP(resroute);
    this.AcademicP(acaroute);
  }


    AdministrativeP = (adroute) =>{
      axios.get(adroute)
      .then( response => {
        this.setState({
          administrative:response.data.result
        })
      });
    }
    AcademicP = (acaroute) =>{
      axios.get(acaroute)
      .then( response => {
        console.log(response.data)
        this.setState({
          academics:response.data.result,
        })
      });
    }
    ResearchP = (resroute) =>{
      axios.get(resroute)
      .then( response => {

        this.setState({research:response.data.result})
      });
    }

  componentWillUnmount =()=> {
    this._isMounted = false;
  }

render(){

  return (
    <React.Fragment>
    <div className="row">
    <div className="col l4 xl4 s12 m12">
    <Example label="Administrative">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.state.administrative}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    <div className="col l4 xl4 s12 m12">
    <Example label="Academics">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.state.academics}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>

    <div className="col l4 xl4 s12 m12">
    <Example label="Research">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.state.research}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    </div>
    <div>
    {this.props.choice=== false && <Daa />}
     <FreeSlots choice={this.props.choice}/>

    </div>
    </React.Fragment>
    );
    }

  }
  function Example(props) {
  return (
  <div className="card hoverable" style={{paddingTop: '10px', paddingBottom:'10px'}}>
     <div className="card-title center #64ffda teal accent-2 black-text">{props.label}</div><br />
      <div style={{marginLeft:'26%',width:'200px'}}>{props.children}</div>
  </div>
  );
  }


  class FreeSlots extends Component {
    constructor() {
      super()
      this.state={
        free_slot_own:'',
        free_slot_other:'',
        absent:'',
      }
      this.componentDidMount =  this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      this.fetchFree()
    }
    fetchFree = () =>{
      let free_slot_own_route,free_slot_other_route,absent_route;
      if(this.props.choice === false)
      {
        free_slot_own_route ='/user/fetch_own_extra_week';
        free_slot_other_route = '/user/fetch_other_extra_week';
        absent_route = '/user/fetch_absent_week'
      }
      else if(this.props.choice === true)
      {
        free_slot_own_route ='/user/fetch_own_free_free_month';
        free_slot_other_route = '/user/fetch_other_extra_month';
        absent_route = '/user/fetch_absent_month'
      }
      // this.FreeSlotOwn(free_slot_own_route);
      this.FreeSlotOther(free_slot_other_route);
      this.Absent(absent_route);
    }

    FreeSlotOwn = (adfroute) =>{
      axios.get(adfroute)
      .then( response => {
        this.setState({
          free_slot_own:response.data.result,
        })
      });
    }
    FreeSlotOther = (free_slot_other_route) =>{
      axios.get(free_slot_other_route)
      .then( response => {
        this.setState({
          total_other:response.data.total,
          free_slot_other:response.data.completed,
        })
      });
    }
    Absent = (absent_route) =>{
      axios.get(absent_route)
      .then( response => {
        this.setState({
          absent:response.data.length,
        })
      });
    }

    render()
    {
      return(
        <React.Fragment>
          <div className="row">
             <div className="col l4">
             <div className="yellow center ">Total Extra Slot Taken(By You)</div>
               <div className="card">
                 <div className="card-content">
                        No of the Extra Slot taken : {this.state.free_slot_own}
                 </div>
               </div>
             </div>

             <div className="col l4">
             <div className="yellow center ">Total Extra Slot Taken(For other Faculty)</div>
               <div className="card">
                 <div className="card-content">
                   <div className="row">
                      <div className="col l8">Hanlded Slot -- Total Request - </div>
                      <div className="col l4">{this.state.free_slot_other} -- {this.state.total_other}</div>
                   </div>
                 </div>
               </div>
             </div>

             <div className="col l4">
             <div className="yellow center">Total Absent</div>
               <div className="card">
                 <div className="card-content">
                   Total Number of  Absent in this week -  <span className=" red-text"><b>{this.state.absent}</b></span>
                 </div>
               </div>
             </div>

</div>

        </React.Fragment>
      );
    }
  }
