import React, { } from 'react'
import { } from 'react-router-dom'
import {Redirect } from 'react-router-dom'
import M from 'materialize-css'
import Modal2 from './Modal'

/*---------------------------------------------------------Code for regular classes time table------------------------------------ */



export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',
      modal: false,
    };

  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}
  componentDidMount(){
    M.AutoInit();
  }
  render()
  {

    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <div className="root-of-time">
        <div className="node">
        <div style={{backgroundColor:'white'}} onClick={ this.selectModal } className="card hoverable">
        <p className="left blue-text" style={{marginLeft:'5px'}}>{this.props.num}</p>
        <p className="center">{this.props.time}</p>
        <p className="go center"><b>{this.props.slots}</b></p>
        </div>
   <Modal2
   day={this.props.day}
   month={this.props.month}
   year={this.props.year}
   displayModal={this.state.modal}
   closeModal={this.selectModal}
   day_slot_time={this.props.day_slot_time}
   usern={this.props.usern}
   day_order={this.props.day_order}
   color={this.props.color}
   time={this.props.time}
   slot={this.props.slots}
   day_sl={this.props.day_sl}
   cday={this.props.cday}
   />
   </div>
   </div>
      )
  }
  }
}



/*
<Modal id="modal1">
          <div className="row">
             <div className="col l6">Submitting Data For</div>
             <div className="col l6">{content.slot}</div>
          </div>
          <div className="row">
          <div className="input-field col l12">
             <input id="topics" type="text" value={this.state.covered} onChange={this.coveredTopic} className="validate" />
             <label for="topics">Enter the topics you have covered</label>
          </div>
           <button href="#modal1" className="btn right" onClick={() => this.completed(content.serial,content.order_from,content.hour,content.slot)}>Submit</button>
          </div>
</Modal>
*/



/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */
