const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const Nav = require('../database/models/FAC/nav_stat')
const User = require('../database/models/FAC/user')
const Degrees = require('../database/models/FAC/degree')
const Common_DayOrder = require('../database/models/FAC/dayorder')
const DayOrder_History = require('../database/models/FAC/dayorderhistory')
const Timetable = require('../database/models/FAC/time_table')
const Alloted_Slot = require('../database/models/FAC/schema_for_alloted')
const Request = require('../database/models/FAC/req')
const AdminOrder = require('../database/models/FAC/admin')
const Admin_Operation = require('../database/models/FAC/admin_operation')
const Five = require('../database/models/FAC/fiveyarplan')
const Section = require('../database/models/FAC/section')
const Extra = require('../database/models/FAC/extraop')
const Extra_Handle = require('../database/models/FAC/extrahour_handle')
const Absent = require('../database/models/FAC/absent')
const Notification = require('../database/models/FAC/notification')
const passport = require('../passport/fac_pass')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')

/*------------------------------------------------------------------------Changing Day Orders------------------- */

let timer;

function nextGo(time)
{
    console.log("Way to fetch next Common_DayOrder.....")
    timer = setTimeout(() =>
    {
    myFunc()
  },time);
}

Common_DayOrder.findOne({day_order : {"$exists" : true} }, function(err, user){
  if(err)
  {

  }
  else if(user)
  {
    let hour = (new Date().getHours())*3600000;
    let minutes = (new Date().getMinutes())*60000;
    let second =(new Date().getSeconds())*1000;
    let total = hour+minutes+second;
    let max = (86400020-total);
    console.log(max);
    nextGo(max);
  }
 else
 {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  var day = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour + minutes + second;
  let time = (86400020-total);
  var day_order;
  var day_order_status;
  if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
  {
    day_order = 0;
    day_order_status = "weekend";
  }
  else
  {
    day_order = 1;
    day_order_status= "online";
  }
    const dayyy = new Common_DayOrder({
      day_order: day_order,
      time: time,
      week: week,
      day_order_status:day_order_status,
      day:day,
      month:month,
      year:year,
      cancel_status: false,
    });
    dayyy.save(() => {
    });

    DayOrder_History.findOne({day_order : {"$exists" : true}}, function(err, user){
      const dayhistory = new DayOrder_History({
        day_order: day_order,
        day:day,
        month:month,
        year:year,
        day_order_status:day_order_status,
        week:week,
        cancel_status: false,
      });
      dayhistory.save(() => {
      });
    });
    nextGo(time);
 }
})

function myFunc()
{
    console.log("Updating Dayorder....")
      Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user1){
        if(err)
        {

        }
        else if(user1)
        {
          var yesday = Date.parse("yesterday").toString("dd");
          var day = Date.today().toString("dd");
          var month = Date.today().toString("M");
          var year = Date.today().toString("yyyy");
          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var today = new Date.today();
          var week =today.getWeek();

          if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
          {
            console.log("Inside weekend Check !!")
            DayOrder_History.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, user){
              if(err)
              {

              }
              else if(!user)
              {
                const dayhistory = new DayOrder_History({
                  day_order: user1.day_order,
                  day:day,
                  month:month,
                  year:year,
                  day_order_status:'weekend',
                  week:week,
                  cancel_status: false,
                });
                dayhistory.save((err, savedUser) => {
                });
              }
              else if(user)
              {
              const dayhistory = new DayOrder_History({
                day_order: user1.day_order,
                day:day,
                month:month,
                year:year,
                day_order_status:'weekend',
                week:week,
                cancel_status: user1.cancel_status,
              });
              dayhistory.save((err, savedUser) => {
              });
            }
            })
              nextGo(86400020);
          }
          else
          {
            var yesday = Date.parse("yesterday").toString("dd");
            var day = Date.today().toString("dd");
            var month = Date.today().toString("M");
            var year = Date.today().toString("yyyy");
            let prev = user1.day_order;
            let recent = prev + 1;
            let next;
            let day_order_status;
            let hour = (new Date().getHours())*3600000;
            let minutes = (new Date().getMinutes())*60000;
            let second =(new Date().getSeconds())*1000;
            let total = hour + minutes + second;
            let max = (86400020-total);
            if(prev === 5)
            {
              next=1;
            }
            else
            {
              next = recent;
            }
            console.log(next);
              DayOrder_History.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, prev_dayorder_history){
                if(err)
                {

                }
                else if(!prev_dayorder_history)
                {
                  day_order_status = "online";
                  let renew_cancel_status = false;
                  update_without_complexity(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else if(prev_dayorder_history)
                {
                  day_order_status = "online";
                  let renew_cancel_status = false;
                 if(prev_dayorder_history.day_order_status === "weekend")
                {
                  updatedy(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else if(prev_dayorder_history.cancel_status === true)
                {
                  cancelup(prev_dayorder_history.day_order,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
                else
                {
                  update_without_complexity(prev_dayorder_history.day_order,next,day_order_status,day,month,year,max,week,renew_cancel_status)
                }
              }

              })
            }
        }

      });

}

 function updatedy(prev_dayorder,next,day_order_status,day,month,year,max,week,cancel_status)
 {
   Common_DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: next,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(next,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function cancelup(prev_dayorder,day_order_status,day,month,year,max,week,cancel_status)
 {
   Common_DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: prev_dayorder,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(prev_dayorder,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function update_without_complexity(prev_dayorder,today,day_order_status,day,month,year,max,week,cancel_status)
 {
   Common_DayOrder.updateOne({day_order: prev_dayorder},{ $set: { day_order: today,day_order_status:day_order_status,day:day,month:month,year:year,time: max,week:week,cancel_status:cancel_status}} ,(err, newdata) => {
     dayorder_history_insert(today,day,month,year,week,day_order_status)
     nextcall();
   })
 }

 function dayorder_history_insert(day_order,day,month,year,week,day_order_status,cancel_status)
 {
   DayOrder_History.findOne({day_order : {"$exists" : true}}, function(err, user){
     const dayhistory = new DayOrder_History({
       day_order: day_order,
       day:day,
       month:month,
       year:year,
       day_order_status:day_order_status,
       week:week,
       cancel_status:cancel_status,
     });
     dayhistory.save(() => {
     });
     return;
   })
 }

function nextcall()
{
  logout();
}

function logout()
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/";
  MongoClient.connect(url,{ useUnifiedTopology: true },{ useNewUrlParser: true } , function(err, db) {
    if (err) throw err;
    var dbo = db.db("eWork");
    dbo.collection("sessions").drop(function(err, delOK) {
      if (err)
      {
        throw err;
      }
      else if (delOK)
      {
        console.log("Collection deleted");
        Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
          nextGo(user.time);
        })
      }
      db.close();
    });
  });
}
clearInterval(timer);



/* ---------------------------------------------Fetch Signup Page Render Status-------------------------*/
router.get('/fetch_render_status', function(req, res) {
  AdminOrder.findOne({usertype:'fac'}, function(err, result){
    if(err)
    {

    }
    else if(result)
    {
      res.send(result.registration_status)
    }
  })
})



router.post(
    '/faclogin',(req, res, next) => {

      const { username} = req.body;
      User.findOne({ username: username }, function(err, objs){
        if(err)
        {
          // console.log(err)
        }
        else if(objs)
        {
         if (objs.count === 0)
          {
              User.updateOne({ username: username }, { $inc: { count: 1 }},(err, user) => {
              })

          }
          else if (objs.count === 1)
          {
              User.updateOne({ username: username }, { count: 2 },(err, user) => {

              })
          }
          else
          {
            return;
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)



router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})




/* -----------------------------------------Fetching Request From Nav Bar ------------------------*/
  router.get('/fetchnav',function(req,res) {
    Nav.find({ }, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

  /*------------------------------------Notification----------------------------------------------*/
  router.get('/fetch_notification', function(req, res) {
    Notification.find({for:req.user.username}, function(err, passed){
      if(err)
      {

      }
      else if(passed)
      {
        res.send(passed)
      }
    })
    });

    router.post('/clear_all_notification', function(req, res) {
      Notification.deleteMany({for:req.user.username},function(err,succ){
        if(err)
        {

        }
        else if(succ)
        {
          res.send('done')
        }
      });
      });

      router.post('/clear_one_notification', function(req, res) {
        Notification.deleteOne({$and:[{for:req.user.username},{serial:req.body.id}]},function(err,succ){
          if(err)
          {

          }
          else if(succ)
          {
            res.send('done')
          }
        });
        });

    /*----End Notification*/


/*--------------------------------------To Set the color of the 10 Cards------------------ */

router.post('/fetchfrom', function(req, res) {
  console.log(req.body)
  const {slot_time,day,month,year} = req.body;
  Timetable.findOne({$and: [{day_slot_time : slot_time},{username: req.user.username},{date:day},{month:month},{year:year}]}, function(err, passed){
    res.send(passed);
  })
  });

/*------------------------To Render Slots in 10 Cards in Timetable---------------------------*/

  router.post('/fetchme', function(req, res) {
        Alloted_Slot.findOne({$and: [{timing : req.body.slot_time},{username: req.user.username}]}, function(err, passed){
            res.send(passed)
        })
      });

      router.post('/knowabsent', function(req, res) {
        Absent.findOne({official_id:req.user.username},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {
            // console.log('yeah')
            Alloted_Slot.findOne({$and: [{timing : req.body.slot_time},{username: req.user.username}]}, function(err, passed){
                if(err)
                {

                }
                else if(passed)
                {
                  res.send('yes');
                }
                else{
                  res.send('no');
                }
            })
          }
          else{
            res.send('no');
          }
        })
          });


          router.post('/extra_slot', function(req, res) {
            var day = Date.parse("today").toString("dd");
            var month = Date.today().toString("M");
            var year = Date.today().toString("yyyy");
          			Timetable.findOne({$and: [{problem_compensation_date:day},{username: req.user.username},{month:month},{year:year}
                ,{problem_compensation_hour:req.body.hour}]},
                  function(err, passed){
          					if(err)
          					{

          					}
          					else if(passed)
          					{
          						res.send('yes');
          					}
          					else{
          						res.send('no');
          					}
          			})
          		});



/*---------------------------------------------------Today's Day Order Fetch----------------------------------- */
  router.get('/fetchdayorder', function(req, res) {
    if(!req.user)
    {
      res.send("Not")
    }
    else
    {
      var day = Date.parse("yesterday").toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      DayOrder_History.findOne({$and: [{username: req.user.username},{day:day},{month:month},{year:year},{day_order_status:"cancel"}]}, function(err, user){
        if(err)
        {

        }
        else if(user)
        {
          var cancel ={
            cancel_day: user.day_order,
          }
          res.send(cancel);
        }
        else
        {
          Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
            if(err){

            }
            else if(user)
            {
                    var day_order ={
                      day_order: user.day_order,
                    }
                    res.send(day_order);

            }
          });
        }

        })
    }
    });
/*--------------------------------------------------YesterDay's Common_DayOrder Fetch----------------------------------------------*/
router.post('/fetch_yesterday_dayorder', function(req, res) {
  DayOrder_History.findOne({$and: [{username: req.user.username},{day:req.body.day},{month:req.body.month},{year:req.body.year}]}, function(err, started){
    if(err)
    {

    }
    else if(started)
    {
      var stock ={
        stock: started.day_order,
      }
      res.send(stock);
    }
    else
    {
      Common_DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err){

        }
        else if(user)
        {
                var initial ={
                  initial: user.day_order,
                }
                res.send(initial);
        }
      });
    }

    })
  });


  /*----------------------------------------End---------------------------- */

    router.post('/fetchfromtimetable', function(req, res) {
      if(!req.user)
      {
        res.send("Not")
      }
      else
      {
      const {day_slot_time} = req.body;
                  Alloted_Slot.findOne({$and: [{timing: day_slot_time},{username: req.user.username}]}, function(err, passed){
                    if(passed)
                    {
                      var alloted_slots ={
                        alloted_slots: passed.alloted_slots,
                      }
                      res.send(alloted_slots);
                    }
                    else{
                      var message ={
                        message: "There is no Saved Data for This Slot",
                      }
                      res.send(message);
                    }
                  })
          }
      });


router.post('/', (req, res) => {
    User.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          const newUser = new User({
              active:false,
              suspension_status:false,
              username: req.body.username,
              password: req.body.password,
              title: req.body.title,
              name: req.body.name,
              mailid: req.body.mailid,
              phone: req.body.phone,
              campus: req.body.campus,
              dept: req.body.dept,
              desgn: req.body.desgn,
              dob: req.body.dob,
              count: 0,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              blue_allot:0,
              h_order: req.body.h_order,
              })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})


router.post('/newd', (req, res) => {

    const {up_username,up_phone,up_name,up_dob} = req.body;
    User.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            User.updateOne({username:req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          User.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)

/*---------------------------------------------reseting password by mail ------------------------------- */
router.post('/reset_from_mail', (req, res) => {
  const {token_come,password} = req.body;
  User.findOne({ resetPasswordToken: token_come }, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if(user) {
        let hash = bcrypt.hashSync(password, 10);
          User.updateOne({resetPasswordToken: token_come},{password: hash} ,(err, user) => {
            var succ= {
              succ: "Password Updated!!"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "failed"
          };
          res.send(fail);
        }
      })
    })


/*End of Signup------------------------------------------------------------------------------ */



/* -------------------------------Degree & Certificates--------------------------------------------------------------*/
router.post('/profile1', (req, res) => {
          var tran = new Degrees(req.body.data);
          tran.save((err, savedUser) => {
            var succ= {
              succ: "Successfully Submitted"
            };
            res.send(succ);
          })
})



router.post('/editprofile1', function(req,res) {
  Degrees.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Edited"
    };
    res.send(succ);
  })

});


router.post('/fetchprofile1',function(req,res) {
  if(req.user)
  {
  Degrees.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
  if(err)
  {

  }
  else
  {
  res.send(docs)
  }
  });
}
})

router.post('/fetch_degree_for_pdf',function(req,res) {
  if(req.user)
  {
    let username;
    if(req.body.username)
    {
      username =  req.body.username;
    }
    else{
      username =  req.user.username;
    }
  Degrees.find({username: username}, function(err, docs){
  if(err)
  {

  }
  else
  {
  res.send(docs)
  }
  });
}
})

router.post('/fetchtoedit',function(req,res) {
Degrees.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/profile1del',function(req,res) {
  Degrees.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
})


/*End of PROFILE1-------------------------------------------------------------------------------- */

router.get('/knowcount', function(req, res) {
  if(req.user)
  {
    User.findOne({ username: req.user.username }, function(err, objs){

        if (objs)
        {
            res.send(objs);
        }
    });
}
    });


router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    // console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })
})


router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

router.post('/dash2', function(req, res) {
  if(req.user)
  {
  let username;
  if(req.body.username)
  {
    username = req.body.username;
  }
  else{
    username = req.user.username;
  }
    User.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});

    router.post('/timeallot', (req, res) => {
      // console.log(req.body)
        const { day_slot_time, selected,day_order,order,date,month,year,week} = req.body;
              Timetable.findOne({$and: [{day_slot_time: day_slot_time},{username: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
                  if (err)
                  {
                      return;
                  }
                  else if(objs)
                  {

                  }
                  else if (!objs) {
                            const newTime = new Timetable({
                              freefield:"Academic",
                              username: req.user.username,
                              freeparts:req.body.freeparts,
                              day_order: day_order,
                              day_slot_time:day_slot_time,
                              date: date,
                              month: month,
                              year: year,
                              order: order,
                              selected:selected,
                              time:req.body.time,
                              slot:req.body.slot,
                              problem_statement: req.body.problem_statement,
                              problem_compensation_date: req.body.compday,
                              problem_compensation_month: req.body.compmonth,
                              problem_compensation_year: req.body.compyear,
                              problem_compensation_hour: req.body.comphour,
                              covered: req.body.covered,
                              week:week,
                              day_sl:req.body.day_sl,
                            })
                            newTime.save((err, savedUser) => {
                              var succ= {
                                succ: "Success"
                              };
                              res.send(succ);
                            });
                          }
            });

  })


router.post('/root_percentage_fetch',(req,res)=>{
  Admin_Operation.find({$and:[{action:req.body.action},{usertype:req.user.desgn}]},(err,obj)=>{
    if(err)
    {

    }
    else if(obj)
    {
      res.send(obj);
    }
  })
})
router.post('/free', (req, res) => {
  const { day_slot_time,freeslot,freefield, order,date,month,year,week } = req.body;
  Extra_Handle.findOne({$and: [{dayorder_hour: day_slot_time},{action:'Foreign_ExtraCompleted'},{handled_by: req.user.username},{day: date},{month: month},{year: year},{week:req.body.week}]}, (err, data) => {
    if(err)
    {

    }
    else if(data)
    {
      var already_have= {
        already_have: "You have already submitted data on behalf of Foreign Request !!"
      };
      res.send(already_have);
    }
    else{
      Timetable.findOne({$and: [{day_slot_time: day_slot_time},{username: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
          if (err)
          {

          }
          else if (objs)
          {

          }
          else if(!objs) {
            Timetable.countDocuments({$and:[{freeparts:req.body.freeparts},{freefield:req.body.freefield},{username:req.user.username},{week:req.body.week}]}, function(err, c) {
                 if(err)
                 {

                 }
                 else if(c)
                 {
                     Admin_Operation.findOne({$and:[{responsibilty_title:req.body.freeparts},{usertype:req.user.desgn},{action:req.body.freefield}]},(err,found)=>{
                       if(err)
                       {

                       }
                       else if(found)
                       {
                         if(found.responsibilty_percentage === c)
                         {
                           var suc= {
                             suc: "Field,you are going to submit has already reached to it's max!!"
                           };
                           res.send(suc);
                         }
                         else
                         {
                           send()
                         }
                       }
                       else
                       {
                       }
                     })
                 }
                 else
                 {
                   send()
                 }
            });
              }
            })
    }
  })


        function send()
        {
        const newTime = new Timetable({
          date: date,
          month: month,
          year: year,
          username: req.user.username,
          day_slot_time:day_slot_time,
          order: order,
          freefield: freefield,
          freeslot: freeslot,
          freeparts: req.body.freeparts,
          week:week,
          time:req.body.time,
          slot:req.body.slot,
          day_sl:req.body.day_sl,
        })
        newTime.save((err, savedUser) => {
          var succ= {
            succ: "Successfully Submitted !!"
          };
          res.send(succ);
        });
        }
})

    router.post('/absent_insert', (req, res) => {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      Date.prototype.getWeek = function () {
          var onejan = new Date(this.getFullYear(), 0, 1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
      };

      var today = new Date.today();
      var week =today.getWeek();
      Timetable.findOne({$and:[{username:req.user.username},{date:day},{month:month},{year:year},{week:week},{freefield:'Academic'}]},(err,data)=>{
        if(err)
        {

        }
        else if(data)
        {
          Timetable.deleteMany({$and:[{username:req.user.username},{date:day},{month:month},{year:year},{week:week},{freefield:'Academic'}]},(err,data)=>{
          })
        }
        else{
          var trans = new Absent({
            official_id:req.user.username,
            reason:req.body.absent_reason,
            week:week,
            day:day,
            month:month,
            year:year,
          });
          trans.save((err, savedUser) => {
            var succ= {
              succ: "Done"
            };
            res.send(succ);
          })
        }
      })

    })

    router.get('/knowabsent', (req, res) => {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      Absent.findOne({$and:[{official_id:req.user.username},{day:day},{month:month},{year:year}]},(err,found)=>{
        if(err)
        {

        }
        else if(found)
        {
          res.send('ok');
        }
      })
    })



    router.post('/forgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       User.findOne(
         {mailid: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'INVALID USER !!'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           // console.log(token);
           User.updateOne({mailid:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 1800000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Reset Password:  eWork', // Subject line
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 30min from now.</p>'
          };
          // console.log("Way to enter.......");
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              // console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })
    })


        router.get('/reset_password', function(req, res) {
          User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
            if (!user) {
              // console.log("Expired");
              var expire ={
                expire:"Link is Expired"
              }
              res.send(expire);
            }
            else if(user){
              res.send(user.resetPasswordToken)
            }

          });
        });


/*------------------------Blue Print Send-------------------------------------------------- */
router.post('/send_blue_print', (req, res) => {
  // console.log(req.body)
    const {alloted_slots,timing} = req.body
    User.findOne({ username: req.user.username}, (err, user) => {
      if(err)
      {

      }
      else if(user)
      {
        Admin_Operation.findOne({$and:[{action:'Academic'},{responsibilty_title:'Curriculam'},{usertype:req.user.desgn}]},(err,found)=>{
          if(err)
          {
            // console.log("No")
          }
          else if(found)
          {
            if(user.blue_allot === found.responsibilty_percentage)
            {
              var exceed = {
                exceed: "No of the maximum input reached!!",
              };
                res.send(exceed);
            }
            else{

                      Alloted_Slot.findOne({$and: [{timing: timing},{username: req.user.username}]}, (err, objs) => {
                        if(err)
                        {

                        }
                        else if(objs)
                        {
                          var emsg = {
                            emsg: "We have datas saved on the following Time !!",
                          };
                            res.send(emsg);
                        }
                        else{
                          const allottime = new Alloted_Slot({
                            alloted_slots: alloted_slots,
                            timing: timing,
                            username: req.user.username,
                          })
                          allottime.save((err, savedUser) => {
                            var succ= {
                              succ: "Success"
                            };
                            res.send(succ);
                          })
                          User.updateOne({username:req.user.username},{$inc:{blue_allot:1}},(err,done)=>{

                          })
                        }
                      })
            }
          }
        })
      }
    })
  })


/*--------------------------------Getting Percentages of Individual /// DashBorad///------------------------------------------------- */

router.post('/know_Complete_Percentage', (req, res) => {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  if(!req.user)
  {

  }
  else{
  Timetable.find({$and:[{freeparts:req.body.res},{username:req.user.username},{week:week}]},(err, c)=> {
    if(err)
    {

    }
    else if(c)
    {
      res.send(c);
    }
  })
}
})


router.get('/knowPercentageAd', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       Admin_Operation.find({$and:[{action:'Administrative'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
     }
   })
  }
})

router.get('/knowPercentageAc', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       Admin_Operation.find({$and:[{action:'Academic'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
     }
   })
  }
})
router.get('/knowPercentageRe', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       Admin_Operation.find({$and:[{action:'Research'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
     }
   })
  }
})



router.get('/knowRootPercentageAc', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       Admin_Operation.findOne({$and:[{action:'Academic'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
     }
   })
  }
})
router.get('/knowRootPercentageAd', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       Admin_Operation.findOne({$and:[{action:'Administrative'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
     }
   })
  }
})
router.get('/knowRootPercentageRe', (req, res) => {
  if(!req.user)
  {

  }
  else{
   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       Admin_Operation.findOne({$and:[{action:'Research'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
     }
   })
  }
})


router.get('/fetchAcademic_Actual', (req, res) => {
  if(!req.user)
  {

  }
  else{
       Admin_Operation.findOne({$and:[{action:'Academic'},{usertype:req.user.desgn}]},(err,obj)=>{
         if(err)
         {

         }
         else if(obj)
         {
           res.send(obj)
         }
       })
  }
})


router.get('/fetchAdmins_Actual', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  if(!req.user)
  {

  }
  else{
    Timetable.find({$and:[{freefield:"Administrative"},{username:req.user.username},{week:week},{month:month},{year:year}]},(err, c)=> {
      if(err)
      {

      }
      else if(c)
      {
        Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:'Administrative'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            let sum = output[0].totalthings;
            Timetable.find({$and:[{freefield:'Administrative'},{username:req.user.username},{week:week}]},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                // console.log(c.length)
                let result1 = ((c.length) /sum)*100;
                var result ={
                  result:result1,
                }
                res.send(result)
              }
            })
          }
        })
      }
    })
  }
})

router.get('/fetchRes_Actual', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  if(!req.user)
  {

  }
  else{
    Timetable.find({$and:[{freefield:"Research"},{username:req.user.username},{week:week},{month:month},{year:year}]},(err, c)=> {
      if(err)
      {

      }
      else if(c)
      {
        Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:'Research'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            let sum = output[0].totalthings;
            Timetable.find({$and:[{freefield:'Research'},{username:req.user.username},{week:week}]},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                // console.log(c.length)
                let result1 = ((c.length) /sum)*100;
                var result ={
                  result:result1,
                }
                // console.log(result)
                res.send(result)
              }
            })
          }
        })
      }
    })
  }
})



router.get('/fetchAcad_Actual', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  if(!req.user)
  {

  }
  else{
    Timetable.find({$and:[{freefield:"Academic"},{username:req.user.username},{week:week},{month:month},{year:year}]},(err, c)=> {
      if(err)
      {

      }
      else if(c)
      {
        Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:'Academic'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            let sum = output[0].totalthings;
            Timetable.find({$and:[{freefield:'Academic'},{freeparts:'Curriculam'},{username:req.user.username},{week:week}]},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                // console.log(c.length)
                let result1 = ((c.length) /sum)*100;
                var result ={
                  result:result1,
                }
                res.send(result)
              }
            })
          }
        })
      }
    })
  }
})
router.get('/fetchAdmins_Actual_month', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  if(!req.user)
  {

  }
  else{
    Timetable.find({$and:[{freefield:"Administrative"},{username:req.user.username},{month:month},{year:year}]},(err, c)=> {
      if(err)
      {

      }
      else if(c)
      {
        Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:'Administrative'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            let sum = output[0].totalthings;
            Timetable.find({$and:[{freefield:'Administrative'},{username:req.user.username},{month:month},{year:year}]},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                // console.log(c.length)
                let result1 = ((c.length) /(sum*4))*100;
                var result ={
                  result:result1,
                }
                res.send(result)
              }
            })
          }
        })
      }
    })
  }
})

router.get('/fetchRes_Actual_month', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  if(!req.user)
  {

  }
  else{
    Timetable.find({$and:[{freefield:"Research"},{username:req.user.username},{month:month},{year:year}]},(err, c)=> {
      if(err)
      {

      }
      else if(c)
      {
        Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:'Research'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            let sum = output[0].totalthings;
            Timetable.find({$and:[{freefield:'Research'},{username:req.user.username},{month:month},{year:year}]},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                // console.log(c.length)
                let result1 = ((c.length) /(sum*4))*100;
                var result ={
                  result:result1,
                }
                // console.log(result)
                res.send(result)
              }
            })
          }
        })
      }
    })
  }
})



router.get('/fetchAcad_Actual_month', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  if(!req.user)
  {

  }
  else{
    Timetable.find({$and:[{freefield:"Academic"},{username:req.user.username},{month:month},{year:year}]},(err, c)=> {
      if(err)
      {

      }
      else if(c)
      {
        Admin_Operation.aggregate([{ $match: {$and:[{usertype:req.user.desgn},{action:'Academic'}]}},{ $group: {_id: null,totalthings: { $sum: "$responsibilty_percentage" }}}],function(err,output){
          if(err)
          {

          }
          else if(output)
          {
            let sum = output[0].totalthings;
            Timetable.find({$and:[{freefield:'Academic'},{freeparts:'Curriculam'},{username:req.user.username},{month:month},{year:year}]},(err, c)=> {
              if(err)
              {

              }
              else if(c)
              {
                // console.log(c.length)
                let result1 = ((c.length) /(sum*4))*100;
                var result ={
                  result:result1,
                }
                res.send(result)
              }
            })
          }
        })
      }
    })
  }
})


router.get('/fetch_other_extra_week', (req, res) => {
  if(req.user)
  {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Extra_Handle.find({$and:[{handled_by:req.user.username},{week:week},{month:month},{year:year}]},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      Extra.find({$and:[{faculty_id:req.user.username},{week:week},{month:month},{year:year}]},(err,count)=>{
        if(err)
        {

        }
        else if(count)
        {
          var data = {
            total:count.length,
            completed:found.length,
          }
          res.send(data)
        }
      })
    }
  })
}

})


router.get('/fetch_other_extra_month', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Extra_Handle.find({$and:[{handled_by:req.user.username},{month:month},{year:year}]},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      Extra.find({$and:[{faculty_id:req.user.username},{month:month},{year:year}]},(err,count)=>{
        if(err)
        {

        }
        else if(count)
        {
          var data = {
            total:count.length,
            completed:found.length,
          }
          res.send(data)
        }
      })
    }
  })

})

router.get('/fetch_absent_week', (req, res) => {
  if(req.user)
  {
  Date.prototype.getWeek = function ()
  {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };
  var today = new Date.today();
  var week =today.getWeek();
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Absent.find({$and:[{official_id:req.user.username},{week:week},{month:month},{year:year}]},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      res.send(found)
    }
  })
}

})


router.get('/fetch_absent_month', (req, res) => {
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  Absent.find({$and:[{official_id:req.user.username},{month:month},{year:year}]},(err,found)=>{
    if(err)
    {

    }
    else if(found)
    {
      res.send(found)
    }
  })

})






/*----------------------------------------------------------Master Table------------------------------------------ */

router.post('/fetchmaster', function(req, res) {
  if(req.user)
  {
  const {slot_time} = req.body;
  Timetable.findOne({$and: [{day_slot_time : slot_time},{username: req.user.username},{week:req.body.week}]}, function(err, passed){
    res.send(passed);
  })
  }
  else{
    res.send('no');
  }
  });


  /*----------------------------------------------------------View FOR USER---------------------------------------------------*/
  router.post('/authenticate', function(req,res) {
    var data1;
    User.findOne({username:req.body.username},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        if((req.user.h_order === 1) && !(found.h_order<=req.user.h_order))
        {
          data1 ={
            username:found.username,
            allowed:'Allowed',
          }
          res.send(data1)
        }
        else if((req.user.h_order>1) && (req.user.h_order<=5))
        {
          if(!(found.h_order<=1) || (found.h_order >=6))
          {
            data1 ={
              username:found.username,
              allowed:'Allowed',
            }
            res.send(data1)
          }
          else {
            res.send("Denied")
          }
        }
        else if(req.user.h_order === 6) {
          if((found.campus === req.user.campus) && (found.desgn !== req.user.desgn) && (found.h_order>req.user.h_order) )
          {
            data1 ={
              username:found.username,
              allowed:'Allowed',
            }
            res.send(data1)
          }
          else {
            res.send("Denied")
          }
        }
        else{
          res.send("Denied")
        }
      }
      else
      {
        res.send("User Not Found !!")
      }
    })
  });

  router.get('/validate_list_view', function(req,res) {
    if(req.user)
    {
    User.findOne({username:req.user.username},(err,found)=>{
      if(err)
      {

      }
      else if(found){
        var data_send;
        if(found.h_order === 1)
        {
          data_send ={
            data_send:found.h_order,
            permission:1,
          }
        }
        else if(found.h_order === 5)
        {
          data_send = {
            data_send:found.h_order,
            permission:1,
          }
        }
        else{
          data_send = {
            permission:0,
          }
        }
        res.send(data_send);
      }
    })
  }
  })

  router.post('/fetch_view_list',function(req,res){
    User.findOne({username:req.user.username},(err,data)=>{
      if(err)
      {

      }
      else if(data)
      {
        Admin_Operation.find({designation_order_no:{ $gt: req.body.start }},(err,found)=>{
          if(err)
          {

          }
          else if(found)
          {
            res.send(found);
          }
        })
      }
    })
  })

  router.get('/fetch_department',function(req,res){
    User.findOne({username:req.user.username},(err,data)=>{
      if(err)
      {

      }
      else if(data)
      {
        if(data.h_order === 5)
        {
          res.send([{department_name:data.dept}]);
        }
        else if(data.h_order === 1)
        {
          Admin_Operation.find({action:'Department'},(err,found)=>{
            if(err)
            {

            }
            else if(found)
            {
              res.send(found);
            }
          })
        }
      }
    })
  })

  router.post('/fetchUser',function(req,res){

    if(req.body){
    User.find({$and:[{desgn:req.body.designation},{dept:req.body.department},{campus:req.user.campus}]},(err,data)=>{
      if(err)
      {

      }
      else if(data)
      {
        res.send(data);
      }
    })
  }
  })

    router.post('/viewSection',function(req,res){
                Section.find({$and:[{action:req.body.section_ref},{username:req.body.username}]},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })


/*------------------------------------------------View For OwnData---------------------------*/
    router.post('/fetch_five_for_view',function(req,res){
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }
                Five.find({username:username},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })
    router.post('/fetch_adminis_for_view',function(req,res){
      if(req.user)
      {
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }

                Timetable.find({$and:[{username:username},{freefield:'Administrative'}]},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
              }
    })
    router.post('/fetch_research_for_view',function(req,res){
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }

                Timetable.find({$and:[{username:username},{freefield:'Research'}]},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })
    router.post('/fetch_academic_for_view',function(req,res){
      let username;
      if(req.body.username)
      {
        username = req.body.username;
      }
      else{
        username =  req.user.username;
      }

                Timetable.find({$and:[{username:username},{freefield:'Academic'}]},(err,found)=>{
                  if(err)
                  {

                  }
                  else if(found)
                  {
                    res.send(found);
                  }
                })
    })




  /*-------------------End of View-----------*/


  /*--------------------------------------------EXTRA CODE---------------------------*/

  router.post('/check_possibility',function(req,res){
            Date.prototype.getWeek = function () {
                var onejan = new Date(this.getFullYear(), 0, 1);
                return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
            };

            var myDate = new Date.today();
            var week =myDate.getWeek();
            var day = Date.today().toString("dd");
            var month = Date.today().toString("M");
            var year = Date.today().toString("yyyy");

    User.findOne({$and:[{username:req.body.id},{desgn:req.user.desgn}]},(err,found)=>{
      if(err)
      {

      }
      else if(found){
        Alloted_Slot.findOne({$and:[{timing:req.body.day_order+'.'+req.body.hour},{username:req.body.id}]},(err,avail)=>{
          if(err)
          {

          }
          else if(avail)
          {
            res.send('no');
          }
          else{
            Timetable.findOne({$and:[{username:req.body.id},{week:week},{day_slot_time:req.body.day_order+'.'+req.body.hour},{date:day},{month:month},{year:year}]},(err,ok)=>{
              if(err)
              {

              }
              else if(ok)
              {
                res.send('no');
              }
              else{
                  Alloted_Slot.findOne({$and:[{timing:req.body.day_order+'.'+req.body.hour},{username:req.user.username}]},(err,having)=>{
                    if(err)
                    {

                    }
                    else if(having)
                    {
                      res.send(having.alloted_slots);
                    }
                    else{
                      res.send('no');
                    }
                  })
              }
            })
          }
        })
      }
      else{
        console.log('In user')
        res.send('no');
      }
    })

  })


  router.post('/add_handover_slots', function(req,res) {
    Extra.findOne({$and:[{faculty_id:req.body.data.faculty_id},{order_from:req.body.data.order_from},{hour:req.body.data.hour},{day:req.body.data.day},{month:req.body.data.month},{year:req.body.data.year}]},(err,found)=>{
      if(err)
      {

      }
      else if(found)
      {
        res.send('no')
      }
      else{
            var trans = new Extra(req.body.data);
            trans.save((err, savedUser) => {
              if(err)
              {

              }
              else if(savedUser)
              {
                var succ= {
                  succ: "Done"
                };
                res.send(succ);
                var tran = new Notification({
                  for:req.body.data.faculty_id,
                  subject:'Slot Handle Request from Other Faculty',
                  details:req.user.username+' has requested you to handle his/her alloted slot. Kindly go to Extra and choose Request Fro Other Faculty,you will able to see all the details. Remember you have to submit within today.'
                });
                tran.save()
              }
            })
          }
        })
  })


  router.post('/fetch_handover_slots',function(req,res) {
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
      Extra.find({$and: [{action:req.body.action},{order_from: req.user.username},{day:day},{month:month},{year:year}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
        else{
          return;
        }
    });
  })
  router.post('/del_handover_slots',function(req,res) {
    Extra.deleteOne({$and: [{serial:  req.body.serial },{order_from: req.user.username}]},function(err,succ){
      res.send("OK")
    });
  });

    router.post('/fetchCovered',function(req,res) {
        Extra_Handle.find({$and: [{handled_by:req.body.faculty_id},{completed_for: req.user.username},{slot:req.body.slot}]}, function(err, docs){
          if(err)
          {

          }
          else
          {
            res.send(docs)
          }
      });
    })



/*-----------------------------------Fetch OWN EXTRA--------------------------------------------*/

router.get('/fetchcompense',function(req,res) {
  if(req.user)
  {
  var day = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
    Timetable.find({$and: [{selected:'Problem'},{username: req.user.username},{problem_compensation_date:day},{month:month},{year:year}]}, function(err, docs){
      if(err)
      {

      }
      else if(docs)
      {
        res.send(docs)
      }
  });
}
})
/*----fetch foreign----*/

  router.post('/fetch_foreign_slots',function(req,res) {
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
      Extra.find({$and: [{action:'handover_slots'},{faculty_id: req.user.username},{day:day},{month:month},{year:year}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          console.log('ok')
          res.send(docs)
        }
    });
  })

  router.post('/completed_foreign_hour',function(req,res) {
    var day = Date.today().toString("dd");
    var month = Date.today().toString("M");
    var year = Date.today().toString("yyyy");
    Date.prototype.getWeek = function ()
    {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };
    var today = new Date.today();
    var week =today.getWeek();
    const extra = new Extra_Handle({
      action:'Foreign_ExtraCompleted',
      completed_for:req.body.req_from,
      handled_by:req.user.username,
      handledtopic:req.body.coveredTopic,
      dayorder_hour:req.body.dayorder+'.'+req.body.hour,
      slot:req.body.slot,
      day:day,
      week:week,
      month:month,
      year:year,
    })
    extra.save((err, saved) => {
      if(err)
      {

      }
      else if(saved){

        Extra.updateOne({$and: [{serial:req.body.serial},{faculty_id: req.user.username}]},{status:true}, function(err, updated){
          if(err)
          {

          }
          else if(updated)
          {
           res.send('ok');
          }
      });
      }

    })
  })


  /*-------End EXTRA---*/

/*--------------------------------------------CRUD CODE--------------------------------------------- */
router.post('/fetch_section',function(req,res){
            Admin_Operation.find({action:'Section'},(err,found)=>{
              if(err)
              {

              }
              else if(found)
              {
                res.send(found);
              }
            })
})
  router.post('/addmm', function(req,res) {
            var trans = new Section(req.body.data);
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
  });

    router.post('/editt', function(req,res) {
      Section.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
        var succ= {
          succ: "Successful SignedUP"
        };
        res.send(succ);
      })

    });


router.post('/fetchall',function(req,res) {
  if(req.user)
  {
  console.log(req.body.action)
    Section.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
  });
}
})
router.post('/fetch_section_for_view',function(req,res) {
  if(req.user)
  {
  let username;
  if(req.body.username)
  {
    username= req.body.username;
  }
  else{
    username = req.user.username;
  }
  console.log(req.body.username)
    Section.find({username: username}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
  });
}
})

router.post('/fetcheditdata',function(req,res) {
  Section.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/del',function(req,res) {
  Section.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
});
/*------------------------------------------------Code For Manage Page----------------------------------- */
router.post('/request_for_entry',function(req,res) {
  DayOrder_History.findOne({$and: [{day:req.body.day},{month: req.body.month},{year: req.body.year},{day_order:req.body.day_order},{day_order_status: 'online'}]}, function(err, present){
    if(err)
    {
      console.log(err)
    }
    else if(present)
    {
      Request.findOne({$and: [{username:req.user.username},{day:req.body.day},{month: req.body.month},{year: req.body.year}]}, function(err, passed){
        if(err)
        {
          console.log(err)
        }
        else if(passed)
        {
          var handled;
          if(passed.action === "approved")
          {
          handled= {
            handled: "Request Already Approved !!"
          };
          res.send(handled);
          }
          else
          {
            handled= {
              handled: "Admin Has Not Approved Till Now!!"
            };
            res.send(handled);
          }
        }
        else{
          var requests = new Request(req.body);
          requests.save((err, savedUser) => {
            var succ= {
              succ: "We have sent your request!!"
            };
            res.send(succ);
          })
        }
      })
    }
    else
    {
      var noday=
      {
        noday:"Invalid Request!!"
      }
      res.send(noday)
    }
  })
})


router.get('/fetch_requested_list',function(req,res) {
  Request.find({username: req.user.username}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
  else
  {
    res.send(docs)
  }
 });
})


router.post('/fetch_requested_status',function(req,res) {
  Request.findOne({$and:[{username: req.user.username},{serial:req.body.serial}]}, function(err, docs){
  if(err)
  {
    console.log(err)
  }
  else if(docs)
  {
    if(docs.action ==="approved")
    {
      res.send("approved")
    }
    else if(docs.action ==="denied")
    {
      res.send("denied")
    }
    else if(docs.action ==="false")
    {
      res.send("pending")
    }
  }
  else
  {

  }
});
})


/*----------------------------------------Five Year Plan--------------------------------------------- */


function knowstat(user,body)
{
  var year = Date.today().toString("yyyy");
  var year_int = parseInt(year);
  Five.find({$and: [{username:user},{expired:false},{expire_year:year_int}]},function(err,docs){
    if(docs)
    {
      Five.updateMany({$and :[{username:user},{expire_year: year_int}]},{expired: true},function(err,done){
        if(done)
        {
        console.log("done")
        }
        return;
      })
    }
  })
}

router.post('/update_status_fiveyear', function(req,res) {
  Five.updateOne({$and :[{ serial: req.body.key},{username: req.user.username}]},{completed:req.body.status}, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
  })

});

router.post('/fetch_update_status_fiveyear', function(req,res) {
  Five.findOne({$and :[{ serial: req.body.key},{username: req.user.username}]},{completed:req.body.status}, (err, user) => {
    var succ= {
      succ: "Updated"
    };
    res.send(succ);
  })

});
router.post('/fetch_complete_fiveyear', function(req,res) {
  Five.findOne({$and :[{ serial: req.body.serial},{username: req.user.username}]}, (err, user) => {
    if(err)
    {

    }
    else if(user)
    {
        res.send(user.completed);
    }
  })

});
    router.post('/add_five_year_plan', function(req,res) {
              var fives = new Five(req.body);
              fives.save((err, savedUser) => {
                console.log("save")
                res.send("Saved");
              })
    });




          router.post('/edit_five_year_plan', function(req,res) {
            Five.updateOne({$and :[{ serial: req.body.serial},{username: req.user.username}]},req.body, (err, user) => {
              var succ= {
                succ: "Updated"
              };
              res.send(succ);
            })

          });


      router.post('/fetch_five_year_plan',function(req,res) {
        if(req.user)
        {
        knowstat(req.user.username,req.body);
        Five.find({$and: [{action: req.body.action},{username: req.user.username},{expired:false}]}, function(err, docs){
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
      });
    }
    else{
      res.send('no')
    }
      })


      router.post('/fetch_five_year_existing_data',function(req,res) {
        console.log(req.body)
        Five.findOne({$and:[{serial: req.body.id},{username:req.user.username}]}, function(err, docs){
          // console.log(docs)
        if(err)
        {

        }
        else if(docs)
        {
          res.send(docs)
        }
      });
      })

      router.post('/del_five_year_plan',function(req,res) {
        Five.remove({$and: [{serial:  req.body.serial },{username:req.user.username}]},function(err,succ){
          res.send("OK")
        });
      })

      /*-------------------------------------Declare a Holiday---------------------------------------------- */

      router.post('/declare_cancel_or_holiday',function(req,res) {
        var next_dayorder;
        console.log(req.body)
        Common_DayOrder.findOne({$and: [{day:req.body.cday},{month: req.body.cmonth},{year:req.body.cyear},{cancel_status: false}]}, function(err, result){
           if(err)
           {
             return;
           }
           else if(result)
           {
             if(result.day_order === 1)
             {
               next_dayorder = 5;
             }
             else
             {
               next_dayorder = result.day_order - 1;
             }
                Common_DayOrder.updateOne({day_order: result.day_order},{ $set: { day_order: next_dayorder,cancel_status: true}} ,(err, newdata) => {
                })
                DayOrder_History.updateOne({day_order: result.day_order},{ $set: { day_order: next_dayorder,cancel_status: true}} ,(err, newdata) => {
                })

            res.send("Updated!!")
           }
           else if(!result)
           {
             res.send("It's Already Updated!!")
           }
        })

      })



      /*----------------------------------------------------------Start of Department-Admin Operation----------------------------- */
      router.get('/fetch_request', (req, res) => {
           User.find({$and:[{active:false},{campus:req.user.campus},{dept:req.user.dept}]}, (err, user) => {
             if(err)
             {

             }
             else if(user)
             {
               res.send(user);
             }
           })
        })

        router.get('/fetch_signup_request_for_admin', (req, res) => {
             User.find({active:false}, (err, user) => {
               if(err)
               {

               }
               else if(user)
               {
                 res.send(user);
               }
             })
          })

        router.post('/active_user', (req, res) => {
             User.updateOne({username:req.body.username},{active:true}, (err, user) => {
               if(err)
               {

               }
               else if(user)
               {
                 res.send('done');
               }
             })
          })





      /*----------------------------------------------End of Department-Admin--------------------------------------------------------*/


/*----------------------------------------------------------Start of Admin Operation----------------------------- */

/*----------------------------------------------Admin Operation--------------------------------------------------------*/
router.post('/auth', (req, res) => {
  const {id,username,password} = req.body;

   if(req.body.id === "2019")
   {
     User.findOne({username:username}, (err, user) => {
       if(user)
       {
         var existadmin ={
           existadmin:'You already have an account !!',
         }
         res.send(existadmin);
       }
       else
       {
         const newadmin = new User({
           suspension_status:false,
           username:username,
           password: password,
           count: 4,
           resetPasswordExpires:'',
           resetPasswordToken:'',
           h_order:0,
         })
         newadmin.save((err, savedUser) => {
           var succ= {
             succ: "Successful SignedUP"
           };
           res.send(succ);
         })
       }
     })
  }
  else
  {
    var wrong ={
      wrong:'Passcode is Wrong !!'
    }
    res.send(wrong)
  }
  })

  router.post('/insert_dept_admin', (req, res) => {
       User.findOne({$and:[{username:req.body.data.username},{h_order:0.5}]}, (err, user) => {
         if(err)
         {

         }
         else if(user)
         {
           res.send('no');
         }
         else
         {
           const newadmin = new User(req.body.data)
           newadmin.save((err, savedUser) => {
             if(err)
             {

             }
             else if(savedUser)
             {
               mailsend(req.body.data.mailid,res,req.body.data.username)
             }
           })
         }
       })
    })

    function mailsend(mailid,res,name)
    {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
           const token = crypto.randomBytes(20).toString('hex');
           User.updateOne({$and:[{mailid:mailid},{h_order:0.5}]},{ $set: {deptToken:token}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Team eWork : Department Admin Choosen', // Subject line
            html: '<p>Dear '+name+',</p><br /><p>You are receiving this message from SRM CARE,eWork team.You have been choosen as a department admin for eWork. In order to validate yourself kindlly navigate '+'<a href="http://localhost:3000/department_admin">Here</a> </p><br /><p>Please use this code <b>'+token+'</b> at the time of signup.</p><br />Thank You..'
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else
            {
              res.send('ok');
            }
         });

    }

    router.post('/response_from_deptadmin', (req, res) => {
      let password = bcrypt.hashSync(req.body.password, 10);
         User.findOne({$and:[{username:req.body.username},{deptToken:req.body.id},{h_order:0.5}]}, (err, user) => {
           if(err)
           {

           }
           else if(user)
           {
             User.updateOne({$and:[{username:req.body.username},{deptToken:req.body.id}]},{$set :{password:password,deptToken:'',suspension_status:false,active:true}}, (err, done) => {
               if(err)
               {

               }
               else if(done)
               {
                 res.send('ok');
               }
             })
           }
           else if(!user)
           {
             res.send('no');
           }
         })
      })


    router.post('/addnav', function(req,res) {
              var trans = new Nav(req.body.data);
              trans.save((err, savedUser) => {
                var succ= {
                  succ: "Done"
                };
                res.send(succ);
              })
    });


      router.post('/editnav', function(req,res) {
        Nav.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
          var succ= {
            succ: "Updated"
          };
          res.send(succ);
        })

      });


  router.post('/fetchnav',function(req,res) {
    Nav.find({action: req.body.action}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })
  router.post('/edit_existing_nav',function(req,res) {
    Nav.findOne({serial: req.body.id}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

  router.post('/delnav',function(req,res) {
    Nav.remove({serial:  req.body.serial },function(err,succ){
      res.send("OK")
    });
  })

  /*---------------------------------------CRUD FOR ADDING Responsibility CODE______________________________________________________________ */
  router.post('/add_from_insert_in_admin', function(req,res) {
        var respon = new Admin_Operation(req.body.data);
        respon.save((err, savedUser) => {
          var succ= {
            succ: "Done"
          };
          res.send(succ);
        })
});


router.post('/edit_inserted_data', function(req,res) {
Admin_Operation.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
var succ= {
  succ: "Updated"
};
res.send(succ);
})

});


router.post('/fetch_in_admin',function(req,res) {
Admin_Operation.find({$and:[{action: req.body.action},{usertype:req.body.usertype}]}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})


router.post('/fetch_for_edit',function(req,res) {
Admin_Operation.findOne({serial: req.body.id}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/del_inserted_data',function(req,res) {
Admin_Operation.remove({serial:  req.body.serial },function(err,succ){
res.send("OK")
});
})


  router.get('/getrequest',function(req,res) {
    Request.find({action: "false"}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

    router.get('/fetch_denied_list',function(req,res) {
      Request.find({action: "denied"}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
    });
    })


  router.post('/approve_request',function(req,res) {
    console.log(req.body)
    Timetable.findOne({$and:[{username:req.body.username},{date:req.body.day},{month: req.body.month},{year: req.body.year}]}, function(err, docs){
    if(err)
    {

    }
    else if(docs)
    {

      res.send("yes")
    }
    else
    {
      res.send("no")
    }
  });
  })



    router.post('/denyrequest',function(req,res) {
      Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},{action: "denied"},function(err,done){
          res.send("Done")
        })
      }
    });
    })



    router.post('/approverequest',function(req,res) {

    Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},{action: "approved"},function(err,done){
          res.send("Done")
        })
      }
    });
    })


    router.post('/handleFaculty_Registration',function(req,res) {

    AdminOrder.findOne({usertype: req.body.history}, function(err, docs){
      if(err)
      {

      }
      else if(docs)
      {
        AdminOrder.updateOne({usertype:req.body.history},{registration_status: req.body.checked},function(err,done){
          res.send("Done")
        })
      }
      else{
        var trans = new AdminOrder({
          usertype:req.body.history,
          registration_status: req.body.checked
        }
        );
        trans.save((err, savedUser) => {
          var succ= {
            succ: "Done"
          };
          res.send(succ);
        })
      }
    })
    })



    router.get('/fac_reg_status', (req, res, next) => {
      AdminOrder.findOne({usertype:'fac'},function(err,result){
        if(err)
        {

        }
        else if(result)
        {
          res.send(result.registration_status)
        }
      })
    })

    /*----------------------------------Admin Operation for opening a set of dayorder availbale to everyone----------------- */
    /*-----Validate Admin---------------------------------------- */
    router.post('/checkadmin',function(req,res){
      User.findOne({username:req.user.username},function(err,found){
        if(err)
        {

        }
        else if(found)
        {
          if(bcrypt.compareSync(req.body.password, found.password)) {
            var succ ={
              succ:"Matched"
            }
            res.send(succ)
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!found)
        {

        }
      })
    })
 /*----------------------------------------Delete User ------------------------------------------------------ */

 router.post('/deleteadmin',function(req,res){
   User.findOne({username:req.body.username},function(err,found){
     if(err)
     {

     }
     else if(found)
     {
       User.deleteOne({username:found.username},function(err,done)
       {
         if(err)
         {

         }
         else if(done)
         {
          res.send("Deleted")
         }
       })
     }
   })
 })


 router.post('/deleteuser',function(req,res){
  User.findOne({username:req.body.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      User.deleteOne({username:found.username},function(err,done)
      {
      })
      Request.deleteMany({username:found.username},function(err,done)
      {
      })
      Timetable.deleteMany({username:found.username},function(err,done)
      {
      })
      Five.deleteMany({username:found.username},function(err,done)
      {
      })
      Degrees.deleteMany({username:found.username},function(err,done)
      {
      })
      Alloted_Slot.deleteMany({username:found.username},function(err,done)
      {
      })
      res.send("Deleted")
    }
  })
})

/*------------------------------------------------------------------Modaify Part-----------------------------------------------*/
router.post('/handle_change_today_dayorder',function(req,res){
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      var day = Date.today().toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      Common_DayOrder.findOne({$and:[{day:day},{month:month},{year:year}]}, function (err, doc){
        if(err)
        {

        }
        else if(doc)
        {
          doc.day_order = req.body.day_order;
          doc.save();
          res.send("Common_DayOrder Updated!!")
        }
        else{
          res.send("Something went wrong !!")
        }
      })
      DayOrder_History.findOne({$and:[{day:day},{month:month},{year:year}]}, function (err, doc){
        if(err)
        {

        }
        else if(doc)
        {
        doc.day_order = req.body.day_order;
        doc.save();
        }
      })
    }
  })
})


router.post('/handle_start_of_semester',function(req,res){
  console.log(req.body.startday)
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      AdminOrder.findOne({order:'Start_Of_Semester'},function(err,foud)
      {
        if(err)
        {

        }
        else if(foud)
        {
          AdminOrder.update({order:'Start_Of_Semester'},{$set:{startday:req.body.startday,startmonth:req.body.startmonth,startyear:req.body.startyear}},function(err,data){
            res.send("Date is Updated !!")
          })
        }
        else
        {
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Saved Into the DB !!");
          })
        }
      })
    }
  })
})

router.post('/handle_end_of_semester',function(req,res){
  User.findOne({username:req.user.username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      AdminOrder.findOne({order:'End_Of_Semester'},function(err,foud)
      {
        if(err)
        {

        }
        else if(foud)
        {
          AdminOrder.update({order:'End_Of_Semester'},{$set:{endday:req.body.endday,endmonth:req.body.endmonth,endyear:req.body.endyear}},function(err,data){
            res.send("Date is Updated !!")
          })
        }
        else
        {
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Saved Into the DB !!");
          })
        }
      })
    }
  })
})

router.get('/fetch_start_date',function(req,res){
      AdminOrder.findOne({order:'Start_Of_Semester'},function(err,found)
      {
        if(err)
        {

        }
        else if(found)
        {
          res.send(found)
        }
        else
        {

        }
      })
})


router.get('/fetch_end_date',function(req,res){
  AdminOrder.findOne({order:'End_Of_Semester'},function(err,found)
  {
    if(err)
    {

    }
    else if(found)
    {
      res.send(found)
    }
    else
    {

    }
  })
})
router.post('/delete_start_of_sem',function(req,res){
  AdminOrder.remove({order:'Start_Of_Semester'},function(err,succ){
    res.send("Deleted !!")
  });
})

router.post('/delete_start_of_sem',function(req,res){
  AdminOrder.remove({order:'End_Of_Semester'},function(err,succ){
    res.send("Deleted !!")
  });
})

/*-----------------------------------------------Update Operations---------------------------------------------------------------*/
router.post('/update_faculty_password',function(req,res){
  console.log(req.body)
  User.findOne({username:req.body.faculty_username},function(err,found){
    if(err)
    {

    }
    else if(found)
    {
      let hash = bcrypt.hashSync(req.body.faculty_password, 10);
      User.updateOne({username:req.body.faculty_username},{password: hash} ,(err, done) => {
        if(err)
        {

        }
        else if(done)
        {
          res.send("Updated");
        }
      })
    }
    else
    {
      res.send("User Not Found !!")
    }
  })
})

/*----------------------------------------------Suspension Operation----------------------------------------------------------*/
router.post('/suspension_user',function(req,res){
  console.log(req.body)
  User.findOne({username:req.body.suspend_faculty_username},function(err,found){
    if(err)
    {
      res.status(404).send("error!!")
    }
    else if(found)
    {
      AdminOrder.findOne({$and:[{victim_username:req.body.suspend_faculty_username},{order:'Suspend User'}]},function(err,obj){
        if(err)
        {

        }
        else if(obj)
        {
          res.send("Already Suspended !!")
        }
        else{
          var suspend = new AdminOrder({order:'Suspend User',victim_username:req.body.suspend_faculty_username,reason:req.body.suspend_reason_faculty,suspend_status:true});
          suspend.save((err, savedUser) => {
            if(err)
            {

            }
            else if(savedUser)
            {
              User.updateOne({username:req.body.suspend_faculty_username},{suspension_status: true},function(err,done){
                if(err)
                {

                }
                else if(done)
                {
                  res.send("Suspended !!")
                }
              })
            }
          })
        }
      })
    }
    else{
      res.send("User Not Found !!")
    }
  })
})
router.post('/remove_user_suspension',function(req,res){
  console.log("Remove")
  User.findOne({username:req.body.suspend_faculty_username},function(err,found){
    if(err)
    {
      res.status(404).send("error!!")
    }
    else if(found)
    {
      AdminOrder.findOne({$and:[{victim_username:req.body.suspend_faculty_username},{order:'Suspend User'}]},function(err,obj){
        if(err)
        {

        }
        else if(obj)
        {
          User.updateOne({username:req.body.suspend_faculty_username},{suspension_status: false},function(err,done){
            if(err)
            {

            }
            else if(done)
            {
              obj.remove();
              res.send("Removed Suspension !!")
            }
          })
        }
        else{
          res.send("User Not Found !!")
        }
      })
    }
    else{
      res.send("User Not Found !!")
    }
  })
})

module.exports = router
