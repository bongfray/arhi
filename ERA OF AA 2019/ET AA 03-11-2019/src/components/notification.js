import React, { Component } from 'react'
import Nav from './dynnav'
import axios from 'axios'
import  { Button } from 'react-materialize'
import { Redirect } from 'react-router-dom'
import M from 'materialize-css'
import ContentLoader from "react-content-loader"

export default class Notification extends Component {
    constructor()
    {
        super()
        this.state={
            display:'',
            loader:false,
            home:'/faculty',
            logout:'/user/logout',
            get:'/user/',
            nav_route: '/user/fetchnav',
            order_action:'faculty',
            notification:[],
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      M.AutoInit()
      this.loggedin()
      this.fetchNoti()
    }
    loggedin = ()=>{
      axios.get('/user/')
      .then(res=>{
        if(res.data.user)
        {
          this.setState({username:res.data.user.username})
        }
        else{
          this.setState({redirectTo:'/faculty'})
           window.M.toast({html: 'You are not Logged In',classes:'rounded #ec407a pink lighten-1'});
        }
      })
    }
  fetchNoti = ()=>{
    axios.get('/user/fetch_notification')
    .then( res => {
        if(res.data.length === 0)
        {
          this.setState({notfound:'No Notification are there !!'})
        }
        else{
          this.setState({notification:res.data})
        }
    });
  }
    clearOne =(id)=>{
      const { notification } = this.state;
      axios.post('/user/clear_one_notification',{id:id})
          .then(response => {
            this.setState({
              response: response,
              notification: notification.filter(noti => noti.serial !== id)
           })
          })
    }
    clearAll = ()=>{
      axios.post('/user/clear_all_notification')
      .then( res => {
        this.setState({
          notification: [],
       })
      });
    }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
        return (
            <React.Fragment>
            <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

            {this.state.notification.map((content,index)=>{
              return(
                <div key={index}>
                <div className="row">
                <div className="card hoverable col l4 white-text" style={{background:'linear-gradient(to bottom, #ff99cc 0%, #000066 90%',marginLeft:'10px'}}>
                  <div className="card-tile center">
                                         <h6 className="yellow-text"><b>{content.subject}</b></h6>
                  </div>
                  <div className="card-content">
                       <p>{content.details}</p>
                  </div>
                                <React.Fragment>
                                <Button onClick={() => this.clearOne(content.serial)} style={{background:'linear-gradient(to bottom, #00ffff 0%, #cc33ff 100%'}} className="right btn-floating btn-large waves-effect waves-light" waves="light" tooltip="Dismiss"><i className="small material-icons">clear</i></Button>
                                </React.Fragment>
                </div>
                </div>
</div>
            );
            })}

            <div className="row">
<a href="#" onClick={this.clearAll} className="waves-effect tooltipped" data-position="bottom" data-tooltip="Clear All"><i className="red-text large material-icons">delete_forever</i></a>

    <div className="div"></div>
</div>

            </React.Fragment>
        );
      }
    }
}
