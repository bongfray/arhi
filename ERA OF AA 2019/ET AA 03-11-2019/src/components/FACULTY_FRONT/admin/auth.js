import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'

var empty = require('is-empty');

export default class Auth extends Component {
	constructor() {
    super()
    this.state = {
			redirectTo: null,
        id: '',
        username:'',
        password:'',
    }
    this.handleAuth = this.handleAuth.bind(this)
    }

	    handleInput = (e) => {
	            this.setState({
	                [e.target.name]: e.target.value
	            });
	        };

	handleAuth(event) {
		event.preventDefault()
		if(empty(this.state.id))
		{
			window.M.toast({html: "Fill the Empty Fields !!", outDuration:'9000', classes:'rounded #ba68c8 pink lighten-2'});
      return false;
		}
		else{
		axios.post('/user/auth', {
			id: this.state.id,
      username: this.state.username,
      password: this.state.password,
		})
			.then(response => {
				if(response.status===200){
          if(response.data.wrong)
          {
            window.M.toast({html: "Wrong Passcode !!", outDuration:'9000', classes:'rounded #ba68c8 red lighten-2'});
            this.setState({
              id:'',
              username:'',
              password:'',
            })
          }
          else if(response.data.succ)
					{
						window.M.toast({html: "Signed Up !!", outDuration:'9000', classes:'rounded #ba68c8 green lighten-2'});
						this.setState({
								redirectTo: '/flogin'
						})
					}
          else if(response.data.existadmin)
          {
            window.M.toast({html: "Already have an account !!", outDuration:'9000', classes:'rounded #ba68c8 green lighten-2'});
						this.setState({
								redirectTo: '/login'
						})
          }
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
			})
			this.setState({
	    id: '',
		})
	}
}

    componentDidMount() {
        M.AutoInit();
    }

render() {
	if (this.state.redirectTo) {
			 return <Redirect to={{ pathname: this.state.redirectTo }} />
	 } else {
	return (
		<div className="row">

		<div className="col l3">
		</div>

		<div className="col l6 s12 m12 form-signup">
				<div className="ew center">
						<h5 className="reg">ADMIN SIGNUP</h5>
				</div>
				<form className="row form-con">
						<div className="row">
                <div className="col l4"></div>
								<div className="input-field col l4 s6 m6">
								<input id="unname" type="text" name="id" className="validate" value={this.state.id} onChange={this.handleInput} required />
								<label htmlFor="unname">Enter the Unique Code</label>
								</div>
                <div className="col l4"></div>
						</div>

            <div className="input-field row">
                <div className="input-field col l6 s12">
                <input id="adname" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleInput} required />
                <label htmlFor="adname">Username</label>
                </div>

                <div className="input-field col l6 s12">
                <input id="pass_admin" type="password" className="validate" name="password" value={this.state.password} onChange={this.handleInput} required />
                <label htmlFor="pass_admin">Password</label>
                </div>
            </div>

              <button className="waves-effect btn col l3 s6 blue-grey darken-2 sup right" onClick={this.handleAuth}>SIGN UP</button>

				</form>
		</div>
    <div className="col l3"></div>

		</div>

	);
}
}
}
