import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import {} from 'materialize-css'
import AddProduct from './add';
import ProductList from './prod'
var empty = require('is-empty');

 export default class InsertDept extends Component{
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{

}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;
     var addroute,editroute;
       addroute="/user/add_from_insert_in_admin";
       editroute = "/user/edit_inserted_data";
     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = editroute;
     } else {
       apiUrl = addroute;
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     var editProd;
       editProd ="/user/fetch_for_edit"


     axios.post(editProd,{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm;

            var  data1 = {
               fielddata: [
                 {
                   header: "Designation Name",
                   name: "designation_name",
                   placeholder: "Enter the Name of the Designation",
                   type: "text",
                   grid: 2,
                   div:"col l5 xl5 s3 m3 center",
                 },
                 {
                   header: "Designation Order",
                   name: "designation_order_no",
                   placeholder: "Enter the No Of the Designation",
                   type: "text",
                   grid: 2,
                   div:"col l4 xl4 s3 m3 center",
                 }
               ],
             };
             if(this.state.isAddProduct || this.state.isEditProduct) {
             productForm = <AddProduct action="Designation" data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
             }
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div>
    {!this.state.isAddProduct && <ProductList action="Designation" data={data1}  editProduct={this.editProduct}/>}
    {!this.state.isAddProduct &&
     <React.Fragment>
     <div className="row">
     <div className="col l6 m6 s6 left" />
     <div className="col l6 m6 s6">
       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
       </div>
    </div>
    </React.Fragment>
  }
    { productForm }
  </div>
</React.Fragment>
);
}
}
}
