import React, { Component} from 'react'
import { } from 'react-router-dom'
import { Select } from 'react-materialize';
import NavAdd from '../CRUD_In_Input/NavHandle'
import AddSection from '../CRUD_In_Input/handleSection'
import Res from '../CRUD_In_Input/handleRes'
import InsertDesignation from '../CRUD_In_Input/insertDesignation'
import InsertDept from '../CRUD_In_Input/handleDept'
import axios from 'axios'
export default class InsertUser extends Component{
    constructor(){
        super();
        this.initialState = {
            username: 'D',
            mailid: '',
            campus:'',
            dept:'',
            h_order:'0.5',
            password:'',
            deptToken:'',
            suspension_status:true,
            resetPasswordToken:'',
            resetPasswordExpires:'',
            count:3,
            active:false,
        }
        this.state = this.initialState;
    }
    handleField = (e) =>{
        this.setState({
          [e.target.name] : e.target.value,
        })
    }
    InsertDeptAdmin =(event)=> {
      event.preventDefault()
      if(!(this.state.username) || !(this.state.dept) || !(this.state.campus) || !(this.state.mailid))
      {
        window.M.toast({html: 'Fill all the Details Please!!',outDuration:'9000', classes:'rounded red lighten-1'});
      }
      else
      {
      window.M.toast({html: 'Submitting...... !!',outDuration:'9000', classes:'rounded green lighten-1'});
      axios.post('/user/insert_dept_admin',{data:this.state})
      .then( res => {
          if(res.data === 'ok')
          {
            window.M.toast({html: 'Successfully Inserted  !!',outDuration:'9000', classes:'rounded green lighten-1'});
          }
          else if(res.data === 'no')
          {
            window.M.toast({html: 'User Already There !!',outDuration:'9000', classes:'rounded pink lighten-1'});
          }
          this.setState(this.initialState)
      });
    }
    }
    render(){
        if(this.props.select==='insertadmin')
        {
            return(
                <div className="row">
                <div className="col l12">
                <div className="input-field col s3">
								<input id="username" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleField} required />
								<label htmlFor="username">Enter Offical Id</label>
								</div>

								<div className="input-field col s3">
								<input id="mailid" type="email" className="validate" name="mailid" value={this.state.mailid} onChange={this.handleField} required />
								<label htmlFor="mailid">Maild Id</label>
								</div>
                <div className="input-field col s3">
                    <Select className="col l1 s6 m6 xl6 over" value={this.state.campus} name="campus" onChange={this.handleField}>
                    <option value="" disabled defaultValue>Campus</option>
                    <option value="Kattankulathur Campus">Kattankulathur Campus</option>
                    <option value="Ramapuram Campus">Ramapuram Campus</option>
                    <option value="Vadapalani Campus">Vadapalani Campus</option>
                    <option value="NCR Campus">NCR Campus</option>
                    </Select>
                </div>
                <div className="input-field col s3">
                      <Select value={this.state.dept} name="dept" onChange={this.handleField}>
                      <option value="" disabled defaultValue>Department</option>
                      <option value="Computer Science">Computer Science</option>
                      <option value="Information Technology">Information Technology</option>
                      <option value="Software Engineering">Software</option>
                      <option value="Mechanical Engineering">Mechanical</option>
                      </Select>
                </div>
								<button className="waves-effect btn #37474f blue-grey darken-3 col l4 left" onClick={this.InsertDeptAdmin}>Insert Department Admin</button>
                </div>
                </div>
                )
        }
        else if(this.props.select==='insert_in_nav')
        {
            return(
                  <NavAdd />
                )
        }
        else if(this.props.select==='responsibilty_percentages')
        {
            return(
                  <div><Res /></div>
                )
        }
        else if(this.props.select==='section_part_insert')
        {
            return(
                  <React.Fragment>
                     <div><AddSection /></div>
                  </React.Fragment>
                )
        }
        else if(this.props.select === 'insert_designation')
        {
          return(
            <div><InsertDesignation /></div>
          )
        }
        else if(this.props.select === 'insert_department')
        {
          return(
            <div><InsertDept/></div>
          )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}
