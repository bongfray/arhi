const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const uri = 'mongodb://localhost:27017/eWork_stud'
var autoIncrement = require('mongoose-auto-increment');

mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    }).then(
    () => 
    {
        console.log('Connected Student DB');
    },
    err => 
    {
      console.log('error connecting to Mongo: '+err)
    }
  );
  var connection = mongoose.createConnection("mongodb://localhost:27017/eWork_stud",{ useNewUrlParser: true,useUnifiedTopology: true});
  autoIncrement.initialize(connection);
module.exports = mongoose.connection
