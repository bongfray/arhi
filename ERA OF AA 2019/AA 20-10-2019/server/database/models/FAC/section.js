const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

const different_res = new Schema({
  action: {type: String , unique: false, required: false},
  usertype: {type: String , unique: false, required: false},
  username: {type: String , unique: false, required: false},
  section_data_name:{ type: String, unique: false, required: false },
  section_props:{ type: String, unique: false, required: false },
  role:{ type: String, unique: false, required: false },
  description:{ type: String, unique: false, required: false },
  date_of_starting:{ type: String, unique: false, required: false },
  date_of_complete:{ type: String, unique: false, required: false },
  achivements:{ type: String, unique: false, required: false },
})



different_res.plugin(autoIncrement.plugin, { model: 'Section', field: 'serial', startAt: 1,incrementBy: 1 });

const Section = mongoose.model('Section', different_res)
module.exports = Section
