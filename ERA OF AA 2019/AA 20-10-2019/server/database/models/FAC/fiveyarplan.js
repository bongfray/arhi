const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');



const five = new Schema({
expired: { type: Boolean, unique: false, required: false },
completed: { type: Boolean, unique: false, required: false },
expire_year: { type: Number, unique: false, required: false },
username: { type: String, unique: false, required: false },
action: { type: String, unique: false, required: false },
YearIAdministrative:{ type: String, unique: false, required: false },
YearIAcademic:{ type: String, unique: false, required: false },
YearIResearch:{ type: String, unique: false, required: false },
YearIIAdministrative:{ type: String, unique: false, required: false },
YearIIAcademic:{ type: String, unique: false, required: false },
YearIIResearch:{ type: String, unique: false, required: false },
YearIIIAdministrative:{ type: String, unique: false, required: false },
YearIIIAcademic:{ type: String, unique: false, required: false },
YearIIIResearch:{ type: String, unique: false, required: false },
YearIVAdministrative:{ type: String, unique: false, required: false },
YearIVAcademic:{ type: String, unique: false, required: false },
YearIVResearch:{ type: String, unique: false, required: false },
YearVAdministrative:{ type: String, unique: false, required: false },
YearVAcademic:{ type: String, unique: false, required: false },
YearVResearch:{ type: String, unique: false, required: false },
})

five.plugin(autoIncrement.plugin, { model: 'Schema-Five_Year_Plan', field: 'serial', startAt: 1,incrementBy: 1 });



var fivep = mongoose.model('five_year_plan', five);




module.exports = fivep
