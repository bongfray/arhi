import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetchall',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { products} = this.state;
      return(
        <div>
            <div className="row">
            <div className="col s1 l1 xl1 m1"><b>Serial No</b></div>
            {this.props.data.fielddata.map((content,index)=>(
              <div className={content.grid} key={index}><b>{content.header}</b></div>
            ))}
            <div className="col s1 l1 xl1 m1 center"><b>Action</b></div>
            </div>

            <hr />

            <div className="row">
              {products.map((product,index) => (
                <div key={product.serial}>
                  <div className="center col l1 xl1">{index+1}</div>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className={content.grid} key={index}>{product[content.name]}</div>
                  ))}
                  <div className="center col l1 xl1">
                  <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                  &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                  </div>
                </div>
              ))}
            </div>
<hr />

        </div>
      )

  }
}
