import React, { Component } from 'react';
import HandleSkill from './ARHI-Operation/handle-skill'
import HandleDomain from './ARHI-Operation/handle-domain'

export default class Arhi extends Component {
  constructor(){
      super();
      this.state ={
          radio:[{name:'radio1',color:'blue',value:'Resume-Skill'},{name:'radio2',color:'blue',value:'Domain'}],
          isChecked: false,
          choosed: '',
          active:'',
      }
  }

  handleChecked =(e,index,color)=>{
      this.setState({
          isChecked: !this.state.isChecked,
          choosed: e.target.value
      });
      if (this.state.active === index) {
        this.setState({active : null})
      } else {
        this.setState({active : index})
      }
  }
  color =(position) =>{
    if (this.state.active === position) {
        return "col l10 s10 m10 form-signup #18ffff cyan accent-2";
      }
      return "col l10 s10 m10 form-signup black-text";
  }

  render()
  {

      return(
          <React.Fragment>
         <div className="row">
              {this.state.radio.map((content,index)=>(
                  <div className="col s6 l2 m2" key={index}>
                      <div className={this.color(index)}>
                      <p>
                      <label>
                      <input type='radio' className="yellow" id={content.name} name='myRadio' value={content.value} onChange={(e)=>{this.handleChecked(e,index,content.color)}} />
                      <span style={{letterSpacing: '.1em'}} className="black-text"><b>{content.value}</b></span>
                      </label>
                      </p>
                      </div>
                </div>
              ))}
         </div>

              <div className="row">
                  <div className="col s12 m12 l12">
                     <Display choosed={this.state.choosed}/>
                  </div>
              </div>
          </React.Fragment>
      );
  }
}


 class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: '',
            select: '',
            active:'',
        }
    }
    handleCheck = (e) => {
        this.setState({
            isChecked: e,
            select: e
        })

        if (this.state.active === e) {
          this.setState({active : null})
        } else {
          this.setState({active : e})
        }

    }
    color =(position) =>{
      if (this.state.active === position) {
          return "#00695c teal darken-3 white-text display-style";
        }
        return "display-style hover-effect";
    }

    render(){
      let choosed_option;
        if(this.props.choosed === 'Resume-Skill')
        {
            choosed_option=
              <div className="row ">
              <div></div>
              <div className="col s12 l12 xl12 m12">
                  <div className="col s12 xl2 m2 l2">
                        <div className={this.color('add-skill')} onClick={(e)=>{this.handleCheck('add-skill')}}>Add Skill</div>
                        <div className={this.color('skill-level')} onClick={(e)=>{this.handleCheck('skill-level')}}>Statement on Skills</div>
                        <div className={this.color('set-task')} onClick={(e)=>{this.handleCheck('set-task')}}>Test Skill Tasks</div>
                        </div>
                        <div className="col s12 l10 xl10 m10">
                                <HandleSkill select={this.state.select} />
                        </div>


                    </div>
                    </div>
        }
        else if(this.props.choosed === 'Domain')
        {
            choosed_option=
              <div className="row ">
              <div></div>
              <div className="col s12 l12 xl12 m12">
                  <div className="col s12 xl2 m2 l2">
                        <div className={this.color('add-domain')} onClick={(e)=>{this.handleCheck('add-domain')}}>Add Domain</div>
                        <div className={this.color('achieve-domain')} onClick={(e)=>{this.handleCheck('achieve-domain')}}>Add Path to Achieve Domain</div>
                        </div>
                        <div className="col s12 l10 xl10 m10">
                                <HandleDomain select={this.state.select} />
                        </div>


                    </div>
                    </div>
        }
        else{
            choosed_option=
                <div></div>
        }
        return(
          <React.Fragment>
             <div className="row" style={{marginTop:'15px'}}>
               {choosed_option}
             </div>
          </React.Fragment>
        )
    }
}
