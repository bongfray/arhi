import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import AddProduct from './add_datas';
import ProductList from './show_info'
var empty = require('is-empty');

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{
  this.setState({
    isAddProduct: false,
    isEditProduct: false,
  })
}

   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/user2/editt';
     } else {
       apiUrl = '/user2/addmm';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/user2/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
   render() {
     let productForm,title,description;
     var data;
             title ='Skills';
             description ='Please provide information about graduate thesis/project supervised';
             data = {
               Action:'Skills',
               button_grid:'col l2 xl2 s2 m2 center',
               fielddata: [
                 {
                   header: "Your Skills",
                   name: "skill_name",
                   placeholder: "Enter Your Skill",
                   type: "text",
                   grid:'col l3 xl3 s1 m1 center'
                 },
                 {
                   header: "Level of Your Skill",
                   name: "skill_level",
                   placeholder: "Enter Your Lavel in this Skill",
                   type: "text",
                   grid: 'col l3 xl3 s1 m1 center',
                   exception:true,
                 },
                 {
                   header: "Verification",
                   name: "skill_verification",
                   placeholder: "",
                   type: "text",
                   grid:'col l3 xl3 s1 m1 center',
                 },

               ],
             };


if(this.state.isAddProduct || this.state.isEditProduct) {
productForm = <AddProduct username={this.props.username} action={this.props.options} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}

return (
  <div className="card hoverable">
  <div>
    <div>
  <div>
    {!this.state.isAddProduct && <ProductList username={this.props.username} title={title} description={description} action={this.props.options} data={data}  editProduct={this.editProduct}/>}
    <br />
    {!this.state.isAddProduct &&
      <React.Fragment>
      <div className="row">
      <div className="right">
        <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
      </div>
      </div>
     </React.Fragment>
   }
     { productForm }
     <br/>
   </div>
   </div>
 </div>
   </div>
);
}
}
