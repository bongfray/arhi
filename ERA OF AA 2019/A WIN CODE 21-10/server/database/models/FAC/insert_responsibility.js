const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const different_res = new Schema({
  action: {type: String , unique: false, required: false},
  section_data_name:{ type: String, unique: false, required: false },
  section_props:{ type: String, unique: false, required: false },
})



different_res.plugin(autoIncrement.plugin, { model: 'Nav', field: 'serial', startAt: 1,incrementBy: 1 });

const Different_res = mongoose.model('Handle_Dynamic_CRUD', different_res)
module.exports = Different_res
