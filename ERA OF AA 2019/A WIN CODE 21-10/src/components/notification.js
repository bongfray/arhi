import React, { Component } from 'react'
import Nav from './dynnav'
import ContentLoader from "react-content-loader"

export default class Notification extends Component {
    constructor()
    {
        super()
        this.state={
            display:'',
            loader:false,
            home:'/faculty',
            logout:'/user/logout',
            get:'/user/',
            nav_route: '/user/fetchnav',
            order_action:'faculty',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {

    }
    componentDidUpdate = (prevProps) =>{
        if(prevProps.display !== this.props.display)
        {

        }
    }
    render() {
        let noti;
            noti=
            <div className="notification">
              <h4 className="white-text">Notification Page</h4> 
            </div>

        return (
            <React.Fragment>
            <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
            <div className="row">
                <div className="col l1" />
                <div className="col l10 s11 m11 xl10">
                        <ul className="collapsible popout">
                                <li className="collap-body-noti">
                                <div className="collapsible-header white black-text collap-head-noti">
                                      Notification 1
                                </div>
                                <div className="collapsible-body collap-body-noti"><span>Lorem ipsum dolor sit amet.</span></div>
                                </li>
                        </ul>
                </div>
                <div className="col l1 xl1 s1 m1">
                <a className="btn-floating btn-medium waves-effect white cancel-noti tooltipped" style={{marginTop:'22px'}} data-position="bottom" data-tooltip="Dismiss"><i className=" red material-icons">clear</i></a>
                </div>
            </div>
            <div className="row">
                <div className="col l1" />
                <div className="col l10 s11 m11 xl10">
                        <ul className="collapsible popout">
                                <li className="collap-body-noti">
                                <div className="collapsible-header white black-text collap-head-noti">
                                      Notification 1
                                </div>
                                <div className="collapsible-body collap-body-noti"><span>Lorem ipsum dolor sit amet.</span></div>
                                </li>
                        </ul>
                </div>
                <div className="col l1 xl1 s1 m1">
                <a className="btn-floating btn-medium waves-effect white cancel-noti tooltipped" style={{marginTop:'22px'}} data-position="bottom" data-tooltip="Dismiss"><i className=" red material-icons">clear</i></a>
                </div>
            </div>
            <div className="row">
                <div className="col l1" />
                <div className="col l10 s11 m11 xl10">
                        <ul className="collapsible popout">
                                <li className="collap-body-noti">
                                <div className="collapsible-header white black-text collap-head-noti">
                                      Notification 1
                                </div>
                                <div className="collapsible-body collap-body-noti"><span>Lorem ipsum dolor sit amet.</span></div>
                                </li>
                        </ul>
                </div>
                <div className="col l1 xl1 s1 m1">
                <a className="btn-floating btn-medium waves-effect white cancel-noti tooltipped" style={{marginTop:'22px'}} data-position="bottom" data-tooltip="Dismiss"><i className=" red material-icons">clear</i></a>
                </div>
            </div>
            <div className="row">
                <div className="col l1" />
                <div className="col l10 s11 m11 xl10">
                        <ul className="collapsible popout">
                                <li className="collap-body-noti">
                                <div className="collapsible-header white black-text collap-head-noti">
                                      Notification 1
                                </div>
                                <div className="collapsible-body collap-body-noti"><span>Lorem ipsum dolor sit amet.</span></div>
                                </li>
                        </ul>
                </div>
                <div className="col l1 xl1 s1 m1">
                <a className="btn-floating btn-medium waves-effect white cancel-noti tooltipped" style={{marginTop:'22px'}} data-position="bottom" data-tooltip="Dismiss"><i className=" red material-icons">clear</i></a>
                </div>
            </div>
            <div className="row">
            <a className="waves-effect tooltipped" data-position="bottom" data-tooltip="Clear All"><i className="red-text large material-icons">delete_forever</i></a>

                <div className="div"></div>
            </div>

            </React.Fragment>
           
        )
    }
}
