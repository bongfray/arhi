import React, { Component } from 'react';
import {} from 'materialize-css'
import axios from 'axios'
import {  Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import Daa from './DAA'
import ContentLoader from "react-content-loader"
import {
  CircularProgressbar,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./AnimatedProgressProvider";

const academics = 66;
const research = 76;
const administrative = 55;
export default class Dash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  fetchlogin = () =>{
    axios.get('/user/fetchdayorder',
  this.setState({loading: true})
)
    .then(response =>{
      this.setState({loading: false})
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'1500', classes:'rounded #ec407a pink lighten-1'});
      }
    })
  }
  componentDidMount()
  {
    this.fetchlogin()
  }

render(){
  const MyLoader = () => (
    <ContentLoader 
      height={160}
      width={400}
      speed={2}
      primaryColor="#f3f3f3"
      secondaryColor="#c0c0c0"
    >
      <rect x="20" y="10" rx="5" ry="5" width="110" height="50" />  
      <rect x="140" y="10" rx="3" ry="3" width="110" height="50" />  
      <rect x="270" y="10" rx="3" ry="3" width="110" height="50" />  

      <rect x="20" y="90" rx="1" ry="1" width="110" height="15" />  
      <rect x="140" y="90" rx="1" ry="1" width="110" height="15" />  
      <rect x="270" y="90" rx="1" ry="1" width="110" height="15" />  


    </ContentLoader>
  )
  if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
  return (
    <React.Fragment>
    <Nav home={ this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
    <div className="row">
    <div className="col l4">
    <Example label="Academics">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={academics}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    <div className="col l4">
    <Example label="Administrative">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={administrative}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>

    <div className="col l4">
    <Example label="Research">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={research}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    </div>
    <div>
     <Daa />
    </div>
    </React.Fragment>
    );
      }
    }
    }

  }
  function Example(props) {
  return (
  <div className="card hoverable" style={{paddingTop: '10px', paddingBottom:'10px'}}>
      <div style={{marginLeft:'26%',width:'200px'}}>{props.children}</div>
  </div>
  );
  }
