import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import Collap from './Gg'
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      fetched_data:[],
      loading:true,
      username:'',
      redirectTo:'',
      option:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
      order_action:'faculty',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}
getUser()
{
  axios.get('/user/',
  this.setState({loading: true})
)
   .then(response =>{
     this.setState({loading: false})
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/faculty',
       });
       window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
     }
   })
}
getSectionData()
{
  axios.post('/user/fetchnav',{action:'Section'}).then(response =>{
    console.log(response.data)
    this.setState({
     fetched_data: response.data,
   })
  })
}

componentDidMount(){
  M.AutoInit()
  this.getUser()
  this.getSectionData()
}


  render()
  {
    const MyLoader = () => (
      <ContentLoader 
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#c0c0c0"
      >
       <rect x="130" y="20" rx="3" ry="3" width="120" height="20" /> 
        <rect x="35" y="60" rx="3" ry="3" width="330" height="30" />    
      </ContentLoader>
    )
    if(this.state.loading ===  true)
    {
      return(
        <MyLoader />
      );
    }
    else{
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <h5 className="center prof-heading black-text">This  Part  is  For  Enlisting  Your  Achivements</h5>
      <div className="row">
      <div className="col l1" />
      <div className="col l10">
      <div className="row form-signup">
      <div className="col l6">
        <p className="proftitle">Kindly Select On Which Area You Are Going to Upload You Datas</p>
      </div>
        <div className="col l6">
            <select name="title" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled defaultValue>Select Here...</option>
            <option value="thesis_projects_supervised">Thesis & Projects Supervised</option>
            <option value="training_workshop_attended">Training or Workshop or Seminer or Conference Attended</option>
            <option value="training_workshop_held">Training or Workshop or Seminer or Conference Held</option>
            <option value="paper_published_accepted">Paper Published & Accepted</option>
            <option value="research_projects_done_by_you">Research or Projects Done by You</option>
            <option value="contribution_to_university">Contribution to University</option>
            <option value="department_activities">Department Activities</option>
            <option value="department_committe_membership">Department Committes Membership</option>
            <option value="advising_counselling_details">Advising and Counselling Details</option>
            </select>
        </div>
        </div>
        </div>
        <div className="col l1" />
        </div>
          <Collap options={this.state.option} username={this.state.username}/>
      </React.Fragment>

    )
  }
}
  }
}
