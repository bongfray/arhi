import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
// import '../styles.css'

import InsertUser from './insertuser'
import UpdateUser from './updateuser'
import SuperUser from './superuser'
import DeleteUser from './deleteuser'
import ModifyUser from './modifyuser'
import SuspendUser from './suspenduser'


export default class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: false,
            select: ''
        }
    }
    handleCheck = (e) => {
        this.setState({
            isChecked: true,
            select: e.target.value
        })

    }
    render(){
      let choosed_option;
        if(this.props.choosed === 'Insert')
        {
            choosed_option=
              <React.Fragment>
                    <div className="card col l2 s12" style={{marginRight: '1px solid'}}>
                        <p>
                        <label>
                        <input type='radio' id='radio-d1' name='myRadio' value='insertadmin' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Department Admin</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d2' name='myRadio' value='insert_in_nav' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Into NavBar</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d3' name='myRadio' value='responsibilty_percentages' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Responsibilty Percentages</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-nd3' name='myRadio' value='section_part_insert' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Options For Section's Dropdown</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="card col l10 s12">
                            <InsertUser select={this.state.select} />
                    </div>
                    </React.Fragment>
        }
        else if(this.props.choosed === 'Update')
        {
            choosed_option=
                <div className="row card">
                <div className="col s12">
                    <div className="col s12 l3 brd-r">
                        <p>
                        <label>
                        <input type='radio' id='radio-d4' name='myRadio' value='adminpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset DepartmentAdmin Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d5' name='myRadio' value='facpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Faculty Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d6' name='myRadio' value='studpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Student Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d7' name='myRadio' value='all_user_dayorder_entry' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Add DayOrder</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col s9">
                            <UpdateUser select={this.state.select} />
                    </div>


                </div>
                </div>
        }
        else if(this.props.choosed === 'Super')
        {
            choosed_option=

                <div className="row">
                <div className="col l12">
                    <div className="col l2 card">
                        <p>
                        <label>
                        <input type='radio' id='radio-d8' name='myRadio' value='register' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Handle Registration</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d9' name='myRadio' value='facprofile' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Faculty Profile</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d10' name='myRadio' value='studprofile' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Student Profile</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d11' name='myRadio' value='facreport' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Faculty Report</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d12' name='myRadio' value='studreport' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Student Report</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-d13' name='myRadio' value='approve_single_faculty_req' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Requests From Users</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col l10">
                            <SuperUser select={this.state.select} />
                    </div>
                </div>
                </div>
        }
        else if(this.props.choosed === 'Delete')
        {
            choosed_option=
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-d14' name='myRadio' value='deleteadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d15' name='myRadio' value='deletefac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d16' name='myRadio' value='deletestu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Student</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <DeleteUser select={this.state.select} />
                </div>
                </div>
                </div>
        }
        else if(this.props.choosed === 'Modify')
            {
            choosed_option=
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-d17' name='myRadio' value='start-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Start Semester Date</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d18' name='myRadio' value='end-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>End Semester Date</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d19' name='myRadio' value='modifytoday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Modify Today's Day Order</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d20' name='myRadio' value='cancel_or_declare_today_as_a_holiday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Cancel Today's DayOrder</b><br />(Update here only if today is, also a holiday)</span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d21' name='myRadio' value='holiday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Declartion for UpComing Holiday</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <ModifyUser select={this.state.select} />
                </div>
                </div>
                </div>
        }
        else if(this.props.choosed === 'Suspend')
        {
            choosed_option=
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-d22' name='myRadio' value='suspendadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d23' name='myRadio' value='suspendfac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d24' name='myRadio' value='suspendstu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Student</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-d25' name='myRadio' value='suspend_request' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspended Request</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <SuspendUser select={this.state.select} />
                </div>
                </div>
                </div>
        }

        else{
            choosed_option=
                <div></div>
        }
        return(
          <React.Fragment>
             <h5 className="center">{this.props.choosed} Operations</h5>
             {choosed_option}
          </React.Fragment>
        )
    }
}
