
import React from 'react';



class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      Action:'',
      name: '',
      emailid: '',
      mobileno:'',
      address: '',
      gender:'',

      skill_name:'',
      skill_level:'',

      date_of_training_held:'',
      details_workshop_seminar_held:'',
      location_of_workshop_seminar_held:'',
      duration_of_workshop_seminar_held:'',
      role_workshop_seminar_held:'',

      date_of_paper_publication:'',
      paper_title:'',
      volume_no_of_publiction:'',
      impact_of_publication:'',
      paper_accepted_by:'',

      research_project_area:'',
      project_research_title:'',
      description_project_research:'',
      duration_of_project_research_tobe_done:'',
      achivements_from_project_research_work:'',

      contribution_field:'',
      details_of_contribution:'',
      result_of_contribution:'',
      achivements_for_your_contribution:'',

      field_of_activity:'',
      details_of_dept_activity:'',
      result_from_activity:'',
      achivements_from_the_activity:'',

      date_of_meeting:'',
      commity_details:'',
      position_role:'',
      number_of_meeting_attended:'',

      date_of_counselling:'',
      details_of_student:'',
      no_of_advising_hour:'',
      description_of_activity:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.Action,
    username: this.props.username
  })
}


  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    // console.log(this.initialState.username)
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS TO YOUR PERSONAL DATA</h5>
}

    return(
      <div>
      <div className="center">{pageTitle}</div>
      <div>
      <div>
          <form onSubmit={this.handleSubmit}>
          {this.props.data.fielddata.map((content,index)=>(
          <div className="row">
          <div className="col l1"/>
          <div className="col l2 m2 s2">{content.header}</div>
            <div className="col l8 s8 m8 input-field" key={index}>
              <input
                className=""
                type={content.type}
                placeholder=" "
                min="10"
                max="60"
                name={content.name}
                value={this.state[content.name]}
                onChange={e => this.handleD(e, index)}
              required/>
              <label>{content.placeholder}</label>
            </div>
            <div className="col l1" />
          </div>
          ))}
            <div className="row">
              <button className="btn right blue-grey darken-2 sup" style={{marginRight:'40px'}} type="submit">UPLOAD</button>
            </div>
          </form>
      </div>
      </div>

      </div>
    )
  }
}

export default AddProduct;
