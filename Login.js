import React, { Component } from 'react';
import M from "../node_modules/materialize-css";
import { Link } from 'react-router-dom';
import Dashboard from './Dashboard';
var empty = require('is-empty');


class Login extends Component{
    state = {
        email: '',
        pswd: '',
    }
    handleEmail = (e) => {
        this.setState({
            email: e.target.value
        });
    };
    handlePassword = (e) => {
        this.setState({
            pswd: e.target.value
        });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        if(empty(this.state.email)||empty(this.state.pswd))
        {
            window.M.toast({html: 'Enter all the Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
            this.setState({
                cnf_pswd:''
            })
            return false;
        }
        else
        {
            return true;
        }
    };
    

    
    notify = () => window.M.toast({html: 'Welcome to eWork!', outDuration:'850', inDuration:'800', displayLength:'1500'});
    componentDidMount(){
      this.notify();
    }
    
    render() {
        return(
            <div className="row">

            <div className="col s4">
            </div>

            <div className="col s4 form-login">
                <div className="ew center">
                    <h5 className="blue-grey darken-2 highlight">eWORK</h5>
                    <h5>LOG IN</h5>
                </div>
                <form className="form-con">
                    <div className="row">
                    <div className="input-field col s12">
                        <input id="email" type="email" className="validate" value={this.state.email} onChange={this.handleEmail} required />
                        <label htmlFor="email">Email</label>
                    </div>
                    <div className="input-field inline col s12">
                        <input id="password" type="password" className="validate" value={this.state.pswd} onChange={this.handlePassword}required/>
                        <label htmlFor="password">Password</label>
                    </div>
                    </div>
                    <br/>
                    <div className="row">
                    <Link to="/Signup" className="waves-effect waves-light btn blue-grey darken-2 col s5">Register</Link>
                    <div className="col s2"></div>
                    <Link to="/Dashboard" className="waves-effect waves-light btn blue-grey darken-2 col s5" onClick={this.handleSubmit}>Login</Link>
                    </div>
                    <br/>
                    <hr/>
                    <div className="row">
                    <a className="black-text col s6 offset-s4 but" href="/">Forgot Password?</a>
                    </div>
                    
                </form>
            </div>

            <div className="col s4">
            </div>

            </div>
    
        )
    }
}

export default Login;