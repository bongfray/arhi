const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

 
const student = new Schema({

username: { type: String, unique: false, required: false },
password: { type: String, unique: false, required: false },
name: { type: String, unique: false, required: false },
mailid: { type: String, unique: false, required: false },
phone: { type: String, unique: false, required: false },
campus: { type: String, unique: false, required: false },
dept: { type: String, unique: false, required: false },
dob: { type: String, unique: false, required: false },
degree: { type: String, unique: false, required: false },
st_year: { type: String, unique: false, required: false },
fn_year: { type: String, unique: false, required: false },
resetPasswordToken: { type: String, unique: false, required: false },
resetPasswordExpires: { type: String, unique: false, required: false },
count: { type: String, unique: false, required: false },
})


student.methods = {
	checkPassword: function (inputPassword) {
		return bcrypt.compareSync(inputPassword, this.password)
	},
	hashPassword: plainTextPassword => {
		return bcrypt.hashSync(plainTextPassword, 10)
	}
}



student.pre('save', function (next) {
	if (!this.password) {
		console.log('models/user.js =======NO PASSWORD PROVIDED=======')
		next()
	} else {
		console.log('models/user.js hashPassword in pre save');

		this.password = this.hashPassword(this.password)
		next()
	}
})



const Suser = mongoose.model('student_login', student)
module.exports = Suser
