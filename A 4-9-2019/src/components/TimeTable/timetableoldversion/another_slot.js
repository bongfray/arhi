import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link,Redirect } from 'react-router-dom'
import {Button, Modal} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Allot'
import Free from './Free'
import Cancel from './Cancel'
import Modal2 from './Modal'

{/*---------------------------------------------------------Code for regular classes time table------------------------------------ */}



export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',
      modal: false,

    };

  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal}) // true/false toggle
}
  componentDidMount(){
    const options ={
      inDuration: 250,
      outDuration: 250,
      opacity: 0.5,
      dismissable: false,
      startingTop: "94%",
      endingTop: "%"
    };
    M.Modal.init(this.Modal,options);



    M.AutoInit();

  }


  handle1 = (e, index) => {
    // alert(e.target.value);
    // alert(e.target.name);
    const key = e.target.name;
    const value = e.target.value;
    this.setState({ rit: value, index ,day_order: key, index});

  };


  render()
  {
    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <div>

        <a key={this.props.slot_time} className="go col s6 offset-s3 but N/A transparent modal-trigger" data-target="modal1">
        GO
        </a>
        <div ref={Modal => {
          this.Modal =Modal;
        }}
         id="modal1" className="modal">
    <div className="modal-content">
    <div className="root-of-time" >
    <div className="row">
         <div className="col l12 card hoverable ">
         <div className="card-title">
         <span className="">
         <span>{this.props.key_id}</span>
         <div className="center day-order-day">{this.state.day_order}</div>
         </span>
         </div><br /><br />
           <div>
             <select name={this.state.day_order} onChange={e => this.handle1(e, this.state.day_order)}>
                 <option defaultValue="" disabled selected>Choose</option>
                 <option value="allot">Alloted Class</option>
                 <option value="free">Free Slot</option>
                 <option value="slot_cancel">Slot Cancelled</option>
             </select>
           </div>
           <span className="drop">

             <TableDisplay
             selectValue={this.state.rit} day_order={this.state.day_order} usern={this.props.usern}
             />
           </span>
         </div>


         </div>
>




</div>
   </div>
   <div className="modal-footer">
    <a href="#!" className="close modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
   </div>
   </div>
      )
  }
  }
}



{/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */}


class TableDisplay extends Component {
  constructor() {
    super()
    this.state = {
      allot:'',
        count: 0,
        selected:'',
        problem_conduct:'',


        saved_dayorder: '',
        day_recent:'',

    }

    }




componentDidMount(){

}




  render() {


                if (this.props.selectValue === "allot") {
                  return(
                    <div className="">
                    <Allot usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                    </div>
                  );

                } else if (this.props.selectValue === "free") {
                  return(
                    <Free usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                  );

                }
                else if (this.props.selectValue === "slot_cancel") {
                 return(
                   <Cancel usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                 );

               }
                else{
                  return(
                    <div className="inter-drop center">
                      Choose from above DropDown
                    </div>
                  );
                }
  }
}
