import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import Slogo from './Slogo.png'
import './css_home.css'
import M from 'materialize-css'
import Fhome from './fhome2.png'

class Home extends Component{

    render(){
        return(
          <div className="wrapper bdy">
        <div className="floor"></div>
        <div className="left">
        <div id="container sideslid">
	<div className="paper">
		<div className="tape"></div>
		<div className="red-line first"></div>
		<div className="red-line"></div>
		<ul id="lines">
			<li></li>
			<li>About</li>
			<li></li>
			<li></li>
		</ul>
		<div className="left-shadow"></div>
		<div className="right-shadow"></div>
	</div>
</div>
        </div>
        <div className="shelf-wrapper">
            <div className="shelf1">
                <div id="slantbook"></div>
                <div id="flatbook"></div>

            </div>
            <div className="shelf2">
                <div id="flatbook3"></div>
                <div className="penstand">
                    <div id="pen"></div>
                </div>

            </div>
            <div className="shelfknob">
                <div className="knob" id="knob1"></div>
                <div className="knob" id="knob2"></div>

            </div>

        </div>
        <div className="window">
            <span id="moon"></span>
        </div><br />
        {/*<div className="right directions">
          <Link to="/login" className="col l5 m12 s12 waves-effect btn #03a9f4 light-blue"><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></Link>
        </div>*/}
        <Link to="/login">
     <div className="right directions">
        <div id="Awesome" className="anim750">
	<div className="sticky anim750">
		<div className="front circle_wrapper anim750">
			<div className="circle anim750"></div>
	  </div>
	</div>

  <h6>Login & SignUP</h6>

  <div className="sticky anim750">
		<div className="back circle_wrapper anim750">
			<div className="circle anim750"></div>
		</div>
	</div>

</div>
</div>
</Link>



        <div className="container">
            <div className="cup"></div>
            <div className="table">
                <span className="handle" id="handle1"></span>
                <span className="handle" id="handle2"></span>
                <div id="margintop">
                    <div className="tableleg" id="table0"></div>
                    <div className="tableleg" id="table1"></div>
                    <div className="tableleg" id="table2"></div>
                    <div className="tableleg" id="table3"></div>
                    <div className="tableleg" id="table4"></div>
                    <div className="tableleg" id="table5"></div>
                    <div className="tableleg" id="table6"></div>
                    <div className="tableleg" id="table7"></div>

                </div>
            </div>
            <div className="lamp">
                <div id="lampjoint"></div>
                <div className="lamphead"></div>
            </div>
            <div className="monitor">
                <span id="monitor-base"></span>
            </div>

            <div className="semicircle"></div>
        </div>




    </div>

        );
    }
}

export default Home;



/*


import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Slogo from './Slogo.png'
import './style.css'
import M from 'materialize-css'
import Fhome from './fhome2.png'

class FrontPage extends Component{
  constructor(props)
  {
    super(props);
    this.state ={
      display:'',
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }


  getUser = () => {
    axios.get('/user/').then(response => {
      if (response.data.user) {
          this.setState({display:'disabled'})
      }
        })
      }


    componentDidMount() {
        this.getUser();
        M.AutoInit();
        let elems = document.querySelectorAll(".modal-trigger");
        M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });

    }
    render(){
console.log(this.props.loggedIn)
        return(
            <div className="row">
                <div className="col s12 l6 m12">
                    <div className="fstyle">
                    <center><div className="title-of-home"><h4  className="ework_name">E-Work</h4></div></center><br /><br />
                    <p className="fpara">Ework is a simplified analytics tool.It is a tool to keep trace and record of each and everyday routine of staff members of the institute.E-Work is a tool designed by CARE which majorly focuses on the encouragement of the well deserved ones. </p><br/><br/>
                    <div className="row">
                    <div className="col l1 m1 hide-on-down" />
                      <div className="col l4 s12 m12">
                        <Link to="/login" style={{width:'100%',}} className={"left waves-effect btn #03a9f4 light-blue "+this.state.display}><i className="material-icons right">exit_to_app</i><b>LOGIN or SignUp</b></Link>
                      </div>
                      <div className="col l2 m2 hide-on-down"></div>
                      <div className="col l4 s12 m12">
                       <Link to="#" style={{width:'100%'}} className="waves-effect btn #c0ca33 lime darken-1 right"><i className="material-icons right">desktop_mac</i><b>About</b></Link>
                      </div>
                      <div className="col l1 m1 hide-on-down" />
                    </div>
                    <a className=" col s3 offset-l9 offset-s5 offset-m5 fprivacy blink modal-trigger" href="#modal1">Privacy Policy</a>
                    <div id="modal1" className="modal modal-fixed-footer modal-fixed">
                <div className="modal-content">
                  <h4 className="mheader center">Privacy Policy</h4>
                  <p className="mcont center">This section will be updated soon !!</p>
                </div>
                <div className="modal-footer">
                  <a href="#!" className="modal-close btn-flat"><i className="material-icons right">close</i>Close</a>
                </div>
                </div>


                    </div>
                </div>
                <div className="col l6 hide-on-med-and-down">
                    <img className="img-home imgf" src={Fhome} alt=""/>
                </div>
            </div>
        );
    }
}

export default FrontPage;



*/
