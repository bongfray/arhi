import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
import Slogo from '../Slogo.png'
import './style.css'
import M from 'materialize-css'

export default class Navbar extends Component {
  constructor()
  {
    super()
    this.state ={
      loggedIn:false,
      username:'',
    }
    this.logout = this.logout.bind(this)
  }
  getUser = () =>{
    axios.get(this.props.get).then(response => {
      if (response.data.user) {
        console.log("Ok")
        console.log(response.data.user);
        this.setState({
          loggedIn: true,
          username: response.data.user.username,

        })
      } else {
        this.setState({
          loggedIn: false,
          username: null,

        })
      }
    })
  }

  componentDidMount = () => {
    M.AutoInit();
    this.getUser();
    let elems=document.querySelectorAll('.dropdown-button');
    M.Dropdown.init(elems,{inDuration : 300 ,outDuration :225, leftOrigin: true});
    M.Sidenav.init(elems, {
    inDuration: 350,
    outDuration: 350,
    edge: 'left'
    });
}


    logout(event) {
        event.preventDefault()
        axios.post(this.props.logout).then(response => {
          if (response.status === 200) {
            this.setState({
              loggedIn: false,
              username: null
            })
            window.location.assign(this.props.home);
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }
    render() {
 if(this.state.loggedIn === false)
{
  return(
    <div class="navbar-fixed">
  <nav>
    <div className="nav-wrapper blue-grey darken-2">
      <a href="#!" className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a>
      <div className="row">
      <div className="col s2 m2 xl2 l2" />
      <div className="col s8 m8 xl8 l8" style={{paddingLeft:'90px'}} >
      <a href="http://care.srmuniv.ac.in" className="con nav-cen hide-on-med-and-down">SRM Centre for Applied Research in Education</a></div>
      <div className="col s2 xl2 m2 l2">
      <ul className="right hide-on-large-only">
        <li><Link to='/flogin' className="right">LOGIN</Link></li>
      </ul>
      </div>
      </div>
    </div>
  </nav>
</div>
  );
}
else if(this.state.loggedIn === true)
{
        return (
                    <div className="navbar-fixed">

                      <ul id="slide-out" className="sidenav">
                        <li><Link className="waves-effect" to={this.props.home}>Home</Link></li>
                        {this.props.content.map((content,index)=>{
                          return(
                            <li key={content.val}><Link to={content.link} className="center">{content.val}</Link></li>
                          )
                        })}
                        <li><Link to="#" className="" onClick={this.logout}>Logout</Link></li>
                     </ul>
                     <nav>
                          <div className="nav-wrapper blue-grey darken-2">
                              <a href="#" data-target="slide-out" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                          <div className="brand-logo col s2"><a href="http://srmuniv.ac.in"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a></div>
                          <span className="hide-on-med-and-down center nav-cen srm-care"><a href="http://care.srmuniv.ac.in">SRM CARE</a></span>
                          <ul className="right">
                          <li title="Home">
                            <Link to={this.props.home}><i className=" hide-on-med-and-down material-icons">home</i></Link>
                          </li>
                          <li title="Log Out">
                               <Link to="#" className="right" onClick={this.logout}>
                               <i className="material-icons right">exit_to_app</i>
                               </Link>
                          </li>
                          <li title="notification">
                               <Link to="#" className="right">
                               <i className="hide-on-med-and-down material-icons right">notifications</i>
                               </Link>
                          </li>
                          <li title="menu" className="droppp">
                          <i className="hide-on-med-and-down material-icons men" >more_vert</i>

                          <ul >
                          {this.props.content.map((content,index)=>{
                            return(
                              <li key={content.val}><Link to={content.link} className="center">{content.val}</Link></li>
                            )
                          })}
                          </ul>
                            </li>
                          <li></li>
                          </ul>
                          </div>
                      </nav>
                      </div>
        );
      }

    }
}
