import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import newId from './id'
import Pari from "./tab";
import '../style.css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'

class Navbar extends Component {

  state = {
    width: "600px",
    height: "30px"
  };

    componentDidMount() {
      M.AutoInit();
      this.id=newId();
      let collapsible = document.querySelectorAll(".collapsible");
      M.Collapsible.init(collapsible, {});

  }

    render() {
      var data = {
            fielddata: [
              {
                header: "No. ",
                inputfield: true,
                length: 10,
                name: "date_created",
                placeholder: "",
                content: "text",
                grid : "two",
              },
              {
                header: "Date",
                inputfield: true,
                length: 10,
                name: "date",
                placeholder: "",
                content: "text",
                grid : "two",
              },
              {
                header: "Conference Title",
                inputfield: true,
                length: 10,
                name: "conft",
                content: "text",
                grid : "two",
                placeholder: ""
              },
              {
                header: "Location",
                inputfield: true,
                length: 7,
                name: "loc",
                content: "text",
                grid : "two",
                placeholder: ""
              },
              {
                header: "Mention Your Role",
                inputfield: true,
                length: 7,
                name: "role",
                content: "text",
                grid : "two",
                placeholder: ""
              }
            ],
            instances: [
              {
              date_created: "",
              id: ''
            }
            ]
          };
      var dat1 = {

            fielddata: [

              {
                id: '',
                header: "No.",
                inputfield: true,
                length: 10,
                name: "date_created",
                placeholder: "",
                content: "text",
                grid : "two",
              },
              {
                id: '',
                header: "Research Area",
                inputfield: true,
                length: 10,
                name: "rarea",
                placeholder: "",
                content: "text",
                grid : "two",
              },
              {
                id: '',
                header: "Project/Research Title",
                inputfield: true,
                length: 10,
                name: "p/rtitle",
                content: "text",
                grid : "two",
                placeholder: ""
              },
              {
                id: '',
                header: "Description",
                inputfield: true,
                length: 7,
                name: "descrip",
                content: "text",
                grid : "two",
                placeholder: ""
              },
              {
                id: '',
                header: "Mention Your Role",
                inputfield: true,
                length: 7,
                name: "role",
                content: "text",
                grid : "two",
                placeholder: ""
              },
            ],
            instances: [
              {
              date_created: '',
              id: ''
            }
            ]
          };
          var dat2 = {
                fielddata: [
                  {
                    header: "No. ",
                    inputfield: true,
                    length: 10,
                    name: "date_created",
                    placeholder: "",
                    content: "text",
                    grid : "two",
                  },
                  {
                    header: "Date",
                    inputfield: true,
                    length: 10,
                    name: "dedate",
                    placeholder: "",
                    content: "text",
                    grid : "two",
                  },
                  {
                    header: "Activity Details",
                    inputfield: true,
                    length: 10,
                    name: "dactivity",
                    content: "text",
                    grid : "three",
                    placeholder: ""
                  },
                  {
                    header: "Mention your role",
                    inputfield: true,
                    length: 7,
                    name: "drole",
                    content: "text",
                    grid : "three",
                    placeholder: ""
                  }
                ],
                instances: [
                  {
                  date_created: "",
                  id: ''
                }
                ]
              };
              var dat3 = {
                    fielddata: [
                      {
                        header: "No. ",
                        inputfield: true,
                        length: 10,
                        name: "date_created",
                        placeholder: "",
                        content: "text",
                        grid : "two",
                      },
                      {
                        header: "Date",
                        inputfield: true,
                        length: 10,
                        name: "depcommitydate",
                        placeholder: "",
                        content: "text",
                        grid : "two",
                      },
                      {
                        header: "Commity Details",
                        inputfield: true,
                        length: 10,
                        name: "conft",
                        content: "text",
                        grid : "two",
                        placeholder: ""
                      },
                      {
                        header: "Mention your Position and Role",
                        inputfield: true,
                        length: 7,
                        name: "loc",
                        content: "text",
                        grid : "two",
                        placeholder: ""
                      },
                      {
                        header: "Number of the Metting Attended",
                        inputfield: true,
                        length: 7,
                        name: "role",
                        content: "text",
                        grid : "two",
                        placeholder: ""
                      }
                    ],
                    instances: [
                      {
                      date_created: "",
                      id: ''
                    }
                    ]
                  };
                  var dat4 = {
                        fielddata: [
                          {
                            header: "No. ",
                            inputfield: true,
                            length: 10,
                            name: "date_created",
                            placeholder: "",
                            content: "text",
                            grid : "two",
                          },
                          {
                            header: "Date",
                            inputfield: true,
                            length: 10,
                            name: "guidingdate",
                            placeholder: "",
                            content: "text",
                            grid : "two",
                          },
                          {
                            header: "Details of the Student(Academic only like sem,year,reg no)",
                            inputfield: true,
                            length: 10,
                            name: "conft",
                            content: "text",
                            grid : "two",
                            placeholder: ""
                          },
                          {
                            header: "Avg no of advising Hours per sem",
                            inputfield: true,
                            length: 7,
                            name: "loc",
                            content: "text",
                            grid : "two",
                            placeholder: ""
                          },
                          {
                            header: "Description of Advising Activity",
                            inputfield: true,
                            length: 7,
                            name: "role",
                            content: "text",
                            grid : "two",
                            placeholder: ""
                          }
                        ],
                        instances: [
                          {
                          date_created: "",
                          id: ''
                        }
                        ]
                      };
                      var dat5 = {
                            fielddata: [
                              {
                                header: "No. ",
                                inputfield: true,
                                length: 10,
                                name: "date_created",
                                placeholder: "",
                                content: "text",
                                grid : "two",
                              },
                              {
                                header: "Date",
                                inputfield: true,
                                length: 10,
                                name: "date",
                                placeholder: "",
                                content: "text",
                                grid : "two",
                              },
                              {
                                header: "Conference Title",
                                inputfield: true,
                                length: 10,
                                name: "conft",
                                content: "text",
                                grid : "two",
                                placeholder: ""
                              },
                              {
                                header: "Location",
                                inputfield: true,
                                length: 7,
                                name: "loc",
                                content: "text",
                                grid : "two",
                                placeholder: ""
                              },
                              {
                                header: "Mention Your Role",
                                inputfield: true,
                                length: 7,
                                name: "role",
                                content: "text",
                                grid : "two",
                                placeholder: ""
                              }
                            ],
                            instances: [
                              {
                              date_created: "",
                              id: ''
                            }
                            ]
                          };

        const loggedIn = this.props.loggedIn;
        //console.log('navbar render, props: ')
        //console.log(this.props);


            if(this.props.loggedIn)
            {

              return(
              <div className="grid-of-parts">

              {/*This is a link to Part A */}
              <Link to="/partA">
              <a class="btn-floating btn-large waves-effect waves-light #e91e63 pink move"><i class="large material-icons icn">chevron_left</i></a>
              </Link>
              <span className="go-to-alt"><b className="mv">Go to Part A</b></span>

            {/* Here start Sections codes of collapsible */}

              <Collapsible popout>
                    <CollapsibleItem header={
                        <h5 className="collaphead">Professional Development Activities</h5>
                      } expanded>
       <div className="row">
          <div className="col s1"><h5 className="right td ft">A .</h5>
          </div>
       <div className="col s11 td">
       <h5 className="ft">Please Provide info about the International /National Conference Attended / Participated </h5>
       </div>
     </div>
<br />
<br />
     <div className="row">
       <Pari data={data} />
     </div>
  </CollapsibleItem>
</Collapsible>

{/*--------------------------------------------------------------------------------------------End of Profrssional Development Activities */}

<Collapsible popout>
<CollapsibleItem header={
          <h5 className="collaphead">Research and Projects Works</h5>
        }>
        <div className="row">
        <div className="col s1"><h5 className="right td ft">A .</h5>
        </div>
        <div className="col s11 td">
        <h5 className="ft">Please Provide your Research and Project Details </h5>
        </div>
        </div>
<br />
<br />
<div className="row">
<Pari data={dat1} />
</div>
</CollapsibleItem>
</Collapsible>

{/*----------------------------------------------------------------------------------------------------End of Research and Project works*/}


<Collapsible popout>
      <CollapsibleItem header={
          <h5 className="collaphead">Department Activities</h5>
        }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Please Provide the Details about your Department Activities(including events Like Seminer, Workshop etc) </h5>
</div>
</div>
<br />
<br />
<div className="row">
<Pari data={dat2} />
</div>
</CollapsibleItem>
</Collapsible>

{/*----------------------------------------------------------------------------------------------------------End of Department Activities*/}

<Collapsible popout>
      <CollapsibleItem header={
          <h5 className="collaphead">Department Committes Membership</h5>
        }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Please Provide about your Participation in Dept. Committes</h5>
</div>
</div>
<br />
<br />
<div className="row">
<Pari data={dat3} />
</div>
</CollapsibleItem>
</Collapsible>

{/*-------------------------------------------------------------------------------------------------End of Department Commities meetings*/}


<Collapsible popout>
      <CollapsibleItem header={
          <h5 className="collaphead">Advising and Counselling Details</h5>
        }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Mention your Participation in Advising and Counselling Students(for example guiding the Students in their academic and so on)</h5>
</div>
</div>
<br />
<br />
<div className="row">
<Pari data={dat4} />
</div>
</CollapsibleItem>
</Collapsible>

{/* -----------------------------------------------------------------------------------------------End of Advising & Counselling Details*/}


<Collapsible popout>
      <CollapsibleItem header={
          <h5 className="collaphead">Professional Development Activities</h5>
        }>
<div className="row">
<div className="col s1"><h5 className="right td ft">A .</h5>
</div>
<div className="col s11 td">
<h5 className="ft">Please Provide info about the International /National Conference Attended / Participated </h5>
</div>
</div>
<br />
<br />
<div className="row">
<Pari data={dat5} />
</div>
</CollapsibleItem>
</Collapsible>

              </div>
            );

          }
          else{
              return(
                <div className="error-div">
                 <div className="row">
                 <div className="col s4">
                 <i className="large material-icons right error-icon">warning</i>
                 </div>
                 <div className="col s6">
                 <h3 className="left error-msg">Sorry !! You are not logged in</h3>
                 </div>
                 <div className="col s2">
                 </div>
                 </div>
                 <div className="row">
                 <h5 className="reminder">Please Log In to Access this Site</h5>
                 </div>
                </div>
              );
          }




    }
}

export default Navbar
