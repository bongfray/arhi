import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'

var empty = require('is-empty');

class Signup extends Component {
	constructor() {
    super()
    this.state = {
			redirectTo: null,
        title: '',
        name: '',
        mailid: '',
        username: '',
        phone: '',
        password: '',
        cnf_pswd: '',
        campus: '',
        dept: '',
        desgn: '',
				dob:'',
				count: 0,
    }
    this.handleDesgn = this.handleDesgn.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    }

		handleField = (e) =>{
			this.setState({
				[e.target.name] : e.target.value,
			})
		}
					handleDesgn = (e) => {
						let total_adminispercentage;
						let total_researchpercentage;
						let total_academicpercentage;
						let ad_total_role_res;
						let ad_total_clerical ;
						let ad_total_planning ;
						let academic_total_curricular;
						let academic_total_cocurricular;
						let academic_total_extracurricular;
						let academic_total_evaluation_placementwork;
						let research_total_publication;
						let research_total_ipr_patents;
						let research_total_funded_sponsored_projectes;
						let research_total_tech_dev_consultancy;
						let research_total_product_development;
						let research_total_research_center_establish;
						let research_total_research_guidnce;
						if( (e.target.value==="Principle") || (e.target.value==="Director") )
						{
							total_adminispercentage = 50;
							total_academicpercentage = 10;
							total_researchpercentage = 40;
							ad_total_role_res =10;
							ad_total_clerical = 5;
							ad_total_planning =5;
							academic_total_curricular=4;
							research_total_publication=2;
							research_total_ipr_patents=4;
							research_total_funded_sponsored_projectes=2;
							research_total_tech_dev_consultancy=4;
							research_total_research_center_establish=4;
						}
						else if((e.target.value==="Assistant Director") || (e.target.value==="Dean") || (e.target.value==="HOD") )
						{
							total_adminispercentage = 45;
							total_academicpercentage = 25;
							total_researchpercentage = 30;
							ad_total_role_res =6;
							ad_total_clerical = 6
							ad_total_planning =6;
							academic_total_curricular=10;
							research_total_publication=2;
							research_total_ipr_patents=2;
							research_total_funded_sponsored_projectes=2;
							research_total_tech_dev_consultancy=4;
						}
						else if((e.target.value==="Professor") )
						{
							total_adminispercentage = 25;
							total_academicpercentage= 35;
							total_researchpercentage = 40;
							ad_total_role_res =2;
							ad_total_clerical =2;
							ad_total_planning =6;
							academic_total_curricular=14;
							research_total_publication=2;
							research_total_ipr_patents=4;
							research_total_funded_sponsored_projectes=4;
							research_total_tech_dev_consultancy=4;
							research_total_research_guidnce=2;
						}
						else if((e.target.value==="Assistant Professor") )
						{
							total_adminispercentage = 25;
							total_academicpercentage = 50;
							total_researchpercentage = 25;
							ad_total_role_res =6;
							ad_total_clerical =2;
							ad_total_planning =2;
							academic_total_curricular=16;
							academic_total_cocurricular=1;
							academic_total_extracurricular=1;
							academic_total_evaluation_placementwork=2;
							research_total_publication=3;
							research_total_ipr_patents=1;
							research_total_funded_sponsored_projectes=2;
							research_total_tech_dev_consultancy=2;
							research_total_research_center_establish=2;
						}
						else if((e.target.value==="Associate Professor") )
						{
							total_adminispercentage = 20;
							total_academicpercentage = 40;
							total_researchpercentage = 40;
							ad_total_role_res =4;
							ad_total_clerical =2;
							ad_total_planning =2;
							academic_total_curricular=14;
							academic_total_cocurricular=1;
							academic_total_evaluation_placementwork=1;
							research_total_publication=4;
							research_total_ipr_patents=2;
							research_total_funded_sponsored_projectes=4;
							research_total_tech_dev_consultancy=2;
							research_total_product_development=2;
							research_total_research_guidnce=2;
						}
						this.setState({
								desgn: e.target.value,
								total_adminispercentage: total_adminispercentage,
								total_academicpercentage: total_academicpercentage,
								total_researchpercentage: total_researchpercentage,
								ad_total_role_res:ad_total_role_res,
								ad_total_clerical:ad_total_clerical,
								ad_total_planning:ad_total_planning,
								academic_total_curricular:academic_total_curricular,
								academic_total_cocurricular:academic_total_cocurricular,
								academic_total_extracurricular:academic_total_extracurricular,
								academic_total_evaluation_placementwork:academic_total_evaluation_placementwork,
								research_total_publication:research_total_publication,
								research_total_ipr_patents:research_total_ipr_patents,
								research_total_funded_sponsored_projectes:research_total_funded_sponsored_projectes,
								research_total_tech_dev_consultancy:research_total_tech_dev_consultancy,
								research_total_product_development:research_total_product_development,
								research_total_research_center_establish:research_total_research_center_establish,
								research_total_research_guidnce:research_total_research_guidnce,
						});
			        };
	handleSubmit(event) {
		event.preventDefault()
		if(empty(this.state.title)||empty(this.state.name)||empty(this.state.mailid)||empty(this.state.username)||empty(this.state.phone)||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.dept)||empty(this.state.desgn))
		{
			window.M.toast({html: 'Enter all the Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
      return false;
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
	     window.M.toast({html: 'Password does not match', outDuration:'850', inDuration:'800', displayLength:'1500'});
	     return false;

	  }
		else if ( ((this.state.password).match(/\d/)) && ((this.state.password).match(/[A-Z]/)) && ((this.state.password).match(/[A-z]/)))
		{
			window.M.toast({html: 'Password Pattern {A-a-1}', outDuration:'850', inDuration:'800', displayLength:'1500'});

		}
		else if ((this.state.phone).length!==10)
		{
			window.M.toast({html: 'Enter correct format of Phone no', outDuration:'850', inDuration:'800', displayLength:'1500'});
			this.setState(this.state);
			return false;
		}
		else{



		axios.post('/user/', {
			username: this.state.username,
			password: this.state.password,
			title: this.state.title,
      name: this.state.name,
      mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
	    dept: this.state.dept,
	    desgn:this.state.desgn,
			dob: this.state.dob,
			count: this.state.count,
			total_adminispercentage: this.state.total_adminispercentage,
			total_academicpercentage: this.state.total_academicpercentage,
			total_researchpercentage: this.state.total_researchpercentage,
			ad_total_role_res:this.state.ad_total_role_res,
			ad_total_clerical:this.state.ad_total_clerical,
			ad_total_planning:this.state.ad_total_planning,
			academic_total_curricular:this.state.academic_total_curricular,
			academic_total_cocurricular:this.state.academic_total_cocurricular,
			academic_total_extracurricular:this.state.academic_total_extracurricular,
			academic_total_evaluation_placementwork:this.state.academic_total_evaluation_placementwork,
			research_total_publication:this.state.research_total_publication,
			research_total_ipr_patents:this.state.research_total_ipr_patents,
			research_total_funded_sponsored_projectes:this.state.research_total_funded_sponsored_projectes,
			research_total_tech_dev_consultancy:this.state.research_total_tech_dev_consultancy,
			research_total_product_development:this.state.research_total_product_development,
			research_total_research_center_establish:this.state.research_total_research_center_establish,
			research_total_research_guidnce:this.state.research_total_research_guidnce,
		})
			.then(response => {
				console.log(response)
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, outDuration:'9000', classes:'rounded #ba68c8 purple lighten-2'});
				  }
					else if(response.data.succ)
					{
						alert(response.data.succ);
						this.setState({
								redirectTo: '/flogin'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
			})
			this.setState(this.state)
	}
}
	notifi = () => window.M.toast({html: 'Enter Details', outDuration:'1000', inDuration:'900', displayLength:'1800'});

    componentDidMount() {
        this.notifi();
        M.AutoInit();
    }

render() {
	if (this.state.redirectTo) {
			 return <Redirect to={{ pathname: this.state.redirectTo }} />
	 } else {
	return (
		<div className="row">

		<div className="col s2">
		</div>

		<div className="col l8 s12 m12 form-signup">
				<div className="ew center">
						<h5 className="reg">REGISTRATION</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">

								<div className="input-field col s2">
										<select name="title" value={this.state.title} onChange={this.handleField}>
										<option value="" disabled selected>Title</option>
										<option value="Mr.">Mr.</option>
										<option value="Mrs.">Mrs.</option>
										<option value="Miss.">Miss.</option>
										<option value="Dr.">Dr.</option>
										</select>
								</div>

								<div className="input-field col s6">
								<input id="name" name="name" type="text" className="validate" value={this.state.name} onChange={this.handleField} required />
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s4">
								<input id="fac_id" name="username" type="text" className="validate" value={this.state.username} onChange={this.handleField} required />
								<label htmlFor="fac_id">Official ID</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s5">
								<input id="email" name="mailid" type="email" className="validate" value={this.state.mailid} onChange={this.handleField} required />
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4">
								<input id="ph_num" type="text" name="phone" className="validate" value={this.state.phone} onChange={this.handleField} required />
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="input-field col s3">
								<input id="dob" type="text" name="dob" className="validate" value={this.state.dob} onChange={this.handleField} required />
								<label htmlFor="dob">D.O.B.</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6">
								<input onChange={this.handleField}  name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								</div>

								<div className="input-field col s6">
								<input onChange={this.handleField} id="cnf_pswd" name="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>

						</div>
						<div className="input-field row">

								<div className="input-field col s4">
										<select value={this.state.campus} name="campus" onChange={this.handleField}>
										<option value="" disabled selected>Campus</option>
										<option value="Kattankulathur Campus">Kattankulathur Campus</option>
										<option value="Ramapuram Campus">Ramapuram Campus</option>
										<option value="Vadapalani Campus">Vadapalani Campus</option>
										<option value="NCR Campus">NCR Campus</option>
										</select>
								</div>
								<div className="input-field col l4 s4 m4">
										<select value={this.state.dept} name="dept" onChange={this.handleField}>
										<option value="" disabled selected>Department</option>
										<option value="Computer Science">Computer Science</option>
										<option value="Information Technology">Information Technology</option>
										<option value="Software Engineering">Software</option>
										<option value="Mechanical Engineering">Mechanical</option>
										</select>
								</div>
								<div className="input-field col l4 s4 m4">
										<select value={this.state.desgn} onChange={this.handleDesgn}>
										<option value="" disabled selected>Designation</option>
										<option value="Director">Director</option>
										<option value="Principle">Principle</option>
										<option value="Assistant Director">Assistant Director</option>
										<option value="Dean">Dean</option>
										<option value="HOD">HOD</option>
										<option value="Professor">Professor</option>
										<option value="Associate Professor">Associate Professor</option>
										<option value="Assistant Professor">Assistant Professor</option>

										</select>
								</div>


						</div>
						<br/>
						<div className="row"><div className="col l6 m12 s12 left">
						<Link to='/flogin' className="log"> <b> Login Instead ?</b></Link></div>

						<div className="col l6 s12 m12 right">
						<Link to="#" className="waves-effect btn col l6 s6 blue-grey darken-2 sup right" onClick={this.handleSubmit}>Submit</Link>
						</div>
						</div>
				</form>
		</div>
		</div>

	);
}
}
}

export default Signup
