import React, { Component } from 'react';
import { Route, Switch} from 'react-router-dom'
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';


import Signup from './components/Faculty/sign-up'
import Login from './components/Faculty/login-form'
import Home from './components/Home/home'
import Flip from './components/Faculty/flip'
import Form from './primary-react'
class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">

         <Switch>

        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route path="/signup" component={Signup} />

        <Route path="/flip" component={Flip} />
        <Route path="/form" component={Form}/>


        </Switch>
      
      </div>
    );
  }
}

export default App;
