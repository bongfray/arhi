import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import {} from 'materialize-css'

export default class Form extends Component{
    constructor(){
        super();
        this.state={
            name:'',
            email:'',
            college:'',
            regid:'',
            file:'',
        }
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    handleField = (e) =>{
        this.setState({
            [e.target.name] : e.target.value,
        })
    }
    handleSubmit(event) {
        event.preventDefault()
        var files = this.state.file.files;
        var accept = {
            binary : ["image/png", "image/jpeg"],
            text   : ["text/plain", "text/css", "application/xml", "text/html"]
          };
        var file;

      var data = file.getAsBinary();
      console.log(data)
    
  
}

    
    render(){
        return(
            <React.Fragment>
                <div className="row">
                    <div className="center">
                        <div className="col s4">
                            <div className="input-field col s12">
                                <input id="name" name="name" type="text" onChange={this.handleField} className="validate" required />
                                <label htmlFor="name">Name:</label>
                            </div>
                            <div className="input-field col s12">
                                <input id="email" name="email" type="text" onChange={this.handleField} className="validate" required />
                                <label htmlFor="email">E-Mail (College Official Mail) :</label>
                            </div>
                            <div className="input-field col s12">
                                <input id="college" name="college" type="text" onChange={this.handleField} className="validate" required />
                                <label htmlFor="college">College Name :</label>
                            </div>
                            <div className="input-field col s12">
                                <input id="regid" name="regid" type="text" onChange={this.handleField} className="validate" required />
                                <label htmlFor="regid">Registration ID (Student ID):</label>
                            </div>
                            <div className="input-field col s12">
                                <span>File to Upload: </span><input id="file" onChange={this.handleField} name="file" type="file" className="validate" required />
                            </div>
                            <Link onClick={this.handleSubmit} className="btn #1565c0 blue darken-3 col s5">Submit</Link>

                        </div>
                    </div>

                </div>
            </React.Fragment>
        )
    }
}