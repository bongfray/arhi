import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import {} from 'materialize-css'
import {Select,Modal,Button,Icon} from 'react-materialize'

var empty = require('is-empty');

export default class Signup extends Component {
	constructor() {
    super()
    this.initialState = {
			redirectTo: null,
			username: '',
			password: '',
			cnf_pswd: '',
	}
    }
		componentDidMount(){
			
		}


render() {
	return (
		<div className="row">

			<div className="col s12 nav-fac #1565c0 blue darken-3">Faculty Sign Up</div>

				<div className="col s1"></div>

					<div className="col s10 card form-log">
						<div className="center heading #1565c0 blue darken-3">GRAD</div>
						<h5 className="center">Registration</h5>
						<form>
							<div className="row">

								<div class="input-field col s2">
									<select>
									<option value="" disabled selected>Select</option>
									<option value="Mr.">Mr.</option>
									<option value="Mrs.">Mrs.</option>
									<option value="Miss.">Miss.</option>
									<option value="Dr.">Dr.</option>
									</select>
									<label>Salutation</label>
								</div>

								<div className="input-field col s5">
									<input id="name" type="text" className="validate" required />
									<label htmlFor="name">Full Name</label>
								</div>

								<div className="input-field col s5">
									<input id="facid" type="text" className="validate" required />
									<label htmlFor="facid">Official ID</label>
								</div>

							</div>

							<div className="row">

								<div className="input-field col s6">
									<input id="mailid" type="text" className="validate" required />
									<label htmlFor="mailid">Email</label>
								</div>

								<div className="input-field col s4">
									<input id="phone" type="text" className="validate" required />
									<label htmlFor="phone">Phone Number</label>
								</div>
								
								<div className="input-field col s2">
									<input id="dob" type="text" className="validate" required />
									<label htmlFor="dob">D.O.B</label>
								</div>

							</div>

							<div className="row">

								<div className="input-field col s6">
									<input id="pswd" type="password" className="validate" required />
									<label htmlFor="pswd">Password</label>
								</div>

								<div className="input-field col s6">
									<input id="cnf_pswd" type="password" className="validate" required />
									<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>
							
							</div>
							
							<div className="row">
								<Link id="button" onClick={this.handleClick} className="btn #1565c0 blue darken-3 col s4 offset-s4">Submit</Link>
							</div>
								
						</form>
						</div>

						<div className="col s1"></div>

						</div>
			
	);
						}

}
