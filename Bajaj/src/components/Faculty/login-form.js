import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import axios from 'axios'
import $ from "jquery";
import M from 'materialize-css';
import { } from 'materialize-css';
import './styles.css'
import Signup from './sign-up'
var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            error:'',
            username: '',
            password: '',
            redir: '',
            redirectTo: null,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleClick=this.handleClick.bind(this)
    }
        componentDidMount() {
            M.AutoInit();
        }

        handleClick(){
            var element = document.getElementById("new");
            element.classList.toggle("half");
            var ele = document.getElementById("con");
            ele.classList.toggle("half");
        }
        

       
    render() {
        
            return (
              <React.Fragment>
                
                    <div id="con" className="content-container">
                        <div className="content">
                            <div className="row">

                            <div className="col s12 nav-fac #1565c0 blue darken-3">Faculty Log In</div>

                            <div className="col s4"></div>

                            <div className="col s4 card form-log">
                                <div className="center heading #1565c0 blue darken-3">GRAD</div>
                                <h5 className="center">LOG IN</h5>
                               <form>
                                    <div className="row">
                                        <div className="input-field col s12">
                                            <input id="email" type="text" className="validate" required />
                                            <label htmlFor="email">Username</label>
                                        </div>
                                        <div className="input-field col s12">
                                            <input id="password" type="password" className="validate" required />
                                            <label htmlFor="password">Password</label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <Link id="button" onClick={this.handleClick} className="btn #1565c0 blue darken-3 col s5">Register</Link>
                                        <div className="col s2"></div>
                                        <Link to="/login" className="btn #1565c0 blue darken-3 col s5">Login</Link>
                                    </div>
                                    
                               </form>
                            </div>

                            <div className="col s4"></div>
                            
                            </div>
                        </div>
                    </div>
              

              
                    <div id="new" className="new-content">
                        <Signup />
                    </div>

             </React.Fragment>
           );
            }
         

    }


export default LoginForm
