import React, { Component } from 'react'
import {} from 'materialize-css'

export default class Flip extends Component {
    render() {
        return (
            <div class='card center col s3' id='flip'>
                <a href='#flip' class='flip'>Flip front</a>
                <a href='#unflip' class='unflip'>back</a>
                <section className="front"><br/><br/>    <img src='https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRZNrO98qaNGRBnMuuMctzqNMsE0DeUQXelgWi2zPsKYmjD-lqn'/>
</section>
                <section className="back"><br/><br/>    <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSwcvrsqZZFqZYMPSfVGDomBaDbQc2b7A1i88kDA2P52k3VN0y7w' />
</section>
               
            </div>
        )
    }
}
