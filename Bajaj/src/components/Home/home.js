import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import $ from "jquery";
import {} from 'materialize-css'
import './styles.css'
import Back from './backg.png'



export default class Home extends Component {
  componentDidMount() {
    $('#button').click(function(){
      $('.new-content').toggleClass('half').delay(600).fadeIn(100);
      $('.content-container').toggleClass('half');
    });
  }
    
    render() {
        return (
            <React.Fragment>
            
                <div className="row">

                  <div className="contain col s6 center fac">
                  Faculty
                    <div className="overlay">
                      <Link to="/login" className="text">Go to Faculty Section </Link><span><i class="small material-icons">account_box</i></span>
                    </div>
                  </div>

                  <div className="contain col s6 center stu">
                  Student
                  <div className="overlay">
                      <Link to="/login" className="text">Go to Student Section </Link><span><i class="small material-icons">account_box</i></span>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col s4 fac-highlight">
                    <h4>Faculty Component</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus, consequuntur dicta assumenda, tempora excepturi quis voluptas aliquam praesentium nam, quae cupiditate! Quo aliquam earum ratione quasi voluptas! Inventore, amet qui!</p>
                  </div>
                  <div className="col s4 center"> <img src={Back} alt="" height='500px'/> </div>
                  <div className="col s4 stu-highlight">
                  <h4>Student Component</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus, consequuntur dicta assumenda, tempora excepturi quis voluptas aliquam praesentium nam, quae cupiditate! Quo aliquam earum ratione quasi voluptas! Inventore, amet qui!</p>
                  
                  </div>
                  
                </div>

                <div className="row center contact">
                  Contact Us
                </div>

                {/* <div class="content-container">
              <div class="content">
                <button id="button">
                  Click me
                </button>
                
              </div>
            </div>
            
            <div class="new-content">

              <p>sjnfdn gjsknfg najnfgn sn</p>
            </div>
               
           */}
            
            </React.Fragment>
        )
    }
}
