import React, { Component } from "react";
import axios from "axios";

import { Link } from "react-router-dom";
import M from "materialize-css";
//import { } from 'materialize-css';
import "../style.css";
var empty = require("is-empty");

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      name: "",
      id: "",
      username: "",
      phone: "",
      campus: "",
      dept: "",
      desgn: "",
      ust_year: "",
      ufn_year: "",
      marksug: "",
      pst_year: "",
      pfn_year: "",
      markspg: "",
      password: "",
      cnf_pswd: ""
    };
    this.handleTitle = this.handleTitle.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleId = this.handleId.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePhone = this.handlePhone.bind(this);
    this.handleCampus = this.handleCampus.bind(this);
    this.handleDept = this.handleDept.bind(this);
    this.handleDesgn = this.handleDesgn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleuMarks = this.handleuMarks.bind(this);
    this.handleuStYear = this.handleuStYear.bind(this);
    this.handleuFnYear = this.handleuFnYear.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleConfirmPassword = this.handleConfirmPassword.bind(this);
  }

  handleTitle = e => {
    this.setState({
      title: e.target.value
    });
  };
  handleName = e => {
    this.setState({
      name: e.target.value
    });
  };
  handleId = e => {
    this.setState({
      id: e.target.value
    });
  };
  handleEmail = e => {
    this.setState({
      username: e.target.value
    });
  };
  handlePhone = e => {
    this.setState({
      phone: e.target.value
    });
  };
  handleCampus = e => {
    this.setState({
      campus: e.target.value
    });
  };
  handleDept = e => {
    this.setState({
      dept: e.target.value
    });
  };
  handleDesgn = e => {
    this.setState({
      desgn: e.target.value
    });
  };
  handleuStYear = e => {
    this.setState({
      ust_year: e.target.value
    });
  };
  handleuFnYear = e => {
    this.setState({
      ufn_year: e.target.value
    });
  };
  handleuMarks = e => {
    this.setState({
      marksug: e.target.value
    });
  };
  handlepStYear = e => {
    this.setState({
      pst_year: e.target.value
    });
  };
  handlepFnYear = e => {
    this.setState({
      pfn_year: e.target.value
    });
  };
  handlePasswordChange = e => {
    this.setState({
      password: e.target.value
    });
  };
  handleConfirmPassword = e => {
    this.setState({
      cnf_pswd: e.target.value
    });
  };
  handlepMarks = e => {
    this.setState({
      markspg: e.target.value
    });
  };
  handleSubmit(event) {
    //console.log('sign-up handleSubmit, username: ')
    //console.log(this.state.username)
    event.preventDefault();
  }
  notifi = () =>
    window.M.toast({
      html: "Enter Details",
      outDuration: "850",
      inDuration: "800",
      displayLength: "1500"
    });

  componentDidMount() {
    this.notifi();
    M.AutoInit();
  }

  render() {
    return (
      <div className="row">
        <div className="col s2" />

        <div className="col s8 form-signup">
          <div className="ew center">
            <h5 className="reg">PROFILE INFORMATION</h5>
          </div>
          <form className="row form-con">
            <div className="input-field row">
              <div className="input-field col s2">
                <input
                  id="title"
                  type="text"
                  className="validate"
                  value={this.state.title}
                  onChange={this.handleTitle}
                  disabled
                />
                <label htmlFor="title">Title</label>
              </div>

              <div className="input-field col s6">
                <input
                  id="name"
                  type="text"
                  className="validate"
                  value={this.state.name}
                  onChange={this.handleName}
                  disabled
                />
                <label htmlFor="name">Name</label>
              </div>

              <div className="input-field col s4">
                <input
                  id="fac_id"
                  type="text"
                  className="validate"
                  value={this.state.id}
                  onChange={this.handleId}
                  disabled
                />
                <label htmlFor="fac_id">ID</label>
              </div>
            </div>

            <div className="input-field row">
              <div className="input-field col s4">
                <input
                  id="campus"
                  type="text"
                  className="validate"
                  value={this.state.campus}
                  onChange={this.handleCampus}
                  disabled
                />
                <label htmlFor="campus">Campus</label>
              </div>
              <div className="input-field col s4">
                <input
                  id="dept"
                  type="text"
                  className="validate"
                  value={this.state.dept}
                  onChange={this.handleDept}
                  disabled
                />
                <label htmlFor="dept">Department</label>
              </div>
              <div className="input-field col s4">
                <input
                  id="desgn"
                  type="text"
                  className="validate"
                  value={this.state.desgn}
                  onChange={this.handleDesgn}
                  disabled
                />
                <label htmlFor="desgn">Name</label>
              </div>
            </div>

            <div className="input-field row">
              <div className="input-field col s8">
                <input
                  id="email"
                  type="email"
                  className="validate"
                  value={this.state.username}
                  onChange={this.handleEmail}
                  disabled
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="input-field col s4">
                <input
                  id="ph_num"
                  type="text"
                  className="validate"
                  value={this.state.phone}
                  onChange={this.handlePhone}
                  disabled
                />
                <label htmlFor="ph_num">Phone Number</label>
              </div>
            </div>
            <div className="input-field row">
              <div className="input-field col s6">
                <input
                  onChange={this.handlePasswordChange}
                  id="pswd"
                  value={this.state.password}
                  type="password"
                  className="validate"
                  required
                />
                <label htmlFor="pswd">Password</label>
              </div>

              <div className="input-field col s6">
                <input
                  onChange={this.handleConfirmPassword}
                  id="cnf_pswd"
                  value={this.state.cnf_pswd}
                  type="password"
                  className="validate"
                  required
                />
                <label htmlFor="cnf_pswd">Confirm Password</label>
              </div>
            </div>
            <div className="input-field row">
              <div className="col s3">
                <h6 className="labdeg">U.G Degree:</h6>
              </div>
              <div className="input-field col s3">
                <input
                  id="ust_year"
                  type="number"
                  className="validate"
                  value={this.state.ust_year}
                  onChange={this.handleuStYear}
                  disabled
                />
                <label htmlFor="ust_year">Starting Year</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="ufn_year"
                  type="number"
                  className="validate"
                  value={this.state.ufn_year}
                  onChange={this.handleuFnYear}
                  disabled
                />
                <label htmlFor="ufn_year">Year of Completion</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="marksug"
                  type="text"
                  className="validate"
                  value={this.state.marksug}
                  onChange={this.handleuMarks}
                  disabled
                />
                <label htmlFor="marksug">Percentage/GPA</label>
              </div>
            </div>
            <div className="input-field row">
              <div className="col s3 labdeg">
                <h6 className="labdeg">P.G Degree:</h6>
              </div>
              <div className="input-field col s3">
                <input
                  id="pst_year"
                  type="number"
                  className="validate"
                  value={this.state.pst_year}
                  onChange={this.handlepStYear}
                  disabled
                />
                <label htmlFor="pst_year">Starting Year</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="pfn_year"
                  type="number"
                  className="validate"
                  value={this.state.pfn_year}
                  onChange={this.handlepFnYear}
                  disabled
                />
                <label htmlFor="pfn_year">Year of Completion</label>
              </div>

              <div className="input-field col s3">
                <input
                  id="markspg"
                  type="text"
                  className="validate"
                  value={this.state.markspg}
                  onChange={this.handlepMarks}
                  disabled
                />
                <label htmlFor="markspg">Percentage/GPA</label>
              </div>
            </div>
            <div>
              <div className="col s3" />
              <div className="col s5" />
              <Link
                to="#"
                className="waves-effect waves-light btn blue-grey darken-2 col s4 sup"
                onClick={this.handleSubmit}
              >
                Save
              </Link>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Profile;
