import React, { Component } from 'react';
import Sunburst from './global-data/Sunburst';
import data from './global-data/data';


class AssoDash extends Component{
  notify = () => window.M.toast({html: 'Welcome to Dashboard!', outDuration:'850', inDuration:'800', displayLength:'1500'});
componentDidMount(){
  this.notify();
}
    render(){
        return(
            <div>

            <Sunburst data={data}
            width="700"
            height="700"
            count_member="size"
            labelFunc={(node)=>node.data.name}
            _debug={true} />
            </div>
        )
    }
}

export default AssoDash;
