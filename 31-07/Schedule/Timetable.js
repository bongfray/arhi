import React, { Component } from 'react';
import axios from 'axios'
// import {API_URL} from '../utils/apiUrl'
import { Link } from 'react-router-dom';
import M from 'materialize-css';
//import { } from 'materialize-css';
import '../style.css'
var empty = require('is-empty');


class Timetable extends Component {
	constructor() {
    super()
    this.state = {
    
    }
   

    }

    
  notifi = () => window.M.toast({html: 'Enter Details', outDuration:'850', inDuration:'800', displayLength:'1500'});

    componentDidMount() {
        this.notifi();
        M.AutoInit();
    }


render() {
	return (
        <div className="row tcontent">
        <table className="striped centered">
        <thead>
          <tr>
              <th style={{border: '1px solid'}}><h6><b>Hour /<br/>Day Order</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>08:00 - 08:50<br/>1</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>08:50 - 09:40<br/>2</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>09:45 - 10:35<br/>3</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>10:40 - 11:30<br/>4</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>11:35 - 12:25<br/>5</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>12:30 - 01:20<br/>6</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>01:25 - 02:15<br/>7</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>02:20 - 03:10<br/>8</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>03:15 - 04:05<br/>9</b></h6></th>
              <th style={{border: '1px solid'}}><h6><b>04:05 - 04:55<br/>10</b></h6></th>
          </tr>
		  <br/>

        </thead>

        <tbody>
          <tr className="card hoverable">
          <td style={{borderRight: '1px solid'}}><b>Day Order 1</b></td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s1"  type="text" className="validate"/>
					<label htmlFor="d1s1">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s2" type="text" className="validate"/>
					<label htmlFor="d1s2">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s3" type="text" className="validate"/>
					<label htmlFor="d1s3">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s4" type="text" className="validate"/>
					<label htmlFor="d1s4">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s5" type="text" className="validate"/>
					<label htmlFor="d1s5">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s6" type="text" className="validate"/>
					<label htmlFor="d1s6">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s7" type="text" className="validate"/>
					<label htmlFor="d1s7">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s8" type="text" className="validate"/>
					<label htmlFor="d1s8">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s9" type="text" className="validate"/>
					<label htmlFor="d1s9">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d1s10" type="text" className="validate"/>
					<label htmlFor="d1s10">Enter Details</label>
				</div>
            </td>
            
          </tr>
		  <br/>

          <tr className="card hoverable">
          <td style={{borderRight: '1px solid'}}><b>Day Order 2</b></td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s1" type="text" className="validate"/>
					<label htmlFor="d2s1">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s2" type="text" className="validate"/>
					<label htmlFor="d2s2">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s3" type="text" className="validate"/>
					<label htmlFor="d2s3">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s4" type="text" className="validate"/>
					<label htmlFor="d2s4">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s5" type="text" className="validate"/>
					<label htmlFor="d2s5">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s6" type="text" className="validate"/>
					<label htmlFor="d2s6">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s7" type="text" className="validate"/>
					<label htmlFor="d2s7">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s8" type="text" className="validate"/>
					<label htmlFor="d2s8">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s9" type="text" className="validate"/>
					<label htmlFor="d2s9">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d2s10" type="text" className="validate"/>
					<label htmlFor="d2s10">Enter Details</label>
				</div>
            </td>
            
          </tr>
		  <br/>

          <tr className="card hoverable">
          <td style={{borderRight: '1px solid'}}><b>Day Order 3</b></td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s1" type="text" className="validate"/>
					<label htmlFor="d3s1">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s2" type="text" className="validate"/>
					<label htmlFor="d3s2">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s3" type="text" className="validate"/>
					<label htmlFor="d3s3">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s4" type="text" className="validate"/>
					<label htmlFor="d3s4">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s5" type="text" className="validate"/>
					<label htmlFor="d3s5">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s6" type="text" className="validate"/>
					<label htmlFor="d3s6">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s7" type="text" className="validate"/>
					<label htmlFor="d3s7">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s8" type="text" className="validate"/>
					<label htmlFor="d3s8">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s9" type="text" className="validate"/>
					<label htmlFor="d3s9">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d3s10" type="text" className="validate"/>
					<label htmlFor="d3s10">Enter Details</label>
				</div>
            </td>
            
          </tr>
		  <br/>

          <tr className="card hoverable">
          <td style={{borderRight: '1px solid'}}><b>Day Order 4</b></td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s1" type="text" className="validate"/>
					<label htmlFor="d4s1">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s2" type="text" className="validate"/>
					<label htmlFor="d4s2">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s3" type="text" className="validate"/>
					<label htmlFor="d4s3">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s4" type="text" className="validate"/>
					<label htmlFor="d4s4">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s5" type="text" className="validate"/>
					<label htmlFor="d4s5">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s6" type="text" className="validate"/>
					<label htmlFor="d4s6">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s7" type="text" className="validate"/>
					<label htmlFor="d4s7">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s8" type="text" className="validate"/>
					<label htmlFor="d4s8">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s9" type="text" className="validate"/>
					<label htmlFor="d4s9">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d4s10" type="text" className="validate"/>
					<label htmlFor="d4s10">Enter Details</label>
				</div>
            </td>
            
          </tr>
		  <br/>

          <tr className="card hoverable">
          <td style={{borderRight: '1px solid'}}><b>Day Order 5</b></td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s1" type="text" className="validate"/>
					<label htmlFor="d5s1">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s2" type="text" className="validate"/>
					<label htmlFor="d5s2">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s3" type="text" className="validate"/>
					<label htmlFor="d5s3">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s4" type="text" className="validate"/>
					<label htmlFor="d5s4">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s5" type="text" className="validate"/>
					<label htmlFor="d5s5">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s6" type="text" className="validate"/>
					<label htmlFor="d5s6">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s7" type="text" className="validate"/>
					<label htmlFor="d5s7">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s8" type="text" className="validate"/>
					<label htmlFor="d5s8">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s9" type="text" className="validate"/>
					<label htmlFor="d5s9">Enter Details</label>
				</div>
            </td>
            <td>
                <div className="input-field">
					<select>
					<option value="" disabled selected>Slots</option>
					<option value="acad">Academics</option>
					<option value="free">Free</option>
					<option value="res">Research</option>
					</select>
				</div>
                <div className="input-field">
					<input id="d5s10" type="text" className="validate"/>
					<label htmlFor="d5s10">Enter Details</label>
				</div>
            </td>
            
          </tr>
		  <br/>
        </tbody>
        
      </table>
        
        <Link to="#" className="waves-effect waves-light btn blue-grey darken-2 col s2 offset-s10 sup">Submit</Link>
	</div>
        

		
	)
}
}

export default Timetable
