import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
// import '../styles.css'

import InsertUser from './insertuser'
import UpdateUser from './updateuser'
import SuperUser from './superuser'
import DeleteUser from './deleteuser'
import ModifyUser from './modifyuser'
import SuspendUser from './suspenduser'


export default class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: false,
            select: ''
        }
    }
    handleCheck = (e) => {
        this.setState({
            isChecked: true,
            select: e.target.value
        })

    }
    render(){
        if(this.props.selected === 'insert')
        {
            return(
              <React.Fragment>
                    <div className="card col l2 s12" style={{marginRight: '1px solid'}}>
                        <p>
                        <label>
                        <input type='radio' id='radio-7' name='myRadio' value='insertadmin' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Department Admin</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-8' name='myRadio' value='insert_in_nav' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Into NavBar</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="card col l10 s12">
                            <InsertUser select={this.state.select} />
                    </div>
                    </React.Fragment>
            );
        }
        else if(this.props.selected === 'update')
        {
            return(
                <div className="row card">
                <div className="col s12">
                    <div className="col s3 brd-r">
                        <p>
                        <label>
                        <input type='radio' id='radio-apwd' name='myRadio' value='adminpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Admin Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-fpsw' name='myRadio' value='facpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Faculty Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-spsw' name='myRadio' value='studpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Student Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-spsw' name='myRadio' value='all_user_dayorder_entry' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Add DayOrder</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col s9">
                            <UpdateUser select={this.state.select} />
                    </div>


                </div>
                </div>

            );
        }
        else if(this.props.selected === 'super')
        {
            return(

                <div className="row">
                <div className="col l12">
                    <div className="col l2 card">
                        <p>
                        <label>
                        <input type='radio' id='radio-reg' name='myRadio' value='register' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Handle Registration</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-frep' name='myRadio' value='facprofile' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Faculty Profile</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-srep' name='myRadio' value='studprofile' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Student Profile</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-frep' name='myRadio' value='facreport' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Faculty Report</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-srep' name='myRadio' value='studreport' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Student Report</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-req' name='myRadio' value='approve_single_faculty_req' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Approve Request</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col l10">
                            <SuperUser select={this.state.select} />
                    </div>
                </div>
                </div>



            );
        }
        else if(this.props.selected === 'delete')
        {
            return(
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-20' name='myRadio' value='deleteadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-21' name='myRadio' value='deletefac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-22' name='myRadio' value='deletestu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Student</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <DeleteUser select={this.state.select} />
                </div>
                </div>
                </div>

            );
        }
        else if(this.props.selected === 'modify')
            {
            return(
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-18' name='myRadio' value='start-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Start Semester</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-19' name='myRadio' value='end-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>End Semester</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-13' name='myRadio' value='modifytoday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Modify Today's Day Order</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-17' name='myRadio' value='holiday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Cancel Today's DayOrder</b><br />(Update here only if today is a holiday)</span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-18' name='myRadio' value='holiday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Declartion for UpComing Holiday</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <ModifyUser select={this.state.select} />
                </div>
                </div>
                </div>
            )
        }
        else if(this.props.selected === 'suspend')
        {
            return(
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-14' name='myRadio' value='suspendadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-15' name='myRadio' value='suspendfac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-16' name='myRadio' value='suspendstu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Student</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-17' name='myRadio' value='suspend_request' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspended Request</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <SuspendUser select={this.state.select} />
                </div>
                </div>
                </div>

            )
        }

        else{
            return(
                <div></div>
            )
        }
    }
}
