import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
import '../style.css'
import Nav from '../dynnav'
import M from 'materialize-css'
import NavAdd from './NavControl/NavHandle'

import Display from './Operations/display'

export default class Admin extends Component{
constructor(){
    super();
    this.state ={
        isChecked: false,
        selected: '',
        home:'/admin_panel',
        logout:'/user/logout',
        get:'/user/',
        nav_route: '/user/fetchnav',
    }
}
handleChecked =(e)=>{
    this.setState({
        isChecked: !this.state.isChecked,
        selected: e.target.value
    });
}
render()
{
    return(
        <React.Fragment>
<Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
            <div className="row">

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-1' name='myRadio' value='insert' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Insert</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-2' name='myRadio' value='update' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Update</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-3' name='myRadio' value='super' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Super</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-4' name='myRadio' value='delete' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Delete</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-5' name='myRadio' value='modify' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Modify</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-6' name='myRadio' value='suspend' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Suspension</b></span>
            </label>
            </p>
            </div>
            </div>

            </div>

            <div className="row">
                <div className="col l12">
            <Display selected={this.state.selected}/>
            </div>
            </div>
        </React.Fragment>
    );
}
}