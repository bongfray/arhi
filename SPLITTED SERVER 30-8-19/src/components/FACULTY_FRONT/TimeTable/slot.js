import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Allot'
import Free from './Free'
import Cancel from './Cancel'


{/*---------------------------------------------------------Code for regular classes time table------------------------------------ */}
export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',

    };
    this.componentDidMount= this.componentDidMount.bind(this);
  }
  componentDidMount(){
    M.AutoInit();
  }


  handle1 = (e, index) => {
    // alert(e.target.value);
    // alert(e.target.name);
    const key = e.target.name;
    const value = e.target.value;
    this.setState({ rit: value, index ,day_order: key, index});

  };


  render() {
    // alert(this.state.day_order);
const {datt} = this.props.datt;
    return (
            <div className="root-of-time" >


             {this.props.datt.map((content,index) => {
               return(
              <div className="row" key={index}>
                   <div className="col l12 card hoverable ">
                   <div className="card-title">
                   <span className="">
                   <div className="center day-order-day">{content.head}</div>
                   </span>
                   </div><br /><br />
                     <div>
                       <select name={content.con} onChange={e => this.handle1(e, index)}>
                           <option defaultValue="" disabled selected>Choose</option>
                           <option value="allot">Alloted Class</option>
                           <option value="free">Free Slot</option>
                           <option value="slot_cancel">Slot Cancelled</option>
                       </select>
                     </div>
                     <span className="drop">

                       <TableDisplay
                       selectValue={
                         index == this.state.index && this.state.rit} day_order={this.state.day_order} usern={this.props.usern}
                       />
                     </span>
                   </div>


                   </div>
             );
             })}


</div>
          );
  }
}



{/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */}


class TableDisplay extends Component {
  constructor() {
    super()
    this.state = {
      allot:'',
        count: 0,
        selected:'',
        problem_conduct:'',


        saved_dayorder: '',
        day_recent:'',

    }

    }




componentDidMount(){

}




  render() {


                if (this.props.selectValue === "allot") {
                  return(
                    <div className="">
                    <Allot usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                    </div>
                  );

                } else if (this.props.selectValue === "free") {
                  return(
                    <Free usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                  );

                }
                else if (this.props.selectValue === "slot_cancel") {
                 return(
                   <Cancel usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                 );

               }
                else{
                  return(
                    <div className="inter-drop center">
                      Choose from above DropDown
                    </div>
                  );
                }
  }
}
