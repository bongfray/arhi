import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/del_five_year_plan',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetch_five_year_plan',{
    action: this.props.action
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {
    const { products} = this.state;
      return(
        <div>
        <div className="row">
          <div className="col l1 center">No.</div>
          <div className="col l8 center">Plans</div>
          <div className="col l2 right">Action</div>
        </div>
        <div>
        {this.state.products.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1 center">{index+1}</div>
          <div className="col l8 center" onClick={() => this.props.editProduct(content.serial,this.props.action)}>{content[this.props.action]}</div>
          <div className="col l2 right">
          <i className="material-icons" onClick={() => this.deleteProduct(content.serial,this.props.action)}>delete</i>
          </div>
          </div>
        ))}
        </div>
        </div>
      )

  }
}
