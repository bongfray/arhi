
import React from 'react';
class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      username: this.props.username,
      serial:'',
      action:'',
      YearIAdministrative:'',
      YearIAcademic:'',
      YearIResearch:'',
      YearIIAdministrative:'',
      YearIIAcademic:'',
      YearIIResearch:'',
      YearIIIAdministrative:'',
      YearIIIAcademic:'',
      YearIIIResearch:'',
      YearIVAdministrative:'',
      YearIVAcademic:'',
      YearIVResearch:'',
      YearVAdministrative:'',
      YearVAcademic:'',
      YearVResearch:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })

  }


componentDidMount(){
  this.setState({
    action:this.props.data.index,
  })
}



  handleSubmit(event,index) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }

  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS</h5>
}
    return(
      <div>
      <div>
      <div className="row">
        <div className="col s12 l12" >
          <form onSubmit={this.handleSubmit}>
              <input
                className=""
                type="text"
                placeholder=" "
                min="10"
                max="60"
                name={this.props.data.index}
                value={this.state[this.props.data.index]}
                onChange={e => this.handleD(e,this.props.data.index)}
                placeholder="Enter Here..."
              />
            <div>
              <button className="btn right blue-grey darken-2 sup " type="submit">UPLOAD</button>
            </div>
          </form>
        </div>
      </div>
      </div>

      </div>
    )
  }
}

export default AddProduct;
