import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'
import AddProduct from './add'
import ProductList from './prod'
class FiveYear extends Component{
    constructor(){
        super();
        this.state={
          isAddProduct: false,
          response: {},
          action:'',
          product: {},
          isEditProduct: false,
          five:[{year:'Year - I',parts:[{name:'Administrative',index:'YearIAdministrative'},{name:'Academic',index:'YearIAcademic'},{name:'Research',index:'YearIResearch'}]},
          {year:'Year - II',parts:[{name:'Administrative',index:'YearIIAdministrative'},{name:'Academic',index:'YearIIAcademic'},{name:'Research',index:'YearIIResearch'}]},
          {year: 'Year - III',parts:[{name:'Administrative',index:'YearIIIAdministrative'},{name:'Academic',index:'YearIIIAcademic'},{name:'Research',index:'YearIIIResearch'}]},
          {year:'Year - IV',parts:[{name:'Administrative',index:'YearIVAdministrative'},{name:'Academic',index:'YearIVAcademic'},{name:'Research',index:'YearIVResearch'}]},
          {year:'Year - V',parts:[{name:'Administrative',index:'YearVAdministrative'},{name:'Academic',index:'YearVAcademic'},{name:'Research',index:'YearVResearch'}]}]
        }
    }
    componentDidMount(){
        M.AutoInit();
    }

    onCreate = (e,index) => {
      this.setState({ isAddProduct: true ,product: {}});
    }
    onFormSubmit =(data) => {
      let apiUrl;
      if(this.state.isEditProduct){
        apiUrl = '/user/edit_five_year_plan';
      } else {
        apiUrl = '/user/add_five_year_plan';
      }
      axios.post(apiUrl,data)
          .then(response => {
            this.setState({
              response: response.data,
              isAddProduct: false,
              isEditProduct: false
            })
          })
    }

    editProduct = (productId,index)=> {
      axios.post('/user/fetch_five_year_existing_data',{
        id: productId,
      })
          .then(response => {
            this.setState({
              product: response.data,
              isEditProduct: true,
              isAddProduct: true,
            });
          })

   }


    render(){
      let productForm;
        return(
            <React.Fragment>
                <div className="row">
                {this.state.five.map((content,index)=>(
                  <div key={index} className="col l3 card">
                      <p className="center">{content.year}<br/>(2017-2018)</p>
                      {content.parts.map((items,no)=>(
                        <ul className="collapsible" key={no}>
                            <li>
                                <div className="center collapsible-header #757575 grey darken-1">{items.name}</div>
                                <div className="collapsible-body">
                                    <div className="row">
                                    {!this.state.isAddProduct && <ProductList data={items}  action={items.index} editProduct={this.editProduct}/>}
                                    {!this.state.isAddProduct &&
                                       <button className="btn right blue-grey darken-2 sup subm" onClick={(e) => this.onCreate(e,items.index)}>Add Data</button>
                                     }

                                     {((this.state.isAddProduct) || (this.state.isEditProduct)) &&
                                     <AddProduct data={items}  action={items.index} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
                                     }
                                    </div>
                                </div>
                            </li>
                      </ul>
                      ))}
                  </div>
                ))}
                </div>
            </React.Fragment>
        )
    }
}

export default FiveYear;
