import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import '../style.css'

class FiveYear extends Component{
    constructor(){
        super();
    }
    componentDidMount(){
        M.AutoInit();
    }
    render(){
        return(
            <React.Fragment>
                <div className="row">
                        <div className="col s1"></div>
                        <div className="col s2 card">
                            <p className="center">Year - I<br/>(2017-2018)</p>
                                <ul className="collapsible">
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Administrative</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Academics</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Research</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                        <div className="col s2 card">
                            <p className="center">Year - II<br/>(2018-2019)</p>
                            <ul className="collapsible">
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Administrative</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Academics</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Research</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                        <div className="col s2 card">
                            <p className="center">Year - III<br/>(2019-2020)</p>
                            <ul className="collapsible">
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Administrative</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Academics</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Research</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                        <div className="col s2 card">
                            <p className="center">Year - IV<br/>(2020-2021)</p>
                            <ul className="collapsible">
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Administrative</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Academics</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Research</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                        <div className="col s2 card">
                            <p className="center">Year - V<br/>(2021-2022)</p>
                            <ul className="collapsible">
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Administrative</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Academics</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="center collapsible-header #757575 grey darken-1">Research</div>
                                        <div className="collapsible-body">
                                            <div className="row">
                                                <input type="text"/>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                </div>
            </React.Fragment>
        )
    }
}

export default FiveYear;