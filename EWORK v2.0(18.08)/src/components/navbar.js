import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
import Slogo from './Slogo.png'
import './style.css'
import M from 'materialize-css'

class Navbar extends Component {
  componentDidMount() {
    M.AutoInit();
    let elems=document.querySelectorAll('.dropdown-button');
    M.Dropdown.init(elems,{inDuration : 300 ,outDuration :225, hover: true, belowOrigin: true});
    M.Sidenav.init(elems, {
  inDuration: 350,
  outDuration: 350,
  edge: 'left'
});

}
    constructor() {
        super()
        this.logout = this.logout.bind(this)
        this.componentDidMount =this.componentDidMount.bind(this)
    }

    logout(event) {
        event.preventDefault()
        //console.log('logging out')
        axios.post('/user/logout').then(response => {
          //console.log(response.data)
          if (response.status === 200) {
            this.props.updateUser({
              loggedIn: false,
              username: null
            })
            window.location.assign("/");
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }
    render() {
        const loggedIn = this.props.loggedIn;
        const logins =this.props.logins;
        //console.log('navbar render, props: ')
        console.log(this.props);

        return (

 <div>

            {loggedIn ? (

                      <nav>
                      <ul id="slide-out" className="sidenav">
                        <li><Link className="waves-effect" to="/">Home</Link></li>
                        <li><Link className="waves-effect" to="/newd">Profile</Link></li>
                        <li><Link className="waves-effect" to="/dash">DashBoard</Link></li>
                        <li><Link className="waves-effect" to="/partA">Sections</Link></li>
                        <li><Link className="waves-effect" to="/time_new">TimeTable</Link></li>
                        <li><Link to="#" className="" onClick={this.logout}>Logout</Link></li>
                     </ul>
                          <div className="nav-wrapper blue-grey darken-2">
                              <a href="#" data-target="slide-out" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                          <div className="brand-logo col s2"><a href="http://srmuniv.ac.in"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a></div>
                          <span className="hide-on-med-and-down center nav-cen srm-care"><a href="http://care.srmuniv.ac.in">SRM CARE</a></span>
                          <ul className="right">
                          <li title="Home">
                            <Link to="/"><i className=" hide-on-med-and-down material-icons">home</i></Link>
                          </li>
                          <li title="Log Out">
                               <Link to="#" className="right" onClick={this.logout}>
                               <i className="material-icons right">input</i>
                               </Link>
                          </li>
                          <li title="notification">
                               <Link to="#" className="right">
                               <i className="material-icons right">notifications</i>
                               </Link>
                          </li>
                          <li title="menu">
                             <Dropdown trigger={<i className="hide-on-med-and-down material-icons men">more_vert</i>}>
                              <Link to="/newd">
                               <span className="drop_down">Profile</span>
                              </Link>
                             <Link to='/Dash'>
                               <span className="drop_down">DashBoard</span>
                             </Link>
                             <Link to="/partA">
                               <span className="drop_down">Sections</span>
                             </Link>
                             <Link to="/time_new">
                               <span className="drop_down">TimeTable</span>
                             </Link>
                            </Dropdown>
                          </li>
                          <li></li>
                          </ul>
                          </div>
                      </nav>

            ) : (
<nav>
              <div className="nav-wrapper blue-grey darken-2">
                  <div className="brand-logo col s2"><a href="http://srmuniv.ac.in"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></a></div>
                  <div className="col s10 center con nav-cen nav-wrapper"><a href="http://care.srmuniv.ac.in">SRM Centre for Applied Research in Education</a></div>
              </div>
  </nav>
                )}


</div>
        );

    }
}

export default Navbar
