import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'

import Particles from 'react-particles-js';
import newId from './id'
import Par from "./tab";
import '../style.css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'

class PartA extends Component {
  constructor(){
    super()
    this.componentDidMount();
  }
  state = {
    width: "600px",
    height: "30px"
  };

  componentDidMount() {
    M.AutoInit();
    this.id = newId();
    let collapsible = document.querySelectorAll(".collapsible");
    M.Collapsible.init(collapsible, {});
  }

  render() {
    var data1 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Course Code/Course Info",
          inputfield: true,
          length: 20,
          name: "code",
          placeholder: "",
          content: "text",
          grid: "six"
        },
        {
          header: "No. of Students",
          inputfield: true,
          length: 10,
          name: "totalstud",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data2 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Course Code/Course Info",
          inputfield: true,
          length: 20,
          name: "code1",
          placeholder: "",
          content: "text",
          grid: "six"
        },
        {
          header: "No. of Students",
          inputfield: true,
          length: 10,
          name: "totalstud1",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data3 = {
      fielddata: [
        {
          id: "",
          header: "No.",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          id: "",
          header: "Title of Completed Thesis/Project",
          inputfield: true,
          length: 10,
          name: "toctp",
          placeholder: "",
          content: "text",
          grid: "four"
        },
        {
          id: "",
          header: "Credits",
          inputfield: true,
          length: 10,
          name: "credits",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          id: "",
          header: "Date of Completion",
          inputfield: true,
          length: 7,
          name: "dateoc",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data4 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "one"
        },
        {
          header: "Date",
          inputfield: true,
          length: 10,
          name: "trdate",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Workshop/Seminar",
          inputfield: true,
          length: 10,
          name: "wksem",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Location",
          inputfield: true,
          length: 7,
          name: "loca",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Duration",
          inputfield: true,
          length: 10,
          name: "duration",
          placeholder: "",
          content: "text",
          grid: "one"
        },
        {
          header: "Your Role",
          inputfield: true,
          length: 7,
          name: "role",
          content: "text",
          grid: "two",
          placeholder: ""
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };
    var data5 = {
      fielddata: [
        {
          header: "No. ",
          inputfield: true,
          length: 10,
          name: "date_created",
          placeholder: "",
          content: "text",
          grid: "one"
        },
        {
          header: "Date",
          inputfield: true,
          length: 10,
          name: "pdate",
          placeholder: "",
          content: "text",
          grid: "two"
        },
        {
          header: "Paper Title",
          inputfield: true,
          length: 10,
          name: "ptitle",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Name of Publication",
          inputfield: true,
          length: 7,
          name: "nopub",
          content: "text",
          grid: "two",
          placeholder: ""
        },
        {
          header: "Volume No",
          inputfield: true,
          length: 7,
          name: "volno",
          content: "text",
          grid: "one",
          placeholder: ""
        },
        {
          header: "Impact Factor",
          inputfield: true,
          length: 10,
          name: "impfac",
          placeholder: "",
          content: "text",
          grid: "two"
        }
      ],
      instances: [
        {
          date_created: "",
          id: ""
        }
      ]
    };

    const loggedIn = this.props.loggedIn;
    //console.log('navbar render, props: ')
    //console.log(this.props);

    if (this.props.loggedIn) {
      return (
        <div className="grid-of-parts">

                {/*This is a link to Part A  */}
          <div className="row">

            <Link to="/partB">
              <a class="btn-floating btn-large waves-effect waves-light #e91e63 pink move right rbtn">
                <i class="large material-icons icn">chevron_right</i>
              </a>
            </Link>
            <span className="go-to-alt right rsbtn">
              <b className="mv">Submit & Go to Part B</b>
            </span>
       </div>
            {/* Here start Sections codes of collapsible */}


          <Collapsible popout>
            <CollapsibleItem
              header={
                <div className="row">
                  <div className="col l6 s12">
                    <h5 className="collaphead">Teaching History</h5>
                  </div>
                  <div className="col m6 l6 hide-on-med-and-down">
                    <Particles
                      {...this.state}
                      params={{
                        particles: {
                          number: {
                            value: 20
                          },
                          size: {
                            value: 2
                          }
                        },
                        interactivity: {
                          events: {
                            onhover: {
                              enable: true,
                              mode: "grabss"
                            }
                          }
                        }
                      }}
                    />
                  </div>
                </div>
              }
              expanded
            >
              <div className="row">
                <div className="col l1 s1 m1">
                  <h5 className="right td ft">A .</h5>
                </div>
                <div className="col l11 td">
                <div className="row">
                <div className="col l8 s12 m12">
                <h5 className="ft">Please provide your current teching info and also semester of : </h5></div>
                <div className="col l2 s6 m6 left">
                <select>
                  <option value="" disabled selected>
                    Semester
                  </option>
                  <option value="odd">Odd Semester</option>
                  <option value="even">Even Semester</option>
                </select>
                </div>
              </div>
                </div>
              </div>

              <br />
              <br />
              <div className="row">
                <Par data={data1} />
              </div>
            </CollapsibleItem>
          </Collapsible>

          {/*--------------------------------------------------------------------------------------------End of Teaching */}

          <Collapsible popout>
            <CollapsibleItem
              header={
                <div className="row">
                  <div className="col l6 s12">
                    <h5 className="collaphead">Thesis & Project Supervision</h5>
                  </div>
                  <div className="col m6 l6 hide-on-med-and-down">
                    <Particles
                      {...this.state}
                      params={{
                        particles: {
                          number: {
                            value: 20
                          },
                          size: {
                            value: 2
                          }
                        },
                        interactivity: {
                          events: {
                            onhover: {
                              enable: true,
                              mode: "grabss"
                            }
                          }
                        }
                      }}
                    />
                  </div>
                </div>
              }
              
            >
            <div className="row">
              <div className="col s1">
                <h5 className="right td ft">A .</h5>
              </div>
              <div className="col s11 td">
                <h5 className="ft">
                  Please provide information about graduate thesis/project
                  supervised
                </h5>
              </div>
            </div>
            <br />
            <br />
            <div className="row">
              <Par data={data3} />
            </div>
            </CollapsibleItem>
          </Collapsible>

          {/*----------------------------------------------------------------------------------------------------Thesis / Project*/}

          <Collapsible popout>
            <CollapsibleItem
              header={
                <div className="row">
                  <div className="col l6 m12 s12">
                    <h5 className="collaphead">
                      Training/Workshop Attended
                    </h5>
                  </div>
                  <div className="col s12 m12 l6 hide-on-med-and-down">
                    <Particles
                      {...this.state}
                      params={{
                        particles: {
                          number: {
                            value: 20
                          },
                          size: {
                            value: 2
                          }
                        },
                        interactivity: {
                          events: {
                            onhover: {
                              enable: true,
                              mode: "grabss"
                            }
                          }
                        }
                      }}
                    />
                  </div>
                </div>
              }
            >
              <div className="row">
                <div className="col s1">
                  <h5 className="right td ft">A .</h5>
                </div>
                <div className="col s11 td">
                  <h5 className="ft">
                    Please Provide the Details about the
                    training/workshop/seminar attended/participated
                  </h5>
                </div>
              </div>
              <br />
              <br />
              <div className="row">
                <Par data={data4} />
              </div>
            </CollapsibleItem>
          </Collapsible>

          {/*----------------------------------------------------------------------------------------------------------Training/Workshop/Seminar*/}

          <Collapsible popout>
            <CollapsibleItem
              header={
                <div className="row">
                  <div className="col l6 m12 s12">
                    <h5 className="collaphead">
                    Paper Published/ Accepted
                    </h5>
                  </div>
                  <div className="col l6 hide-on-med-and-down">
                    <Particles
                      {...this.state}
                      params={{
                        particles: {
                          number: {
                            value: 20
                          },
                          size: {
                            value: 2
                          }
                        },
                        interactivity: {
                          events: {
                            onhover: {
                              enable: true,
                              mode: "grabss"
                            }
                          }
                        }
                      }}
                    />
                  </div>
                </div>
              }
            >
              <div className="row">
                <div className="col s1">
                  <h5 className="right td ft">A .</h5>
                </div>
                <div className="col s11 td">
                  <h5 className="ft">
                    Please Provide information about published and accepted paper in journal/conference/book chapter/newspaper article
                  </h5>
                </div>
              </div>
              <br />
              <br />
              <div className="row">
                <Par data={data5} />
              </div>
            </CollapsibleItem>
          </Collapsible>

          {/*-------------------------------------------------------------------------------------------------Paper, Journal etc*/}
        </div>
      );
    }
   else{
      return (
        <div className="error-div">
          <div className="row">
            <div className="col s4">
              <i className="large material-icons right error-icon">warning</i>
            </div>
            <div className="col s6">
              <h3 className="left error-msg">Sorry !! You are not logged in</h3>
            </div>
            <div className="col s2" />
          </div>
          <div className="row">
            <h5 className="reminder">Please Log In to Access this Site</h5>
          </div>
        </div>
      );
    }
  }
}

export default PartA;
