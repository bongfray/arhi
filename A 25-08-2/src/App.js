import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import Ar from './components/TimeTable/Al2'
import Signup from './components/sign-up'
import LoginForm from './components/login-form'
import Navbar from './components/navbar'
import PartB from './components/Sections/par'
import PartA from './components/Sections/PartA'
import Home from './components/Home'
import Dash from './components/DashBoard/Dashboard'
import Profile from './components/Profile/profile'
import Con from './components/online-offline/Connection'
import DashProf from './components/Profile/DashProf'
import Home2 from './components/Home2'
import Scroll from './components/Top/scroll_top'
import TimeTab2 from './components/TimeTable/time2'
import TimeTabYes from './components/TimeTable/yesterday_slot'
import DashProf2 from './components/Profile/NewDash'
import Dash3 from './components/Profile/exdash'
import TimeTable from './components/Schedule/Timetable'
import Color from './components/TimeTable/colordiv'
import ResetPass from './components/Reset/reset_password'
import Slot2 from './components/TimeTable/slot2'
import NotFound from './components/default'

import Master from './components/master/mater'


import AdminAuth from './components/admin/auth'
import Panel from './components/admin/admin_operations'




import Daa from './components/DashBoard/DAA'
import './style.css'
require('disable-react-devtools');

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      day_order: '',
      time: '',

    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }


  updateUser (userObject) {
    this.setState(userObject)
  }



  // getUser() {
  //   axios.get('/user/').then(response => {
  //     console.log('Get user response: ')
  //     console.log(response.data)
  //     if (response.data.user) {
  //       console.log('Get User: There is a user saved in the server session: ')
  //       console.log(response.data.user);
  //       this.setState({
  //         loggedIn: true,
  //         username: response.data.user.username,
  //
  //       })
  //     } else {
  //       console.log('Get user: no user');
  //       this.setState({
  //         loggedIn: false,
  //         username: null,
  //
  //       })
  //     }
  //   })
  // }


  componentDidMount() {

  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }


  render() {
    return (
       <div className="App">

  {/*      <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn} logins={this.state.logins}/>*/}


        {/*<Con />*/} {/*This is for checking the connectivity */}

        <Route path="/daa" component={Daa} />
        <Route exact path="/" render={() =>
          <Home
         loggedIn={this.state.loggedIn}
         />}
       />


        <Route path="/login" render={() =>
          <LoginForm
         updateUser={this.updateUser} loggedIn={this.state.loggedIn}
         />}
       />

       <Route path="/signup" component={Signup} />
<Route path="/ai" component={Ar}/>


      <Route path="/partB" render={() =>
        <PartB
       updateUser={this.updateUser} loggedIn={this.state.loggedIn}
       />} />
       <Route path="/partA" render={() =>
         <PartA loggedIn={this.state.loggedIn}
        />} />


       <Route path="/profile1" render={() =>
         <Profile username={this.state.username} loggedIn={this.state.loggedIn}
        />} />

       <Route path="/dashprof" component={DashProf} />
       <Route path="/dash2" render={() =>
         <DashProf2 loggedIn={this.state.loggedIn}
        />} />
       <Route path="/home" component={Home2} />
       <Route path="/Dash" render={() =>
         <Dash
        updateUser={this.updateUser} loggedIn={this.state.loggedIn}
        />} />

         <Route path="/newd" component={Dash3} />
         <Route path="/slot" component={Slot2} />
         <Route path="/blue_print" component={TimeTable} />
         <Route path="/reset_password/:token" component={ResetPass} />


          <Route path="/time_new" render={() =>
            <TimeTab2 loggedIn={this.state.loggedIn} username={this.state.username}
           />} />
           <Route path="/time_yes" render={() =>
             <TimeTabYes loggedIn={this.state.loggedIn} username={this.state.username}
            />} />

            <Route path="/no" component={NotFound} />

            <Route path="/admin" component={AdminAuth} />
            <Route path="/admin_panel" component={Panel} />

            <Route path="/master" render={() =>
              <Master loggedIn={this.state.loggedIn} username={this.state.username}
             />} />

         {/*<Scroll/>*/}
      </div>
    );
  }
}

export default App;
