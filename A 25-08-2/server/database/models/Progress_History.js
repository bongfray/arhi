const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const progressSchema = new Schema({

username: { type: String, unique: false, required: false },


complete_adminispercentage: { type: Number, unique: false, required: false },
complete_researchpercentage: { type: Number, unique: false, required: false },
complete_academicpercentage: { type: Number, unique: false, required: false },



ad_complete_role_res: { type: Number, unique: false, required: false },
ad_complete_clerical: { type: Number, unique: false, required: false },
ad_complete_planning: { type: Number, unique: false, required: false },
ad_complete_account_finace: { type: Number, unique: false, required: false },
ad_complete_dataentry_analysis: { type: Number, unique: false, required: false },


academic_complete_curricular: { type: Number, unique: false, required: false },
academic_complete_cocurricular: { type: Number, unique: false, required: false },
academic_complete_extracurricular: { type: Number, unique: false, required: false },
academic_complete_evaluation_placementwork: { type: Number, unique: false, required: false },


research_complete_publication: { type: Number, unique: false, required: false },
research_complete_ipr_patents: { type: Number, unique: false, required: false },
research_complete_funded_sponsored_projectes: { type: Number, unique: false, required: false },
research_complete_tech_dev_consultancy: { type: Number, unique: false, required: false },
research_complete_product_development: { type: Number, unique: false, required: false },
research_complete_research_center_establish: { type: Number, unique: false, required: false },
research_complete_research_guidnce: { type: Number, unique: false, required: false },
})


const Progress_History = mongoose.model('login_db', progressSchema)
module.exports = Progress_History
