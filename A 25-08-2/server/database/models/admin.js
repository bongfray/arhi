const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const adminSchema = new Schema({

username: { type: String, unique: false, required: false },
password: { type: String, unique: false, required: false },
status: { type: Number, unique: false, required: false },
resetPasswordToken: { type: String, unique: false, required: false },
resetPasswordExpires: { type: String, unique: false, required: false },
})




const Admin = mongoose.model('Admin', adminSchema)
module.exports = Admin
