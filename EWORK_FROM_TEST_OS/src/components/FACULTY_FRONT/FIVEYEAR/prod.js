import React from 'react';
import axios from 'axios';
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username:'',
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/del_five_year_plan',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetch_five_year_plan',{
    action: this.props.action
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {
    const { products} = this.state;
      return(
        <div>
        <div className="row">
          <div className="col l1 s1 m1 xl1 center"><b>No.</b></div>
          <div className="col l8 s8 m8 xl8 center"><b>Plans</b></div>
          <div className="col l3 s3 m3 right"><b>Action</b></div>
        </div>
        <div>
        {this.state.products.map((content,index)=>(
          <div className="row" key={index}>
          <div className="col l1 s1 m1 center">{index+1}</div>
          <div className="col l8 s8 m8 xl8 center go">{content[this.props.action]}</div>
          <div className="col l3 s3 m3 right">
          <i className="material-icons go" onClick={() => this.props.editProduct(content.serial,this.props.action)}>edit</i>
          &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(content.serial,this.props.action)}>delete</i>
          </div>
          </div>
        ))}
        </div>
        </div>
      )

  }
}
