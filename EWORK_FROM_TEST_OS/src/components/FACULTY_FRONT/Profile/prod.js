import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){

      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/profile1del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetchprofile1',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { error, products} = this.state;
      return(
        <div className="row">
        <div className="col l1 left proftitle">{this.props.title}</div>
            <div className="col l11">
              {products.map((product,index) => (
                <div className="row">
                <div className="col l10">
                <div className="row">
                <div className="col l1 center">{index+1}</div>
                <div className="col l11">
                 <div className="row" key={product.serial}>
                  {this.props.data.fielddata.map((content,index)=>(
                    <div className=" col l3 center" key={index}>{product[content.name]}</div>
                  ))}
                  </div>
                  </div>
                  </div>
                  </div>
                  <div className="col l2">
                    <div className="center"><button className="btn-small  green" onClick={() => this.props.editProduct(product.serial,this.props.action)}>Edit</button>
                    &nbsp;<button className="btn-small  red" onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </div>
                  </div>
                  </div>

              ))}
            </div>
        </div>

      )

  }
}
