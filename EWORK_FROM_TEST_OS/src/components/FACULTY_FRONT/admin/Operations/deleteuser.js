import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'


export default class DeleteUser extends Component{
    constructor(){
        super();
        this.state={
            username: ''
        }
    }
    handleDelete = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handleSubmit(event){

    }
    render(){
      let deleteoperation;
        if(this.props.select === 'deleteadmin'){
          deleteoperation =	<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Admin</Link>
        }
        else if(this.props.select === 'deletefac'){
          deleteoperation = <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Faculty</Link>
        }
        else if(this.props.select === 'deletestu'){
           deleteoperation =   <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Student</Link>
        }
        else{
            return(
                <div></div>
            )
        }
        return(
            <div className="center">
            <div className="input-field col s12">
              <input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleDelete} required />
              <label htmlFor="username">Enter Username to Delete</label>
            </div>
            {deleteoperation}
            </div>

            )
    }

}