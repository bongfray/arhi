import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import ProfDash from './dash/ProfDash'
import DirDash from './dash/DirDash'
import AssiDash from './dash/AssiDash'
import AssoDash from './dash/AssoDash'
import Signup from './components/sign-up'
import LoginForm from './components/login-form'
import Navbar from './components/navbar'
import Par from './components/par'
import PartB from './components/PartB/partB'
import Home from './components/Home'
import './style.css'


class App extends Component {
  constructor() {
    super()
    this.state = {
      loggedIn: false,
      username: null,
      logins: null
    }

    this.getUser = this.getUser.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }


  componentDidMount() {
    this.getUser()
  }

  updateUser (userObject) {
    this.setState(userObject)
  }

  getUser() {
    axios.get('/user/').then(response => {
      console.log('Get user response: ')
      console.log(response.data)
      if (response.data.user) {
        console.log('Get User: There is a user saved in the server session: ')
        console.log(response.data.user);
        this.setState({
          loggedIn: true,
          username: response.data.user.username,
          logins: response.data.user.use

        })
      } else {
        console.log('Get user: no user');
        this.setState({
          loggedIn: false,
          username: null,
          logins: null

        })
      }
    })
  }


  render() {
    //const redir = this.props.logins;
    return (
       <div className="App">

        <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn} logins={this.state.logins}/>
        {/* greet user if logged in: */}

        {this.state.loggedIn &&
          <p className="center">Welcome , {this.state.username}!</p>

        }
        {/*this.state.loggedIn &&

          window.location.assign(API_URL+'/signup')
        */}
    {  /*  {this.state.loggedIn ?
        (

          window.location.assign(API_URL+'/signup')
        )
        :
        (
        window.location.assign(API_URL+'/')
        )
      }*/}
        <Route path="/login" render={() =>
          <LoginForm
         updateUser={this.updateUser}
         />}
       />

       <Route path="/signup" component={Signup} />

      <Route path="/ProfDash"  component={ProfDash} />
      <Route path="/DirDash" component={DirDash} />
      <Route path="/AssiDash" component={AssiDash} />
      <Route path="/AssoDash" component={AssoDash} />
      <Route path="/partB" render={() =>
        <PartB loggedIn={this.state.loggedIn}/>} />
      <Route path="/par" render={() =>
        <Par
       updateUser={this.updateUser} loggedIn={this.state.loggedIn}
       />} />
       <Route exact path="/" component={Home} />
      </div>
    );
  }
}

export default App;
