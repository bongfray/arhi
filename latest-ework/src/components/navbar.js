import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Route, Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown , Button} from 'react-materialize'
import Slogo from './Slogo.png'
import './style.css'
import Logo from './Logout.png'
import M from 'materialize-css'

class Navbar extends Component {
  componentDidMount() {
    M.AutoInit();
    let elems=document.querySelectorAll('.dropdown-button');
    M.Dropdown.init(elems,{inDuration : 300 ,outDuration :225, hover: true, belowOrigin: true});

}
    constructor() {
        super()
        this.logout = this.logout.bind(this)
    }

    logout(event) {
        event.preventDefault()
        //console.log('logging out')
        axios.post('/user/logout').then(response => {
          //console.log(response.data)
          if (response.status === 200) {
            this.props.updateUser({
              loggedIn: false,
              username: null
            })
            window.location.assign("/");
          }
        }).catch(error => {
            window.M.toast({html: 'Logout Error! Internal Problem !!', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
      }
    render() {
        const loggedIn = this.props.loggedIn;
        const logins =this.props.logins;
        //console.log('navbar render, props: ')
        console.log(this.props);

        return (

<div>

            {loggedIn ? (

                      <nav>
                          <div class="nav-wrapper blue-grey darken-2">
                          <div className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div>
                          <span className="center nav-cen srm-care">SRM CARE</span>
                          <ul class="right">
                          <li>
                            <Link to="/">Home</Link>
                          </li>
                          <li>
                             <Dropdown trigger={<Button />}>
                              <Link to="/profile">
                               Profile
                              </Link>
                             <a href={logins}>
                               DashBoard
                             </a>
                             <Link to="/partB">
                               Sections
                             </Link>
                            </Dropdown>
                          </li>
                          <li>
                               <Link to="#" className="right" onClick={this.logout}>
                                <img src={Logo} className="lgout" alt="log"/>
                               </Link>
                          </li>
                          </ul>
                          </div>
                      </nav>

            ) : (
<nav>
              <div className="nav-wrapper blue-grey darken-2">
                  <div className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div>
                  <div className="center con"><span className="nav-cen ">SRM Centre for Applied Research in Education</span> <Link to="/login" className="col s2 login-sign right"><b>LOG IN</b></Link></div>

              </div>
  </nav>
                )}


</div>
        );

    }
}

export default Navbar
