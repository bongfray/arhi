const express = require('express')
const router = express.Router()
const User = require('../database/models/user')
const passport = require('../passport')

router.post('/', (req, res) => {
    //console.log('user signup');

    const { username, password,title,name,id,phone,campus,dept,desgn } = req.body
    User.findOne({ username: username }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            msg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
              username: username,
              password: password,
              title: title,
              name: name,
              id: id,
              phone: phone,
              campus: campus,
              dept: dept,
              desgn: desgn
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})

router.post(
    '/login',
    function (req, res, next) {
        console.log('routes/user.js, login, req.body: ');
        console.log(req.body)
        next()
    },
    passport.authenticate('local'),
    (req, res) => {
        //console.log('logged in', req.user);
        var userInfo = {
            use: req.user.desgn,
            username: req.user.username
        };
        res.send(userInfo);
    }
)

router.get('/', (req, res, next) => {
    //console.log('===== user!!======')
    console.log(req.user)
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})

router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out' })
    } else {
        res.send({ msg: 'no user to log out' })
    }
})

module.exports = router
