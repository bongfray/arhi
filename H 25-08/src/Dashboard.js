import React, { Component } from 'react';
import { render } from "react-dom";
// import "./styles.css";
import { css } from '@emotion/core';
// First way to import
// Another way to import


// Can be a string as well. Need to ensure each key-value pair ends with ;
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./AnimatedProgressProvider";
import ChangingProgressProvider from "./ChangingProgressProvider";
import RadialSeparators from "./RadialSeparators";
import ChartsPage from "./newani.js";

const percentage = 66;
const research = 76;
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

render(){
  return (
    <div className="App ">
    <div className="col l8">
    <Example label="Fully controlled animation, including text">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={percentage}
        duration={1.4}
        easingFunction={easeQuadInOut}

        // repeat
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              /* This is important to include, because if you're fully managing the
        animation yourself, you'll want to disable the CSS animation. */
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    <div className="">
    <Example label="Fully controlled animation, including text">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={percentage}
        duration={1.4}
        easingFunction={easeQuadInOut}

        // repeat
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              /* This is important to include, because if you're fully managing the
        animation yourself, you'll want to disable the CSS animation. */
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>


    </div>
    );
    }

  }
  function Example(props) {
  return (
  <div className="">
      <div style={{ width: 100 }}>{props.children}</div><br />
        <h3 className="h5">{props.label}</h3>
  </div>
  );
  }

export default App;
