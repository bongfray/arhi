import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
import Slogo from './Slogo.png'
import './style.css'

import M from 'materialize-css'



export default class Admin extends Component{
constructor(){
    super();
    this.state ={
        isChecked: false,
        selected: ''
    }
    // this.componentDidMount= this.componentDidMount.bind(this);
}
handleChecked =(e)=>{
    this.setState({
        isChecked: !this.state.isChecked,
        selected: e.target.value
    });
}
render()
{
    return(
        <React.Fragment>
            <div className="row">
                
            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-1' name='myRadio' value='insert' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Insert</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-2' name='myRadio' value='update' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Update</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-3' name='myRadio' value='edit' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Edit</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-4' name='myRadio' value='delete' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Delete</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-5' name='myRadio' value='modify' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Modify</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-6' name='myRadio' value='suspend' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Suspension</b></span>
            </label>
            </p>
            </div>
            </div>

            </div>

            
            <Display selected={this.state.selected}/>
        


            
        </React.Fragment>
    );
}
}


class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: false,
            select: ''
        }
    }
    handleCheck = (e) => {
        this.setState({
            isChecked: true,
            select: e.target.value
        })

    }
    render(){
        if(this.props.selected === 'insert')
        {
            return(
                <div className="row">
                <div className="col s1"></div>
                <div className="card col s10">
                    <div className="col s2 brd-r">
                        <p>
                        <label>
                        <input type='radio' id='radio-7' name='myRadio' value='insertadmin' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Admin</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-8' name='myRadio' value='insertfac' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Faculty</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-9' name='myRadio' value='insertstu' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Student</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col s10">
                        <div className="col s2"></div>
                        <div className="col s8">
                            <InsertUser select={this.state.select} />
                        </div>
                        <div className="col s2"></div>
                    </div>
                    <div className="col s1"></div>
                
                
                </div>
                </div>

            );
        }
        else if(this.props.selected === 'update')
        {
            return(
                <div className="row">
                <div className="col s1"></div>
                <div className="card col s10">
                <p>
                <label>
                <input type='radio' id='radio-10' name='myRadio' value='updateuser' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Update User</b></span>
                </label>
                </p>
                {/* <InsertUser select={this.state.select} /> */}
                </div>
                </div>

            );
        }
        else if(this.props.selected === 'edit')
        {
            return(
                <div className="row">
                <div className="col s1"></div>
                <div className="card col s10">
                <p>
                <label>
                <input type='radio' id='radio-11' name='myRadio' value='edituser' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Edit User</b></span>
                </label>
                </p>
                {/* <InsertUser select={this.state.select} /> */}
                </div>
                </div>

            );
        }
        else if(this.props.selected === 'delete')
        {
            return(
                <div className="row">
                <div className="col s1"></div>
                <div className="card col s10">
                <div className="col s2 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-20' name='myRadio' value='deleteadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-21' name='myRadio' value='deletefac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-22' name='myRadio' value='deletestu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Student</b></span>
                </label>
                </p>
                </div>
                <div className="col s10">
                    <div className="col s2"></div>
                    <div className="col s8">
                    <DeleteUser select={this.state.select} />
                    </div>
                    <div className="col s2"></div>
                </div>
                </div>
                </div>

            );
        }
        else if(this.props.selected === 'modify')
            {
            return(
                <div className="row">
                <div className="col s1"></div>
                <div className="card col s10">
                <div className="col s2 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-18' name='myRadio' value='start-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Start Semester</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-19' name='myRadio' value='end-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>End Semester</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-13' name='myRadio' value='modifytoday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Modify Today's Day Order</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-17' name='myRadio' value='holiday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Declare a Holiday</b></span>
                </label>
                </p>
                </div>
                <div className="col s10">
                    <div className="col s2"></div>
                    <div className="col s8">
                    <ModifyUser select={this.state.select} />
                    </div>
                    <div className="col s2"></div>
                </div>
                </div>
                </div>
            )
        }
        else if(this.props.selected === 'suspend')
        {
            return(
                <div className="row">
                <div className="col s1"></div>
                <div className="card col s10">
                <div className="col s2 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-14' name='myRadio' value='suspendadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-15' name='myRadio' value='suspendfac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-16' name='myRadio' value='suspendstu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Student</b></span>
                </label>
                </p>
                </div>
                <div className="col s10">
                    <div className="col s2"></div>
                    <div className="col s8">
                    <SuspendUser select={this.state.select} />
                    </div>
                    <div className="col s2"></div>
                </div>
                </div>
                </div>

            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}

/*------------Insert------------ */

class InsertUser extends Component{
    constructor(){
        super();
        this.state = {
            username: '',
            pswd: ''
        }
        
    }
    handleUsername = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handlePswd = (e) =>{
        this.setState({
            pswd: e.target.value
        })
    }
    
    handleSubmit(event) {

    }
    render(){
        if(this.props.select==='insertadmin')
        {
            return(
                <div className="center">

                                <div className="input-field col s6">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>

								<div className="input-field col s6">
								<input id="password" type="password" className="validate" value={this.state.pswd} onChange={this.handlePswd} required />
								<label htmlFor="password">Password</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Insert Admin</Link>
                </div>
        )
        }
        else if(this.props.select==='insertfac')
        {
            return(
                <div className="center">

                                <div className="input-field col s6">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>

								<div className="input-field col s6">
								<input id="password" type="password" className="validate" value={this.state.pswd} onChange={this.handlePswd} required />
								<label htmlFor="password">Password</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Insert Faculty</Link>
                </div>
            )
        }
        else if(this.props.select==='insertstu')
        {
            return(
                <div className="center">

                                <div className="input-field col s6">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>

								<div className="input-field col s6">
								<input id="password" type="password" className="validate" value={this.state.pswd} onChange={this.handlePswd} required />
								<label htmlFor="password">Password</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Insert Student</Link>
                </div>
            )
        }
        else if(this.props.select==='deleteuser')
        {
            return(
                <div>
                    <input type="text"/>
                </div>
            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}

/*-------------------End of Insert--------------- */

/*-------------------Delete--------------------- */

class DeleteUser extends Component{
    constructor(){
        super();
        this.state={
            username: ''
        }
    }
    handleDelete = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handleSubmit(event){

    }
    render(){
        if(this.props.select === 'deleteadmin'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleDelete} required />
								<label htmlFor="username">Enter Username to Delete</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Admin</Link>
                </div>

            )
        }
        else if(this.props.select === 'deletefac'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleDelete} required />
								<label htmlFor="username">Enter Username to Delete</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Faculty</Link>
                </div>

                )
        }
        else if(this.props.select === 'deletestu'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleDelete} required />
								<label htmlFor="username">Enter Username to Delete</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Student</Link>
                </div>

                )
        }
        else{
            return(
                <div></div>
            )
        }
    }

}

/*---------------End of Delete---------------- */

/*-------------------Modify-------------------- */

class ModifyUser extends Component{
    constructor(){
        super();
        this.state={
            dayorder: '',
            day: '',
            month: '',
            year: '',
            sday: '',
            smonth: '',
            syear: '',
            eday: '',
            emonth: '',
            eyear: ''
        }
    }
    handleDayorder = (e) =>{
        this.setState({
            dayorder: e.target.value
        })
    }
    handleDay = (e) =>{
        this.setState({
            day: e.target.value
        })
    }
    handleMonth = (e) =>{
        this.setState({
            month: e.target.value
        })
    }
    handleYear = (e) =>{
        this.setState({
            year: e.target.value
        })
    }
    handleSday = (e) =>{
        this.setState({
            sday: e.target.value
        })
    }
    handleSmonth = (e) =>{
        this.setState({
            smonth: e.target.value
        })
    }
    handleSyear = (e) =>{
        this.setState({
            syear: e.target.value
        })
    }
    handleEday = (e) =>{
        this.setState({
            eday: e.target.value
        })
    }
    handleEmonth = (e) =>{
        this.setState({
            emonth: e.target.value
        })
    }
    handlEyear = (e) =>{
        this.setState({
            eyear: e.target.value
        })
    }
    handleSubmit(event){

    }
    handleHoliday(event){

    }
    handleStart(event){

    }
    handleEnd(event){

    }

    render(){
        if(this.props.select === 'start-semester'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="sday" type="text" className="validate" value={this.state.sday} onChange={this.handleSday} required />
								<label htmlFor="sday">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="smonth" type="text" className="validate" value={this.state.smonth} onChange={this.handleSmonth} required />
								<label htmlFor="smonth">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="syear" type="text" className="validate" value={this.state.syear} onChange={this.handleSyear} required />
								<label htmlFor="syear">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleStart}>Start Semester</Link>
                </div>

            )
        }
        else if(this.props.select === 'end-semester'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="eday" type="text" className="validate" value={this.state.eday} onChange={this.handleEday} required />
								<label htmlFor="eday">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="emonth" type="text" className="validate" value={this.state.emonth} onChange={this.handleEmonth} required />
								<label htmlFor="emonth">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="eyear" type="text" className="validate" value={this.state.eyear} onChange={this.handleEyear} required />
								<label htmlFor="eyear">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleEnd}>End Semester</Link>
                </div>

            )
        }
        else if(this.props.select === 'modifytoday'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="dayorder" type="text" className="validate" value={this.state.dayorder} onChange={this.handleDayorder} required />
								<label htmlFor="dayorder">Reset Today's Day Order to:</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Reset Day Order</Link>
                </div>
                
            )
        }
        else if(this.props.select === 'holiday'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="day" type="text" className="validate" value={this.state.day} onChange={this.handleDay} required />
								<label htmlFor="day">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="month" type="text" className="validate" value={this.state.month} onChange={this.handleMonth} required />
								<label htmlFor="month">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="year" type="text" className="validate" value={this.state.year} onChange={this.handleYear} required />
								<label htmlFor="year">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleHoliday}>Declare a Holiday</Link>
                </div>

            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}

/*-------------------End of Modify-------------*/

/*------Suspend -----*/

class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            username: ''
        }
    }
    handleUsername = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handleSuspend(event){

    }
    handleRemove(event){
        
    }
    render(){
        if(this.props.select === 'suspendadmin'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Admin</Link>
                </div>
                
            )
        }
        else if(this.props.select === 'suspendfac'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Faculty</Link>
                </div>
                
            )
        }
        else if(this.props.select === 'suspendstu'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Student</Link>
                </div>
                
            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}