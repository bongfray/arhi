
import React from 'react'
import {Modal} from 'react-materialize'
import '../style.css'
const Insta = props => {

     const divStyle = {
          display: props.displayModal ? 'block' : 'none'
     };
     function closeModal(e) {
        e.stopPropagation()
        props.closeModal()
     }
     return (
       <div
         className="modal instructionmodal"
         style={divStyle}
         onClick={ closeModal }>
         <div className="modal-content">
           <h4 className="center">Instructions</h4>
           <p >1. Click on the "Select Here" part in the left hand side<br/>
              2. When you click on Regular Class, some contents will appear.<br />
              3. The contents with <span style={{color:'red'}}>red color</span> implies the task is pending till now.<br />
              4. By clicking on the each div with red color,you can easily see the details of the following div and there itself you have to submit datas.<br />
              5. And contents with <span style={{color:'green'}}>green color</span> is the confirmation of your task completion.<br />
              6. Remember one thing once you submit the data, in any circumtence it is not editable.<br />
              7. You can submit Yesterday's DayOrder data but only in between 24 Hours time span.<br />
              8. On the left hand side in the top of the regular class page you can see Yesterday dayorder navigation.<br />
           </p>

         </div>
         <div className="modal-footer">
           <a href="#!" className="modal-close waves-effect waves-green btn-flat" onClick={ closeModal }>Good to go ?</a>
         </div>
       </div>
     );
}
export default Insta;
