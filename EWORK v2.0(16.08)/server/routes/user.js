const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const User = require('../database/models/user')
const Prof = require('../database/models/profile1')
const DayOrder = require('../database/models/dayorder')
const Timetable = require('../database/models/time_table')
const BluePrint = require('../database/models/blueprint')
const passport = require('../passport')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')


function logout()
{
  const url = 'mongodb://localhost:27017/eWork'
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    var cursor = client.collection('sessions').remove( { } );
  });
}


/*------------------------------------------------------------------------Changing Day Orders------------------- */
let timer;

function nextGo(time)
  {
    timer = setTimeout(() => {
    myFunc()
  },time);
}
clearInterval(timer);

DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
  if(user)
  {
  console.log("Server Started!!!");
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour + minutes + second;
  let max = (89997000-total);
  let gap = (user.time-max);
  console.log(user.time-gap);
  let actual = (user.time - gap);
  // console.log(Date.today().equals(Date.parse("tomorrow")));
  nextGo(actual);
  }
else
{
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour + minutes + second;
  let time = (89997000-total);
  // let time = 2500000;
  const dayyy = new DayOrder({
    day_order: 1,
    time: time,
    time_now: total,
  });

  dayyy.save((err, savedUser) => {
  });
  nextGo(time);
}
})


function myFunc()
{
    console.log("Entered")
      DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err)
        {

        }
        else if(user)
        {
            let prev = user.day_order;
            let recent = prev + 1;
            let next;
            if(prev === 5)
            {
              next=1;
            }
            else
            {
              next = recent;
            }
            let hour = (new Date().getHours())*3600000;
            let minutes = (new Date().getMinutes())*60000;
            let second =(new Date().getSeconds())*1000;
            let total = hour + minutes + second;
            let max = (89997000-total);
            if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
            {
              DayOrder.updateOne({day_order: prev},{ $set: { day_order: 0, time: max}} ,(err, newdata) => {
                DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
                  nextGo(user.time);
                })

              });
            }
            else
            {
              console.log(next);
              DayOrder.updateOne({day_order: prev},{ $set: { day_order: next, time: max}} ,(err, newdata) => {
                logout();
                DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
                  nextGo(user.time);
                })
              });
            }
        }

      });

}








/*--------------------------------------To Set the color of the 10 Cards------------------ */

router.post('/fetchfrom', function(req, res) {
  Timetable.findOne({$and: [{day_slot_time : req.body.slot_time},{usern: req.user.username}]}, function(err, passed){
    res.send(passed);
  })
  });

/*------------------------To Render Slots in 10 Cards in Timetable---------------------------*/
  router.post('/fetchme', function(req, res) {
    BluePrint.findOne({$and: [{timing : req.body.slot_time},{username: req.user.username}]}, function(err, passed){
      res.send(passed);
    })
    });


  router.get('/fetchdayorder', function(req, res) {
    if(!req.user)
    {
      res.send("Not")
    }
    else
    {
      DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err){

        }
        else if(user)
        {
                var have ={
                  time: user.time,
                  day_order: user.day_order,
                  time_upload: user.time_now,
                }
                res.send(have);
        }
      });
    }
    });

    router.post('/fetchfromtimetable', function(req, res) {
      if(!req.user)
      {
        res.send("Not")
      }
      else
      {
      const {day_slot_time} = req.body;
                  BluePrint.findOne({$and: [{timing: day_slot_time},{username: req.user.username}]}, function(err, passed){
                    if(passed)
                    {
                      var alloted_slots ={
                        alloted_slots: passed.alloted_slots,
                      }
                      res.send(alloted_slots);
                    }
                    else{
                      var message ={
                        message: "There is no Saved Data for This Slot",
                      }
                      res.send(message);
                    }
                  })
          }
      });


router.post('/', (req, res) => {

    const { username, password,title,name,id,phone,campus,dept,desgn,dob,count} = req.body
    User.findOne({ username: username }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
              username: username,
              password: password,
              title: title,
              name: name,
              id: id,
              phone: phone,
              campus: campus,
              dept: dept,
              desgn: desgn,
              dob: dob,
              count: count,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              day_order: 1,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})



router.post('/newd', (req, res) => {

    const {id,up_username,up_phone,up_name,up_dob} = req.body;
    // var password = req.body.new_password;
    User.findOne({ id: id }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          // let exist_password = req.body.current_password;
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            // console.log("Successful Authen");
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            // console.log(hash);
            User.updateOne({id:id},{ $set: { username: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              // console.log("Successful Hash");
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          User.updateOne({id:id},{ $set: { username: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)







/*---------------------------------------------reseting password by mail ------------------------------- */
router.post('/reset_from_mail', (req, res) => {
  //console.log('user signup');

  const {token_come,password} = req.body;
  User.findOne({ resetPasswordToken: token_come }, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if(user) {
        console.log(user)
        let hash = bcrypt.hashSync(password, 10);
          User.updateOne({resetPasswordToken: token_come},{password: hash} ,(err, user) => {
            console.log("Successful Hash");
            var succ= {
              succ: "Password Updated"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "failed"
          };
          res.send(fail);
        }
      })
    })


/*End of Signup------------------------------------------------------------------------------ */
router.post('/profile1', (req, res) => {

    const { username, id , ug_clg_name,pg_clg_name,phd_clg_name,ug_start_year,pg_start_year,phd_start_year,ug_end_year,pg_end_year,phd_end_year,marks_ug,marks_pg,marks_phd,other_degree,other_degree_info,certificate,honors_awards} = req.body
    User.findOne({ username: username }, (err, objs) => {
        if (err)
        {
          var errormsg = {
            errormsg: "Kindly Priovide Correct Mail Id & Id",
          };
            res.send(errormsg);
        }
        else if (objs.count==="2") {
          var emsg = {
            msg: "Datas are already there",
          };
            res.send(emsg);
        }
        else if ((objs.count === "1") || (obj.count === "0"))
        {
            const profData = new Prof({
              username: username,
              id: id,
              ug_clg_name:ug_clg_name,
              pg_clg_name:pg_clg_name,
              phd_clg_name:phd_clg_name,
              ug_end_year:ug_end_year,
              pg_end_year:pg_end_year,
              phd_end_year:phd_end_year,
              ug_start_year:ug_start_year,
              pg_start_year:pg_start_year,
              phd_start_year:phd_start_year,
              marks_pg:marks_pg,
              marks_ug:marks_ug,
              marks_phd:marks_phd,
              other_degree:other_degree,
              other_degree_info:other_degree_info,
              certificate:certificate,
              honors_awards:honors_awards
            })
            profData.save((err, savedUser) => {
              var succ= {
                succ: "Successful Submitted"
              };
              res.send(succ);
            })
        }
    })
})




/*End of PROFILE1-------------------------------------------------------------------------------- */



router.post(
    '/login',(req, res, next) => {


      const { username, password} = req.body;


      User.findOne({ username: username }, function(err, objs){
        if(err)
        {
          return;
        }
        else if(objs)
        {
         if (objs.count === "0")
          {
              var counter = 0;
              User.updateOne({ username: username }, {count: counter+1},(err, user) => {

              })

          }
          else if (objs.count === "1")
          {
              var coun = 0;
              User.updateOne({ username: username }, {count: coun+2},(err, user) => {

              })
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)

router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})


router.get('/knowcount', function(req, res) {
    const { username} = req.user;
    if(req.user)
    {
    User.findOne({ username: username }, function(err, objs){

        if (objs)
        {
            res.send(objs);
        }
    });
  }
  else{
    return;
  }
    });



router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })



})

router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})



router.get('/dash2', function(req, res) {
    const { username} = req.user;
    User.findOne({ username: username }, function(err, objs){

        if (objs)
        {
            res.send(objs);
        }
    });
    });




    router.post('/timeallot', (req, res) => {
        const { day_slot_time, selected, usern,order, saved_slots,problem_statement,covered} = req.body
        console.log(req.body);
        User.findOne({ username: usern }, (err, user) =>{
          if(err){
          }
          else if(user)
          {

        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{usern: user.username}]}, (err, objs) => {
            if (err)
            {
                alert("Error")
            }
            else if (objs)
            {
              var emsg = {
                msg: "We have datas saved on the following slot",
              };
                res.send(emsg);
            }
            else if(saved_slots === "There is no Saved Data for This Slot")
            {
              var e_msg = {
                e_msg: "This Slot is not Saved Previously. You are not allowed to Store so.",
              };
                res.send(e_msg);
            }
            else {
                const newTime = new Timetable({
                  usern: usern,
                  day_slot_time:day_slot_time,
                  order: order,
                  selected:selected,
                  problem_statement: problem_statement,
                  covered: covered,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                })


        }
      });

      }
    })
  })

    router.post('/timefree', (req, res) => {
        //console.log('user signup');

        const { day_slot_time,usern, freeslot,freefield, order } = req.body
        User.findOne({ username: usern }, (err, user) =>{
          if(err){

          }
          else if(user)
          {
        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{usern: user.username}]}, (err, objs) => {
            if (err)
            {
                //console.log('User.js post error: ', err)
            }
            else if (objs)
            {

              var emsg = {
                msg: "We have datas saved on the following slot",
              };
                res.send(emsg);
            }
            else {
              BluePrint.findOne(({ timing: day_slot_time }), (err, loaded) => {
                if(loaded)
                {
                  var newmsg = {
                    newmsg: "This is Alloted Slot. You are not Supposed to do other work",
                  };
                  console.log(newmsg);
                    res.send(newmsg);

                }
                else {
                const newTime = new Timetable({
                  usern: usern,
                  day_slot_time:day_slot_time,
                  order: order,
                  freefield: freefield,
                  freeslot: freeslot,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                })
              }
              });
            }
        })
      }
      })
    })

    router.post('/timecancel', (req, res) => {
        //console.log('user signup');

        const { day_slot_time,usern, order, compensation_status,c_cancelled } = req.body
        User.findOne({ username: usern }, (err, user) =>{
          if(err){
            alert("NO?");

          }
          else if(user)
          {
        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{usern: user.username}]}, (err, objs) => {
            if (err)
            {
                //console.log('User.js post error: ', err)
            }
            else if (objs)
            {
              var emsg = {
                msg: "We have datas saved on the following slot",
              };
                res.send(emsg);
            }
            else {
              BluePrint.findOne(({ timing: day_slot_time }), (err, wehave) => {
              if(err){

              }
              else if(wehave){
                var exist = {
                  exist: "This is Alloted Slot. You are not Supposed to do other work",
                }
                console.log(exist);
                res.send(exist);
              }
              else{
                const newTime = new Timetable({
                  usern: usern,
                  day_slot_time:day_slot_time,
                  order: order,
                  c_cancelled: c_cancelled,
                  compensation_status: compensation_status,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                })
            }
          });
          }
        })
      }
      })
    })




    router.post('/forgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       User.findOne(
         {username: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           console.log("Error");
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           console.log(token);
           User.updateOne({username:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 900000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Reset Password:  eWorks', // Subject line
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 15min</p>'
          };
          // console.log("Way to enter.......");
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })

        })

        router.get('/reset_password', function(req, res) {
          console.log(req.query);
          User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
            if (!user) {
              console.log("Expired");
              var expire ={
                expire:"Link is Expired"
              }
              res.send(expire);
            }
            else if(user){
              console.log("Tested:  "+user.resetPasswordToken)
              res.send(user.resetPasswordToken)
            }

          });
        });





/*------------------------Blue Print Send-------------------------------------------------- */
router.post('/send_blue_print', (req, res) => {
    //console.log('user signup');
    console.log(req.body)
    const {alloted_slots,timing} = req.body
    User.findOne({ username: req.user.username}, (err, user) => {
      if(err)
      {

      }
      else if(user)
      {
        BluePrint.findOne({$and: [{timing: timing},{username: user.username}]}, (err, objs) => {
          if(err)
          {

          }
          else if(objs)
          {
            console.log(objs)
            var emsg = {
              emsg: "We have datas saved on the following Time",
            };
              res.send(emsg);
          }
          else{
            const allottime = new BluePrint({
              alloted_slots: alloted_slots,
              timing: timing,
              username: req.user.username,

            })
            allottime.save((err, savedUser) => {
              var succ= {
                succ: "Success"
              };
              res.send(succ);
            })
          }
        })
      }
    })
  })

module.exports = router
