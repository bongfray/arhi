import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Allot'


{/*---------------------------------------------------------Code for regular classes time table------------------------------------ */}
export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',

    };
    this.componentDidMount= this.componentDidMount.bind(this);
  }
  componentDidMount(){
    M.AutoInit();
  }


  handle1 = (e, index) => {
    // alert(e.target.value);
    // alert(e.target.name);
    const key = e.target.name;
    const value = e.target.value;
    this.setState({ rit: value, index ,day_order: key, index});

  };


  render() {
    // alert(this.state.day_order);
const {datt} = this.props.datt;
    return (
            <div className="root-of-time" >


             {this.props.datt.map((content,index) => {
               return(
              <div className="row" key={index}>
                   <div className="col l12 card hoverable ">
                   <div className="card-title">
                   <span className="">
                   <div className="center day-order-day">{content.head}</div>
                   </span>
                   </div><br /><br />
                     <div>
                       <select name={content.con} onChange={e => this.handle1(e, index)}>
                           <option defaultValue="" disabled selected>Choose</option>
                           <option value="allot">Alloted Class</option>
                           <option value="free">Free Slot</option>
                           <option value="slot_cancel">Slot Cancelled</option>
                       </select>
                     </div>
                     <span className="drop">

                       <TableDisplay
                       selectValue={
                         index == this.state.index && this.state.rit} day_order={this.state.day_order} usern={this.props.usern}
                       />
                     </span>
                   </div>


                   </div>
             );
             })}


</div>
          );
  }
}



{/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */}


class TableDisplay extends Component {
  constructor() {
    super()
    this.state = {
      allot:'',
        count: 0,
        selected:'',
        problem_conduct:'',
        freeslot:'',
        freefield:'',
        c_cancelled: '',
        compensation:'false',
        saved_dayorder: '',

    }

    this.handleFreeSlot = this.handleFreeSlot.bind(this)
    this.handleFreeVal = this.handleFreeVal.bind(this)
    this.handleFreeField = this.handleFreeField.bind(this)
    this.handleMissComp = this.handleMissComp.bind(this)
    this.handleCancel = this.handleCancel.bind(this)

    }



    handleFreeField =(e) =>{
      this.setState({
        freefield: e.target.value,
      })

    }

    handleFreeVal =(e) =>{
      this.setState({
        freeslot: e.target.value,
      })

    }


    handleFreeSlot =(e) =>{
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      // alert(this.props.usern)

      axios.post('/user/timefree', {
        usern: this.props.usern,
        order: this.props.day_order+'Free',
        day_slot_time: this.props.day_order,
        freefield: this.state.freefield,
        freeslot: this.state.freeslot,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.msg)
            {
              alert(response.data.msg);
            }
            else if(response.data.succ)
            {
              alert('Success');
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          selected: '',
      })


    }


    handleCancel =(e) =>{
      // alert(e.target.value)
      this.setState({
          c_cancelled: e.target.value,
      })

    }


    handleCancelSlot =(e) =>{
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      alert(this.state.compensation)
      alert(this.state.c_cancelled)

      axios.post('/user/timecancel', {
        usern: this.props.usern,
        order: this.props.day_order+'Cancel',
        day_slot_time: this.props.day_order,
        compensation_status: this.state.compensation,
        c_cancelled: this.state.c_cancelled,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.msg)
            {
              alert(response.data.msg);
            }
            else if(response.data.succ)
            {
              alert('Success');
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          selected: '',
      })


    }

    handleMissComp = (e) =>{
      // alert(e.target.value)
      this.setState({
        compensation: e.target.value,
      })
    }






  render() {


                if (this.props.selectValue === "allot") {
                  return(
                    <div className="">
                    <Allot usern={this.props.usern } day_slot_time ={this.props.day_order}/>
                    </div>
                  );

                } else if (this.props.selectValue === "free") {
                  return(
                    <div><h6 className="free-head">Free Slot is for different Works. Kindly fill the Details..</h6><br />
                    <span className="quali">Plaese Mention on which area you are going to work :</span><br />
                              <div className="input-field inline">
                                <input id="email_inline" type="text" value={this.state.freefield} onChange={this.handleFreeField} required/>
                                <label for="email_inline">Fill it correctly</label>
                              </div>
                    <label className="pure-material-textfield-outlined alignfull">
                      <textarea
                        className="area"
                        type="text"
                        placeholder=" "
                        min="10"
                        max="60"
                        value={this.state.freeslot}
                        onChange={this.handleFreeVal}
                      />
                      <span>Detail About Your work</span>
                    </label>
                    <div className="right btn-of-submit-at-time"><Link to ="#" className="btn" onClick={this.handleFreeSlot}>SUBMIT</Link></div>
                    </div>
                  );

                }
                else if (this.props.selectValue === "slot_cancel") {
                 return(
                   <div>
                   <label className="pure-material-textfield-outlined alignfull">
                     <textarea
                       className="area"
                       type="text"
                       placeholder=" "
                       min="10"
                       max="60"
                       value={this.state.c_cancelled}
                       onChange={this.handleCancel}
                     />
                     <span>Enter the reason</span>
                   </label><br />
                   <p className="need">
                    <label>
                      <input type="checkbox" value="true" onChange={this.handleMissComp} />
                      <span style={{color:'black'}}>Would you like to Get a extra slot in this week ? </span>
                    </label>
                  </p>
                  <div className="right btn-of-submit-at-time"><Link className="btn" to="#" onClick={this.handleCancelSlot} >SUBMIT</Link>
                  </div>
                   </div>
                 );

               }
                else{
                  return(
                    <div className="inter-drop center">
                      Choose from above DropDown
                    </div>
                  );
                }
  }
}
