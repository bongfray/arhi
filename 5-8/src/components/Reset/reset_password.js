import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link ,Redirect} from 'react-router-dom'
import axios from 'axios'
import '../style.css'

import M from 'materialize-css'

export default class ResetPassword extends Component {
    constructor() {
        super()
        this.state = {
            redirectTo: null,
            password: '',
            con_password: '',
            username:'',

        }
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleConPasswordChange = this.handleConPasswordChange.bind(this)
        this.handleResetPass = this.handleResetPass.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)

        }
            handlePasswordChange =(e) =>{
                this.setState({
                    password: e.target.value
                });
            }
            handleConPasswordChange =(e) =>{
                this.setState({
                    con_password: e.target.value
                });
            }
            handleResetPass =(e) =>{
                alert(this.state.username);
                e.preventDefault();
                axios.post('/user/reset_from_mail', {
                    username: this.state.username,
                    password: this.state.password,
                  })
                   .then(response => {
                     console.log(response)
                     if(response.status===200){
                       // alert("Success")
                       if(response.data.succ)
                        {
                         alert(response.data.succ);
                          this.setState({
                             redirectTo: '/'
                         })
                       }
                     }
                    }).catch(error => {
                      window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
                    })
            }


        componentDidMount() {
          // alert(this.props.match.params.token);
           axios.get('/user/reset_password',{
             params: {
               resetPasswordToken: this.props.match.params.token,
             },
          }).then(response => {
            if(response.status === 200)
            {



              if(response.data.expire)
              {
                alert(response.data.expire);
                window.location.assign('/');
              }
              else
              {
                // alert(response.data)
                this.setState({
                  username: response.data,
                })
              }
            }
            else{
              // alert(response.data.usern)
              alert("Error");
            }
          })

            M.AutoInit();
        }
    render(){
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
        return(
            <div className="">
<div className="card">
    <div className="row">
            <div className="input-field col s6">
            <input onChange={this.handlePasswordChange} id="pswd" value={this.state.password} type="password" className="validate" required />
		    <label htmlFor="pswd">Password</label>
			</div>
 <div className="input-field col s6">
  <input onChange={this.handleConPasswordChange} id="cpswd" value={this.state.con_password} type="password" className="validate" required />
  <label htmlFor="cpswd">Confirm Password</label>
 </div>
 </div>
 <div className=""><Link className="btn" to="#" onClick={this.handleResetPass}>Submit</Link>
     </div>
 </div>
            </div>

        );
    }
    }
}
