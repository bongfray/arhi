import React, { Component } from 'react';
import Slider from 'infinite-react-carousel';
import {} from 'materialize-css'
import M from 'materialize-css'
import $ from 'jquery'
import C1 from './c1.jpg'
import C2 from './c2.jpg'
import C3 from './c3.jpg'
import C4 from './c4.jpg'
import C5 from './c5.jpg'
import Logo from './logo.png'


export default class Home extends Component {
  componentDidMount(){
    
    var el = $('.tabs').first()[0];
    var instance = M.Tabs.init(el, {
      duration: 300,
      onShow: null,
      swipeable: false
    });

    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
      if (scroll >= 450) {
          $("#tabs").addClass("tabs-fixed z-depth-0");
          $(".tabs").addClass("#00838f cyan darken-3 tabs-transparent"); 
      }
      else {
          $("#tabs").removeClass("tabs-fixed z-depth-0");
      }
  });

  }
  render() {
    const settings =  {
      arrows: false,
      arrowsBlock: false,
      autoplay: true,
      initialSlide: true,      
    };
    return (
      <div>
      
        <Slider { ...settings }>
          <div className="center carousel-h"> 
            <img src={C1} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h green">
          <img src={C2} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h red">
          <img src={C3} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h brown">
          <img src={C4} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h yellow">
          <img src={C5} alt="" height='450px' width='100%'/>          
          </div>
        </Slider>

        <div className="row" style={{marginTop:'-6px'}}>
          <div className="tabs-wrapper">
            <div id="tabs">
              
              <ul class="tabs #00838f cyan darken-3 tabs-transparent">
                <li class="col s2"><img src={Logo} alt="" height='50px'/></li>
                <li class="tab"><a className="active" href="#test1">Test 1</a></li>
                <li class="tab"><a href="#test2">Test 2</a></li>
                <li class="tab"><a href="#test3">Test 3</a></li>
                <li class="tab"><a href="#test4">Test 4</a></li>
              </ul>
            </div>
    <div id="test1" class="col s12">
      Test 1 
      <p class="flow-text">Don't forget to click the like button if you found this helpful.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus reprehenderit sapiente at est culpa dolor impedit, eius ea nihil molestiae doloribus quisquam ipsa possimus, aperiam earum, qui sunt libero placeat!
    </div>
    <div id="test2" class="col s12">
      Test 2
      <p class="flow-text">Don't forget to click the like button if you found this helpful.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
    </div>
    <div id="test3" class="col s12">Test 3</div>
    <div id="test4" class="col s12">Test 4</div>
  </div> </div>

</div>
    );
  }
}