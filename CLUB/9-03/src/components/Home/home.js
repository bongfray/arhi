import React, { Component } from 'react';
import{ Link } from 'react-router-dom'
import Slider from 'infinite-react-carousel';
import {} from 'materialize-css'
import M from 'materialize-css'
import Typical from 'react-typical'

import $ from 'jquery'
import C1 from './c1.jpg'
import C2 from './c2.jpg'
import C3 from './c3.jpg'
import C4 from './c4.jpg'
import C5 from './c5.jpg'
import Logo from './auv.png'

import NL from './logo.png'

import Vid from './back.mp4';

import Sponsor from '../sponsor'


export default class Home extends Component {
  componentDidMount(){
    
    // FADE --------------
    $(window).on("load",function() {
      $(document).ready(function(){
      $(this).scrollTop(0);
      });
      $(window).scroll(function() {
        var windowBottom = $(this).scrollTop() + $(this).innerHeight();
        $(".fade").each(function() {
          /* Check the location of each desired element */
          var objectBottom = $(this).offset().top + $(this).outerHeight();
          
          /* If the element is completely within bounds of the window, fade it in */
          if (objectBottom < windowBottom) { //object comes into view (scrolling down)
            if ($(this).css("opacity")==0) {$(this).fadeTo(300,1);}
          } else { //object goes out of view (scrolling up)
            if ($(this).css("opacity")==1) {$(this).fadeTo(300,0);}
          }
        });
        //content not nav
        $(".fade-content").each(function() {
          /* Check the location of each desired element */
          var objectBottom = $(this).offset().top + $(this).outerHeight();
          
          /* If the element is completely within bounds of the window, fade it in */
          if (objectBottom < windowBottom) { //object comes into view (scrolling down)
            if ($(this).css("opacity")==0) {$(this).fadeTo(600,1);}
          } else { //object goes out of view (scrolling up)
            if ($(this).css("opacity")==1) {$(this).fadeTo(600,0);}
          }
        });
      }).scroll(); //invoke scroll-handler on page-load
    });

    //END FADE-------------
    
    $(document).ready(function () {
      $(document).on("scroll", onScroll);
        $('a[href^="#"]').on('click', function (e) {
          e.preventDefault();
          $(document).off("scroll");
          
          $('a').each(function () {
              $(this).removeClass('active');
          })
          $(this).addClass('active');
        
          var target = this.hash,
              menu = target;
          target = $(target);
          $('html, body').stop().animate({
              'scrollTop': target.offset().top+2
          },500, 'swing', function () {
              window.location.hash = target;
              $(document).on("scroll", onScroll);
          });
      });
  });
  
  function onScroll(event){
      var scrollPos = $(document).scrollTop();
      $('.menu-ce a').each(function () {
          var currLink = $(this);
          var refElement = $(currLink.attr("href"));
          if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
              $('.menu-ce ul li a').removeClass("active");
              currLink.addClass("active");
          }
          else{
              currLink.removeClass("active");
          }
      });
  }
    
  // To display none and block on scrolling
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
      if (scroll >= 735) {
          $("#tabs").addClass("tabs-fixed z-depth-0");
          $(".tabs").addClass("#00838f cyan darken-3 tabs-transparent"); 
      }
      else {
          $("#tabs").removeClass("tabs-fixed z-depth-0");
      }

      if(scroll>=250){
        $('.type').addClass("pos_fix");
      }
      else{
        $('.type').removeClass("pos_fix");

      }
      if(scroll>=525){
        $('.arrows').addClass("nav-l");
      }
      else{
        $('.arrows').removeClass("nav-l");

      }
      if(scroll>=735){
        $('.type').removeClass("pos_fix");
        $('.logo-auv').addClass("pos_abs");
        $('.nav').addClass("nav_fix");        
        $('.nav-logo').removeClass("nav-l");

      }
      else{
        $('.logo-auv').removeClass("pos_abs");
        $('.nav').removeClass("nav_fix"); 
        $('.nav-logo').addClass("nav-l");


      }
      if(scroll>=800){
        $('.menu').addClass("nav_fix");
      }
      else{
        $('.menu').removeClass("nav_fix");

      }

      
  });

  //END SCrolling

  }
  render() {
    const settings =  {
      arrows: false,
      arrowsBlock: false,
      autoplay: true,
      initialSlide: true,      
    };
    return (
      <div><svg class="arrows">
							<path class="a1" d="M0 0 L30 32 L60 0"></path>
							<path class="a2" d="M0 20 L30 52 L60 20"></path>
							<path class="a3" d="M0 40 L30 72 L60 40"></path>
						</svg>
            <section style={{backgroundColor:'#1d2024'}}>

              <span><img src={Logo} className="logo-auv" alt="" height='50px'/></span>

              <span> <p className="a-head">SRM AUV</p> </span>

              <span className="type"> 
                <b>
                <Typical
                steps={["SRMIST's Official AUV Team", 1000, "Diving in to the depths", 1000, "Exploring the Deep Blue", 1000]}
                loop={Infinity}
                wrapper="p"
                />
                </b>
              </span>
              <video
                  autoPlay
                  muted
                  loop
                  style={{
                  position: "relative",
                  width: "100%",
                  left: 0,
                  top: 0,
                  bottom: 80,
                  borderRadius: '0 0 50%'
                  }}
              >
                <source src={Vid} type="video/mp4" />
              </video><div className="row"></div>


            </section>

            <section>
              
                <div class="m1 menu row">
                  <div className="col l3"><img src={Logo} class="fade nav-logo" alt="" height='50px'/></div>
                  <div className="col l5">
                  <div class="mmenu fade">
                    <div class="label">Follow Us</div>
                    <div class="spacer"></div>
                    <a href=""><div class="item"><span>Twitter</span></div></a>
                    <a href=""><div class="item"><span>Instagram</span></div></a>
                    <a href=""><div class="item"><span>Facebook</span></div></a>
                    <a href=""><div class="item"><span>Linkedin</span></div></a>
                    <a href=""><div class="item"><span>YouTube</span></div></a>
                  </div>
                  </div>
                  <div class="col l4 menu-ce right">
                      <ul className="fade">
                          <li><a class="active" href="#home">Home</a>

                          </li>
                          <li><a href="#portfolio">Portfolio</a>

                          </li>
                          <li><a href="#sponsor">Sponsors</a>

                          </li>
                          <li><a href="#contact">Contact</a>

                          </li>
                      </ul>
                  </div>
                </div>
<div id="home">
  <p className="center">Home</p>
  <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aliquam, explicabo, incidunt ipsam nemo eos assumenda impedit beatae exercitationem magnam quod consequatur earum aperiam? Libero repudiandae delectus, eveniet aliquid totam corporis!loremlorem  Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio, exercitationem, quasi dolores quas inventore recusandae suscipit libero animi optio assumenda quae architecto corrupti odit magnam, dicta sequi aut quidem voluptatum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde distinctio necessitatibus praesentium iusto eaque corrupti voluptates incidunt doloribus consequatur, blanditiis illo voluptas saepe consectetur minima sapiente debitis eum voluptate est. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odio officia eius nisi dolorum cumque facilis voluptatem consequatur temporibus vel labore, voluptatum similique perferendis eligendi a animi dignissimos laboriosam iste. Explicabo.</p>
</div>
<div id="sponsor"> <Sponsor/> </div>
<div id="portfolio">
<p className="center">Hhgvvhe</p>
  <p>Lorem ipsum dolor sit a lorem  lorem Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui unde laborum sed accusamus modi labore. Expedita quibusdam, nulla assumenda porro debitis animi exercitationem modi minus culpa recusandae autem illo perferendis! Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat, sit? Quam, iste aspernatur ea reprehenderit adipisci eum natus, omnis iure blanditiis nihil repellat hic corporis minima alias obcaecati, cupiditate incidunt! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda quidem voluptatum reprehenderit ipsam repellat ipsum laudantium, eius, itaque asperiores repellendus atque nostrum sit rem accusantium quo. Placeat recusandae nesciunt ducimus! Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere dolorem, voluptate porro incidunt accusamus debitis sequi at provident beatae itaque doloribus voluptatem ratione magni excepturi. Exercitationem nihil magni a necessitatibus. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat eum ad error, deleniti animi inventore nihil. Omnis esse id, quasi accusantium reprehenderit soluta iste, rem fugit dolor nostrum perspiciatis voluptatibus? met consectetur, adipisicing elit. Aliquam, explicabo, incidunt ipsam nemo eos assumenda impedit beatae exercitationem magnam quod consequatur earum aperiam? Libero repudiandae delectus, eveniet aliquid totam corporis!loremlorem  Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio, exercitationem, quasi dolores quas inventore recusandae suscipit libero animi optio assumenda quae architecto corrupti odit magnam, dicta sequi aut quidem voluptatum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde distinctio necessitatibus praesentium iusto eaque corrupti voluptates incidunt doloribus consequatur, blanditiis illo voluptas saepe consectetur minima sapiente debitis eum voluptate est. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odio officia eius nisi dolorum cumque facilis voluptatem consequatur temporibus vel labore, voluptatum similique perferendis eligendi a animi dignissimos laboriosam iste. Explicabo.</p>

</div>
<div id="contact"></div>
              
            </section>

            {/* <div className="row" style={{backgroundColor:'#1d1f20', padding:'10px'}}>
            <a href="" class="button glow-button">Glow Button</a><a class="cool-link" href="">A cool link</a>
            
            </div> */}

            
            {/* <section id="homee">
                  <div className="row center">HOME</div>
                  <div> <p style={{fontSize:'50px'}}>Lorem  Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint velit beatae deleniti mollitia quaerat reprehenderit quos illo iste, laborum maiores libero impedit? Ab sit soluta veritatis unde, molestiae ut voluptatum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, voluptas tempore. Hic aliquam beatae mollitia provident similique fugit dolorem odit sed, eum recusandae labore rerum harum. Nesciunt, iure? Earum, pariatur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis totam pariatur possimus quis exercitationem alias fugiat, dignissimos nostrum soluta! Molestiae alias aliquam quasi officiis dicta ullam mollitia! Aliquid, vero fugiat! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed illum nihil earum odit consectetur quod vel quia, facere, quam, porro itaque. Magni, iste quis? Dignissimos illum lorem Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae laborum esse quisquam dolorem vitae vel nam recusandae necessitatibus aspernatur minus, minima dolores quos reprehenderit unde porro qui? Hic, commodi repellat. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptate omnis a illo, ipsa corrupti saepe facilis architecto, quod molestiae, illum sed dolore voluptas officia optio modi doloremque consectetur eligendi dolor! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Explicabo vel perferendis eum ab aspernatur molestiae, dolor eveniet dolores recusandae pariatur sapiente quae ullam ea, molestias dolorem earum nisi? Quibusdam, dolorem. blanditiis culpa fuga consectetur? ipsum dolor sit amet consectetur adipisicing elit. Inventore, magni voluptatum enim eius nam corrupti tenetur ab aspernatur facere illum praesentium veniam a placeat molestiae incidunt quia cum iure iste.</p> </div>
            </section>

             <section id="about">
             <div className="row center">ABOUT</div>
                  <div> <p style={{fontSize:'50px'}}>Lorem  Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint velit beatae deleniti mollitia quaerat reprehenderit quos illo iste, laborum maiores libero impedit? Ab sit soluta veritatis unde, molestiae ut voluptatum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, voluptas tempore. Hic aliquam beatae mollitia provident similique fugit dolorem odit sed, eum recusandae labore rerum harum. Nesciunt, iure? Earum, pariatur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis totam pariatur possimus quis exercitationem alias fugiat, dignissimos nostrum soluta! Molestiae alias aliquam quasi officiis dicta ullam mollitia! Aliquid, vero fugiat! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed illum nihil earum odit consectetur quod vel quia, facere, quam, porro itaque. Magni, iste quis? Dignissimos illum lorem Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae laborum esse quisquam dolorem vitae vel nam recusandae necessitatibus aspernatur minus, minima dolores quos reprehenderit unde porro qui? Hic, commodi repellat. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptate omnis a illo, ipsa corrupti saepe facilis architecto, quod molestiae, illum sed dolore voluptas officia optio modi doloremque consectetur eligendi dolor! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Explicabo vel perferendis eum ab aspernatur molestiae, dolor eveniet dolores recusandae pariatur sapiente quae ullam ea, molestias dolorem earum nisi? Quibusdam, dolorem. blanditiis culpa fuga consectetur? ipsum dolor sit amet consectetur adipisicing elit. Inventore, magni voluptatum enim eius nam corrupti tenetur ab aspernatur facere illum praesentium veniam a placeat molestiae incidunt quia cum iure iste.</p> </div>
           
            </section>

            <section id="test">

            </section>
            <div className="justify"> <p style={{fontSize:'50px'}}>Lorem  Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint velit beatae deleniti mollitia quaerat reprehenderit quos illo iste, laborum maiores libero impedit? Ab sit soluta veritatis unde, molestiae ut voluptatum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, voluptas tempore. Hic aliquam beatae mollitia provident similique fugit dolorem odit sed, eum recusandae labore rerum harum. Nesciunt, iure? Earum, pariatur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis totam pariatur possimus quis exercitationem alias fugiat, dignissimos nostrum soluta! Molestiae alias aliquam quasi officiis dicta ullam mollitia! Aliquid, vero fugiat! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed illum nihil earum odit consectetur quod vel quia, facere, quam, porro itaque. Magni, iste quis? Dignissimos illum lorem Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae laborum esse quisquam dolorem vitae vel nam recusandae necessitatibus aspernatur minus, minima dolores quos reprehenderit unde porro qui? Hic, commodi repellat. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptate omnis a illo, ipsa corrupti saepe facilis architecto, quod molestiae, illum sed dolore voluptas officia optio modi doloremque consectetur eligendi dolor! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Explicabo vel perferendis eum ab aspernatur molestiae, dolor eveniet dolores recusandae pariatur sapiente quae ullam ea, molestias dolorem earum nisi? Quibusdam, dolorem. blanditiis culpa fuga consectetur? ipsum dolor sit amet consectetur adipisicing elit. Inventore, magni voluptatum enim eius nam corrupti tenetur ab aspernatur facere illum praesentium veniam a placeat molestiae incidunt quia cum iure iste.</p> </div>

     */}
</div>
    );
  }
}


{/*

CAROUSEL--------------------------
 <Slider { ...settings }>
          <div className="center carousel-h"> 
            <img src={C1} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h green">
          <img src={C2} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h red">
          <img src={C3} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h brown">
          <img src={C4} alt="" height='450px' width='100%'/>          
          </div>
          <div className="center carousel-h yellow">
          <img src={C5} alt="" height='450px' width='100%'/>          
          </div>
        </Slider> */}

        

       {/*
       
       tabs
       
        <div className="row" style={{marginTop:'-6px'}}>
          <div className="tabs-wrapper">
            <div id="tabs">
              
              <ul class="tabs #00838f cyan darken-3 tabs-transparent">
                <li class="col s2"><img src={Logo} alt="" height='50px'/></li>
                <li class="tab"><a className="active" href="#test1">Test 1</a></li>
                <li class="tab"><a href="#test2">Test 2</a></li>
                <li class="tab"><a href="#test3">Test 3</a></li>
                <li class="tab"><a href="#test4">Test 4</a></li>
              </ul>
            </div>
    <div id="test1" class="col s12">
      Test 1 
      <p class="flow-text">Don't forget to click the like button if you found this helpful.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus reprehenderit sapiente at est culpa dolor impedit, eius ea nihil molestiae doloribus quisquam ipsa possimus, aperiam earum, qui sunt libero placeat!
    </div>
    <div id="test2" class="col s12">
      Test 2
      <p class="flow-text">Don't forget to click the like button if you found this helpful.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias velit asperiores soluta consectetur quasi omnis consequatur sunt animi quaerat amet nemo autem tempora ut, voluptas similique, eos in cumque sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse similique dicta mollitia earum omnis asperiores ipsam facilis voluptas adipisci ad dolorum vel, accusamus, totam amet illo error. Quia, laboriosam, hic.</p>
    </div>
    <div id="test3" class="col s12">Test 3</div>
    <div id="test4" class="col s12">Test 4</div>
  </div> </div> */}
