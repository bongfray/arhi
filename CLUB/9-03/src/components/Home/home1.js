import React, { Component } from 'react'
import Vid from './back.mp4'
import $ from 'jquery';
import {} from 'materialize-css'
import M from 'materialize-css'
import Typical from 'react-typical'
import Logo from './auv.png'


// var Modernizr = require("modernizr");

export default class Home1 extends Component {

    render() {
        return (
 
            <React.Fragment>
            <div class="ct" id="t1">
                <div class="ct" id="t2">
                    <div class="ct" id="t3">
                        <div class="ct" id="t4">
                            <section>  
                                <ul>
                                    <a href="#t1"><li id="uno">  <i class="white material-icons">add</i></li></a>
                                    <a href="#t2"><li id="dos"><i className="white material-icons">announcement
</i> </li></a>
                                    <a href="#t3"><li id="tres"> <i class="white material-icons">add</i></li></a>
                                    <a href="#t4"><li id="cuatro"></li><i class="white material-icons">add</i></a>
                                </ul>
                                <div class="page" id="p1">
                                    <span><img src={Logo} className="logo-auv" alt="" height='50px'/></span>

                                    <span> <p className="a-head">SRM AUV</p> </span>

                                    <span className="type"> 
                                    <b>
                                    <Typical
                                    steps={["SRMIST's Official AUV Team", 1000, "Diving in to the depths", 1000, "Exploring the Deep Blue", 1000]}
                                    loop={Infinity}
                                    wrapper="p"
                                    />
                                    </b>
                                    </span>
                                    <video
                                        autoPlay
                                        muted
                                        loop
                                        style={{
                                        position: "relative",
                                        width: "100%",
                                        left: 0,
                                        top: 0,
                                        bottom: 80
                                        }}
                                    >
                                        <source src={Vid} type="video/mp4" />
                                    </video>                                </div>
                                <div class="page" id="p2">
                                    <li class="icon fa fa-keyboard-o"><span class="title">Type</span></li>
                                </div>  
                                <div class="page" id="p3">
                                    <li class="icon fa fa-coffee"><span class="title">Coffee</span></li>
                                </div>
                                <div class="page" id="p4">
                                <li class="icon fa fa-dribbble"><span class="title">Dribbble</span></li>
                                </div>  
                            </section>
       
     </div>
     </div>
   </div>
  </div>
</React.Fragment>  
        )
    }
}
