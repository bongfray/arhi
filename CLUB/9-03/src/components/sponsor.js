import React, { Component } from 'react'
import M from 'materialize-css';
import {} from 'materialize-css';

import S1 from './Sponsors/alind.jpg'
import S2 from './Sponsors/anekonnect.png'
import S3 from './Sponsors/blue.png'
import S4 from './Sponsors/ids.png'
import S5 from './Sponsors/navicom.png'
import S6 from './Sponsors/niot.jpg'
import S7 from './Sponsors/nvidia.png'
import S8 from './Sponsors/pcbpower.png'
import S9 from './Sponsors/rje1.png'
import S10 from './Sponsors/siic.jpg'
import S11 from './Sponsors/spartan.png'
import S12 from './Sponsors/solidworks.png'

export default class Sponsor extends Component {
    render() {
        return (
            
                <React.Fragment>
                <h1 className="center tag-head fade-content"> <span className="team-header1">S</span>ponsorship <span className="team-header2">D</span>etails</h1><br/>
                        <div className="row fade-content">
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S1} width="100%" style={{marginTop:'12%'}} title="ALIND" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S2} width="100%" style={{marginTop:'12%'}} title="Anekonnect" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S3} width="100%" style={{marginTop:'18%'}} title="Blue Robotics"alt=""/>
                            </div>
                        </div>
                       
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S4} width="100%" style={{marginTop:'17%'}} title="IDS" alt=""/>
                            </div>
                        </div>
                        </div>
                        <div className="row fade-content">
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S5} width="100%" title="Navicom" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S6} width="60%" height="100%" title="National Institute of Ocean Technology (Chennai)" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S7} width="100%" height="100%" title="NVIDIA" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S8} width="100%" height="100%" title="PCB Power" alt=""/>
                            </div>
                        </div>
                        </div>
                        <div className="row fade-content">
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S9} width="60%" height="100%" title="RJE" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S10} width="100%" height="100%" title="SRM Innovation and Incubation Centre" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S11} width="100%" style={{marginTop:'15%'}} title="Spartan" alt=""/>
                            </div>
                        </div>
                        <div className="col s12 l3 m3 center">
                            <div className="box">
                                <img src={S12} width="100%" style={{marginTop:'7%'}} title="Solid Works" alt=""/>
                            </div>
                        </div>
                    </div>
                </React.Fragment>

            )
    }
}
