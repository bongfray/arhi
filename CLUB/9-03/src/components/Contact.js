import React, { Component } from 'react'

import $ from 'jquery'

export default class Contact extends Component {
    constructor()
    {
        super()
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        $(function() {
            $('.material-card > .mc-btn-action').click(function () {
                var card = $(this).parent('.material-card');
                var icon = $(this).children('i');
                icon.addClass('fa-spin-fast');
    
                if (card.hasClass('mc-active')) {
                    card.removeClass('mc-active');
    
                    window.setTimeout(function() {
                        icon
                            .removeClass('fa-arrow-left')
                            .removeClass('fa-spin-fast')
                            .addClass('fa-bars');
    
                    }, 400);
                } else {
                    card.addClass('mc-active');
    
                    window.setTimeout(function() {
                        icon
                            .removeClass('fa-bars')
                            .removeClass('fa-spin-fast')
                            .addClass('fa-arrow-left');
    
                    }, 400);
                }
            });
        });
    }

    render() {
        return (

            <section class="team-details">
    
        <h1 className="center tag-head"> <span className="team-header1">T</span>eam <span className="team-header2">D</span>etails</h1><br/>

    <div class="row active-with-click">

    <div class="col l3 s12">
            <article class="material-card Red">
                <h2>
                    <span>Christopher Walken</span>
                    <strong>
                        The Deer Hunter
                    </strong>
                </h2>
                <div class="mc-content">
                    <div class="img-container">
                        <img class="img-responsive" src="http://u.lorenzoferrara.net/marlenesco/material-card/thumb-christopher-walken.jpg"/>
                    </div>
                    <div class="mc-description">
                        He has appeared in more than 100 films and television shows, including The Deer Hunter, Annie Hall, The Prophecy trilogy, The Dogs of War ...
                    </div>
                </div>
                <a class="mc-btn-action">
                <i className="fa fa-bars small" aria-hidden="true"></i>
                </a>
                <div class="mc-footer">
                    <h4>
                        Social Accounts
                    </h4>
                    <div className="center">
                    <a class="fa fa-fw fa-facebook"></a>
                    <a class="fa fa-fw fa-twitter"></a>
                    <a class="fa fa-fw fa-linkedin"></a>
                    <a class="fa fa-fw fa-google-plus"></a>
                    </div></div>
            </article>
        </div>

        <div class="col l3 s12">
            <article class="material-card Green">
                <h2>
                    <span>Christopher Walken</span>
                    <strong>
                        The Deer Hunter
                    </strong>
                </h2>
                <div class="mc-content">
                    <div class="img-container">
                        <img class="img-responsive" src="http://u.lorenzoferrara.net/marlenesco/material-card/thumb-christopher-walken.jpg"/>
                    </div>
                    <div class="mc-description">
                        He has appeared in more than 100 films and television shows, including The Deer Hunter, Annie Hall, The Prophecy trilogy, The Dogs of War ...
                    </div>
                </div>
                <a class="mc-btn-action">
                <i className="fa fa-bars small" aria-hidden="true"></i>
                </a>
                <div class="mc-footer">
                    <h4>
                        Social Accounts

                    </h4>
                    <a class="fa fa-fw fa-facebook"></a>
                    <a class="fa fa-fw fa-twitter"></a>
                    <a class="fa fa-fw fa-linkedin"></a>
                    <a class="fa fa-fw fa-google-plus"></a>
                </div>
            </article>
        </div>

        <div class="col l3 s12">
            <article class="material-card Pink">
                <h2>
                    <span>Sean Penn</span>
                    <strong>
                        <i class="fa fa-fw fa-star"></i>
                        Mystic River
                    </strong>
                </h2>
                <div class="mc-content">
                    <div class="img-container">
                        <img class="img-responsive" src="http://u.lorenzoferrara.net/marlenesco/material-card/thumb-sean-penn.jpg"/>
                    </div>
                    <div class="mc-description">
                        He has won two Academy Awards, for his roles in the mystery drama Mystic River (2003) and the biopic Milk (2008). Penn began his acting career in television with a brief appearance in a 1974 episode of Little House on the Prairie ...
                    </div>
                </div>
                <a class="mc-btn-action">
                <i className="fa fa-bars small" aria-hidden="true"></i>
                </a>
                <div class="mc-footer">
                    <h4>
                                                Social Accounts

                    </h4>
                    <a class="fa fa-fw fa-facebook"></a>
                    <a class="fa fa-fw fa-twitter"></a>
                    <a class="fa fa-fw fa-linkedin"></a>
                    <a class="fa fa-fw fa-google-plus"></a>
                </div>
            </article>
        </div>
        <div class="col l3 s12">
            <article class="material-card Purple">
                <h2>
                    <span>Clint Eastwood</span>
                    <strong>
                        Million Dollar Baby
                    </strong>
                </h2>
                <div class="mc-content">
                    <div class="img-container">
                        <img class="img-responsive" src="http://u.lorenzoferrara.net/marlenesco/material-card/thumb-clint-eastwood.jpg"/>
                    </div>
                    <div class="mc-description">
                        He rose to international fame with his role as the Man with No Name in Sergio Leone's Dollars trilogy of spaghetti Westerns during the 1960s ...
                    </div>
                </div>
                <a class="mc-btn-action">
                    <i className="fa fa-bars small" aria-hidden="true"></i>
                </a>
                <div class="mc-footer">
                    <h4>
                                                Social Accounts

                    </h4>
                    <a class="fa fa-fw fa-facebook"></a>
                    <a class="fa fa-fw fa-twitter"></a>
                    <a class="fa fa-fw fa-linkedin"></a>
                    <a class="fa fa-fw fa-google-plus"></a>
                </div>
            </article>
        </div>

        
        </div>
        <div className="row">
    </div>
</section>
        )
    }
}
