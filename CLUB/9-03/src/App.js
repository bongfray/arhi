import React, { Component } from 'react';
import { Route, Switch} from 'react-router-dom'
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';


import Home from './components/Home/home';
import Contact from './components/Contact';
// import About fro./components/sponsorout'
import Er from './components/error'


class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">

         <Switch>

        <Route exact path="/" component={Home} />
        <Route exact path="/contact" component={Contact} />
        {/* <Route exact path="/about" component={Sponsor} /> */}
        <Route exact path="/err" component={Er} />

        

        </Switch>
      
      </div>
    );
  }
}

export default App;
