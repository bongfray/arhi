import React, { Component } from 'react'
import $ from 'jquery';
import Axios from 'axios';

export default class ContactUs extends Component {
    constructor()
    {
        super()
        this.state={
            name:'',
            message:'',
            mailid:'',
            contactdetails:'',
            fetching:true,
        }
    }
    componentDidMount(){
        $(document).ready(function(){
            //button click event
            $("#btn").click(function(){
              // pushed form down
              $(".form").css("margin-top", "6px");
              // folds down envelope top
               $(".top").css({
                "transform": "rotatex(0deg)",
                "z-index": "3"
              });
              // rotates envelope
              $(".envelope").css("transform", "rotatey(180deg)");
              // stores first name text
              var ipt = document.getElementById('first').value;
              // enters text in to div
              $( "#name" ).html( ipt );
              // adds box shadow to front of envelope
              $(".envelope__bottom--front").css("box-shadow", "0 0 30px black");
            });
          });
          this.fetchDetails();
    }

    fetchDetails=()=>{
        Axios.post('/user/fetch_contact_details',{
            action: "Contact Details",
          })
          .then(response =>{
            //console.log(response.data)
            this.setState({
             contactdetails: response.data.found,
             fetching:false,
           })
          })

    }

    setData=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    sendContactInfo=(e)=>{
        if(!this.state.name || !this.state.message || !this.state.mailid)
        {
            window.M.toast({html:'Enter all the details',classes:'red rounded'})
        }
        else{
            window.M.toast({html:'Sending !!',classes:'orange rounded'})
            Axios.post('/user/contactUs',{data:this.state})
            .then(res=>{
                if(res.data)
                {
                    window.M.toast({html:'Sent !!',classes:'green darken-1 rounded'})
                }
            })
            .catch(err=>{
                window.M.toast({html:'Soething Went Wrong !!',classes:'red rounded'})
            })
        }
    }

    render() {
        if(this.state.fetching)
        {
            return(
                <div className="center">Loading...</div>
            )
        }
        else{
        return (
            <React.Fragment>

            <div className="row loading2 wave2" data-aos='slide-up'>
                Contact Us
            </div>

            <div className="row mob-c">
                <div className="col s12">
                        <ul className="social-media-list">
                        {this.state.contactdetails.length>0 &&
                        <React.Fragment>
                                <a href={this.state.contactdetails[0].twitter} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-twitter" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].insta} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-instagram" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].fb} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-facebook" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].linkedin} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-linkedin" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].youtube} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-youtube" aria-hidden="true"></i></a>

                        </React.Fragment>
                        }


                        </ul>

                </div>

                <div className="col s12">
                    <div className="letter" data-aos="fade-up">
                                <div className="input-field col s12">
                                    <input id="mname1" type="text" name="name" onChange={this.setData}  className="validate" required />
                                    <label htmlFor="mname1" className="label-c">Name</label>
                                </div>
                                <div className="input-field col s12">
                                    <input id="mmailid1" type="text" name="mailid" onChange={this.setData} className="validate" required />
                                    <label htmlFor="mmailid1" className="label-c">E-Mail</label>
                                </div>
                                <div className="input-field col s12">
                                    <textarea id="mmessage1" cols="60" name="message" onChange={this.setData} className="materialize-textarea txt-c"></textarea>
                                    <label htmlFor="mmessage1" className="label-c">Message</label>
                                </div>
                                <div className="col s12">
                                  <button  style={{width:'100%',backgroundColor:'#37474F'}} className="btn btn-small send-c white-text"
                                  onClick={this.sendContactInfo}>Send</button>
                                </div>
                    </div>
                </div>

                <div className="col s1"></div>
                <div className="col s10">

                <ul className="contact-list">
                        <li data-aos='zoom-out' className="list-item"><i className="fa fa-map-marker fa-2x">
                            <span className="contact-text place">SRM IST</span></i></li>
                            {this.state.contactdetails.length>0 &&
                             <React.Fragment>
                        <li data-aos='zoom-out' className="list-item">
                            <i className="fa fa-phone fa-2x"><span className="contact-text phone">
                                <a
                                href={"tel:"+this.state.contactdetails[0].phone}
                                title="Give me a call">
                                    {this.state.contactdetails[0].phone}
                                    </a></span></i></li>

                        <li data-aos='zoom-out' className="list-item"><i className="fa fa-envelope fa-2x">
                            <span className="contact-text gmail">
                                <a href={"mailto:"+this.state.contactdetails[0].gmail}
                                 title="Send me an email">{this.state.contactdetails[0].gmail}</a></span></i></li>
                        </React.Fragment>
                        }
                    </ul>
                </div>
                <div className="col s1"></div>
            </div>

            <div className="row show-on-medium hide-on-med-and-up hide-on-med-and-down">
                <div className="col m8 center">
                    <ul className="social-media-list">
                        {this.state.contactdetails.length>0 &&
                        <React.Fragment>
                                <a href={this.state.contactdetails[0].twitter} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-twitter" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].insta} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-instagram" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].fb} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-facebook" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].linkedin} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-linkedin" aria-hidden="true"></i></a>



                                <a href={this.state.contactdetails[0].youtube} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                                <i className="fa fa-youtube" aria-hidden="true"></i></a>

                        </React.Fragment>
                        }

                        </ul>
                        <div className="letter" data-aos="fade-up" style={{height:'100%'}}>
                                <div className="input-field col s12">
                                    <input id="mname" type="text" name="name" onChange={this.setData}  className="validate" required />
                                    <label htmlFor="mname" className="label-c">Name</label>
                                </div>
                                <div className="input-field col s12">
                                    <input id="mmailid" type="text" name="mailid" onChange={this.setData} className="validate" required />
                                    <label htmlFor="mmailid" className="label-c">E-Mail</label>
                                </div>
                                <div className="input-field col s12">
                                    <textarea id="mmessage" cols="60" name="message" onChange={this.setData} className="materialize-textarea txt-c"></textarea>
                                    <label htmlFor="mmessage" className="label-c">Message</label>
                                </div>
                                <div className="col s12">
                                  <button  style={{width:'100%',backgroundColor:'#37474F'}} className="btn btn-small send-c white-text"
                                  onClick={this.sendContactInfo}>Send</button>
                                </div>
                        </div>
                    </div>
                    <div className="col m4">
                        <ul className="contact-list">
                        <li data-aos='zoom-out' className="list-item"><i className="fa fa-map-marker fa-2x">
                            <span className="contact-text place">SRM IST</span></i></li>
                            {this.state.contactdetails.length>0 &&
                             <React.Fragment>
                        <li data-aos='zoom-out' className="list-item">
                            <i className="fa fa-phone fa-2x"><span className="contact-text phone">
                                <a
                                href={"tel:"+this.state.contactdetails[0].phone}
                                title="Give me a call">
                                    {this.state.contactdetails[0].phone}
                                    </a></span></i></li>

                        <li data-aos='zoom-out' className="list-item"><i className="fa fa-envelope fa-2x">
                            <span className="contact-text gmail">
                                <a href={"mailto:"+this.state.contactdetails[0].gmail}
                                 title="Send me an email">{this.state.contactdetails[0].gmail}</a></span></i></li>
                        </React.Fragment>
                        }
                    </ul>
                </div>
            </div>


            <div className="row desk-c hide-on-med-and-down">
                <div className="col l3 m3">
                    <ul className="social-media-list">
                    {this.state.contactdetails.length>0 &&
                        <React.Fragment>
                        <div className="col l12">
                            <a href={this.state.contactdetails[0].twitter} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                            <i className="fa fa-twitter" aria-hidden="true"></i></a>

                        </div>
                        <div className="col l12">
                            <a href={this.state.contactdetails[0].insta} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                            <i className="fa fa-instagram" aria-hidden="true"></i></a>

                        </div>
                        <div className="col l12">
                            <a href={this.state.contactdetails[0].fb} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                            <i className="fa fa-facebook" aria-hidden="true"></i></a>

                        </div>
                        <div className="col l12">
                            <a href={this.state.contactdetails[0].linkedin} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                            <i className="fa fa-linkedin" aria-hidden="true"></i></a>

                        </div>
                        <div className="col l12">
                            <a href={this.state.contactdetails[0].youtube} target="_blank" rel="noopener noreferrer"  className="contact-icon" data-aos="fade-up">
                            <i className="fa fa-youtube" aria-hidden="true"></i></a>

                        </div>
                     </React.Fragment>
                     }
                    </ul>
                </div>

                <div className="col l6 hide-on-med-and-down">
                    <div className="envelope" title="">
                        <div className="back"></div>
                            <div className="letter">
                                <div className="input-field col s12">
                                    <input id="name" type="text" name="name" onChange={this.setData} className="validate" required />
                                    <label htmlFor="name" className="label-c">Name</label>
                                </div>
                                <div className="input-field col s12">
                                    <input id="mailid" type="text" name="mailid" onChange={this.setData} className="validate" required />
                                    <label htmlFor="mailid" className="label-c">E-Mail</label>
                                </div>
                                <div className="input-field col s12">
                                    <textarea id="message" cols="60" name="message" onChange={this.setData} className="materialize-textarea txt-c"></textarea>
                                    <label htmlFor="message" className="label-c">Message</label>
                                </div>
                                <div className="col s12">
                                  <button style={{width:'100%',backgroundColor:'#37474F'}} className="btn btn-small send-c white-text"
                                  onClick={this.sendContactInfo}>Send</button>
                                </div>
                            </div>
                    </div>
                </div>




                <div className="col l3 m3">
                    <ul className="contact-list">
                        <li data-aos='zoom-out' className="list-item"><i className="fa fa-map-marker fa-2x">
                            <span className="contact-text place">SRM IST</span></i></li>
                            {this.state.contactdetails.length>0 &&
                             <React.Fragment>
                                <li data-aos='zoom-out' className="list-item"><i className="fa fa-phone fa-2x">
                                    <span className="contact-text phone"><a href={"tel:"+this.state.contactdetails[0].phone}
                                    title="Give me a call">{this.state.contactdetails[0].phone}</a></span></i></li>

                                <li data-aos='zoom-out' className="list-item"><i className="fa fa-envelope fa-2x">
                                    <span className="contact-text gmail"><a href={"mailto:"+this.state.contactdetails[0].gmail}
                                    target="_blank" rel="noopener noreferrer"  title="Send me an email">{this.state.contactdetails[0].gmail}</a></span></i></li>
                            </React.Fragment>
                                }
                    </ul>


                </div>


            </div>

            </React.Fragment>
        )
      }
    }
}
