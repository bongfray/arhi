import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';

 
export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      uploading:false,
      loaded_percent:0,
      username:'',
      redirectTo:'',
      option:'',
      file:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
 M.AutoInit();
}
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
       this.setState({disable_btn:true})
     let apiUrl;
       apiUrl = '/user/uploadD';
     const formData = new FormData();
     formData.append('myImage',data.file);
     formData.append('spons_name',data.name);
     formData.append('serial',data.serial);
     window.M.toast({html: 'Uploading ....!!',classes:'orange rounded'});
     axios.post(apiUrl,formData,{
         onUploadProgress:ProgressEvent=>{
              var loaded = Math.round(ProgressEvent.loaded/ProgressEvent.total*100);
              this.setState({uploading:true,loaded_percent:loaded})
         }
     })
         .then(response => {
           if(response.data === 'have')
           {
            window.M.toast({html: 'Already There !!',classes:'pink rounded'});
            this.setState({disable_btn:false})
           }
           else{
            window.M.toast({html: 'Saved !!',classes:'green darken-1 rounded'});
            this.setState({
              uploading:false,
              loaded_percent:0,
               response: response.data,
               isAddProduct: false,
               isEditProduct: false,
               disable_btn:false,
             })
           }

         })
         .catch(err=>{
             this.setState({loading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetch_to_edit_sponsor',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })
         .catch(err=>{
             this.setState({loading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Sponsor Name",
          name: "name",
          placeholder: "Enter the name of the Sponsor",
          type: "text",
          size:false,
        },
        {
          header: "Uploaded Photo",
          name: "photo",
          placeholder: "none",
          type: "text",
        },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct loaded_percent={this.state.loaded_percent} cancel={this.updateState} username={this.state.username}
     action={"Sponsors"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading)
    {
      return(
         <div>Loading...</div>
      );
    }
    else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Sponsors Here</h5>
                    <br />
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                    <button className="btn right blue-grey darken-2 sup subm"
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                        {!this.state.isAddProduct &&
                            <ProductList image={true} username={this.props.username} title={this.state.option}
                            action={"Sponsors"} data={data}  editProduct={this.editProduct}/>
                        }<br />

                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
        }
}
  }
}
