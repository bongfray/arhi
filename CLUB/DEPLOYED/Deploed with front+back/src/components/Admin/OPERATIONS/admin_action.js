import React, { Component } from 'react'
import Axios from 'axios';



export default class SSSS extends Component {
    constructor()
    {
        super()
        this.state={
            status:false,
            loading:true,
            sponsor:0,
            team:0,
            alumni:0,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchStat();
        this.fetchSponsor();
    }
    fetchStat=()=>{
        Axios.post('/user/admin_action',{action:'signup_stat'})
        .then(res=>{
            if(!(res.data==='no'))
            {
                this.setState({status:res.data.status})
            }
        })
    }
    setSignup=(e)=>{
        this.setState({action:e.target.value,status:!this.state.status})
        Axios.post('/user/setSignup',{action:e.target.value,status:!this.state.status})
        .then(res=>{

        })
    }

    fetchSponsor=()=>{
        Axios.post('/user/fetch_sponsor',{action:'Sponsors'})
        .then(res=>{
            this.setState({sponsor:res.data.found.length})
            this.fetchTeam();
        })
        .catch(err=>{
            this.setState({loading:false})
            if(err.response.status === 404)
            {
              window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
            }
            else
            {
              window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
            }
        })

    }
    fetchTeam=()=>{
        Axios.post('/user/fetch_team',{action:'Team Member'})
        .then(res=>{
            let alumni = (res.data.found.filter(item=>item.r_type === 'Alumni')).length;
            this.setState({team:res.data.found.length-alumni,alumni:alumni,loading:false})
        })
        .catch(err=>{
            this.setState({loading:false})
            if(err.response.status === 404)
            {
              window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
            }
            else
            {
              window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
            }
        })
    }

    render()
    {
        if(this.state.loading)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Loading....
                </h5>
                </React.Fragment>
            )
        }
        else
        {
        return(
            <React.Fragment>
                <div className="row">
                    <div className="col l4 xl4 s12 m4 card">
                        Render Signup Page -
                            <p>
                                <label>
                                    <input checked={this.state.status} type="checkbox" value="signup_stat"
                                    onChange={this.setSignup}/>
                                    <span>Status</span>
                                </label>
                            </p>
                    </div>
                </div>
                <div className="row"><h5 className="">Details of the System</h5></div>
                <div className="row">
                    <div className="col l4 xl4 s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Sponsors</span>
                                <h4 className="center">{this.state.sponsor}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col l4 xl4 s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Team Members</span>
                                <h4 className="center">{this.state.team}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col l4 xl4 s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Alumnis</span>
                                <h4 className="center">{this.state.alumni}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
      }
    }
}
