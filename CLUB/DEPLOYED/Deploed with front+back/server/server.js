const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const session = require('express-session')
const mongoose = require('mongoose')
const dbConnection = require('./database')
const MongoStore = require('connect-mongo')(session)
const passport = require('./passport/fac_pass');
const http = require("http");
const path = require("path");
const socketIo = require("socket.io");

const app = express()
const PORT = process.env.PORT || 8080


const user = require('./routes/user')


app.use(morgan('dev'))
app.use(
	bodyParser.urlencoded({
		extended: true
	})
)
app.use(bodyParser.json())

// Sessions
app.use(
	session({
		secret: 'fraggle-rock',
		store: new MongoStore({ mongooseConnection: dbConnection}),
		resave: false,
		saveUninitialized: false,
		maxAge: Date.now() + (86400020)
	})
)



// Passport
app.use(passport.initialize())
app.use(passport.session()) // calls the deserializeUser


// Routes
app.use('/user', user)


if (process.env.NODE_ENV === 'production') {
	app.use(express.static(path.join(__dirname, '../build')));
}



app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'))
});

let c=0;

const server = http.createServer(app,()=>{
  console.log(c++);
});
const io = socketIo(server);

const serve = server.listen(PORT, () => console.log('App listening on port 8080!'));

process.on('SIGINT', () => {
  console.log('Gracefully Shutting Down !!');
  serve.close(() => {
    console.log('Http server closed.');
    mongoose.connection.close(false, () => {
      console.log('MongoDb connection closed.');
      process.exit(0);
    });
  });
});
