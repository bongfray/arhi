const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const saltRounds = 10;

const userSchema = new Schema({
username: { type: String, unique: false, required: false },
password: { type: String, unique: false, required: false },
mailid: { type: String, unique: false, required: false },
resetPasswordToken:{ type: String, unique: false, required: false },
resetPasswordExpires:{ type: String, unique: false, required: false },
ip:{ type: String, unique: false, required: false },
status:{ type: Boolean, unique: false, required: false },
expires:{ type: Number, unique: false, required: false },
})


userSchema.pre('save', function(next) {
  if (this.isNew || this.isModified('password')) {
    const document = this;
    bcrypt.hash(this.password, saltRounds, function(err, hashedPassword) {
      if (err) {
        next(err);
      } else {
        document.password = hashedPassword;
        next();
      }
    });
  } else {
    next();
  }
});

userSchema.methods.isCorrectPassword = function(password, callback) {
  bcrypt.compare(password, this.password, function(err, same) {
    if (err) {
      callback(err);
    } else {
      callback(err, same);
    }
  });
}

const User = mongoose.model('User', userSchema)
module.exports = User