import React, { Component } from 'react'
import Axios from 'axios';
import AOS from 'aos';
import 'aos/dist/aos.css';
import {Carousel} from 'react-materialize';
import {API_URL} from '../../utils/apiUrl'

export default class Contact extends Component {
    constructor()
    {
        super()
        this.state={
            faculty:'',
            vice_captain:'',
            captain:'',
            mechanical:'',
            web:'',
            electrical:'',
            software:'',
            corporate:'',
            alumni:'',
            fetching:true,
            moving_icon:'',
            active:'',
            clicked:false,

            visible_alumni: 8,
            start_alumni:0,
            restore_start_alumni:0,
            restore_next_alumni:0,

            visible_mech: 8,
            start_mech:0,
            restore_start_mech:0,
            restore_next_mech:0,

            visible_soft: 8,
            start_soft:0,
            restore_start_soft:0,
            restore_next_soft:0,

            visible_corp: 8,
            start_corp:0,
            restore_start_corp:0,
            restore_next_corp:0,

            visible_web: 8,
            start_web:0,
            restore_start_web:0,
            restore_next_web:0,

            visible_electrical: 8,
            start_electrical:0,
            restore_start_electrical:0,
            restore_next_electrical:0,
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
      AOS.init({
        duration : 1500,
      })
        this.fetchTeam();
    }

    loadMoreMech=()=> {
            this.setState((prev) => {
                return {restore_start_mech:prev.start_mech,restore_next_mech:prev.visible_mech,start_mech:prev.visible_mech,
                    visible_mech: prev.visible_mech + 8};
              });
      }
      loadPrevMech=()=> {
        this.setState((prev) => {
            return {start_mech:this.state.restore_start_mech,visible_mech: this.state.restore_next_mech,
                restore_start_mech:this.state.restore_start_mech-8,restore_next_mech:this.state.restore_next_mech-8};
          });
      }

      loadMoreSoft=()=> {
        this.setState((prev) => {
            return {restore_start_soft:prev.start_soft,restore_next_soft:prev.visible_soft,start_soft:prev.visible_soft,
                visible_soft: prev.visible_soft + 8};
          });
  }
  loadPrevSoft=()=> {
    this.setState((prev) => {
        return {start_soft:this.state.restore_start_soft,visible_soft: this.state.restore_next_soft,
            restore_start_soft:this.state.restore_start_soft-8,restore_next_soft:this.state.restore_next_soft-8};
      });
  }

   loadMoreCorp=()=> {
        this.setState((prev) => {
            return {restore_start_corp:prev.start_corp,restore_next_corp:prev.visible_corp,start_corp:prev.visible_corp,
                visible_corp: prev.visible_corp + 8};
        });
    }
    loadPrevCorp=()=> {
        this.setState((prev) => {
            return {start_corp:this.state.restore_start_corp,visible_corp: this.state.restore_next_corp,
                restore_start_corp:this.state.restore_start_corp-8,restore_next_corp:this.state.restore_next_corp-8};
        });
    }
loadMoreWeb=()=> {
    this.setState((prev) => {
        return {restore_start_web:prev.start_web,restore_next_web:prev.visible_web,start_web:prev.visible_web,
            visible_web: prev.visible_web + 8};
      });
}
loadPrevWeb=()=> {
    this.setState((prev) => {
        return {start_web:this.state.restore_start_web,visible_web: this.state.restore_next_web,
            restore_start_web:this.state.restore_start_web-8,restore_next_web:this.state.restore_next_web-8};
    });
}

loadMoreElectrical=()=> {
    this.setState((prev) => {
        return {restore_start_electrical:prev.start_electrical,restore_next_electrical:prev.visible_electrical,start_electrical:prev.visible_electrical,
            visible_electrical: prev.visible_electrical + 8};
      });
}
loadPrevElectrical=()=> {
    this.setState((prev) => {
        return {start_electrical:this.state.restore_start_electrical,visible_electrical: this.state.restore_next_electrical,
            restore_start_electrical:this.state.restore_start_electrical-8,restore_next_electrical:this.state.restore_next_electrical-8};
    });
}
    loadMoreAlumni=()=> {
        this.setState((prev) => {
            return {restore_start_alumni:prev.start_alumni,restore_next_alumni:prev.visible_alumni,start_alumni:prev.visible_alumni,
                visible_alumni: prev.visible_alumni + 8};
        });
    }
    loadPrevAlumni=()=> {
    this.setState((prev) => {
        return {start_alumni:this.state.restore_start_alumni,visible_alumni: this.state.restore_next_alumni,
            restore_start_alumni:this.state.restore_start_alumni-8,restore_next_alumni:this.state.restore_next_alumni-8};
    });
    }
    settedClass=(num,index)=>{
        if(this.state.active === num && this.state.clicked === num)
        {
          if(index % 2 === 0)
          {
            return 'Blue mc-active';
          }
          else{
            return 'Amber mc-active';
          }

        }
        else
        {
          if(index % 2 === 0)
          {
            return 'Blue';
          }
          else{
            return 'Amber';
          }
            return '';
        }
    }
    setSpin=(ins)=>{
            this.setState({clicked:ins,active:ins})
    }

    moveIcon=(incom)=>{
        if((this.state.active === incom))
        {
            if(this.state.clicked === incom)
            {
                return 'fa-user';
            }
            else{
                return 'fa-bars';
            }

        }
        else
        {
            return 'fa-bars';
        }
    }

    fetchTeam=()=>{
        Axios.post(API_URL+'/user/fetch_team',{
            action: 'Team Member',
          })
          .then(response =>{
              var faculty = response.data.found.filter(item=>item.r_type === 'Faculty');
              var captain = response.data.found.filter(item=>item.r_type === 'Captain of SRM AUV Team');
              var vice_captain = response.data.found.filter(item=>item.r_type === 'Vice-Captain of SRM AUV Team');
              let alumni = response.data.found.filter(item=>item.r_type === 'Alumni');
              var member = response.data.found.filter(item=>item.r_type === 'Member');
              var mechanical =member.filter(item=>item.domain ==='Mechanical');
              var software =member.filter(item=>item.domain ==='Software');
              var electrical =member.filter(item=>item.domain ==='Electrical');
              var corporate =member.filter(item=>item.domain ==='Corporate');
              var web =member.filter(item=>item.domain ==='Web');

            this.setState({
             faculty:faculty,
             captain:captain,
             vice_captain:vice_captain,
             mechanical:mechanical,
             software:software,
             electrical:electrical,
             corporate:corporate,
             web:web,
             alumni:alumni,
             fetching:false,
             rFiles:response.data.files,
           })
          })
    }

    render() {
 if(this.state.fetching)
 {
     return(
     <React.Fragment>
        <div className="center">
            <div className="box-frames" style={{margin:'20px'}}>
                <div className="mloader-39"></div>
            </div>
        </div>
        <h5 className="center">
            Fetching Team Details....
        </h5>
        </React.Fragment>
        )
 }
 else
 {

   var { rFiles } = this.state;
   function setImage(data)
   {
     var data =rFiles.filter(item=>item.filename === data);
     return data[0].filename;
   }
        return (
            <React.Fragment>
              <svg className="arrows">
                <path className="a1" d="M0 0 L30 32 L60 0"></path>
                <path className="a2" d="M0 20 L30 52 L60 20"></path>
                <path className="a3" d="M0 40 L30 72 L60 40"></path>
              </svg>
                <div className="gallery">
                        <div className="gallery-image" style={{height:'100vh'}}>
                            <img alt="Whole Team" src={require('./team.jpg')} style={{height:'100%',width:'100%',objectFit:'cover'}} />
                                <div className="gallery-text">
                                <h1 className="hey"><span>M</span><span>E</span><span>E</span><span>T</span><span> </span><span>O</span><span>U</span><span>R</span><span> </span><span>T</span><span>E</span><span>A</span><span>M</span></h1>                                </div>
                        </div>
                    </div>



            <section className="team-details">


            {this.state.faculty.length>0 &&
                <React.Fragment>
                  <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                    <h5>Faculty Member</h5>
                    <div className="line"></div>
                  </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                          <div className="col l3 xl3 m3"/>
                                {this.state.faculty.map((content,index)=>{
                                    return(
                                            <div className="col l3 xl3 m3" key={index}>
                                                    <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                          <h2>
                                                              <span>{content.name}</span>
                                                              <strong>
                                                               Faculty Member
                                                              </strong>
                                                          </h2>
                                                            <div className="mc-content">
                                                                <div className="img-container">
                                                                    <img alt="team" className="img-responsive"
                                                                     src={!content.photo ? require('./t2.png')
                                                                     :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                </div>
                                                                <div className="mc-description">
                                                                   {content.about_member}
                                                                </div>
                                                            </div>
                                                            <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                            <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                            </div>
                                                            <div className="mc-footer">
                                                                <h4>
                                                                    Social Accounts
                                                                </h4>
                                                                <div className="center">
                                                                  {content.github &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                  {content.linkedin && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                  {content.gmail &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                </div></div>
                                                        </article>
                                            </div>
                                    )
                                })}
                            <div className="col l3 xl3 m3"/>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.faculty.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.name}<br />
                                             {content.about_member}
                                            </span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                          {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}

                                </Carousel>
                                  </div>
                          </div>
                    </React.Fragment>
              }

                {this.state.alumni.length>0 &&
                  <React.Fragment>
                    <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                      <h5>Alumni</h5>
                      <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                    <div className="col l1 xl1 m1">
                            {this.state.start_alumni>0 &&
                                <button data-aos='slide-right' onClick={this.loadPrevAlumni}  style={{marginTop:'100%'}} type="button"
                                 className=" btn btn-floating btn-large pink">
                                <i className="material-icons small">chevron_left</i>
                                </button>
                            }
                        </div>
                        <div className="col l10 xl10 m10">
                            <div className="row">
                                    {this.state.alumni.slice(this.state.start_alumni, this.state.visible_alumni).map((content,index)=>{
                                        return(
                                            <div className="col l3 xl3 m3 s12" key={index}>
                                                    <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                            <h2>
                                                                <span>{content.name}</span>
                                                            </h2>
                                                            <div className="mc-content">
                                                                <div className="img-container">
                                                                    <img alt="team" className="img-responsive"
                                                                     src={!content.photo ? require('./t2.png')
                                                                     :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                </div>
                                                                <div className="mc-description">
                                                                   {content.alumni_journey}
                                                                </div>
                                                            </div>
                                                            <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                            <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                            </div>
                                                            <div className="mc-footer">
                                                                <h4>
                                                                    Social Accounts
                                                                </h4>
                                                                <div className="center">
                                                                {content.github &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                {content.linkedin && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                {content.gmail &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                </div></div>
                                                        </article>
                                            </div>
                                        )
                                    })}
                        </div>
                        </div>
                        <div className="col l1 xl1 m1">
                          {this.state.visible_alumni < this.state.alumni.length &&
                            <button  onClick={(this.loadMoreAlumni)} style={{marginTop:'100%'}} type="button" className=" btn btn-floating btn-large pink">
                              <i className="large material-icons">chevron_right</i>
                            </button>
                          }
                        </div>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.alumni.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.name}
                                            </span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                          {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}
                                </Carousel>
                                  </div>
                          </div>

                    </React.Fragment>
                  }

                  {((this.state.captain.length>0) || (this.state.vice_captain>0)) &&
                    <React.Fragment>
                    <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                      <h5>Team Head</h5>
                      <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                      <div className="col l3 xl3 m2"/>
                       {((this.state.captain.length>0)) &&
                         <div className="col l3 xl3 m4">
                            <article className={"material-card "+this.settedClass(this.state.captain[0].serial,this.state.captain[0].serial)}>
                                                            <h2>
                                                                <span>{this.state.captain[0].name}</span>
                                                                <strong>
                                                                    {this.state.captain[0].r_type}
                                                                </strong>
                                                            </h2>
                                                            <div className="mc-content">
                                                                <div className="img-container">
                                                                    <img alt="team" className="img-responsive"
                                                                     src={!this.state.captain[0].photo ? require('./t2.png')
                                                                     :API_URL+'/user/admin_panel/:'+setImage(this.state.captain[0].photo)}/>
                                                                </div>
                                                                <div className="mc-description">
                                                                 {this.state.captain[0].about_member &&
                                                                   <React.Fragment>{this.state.captain[0].about_member}</React.Fragment>
                                                                 }
                                                                </div>
                                                            </div>
                                                            <div className="mc-btn-action" onClick={()=>this.setSpin(this.state.captain[0].serial)}>
                                                            <i className={"fa small "+this.moveIcon(this.state.captain[0].serial)} aria-hidden="true"></i>
                                                            </div>
                                                            <div className="mc-footer">
                                                                <h4>
                                                                    Social Accounts
                                                                </h4>
                                                                <div className="center">
                                                                {!this.state.captain[0].github  &&  <a href={this.state.captain[0].github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                {!this.state.captain[0].linkedin && <a href={this.state.captain[0].linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                {!this.state.captain[0].gmail &&  <a href={"mailto:"+this.state.captain[0].gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                </div></div>
                            </article>

                        </div>}
                        <div className="col l3 xl3 m4">
                            {((this.state.vice_captain.length>0)) &&
                              <article className={"material-card "+this.settedClass(this.state.vice_captain[0].serial,this.state.vice_captain[0].serial)}>
                                                              <h2>
                                                                  <span>{this.state.vice_captain[0].name}</span>
                                                                  <strong>
                                                                      {this.state.vice_captain[0].r_type}
                                                                  </strong>
                                                              </h2>
                                                              <div className="mc-content">
                                                                  <div className="img-container">
                                                                      <img alt="team" className="img-responsive"
                                                                       src={!this.state.vice_captain[0].photo ? require('./t2.png')
                                                                       :API_URL+'/user/admin_panel/:'+setImage(this.state.vice_captain[0].photo)}/>
                                                                  </div>
                                                                  <div className="mc-description">
                                                                   {this.state.vice_captain[0].about_member &&
                                                                     <React.Fragment>{this.state.vice_captain[0].about_member}</React.Fragment>
                                                                   }
                                                                  </div>
                                                              </div>
                                                              <div className="mc-btn-action" onClick={()=>this.setSpin(this.state.vice_captain[0].serial)}>
                                                              <i className={"fa small "+this.moveIcon(this.state.vice_captain[0].serial)} aria-hidden="true"></i>
                                                              </div>
                                                              <div className="mc-footer">
                                                                  <h4>
                                                                      Social Accounts
                                                                  </h4>
                                                                  <div className="center">
                                                                  {!this.state.captain[0].github &&  <a href={this.state.captain[0].github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                  {!this.state.captain[0].linkedin && <a href={this.state.captain[0].linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                  {!this.state.captain[0].gmail &&  <a href={"mailto:"+this.state.captain[0].gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                  </div></div>
                              </article>
                            }
                          </div>
                        <div className="col l3 xl3 m2"/>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                       <div className="deeps col s12">
                                       {(this.state.captain.length>0) ?
                                          <React.Fragment>
                                              <div className="image-wrap">
                                              <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                               src={(!this.state.captain[0].photo) ? require('./t2.png')
                                               :API_URL+'/user/admin_panel/:'+setImage(this.state.captain[0].photo)}/>
                                              </div>
                                              <div className="info">
                                                <span className="school">{this.state.captain[0].name}<br />
                                                  {this.state.captain[0].r_type}
                                                </span>
                                                <span className="state" style={{marginTop:'5px'}}>
                                                {!this.state.captain[0].github &&  <React.Fragment><a href={this.state.captain[0].github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                                {!this.state.captain[0].linkedin && <React.Fragment><a href={this.state.captain[0].linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                                </span>
                                              </div>
                                              <div className="center black-text">
                                                {this.state.captain[0].name}
                                              </div>
                                          </React.Fragment>
                                           :
                                           <div className="center black-text">No Captain Found</div>
                                         }
                                      </div>

                                       <div className="deeps col s12">
                                        {(this.state.vice_captain.length>0) ?
                                            <React.Fragment>
                                               <div className="image-wrap">
                                               <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                                src={(!this.state.vice_captain[0].photo) ? require('./t2.png')
                                                :API_URL+'/user/admin_panel/:'+setImage(this.state.vice_captain[0].photo)}/>
                                               </div>
                                               <div className="info">
                                                 <span className="school">{this.state.vice_captain[0].name}<br />
                                                   {this.state.vice_captain[0].r_type}
                                                 </span>
                                                 <span className="state" style={{marginTop:'5px'}}>
                                                 {!this.state.captain[0].github  &&  <React.Fragment><a href={this.state.captain[0].github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                                 {!this.state.captain[0].linkedin  && <React.Fragment><a href={this.state.captain[0].linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                                 </span>
                                               </div>
                                               <div className="center black-text">
                                                 {this.state.vice_captain[0].name}
                                               </div>
                                           </React.Fragment>
                                           :
                                           <div className="center black-text">No Vice Captain Found</div>
                                         }
                                     </div>

                                </Carousel>
                                  </div>
                          </div>
                      </React.Fragment>
                    }
                    {/*Team-Head-Ends*/}
                     {/*Mechanical*/}
                    {this.state.mechanical.length>0 &&
                      <React.Fragment>
                    <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                    <h5>Mechanical Domain</h5>
                    <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                        <div className="col l1 xl1 m1">
                            {this.state.start_mech>0 &&
                                <button data-aos='slide-right' onClick={this.loadPrevMech} style={{marginTop:'100%'}} type="button"
                                 className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_left</i>
                                </button>
                            }
                        </div>
                        <div className="col l10 xl10 m10">
                            <div className="row">
                                    {this.state.mechanical.slice(this.state.start_mech, this.state.visible_mech).map((content,index)=>{
                                        return(
                                            <div className="col l3 s12" key={index}>
                                                    <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                            <h2>
                                                                <span>{content.recent && <i className="fa fa-star"></i>}{content.name}</span>
                                                            </h2>
                                                            <div className="mc-content">
                                                                <div className="img-container">
                                                                    <img alt="team" className="img-responsive"
                                                                     src={!content.photo ? require('./t2.png')
                                                                     :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                </div>
                                                                <div className="mc-description">
                                                                  {content.about_member}
                                                                </div>
                                                            </div>
                                                            <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                              <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                            </div>
                                                            <div className="mc-footer">
                                                                <h4>
                                                                    Social Accounts
                                                                </h4>
                                                                <div className="center">
                                                                {content.github  &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                {content.linkedin  && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                {content.gmail  &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                </div></div>
                                                        </article>
                                            </div>
                                        )
                                    })}
                        </div>
                        </div>
                            <div className="col l1 xl1 m1">
                            {this.state.visible_mech < this.state.mechanical.length &&
                                <button  onClick={this.loadMoreMech} style={{marginTop:'100%'}} type="button"
                                 className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_right</i>
                                </button>
                            }
                            </div>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.mechanical.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.recent && <i className="fa fa-star"></i>}{content.name}</span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                            {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}
                                </Carousel>
                                  </div>
                          </div>
                      </React.Fragment>
                    }
                    {/*Mechanical-Ends*/}

                    {/*Corporate-Domain*/}
                    {this.state.corporate.length>0 &&
                      <React.Fragment>
                        <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Corporate Domain</h5>
                        <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                        <div className="col l1 xl1 m1">
                            {this.state.start_corp>0 &&
                                <button data-aos='slide-right' onClick={this.loadPrevCorp} style={{marginTop:'100%'}} type="button"
                                 className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_left</i>
                                </button>
                            }
                        </div>
                        <div className="col l10 xl10 m10">
                            <div className="row">
                                      {this.state.corporate.slice(this.state.start_corp, this.state.visible_corstart_corp).map((content,index)=>{
                                          return(
                                              <div className="col l3 s12" key={index}>
                                                      <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                              <h2>
                                                                  <span>{content.recent && <i className="fa fa-star"></i>}{content.name}</span>
                                                              </h2>
                                                              <div className="mc-content">
                                                                  <div className="img-container">
                                                                      <img alt="team" className="img-responsive"
                                                                       src={!content.photo ? require('./t2.png')
                                                                       :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                  </div>
                                                                  <div className="mc-description">
                                                                    {content.about_member}
                                                                  </div>
                                                              </div>
                                                              <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                                <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                              </div>
                                                              <div className="mc-footer">
                                                                  <h4>
                                                                      Social Accounts
                                                                  </h4>
                                                                  <div className="center">
                                                                  {content.github &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                  {content.linkedin && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                  {content.gmail &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                  </div></div>
                                                          </article>
                                              </div>
                                          )
                                      })}
                        </div>
                        </div>
                            <div className="col l1 xl1 m1">
                            {this.state.visible_corp < this.state.corporate.length &&
                                <button  onClick={this.loadMoreCorp} style={{marginTop:'100%'}}
                                 type="button" className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_right</i>
                                </button>
                            }
                            </div>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.corporate.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.recent && <i className="fa fa-star"></i>}{content.name}<br/>
                                              {content.recent ?
                                              'Head':content.r_type} of {content.domain} Domain
                                            </span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                          {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}
                                </Carousel>
                                  </div>
                          </div>

                      </React.Fragment>
                  }

                  {/*Corporate-Ends*/}

                  {/*Software-Domain*/}
                  {this.state.software.length>0 &&
                    <React.Fragment>
                        <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Software Domain</h5>
                        <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                        <div className="col l1 xl1 s1">
                            {this.state.start_soft>0 &&
                                <button data-aos='slide-right' onClick={this.loadPrevSoft} style={{marginTop:'100%'}} type="button"
                                 className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_left</i>
                                </button>
                            }
                        </div>
                        <div className="col l10 xl10 m10">
                            <div className="row">
                                    {this.state.software.slice(this.state.start_soft, this.state.visible_soft).map((content,index)=>{
                                        return(
                                            <div className="col l3 xl3 m3" key={index}>
                                                    <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                            <h2>
                                                                <span>{content.recent && <i className="fa fa-star"></i>}{content.name}</span>
                                                            </h2>
                                                            <div className="mc-content">
                                                                <div className="img-container">
                                                                    <img alt="team" className="img-responsive"
                                                                     src={!content.photo ? require('./t2.png')
                                                                     :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                </div>
                                                                <div className="mc-description">
                                                                  {content.about_member}
                                                                </div>
                                                            </div>
                                                            <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                              <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                            </div>
                                                            <div className="mc-footer">
                                                                <h4>
                                                                    Social Accounts
                                                                </h4>
                                                                <div className="center">
                                                                {content.github  &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                {content.linkedin  && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                {content.gmail  &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                </div></div>
                                                        </article>
                                            </div>
                                        )
                                    })}
                        </div>
                        </div>
                            <div className="col l1 xl1 m1">
                            {this.state.visible_soft < this.state.software.length &&
                                <button  onClick={this.loadMoreSoft} style={{marginTop:'100%'}}
                                 type="button" className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_right</i>
                                </button>
                            }
                            </div>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.software.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.recent && <i className="fa fa-star"></i>}{content.name}<br />
                                            {content.recent ?
                                            'Head ':content.r_type} of {content.domain} Domain
                                            </span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github  &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin  && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                          {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}

                                </Carousel>
                                  </div>
                          </div>


                      </React.Fragment>
                    }
                    {/*Software-Domain-Ends*/}

                    {/*Electrical-Domain*/}

                    {this.state.electrical.length>0 &&
                      <React.Fragment>
                        <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Electrical Domain</h5>
                        <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                        <div className="col l1 xl1 m1">
                            {this.state.start_electrical>0 &&
                                <button data-aos='slide-right' onClick={this.loadPrevElectrical} style={{marginTop:'100%'}} type="button"
                                 className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_left</i>
                                </button>
                            }
                        </div>
                        <div className="col l10 xl10 m10">
                            <div className="row">
                                      {this.state.electrical.slice(this.state.start_electrical, this.state.visible_electrical).map((content,index)=>{
                                          return(
                                              <div className="col l3 xl3 m3" key={index}>
                                                      <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                              <h2>
                                                                  <span>{content.recent && <i className="fa fa-star"></i>}{content.name}</span>
                                                              </h2>
                                                              <div className="mc-content">
                                                                  <div className="img-container">
                                                                      <img alt="team" className="img-responsive"
                                                                       src={!content.photo ? require('./t2.png')
                                                                       :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                  </div>
                                                                  <div className="mc-description">
                                                                  {content.about_member}
                                                                  </div>
                                                              </div>
                                                              <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                                <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                              </div>
                                                              <div className="mc-footer">
                                                                  <h4>
                                                                      Social Accounts
                                                                  </h4>
                                                                  <div className="center">
                                                                  {content.github  &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                  {content.linkedin  && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                  {content.gmail  &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                  </div></div>
                                                          </article>
                                              </div>
                                          )
                                      })}
                        </div>
                        </div>
                            <div className="col l1 xl1 m1">
                            {this.state.visible_electrical < this.state.electrical.length &&
                                <button  onClick={this.loadMoreElectrical} style={{marginTop:'100%'}}
                                 type="button" className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_right</i>
                                </button>
                            }
                            </div>
                        </div>

                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.electrical.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.recent && <i className="fa fa-star"></i>}{content.name}<br />
                                            {content.recent ?
                                            'Head ':content.r_type} of {content.domain} Domain
                                            </span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github  &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin  && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                            {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}

                                </Carousel>
                                  </div>
                          </div>


                      </React.Fragment>
                  }

                  {/* Electrical-Domain-Ends*/}

                  {/* Web -Domain*/}

                  {this.state.web.length>0 &&
                    <React.Fragment>
                        <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Web Domain</h5>
                        <div className="line"></div>
                    </div>
                    <div className="row display-web" style={{paddingTop:'50px'}}>
                        <div className="col l1 xl1 s1 m1">
                            {this.state.start_web>0 &&
                                <button  data-aos='slide-right' onClick={this.loadPrevWeb} style={{marginTop:'100%'}}
                                 type="button" className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_left</i>
                                </button>
                            }
                        </div>
                        <div className="col l10 xl10 m10 s10">
                            <div className="row">
                                    {this.state.web.slice(this.state.start_web, this.state.visible_web).map((content,index)=>{
                                        return(
                                            <div className="col l3 xl3 m3 s12" key={index}>
                                                    <article className={"material-card "+this.settedClass(content.serial,index)}>
                                                            <h2>
                                                                <span>{content.recent && <i className="fa fa-star"></i>}{content.name}</span>
                                                            </h2>
                                                            <div className="mc-content">
                                                                <div className="img-container">
                                                                    <img alt="team" className="img-responsive"
                                                                     src={(!content.photo) ? require('./t2.png')
                                                                     :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                                                </div>
                                                                <div className="mc-description">
                                                                {content.about_member}
                                                                </div>
                                                            </div>
                                                            <div className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                              <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                            </div>
                                                            <div className="mc-footer">
                                                                <h4>
                                                                    Social Accounts
                                                                </h4>
                                                                <div className="center">
                                                                {content.github  &&  <a href={content.github} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>}
                                                                {content.linkedin  && <a href={content.linkedin} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>}
                                                                {content.gmail  &&  <a href={"mailto:"+content.gmail} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-google"></a>}
                                                                </div></div>
                                                        </article>
                                            </div>
                                        )
                                    })}
                        </div>
                        </div>
                            <div className="col l1">
                            {this.state.visible_web < this.state.web.length &&
                                <button data-aos='slide-left' onClick={this.loadMoreWeb} style={{marginTop:'100%'}}
                                 type="button" className=" btn btn-floating btn-large pink">
                                <i className="material-icons large">chevron_right</i>
                                </button>
                            }
                            </div>
                        </div>


                        <div className="display-ph" style={{padding:'25px'}}>
                             <div className="row">
                               <Carousel
                                 carouselId="Carousel-2"
                                 className="white-text center"
                                 options={{
                                   fullWidth: true,
                                 }}
                               >
                                   {this.state.web.map((content,index)=>{
                                     return(
                                       <div className="deeps col s12" key={index}>
                                          <div className="image-wrap">
                                          <img alt="team" style={{height:'350px',width:'500px',objectFit:'fill'}}
                                           src={(!content.photo) ? require('./t2.png')
                                           :API_URL+'/user/admin_panel/:'+setImage(content.photo)}/>
                                          </div>
                                          <div className="info">
                                            <span className="school">{content.recent && <i className="fa fa-star"></i>}{content.name}<br />
                                            {content.recent ?
                                            'Head':content.r_type} of {content.domain} Domain
                                            </span>
                                            <span className="state" style={{marginTop:'5px'}}>
                                            {content.github  &&  <React.Fragment><a href={content.github} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-github"></a>&emsp;</React.Fragment>}
                                            {content.linkedin  && <React.Fragment><a href={content.linkedin} style={{fontSize:'30px',color:'yellow'}} target="_blank" rel="noopener noreferrer" className="fa fa-fw fa-linkedin"></a>&emsp;</React.Fragment>}
                                            </span>
                                          </div>
                                          <div className="center black-text">
                                          {content.name}
                                          </div>
                                      </div>
                                     )
                                   })}

                                </Carousel>
                                  </div>
                          </div>
                    </React.Fragment>
                }

                  {/* Web -Domain-Ends*/}
</section>
</React.Fragment>
        )
                    }

    }
}
