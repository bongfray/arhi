import React, { Component } from 'react';
import Axios from 'axios';
import {API_URL} from '../../utils/apiUrl';

export default class Achievement extends Component {
    constructor()
    {
        super()
        this.state={
            achievements:[],
            loading:true,
            visible: 8,
            start:0,
            restore_start:0,
            restore_next:0,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchData();
    }
    fetchData=()=>{
        Axios.post(API_URL+'/user/fetch_achievment',{
            action: 'Achievments',
          })
          .then(response =>{
            this.setState({
                achievements:response.data.found,
                loading:false,
           })
          })
          .catch(err=>{
            this.setState({
                loading:false,
           })
            window.M.Toast({html: 'Something Went Wrong !!',classes:'red rounded'})
          })
    }

    loadMore=()=> {
        this.setState((prev) => {
            console.log(prev.visible)
            return {restore_start:prev.start,restore_next:prev.visible,start:prev.visible,
                visible: prev.visible + 8};
          });
  }
  loadPrev=()=> {
    this.setState((prev) => {
        return {start:this.state.restore_start,visible: this.state.restore_next,
            restore_start:this.state.restore_start-8,restore_next:this.state.restore_next-8};
      });
  }

    render() {
        if(this.state.loading)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Fetching Achievements....
                </h5>
                </React.Fragment>
            )
        }
        else
        {
        return (
            <React.Fragment>
                {this.state.achievements.length>0 &&
                    <React.Fragment>
                            <div className="achievement">
                                <div className="shadowbox">
                                    <h2 className="center ach-head" data-aos='fade-up'>ACHIEVEMENTS</h2>
                                    <div className="row">
                                    {this.state.start>0 &&
                                        <div className="center">
                                                            <button onClick={this.loadPrev} type="button"
                                                            className="center btn btn-floating btn-large #69f0ae green accent-2" data-aos='fade-down'>
                                                            <i className="material-icons large black-text" >expand_less</i>
                                                            </button>
                                        </div>
                                    }
                                    </div>
                                    <div className="row">
                                        <React.Fragment>
                                            {this.state.achievements.slice(this.state.start, this.state.visible).map((content,index)=>{
                                                return(
                                                    <div className="row" key={index}>
                                                        <div className="col s12 l12 m12" data-aos='fade-up'>
                                                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                                                            <div className="col s11 m11 l11"><p className="ach-cont">{content.name}</p></div>
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                        </React.Fragment>
                                    </div>
                                    <div className="row">
                                    {this.state.visible < this.state.achievements.length &&
                                      <div className="center">
                                                        <button onClick={this.loadMore}  type="button"
                                                        className="center btn btn-floating btn-large #69f0ae green accent-2" data-aos='fade-up'>
                                                        <i className="material-icons large black-text">expand_more</i>
                                                        </button>
                                      </div>
                                    }
                                    </div>


                                </div>
                            </div>
                    </React.Fragment>
                }
         </React.Fragment>
        )

      }
    }
}
