import React, { Component } from 'react'
import Logo from '../Home/auv.png';
import Axios from 'axios';
import {API_URL} from '../../utils/apiUrl';

export default class Footer extends Component {
    constructor()
    {
        super()
        this.state={
            name:'',
            message:'',
            mailid:'',
            contactdetails:'',
            fetching:true,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchDetails();
    }
    fetchDetails=()=>{
        Axios.post(API_URL+'/user/fetch_contact_details',{
            action: "Contact Details",
          })
          .then(response =>{
            //  console.log(response.data)
            this.setState({
             contactdetails: response.data.found,
             fetching:false,
           })
          })
          .catch(err=>{
            window.M.Toast({html: 'Something Went Wrong !!',classes:'red rounded'})
          })
    }

    render() {
        if(this.state.fetching)
        {
            return(
                <div className="center">Loading...</div>
            )
        }
        else{
        return (
            <React.Fragment>

                <div className="footer" style={{display:'none'}}>
                    <div id="button"></div>
                    <div id="container">
                        <div className="row center">
                            <div className="col l4 s4 m4">
                                <div className="row">
                                    <div className="l12 m12 s12">
                                        <img src={Logo} alt="" data-aos='fade-up' className="footer-logo"/>
                                    </div>
                                    <div className="col l12 s12 m12">
                                        <h3 className="footer-lname" data-aos='fade-up'>SRM AUV</h3>
                                        <p className="footer-lp" data-aos='fade-up'>SRM IST's Official AUV Team</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col l4 s4 m4 footer-list">
                                <h5 className="center footer-h4" data-aos='fade-up'>Explore</h5>
                                <ul className="wrap-nav">
                                <li data-aos='fade-up'><a href="/">Home</a></li>
                                <li data-aos='fade-up'><a href="/#vehicle">Vehicle</a></li>
                                <li data-aos='fade-up'><a href="/#team">Team</a></li>
                                <li data-aos='fade-up'><a href="/#sponsor">Sponsors</a></li>
                                </ul>
                            </div>

                            <div className="col s4 m4">
                            <h5 className="center footer-h4" data-aos='fade-up'>Follow Us</h5>

                            <div className="rounded-social-buttons" data-aos='zoom-out'>
                                {this.state.contactdetails.length>0 &&
                                    <React.Fragment>
                                    <a className="social-button facebook" href={this.state.contactdetails[0].fb} target="_blank" rel="noopener noreferrer" ><i className="fa fa-facebook-f"></i></a>
                                    <a className="social-button twitter" href={this.state.contactdetails[0].twitter} target="_blank" rel="noopener noreferrer" ><i className="fa fa-twitter"></i></a>
                                    <a className="social-button linkedin" href={this.state.contactdetails[0].linkedin} target="_blank" rel="noopener noreferrer" ><i className="fa fa-linkedin"></i></a>
                                    <a className="social-button youtube" href={this.state.contactdetails[0].youtube} target="_blank" rel="noopener noreferrer" ><i className="fa fa-youtube"></i></a>
                                    <a className="social-button instagram" href={this.state.contactdetails[0].insta} target="_blank" rel="noopener noreferrer" ><i className="fa fa-instagram"></i></a>
                                    </React.Fragment>
                                }
                            </div>
                            <hr/>
                            <p className="copyright" data-aos='zoom-in'>© Made with <i className="fa fa-heart"></i> by ArHi &#128526; 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
      }
    }
}
