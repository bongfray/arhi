import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import {API_URL} from '../../../utils/apiUrl';


export default class Section extends Component {
    constructor()
    {
        super()
        this.state={type:''}
        this.componentDidMount  = this.componentDidMount.bind(this);
    }
    componentDidMount()
    {
        M.AutoInit();
    }
    setType=(e)=>{
        this.setState({type:e.target.value})
    }
    render()
    {
        return(
              <React.Fragment>
                  <div className="notinoti">
                    <div className="btn-floating btn-small red" style={{margin:'5px'}}>
                        <i className="material-icons right" onClick={this.props.close}>close</i>
                    </div>
                      <h5 className="center">Vehicle Name - {this.props.vehicle_name}</h5>
                            <div className="row">
                                <div style={{marginLeft:'10px'}} className="input-field col l3">
                                    <select value={this.state.type} onChange={this.setType}>
                                        <option value="" disabled defaultValue>Choose your option</option>
                                        <option value="Vehicle Dimensions">Vehicle Dimensions</option>
                                        <option value="Mass of vehicle">Mass of vehicle</option>
                                        <option value="Top Speed">Top Speed</option>
                                        <option value="Degrees of Freedom">Degrees of Freedom</option>
                                        <option value="Propulsion System">Propulsion System</option>
                                        <option value="Single Board Computer Model">Single Board Computer Models</option>
                                        <option value="Camera Sensors">Camera Sensors</option>
                                        <option value="Software Architecture">Software Architecture</option>
                                        <option value="Power System">Power System</option>
                                    </select>
                                </div>
                             </div>
                            <div className="row" style={{margin:'5px'}}>
                              {this.state.type && <AddDetails type={this.state.type}  vehicle_name={this.props.vehicle_name}/>}
                            </div>


                  </div>
              </React.Fragment>
        )
    }
}

class AddDetails extends Component {
    constructor(props)
    {
      super(props)
      this.state ={
        isAddProduct: false,
        response: {},
        product: {},
        isEditProduct: false,
        loading:false,
        uploading:false,
        loaded_percent:0,
        redirectTo:'',
      }
      this.componentDidMount = this.componentDidMount.bind(this)
       this.onFormSubmit = this.onFormSubmit.bind(this);
    }
  componentDidMount(){
    M.AutoInit();
  }
     onCreate = (e,index) => {

       this.setState({ isAddProduct: true ,product: {}});
     }
     onFormSubmit(data) {
       this.setState({uploading:true})
       let apiUrl;
       if(this.state.isEditProduct){
         apiUrl = '/user/edit_vehicle_details';
       } else {
         apiUrl = '/user/add_details_of_vehicle';
       }
       axios.post(API_URL+apiUrl,{data})
           .then(response => {
             this.setState({
               uploading:false,
               response: response.data,
               isAddProduct: false,
               isEditProduct: false
             })
           })
           .catch(err=>{
               this.setState({uploading:false})
               if(err.response.status === 404)
               {
                 window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
               }
               else
               {
                 window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
               }
           })

     }

     editProduct = (productId,index)=> {
       this.setState({loading:true})
       axios.post(API_URL+'/user/fetch_to_edit_vehicle_details',{
         id: productId,
       })
           .then(response => {
             this.setState({
               loading:false,
               product: response.data,
               isEditProduct: true,
               isAddProduct: true,
             });
           })
           .catch(err=>{
               this.setState({loading:false})
               if(err.response.status === 404)
               {
                 window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
               }
               else
               {
                 window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
               }
           })

    }
    updateState = () =>{
      this.setState({
        isAddProduct:false,
        isEditProduct:false,
      })
    }

    render()
    {
      let productForm;
      var data = {
        fielddata: [
          {
            header: "Data",
            name: "data",
            placeholder: "Put the Info",
            type: "text",
            size:true,
          },
        ],
      };
      if(this.state.isAddProduct || this.state.isEditProduct) {
      productForm = <AddProduct type={this.props.type} cancel={this.updateState} username={this.state.username}
       action={this.props.vehicle_name} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
      }

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
         if(this.state.uploading)
         {
           return(
             <div className="cover_all">
             <div className="center">
                 <div className="box-frames" style={{margin:'20px'}}>
                     <div className="mloader-39 white-text"></div>
                 </div>
             </div>
             <h5 className="center white-text">
                 Uploading You Data.... <span className="yellow-text">{this.state.loaded_percent}</span>
             </h5>
             </div>
           );
         }
      else
      {
        if(this.state.loading)
        {
          return(
            <div className="cover_all">
              <div className="center">
                  <div className="box-frames" style={{margin:'20px'}}>
                      <div className="white-text mloader-39"></div>
                  </div>
              </div>
              <h5 className="center white-text">
                  Processing your request...
              </h5>
            </div>
          );
        }
        else
         {
                  return(
                  <React.Fragment>
                      <br />
                      <div className="sponsor">
                          <div>
                          {!this.state.isAddProduct &&
                              <ProductList image={false} username={this.props.username} title={this.state.option}
                              action={this.props.vehicle_name} type={this.props.type} data={data}  editProduct={this.editProduct}/>
                          }<br />
                          {!this.state.isAddProduct &&
                          <React.Fragment>
                          <div className="row">
                              <div className="col l6 s12 xl6 m12 left" />
                              <div className="col l6 s6 xl6 m6">
                                      <button className="btn right blue-grey darken-2 sup subm"
                                      onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                  </div>
                              </div>
                          </React.Fragment>
                          }
                          { productForm }
                          <br/>
                          </div>
                      </div>
                  </React.Fragment>
                  )
              }
          }
  }
    }
}
