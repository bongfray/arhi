import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {API_URL} from '../../../utils/apiUrl';

export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      uploading:false,
      loaded_percent:0,
      redirectTo:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
  M.AutoInit();
}


   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;

       apiUrl = '/user/team_details';

     const formData = new FormData();
     formData.append('myImage',data.file);
     formData.append('name',data.name);
     formData.append('domain',data.domain);
     formData.append('r_type',data.r_type);

     if(data.about_member)
      {
        formData.append('about_member',data.about_member);
      }
    if(data.alumni_journey)
      {
        formData.append('alumni_journey',data.alumni_journey);
      }
    if(data.linkedin)
      {
        formData.append('linkedin',data.linkedin);
      }
    if(data.gmail)
      {
        formData.append('gmail',data.gmail);
      }
    if(data.github)
      {
        formData.append('github',data.github);
      }
     formData.append('action',data.action);
     formData.append('serial',data.serial);
     this.setState({uploading:true})
     window.M.toast({html: 'Uploading ....!!',classes:'orange rounded'});
     axios.post(API_URL+apiUrl,formData,{
         onUploadProgress:ProgressEvent=>{
              var loaded = Math.round(ProgressEvent.loaded/ProgressEvent.total*100);
              //console.log(loaded)
              this.setState({loaded_percent:loaded})
         }
     })
         .then(response => {
          if(response.data === 'have')
          {
           window.M.toast({html: 'Already There !!',classes:'pink rounded'});
           this.setState({uploading:false})
          }
          else
          {
            window.M.toast({html: 'Saved !!',classes:'green darken-1 rounded'});

           this.setState({
            uploading:false,
            loaded_percent:0,
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
          }
         })
         .catch(err=>{
             this.setState({uploading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })

   }

   editProduct = (productId,index)=> {
     this.setState({loading:true})
     axios.post(API_URL+'/user/fetch_to_edit_team',{
       id: productId,
     })
         .then(response => {
           this.setState({
             loading:false,
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })
         .catch(err=>{
             this.setState({loading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
            {
              header: "Member Name",
              name: "name",
              placeholder: "Enter the Member Name",
              type: "text",
              size:false,
              alumni:true,
            },
            {
              header: "Your Journey with this CLUB [Only 75 Letters allowed including Space]",
              name: "alumni_journey",
              placeholder: "Brief about Journey",
              type: "text",
              size:true,
              alumni:true,
            },
            {
              header: "LinkedIn Account",
              name: "linkedin",
              placeholder: "Enter LinkedIn Account",
              type: "text",
              size:false,
              alumni:true,
            },
            {
              header: "Gmail Account",
              name: "gmail",
              placeholder: "Enter Gmail Account",
              type: "text",
              size:false,
              alumni:true,
            },
            {
              header: "GitHub Account",
              name: "github",
              placeholder: "Enter GitHub Account",
              type: "text",
              size:false,
            },
            {
              header: "Uploaded Photo",
              name: "photo",
              placeholder: "none",
              type: "text",
            },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct loaded_percent={this.state.loaded_percent} cancel={this.updateState} username={this.state.username}
     action={"Team Member"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
       if(this.state.uploading)
       {
         return(
           <div className="cover_all">
           <div className="center">
               <div className="box-frames" style={{margin:'20px'}}>
                   <div className="mloader-39 white-text"></div>
               </div>
           </div>
           <h5 className="center white-text">
               Uploading You Data.... <span className="yellow-text">{this.state.loaded_percent}</span>
           </h5>
           </div>
         );
       }
    else{
      if(this.state.loading)
      {
        return(
          <div className="cover_all">
            <div className="center">
                <div className="box-frames" style={{margin:'20px'}}>
                    <div className="white-text mloader-39"></div>
                </div>
            </div>
            <h5 className="center white-text">
                Processing your request...
            </h5>
          </div>
        );
      }
         else{
                return(
                <div style={{padding:'20px'}}>
                    {!this.state.isAddProduct &&
                      <React.Fragment>
                      <div className="row">
                          <div className="col l6 s12 xl6 m12 left" />
                          <div className="col l6 s6 xl6 m6">
                                  <button className="btn right blue-grey darken-2 sup subm"
                                  onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                              </div>
                          </div>
                      </React.Fragment>
                     }
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct &&
                            <ProductList image={true} no_lead={true} username={this.props.username} title={this.state.option}
                            action={"Team Member"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </div>
                )
          }
        }
}
  }
}



 class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      action:'',
      name:'',
      photo:'',
      file:'',
      type:'',
      domain:'',
      r_type:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }
componentDidMount(){
  M.AutoInit();
  this.setState({
    action:this.props.action,
    type:this.props.type,
    r_type:'Alumni',
    recent:false,
  })
}
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    let validS;
        if(this.state.photo)
        {
          validS = (!this.state.name || (this.state.alumni_journey.length>75));
        }
        else{
          validS = (!this.state.name || (!this.state.file));
        }

      if(validS)
      {
        window.M.toast({html:'Enter all the details !!',classes:'red rounded'})
      }
      else if(this.state.file)
      {
        if(this.state.file.size>400000)
        {
          window.M.toast({html:'File Size can not be more than 400KB !!',classes:'red rounded'})
        }
        else
        {
          this.props.onFormSubmit(this.state);
          this.setState(this.initialState);
        }
      }
      else
      {
        this.props.onFormSubmit(this.state);
        this.setState(this.initialState);
      }
  }
  onChange=(e)=> {
    this.setState({file:e.target.files[0]});
}

  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS</h5>
}

    return(
      <div className="cover_all">
      <div className="card hoverable up" style={{padding:'10px'}}>
      <div className="center">{pageTitle}</div>
      <div>
        <div className="row">
            <div className="col l12 xl12 s12 m12">
                    {this.props.data.fielddata.map((content,index)=>(
                    <div className="row" key={index}>
                        <div className="col l12 xl12 s12 m12">
                         {content.placeholder === "none" ?
                            <React.Fragment>
                                <input style={{display:'none'}} accept=".jpg,.jpeg,.png,.jfif"
                                 type="file" onChange= {this.onChange}
                                 ref={fileInput=>this.fileInput = fileInput} />
                                <button className="btn small" onClick={()=>this.fileInput.click()}>Upload the Photo</button>
                            </React.Fragment>
                          :
                          <React.Fragment>
                             {content.select ?
                                 <select value={this.state.r_type} onChange={(e)=>this.setState({r_type:e.target.value})}>
                                    <option value="" defaultValue>Choose the Department</option>
                                    <option value="Faculty">Faculty</option>
                                    <option value="Alumni">Alumni</option>
                                    <option value="Captain of SRM AUV Team">Captain of SRM AUV Team</option>
                                    <option value="Vice-Captain of SRM AUV Team">Vice-Captain of SRM AUV Team</option>
                                    <option value="Member">Member</option>
                                 </select>
                              :
                              <React.Fragment>
                                {content.domain_type ?
                                 <select value={this.state.domain} onChange={(e)=>this.setState({domain:e.target.value})}>
                                    <option value="" defaultValue>Choose Domain</option>
                                    <option value="Mechanical">Mechanical</option>
                                    <option value="Software">Software</option>
                                    <option value="Electrical">Electrical</option>
                                    <option value="Corporate">Corporate</option>
                                    <option value="Web">Web</option>
                                    <option value="Faculty">Faculty</option>
                                  </select>
                                  :
                                  <React.Fragment>
                                      {content.size === false ?
                                        <div className="input-field col l12 xl12 s12 m12">
                                          <input id={content.name} type={content.type} name={content.name} value={this.state[content.name]}
                                            onChange={e => this.handleD(e, index)} className="validate" required/>
                                          <label htmlFor={content.name} className="label-c">{content.placeholder}</label>
                                        </div>
                                      :
                                      <div className="input-field">
                                            <textarea id={content.name} name={content.name} value={this.state[content.name]}
                                            onChange={e => this.handleD(e, index)} className="materialize-textarea"></textarea>
                                            <label htmlFor={content.name} className="label-c">{content.placeholder}</label>
                                        </div>
                                      }
                                  </React.Fragment>
                                }
                              </React.Fragment>
                             }
                          </React.Fragment>
                            }

                        </div>
                    </div>
                    ))}
                <div className="row">
                    <div className="col l6 xl6 s4 m6" />
                    <div className="col l6 xl6 s8 m6">
                            <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
                            <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
      </div>
      </div>
    )
  }
}


class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      show_info:false,
      fetching:true,
      fullview:false,
      img:'',
      r_marked:false,
      r_vehicle:'',
      name_marked:false,
      lead_name:'',
      value_for_search:'',
      confirm_win:false,
      productId:'',
      rFiles:'',
      ac_id:'',
      submitting:false,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
    M.AutoInit();
    this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


componentDidUpdate=(prevProps)=>{
  if((prevProps.type !== this.props.type) || (prevProps.action !== this.props.action))
  {
    this.fetch();
  }
}

fetch =() =>{
  let url;

    url ='/user/fetch_team';

  axios.post(API_URL+url,{
    action: this.props.action,
    type:this.props.type,
  })
  .then(response =>{
    if(response.data.files)
    {
      this.setState({
       products: response.data.found.filter(item=>item.r_type === 'Alumni'),
       rFiles:response.data.files,
       fetching:false,
     })
    }
    else
    {
      this.setState({
       products: response.data.found,
       fetching:false,
     })
    }

  })
  .catch(err=>{
    this.setState({fetching:false})
  })
}


  render() {
    if(this.state.fetching)
    {
      return(
        <React.Fragment>
        <div className="center">
            <div className="box-frames" style={{margin:'20px'}}>
                <div className="mloader-39"></div>
            </div>
        </div>
        <h5 className="center">
            Loading....
        </h5>
        </React.Fragment>
      )
    }
    else
    {
      if(this.state.submitting)
      {
        return(
          <div className="cover_all">
            <div className="center">
                <div className="box-frames" style={{margin:'20px'}}>
                    <div className="white-text mloader-39"></div>
                </div>
            </div>
            <h5 className="center white-text">
                Processing your request...
            </h5>
          </div>
        )
      }
      else
      {
      if(!this.state.products)
      {
        return(
          <div className="center">No Data</div>
        )
      }
      else
      {
      var { rFiles } = this.state;
      function setImage(data)
      {
        var datas =rFiles.filter(item=>item.filename === data);
        //console.log(data[0].filename)
        return datas[0].filename;
      }

      function delId(data)
      {
        var datas =rFiles.filter(item=>item.filename === data);
        //console.log(data[0]._id)
        return datas[0]._id;
      }

    var { products} = this.state;

    var libal;

    if(this.state.value_for_search.length > 0)
    {
      var  searchString = this.state.value_for_search.trim().toLowerCase();
        libal = products.filter(function(i) {
          return i.name.toLowerCase().match( searchString );
        });
    }
    else{
      libal = products;
    }
      return(
        <div>
          {this.state.confirm_win &&
            <div className="cover_all">
              <div className="center up" style={{padding:'15px'}}>
              <div>
                  <div className="card-content">
                  <h5 className="center">Confirmation of Deletion</h5><br /><br />
                 <div className="center">Rememeber the confirmation of this deltion will delete all datas related to this particular data set.
                 </div><br /><br />
                  </div>
                  <div className="card-action">
                    <div className="right">
                      <button className="btn transparent black-text" onClick={()=>this.setState({confirm_win:false})}>Cancel</button>&emsp;
                      <button className="btn red" onClick={this.deleteIt}>Delete</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          }
          {((this.props.action === "Team Member") || (this.props.action === "Achievments")
           || (this.props.action === "Vehicle") || (this.props.action === "Sponsors")) &&
                  <div className="input-field">
                    <input id="search" type="search" value={this.state.value_for_search} placeholder="Search By Only Name"
                       disabled={products.length>0 ? false : true}
                         onChange={(e)=>this.setState({value_for_search:e.target.value})} required/>
                    <label className="label-icon" htmlFor="search"></label>
                  </div>
            }

          {this.state.fullview &&
            <div className="cover_all">
              <div className="up">
                <div className="btn-floating btn-small red" style={{margin:'5px'}}>
                  <i className="material-icons right" onClick={()=>this.setState({fullview:false})}>close</i>
                </div><br />
                <div className="card">
                  <div className="card-image">
                     <img style={{maxWidth:'100%',height: '700px'}}
                    src={API_URL+'/user/admin_panel/:'+this.state.img} alt="here"/>
                  </div>
                </div>
              </div>
            </div>
          }


          <div className="row">
            {libal.map((product,ind) => (
                 <div className="col s12 m6 l4 xl4" key={ind}>
                    <div className="card">
                      {this.props.image &&
                        <div className="card-image">
                         <img  style={{height:'200px'}} onClick={()=>this.setState({fullview:true,img:setImage(product.photo)})}
                          src = {API_URL+'/user/admin_panel/:'+setImage(product.photo)} alt = "something" />
                        </div>
                      }
                      <div className="card-content">
                        {this.props.data.fielddata.map((content,index)=>(
                          <React.Fragment key={index}>
                              {content.placeholder === 'none' ?
                               <React.Fragment></React.Fragment>
                               :
                               <div className="row">
                                    <div className="col l6 xl6 s6 m6">{content.header}</div>
                                    <div className="col l6 xl6 s6 m6">
                                       <div className={((content.name === ('alumni_journey')) || (content.name === 'about_member')) ? 'cut-text': ''}
                                       style={{wordWrap: 'break-word',whiteSpace: 'pre-line'}}>
                                         {product[content.name]}
                                       </div>
                                    </div>
                                  </div>
                               }
                          </React.Fragment>
                        ))}
                      </div>
                      <div className="card-action">
                        <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                      </div>
                    </div>
                  </div>

                  ))}
            </div>
        </div>
      )
    }
   }
  }

  }
}
