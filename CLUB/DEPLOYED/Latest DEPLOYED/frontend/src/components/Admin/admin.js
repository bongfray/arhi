import React, { Component } from 'react'
import M from 'materialize-css';
import { Redirect } from 'react-router-dom';
import Sponsor from './OPERATIONS/sponsor_admin'
import Team from './OPERATIONS/team_admin'
import Vehicle from './OPERATIONS/vehicle_admin';
import Admin from './OPERATIONS/admin_action'
import Achievement from './OPERATIONS/achiv-admin'
import Gallery from './OPERATIONS/gallery_admin';
import Contact from './OPERATIONS/teamContact';
import Axios from 'axios';
import {API_URL} from '../../utils/apiUrl';


export default class AdminSignup extends Component {
    constructor()
    {
        super()
        this.state={
            selected:'',
            active:'',
            redirectTo:null,
            loggedin:false,
            username:'',
            display:'none',
            loading:true,
            prof:false,
        }
        this.componentDidMount =this.componentDidMount.bind(this)
    }
    path=(e)=>{
        this.setState({selected:e})
        if (this.state.active === e) {
            this.setState({active : null})
          } else {
            this.setState({active : e})
          }

    }

    color =(position) =>{
        if (this.state.active === position) {
            return "#00695c teal darken-3 white-text display-style";
          }
          return "display-style hover-effect";
      }

      componentDidMount()
      {
          M.AutoInit();
          this.getUser();
      }
      getUser=()=>{
        Axios.get(API_URL+'/user/check')
        .then(res => {

              if(res.data==='no')
              {
                this.setState({ loading: false, redirectTo:'/'});
              }
              else if (res.data === 'ok') {
                this.setState({ loading: false,display:'block' });
              }
              else{
                this.setState({ loading: false,redirectTo:'/'});
              }
        })
        .catch(err=>{
          this.setState({ loading: false,redirectTo:'/'});
        })
      }

      logout=(event)=>{
        event.preventDefault()
          window.M.toast({html: 'Logging Out....!!',classes:'pink rounded'});
          Axios.post(API_URL+'/user/logout')
        .then(response => {
            window.M.toast({html: 'Logged Out....!!',classes:'green rounded'});
            this.setState({
              loggedin:false,
              redirectTo:'/',
            })
        })
        .catch(err => {
          console.error(err);
          this.setState({ loading: false, redirectTo:'/',display:'none'});
        });
      }


    render()
    {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
          if(this.state.loading)
          {
            return(
              <React.Fragment>
              <div className="center">
                  <div className="box-frames" style={{margin:'20px'}}>
                      <div className="mloader-39"></div>
                  </div>
              </div>
              <h5 className="center">
                  Loading....
              </h5>
              </React.Fragment>
            )
          }
          else{
            return(
                <React.Fragment>
                    {this.state.prof &&<div className="cover_all">
                            <div className="up zoom-in-div" style={{padding:'10px'}}>
                              <div className="btn-floating btn-small red" style={{margin:'5px'}}>
                                <i className="material-icons right" onClick={()=>this.setState({prof:false})}>close</i>
                              </div><br />
                                <Profile  />
                            </div>
                        </div>
                      }
                        <nav style={{background:'#333'}}>
                            <div className="nav-wrapper">
                                <span className="brand-logo" style={{marginLeft:'10px'}}>SRM AUV</span>
                                  <ul id="nav-mobile" class="right">
                                    <li style={{paddingRight:'5px'}}><span onClick={()=>this.setState({prof:true})} className="white-text go">Profile  </span></li>
                                    <li style={{padding:'0px 5px 0px 2px'}}><span onClick={this.logout} className="white-text go">Logout</span></li>
                                  </ul>
                            </div>
                        </nav>
                        <br />
                <div className="row">
                    <div className="col l2 xl2 s12 m2">
                            <div className={this.color('admin')} onClick={()=>this.path('admin')} >Admin Action</div>
                            <div className={this.color('spons')} onClick={()=>this.path('spons')}>Sponsors</div>
                            <div className={this.color('team')}  onClick={()=>this.path('team')}>Team Member</div>
                            <div className={this.color('vehicle')} onClick={()=>this.path('vehicle')}>Vehicle Info</div>
                            <div className={this.color('gallery')}  onClick={()=>this.path('gallery')}>Gallery</div>
                            <div className={this.color('achiv')}  onClick={()=>this.path('achiv')}>Achievements</div>
                            <div className={this.color('contact')}  onClick={()=>this.path('contact')}>Contact Details</div>
                    </div>
                    <div className="col l10 xl10 m10 s12">
                            <PathDecider selected={this.state.selected}/>
                    </div>
                </div>
                </React.Fragment>
            )
        }
      }

    }
}

class PathDecider extends Component{
    render()
    {
    console.log(this.props.selected)
       if(this.props.selected === 'admin')
       {
           return(
            <Admin />
           )
       }
       else if(this.props.selected === 'spons')
       {
           return(
            <Sponsor />
           )
       }
       else if(this.props.selected === 'team')
       {
           return(
            <Team />
           )
       }
       else if(this.props.selected === 'vehicle')
       {
           return(
            <Vehicle />
           )
       }
       else if(this.props.selected === 'gallery')
       {
           return(
            <Gallery />
           )
       }
       else if(this.props.selected === 'achiv')
       {
           return(
            <Achievement />
           )
       }
       else if(this.props.selected === 'contact')
       {
           return(
            <Contact />
           )
       }
       else{
           return(
               <div></div>
           )
       }
    }
}


class Profile extends Component {

    constructor(props) {
          super(props);
      this.state = {
        loading: true,
        submit:false,
        up_username:'',
        new_password:'',
        current_password:'',
        up_confirm_password:'',
      }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSub = this.handleSub.bind(this)
    }
    componentDidMount() {
      this.fetchUser();
      M.AutoInit()
  }
  fetchUser=()=>{
    Axios.get(API_URL+'/user/dash2',)
    .then(response => {
      if(response.status === 200)
      {
        this.setState({
          loading:false,
        })
        console.log(response.data.mailid);
        if(response.data === 'no')
        {
          window.M.toast({html: 'You are not loggedIn',classes:'rounded #ec407a pink lighten-1'});
        }
        else
        {
        this.setState({
          up_username: response.data.mailid,
        })
      }
      }
    })
  }


  handleSub(event) {
    event.preventDefault()
    if((this.state.up_confirm_password) || (this.state.new_password) || (this.state.current_password))
    {
      if((this.state.up_confirm_password) && (this.state.new_password) && (this.state.current_password))
      {
        if(this.state.up_confirm_password === this.state.new_password)
        {
          this.setState({submit:true})
          window.M.toast({html: 'Submitting....',classes:'rounded orange'});
     Axios.post(API_URL+'/user/newd', {
       new_password: this.state.new_password,
       current_password: this.state.current_password,
     })
      .then(response => {
        if(response.status===200){
          if(response.data.succ)
           {
             this.setState({submit:false})
            window.M.toast({html: response.data.succ,classes:'rounded #ec407a green darken-1'});
          }
          else if(response.data.fail)
          {
            this.setState({submit:false})
            window.M.toast({html: 'Kindly Match Your Current Password !!',classes:'rounded green darken-1'});
            return false;
          }
        }
       }).catch(error => {
         this.setState({submit:false})
         window.M.toast({html: 'Something Went Wrong !!',classes:'rounded red'});
       })
      this.setState({
        up_username: this.state.up_username,
        up_confirm_password: '',
        current_password: '',
        new_password: '',
        })

      }
      else
      {
           window.M.toast({html: 'New Password Does not Match !!',classes:'rounded red'});
        this.setState({
          up_confirm_password: '',
          new_password: '',
          submit:false,
          })
        return false;
      }
    }
      else
      {
        this.setState({submit:false})
        window.M.toast({html: 'Fill all the fields', outDuration:'1850', inDuration:'900', displayLength:'1500'});
        return false;
      }
    }
    else
    {
      this.setState({submit:true})
      window.M.toast({html: 'Submitting....',classes:'rounded orange'});
      Axios.post(API_URL+'/user/newd', {
        up_username: this.state.up_username,
        new_password: this.state.new_password,
        up_confirm_password: this.state.up_confirm_password,
        current_password: this.state.current_password
      })
       .then(response => {
         if(response.status===200){
           if(response.data.succ)
            {
              this.setState({submit:false})
             window.M.toast({html: response.data.succ,classes:'rounded #ec407a green darken-1'});
           }
         }
        }).catch(error => {
          this.setState({submit:false})
           window.M.toast({html: 'Something Went Wrong !!',classes:'rounded red'});
        })
       this.setState({
        up_username: this.state.up_username,
        up_confirm_password: '',
        current_password: '',
        new_password: '',
         })
  }
  }
  handleChange= (e) => {
          this.setState({
              [e.target.name]: e.target.value
          });

      };


    render() {
      if(this.state.submit)
      {
        return(
          <div className="cover_all">
                <div className="up-loader">
                  <div className="center">
                      <div className="box-frames" style={{margin:'20px'}}>
                          <div className="mloader-39"></div>
                      </div>
                  </div>
                  <h5 className="center white-text">
                      Processing your Submission !!
                  </h5>
                </div>
          </div>
        )
      }
      else
       {
        if(this.state.loading === true)
        {
          return(
            <React.Fragment>
            <div className="center">
                <div className="box-frames" style={{margin:'20px'}}>
                    <div className="mloader-39"></div>
                </div>
            </div>
            <h5 className="center">
                Loading....
            </h5>
            </React.Fragment>
          );
        }
        else{
            return (
          <React.Fragment>
            <div className="profile-root">
                    <div className="">
                      <h5 className="center">Profile Datas</h5>
                    </div><br />
                    <div>
                        {this.renderCells()}
                    </div>
            </div>
          </React.Fragment>
          );
        }
      }
    }

    renderCells() {
      return(
        <div className="profile">
            <table>
                <thead>
                    <tr>
                        <td className="center">
                        Mail Id
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="center">
                            <Cell value={ this.state.up_username } name="mail_id" onChange={value => this.setState({up_username: value})} />
                        </td>
                    </tr>
                </tbody>
            </table>
<br />

             <h6 style={{marginLeft:'10px'}}>RESET YOUR PASSWORD -</h6>
<br />

                 <div className="input-field">
                   <input id="curpass" type="password" className="validate" value={this.state.current_password} name="current_password" onChange={this.handleChange} required />
                   <label htmlFor="curpass">Current Password</label>
                 </div><br />
                  <div className="input-field">
                      <input id="newpass" type="password" className="validate" name="new_password" value={this.state.new_password} onChange={this.handleChange} required />
                      <label htmlFor="newpass">New Password</label>
                 </div><br />
                 <div className="input-field">
                     <input id="conpass" type="password" className="validate" name="up_confirm_password" value={this.state.up_confirm_password} onChange={this.handleChange} required />
                     <label htmlFor="conpass">Confirm Password</label>
                </div>
            <br />

        <button className="btn green darken-2 right col l3"
         style={{width:'100%'}} onClick={this.handleSub}>SUBMIT</button>
         <br />
  </div>
          );

      }
  }


  class Cell extends Component {

      constructor(props) {
          super(props);
          this.state = { editing: false };
      }

      render() {
      const name = this.props.name;
          const { value, onChange } = this.props;
      if(this.state.editing)
      {
        return(
          <div className="row">
          <div className="col l8 center">
              <input ref='input' value={value} onChange={e => onChange(e.target.value)} onBlur={ e => this.onBlur()} />
          </div>
          <div className="col l4"><div className="btn left waves-effect btn pink"  onClick={ e => this.onBlur()}>Save</div></div>
          </div>

        );
      }
      else{
        if(name)
        {
        return(
          <div className="row">
            <div className="col l8">
                <span>{value}</span>
            </div>
            <div className="col l4">
                <i onClick={() => this.onFocus()} className="go material-icons small">edit</i>
            </div>
          </div>
        );
      }
      else{
        return(
           <span>{value}</span>
        );
      }
      }


      }

      onFocus() {
          this.setState({ editing: true }, () => this.refs.input.focus());
      }

      onBlur() {
          this.setState({ editing: false });
      }
  }
