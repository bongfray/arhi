import React, { Component } from 'react';
import { Link,Redirect } from 'react-router-dom';
import M from 'materialize-css';
import Axios from 'axios';
import {API_URL} from '../../utils/apiUrl';

export default class AdminSignup extends Component {
    constructor()
    {
        super()
        this.state={
            username:'',
            password:'',
            mailid:'',
            signup:false,
            redirectTo:null,
            submit:false,
            loading:true,
            show_pass:false,
        }
        this.componentDidMount =this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        M.AutoInit();
       this.getUser();
    }
    getUser=()=>{
        Axios.get(API_URL+'/user/check')
        .then(res => {
                if(res.data==='expire')
                {
                  this.setState({ loading: false, redirectTo:'/'});
                }
                else if (res.data === 'ok') {
                  this.setState({ loading: false,redirectTo:'/admin_panel' });
                }
                else{
                  this.setState({loading:false})
                }
          })
          .catch(err=>{
            this.setState({loading:false,redirectTo:'/'})
            console.log(err)
          })
    }
    signIn=()=>{
        window.M.toast({html: 'Signing In !!',classes:'orange rounded'});
        this.setState({submit:true})
        Axios.post(API_URL+'/user/signin',{data:this.state})
          .then(res => {
            if(res.data  === 'ok')
            {
              window.M.toast({html: 'Signed In !!',classes:'green darken-1 rounded'});
              this.setState({submit:false,redirectTo:'/admin_panel',username:'',password:''})
            }
            else if(res.data === 'w pass')
            {
              this.setState({submit:false,password:''})
              window.M.toast({html:'Wrong Password !!',classes:'rounded red'})
            }
            else if(res.data === 'no user')
            {
              this.setState({submit:false,username:'',password:''})
              window.M.toast({html:'User Not Found !!',classes:'rounded red'})
            }
            else if(res.data === 'have')
            {
              this.setState({submit:false})
              window.M.toast({html:'User have another logged history !!',classes:'rounded red'})
            }
             else
             {
                this.setState({loading:false,submit:false})
            }
          })
          .catch(err => {
            window.M.toast({html:'Something went wrong  !!',classes:'rounded red'})
            this.setState({loading:false,submit:false})
          });
    }

    sendMail=(e)=>{
        e.preventDefault();
        if(!this.state.username || !this.state.mailid)
        {
            window.M.toast({html: 'Enter all the details !!',classes:'red rounded'});
        }
        else
        {
            window.M.toast({html: 'Sending Mail...!!',classes:'orange rounded'});
            this.setState({submit:true})
            Axios.post(API_URL+'/user/forgo',{data:this.state})
            .then(res=>{
                  this.setState({submit:false})
                window.M.toast({html: 'Check your Mail Id !!',classes:'green darken-1 rounded'});
            })
            .catch(err=>{
                  this.setState({submit:false})
                window.M.toast({html: 'Something Went Wrong  !!',classes:'red rounded'});
            })
        }
    }
    render()
    {
      if(this.state.submit)
      {
        return(
          <div className="cover_all">
                <div className="up-loader">
                  <div className="center">
                      <div className="box-frames" style={{margin:'20px'}}>
                          <div className="mloader-39 white-text"></div>
                      </div>
                  </div>
                  <h5 className="center white-text">
                      Processing your Submission !!
                  </h5>
                </div>
          </div>
        )
      }
      else
      {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
          if(this.state.loading)
          {
            return(
              <React.Fragment>
              <div className="center">
                  <div className="box-frames" style={{margin:'20px'}}>
                      <div className="mloader-39"></div>
                  </div>
              </div>
              <h5 className="center">
                  Validating You !!
              </h5>
              </React.Fragment>
            )
          }
          else{
        return(
            <React.Fragment>
              {this.state.show_pass &&
                <div className="cover_all">
                   <div className="up" style={{padding:'10px'}}>
                     <div className="btn-floating btn-small red" style={{margin:'5px'}}>
                       <i className="material-icons right" onClick={()=>this.setState({show_pass:false})}>close</i>
                     </div><br />
                      <h4 className="center">Reset Password</h4>
                      <br />
                       <div className="input-field ">
                          <input id="mailid" type="email"
                          onChange={(e)=>this.setState({mailid:e.target.value})} className="validate" required />
                          <label htmlFor="mailid" className="label-c">Mail ID</label>
                       </div>
                       <br />
                       <div className="input-field">
                          <input id="username" type="text"
                          onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                          <label htmlFor="username" className="label-c">Username</label>
                       </div>
                       <br />
                       <button className="btn btn-small right col l4 xl4 m4 s4" onClick={this.sendMail}>Send Mail</button>
                  </div>
                </div>
              }

                {this.state.signup ? <Signup over={()=>this.setState({signup:false})} /> :
               <div className="row">
                   <div className="col l4 xl4 m4"/>
                    <div className="col l4 xl4 m4 s12">
                        <div className="card hoverable" style={{borderRadius:'10px',marginTop:'120px'}}>
                            <div className="card-content">
                                <span className="center card-title">SIGN IN</span>
                                <div className="row">
                                <div className="input-field col l12 xl12 s12 m12">
                                    <input id="susername" type="text" onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                                    <label htmlFor="susername" className="label-c">Username</label>
                                    </div>
                                    <div className="input-field col l12 xl12 s12 m12">
                                    <input id="spassword" type="password" onChange={(e)=>this.setState({password:e.target.value})} className="validate" required />
                                    <label htmlFor="spassword" className="label-c">Password</label>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col l6 xl6 s6 m6">
                                         <button style={{width:'100%'}} onClick={()=>this.setState({signup:true})} className="left btn btn-small pink">SIGN UP</button>
                                    </div>
                                    <div className="col l6 xl6 s6 m6">
                                     <button style={{width:'100%'}} type="submit" onClick={this.signIn} disabled={(this.state.username && this.state.password) ? false :true}
                                      className="btn btn-small pink right">SIGN IN</button>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action center">
                                    <div className="red-text center go" onClick={()=>this.setState({show_pass:true})}>Forgot Password</div>
                            </div>
                        </div>
                    </div>
                   <div className="col l4 xl4 m4"/>
               </div>
               }
            </React.Fragment>
        )
      }
            }
        }
    }
}

class Signup extends Component {
    constructor()
    {
        super()
        this.state={
            username:'',
            password:'',
            mailid:'',
            loading:true,
            renderStat:true,
            submit:false
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchState();
    }
    fetchState=()=>{
        Axios.post(API_URL+'/user/admin_action',{action:'signup_stat'})
        .then(res=>{
            if(res.data)
            {
                this.setState({renderStat:res.data.status,loading:false})
            }
            else
            {
              this.setState({loading:false})
            }
        })
    }
    submit=(e)=>{
        e.preventDefault();
        if(!this.state.username || !this.state.password || !this.state.mailid)
        {
            window.M.toast({html: 'Enter all the details !!',classes:'red rounded'});
        }
        else
        {
            window.M.toast({html: 'Signing Up !!',classes:'orange rounded'});
            this.setState({submit:true})
            Axios.post(API_URL+'/user/signup',{data:this.state})
            .then(res=>{
                if(res.data === 'have')
                {
                  this.setState({submit:false})
                    window.M.toast({html: 'User already there !!',classes:'red rounded'});
                }
                else
                {
                    this.setState({submit:false})
                    window.M.toast({html: 'Signed Up !! Now you can signin !!',classes:'green darken-1 rounded'});
                }
            })
            .catch(err=>{
              this.setState({submit:false})
                window.M.toast({html: 'Something Went Wrong !!',classes:'red rounded'});
            })
        }
    }
    render()
    {
      if(this.state.submit)
      {
        return(
          <div className="cover_all">
                <div className="up-loader">
                  <div className="center">
                      <div className="box-frames" style={{margin:'20px'}}>
                          <div className="mloader-39 white-text"></div>
                      </div>
                  </div>
                  <h5 className="center white-text">
                      Processing your Submission !!
                  </h5>
                </div>
          </div>
        )
      }
      else
       {
        if(this.state.loading)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Loading....
                </h5>
                </React.Fragment>
            )
        }
        else{
        return(
            <div className="row">
            <div className="col l4 xl4 m2 s1"/>
             <div className="col l4 xl4 m8 s10" >
                 <div className="card hoverable" style={{borderRadius:'10px',marginTop:'120px'}}>
                     <div className="card-content">
                         {this.state.renderStat ?
                         <React.Fragment>
                            <span className="center card-title">SIGN UP</span>
                            <div className="row">
                                <div className="input-field col l12 xl12 s12 m12">
                                    <input id="ssusername" type="text" onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                                    <label htmlFor="ssusername" className="label-c">Username</label>
                                </div>
                                <div className="input-field col l12 xl12 s12 m12">
                                    <input id="ssmailid" type="email" onChange={(e)=>this.setState({mailid:e.target.value})} className="validate" required />
                                    <label htmlFor="ssmailid"  className="label-c">Email Id</label>
                                </div>
                                <div className="input-field col l12 xl12 s12 m12">
                                <input id="sspassword" type="password" onChange={(e)=>this.setState({password:e.target.value})} className="validate" required />
                                <label htmlFor="sspassword" className="label-c">Password</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col l6 xl6 s6 m6">
                                    <div style={{width:'100%'}} onClick={this.props.over} className="left go">Sign In</div>
                                </div>
                                <div className="col l6 xl6 s6 m6">
                                <button style={{width:'100%'}} type="submit" onClick={this.submit}
                                className="btn btn-small pink right">SIGN UP</button>
                                </div>
                            </div>
                        </React.Fragment>
                     :
                     <div className="center">
                         <h6 className="center">We are not acceping signup requests !!</h6>
                         <br />
                         <Link to="/"><button className="btn btn-small pink center">HOME</button></Link>
                     </div>
                     }
                    </div>
                 </div>
             </div>
            <div className="col l4 xl4 m2 s1"/>
        </div>
        )
                    }
    }
  }
}
