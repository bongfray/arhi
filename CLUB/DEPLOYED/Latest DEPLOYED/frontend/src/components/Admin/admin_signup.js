import React, { Component } from 'react';
import { Link,Redirect } from 'react-router-dom';
import M from 'materialize-css';
import Axios from 'axios';
import {API_URL} from '../../utils/apiUrl';

export default class AdminSignup extends Component {
    constructor()
    {
        super()
        this.state={
            username:'',
            password:'',
            mailid:'',
            signup:false,
            redirectTo:null,
            submit:false,
            loading:true,
            show_pass:false,
        }
        this.componentDidMount =this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        M.AutoInit();
       this.getUser();
    }
    getUser=()=>{
        Axios.get(API_URL+'/user/check')
        .then(res => {
                if(res.data==='expire')
                {
                  this.setState({ loading: false, redirectTo:'/'});
                }
                else if (res.data === 'ok') {
                  this.setState({ loading: false,redirectTo:'/admin_panel' });
                }
                else{
                  this.setState({loading:false})
                }
          })
          .catch(err=>{
            this.setState({loading:false,redirectTo:'/'})
            console.log(err)
          })
    }
    signIn=()=>{
        window.M.toast({html: 'Signing In !!',classes:'orange rounded'});
        this.setState({submit:true})
        Axios.post(API_URL+'/user/signin',{data:this.state})
          .then(res => {
            if(res.data  === 'ok')
            {
              window.M.toast({html: 'Signed In !!',classes:'green darken-1 rounded'});
              this.setState({submit:false,redirectTo:'/admin_panel',username:'',password:''})
            }
            else if(res.data === 'w pass')
            {
              this.setState({submit:false,password:''})
              window.M.toast({html:'Wrong Password !!',classes:'rounded red'})
            }
            else if(res.data === 'no user')
            {
              this.setState({submit:false,username:'',password:''})
              window.M.toast({html:'User Not Found !!',classes:'rounded red'})
            }
            else if(res.data === 'have')
            {
              this.setState({submit:false})
              window.M.toast({html:'User have another logged history !!',classes:'rounded red'})
            }
             else
             {
                this.setState({loading:false,submit:false})
            }
          })
          .catch(err => {
            window.M.toast({html:'Something went wrong  !!',classes:'rounded red'})
            this.setState({loading:false,submit:false})
          });
    }

    sendMail=(e)=>{
        e.preventDefault();
        if(!this.state.username || !this.state.mailid)
        {
            window.M.toast({html: 'Enter all the details !!',classes:'red rounded'});
        }
        else
        {
            window.M.toast({html: 'Sending Mail...!!',classes:'orange rounded'});
            this.setState({submit:true})
            Axios.post(API_URL+'/user/forgo',{data:this.state})
            .then(res=>{
                  this.setState({submit:false})
                window.M.toast({html: 'Check your Mail Id !!',classes:'green darken-1 rounded'});
            })
            .catch(err=>{
                  this.setState({submit:false})
                window.M.toast({html: 'Something Went Wrong  !!',classes:'red rounded'});
            })
        }
    }
    render()
    {
      if(this.state.submit)
      {
        return(
          <div className="cover_all">
                <div className="up-loader">
                  <div className="center">
                      <div className="box-frames" style={{margin:'20px'}}>
                          <div className="mloader-39 white-text"></div>
                      </div>
                  </div>
                  <h5 className="center white-text">
                      Processing your Submission !!
                  </h5>
                </div>
          </div>
        )
      }
      else
      {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
          if(this.state.loading)
          {
            return(
              <React.Fragment>
              <div className="center">
                  <div className="box-frames" style={{margin:'20px'}}>
                      <div className="mloader-39"></div>
                  </div>
              </div>
              <h5 className="center">
                  Validating You !!
              </h5>
              </React.Fragment>
            )
          }
          else{
        return(
          <React.Fragment>
          {this.state.show_pass &&
            <div className="cover_all">
               <div className="up" style={{padding:'10px'}}>
                 <div className="btn-floating btn-small red" style={{margin:'5px'}}>
                   <i className="material-icons right" onClick={()=>this.setState({show_pass:false})}>close</i>
                 </div><br />
                  <h4 className="center">Reset Password</h4>
                  <br />
                   <div className="input-field ">
                      <input id="mailid" type="email"
                      onChange={(e)=>this.setState({mailid:e.target.value})} className="validate" required />
                      <label htmlFor="mailid" className="label-c">Mail ID</label>
                   </div>
                   <br />
                   <div className="input-field">
                      <input id="username" type="text"
                      onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                      <label htmlFor="username" className="label-c">Username</label>
                   </div>
                   <br />
                   <button className="btn btn-small right col l4 xl4 m4 s4" onClick={this.sendMail}>Send Mail</button>
              </div>
            </div>
          }

          <div className="login">
          <div class="wrapper">
          	<div class="container">

                {this.state.signup ?
                  <React.Fragment>
                   <h1>SIGN UP</h1>
                   <Signup over={()=>this.setState({signup:false})} />
                  </React.Fragment>

                 :
                 <React.Fragment>
                    <h1>SIGN IN</h1>
                     <form class="form">
                       <input type="text" placeholder="Username" onChange={(e)=>this.setState({username:e.target.value})} />
                       <input type="password" onChange={(e)=>this.setState({password:e.target.value})} placeholder="Password" />
                       <button type="submit" id="login-button" disabled={(this.state.username && this.state.password) ? false :true}
                        onClick={this.signIn}>Login</button><br /><br />
                        <div className="row">
                          <div className="col l6 xl6 m6 s6">
                            <div className="left go" onClick={()=>this.setState({show_pass:true})}>FORGOT PASSWORD</div>
                          </div>
                          <div className="col l6 xl6 m6 s6">
                            <div className="right go" onClick={()=>this.setState({signup:true})}>SIGN UP</div>
                          </div>
                        </div>
                     </form>
                  </React.Fragment>
                }

          	</div>

          	<ul class="bg-bubbles">
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          		<li></li>
          	</ul>
          </div>
          </div>
          </React.Fragment>
        )
      }
            }
        }
    }
}

class Signup extends Component {
    constructor()
    {
        super()
        this.state={
            username:'',
            password:'',
            mailid:'',
            loading:true,
            renderStat:true,
            submit:false
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchState();
    }
    fetchState=()=>{
        Axios.post(API_URL+'/user/admin_action',{action:'signup_stat'})
        .then(res=>{
            if(res.data)
            {
                this.setState({renderStat:res.data.status,loading:false})
            }
            else
            {
              this.setState({loading:false})
            }
        })
    }
    submit=(e)=>{
        e.preventDefault();
        if(!this.state.username || !this.state.password || !this.state.mailid)
        {
            window.M.toast({html: 'Enter all the details !!',classes:'red rounded'});
        }
        else
        {
            window.M.toast({html: 'Signing Up !!',classes:'orange rounded'});
            this.setState({submit:true})
            Axios.post(API_URL+'/user/signup',{data:this.state})
            .then(res=>{
                if(res.data === 'have')
                {
                  this.setState({submit:false})
                    window.M.toast({html: 'User already there !!',classes:'red rounded'});
                }
                else
                {
                    this.setState({submit:false})
                    window.M.toast({html: 'Signed Up !! Now you can signin !!',classes:'green darken-1 rounded'});
                }
            })
            .catch(err=>{
              this.setState({submit:false})
                window.M.toast({html: 'Something Went Wrong !!',classes:'red rounded'});
            })
        }
    }
    render()
    {
      if(this.state.submit)
      {
        return(
          <div className="cover_all">
                <div className="up-loader">
                  <div className="center">
                      <div className="box-frames" style={{margin:'20px'}}>
                          <div className="mloader-39 white-text"></div>
                      </div>
                  </div>
                  <h5 className="center white-text">
                      Processing your Submission !!
                  </h5>
                </div>
          </div>
        )
      }
      else
       {
        if(this.state.loading)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Loading....
                </h5>
                </React.Fragment>
            )
        }
        else{
        return(
          <React.Fragment>
            {this.state.renderStat ?
                <form class="form" onSubmit={this.submit}>
                  <input type="text" placeholder="Username" onChange={(e)=>this.setState({username:e.target.value})} />
                  <input type="email" placeholder="Email Id" onChange={(e)=>this.setState({mailid:e.target.value})} />
                  <input type="password" placeholder="Password" onChange={(e)=>this.setState({password:e.target.value})} />
                  <button type="submit" id="login-button" onClick={this.submit}>SIGNUP</button>
                  <br /><br />
                 <div className="center go" onClick={this.props.over}>LOG IN</div>
                </form>
              :
              <React.Fragment>
                <h6>We are not acceping signup requests !!</h6>
                <br />
                <button className="btn btn-small pink center" onClick={this.props.over}>LOGIN</button>
              </React.Fragment>
            }
          </React.Fragment>
        )
                    }
    }
  }
}
