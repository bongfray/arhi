const jwt = require('jsonwebtoken');
const secret = 'mysecretsshhh';

const withAuth = function(req, res, next) {
  const token =
      req.body.token ||
      req.query.token ||
      req.headers['x-access-token'] ||
      req.cookies.token;
console.log(token)
  if (!token) {
    res.status(401).send('Unauthorized: No token provided');
  } else {
    jwt.verify(token, secret, function(err, decoded) {
      if (err) {
        res.status(401).send('Unauthorized: Invalid token');
      } else {
        User.findOne({token:token},(err,data)=>{
          if(err)
          {

          }
          else if(data)
          {
            if(data.status === false || (Date.now()>data.expires))
            {
              console.log('expire');
              User.updateOne({token:token},{$set:{token:token,status:false,expires:0}},(err1,data1)=>{
                res.send('expire')
              })
            }
            else
            {
              console.log('ok');
              req.username = decoded.username;
              next();
            }
          }
          else
          {
            console.log('no');
            res.send('no-data')
          }
        })
      }
    });
  }
}

module.exports = withAuth;
