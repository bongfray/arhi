import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Error extends Component {
    render() {
        return (
            <div class="sea row">
                <div class="bubble"></div>
                <div class="submarine-wrapper">
                    <div class="submarine-body">
                        <div class="window"></div>
                        <div class="engine"></div>
                        <div class="light"></div>
                    </div>
                    <div class="helix"></div>
                    <div class="hat">
                    <div class="leds-wrapper">
                        <div class="periscope"></div>
                        <div class="leds"></div>
                    </div>
                    </div>
                </div>
                <p className="err-head center">Oops! <br/>You dived into some other canal!</p>
                <div className="col s2 l4 m3"></div>
                <div className="col s8 l4 m6">
                <Link to="/"> 
                 <p className="card center err-bck"><i className="fa fa-arrow-left" style={{marginRight:'10px'}}></i>Dive to the Peak</p>
                 </Link>
                </div>
                <div className="col s2 l4 m3"></div>
                

            </div>




        )
    }
}
