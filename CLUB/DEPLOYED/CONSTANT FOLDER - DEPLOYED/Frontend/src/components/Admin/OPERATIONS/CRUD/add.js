import React from 'react';
import M from 'materialize-css';


export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      action:'',
      name:'',
      photo:'',
      file:'',
      type:'',
      domain:'',
      r_type:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }
componentDidMount(){
  M.AutoInit();
  this.setState({
    action:this.props.action,
    type:this.props.type,
    recent:false,
  })
}
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    let validS;
      if(this.props.action ==='Team Member')
      {
        if(this.props.product)
        {
          validS = (!this.state.name || !this.state.r_type || !this.state.domain || !this.state.about_member
          || (!this.state.linkedin) || (!this.state.github) || (!this.state.gmail));
        }
        else{
          validS = (!this.state.name || !this.state.r_type || !this.state.domain || !this.state.about_member
          || (!this.state.linkedin) || (!this.state.github) || (!this.state.gmail) || (!this.state.file));
        }
      }
      else if(this.props.action ==='Sponsors')
      {
        if(this.props.product)
        {
        validS = (!this.state.name);
        }
        else{
          validS = (!this.state.name || (!this.state.file));
        }
      }
      else if(this.props.action ==='Achievments')
      {
        validS = (!this.state.name);
      }
      else if(this.props.action ==='Gallery')
      {

        validS = (!this.state.file);
      }
      else if(this.props.action ==='Vehicle')
      {
        if(this.props.product)
        {
        validS = ((!this.state.name));
        }
        else{
          validS = ((!this.state.name) || (!this.state.file));
        }
      }
      else if(this.props.action ==='Contact Details')
      {
        validS = ((!this.state.linkedin) || (!this.state.fb) || (!this.state.insta) || (!this.state.twitter) || (!this.state.youtube) ||
         (!this.state.phone) || (!this.state.gmail));
      }
      else{
        validS = ((!this.state.data));
      }
      if(validS)
      {
        return false;
      }
      else if(this.state.file)
      {
        if(this.state.file.size>400000)
        {
          window.M.toast({html:'File Size can not be more than 400KB !!',classes:'red rounded'})
        }
        else if((this.state.file.type !== ("image/png" || "image/jpeg" || "image/jpg")))
        {
          window.M.toast({html:'Only JPEG , PNG , JPG image allowed !!',classes:'red rounded'})
        }
        else
        {
          this.props.onFormSubmit(this.state);
          this.setState(this.initialState);
        }
      }
      else
      {
        this.props.onFormSubmit(this.state);
        this.setState(this.initialState);
      }
  }
  onChange=(e)=> {
    this.setState({file:e.target.files[0]});
}

  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS</h5>
}

    return(
      <div className="cover_all">
      <div className="card hoverable up" style={{padding:'10px'}}>
      <div className="center">{pageTitle}</div>
      <div>
        <div className="row">
            <div className="col l12 xl12 s12 m12">
                    {this.props.data.fielddata.map((content,index)=>(
                    <div className="row" key={index}>
                        <div className="col l12 xl12 s12 m12">
                         {content.placeholder === "none" ?
                            <React.Fragment>
                                <input style={{display:'none'}}
                                 type="file" onChange= {this.onChange}
                                 ref={fileInput=>this.fileInput = fileInput} />
                                <button className="btn small" onClick={()=>this.fileInput.click()}>Upload the Photo</button>
                            </React.Fragment>
                          :
                          <React.Fragment>
                             {content.select ?
                                 <select value={this.state.r_type} onChange={(e)=>this.setState({r_type:e.target.value})}>
                                    <option value="" defaultValue>Choose the Department</option>
                                    <option value="Faculty">Faculty</option>
                                    <option value="Alumni">Alumni</option>
                                    <option value="Head of Team">Head of Team</option>
                                    <option value="Member">Member</option>
                                 </select>
                              :
                              <React.Fragment>
                                {content.domain_type ?
                                 <select value={this.state.domain} onChange={(e)=>this.setState({domain:e.target.value})}>
                                    <option value="" defaultValue>Choose Domain</option>
                                    <option value="Mechanical">Mechanical</option>
                                    <option value="Software">Software</option>
                                    <option value="Electrical">Electrical</option>
                                    <option value="Corporate">Corporate</option>
                                    <option value="Web">Web</option>
                                  </select>
                                  :
                                  <React.Fragment>
                                      {content.size === false ?
                                        <div className="input-field col l12 xl12 s12 m12">
                                          <input id={content.name} type={content.type} name={content.name} value={this.state[content.name]}
                                            onChange={e => this.handleD(e, index)} className="validate" required/>
                                          <label htmlFor={content.name} className="label-c">{content.placeholder}</label>
                                        </div>
                                      :
                                      <div className="input-field">
                                            <textarea id={content.name} name={content.name} value={this.state[content.name]}
                                            onChange={e => this.handleD(e, index)} className="materialize-textarea"></textarea>
                                            <label htmlFor={content.name} className="label-c">{content.placeholder}</label>
                                        </div>
                                      }
                                  </React.Fragment>
                                }
                              </React.Fragment>
                             }
                          </React.Fragment>
                            }

                        </div>
                    </div>
                    ))}
                <div className="row">
                    <div className="col l6 xl6 s4 m6" />
                    <div className="col l6 xl6 s8 m6">
                            <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
                            <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
      </div>
      </div>
    )
  }
}
