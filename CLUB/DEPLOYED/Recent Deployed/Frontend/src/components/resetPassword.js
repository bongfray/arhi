import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import axios from 'axios'
import {API_URL} from '../utils/apiUrl';

export default class ResetPassword extends Component {
    constructor() {
        super()
        this.state = {
            redirectTo: null,
            password: '',
            con_password: '',
            token_come:'',
            loading:true,

        }
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleConPasswordChange = this.handleConPasswordChange.bind(this)
        this.handleResetPass = this.handleResetPass.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)

        }
            handlePasswordChange =(e) =>{
                this.setState({
                    password: e.target.value
                });
            }
            handleConPasswordChange =(e) =>{
                this.setState({
                    con_password: e.target.value
                });
            }
            handleResetPass =(e) =>{
              if(!(this.state.con_password)||!(this.state.password))
              {
                window.M.toast({html: 'Can not leave the field Empty !!',classes:'rounded red'});
              }
              else if(this.state.password !==this.state.con_password)
              {
                window.M.toast({html: 'Password Doesnot Match !!',classes:'rounded red'});
                this.setState({
                  password:'',
                  con_password:'',
                })
              }
              else{
                this.setState({loading:true})
                axios.post(API_URL+'/user/reset_from_mail', {
                    token_come: this.state.token_come,
                    password: this.state.password,
                  })
                   .then(response => {
                     //console.log(response)
                     if(response.status===200){
                       if(response.data.succ)
                        {
                          window.M.toast({html: response.data.succ,classes:'rounded green white-text'});
                          this.setState({
                            loading:false,
                             redirectTo: '/'
                         })
                       }
                     }
                    }).catch(error => {
                      window.M.toast({html: 'Internal Error !!',classes:'rounded red'});
                      this.setState({loading:false})
                    })
                  }
            }


        componentDidMount() {
           axios.get(API_URL+'/user/reset_password',{
             params: {
               resetPasswordToken: this.props.match.params.token,
             },
          }).then(response => {
            if(response.status === 200)
            {
              if(response.data.expire)
              {
                window.M.toast({html: 'Link Expired !!',classes:'rounded red'});
                this.setState({loading:false,redirectTo:'/'})
              }
              else
              {
                this.setState({
                  loading:false,
                  token_come: response.data,
                })
              }
            }
            else
            {
              window.M.toast({html: 'Internal Error !!',classes:'rounded red'});
              this.setState({loading:false})
            }
          })
        }
    render(){
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
          if(this.state.loading)
          {
            return(
              <div className="cover_all">
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="white-text mloader-39"></div>
                    </div>
                </div>
                <h5 className="center white-text">
                    Validating Your Request.....
                </h5>
              </div>
            );
          }
          else
          {
        return(

          <div className="row">
              <div className="col l4 xl4 m4"/>
               <div className="col l4 xl4 m4 s12">
                   <div className="card hoverable" style={{borderRadius:'10px',marginTop:'120px'}}>
                       <div className="card-content">
                           <span className="center card-title">RESET PASSWORD</span>
                           <div className="row">
                             <div className="input-field col s12 l12 xl12 m12">
                                     <input onChange={this.handlePasswordChange}
                                     id="pswd" value={this.state.password} type="password" className="validate" required />
                         		    <label htmlFor="pswd" className="label-c">Password</label>
                         			</div>
                              <div className="input-field col s12 l12 xl12 m12">
                                 <input onChange={this.handleConPasswordChange}
                                 id="cpswd" value={this.state.con_password} type="password" className="validate" required />
                                 <label htmlFor="cpswd" className="label-c">Confirm Password</label>
                              </div>
                           </div>
                           <div className="row">
                               <div className="col l12 xl12 s12 m12">
                                <button style={{width:'100%'}} type="submit" onClick={this.handleResetPass}
                                 className="btn pink right">Reset Password</button>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
              <div className="col l4 xl4 m4"/>
          </div>
        );
      }
    }
    }
}
