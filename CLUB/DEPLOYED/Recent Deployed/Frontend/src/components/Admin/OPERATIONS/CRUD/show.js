import React from 'react';
import axios from 'axios';
import ShowPage from '../vehicleDetails';
import M from 'materialize-css';
import Axios from 'axios';
import {API_URL} from '../../../../utils/apiUrl';

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      show_info:false,
      fetching:true,
      fullview:false,
      img:'',
      r_marked:false,
      r_vehicle:'',
      name_marked:false,
      lead_name:'',
      value_for_search:'',
      confirm_win:false,
      productId:'',
      rFiles:'',
      ac_id:'',
      submitting:false,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
    M.AutoInit();
    this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,id,index) => {
    this.setState({confirm_win:true,productId:productId,ac_id:id})
 }

deleteIt=()=>{
  this.setState({submitting:true})
  let url;
  if(this.props.action === "Sponsors")
  {
    url = '/user/del_sponsor';
  }
  else if((this.props.action === "Team Member") || (this.props.action === "Alumni Details") || (this.props.action === "Achievments")
  || (this.props.action === "Gallery") || (this.props.action === "Vehicle") || (this.props.action === "Contact Details"))
  {
    url ='/user/del_team';
  }
  else
  {
    url ='/user/del_vehicle_details';
  }

const { products } = this.state;
axios.post(API_URL+url,{
  serial: this.state.productId,
  id:this.state.ac_id,
})
    .then(response => {
      this.setState({
        response: response,
        products: products.filter(product => product.serial !== this.state.productId),
        productId:0,
        submitting:false,
        confirm_win:false,
     })
    })
}

componentDidUpdate=(prevProps)=>{
  if((prevProps.type !== this.props.type) || (prevProps.action !== this.props.action))
  {
    this.fetch();
  }
}

fetch =() =>{
  let url;
  if(this.props.action === "Sponsors")
  {
    url = '/user/fetch_sponsor';
  }
  else if((this.props.action === "Team Member") || (this.props.action === "Alumni Details")
   || (this.props.action === "Gallery") || (this.props.action === "Vehicle"))
  {
    url ='/user/fetch_team';
  }
  else if(this.props.action === 'Contact Details')
  {
    url ='/user/fetch_contact_details';
  }
  else if(this.props.action === 'Achievments')
  {
    url ='/user/fetch_achievment';
  }
  else
  {
    url ='/user/fetch_vdetails';
  }
  axios.post(API_URL+url,{
    action: this.props.action,
    type:this.props.type,
  })
  .then(response =>{
    if(response.data.files)
    {
      this.setState({
       products: response.data.found,
       rFiles:response.data.files,
       fetching:false,
     })
    }
    else
    {
      this.setState({
       products: response.data.found,
       fetching:false,
     })
    }

  })
  .catch(err=>{
    this.setState({fetching:false})
  })
}

recentVehicle=(v_name)=>{
  this.setState({r_marked:true,r_vehicle:v_name,submitting:true})
  Axios.post(API_URL+'/user/recent_vehicle',{action:'Vehicle',vehicle:v_name})
  .then(res=>{
    if(res.data)
    {
      this.setState({submitting:false})
      window.M.Toast({html: 'Assigned !!',classes:'green darken-1 rounded'})
    }
  })
  .catch(err=>{
    this.setState({submitting:false})
    console.log(err);
  })
}

markAsHead=(name,domain)=>{
  this.setState({name_marked:true,lead_name:name,submitting:true})
  Axios.post(API_URL+'/user/recent_head',{action:'Team Member',lead_name:name,domain:domain})
  .then(res=>{
    if(res.data)
    {
      window.M.Toast({html: 'Assigned !!',classes:'green darken-1 rounded'})
        this.setState({submitting:false})
    }
  })
  .catch(err=>{
  this.setState({submitting:false})
  })
}


  render() {
    if(this.state.fetching)
    {
      return(
        <React.Fragment>
        <div className="center">
            <div className="box-frames" style={{margin:'20px'}}>
                <div className="mloader-39"></div>
            </div>
        </div>
        <h5 className="center">
            Loading....
        </h5>
        </React.Fragment>
      )
    }
    else
    {
      if(this.state.submitting)
      {
        return(
          <div className="cover_all">
            <div className="center">
                <div className="box-frames" style={{margin:'20px'}}>
                    <div className="white-text mloader-39"></div>
                </div>
            </div>
            <h5 className="center white-text">
                Processing your request...
            </h5>
          </div>
        )
      }
      else
      {
      if(!this.state.products)
      {
        return(
          <div className="center">No Data</div>
        )
      }
      else
      {
      var { rFiles } = this.state;
      function setImage(data)
      {
        var datas =rFiles.filter(item=>item.filename === data);
        //console.log(data[0].filename)
        return datas[0].filename;
      }

      function delId(data)
      {
        var datas =rFiles.filter(item=>item.filename === data);
        //console.log(data[0]._id)
        return datas[0]._id;
      }

    var { products} = this.state;

    var libal;

    if(this.state.value_for_search.length > 0)
    {
      var  searchString = this.state.value_for_search.trim().toLowerCase();
        libal = products.filter(function(i) {
          return i.name.toLowerCase().match( searchString );
        });
    }
    else
    {
      libal = products;
    }
      return(
        <div>
          {this.state.confirm_win &&
            <div className="cover_all">
              <div className="center up" style={{padding:'15px'}}>
              <div>
                  <div className="card-content">
                  <h5 className="center">Confirmation of Deletion</h5><br /><br />
                 <div className="center">Rememeber the confirmation of this deltion will delete all datas related to this particular data set.
                 </div><br /><br />
                  </div>
                  <div className="card-action">
                    <div className="right">
                      <button className="btn transparent black-text" onClick={()=>this.setState({confirm_win:false})}>Cancel</button>&emsp;
                      <button className="btn red" onClick={this.deleteIt}>Delete</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          }
          {((this.props.action === "Team Member") || (this.props.action === "Achievments")
           || (this.props.action === "Vehicle") || (this.props.action === "Sponsors")) &&
                  <div className="input-field">
                    <input id="search" type="search" value={this.state.value_for_search} placeholder="Search By Only Name"
                       disabled={products.length>0 ? false : true}
                         onChange={(e)=>this.setState({value_for_search:e.target.value})} required/>
                    <label className="label-icon" htmlFor="search"></label>
                  </div>
            }

          {this.state.fullview &&
            <div className="cover_all">
              <div className="up">
                <div className="btn-floating btn-small red" style={{margin:'5px'}}>
                  <i className="material-icons right" onClick={()=>this.setState({fullview:false})}>close</i>
                </div><br />
                <div className="card">
                  <div className="card-image">
                     <img style={{maxWidth:'100%',height: '700px'}}
                    src={API_URL+'/user/admin_panel/:'+this.state.img} alt="here"/>
                  </div>
                </div>
              </div>
            </div>
          }
          {this.state.show_info &&
                <ShowPage close={()=>this.setState({show_info:false})} vehicle_name={this.state.vehicle_name} />
          }


          <div className="row">
            {libal.map((product,ind) => (
                 <div className="col s12 m6 l4 xl4" key={ind}>
                    <div className="card">
                      {this.props.image &&
                        <div className="card-image">
                         <img  style={{height:'200px'}} onClick={()=>this.setState({fullview:true,img:setImage(product.photo)})}
                          src = {API_URL+'/user/admin_panel/:'+setImage(product.photo)} alt = "something" />
                        </div>
                      }
                      <div className="card-content">
                        {this.props.data.fielddata.map((content,index)=>(
                          <React.Fragment key={index}>
                              {content.placeholder === 'none' ?
                               <React.Fragment></React.Fragment>
                               :
                               <div className="row">
                                    <div className="col l6 xl6 s6 m6">{content.header}</div>
                                    <div className="col l6 xl6 s6 m6">
                                       <div className={((content.name === ('alumni_journey')) || (content.name === 'about_member')) ? 'cut-text': ''}
                                       style={{wordWrap: 'break-word',whiteSpace: 'pre-line'}}>
                                         {product[content.name]}
                                       </div>
                                    </div>
                                  </div>
                               }
                          </React.Fragment>
                        ))}
                      </div>
                      <div className="card-action">
                        <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                      &nbsp;
                      {(!this.props.no_lead) &&
                        <i className="material-icons go" onClick={() => this.deleteProduct(product.serial,delId(product.photo),this.props.action)}>delete</i>
                      }
                      {this.props.action === 'Team Member' &&
                      <React.Fragment>
                        {(!this.props.no_lead) &&
                                <p>
                                    <label>
                                      <input className="with-gap"
                                      checked={product.recent ? true :(this.state.name_marked && this.state.lead_name === product.name)
                                       ? true : false}
                                      onChange={()=>this.markAsHead(product.name,product.domain)} type="radio"  />
                                      <span className="black-text">Mark As Head</span>
                                    </label>
                                  </p>
                          }
                      </React.Fragment>
                        }
                      {this.props.action === 'Vehicle' &&
                      <React.Fragment>
                         <div className="row">
                            <div className="col l6">

                                  <p>
                                    <label>
                                      <input className="with-gap"  checked={product.recent ? true :(this.state.r_marked && this.state.r_vehicle === product.name)
                                       ? true : false}
                                      onChange={()=>this.recentVehicle(product.name)} type="radio"  />
                                      <span className="black-text">Mark As Recent Vehicle</span>
                                    </label>
                                  </p>

                            </div>
                            <div className="col l6">
                                <div className="right btn pink" onClick={()=>this.setState({vehicle_name:product.name,show_info:true})}>Details</div>
                            </div>
                         </div>
                      </React.Fragment>
                        }
                      </div>
                    </div>
                  </div>

                  ))}
            </div>
        </div>
      )
    }
   }
  }

  }
}
