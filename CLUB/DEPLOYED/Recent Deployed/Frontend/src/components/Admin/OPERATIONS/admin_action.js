import React, { Component } from 'react'
import Axios from 'axios';
import {API_URL} from '../../../utils/apiUrl';


export default class SSSS extends Component {
    constructor()
    {
        super()
        this.state={
            status:false,
            status_team:false,
            status_alumni:false,
            loading:true,
            sponsor:0,
            team:0,
            alumni:0,
            submitting:false,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchStat();
        this.fetchSponsor();
    }
    fetchStat=()=>{
        Axios.post(API_URL+'/user/admin_action')
        .then(res=>{
             var signup = res.data.filter(item=>item.type === 'signup_stat');
             var team = res.data.filter(item=>item.type === 'team_render');
             var alumni = res.data.filter(item=>item.type === 'alumni_render');
             if(signup)
             {
               this.setState({status:signup[0].status})
             }
             if(team)
             {
               this.setState({status_team:team[0].status})
             }
             if(alumni)
             {
               this.setState({status_alumni:alumni[0].status})
             }
             this.setState({loading:false})
        })
    }
    setSignup=(e)=>{
        this.setState({action:e.target.value,status:!this.state.status,submitting:true})
        Axios.post(API_URL+'/user/setSignup',{action:'Admin Handle',type:e.target.value,status:!this.state.status})
        .then(res=>{
          this.setState({submitting:false})
        })
        .catch(err=>{
          this.setState({submitting:false})
        })
    }

    setTeamOn=(e)=>{
        this.setState({action:e.target.value,status_team:!this.state.status_team,submitting:true})
        Axios.post(API_URL+'/user/setTeam',{action:'Admin Handle',type:e.target.value,status:!this.state.status_team})
        .then(res=>{
          this.setState({submitting:false})
        })
        .catch(err=>{
          this.setState({submitting:false})
        })
    }

    setAlumniOn=(e)=>{
        this.setState({action:e.target.value,status_alumni:!this.state.status_alumni,submitting:true})
        Axios.post(API_URL+'/user/setAlumni',{action:'Admin Handle',type:e.target.value,status:!this.state.status_alumni})
        .then(res=>{
          this.setState({submitting:false})
        })
        .catch(err=>{
          this.setState({submitting:false})
        })
    }

    fetchSponsor=()=>{
        Axios.post(API_URL+'/user/fetch_sponsor',{action:'Sponsors'})
        .then(res=>{
            this.setState({sponsor:res.data.found.length})
            this.fetchTeam();
        })
        .catch(err=>{
            this.setState({loading:false})
            if(err.response.status === 404)
            {
              window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
            }
            else
            {
              window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
            }
        })

    }
    fetchTeam=()=>{
        Axios.post(API_URL+'/user/fetch_team',{action:'Team Member'})
        .then(res=>{
            let alumni = (res.data.found.filter(item=>item.r_type === 'Alumni')).length;
            this.setState({team:res.data.found.length-alumni,alumni:alumni,loading:false})
        })
        .catch(err=>{
            this.setState({loading:false})
            if(err.response.status === 404)
            {
              window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
            }
            else
            {
              window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
            }
        })
    }

    render()
    {
        if(this.state.loading)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Loading....
                </h5>
                </React.Fragment>
            )
        }
        else
        {
          if(this.state.submitting)
          {
            return(
              <div className="cover_all">
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="white-text mloader-39"></div>
                    </div>
                </div>
                <h5 className="center white-text">
                    Processing your request...
                </h5>
              </div>
            );
          }
          else{
        return(
            <React.Fragment>
                <div className="row">
                    <div className="col l4 xl4 s12 m4 card">
                        Render Signup Page -
                            <p>
                                <label>
                                    <input checked={this.state.status} type="checkbox" value="signup_stat"
                                    onChange={this.setSignup}/>
                                    <span>Status</span>
                                </label>
                            </p>
                    </div>
                    <div className="col l4 xl4 s12 m4 card">
                        Render Team Data Page -
                            <p>
                                <label>
                                    <input checked={this.state.status_team} type="checkbox" value="team_render"
                                    onChange={this.setTeamOn}/>
                                    <span>Status</span>
                                </label>
                            </p>
                    </div>
                    <div className="col l4 xl4 s12 m4 card">
                        Render Alumni Data Page -
                            <p>
                                <label>
                                    <input checked={this.state.status_alumni} type="checkbox" value="alumni_render"
                                    onChange={this.setAlumniOn}/>
                                    <span>Status</span>
                                </label>
                            </p>
                    </div>
                </div>
                <div className="row"><h5 className="">Details of the System</h5></div>
                <div className="row">
                    <div className="col l4 xl4 s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Sponsors</span>
                                <h4 className="center">{this.state.sponsor}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col l4 xl4 s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Team Members</span>
                                <h4 className="center">{this.state.team}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col l4 xl4 s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Alumnis</span>
                                <h4 className="center">{this.state.alumni}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
       }
      }
    }
}
