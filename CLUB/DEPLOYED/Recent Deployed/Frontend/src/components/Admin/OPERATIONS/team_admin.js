import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';
import {API_URL} from '../../../utils/apiUrl';

export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      uploading:false,
      loaded_percent:0,
      redirectTo:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
  M.AutoInit();
}


   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;

       apiUrl = '/user/team_details';

     const formData = new FormData();
     formData.append('myImage',data.file);
     formData.append('name',data.name);
     formData.append('domain',data.domain);
     formData.append('r_type',data.r_type);

     if(data.about_member)
      {
        formData.append('about_member',data.about_member);
      }
    if(data.alumni_journey)
      {
        formData.append('alumni_journey',data.alumni_journey);
      }
    if(data.linkedin)
      {
        formData.append('linkedin',data.linkedin);
      }
    if(data.gmail)
      {
        formData.append('gmail',data.gmail);
      }
    if(data.github)
      {
        formData.append('github',data.github);
      }
     formData.append('action',data.action);
     formData.append('serial',data.serial);
     this.setState({uploading:true})
     window.M.toast({html: 'Uploading ....!!',classes:'orange rounded'});
     axios.post(API_URL+apiUrl,formData,{
         onUploadProgress:ProgressEvent=>{
              var loaded = Math.round(ProgressEvent.loaded/ProgressEvent.total*100);
              //console.log(loaded)
              this.setState({loaded_percent:loaded})
         }
     })
         .then(response => {
          if(response.data === 'have')
          {
           window.M.toast({html: 'Already There !!',classes:'pink rounded'});
           this.setState({uploading:false})
          }
          else
          {
            window.M.toast({html: 'Saved !!',classes:'green darken-1 rounded'});

           this.setState({
            uploading:false,
            loaded_percent:0,
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
          }
         })
         .catch(err=>{
             this.setState({uploading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })

   }

   editProduct = (productId,index)=> {
     this.setState({loading:true})
     axios.post(API_URL+'/user/fetch_to_edit_team',{
       id: productId,
     })
         .then(response => {
           this.setState({
             loading:false,
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })
         .catch(err=>{
             this.setState({loading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
            {
              header: "Member Name",
              name: "name",
              placeholder: "Enter the Member Name",
              type: "text",
              size:false,
              alumni:true,
            },
            {
              header: "Responsibility Type",
              name: "r_type",
              placeholder: "Enter the Responsibility Type",
              type: "text",
              select:true,
              alumni:true,
            },
            {
            header: "Domain",
            name: "domain",
            placeholder: "Enter the Domain Type",
            type: "text",
            domain_type:true,
          },
          {
            header: "About The User",
            name: "about_member",
            placeholder: "Brief about the team member",
            type: "text",
            size:true,
            alumni:true,
          },
          {
            header: "Their Journey",
            name: "alumni_journey",
            placeholder: "Brief about Journey",
            type: "text",
            size:true,
            alumni:true,
          },
          {
            header: "LinkedIn Account",
            name: "linkedin",
            placeholder: "Enter LinkedIn Account",
            type: "text",
            size:false,
            alumni:true,
          },
        {
          header: "Gmail Account",
          name: "gmail",
          placeholder: "Enter Gmail Account",
          type: "text",
          size:false,
          alumni:true,
        },
        {
          header: "GitHub Account",
          name: "github",
          placeholder: "Enter GitHub Account",
          type: "text",
          size:false,
        },
        {
            header: "Uploaded Photo",
            name: "photo",
            placeholder: "none",
            type: "text",
          },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct loaded_percent={this.state.loaded_percent} cancel={this.updateState} username={this.state.username}
     action={"Team Member"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
       if(this.state.uploading)
       {
         return(
           <div className="cover_all">
           <div className="center">
               <div className="box-frames" style={{margin:'20px'}}>
                   <div className="mloader-39 white-text"></div>
               </div>
           </div>
           <h5 className="center white-text">
               Uploading Your Data.... <span className="yellow-text">{this.state.loaded_percent} %</span>
           </h5>
           </div>
         );
       }
    else{
      if(this.state.loading)
      {
        return(
          <div className="cover_all">
            <div className="center">
                <div className="box-frames" style={{margin:'20px'}}>
                    <div className="white-text mloader-39"></div>
                </div>
            </div>
            <h5 className="center white-text">
                Processing your request...
            </h5>
          </div>
        );
      }
         else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Team Member</h5>
                    <br />
                    {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                    <button className="btn right blue-grey darken-2 sup subm"
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct &&
                            <ProductList image={true} username={this.props.username} title={this.state.option}
                            action={"Team Member"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
          }
        }
}
  }
}
