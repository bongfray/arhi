import React, { Component } from 'react'

import Mech from './mech.jpg'
import Soft from './soft.jpg'
import Elect from './elect.jpg'
import Corp from './corp.jpg'

export default class Domain extends Component {
    render() {
        return (

            <React.Fragment>
                <div className="domain">
                <h1 className="center tag-head dom-cont" data-aos='zoom-in'>Domains In SRM AUV</h1><br/>

                <div className="row">
                    <div className="col s1"></div>
                    <div className="col s10 card product" data-aos='fade-up' >
                        <div className="ribbon-wrapper">
                            <div className="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">MECHANICAL</h4>
                            <p className="d-content">•Responsible for designing frame work & designing AUV.
                                        <br/>•Designing a neutrally or positively buoyant & leak proof vehicle.
                                        <br/>•Designing and fabrication battery pod, camera pod & casing for various electronic components.
                                        <br/>•Done using Solid Works & Ansys.
                                    </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                <div className="row">
                    <div className="col s1"></div>
                    <div className="col s10 card product" data-aos='fade-up' >
                        <div className="ribbon-wrapper">
                            <div className="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">Software</h4>
                            <p className="d-content">•Software domain ensures movement of vehicle through image processing.
                                <br/>•Designing algorithms to give input to on board computer JETSON TX1.
                                <br/>•The software also look upon vehicles navigation and sensor fusion.
                                <br/>•Web Development & Robotics are being considered software domain.
                            </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                <div className="row">
                    <div className="col s1"></div>
                    <div className="col s10 card product" data-aos='fade-up' >
                        <div className="ribbon-wrapper">
                            <div className="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">Electrical</h4>
                            <p className="d-content">•The electrical and electronic components of the vehicle are incorporated safely on backplane within the hull of the vehicle.
                                <br/>•Electrtical and Electronics  infrastructure consists of power management system of the vehicle, Acoustic signal processing, and various payload electronics.
                            </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                <div className="row">
                    <div className="col s1"></div>
                    <div className="col s10 card product" data-aos='fade-up' >
                        <div className="ribbon-wrapper">
                            <div className="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">Corporate</h4>
                            <p className="d-content">•Non technical domain of SRM AUV, Corporate domain forms a critical aspect of the operations of the team.
                                <br/>•The challenges range from finance, availability of the tech, sponsorship, public relationship and social media awareness.
                                <br/>•Also known as the face of the company in colloquial terms.
                            </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>





                <div className="containerr" >


                        <div className="box" data-aos='zoom-out'  >
                            <div className="imgBx">
                                <img alt="domain" src={Mech} />
                            </div>
                            <div className="content">
                            <h2 className="dim-h4">Mechanical</h2>
                            <p className="content-p">•Responsible for designing frame work & designing AUV.
                                <br/>•Designing a neutrally or positively buoyant & leak proof vehicle.
                                <br/>•Designing and fabrication battery pod, camera pod & casing for various electronic components.
                                <br/>•Done using Solid Works & Ansys.
                            </p>
                            </div>
                        </div>



                        <div className="box" data-aos='zoom-out' >
                            <div className="imgBx">
                                <img alt="domain" src={Soft} />
                            </div>
                            <div className="content">
                            <h2 className="dim-h4">Software</h2>
                            <p className="content-p">•Software domain ensures movement of vehicle through image processing.
                                <br/>•Designing algorithms to give input to on board computer JETSON TX1.
                                <br/>•The software also look upon vehicles navigation and sensor fusion.
                                <br/>•Web Development & Robotics are being considered software domain.

                            </p>
                            </div>
                        </div>



                        <div className="box" data-aos='zoom-out' >
                            <div className="imgBx">
                                <img alt="domain" src={Elect} />
                            </div>
                            <div className="content">
                            <h2 className="dim-h4">Electrical</h2>
                            <p className="content-p">•The electrical and electronic components of the vehicle are incorporated safely on backplane within the hull of the vehicle.
                                <br/>•Electrtical and Electronics  infrastructure consists of power management system of the vehicle, Acoustic signal processing, and various payload electronics.
                            </p>
                            </div>
                        </div>



                        <div className="box" data-aos='zoom-out' >
                            <div className="imgBx">
                                <img alt="domain" src={Corp} />
                            </div>
                            <div className="content">
                            <h2 className="dim-h4">Corporate</h2>
                            <p className="content-p">•Non technical domain of SRM AUV, Corporate domain forms a critical aspect of the operations of the team.
                                <br/>•The challenges range from finance, availability of the tech, sponsorship, public relationship and social media awareness.
                                <br/>•Also known as the face of the company in colloquial terms.


                            </p>
                            </div>
                        </div>

                </div>

                </div>

            </React.Fragment>

            )
    }
}
