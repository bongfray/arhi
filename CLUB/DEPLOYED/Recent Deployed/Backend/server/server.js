const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
const dbConnection = require('./database')
const cors = require("cors");


const app = express()
const PORT = process.env.PORT || 8080


const secret = 'mysecretsshhh';

const user = require('./routes/user')

app.use(cors());
app.use(morgan('dev'))
app.use(
	bodyParser.urlencoded({
		extended: true
	})
)
app.use(bodyParser.json());

// Routes
app.use('/user', user)

app.get('/', (req, res) => {
  res.send('accepted')
})



const serve = app.listen(PORT, () => console.log('App listening on port'+PORT));

process.on('SIGINT', () => {
  console.log('Gracefully Shutting Down !!');
  serve.close(() => {
    console.log('Http server closed O[ps].');
    mongoose.connection.close(false, () => {
      console.log('MongoDb connection closed.');
      process.exit(0);
    });
  });
});
