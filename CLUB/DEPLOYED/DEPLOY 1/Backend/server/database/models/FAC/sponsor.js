const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');
//Schema
const sponsorschema = new Schema({
  action:{ type: String, unique: false, required: false },
  name: { type: String, unique: false, required: false },
  photo: { type: String, unique: false, required: false },
  createdat: { type: Date, unique: false, required: false },
  updatedat: { type: Date, unique: false, required: false },
})


sponsorschema.plugin(autoIncrement.plugin, { model: 'Sponsor', field: 'serial', startAt: 1,incrementBy: 1 });

const Sponsor = mongoose.model('Sponsor', sponsorschema)
module.exports = Sponsor

