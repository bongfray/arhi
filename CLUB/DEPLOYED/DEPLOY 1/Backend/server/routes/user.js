const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const User = require('../database/models/FAC/user')
const Sponsor = require('../database/models/FAC/sponsor')
const Team = require('../database/models/FAC/team')
const VDetails = require('../database/models/FAC/vehicleDetails')
const crypto = require('crypto');
const multer = require("multer");
const path = require("path");
const GridFsStorage = require("multer-gridfs-storage");
const uri = 'mongodb+srv://arijit:arijit92@cluster0-szr4d.mongodb.net/test?retryWrites=true&w=majority';
const mongoose = require('mongoose')
mongoose.Promise = global.Promise

var nodemailer = require('nodemailer');


var connection = mongoose.createConnection(uri,{ useNewUrlParser: true,useUnifiedTopology: true});

let gfs;
connection.once("open", () => {
console.log('inside');
gfs = new mongoose.mongo.GridFSBucket(connection.db, {
  bucketName: "uploads"
});
});



const storage = new GridFsStorage({
url: uri,
file: (req, file) => {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(16, (err, buf) => {
      if (err) {
        return reject(err);
      }
      const filename = buf.toString("hex") + path.extname(file.originalname);
      console.log('File '+file)
      const fileInfo = {
        filename: filename,
        bucketName: "uploads"
      };
      resolve(fileInfo);
    });
  });
}
});

const upload = multer({
  storage: storage,
  limits:{fileSize:400000},
}).single("myImage");



router.get('/check', function(req, res) {
    var ip =getClientIp(req);
    console.log(ip)
  User.findOne({$and:[{ip:ip},{status:true}]},(err,found)=>{
    if(err)
    {
        res.status(500).send('Error!!');
    }
    else if(found){
      res.send('ok')
    }
    else{
      res.send('no')
    }
  })
});


router.post('/signup', (req, res) => {
  User.findOne({username: req.body.data.username}, (err, user) => {
      if (err)
      {
         res.status(500).send('Error!!');
      }
      else if (user)
      {
          res.send('have');
      }
      else {
        const newUser = new User(req.body.data)
          newUser.save((err, savedUser) => {
            if(err)
            {
              res.status(500).send('Error Occoured');
            }
            else if(savedUser)
            {
              res.send('done');
            }
          })
      }
  })
})


function getClientIp(req) {
  var ipAddress;
  var forwardedIpsStr = req.header('x-forwarded-for');
  if (forwardedIpsStr) {
    var forwardedIps = forwardedIpsStr.split(',');
    ipAddress = forwardedIps[0];
  }
  if (!ipAddress) {
    ipAddress = req.connection.remoteAddress;
  }
  return ipAddress;
};


router.post('/signin', function(req, res) {
  var ip =getClientIp(req);
    console.log(ip)
  User.findOne({$and:[{username:req.body.data.username},{ip:ip},{status:true}]},(err,found)=>{
    if(err)
    {
      res.status(500).send('Error!!');
    }
    else if(found)
    {
      res.send('have')
    }
    else
    {
    //  console.log('inside');
      User.findOne({username:req.body.data.username},(error,found2)=>{
        if(error)
        {
            res.status(500).send('Error!!');
        }
        else if (found2) {
        //  console.log('inside found');
          if(!(bcrypt.compareSync(req.body.data.password,found2.password)))
          {
            res.send('w pass')
          }
          else
          {
            User.updateOne({username:req.body.data.username},{$set:{ip:ip,status:true}},(err1,found1)=>{
              if(err1)
              {
                res.status(500).send('Error!!');
              }
              else(found1)
              {
                  res.send('ok');
              }
            })
          }
        }
        else
        {
            res.send('no user')
        }
      })

    }
  })
})

router.post('/logout', (req, res) => {
var ip =getClientIp(req);
  User.updateOne({ip:ip},{$set:{status:false,ip:''}},(err,found)=>{
    if(err)
    {
      res.status(500).send('Error!!');
    }
    else {
      //console.log(found);
      res.send('done')
    }
  })
})


router.get('/dash2', function(req, res) {
  var ip =getClientIp(req);
  console.log(ip)
  User.findOne({$and:[{ip:ip},{status:true}]}, function(err, objs){
    if(err)
    {
      res.status(500).send('Error!!');
    }
      else if (objs)
      {
        //console.log('find')
          res.send(objs);
      }
      else
      {
      //  console.log('no find')
        res.send('no')
      }
  });

});

router.post('/newd', (req, res) => {
    var ip =getClientIp(req);
  User.findOne({$and:[{ip:ip},{status:true}]}, (err, objs) => {
      if (err)
      {
        res.status(500).send('Error!!');
      }
      else if(objs)
      {
       if(req.body.new_password) {
        if(bcrypt.compareSync(req.body.current_password, objs.password)) {
          let hash = bcrypt.hashSync(req.body.new_password, 10);
          User.updateOne({$and:[{ip:ip},{status:true}]},{ $set:
            {password: hash}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "fail"
          };
          res.send(fail);
        }
      }
      else if(!req.body.new_password)
      {
        User.updateOne({$and:[{ip:ip},{status:true}]},
          { $set: { mailid: req.body.up_username}} ,(err, user) => {
          var succ= {
            succ: "Datas are Updated",
          };
          res.send(succ);
        })
      }
    }
  })
}
)


router.post('/forgo', (req, res) => {
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'srmauvofficial@gmail.com',
      pass: 'gcclueaajtmtnglt'
       }
   });
   User.findOne(
     {$and:[{mailid: req.body.data.mailid},{username:req.body.data.username}]
     },
   ).then(user => {
     if(user === null)
     {
       var nodata ={
         nodata:'INVALID USER !!'
       }
       res.send(nodata);
     }
     else{
       const token = crypto.randomBytes(20).toString('hex');
       // console.log(token);
       User.updateOne({$and:[{mailid: req.body.data.mailid},{username:req.body.data.username}]},
         { $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 1800000,}} ,(err, user) => {
       })
       const mailOptions = {
        from: 'srmauvofficial@gmail.com', // sender address
        to: req.body.data.mailid, // list of receivers
        subject: 'Reset Password:  Team AUV', // Subject line
        html: '<p>You are receiving this message because you have requested for reset password in SRM-AUV website</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset your password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 30min from now.</p>'
      };
      // console.log("Way to enter.......");
      transporter.sendMail(mailOptions, function (err, info) {
        if(err)
        {
          res.status(500).send('error');
        }
        else{
          //console.log(info)
          res.send('done')
        }
     });
     }
   })
})
    router.get('/reset_password', function(req, res) {
      User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
        if (!user) {
          // console.log("Expired");
          var expire ={
            expire:"Link is Expired"
          }
          res.send(expire);
        }
        else if(user){
          res.send(user.resetPasswordToken)
        }
      });
    });




router.post('/setSignup', function(req, res) {
  Team.findOne({action:req.body.action},(err,found)=>{
    if(err)
    {
      res.status(500).send('error');
    }
    else if(found)
    {
      //console.log(req.body)
      Team.updateOne({action:req.body.action},{status:req.body.status},(error,done)=>{
        if(error)
        {

        }
        else{
          //console.log(done)
          res.send('done')
        }
      })
    }
    else{
      var newdata= new Team(req.body);
      newdata.save((error1,saved)=>{
        if(error1)
        {
          res.status(500).send('error');
        }
        else if(saved)
        {
          res.send('done')
        }
        else{
          res.send('no')
        }
      })
    }
  })
});

router.post('/admin_action', function(req, res) {
  Team.findOne({action:req.body.action},(err,found)=>{
    if(err)
    {
      res.status(500).send('error');
    }
    else if(found)
    {
      res.send(found);
    }
    else{
      res.send('no');
    }
  })
});


router.post('/recent_vehicle', function(req, res) {
  Team.findOne({$and:[{recent:true},{action:req.body.action}]},(err,found)=>{
    if(err)
    {
      res.status(500).send('error');
    }
    else if(found)
    {
      if(found.name === req.body.vehicle)
      {
        Team.updateOne({$and:[{name:req.body.vehicle},{action:req.body.action}]},{recent:true},(error,done)=>{
          if(error)
          {
            res.status(500).send('Error!!');
          }
          else
          {
            res.send('done')
          }
        })
      }
      else
      {
        Team.updateOne({$and:[{recent:true},{action:req.body.action}]},{recent:false},(error1,did)=>{
          if(error1)
          {
            res.status(500).send('Error!!');
          }
          else{
            Team.updateOne({$and:[{name:req.body.vehicle},{action:req.body.action}]},{recent:true},(error,done)=>{
              if(error)
              {
                res.status(500).send('Error!!');
              }
              else
              {
                res.send('done')
              }
            })
          }
        })

      }
    }
    else{
      Team.updateOne({$and:[{name:req.body.vehicle},{action:req.body.action}]},{recent:true},(error,done)=>{
        if(error)
        {
          res.status(500).send('Error!!');
        }
        else
        {
          res.send('done')
        }
      })
    }
  })
});


router.post('/recent_head', function(req, res) {
  Team.findOne({$and:[{recent:true},{action:req.body.action}]},(err,found)=>{
    if(err)
    {
      res.status(500).send('error');
    }
    else if(found)
    {
      if(found.name === req.body.vehicle)
      {
        Team.updateOne({$and:[{name:req.body.lead_name},{action:req.body.action}]},{recent:true},(error,done)=>{
          if(error)
          {
            res.status(500).send('Error!!');
          }
          else
          {
            res.send('done')
          }
        })
      }
      else
      {
        Team.updateOne({$and:[{recent:true},{action:req.body.action}]},{recent:false},(error1,did)=>{
          if(error1)
          {
            res.status(500).send('Error!!');
          }
          else{
            Team.updateOne({$and:[{name:req.body.lead_name},{action:req.body.action}]},{recent:true},(error,done)=>{
              if(error)
              {
                res.status(500).send('Error!!');
              }
              else
              {
                res.send('done')
              }
            })
          }
        })

      }
    }
    else{
      Team.updateOne({$and:[{name:req.body.lead_name},{action:req.body.action}]},{recent:true},(error,done)=>{
        if(error)
        {
          res.status(500).send('Error!!');
        }
        else
        {
          res.send('done')
        }
      })
    }
  })
});

router.post("/uploadD",(req, res) => {
  upload(req, res, (err) => {
    if(err)
    {
      //console.log('no uploaded')
      return res.status(500).send('Upload Not Completed');
    }
    else
    {
      Sponsor.findOne({name:req.body.spons_name},(error,found)=>{
        if(error)
        {
          res.status(500).send('Error!!');
        }
        else if(found)
        {
          if(found.name === req.body.spons_name)
          {
            if(!(req.body.serial === undefined))
            {
              Sponsor.findOne({serial:req.body.serial},(error1,found1)=>{
                if(error1)
                {
                  res.status(500).send('error')
                }
                else
                {
                  var f;
                  gfs.find().toArray((err, files) => {
                    if(err)
                    {
                      res.status(500).send('error data')
                    }
                    if (!files || files.length === 0)
                    {
                      res.status('404').send('no data')
                    }
                    else
                    {
                     f = files
                        .map(file => {
                          if (
                            file.contentType === "image/png" ||
                            file.contentType === "image/jpeg"
                          ) {
                            file.isImage = true;
                          } else {
                            file.isImage = false;
                          }
                          return file;
                        })
                        .sort((a, b) => {
                          return (
                            new Date(b["uploadDate"]).getTime() -
                            new Date(a["uploadDate"]).getTime()
                          );
                        });
                        var datas = f.filter(item=>item.filename ===found1.photo);
                        var id = datas[0]._id;
                        gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                          if (err){
                            return res.status(404).json({ err: err.message });
                          }
                          else
                          {
                            Sponsor.updateOne({serial:req.body.serial},{$set:{photo:req.file.filename}},(error,found)=>{
                              res.send('done')
                            })
                          }
                        });
                    }
                  });
                }
              })
            }
            else
            {
              res.send('have')
            }
          }
          else
          {
            if(req.file)
            {
              Sponsor.findOne({serial:req.body.serial},(error1,found1)=>{
                if(error1)
                {
                  res.status(500).send('error')
                }
                else
                {
                  var f;
                  gfs.find().toArray((err, files) => {
                    if(err)
                    {
                      res.status(500).send('error data')
                    }
                    if (!files || files.length === 0)
                    {
                      res.status('404').send('no data')
                    }
                    else
                    {
                     f = files
                        .map(file => {
                          if (
                            file.contentType === "image/png" ||
                            file.contentType === "image/jpeg"
                          ) {
                            file.isImage = true;
                          } else {
                            file.isImage = false;
                          }
                          return file;
                        })
                        .sort((a, b) => {
                          return (
                            new Date(b["uploadDate"]).getTime() -
                            new Date(a["uploadDate"]).getTime()
                          );
                        });
                        var datas = f.filter(item=>item.filename ===found1.photo);
                        var id = datas[0]._id;
                        gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                          if (err){
                            return res.status(404).json({ err: err.message });
                          }
                          else
                          {
                            Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name,photo:req.file.filename}},(error,found)=>{
                              res.send('done')
                            })
                          }
                        });
                    }
                  });
                }
              })
            }
            else
            {
              Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name}},(error,found)=>{
                res.send('done')
              })
            }
          }
        }
        else
        {
          if(!(req.body.serial === 'undefined'))
          {
            if(req.file)
            {
              Sponsor.findOne({serial:req.body.serial},(error1,found1)=>{
                if(error1)
                {
                  res.status(500).send('error')
                }
                else
                {
                  var f;
                  gfs.find().toArray((err, files) => {
                    if(err)
                    {
                      res.status(500).send('error data')
                    }
                    if (!files || files.length === 0)
                    {
                      res.status('404').send('no data')
                    }
                    else
                    {
                     f = files
                        .map(file => {
                          if (
                            file.contentType === "image/png" ||
                            file.contentType === "image/jpeg"
                          ) {
                            file.isImage = true;
                          } else {
                            file.isImage = false;
                          }
                          return file;
                        })
                        .sort((a, b) => {
                          return (
                            new Date(b["uploadDate"]).getTime() -
                            new Date(a["uploadDate"]).getTime()
                          );
                        });
                        var datas = f.filter(item=>item.filename ===found1.photo);
                        var id = datas[0]._id;
                        gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                          if (err){
                            return res.status(404).json({ err: err.message });
                          }
                          else
                          {
                            Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name,photo:req.file.filename}},(error,found)=>{
                              res.send('done')
                            })
                          }
                        });
                    }
                  });
                }
              })
            }
            else
            {
              //console.log('Dagss')
              Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name}},(error,found)=>{
                res.send('done')
              })
            }
          }
          else
          {
            const newSponser = new Sponsor({
              name:req.body.spons_name,
              photo:req.file.filename,
              })
            newSponser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
          }
        }
      })
    }
 });

});

router.post('/fetch_sponsor', (req, res) => {
  var f;
  Sponsor.find({},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else
    {
      if(!gfs) {
        //console.log("some error occured, check connection to db");
        res.send("some error occured, check connection to db");
        process.exit(0);
      }
      else{
        gfs.find().toArray((err, files) => {
          //console.log('inside array');
          if(err)
          {
            //console.log('here err');
              res.status(500).send('err');
          }
          if (!files || files.length === 0)
          {
            //console.log('here no');
             res.status(404).send('err');
          }
          else
          {
            //console.log('grt e');
           f = files
              .map(file => {
                if (
                  file.contentType === "image/png" ||
                  file.contentType === "image/jpeg"
                ) {
                  file.isImage = true;
                } else {
                  file.isImage = false;
                }
                return file;
              })
              .sort((a, b) => {
                return (
                  new Date(b["uploadDate"]).getTime() -
                  new Date(a["uploadDate"]).getTime()
                );
              });
              //console.log('find file');
              //console.log(f);
              var sendData={
                found:found,
                files:f,
              }
              res.send(sendData)
          }

        });
      }
    }
  })
})

router.get('/admin_panel/:id' , (req , res) => {
  //console.log('inside check');
  var id = req.params.id.slice(1);
  gfs.find({
       filename: id
     })
     .toArray((err, files) => {
      // console.log(files);
       if (!files || files.length === 0) {
         return res.status(404).json({
           err: "no files exist"
         });
       }
       gfs.openDownloadStreamByName(files[0].filename).pipe(res);
     });
});

router.post('/fetch_to_edit_sponsor',function(req,res) {
  Sponsor.findOne({$and: [{serial: req.body.id}]}, function(err, docs){
    if(err)
    {
      res.status(500).send('Error!!');
    }
    else
    {
      res.send(docs)
    }
 });
})




router.post('/del_sponsor',function(req,res) {
//console.log(req.body.id);
var id = req.body.id.toString();
  gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
    //console.log(err);
    //console.log(data);
    if (err){
      return res.status(404).json({ err: err.message });
    }
    else{
      Sponsor.deleteOne({$and: [{serial:  req.body.serial }]},function(err,succ){
        res.send("OK")
      });
    }
  });
});


router.post('/team_details', (req, res) => {
  upload(req, res, (err) => {
    if(err)
    {
      return res.status(500).send('Upload Not Completed');
    }
    else
    {
      Team.findOne({name:req.body.name},(error,found)=>{
        if(error)
        {
          res.status(500).send('Error!!');
        }
        else if(found)
        {
          console.log('Fouund Here')
          if(found.name === req.body.name)
          {
            console.log('As')
            if(!(req.body.serial === undefined))
            {
              if(req.file)
              {
                Team.findOne({$and:[{serial:req.body.serial},{action:req.body.action}]},(error1,found1)=>{
                  if(error1)
                  {
                    res.status(500).send('error')
                  }
                  else
                  {
                    var f;
                    gfs.find().toArray((err, files) => {
                      if(err)
                      {
                        res.status(500).send('error data')
                      }
                      if (!files || files.length === 0)
                      {
                        res.status('404').send('no data')
                      }
                      else
                      {
                       f = files
                          .map(file => {
                            if (
                              file.contentType === "image/png" ||
                              file.contentType === "image/jpeg"
                            ) {
                              file.isImage = true;
                            } else {
                              file.isImage = false;
                            }
                            return file;
                          })
                          .sort((a, b) => {
                            return (
                              new Date(b["uploadDate"]).getTime() -
                              new Date(a["uploadDate"]).getTime()
                            );
                          });
                          var datas = f.filter(item=>item.filename ===found1.photo);
                          var id = datas[0]._id;
                          gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                            if (err){
                              return res.status(404).json({ err: err.message });
                            }
                            else
                            {
                              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,photo:req.file.filename,
                                action:req.body.action,
                                domain: req.body.domain,
                                r_type:req.body.r_type,
                                about_member: req.body.about_member,
                                alumni_journey: req.body.alumni_journey,
                                linkedin: req.body.linkedin,
                                gmail: req.body.gmail,
                                github: req.body.github,
                                }},(error,found)=>{
                                res.send('done')
                              })
                            }
                          });
                      }
                    });
                  }
                })
              }
              else
              {
                Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,
                  action:req.body.action,
                  domain: req.body.domain,
                  r_type:req.body.r_type,
                  about_member: req.body.about_member,
                  alumni_journey: req.body.alumni_journey,
                  linkedin: req.body.linkedin,
                  gmail: req.body.gmail,
                  github: req.body.github,}},(error,found)=>{
                  res.send('done')
                })
              }

            }
            else
            {
              res.send('have')
            }
          }
          else
          {
            if(req.file)
            {
              Team.findOne({$and:[{serial:req.body.serial},{action:req.body.action}]},(error1,found1)=>{
                if(error1)
                {
                  res.status(500).send('error')
                }
                else
                {
                  var f;
                  gfs.find().toArray((err, files) => {
                    if(err)
                    {
                      res.status(500).send('error data')
                    }
                    if (!files || files.length === 0)
                    {
                      res.status('404').send('no data')
                    }
                    else
                    {
                     f = files
                        .map(file => {
                          if (
                            file.contentType === "image/png" ||
                            file.contentType === "image/jpeg"
                          ) {
                            file.isImage = true;
                          } else {
                            file.isImage = false;
                          }
                          return file;
                        })
                        .sort((a, b) => {
                          return (
                            new Date(b["uploadDate"]).getTime() -
                            new Date(a["uploadDate"]).getTime()
                          );
                        });
                        var datas = f.filter(item=>item.filename ===found1.photo);
                        var id = datas[0]._id;
                        gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                          if (err){
                            return res.status(404).json({ err: err.message });
                          }
                          else
                          {
                            Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,photo:req.file.filename,
                              action:req.body.action,
                              domain: req.body.domain,
                              r_type:req.body.r_type,
                              about_member: req.body.about_member,
                              alumni_journey: req.body.alumni_journey,
                              linkedin: req.body.linkedin,
                              gmail: req.body.gmail,
                              github: req.body.github,
                              }},(error,found)=>{
                              res.send('done')
                            })
                          }
                        });
                    }
                  });
                }
              })
            }
            else
            {
              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,
                action:req.body.action,
                domain: req.body.domain,
                r_type:req.body.r_type,
                about_member: req.body.about_member,
                alumni_journey: req.body.alumni_journey,
                linkedin: req.body.linkedin,
                gmail: req.body.gmail,
                github: req.body.github,}},(error,found)=>{
                res.send('done')
              })
            }
          }
        }
        else
        {
          if(!(req.body.serial === 'undefined'))
          {
            if(req.file)
            {
              Team.findOne({$and:[{serial:req.body.serial},{action:req.body.action}]},(error1,found1)=>{
                if(error1)
                {
                  res.status(500).send('error')
                }
                else
                {
                  var f;
                  gfs.find().toArray((err, files) => {
                    if(err)
                    {
                      res.status(500).send('error data')
                    }
                    if (!files || files.length === 0)
                    {
                      res.status('404').send('no data')
                    }
                    else
                    {
                     f = files
                        .map(file => {
                          if (
                            file.contentType === "image/png" ||
                            file.contentType === "image/jpeg"
                          ) {
                            file.isImage = true;
                          } else {
                            file.isImage = false;
                          }
                          return file;
                        })
                        .sort((a, b) => {
                          return (
                            new Date(b["uploadDate"]).getTime() -
                            new Date(a["uploadDate"]).getTime()
                          );
                        });
                        var datas = f.filter(item=>item.filename ===found1.photo);
                        var id = datas[0]._id;
                        gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                          if (err){
                            return res.status(404).json({ err: err.message });
                          }
                          else
                          {
                            Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,photo:req.file.filename,
                              action:req.body.action,
                              domain: req.body.domain,
                              r_type:req.body.r_type,
                              about_member: req.body.about_member,
                              alumni_journey: req.body.alumni_journey,
                              linkedin: req.body.linkedin,
                              gmail: req.body.gmail,
                              github: req.body.github,
                              }},(error,found)=>{
                              res.send('done')
                            })
                          }
                        });
                    }
                  });
                }
              })
            }
            else
            {
              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,
                action:req.body.action,
                domain: req.body.domain,
                r_type:req.body.r_type,
                about_member: req.body.about_member,
                alumni_journey: req.body.alumni_journey,
                linkedin: req.body.linkedin,
                gmail: req.body.gmail,
                github: req.body.github,}},(err1,done)=>{
                if(err1)
                {
                    res.status(500).send('error');
                }
                else if(done)
                {
                  res.send('done')
                }
                else
                {
                  res.status(400).send('not updated');
                }
              })
            }
          }
          else
          {
            const newSponser = new Team({
              action:req.body.action,
              name:req.body.name,
              photo:req.file.filename,
              domain: req.body.domain,
              r_type:req.body.r_type,
              about_member: req.body.about_member,
              alumni_journey: req.body.alumni_journey,
              linkedin: req.body.linkedin,
              gmail: req.body.gmail,
              github: req.body.github,
              })
            newSponser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
          }
        }
      })
    }
 });
})

router.post('/fetch_team', (req, res) => {
  var f;
  Team.find({action:req.body.action},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else
    {
      if(!gfs) {
        console.log("some error occured, check connection to db");
        res.send("some error occured, check connection to db");
        process.exit(0);
      }
      else
      {
        gfs.find().toArray((err, files) => {
          console.log('inside array');
          if(err)
          {
            res.status(500).send('error data')
          }
          if (!files || files.length === 0)
          {
            res.status('404').send('no data')
          }
          else
          {
           f = files
              .map(file => {
                if (
                  file.contentType === "image/png" ||
                  file.contentType === "image/jpeg"
                ) {
                  file.isImage = true;
                } else {
                  file.isImage = false;
                }
                return file;
              })
              .sort((a, b) => {
                return (
                  new Date(b["uploadDate"]).getTime() -
                  new Date(a["uploadDate"]).getTime()
                );
              });
              console.log('find file');
              var sendData={
                found:found,
                files:f,
              }
              res.send(sendData)
          }
        });
      }

    }
  })
})



router.post('/fetch_contact_details', (req, res) => {
  Team.find({action:req.body.action},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else
    {
        var sendData={
          found:found,
        }
        res.send(sendData)
    }
  })
})


router.post('/fetch_achievment', (req, res) => {
  Team.find({action:req.body.action},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else
    {
        var sendData={
          found:found,
        }
        res.send(sendData)
    }
  })
})

router.post('/fetch_to_edit_team',function(req,res) {
  Team.findOne({$and: [{serial: req.body.id}]}, function(err, docs){
    if(err)
    {
      res.status(500).send('Error!!');
    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/del_team',function(req,res) {
  //console.log(req.body.id);
  var id = req.body.id.toString();
    gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
      if (err){
        return res.status(404).json({ err: err.message });
      }
      else{
        Team.deleteOne({$and: [{serial:  req.body.serial }]},function(err,succ){
          res.send("OK")
        });
      }
    });
});


router.post('/save_gallery_photo', (req, res) => {
  upload(req, res, (err) => {
    if(err)
    {
      return res.status(500).send('Upload Not Completed');
    }
    else
    {
            if(!(req.body.serial === 'undefined'))
            {
              if(req.file)
              {
                Team.findOne({$and:[{serial:req.body.serial},{action:req.body.action}]},(error1,found1)=>{
                  if(error1)
                  {

                  }
                  else
                  {
                    var f;
                    gfs.find().toArray((err, files) => {
                      if(err)
                      {
                        res.status(500).send('error data')
                      }
                      if (!files || files.length === 0)
                      {
                        res.status('404').send('no data')
                      }
                      else
                      {
                       f = files
                          .map(file => {
                            if (
                              file.contentType === "image/png" ||
                              file.contentType === "image/jpeg"
                            ) {
                              file.isImage = true;
                            } else {
                              file.isImage = false;
                            }
                            return file;
                          })
                          .sort((a, b) => {
                            return (
                              new Date(b["uploadDate"]).getTime() -
                              new Date(a["uploadDate"]).getTime()
                            );
                          });
                          var datas = f.filter(item=>item.filename ===found1.photo);
                          var id = datas[0]._id;
                          gfs.delete(new mongoose.Types.ObjectId(id), (err, data) => {
                            if (err){
                              return res.status(404).json({ err: err.message });
                            }
                            else
                            {
                              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{photo:req.file.filename}},(error,found)=>{
                                res.send('done')
                              })
                            }
                          });
                      }
                    });
                  }
                })
              }
              else
              {
                res.send('no');
              }
            }
            else
            {
              const newSponser = new Team({
                action:req.body.action,
                photo:req.file.filename,
                })
              newSponser.save((err, savedUser) => {
                var succ= {
                  succ: "Successful SignedUP"
                };
                res.send(succ);
              })
            }
    }
 });
})


router.post('/achievment_details', function(req,res) {
  var trans = new Team(req.body.data);
  trans.save((err, savedUser) => {
    if(err)
    {
      res.status(500).send('Error');
    }
    else if(savedUser)
    {
      res.send('done')
    }
  })
});


router.post('/edit_achievment', function(req,res) {
  Team.updateOne({$and: [{ serial: req.body.data.serial},{action: req.body.data.action}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })
});

router.post('/contact_details', function(req,res) {
  var trans = new Team(req.body.data);
  trans.save((err, savedUser) => {
    if(err)
    {
      res.status(500).send('Error');
    }
    else if(savedUser)
    {
      res.send('done')
    }
  })
});


router.post('/edit_contact', function(req,res) {
  Team.updateOne({$and: [{ serial: req.body.data.serial},{action: req.body.data.action}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })
});


router.post('/add_details_of_vehicle', function(req,res) {
  //console.log(req.body)
  var trans = new VDetails(req.body.data);
  trans.save((err, savedUser) => {
    if(err)
    {
      res.status(500).send('Error');
    }
    else if(savedUser)
    {
      res.send('done')
    }
  })
});


router.post('/edit_vehicle_details', function(req,res) {
 // console.log(req.body.data)
 VDetails.updateOne({$and: [{ serial: req.body.data.serial},{action: req.body.data.action}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })
});

router.post('/fetch_to_edit_vehicle_details',function(req,res) {
  VDetails.findOne({$and: [{serial: req.body.id}]}, function(err, docs){
    if(err)
    {
      res.status(500).send('Error!!');
    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/fetch_vdetails', (req, res) => {
  //console.log(req.body)
  VDetails.find({$and:[{type:req.body.type},{action:req.body.action}]},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else{
      res.send(found)
    }
  })
})


router.post('/del_vehicle_details',function(req,res) {
  VDetails.deleteOne({$and: [{serial:  req.body.serial }]},function(err,succ){
    res.send("OK")
  });
});

router.post('/fetchRecent', (req, res) => {
  Team.find({$and:[{action:req.body.action}]},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else{
      res.send(found)
    }
  })
})


router.post('/vehiclePhoto', (req, res) => {
  Team.findOne({$and:[{action:'Vehicle'},{name:req.body.vehicle}]},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else{
      res.send(found)
    }
  })
})

router.post('/detail_info', (req, res) => {
  VDetails.find({action:req.body.vehicle},(error,found)=>{
    if(error)
    {
      res.status(500).send('Error!!');
    }
    else
    {
      res.send(found);
    }
  })
})

router.post('/contactUs', (req, res) => {
  let mailid='arijitnayak92@gmail.com';
  let content ='<p><b>Contact Person Name - </b>'+req.body.data.name+'</p><br /><p><b>Contact Person Mail Id - </b>'+req.body.data.mailid+'</p><br /><p><b>Message - </b>'+req.body.data.message+'</p><br />Team AUV - SRM IST.';

  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'srmauvofficial@gmail.com',
      pass: 'gcclueaajtmtnglt'
       }
   });
   //console.log(req.body.data)
   const mailOptions = {
    from: 'srmauvofficial@gmail.com',
    to: ['arijitnayak92@gmail.com','at4359@srmist.edu.in'],
    subject: 'Message From AUV',
    html: '<p>Dear User,</p><br /><p>This email is from contact us page of SRM AUV official website. Please have a look to the following details.</p><br /><p>'+content+'</p><br />Thank You.'
  };
  console.log("Way to enter.......");
  transporter.sendMail(mailOptions, function (err, info) {
    if(err)
    {
      res.status(500).send('error');
    }
    else{
      //console.log(info)
      res.send('done')
    }
 });
})


router.post('/reset_from_mail', (req, res) => {
  const {token_come,password} = req.body;
  User.findOne({ resetPasswordToken: token_come }, (err, user) => {
      if (err)
      {
          res.status(500).send('Error!!');
      }
      else if(user) {
        let hash = bcrypt.hashSync(password, 10);
          User.updateOne({resetPasswordToken: token_come},{password: hash} ,(err, user) => {
            var succ= {
              succ: "Password Updated!!"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "failed"
          };
          res.send(fail);
        }
      })
    })

    router.get('/reset_password', function(req, res) {
  User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
    if (!user) {
      // console.log("Expired");
      var expire ={
        expire:"Link is Expired"
      }
      res.send(expire);
    }
    else if(user){
      res.send(user.resetPasswordToken)
    }
  });
});


module.exports = router
