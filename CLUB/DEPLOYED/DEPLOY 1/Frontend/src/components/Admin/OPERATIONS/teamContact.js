import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';
import {API_URL} from '../../../utils/apiUrl';

export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      loaded_percent:0,
      redirectTo:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
  M.AutoInit();
}


   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     this.setState({loading:true})
  let apiUrl;
  if(this.state.isEditProduct){
    apiUrl = '/user/edit_contact';
  } else {
    apiUrl = '/user/contact_details';
  }
  axios.post(API_URL+apiUrl,{data})
      .then(response => {
        this.setState({
          loading:false,
          response: response.data,
          isAddProduct: false,
          isEditProduct: false
        })
      })
      .catch(err=>{
          this.setState({loading:false})
          if(err.response.status === 404)
          {
            window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
          }
          else
          {
            window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
          }
      })

}

editProduct = (productId,index)=> {
  this.setState({loading:true})
  axios.post(API_URL+'/user/fetch_to_edit_team',{
    id: productId,
  })
      .then(response => {
        this.setState({
          loading:false,
          product: response.data,
          isEditProduct: true,
          isAddProduct: true,
        });
      })
      .catch(err=>{
        this.setState({loading:false})
      })

}
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
            {
              header: "Facebook Account",
              name: "fb",
              placeholder: "Enter the Facebook Account",
              type: "text",
              size:false,
            },
            {
              header: "Insta Account",
              name: "insta",
              placeholder: "Enter the Insta Account",
              type: "text",
              size:false,
            },
            {
                header: "Twitter Account",
                name: "twitter",
                placeholder: "Enter the Twitter Account",
                type: "text",
                size:false,
              },
              {
                header: "LinkedIn Account",
                name: "linkedin",
                placeholder: "Enter the LinkedIn Account",
                type: "text",
                size:false,
              },
              {
                header: "You Tube Account",
                name: "youtube",
                placeholder: "Enter the You Tube Account",
                type: "text",
                size:false,
              },
             {
                    header: "Phone Number",
                    name: "phone",
                    placeholder: "Enter the Phone Number",
                    type: "text",
                    size:false,
             },
            {
                    header: "Gmail Account",
                    name: "gmail",
                    placeholder: "Enter the Gmail Account",
                    type: "text",
                    size:false,
            },

      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct loaded_percent={this.state.loaded_percent} cancel={this.updateState} username={this.state.username}
     action={"Contact Details"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading)
    {
      return(
        <div className="cover_all">
          <div className="center">
              <div className="box-frames" style={{margin:'20px'}}>
                  <div className="white-text mloader-39"></div>
              </div>
          </div>
          <h5 className="center white-text">
              Processing your request...
          </h5>
        </div>
      );
    }
    else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Contact Details</h5>
                    <br />
                    {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                    <button className="btn right blue-grey darken-2 sup subm"
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct &&
                            <ProductList image={false} username={this.props.username} title={this.state.option}
                            action={"Contact Details"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
        }
}
  }
}
