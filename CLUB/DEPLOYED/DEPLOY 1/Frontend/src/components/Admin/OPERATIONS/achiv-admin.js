import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';
import {API_URL} from '../../../utils/apiUrl';


export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      redirectTo:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
  M.AutoInit();
}
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     this.setState({loading:true})
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/user/edit_achievment';
     } else {
       apiUrl = '/user/achievment_details';
     }
     axios.post(API_URL+apiUrl,{data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false,
             loading:false
           })
         })
         .catch(err=>{
             this.setState({loading:false})
             if(err.response.status === 404)
             {
               window.M.toast({html:'Data Not Found !!',classes:'red rounded'})
             }
             else
             {
               window.M.toast({html:'Something went wrong !!',classes:'red rounded'})
             }
         })
   }

   editProduct = (productId,index)=> {
     this.setState({loading:true})
     axios.post(API_URL+'/user/fetch_to_edit_team',{
       id: productId,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
             loading:false,
           });
         })
         .catch(err=>{
           this.setState({loading:false})
         })
  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Achievments",
          name: "name",
          placeholder: "Enter the content of achievments",
          type: "text",
          size:true,
        },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct cancel={this.updateState}
     action={"Achievments"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading)
    {
      return(
        <div className="cover_all">
          <div className="center">
              <div className="box-frames" style={{margin:'20px'}}>
                  <div className="white-text mloader-39"></div>
              </div>
          </div>
          <h5 className="center white-text">
              Processing your request...
          </h5>
        </div>
      );
    }
    else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Achievments</h5>
                    <br />
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct &&
                            <ProductList image={false} action={"Achievments"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                    <button className="btn right blue-grey darken-2 sup subm"
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
        }
}
  }
}
