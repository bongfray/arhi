import React, { Component } from 'react';
import ZImg from './zarnaimg.jpg';
import Axios from 'axios';
import {API_URL} from '../../utils/apiUrl';

export default class Zarna extends Component {
    constructor()
    {
        super()
        this.state={
            details:false,
            image:'',
            vehicle_dimension:[],
            mass_vehicle:[],
            top_speed:[],
            dof:[],
            propulation:[],
            navigational:[],
            computer_model:[],
            cam_sensor:[],
            soft_arch:[],
            power_system:[],
            loader:true,
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchVehicleInfo();
    }
    fetchVehicleInfo=()=>{
        Axios.post(API_URL+'/user/vehiclePhoto',{vehicle:this.props.vehicle})
        .then(res=>{
                this.setState({image:res.data.photo})
        })
        .catch(err=>{

        })
        this.fetchDetails();
    }
    fetchDetails=()=>{
        Axios.post(API_URL+'/user/detail_info',{vehicle:this.props.vehicle})
        .then(res=>{
            var vehicle_dimension =res.data.filter(item=>item.type === "Vehicle Dimensions");
            var mass_vehicle =res.data.filter(item=>item.type === "Mass of vehicle");
            var top_speed =res.data.filter(item=>item.type === "Top Speed");
            var dof =res.data.filter(item=>item.type === "Degrees of Freedom");
            var propulation =res.data.filter(item=>item.type === "Propulsion System");
            var navigational =res.data.filter(item=>item.type === "Navigational Components");
            var computer_model=res.data.filter(item=>item.type === "Single Board Computer Model");
            var cam_sensor =res.data.filter(item=>item.type === "Camera Sensors");
            var soft_arch =res.data.filter(item=>item.type === "Software Architecture");
            var power_system =res.data.filter(item=>item.type === "Power System");
            this.setState({
                vehicle_dimension:vehicle_dimension,
                mass_vehicle:mass_vehicle,
                top_speed:top_speed,
                dof:dof,
                propulation:propulation,
                navigational:navigational,
                computer_model:computer_model,
                cam_sensor:cam_sensor,
                soft_arch:soft_arch,
                power_system:power_system,
                loader:false,
            })
        })
        .catch(err=>{
            this.setState({loader:false})
        })

    }
    render() {
        if(this.state.loader)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Fetching Details....
                </h5>
                </React.Fragment>

            )
        }
        else{
            return (
                <React.Fragment>
                    <div className="row">

                        <h1 className="center tag-head-v col s12"><span className="z-head" data-aos='fade-up'>{this.props.vehicle ? this.props.vehicle : 'ZARNA'}</span> <br/><span className="team-header1" data-aos='fade-up'>A</span>utonomous <span className="team-header2">U</span>nderwater <span className="team-header4">V</span>ehicle </h1><br/>
                        <div className="col s1"></div>
                        <div className="col s10">

                            <img src={this.state.image ? API_URL+'/user/admin_panel/:'+this.state.image : ZImg }
                            className="col s12" alt="" style={{width:'100%',height:'550px'}} data-aos='zoom-in'/>
                            <div className=" col s6">
                                <h4 className="dim-h4" data-aos='fade-up'>Vehicle Dimensions</h4>
                                {this.state.vehicle_dimension.map((content,index)=>{
                                   return(
                                    <p className="dim-p" data-aos='fade-up' key={index} style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                   )
                                })}

                                <h4 className="dim-h4" data-aos='fade-up'>Mass of vehicle</h4>
                                {this.state.mass_vehicle.length>0 ?
                                    <React.Fragment>
                                                                {this.state.mass_vehicle.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }


                                <h4 className="dim-h4" data-aos='fade-up'>Top Speed</h4>

                                {this.state.top_speed.length>0 ?
                                    <React.Fragment>
                                                                {this.state.top_speed.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }


                                <h4 className="dim-h4" data-aos='fade-up'>Degrees of Freedom</h4>
                                {this.state.dof.length>0 ?
                                    <React.Fragment>
                                                                {this.state.dof.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }

                                <h4 className="dim-h4" data-aos='fade-up'>Propulsion System</h4>

                                {this.state.propulation.length>0 ?
                                    <React.Fragment>
                                                                {this.state.propulation.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }

                            </div>

                            <div className=" col s6">

                                <h4 className="dim-h4" data-aos='fade-up'>Navigational Components</h4>

                                {this.state.navigational.length>0 ?
                                    <React.Fragment>
                                                                {this.state.navigational.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }

                                <h4 className="dim-h4" data-aos='fade-up'>Single Board Computer Model</h4>
                                {this.state.computer_model.length>0 ?
                                    <React.Fragment>
                                                                {this.state.computer_model.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }

                                <h4 className="dim-h4" data-aos='fade-up'>Camera Sensors</h4>
                                {this.state.cam_sensor.length>0 ?
                                    <React.Fragment>
                                                                {this.state.cam_sensor.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }

                                <h4 className="dim-h4" data-aos='fade-up'>Software Architecture</h4>
                                {this.state.soft_arch.length>0 ?
                                    <React.Fragment>
                                                                {this.state.soft_arch.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }
                                <h4 className="dim-h4" data-aos='fade-up'>Power System</h4>
                                {this.state.power_system.length>0 ?
                                    <React.Fragment>
                                                                {this.state.power_system.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" data-aos='fade-up' key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p" data-aos='fade-up'>No Data</p>
                                }
                            </div>
                        </div>

                        <div className="col s1"></div>

                    </div>
                </React.Fragment>

            )
        }
    }
}
