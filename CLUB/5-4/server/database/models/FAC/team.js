const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');
//Schema
const teamschema = new Schema({
  action:{ type: String, unique: false, required: false },
  name: { type: String, unique: false, required: false },
  photo: { type: String, unique: false, required: false },
  dept:{ type: String, unique: false, required: false },
  about_member: { type: String, unique: false, required: false },
  linkedin: { type: String, unique: false, required: false },
  gmail: { type: String, unique: false, required: false },
  regno: { type: String, unique: false, required: false },
  alumni_journey:{ type: String, unique: false, required: false },
  createdat: { type: Date, unique: false, required: false },
  updatedat: { type: Date, unique: false, required: false },
  achievment:{ type: String, unique: false, required: false },
  status:{ type: Boolean, unique: false, required: false },
})


teamschema.plugin(autoIncrement.plugin, { model: 'Team', field: 'serial', startAt: 1,incrementBy: 1 });

const Team = mongoose.model('Team', teamschema)
module.exports = Team

