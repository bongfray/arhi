import React, { Component } from 'react'
import $ from 'jquery'

import G1 from './Pics/G1.jpeg';
import G2 from './Pics/G2.jpeg';
import G3 from './Pics/G3.jpeg';
import G4 from './Pics/G4.jpeg';
import G5 from './Pics/G5.jpeg';
import G6 from './Pics/G6.jpeg';
import G7 from './Pics/G7.jpeg';
import G8 from './Pics/G8.jpg';

export default class Gallery extends Component {
    componentDidMount(){
        $( ".img-wrapper" ).hover(
            function() {
            $(this).find(".img-overlay").animate({opacity: 1}, 600);
            }, function() {
            $(this).find(".img-overlay").animate({opacity: 0}, 600);
            }
        );
        
        var $overlay = $('<div id="overlay"></div>');
        var $image = $("<img>");
        var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
        var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
        var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');
        
        $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
        $("#gallery").append($overlay);
        
        $overlay.hide();
        
        $(".img-overlay").click(function(event) {
            event.preventDefault();
            var imageLocation = $(this).prev().attr("href");
            $image.attr("src", imageLocation);
            $overlay.fadeIn("slow");
        });
        
        $overlay.click(function() {
            $(this).fadeOut("slow");
        });
        
        $nextButton.click(function(event) {
            $("#overlay img").hide();
            var $currentImgSrc = $("#overlay img").attr("src");
            var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
            var $nextImg = $($currentImg.closest(".image").next().find("img"));
            var $images = $("#image-gallery img");
            if ($nextImg.length > 0) { 
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
            } else {
            $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
            }
            event.stopPropagation();
        });
        
        $prevButton.click(function(event) {
            $("#overlay img").hide();
            var $currentImgSrc = $("#overlay img").attr("src");
            var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
            var $nextImg = $($currentImg.closest(".image").prev().find("img"));
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
            event.stopPropagation();
        });
        
        $exitButton.click(function() {
            $("#overlay").fadeOut("slow");
        });
    }
    render() {
        return (
            <section id="gallery">
            <div className="row">
                <span className="center gal-title fade">Gallery Pictures</span>
                <div id="image-gallery">
                
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G1}><img src={G1} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G2}><img src={G2} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G3}><img src={G3} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G4}><img src={G4} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G5}><img src={G5} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G6}><img src={G6} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G7}><img src={G7} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                    <div className="col l3 m3 s6 image fade">
                    <div className="img-wrapper">
                        <a href={G8}><img src={G8} className="img-responsive"/></a>
                        <div className="img-overlay">
                        <i className="fa fa-expand" aria-hidden="true"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            </section>
        )
    }
}
