import React from 'react';
import axios from 'axios';
import ShowPage from '../vehicleDetails';
import M from 'materialize-css';

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
      show_info:false,
      fetching:true,
      fullview:false,
      img:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
    M.AutoInit();
    this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
    let url;
    if(this.props.action === "Sponsors")
    {
      url = '/ework/user/del_sponsor';
    }
    else if((this.props.action === "Team Member") || (this.props.action === "Alumni Details"))
    {
      url ='/ework/user/del_team';
    }
    else{
      url ='/ework/user/del_vehicle_details';
    }

  const { products } = this.state;
  axios.post(url,{
    serial: productId,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}

componentDidUpdate=(prevProps)=>{
  if((prevProps.type !== this.props.type) || (prevProps.action !== this.props.action))
  {
    this.fetch();
  } 
}

fetch =() =>{
  let url;
  if(this.props.action === "Sponsors")
  {
    url = '/ework/user/fetch_sponsor';
  }
  else if((this.props.action === "Team Member") || (this.props.action === "Alumni Details") || (this.props.action === "Achievments")
   || (this.props.action === "Gallery") || (this.props.action === "Vehicle"))
  {
    url ='/ework/user/fetch_team';
  }
  else{
    url ='/ework/user/fetch_vdetails';
  }
  axios.post(url,{
    action: this.props.action,
    type:this.props.type,
  })
  .then(response =>{
    //console.log(response.data)
    this.setState({
     products: response.data,
     fetching:false,

   })
  })
  .catch(err=>{
    window.M.Toast({html: 'Something Went Wrong !!',classes:'red rounded'})
  })
}
  render() {
    if(this.state.fetching){
      return(
        <div className="center">
          <div className="preloader-wrapper active">
            <div className="spinner-layer spinner-red-only">
              <div className="circle-clipper left">
                <div className="circle"></div>
              </div><div className="gap-patch">
                <div className="circle"></div>
              </div><div className="circle-clipper right">
                <div className="circle"></div>
              </div>
            </div>
          </div>
        </div>

      )
    }
    else{
    const { products} = this.state;
      return(
        <div>
          {this.state.fullview && 
            <div className="cover_all">
              <div className="up">
                <a className="btn-floating btn-small red" style={{margin:'5px'}}>
                  <i className="material-icons right" onClick={()=>this.setState({fullview:false})}>close</i>
                </a><br />
                <div className="card">
                  <div className="card-image">
                    <img style={{maxWidth:'100%',height: '700px'}}
                  src={require('../uploads/'+this.state.img)} alt="here"/>
                  </div>
                </div>
              </div>
            </div>
          }
          {this.state.show_info && 
                <ShowPage close={()=>this.setState({show_info:false})} vehicle_name={this.state.vehicle_name} />
          }

        
          <div className="row">
            {products.map((product,ind) => (
                 <div className="col s12 m6 l4 xl4" key={ind}>
                    <div className="card">
                      {this.props.image && <div className="card-image">
                        <img style={{height:'200px'}} onClick={()=>this.setState({fullview:true,img:product.photo})}
                         src={require('../uploads/'+product.photo)} alt="here"/>
                      </div>}
                      <div className="card-content">
                        {this.props.data.fielddata.map((content,index)=>(
                          <React.Fragment key={index}>
                              {content.placeholder === 'none' ? 
                               <React.Fragment></React.Fragment>
                               :
                                 <div className="row">
                                    <div className="col l6 xl6">{content.header}</div>
                                    <div className="col l6 xl6">
                                       <div className="center" style={{wordWrap: 'break-word'}} >{product[content.name]}</div>
                                    </div>
                                 </div>
                               } 
                          </React.Fragment>
                        ))}
                      </div>
                      <div className="card-action">
                        <i className="material-icons go" onClick={() => this.props.editProduct(product.serial,this.props.action)}>edit</i>
                      &nbsp;<i className="material-icons go" onClick={() => this.deleteProduct(product.serial,this.props.action)}>delete</i>
                      {this.props.action === 'Vehicle' && 
                            <div className="right btn pink" onClick={()=>this.setState({vehicle_name:product.name,show_info:true})}>Details</div>
                        }
                      </div>
                    </div>
                  </div>
                
                  ))}
            </div>
        </div>
      )
  }

  }
}
