import React from 'react';
import M from 'materialize-css';

export default class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value:'',
      serial:'',
      action:'',
      name:'',
      photo:'',
      file:'',
      type:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleD = (event,index) =>{
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }
componentDidMount(){
    M.AutoInit();
  this.setState({
    action:this.props.action,
    type:this.props.type,
  })
}
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
      event.preventDefault();
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
  }
  onChange=(e)=> {
    this.setState({file:e.target.files[0]});
}

  render() {
    let pageTitle;
if(this.state.serial) {
  pageTitle = <h5 className="ft">EDIT DATAS</h5>
} else {
  pageTitle = <h5 className="ft">ADD DATAS</h5>
}

    return(
      <div className="card hoverable">
      <div className="center">{pageTitle}</div>
      <div>
        <div className="row">
            <div className="col l3 xl3 m3 hide-on-mid-and-down"/>
            <div className="col l6 xl6 s12 m6">
                    {this.props.data.fielddata.map((content,index)=>(
                    <div className="row" key={index}>
                        <div className="col l12 xl12 s12 m12">
                         {content.placeholder === "none" ?
                            <React.Fragment>
                                <input style={{display:'none'}}
                                 type="file" onChange= {this.onChange}
                                 ref={fileInput=>this.fileInput = fileInput} />
                                <button className="btn small" onClick={()=>this.fileInput.click()}>Upload the Photo</button>
                            </React.Fragment>
                          :
                          <React.Fragment>
                              {content.size === false ? 
                                <div className="input-field col l12 xl12 s12 m12">
                                   <input id={content.name} type={content.type} name={content.name} value={this.state[content.name]}
                                    onChange={e => this.handleD(e, index)} className="validate" required/>
                                   <label htmlFor={content.name}>{content.placeholder}</label>
                                </div>
                               :
                               <div className="input-field">
                                    <textarea id={content.name} name={content.name} value={this.state[content.name]}
                                    onChange={e => this.handleD(e, index)} className="materialize-textarea"></textarea>
                                    <label htmlFor={content.name}>{content.placeholder}</label>
                                </div>  
                               }
                          </React.Fragment>  
                            }
                            
                        </div> 
                    </div>
                    ))}
                <div className="row">
                    <div className="col l6 xl6 s4 m6">Uploading - <span className="red-text">{this.props.loaded_percent}</span></div>
                    <div className="col l6 xl6 s8 m6">
                            <button className="btn left red" onClick={this.props.cancel} type="submit">CANCEL</button>
                            <button className="btn right blue-grey darken-2" type="submit" onClick={this.handleSubmit}>UPLOAD</button>
                    </div>
                </div>
            </div>
            <div className="col l3 xl3 m3 hide-on-mid-and-down"/>
        </div>
        </div>
      </div>
    )
  }
}

