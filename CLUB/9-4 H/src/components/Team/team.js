import React, { Component } from 'react'
import $ from 'jquery'

export default class Team extends Component {
    componentDidMount(){
        $("header").append("<div className='glitch-window'></div>");
        $( "h1.glitched" ).clone().appendTo( ".glitch-window" );
    }
    render() {
        return (
            <React.Fragment>
                <div className="team">
                    <h1 className="team-h center" data-aos='fade-up'><span>M</span><span>E</span><span>E</span><span>T</span><span> </span><span>O</span><span>U</span><span>R</span><span> </span><span>T</span><span>E</span><span>A</span><span>M</span></h1> 

                    

                    <div className="row">
                                    <div className="col l4 m2"></div>
                                    <div className="col l4 s12 m8 center">
                                    <div className="go team-link" data-aos='fade-up'>
                                    <a href="/teamdetails" style={{color:'white'}}>Give a peek behind the curtains <span className="fa fa-arrow-right small"></span></a>
                                    </div>
                                    </div>
                                    <div className="col l4 m2"></div>
                                </div>
                </div>
            </React.Fragment>
            
        )
    }
}
