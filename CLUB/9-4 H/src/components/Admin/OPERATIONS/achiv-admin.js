import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';


export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      uploading:false,
      loaded_percent:0,
      username:'',
      redirectTo:'',
      option:'',
      file:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
  M.AutoInit();
}
   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
       //console.log(data)
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/user/edit_achievment';
     } else {
       apiUrl = '/user/achievment_details';
     }
     axios.post(apiUrl,{data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetch_to_edit_team',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Achievments",
          name: "achievment",
          placeholder: "Enter the content of achievments",
          type: "text",
          size:true,
        },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct cancel={this.updateState} username={this.state.username}
     action={"Achievments"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading ===  true)
    {
      return(
         <div>Loading...</div>
      );
    }
    else{
         if(this.state.uploading)
         {
             return(
                 <div className="center">Loading  {this.state.loaded_percent}</div>
             )
         }
         else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Achievments</h5>
                    <br />
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct && 
                            <ProductList image={false} username={this.props.username} title={this.state.option}  
                            action={"Achievments"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                    <button className="btn right blue-grey darken-2 sup subm" 
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
          }
        }
}
  }
}



