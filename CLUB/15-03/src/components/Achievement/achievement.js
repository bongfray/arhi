import React, { Component } from 'react'

export default class Achievement extends Component {
    render() {
        return (
            <React.Fragment>

            <div className="achievement">
                <div className="shadowbox">
                    <h2 className="center ach-head">ACHIEVEMENTS</h2>
                    <div className="row">

                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">National Champions: Secured first place at NIOT (National Institute for Ocean Technology) SAVE competition held in Chennai in 2015.The competition is organised by NIOT and IEEE-OES</p></div>
                        </div>

                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">SRMAUV won over 10 participating teams including IITK, IITD,IITM, DTU, IMU.</p></div>
                        </div>

                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">Team was invited to exhibit the innovation at ACMEE 2018, 13th International Machine Tool Exhibition held at Chennai Trade Centre, Tamil Nadu.</p></div>
                        </div>
                        
                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">Shortlisted for NIOT SAVe 2018 in the final competition phase. Would be competing among (Student Autonomous UnderwaterVehicle Competition arranged by National Institute of Ocean Technology).</p></div>
                        </div>
                        
                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">Placed 4th Internationally at Student’s Autonomous Underwater Challenge Singapore organised by IEEE Singapore Chapter.</p></div>
                        </div>
                        
                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">SRMAUV has been sponsored by the Indian Government to participate in AUVSI’s International Robosub Competition at San Diego, USA in 2015 to represent India.</p></div>
                        </div>
                        
                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">SRMAUV represented IEEE-OES at International Underwater Conference 2015 at NIOT.</p></div>
                        </div>

                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">I4c Idea Innovation Challenge (Make in India campaign): Shortlisted to top 40 innovations amongst 5000 applicants nationwide.</p></div>
                        </div>

                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">The team has presented the AUV at several colleges and schools at Indian Express EdEx Expo 2014 in Chennai.</p></div>
                        </div>

                        <div className="col s12 l12 m12">
                            <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                            <div className="col s11 m11 l11"><p className="ach-cont">The team invited by Teledyne RDI USA to present a talk on AUV Localisation Techniques at ADCP’s in the Action Conference, NIO, Goa.</p></div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>

            </React.Fragment>    
        )
    }
}
