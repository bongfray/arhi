import React, { Component } from 'react';
import { Route, Switch} from 'react-router-dom'
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';


import Home from './components/Home/home';
import Contact from './components/Team/Contact';
import Zarna from './components/Vehicle/zarna';
import Er from './components/Error/error';
import Domain from './components/Domains/domain'
import ContactUs from './components/ContactUs/contactus';
import AdminS from './components/Admin/admin_signup'
import AdminPanel from './components/Admin/admin';

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">

         <Switch>

          <Route exact path="/" component={Home} />
          <Route exact path="/teamdetails" component={Contact} />
          <Route exact path="/zarna" component={Zarna} />
          <Route exact path="/domain" component={Domain} />
          <Route exact path="/us" component={ContactUs} />
          <Route exact path="/admin" component={AdminS} />
          <Route exact path="/admin_panel" component={AdminPanel} />

          

          <Route exact path="" component={Er} />

        

        </Switch>
      
      </div>
    );
  }
}

export default App;
