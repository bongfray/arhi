import React, { Component } from 'react'
import Logo from '../Home/auv.png'

export default class Footer extends Component {
    render() {
        return (
            <React.Fragment>
           
                <div className="footer" style={{display:'none'}}>
                    <div id="button"></div>
                    <div id="container">
                        <div className="row center">
                            <div className="col l4 s4 m4">
                                <div className="row">
                                    <div className="l12 m12 s12">
                                        <img src={Logo} alt="" data-aos='fade-up' className="footer-logo"/>
                                    </div>
                                    <div className="col l12 s12 m12">
                                        <h3 className="footer-lname" data-aos='fade-up'>SRM AUV</h3>
                                        <p className="footer-lp" data-aos='fade-up'>SRM IST's Official AUV Team</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col l4 s4 m4 footer-list">
                                <h5 className="center footer-h4" data-aos='fade-up'>Explore</h5>
                                <ul className="wrap-nav">
                                <li data-aos='fade-up'><a href="/">Home</a></li>
                                <li data-aos='fade-up'><a href="/#vehicle">Vehicle</a></li>
                                <li data-aos='fade-up'><a href="/#team">Team</a></li>
                                <li data-aos='fade-up'><a href="/#sponsor">Sponsors</a></li>
                                </ul>
                            </div>

                            <div className="col s4 m4">
                            <h5 className="center footer-h4" data-aos='fade-up'>Follow Us</h5>
                                
                            <div className="rounded-social-buttons" data-aos='zoom-out'>
                                <a className="social-button facebook" href="https://www.facebook.com/"><i className="fa fa-facebook-f"></i></a>
                                <a className="social-button twitter" href="https://www.twitter.com/"><i className="fa fa-twitter"></i></a>
                                <a className="social-button linkedin" href="https://www.linkedin.com/"><i className="fa fa-linkedin"></i></a>
                                <a className="social-button youtube" href="https://www.youtube.com/"><i className="fa fa-youtube"></i></a>
                                <a className="social-button instagram" href="https://www.instagram.com/"><i className="fa fa-instagram"></i></a>
                            </div>
                            <hr/>
                            <p className="copyright" data-aos='zoom-in'>© Made with <i className="fa fa-heart"></i> by ArHi &#128526; 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
