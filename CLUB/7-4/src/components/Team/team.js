import React, { Component } from 'react'
import $ from 'jquery'

export default class Team extends Component {
    componentDidMount(){
        $("header").append("<div className='glitch-window'></div>");
        $( "h1.glitched" ).clone().appendTo( ".glitch-window" );
    }
    render() {
        return (
            <React.Fragment>
                <div className="team">

                    
                    <header className="header row ">
                        <h1 className="glitchede" data-aos='flip-up'>MEET THE TEAM</h1>
                    </header>

                    <div className="row te-link hide-on-small-only">
                        <div className="col s1 l4"></div>
                        <div className="col s10 l4 center" data-aos='flip-up'><a href="/teamdetails" className=" t-link">Give a peek behind the curtains <span className="fa fa-arrow-right small"></span> </a></div>
                    </div>
                    <div className="row hide-on-med-and-up ">
                        <div className="col l3"></div>
                        <div className="col s12 l6">
                        <div className="floating-team center ">
                            <span className="" data-aos='flip-up'>MEET THE TEAM</span><br/>
                        </div>
                        </div>
                        <div className="col l3"></div>
                    </div>
                        <div className="row hide-on-med-and-up">
                            <div className="col l4"></div>
                            <div className="col l4 s12 center" data-aos='flip-up'>
                            <a href="/teamdetails" className="t-link"> Give a peek behind the curtains <span className="fa fa-arrow-right small"></span></a>
                            </div>
                            <div className="col l4"></div>
                        </div>                
                </div>
            </React.Fragment>
            
        )
    }
}
