import React, { Component } from 'react';
import Axios from 'axios';

export default class Gallery extends Component {
    constructor()
    {
        super()
        this.state={
            gallery:'',
            loading:true,
            opacity:0,
            active:0,
            show_full:false,
            ref_image:'',
            visible: 10,
            start:0,
            restore_start:0,
            restore_next:0,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount(){
        this.fetchPhoto();
    }

    loadMore=()=> {
        this.setState((prev) => {
            return {restore_start:prev.start,restore_next:prev.visible,start:prev.visible,
                visible: prev.visible + 10};
          });
  }
  loadPrev=()=> {
    this.setState((prev) => {
        return {start:this.state.restore_start,visible: this.state.restore_next,restore_start:this.state.restore_start-10,restore_next:this.state.restore_next-10};
      });
  }

    fetchPhoto=()=>{
        Axios.post('/ework/user/fetch_team',{
            action: "Gallery",
          })
        .then(res=>{
            if(res.data){
                this.setState({gallery:res.data,loading:false})
            }
        })
        .catch(err=>{

        })
    }
    addHover=(index)=>{
        this.setState({active:index})
    }
    removeHover=()=>{
        this.setState({active:0})
    }
    opacityCheck=(index)=>{
        if(this.state.active === index)
        {
            return 1;
        }
        else{
            return 0;
        }
    }
    render() {
        if(this.state.loading)
        {
            return(
                <div className="center">Loading....</div>
            )
        }
        else{
        return (
        <React.Fragment>
                    {this.state.show_full && 
                        <div className="cover_all">
                        <div className="up zoom-in-div" onMouseUp={()=>this.setState({show_full:false})}>
                            <a className="btn-floating btn-small red" style={{margin:'5px'}}>
                            <i className="material-icons right" onClick={()=>this.setState({show_full:false})}>close</i>
                            </a><br />
                            <div className="card">
                            <div className="card-image">
                                <img style={{maxWidth:'100%',height: '400px',width:'100%'}}
                            src={require('../Admin/OPERATIONS/uploads/'+this.state.ref_image)} alt="here"/>
                            </div>
                            </div>
                        </div>
                        </div>
                    }
                        
            <div className="imk">
            <h1 className="center white-text covt" data-aos='zoom-in'>Gallery Pictures</h1><br />   
                <div className="conss">
                    {this.state.gallery.slice(this.state.start, this.state.visible).map((content,index)=>{
                            return(                                           
                                    <img onClick={()=>this.setState({show_full:true,ref_image:content.photo})} style={{borderRadius:'12px'}}
                                     src={require('../Admin/OPERATIONS/uploads/'+content.photo)} alt="Gallery" className="tile"/>
                            )
                    })}
                    <br />
                        <div className="center" style={{marginTop:'30%'}}>
                        {this.state.start>0 &&
                                <button onClick={this.loadPrev} type="button"className=" btn btn-floating btn-large N/A transparent" data-aos='slide-right'>
                                    <i className="material-icons large white-text">chevron_left</i>
                                </button>
                        } &emsp;
                        {this.state.visible < this.state.gallery.length &&
                            <button onClick={(this.loadMore)} type="button" className="btn btn-floating btn-large N/A transparent" data-aos='slide-left'>
                                <i className="material-icons large white-text">chevron_right</i>
                            </button>
                        } 
                        </div>
                    
                </div>
            </div>
        </React.Fragment>
        )
    }
                
    }
}
