import React, { Component } from 'react';
import Axios from 'axios';

export default class Achievement extends Component {
    constructor()
    {
        super()
        this.state={
            achievements:[],
            loading:true,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchData();
    }
    fetchData=()=>{
        Axios.post('/ework/user/fetch_team',{
            action: 'Achievments',
          })
          .then(response =>{
            this.setState({
                achievements:response.data,
                loading:false,
           })
          })
          .catch(err=>{
            window.M.Toast({html: 'Something Went Wrong !!',classes:'red rounded'})
          })
    }
    render() {
        if(this.state.loading)
        {
            return(
                <div className="center">loading..</div>
            )
        }
        else
        {
            console.log(this.state.achievements)
        return (
            <React.Fragment>

            <div className="achievement">
                <div className="shadowbox">
                    <h2 className="center ach-head" data-aos='fade-up'>ACHIEVEMENTS</h2>\
                    {this.state.achievements.length>0 ? 
                    <React.Fragment>
                        {this.state.achievements.map((content,index)=>{
                            return(
                                <div className="row" key={index}>
                                    <div className="col s12 l12 m12" data-aos='fade-up'>
                                        <span className="white-text fa fa-trophy ic-ach col s1 m1 l1"></span>
                                        <div className="col s11 m11 l11"><p className="ach-cont">{content.achievment}</p></div>
                                    </div>
                                </div>
                            )
                        })}
                    </React.Fragment>
                    :
                    <div className="center">No Data</div>
                    }
                </div>
            </div>

            </React.Fragment>    
        )
      }
    }
}
