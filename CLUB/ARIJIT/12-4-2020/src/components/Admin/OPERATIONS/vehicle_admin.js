import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';


export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      uploading:false,
      loaded_percent:0,
      username:'',
      redirectTo:'',
      option:'',
      file:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
 M.AutoInit();
}
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) { 
       this.setState({disable_btn:true})
     let apiUrl;
     apiUrl = '/user/team_details';
     const formData = new FormData();
     formData.append('myImage',data.file);
     formData.append('name',data.name);
     formData.append('serial',data.serial);
     formData.append('action',data.action);
     window.M.toast({html: 'Uploading ....!!',classes:'orange rounded'});
     axios.post(apiUrl,formData,{
         onUploadProgress:ProgressEvent=>{
              var loaded = Math.round(ProgressEvent.loaded/ProgressEvent.total*100);
              this.setState({uploading:true,loaded_percent:loaded})
         }
     })
         .then(response => {
           if(response.data === 'have')
           {
            window.M.toast({html: 'Already There !!',classes:'pink rounded'});
            this.setState({disable_btn:false})
           }
           else{
            window.M.toast({html: 'Saved !!',classes:'green darken-1 rounded'});
            this.setState({
              uploading:false,
              loaded_percent:0,
               response: response.data,
               isAddProduct: false,
               isEditProduct: false,
               disable_btn:false,
             })
           }

         })
         .catch(err=>{
          window.M.toast({html: 'Error !!',classes:'red rounded'});
         })
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetch_to_edit_team',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Vehicle Name",
          name: "name",
          placeholder: "Enter the name of the Vehicle",
          type: "text",
          size:false,
        },
        {
          header: "Vehicle Photo",
          name: "photo",
          placeholder: "none",
          type: "text",
        },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct loaded_percent={this.state.loaded_percent} cancel={this.updateState} username={this.state.username}
     action={"Vehicle"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading)
    {
      return(
         <div>Loading...</div>
      );
    }
    else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Vehicle Here</h5>
                    <br />
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct && 
                            <ProductList image={true} username={this.props.username} title={this.state.option}  
                            action={"Vehicle"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                
                                    <button className="btn right blue-grey darken-2 sup subm" 
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
        }
}
  }
}



