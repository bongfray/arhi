import React, { Component } from 'react';
import axios from 'axios';
import {Carousel} from 'react-materialize';


export default class Sponsor extends Component {
    constructor()
    {
        super()
        this.state={
            sponsors:[],
            loading:true,
            images:'',
            visible: 12,
            start:0,
            restore_start:0,
            restore_next:0,
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    loadMore=()=> {
        this.setState((prev) => {
            return {restore_start:prev.start,restore_next:prev.visible,start:prev.visible,
                visible: prev.visible + 12};
          });
  }
  loadPrev=()=> {
    this.setState((prev) => {
        return {start:this.state.restore_start,visible: this.state.restore_next,
            restore_start:this.state.restore_start-12,restore_next:this.state.restore_next-12};
      });
  }

    componentDidMount(){
          this.fetchSponsors();
    }
    fetchSponsors=()=>{
        axios.post('/user/fetch_sponsor')
          .then(response =>{
             var images=response.data.map(function(item){
                 return item.photo;
             });
             console.log(images)
            this.setState({
             sponsors: response.data,
             loading:false,
             images:images,
           })
          })

    }
    render() {
        if(this.state.loading)
        {
            return(
                <React.Fragment>
                <div className="center">
                    <div className="box-frames" style={{margin:'20px'}}>
                        <div className="mloader-39"></div>
                    </div>
                </div>
                <h5 className="center">
                    Fetching Sponsors Details....
                </h5>
                </React.Fragment>
            )
        }
        else{

                return (
                    <React.Fragment>
                        {this.state.sponsors.length>0 &&
                    <div id="sponsor">
                    <h1 className="center tag-head" data-aos='fade-up'> <span className="team-header1">S</span>ponsorship <span className="team-header2">D</span>etails</h1><br/>
                            <div className="row s-cont">
                                <div className="col l1 xl1 m1">
                                                {this.state.start>0 &&
                                                        <button onClick={this.loadPrev} style={{marginTop:'200%'}} type="button" 
                                                        className=" btn btn-floating btn-large #69f0ae green accent-2">
                                                        <i className="material-icons large white-text">chevron_left</i>
                                                        </button>
                                                    } 
                                </div>
                                <div className="col l10 xl10 m10">
                                    <div className="row" data-aos='fade-up' style={{marginTop:'30px'}}>
                                        {this.state.sponsors.slice(this.state.start, this.state.visible).map((content,index)=>{
                                            return(
                                            <div className="col l3 m3 center" key={index}>
                                                <div className="box">
                                                    <img src={require('../Admin/OPERATIONS/uploads/'+content.photo)}
                                                    style={{width:'100%',height:'100%'}} title={content.name} alt=""/>
                                                </div>
                                            </div>
                                            )
                                        })}   
                                    </div>
                                </div>
                                <div className="col l1 xl1 m1">
                                {this.state.visible < this.state.sponsors.length &&
                                                            <button onClick={(this.loadMore)} type="button"  style={{marginTop:'200%'}}
                                                            className="btn btn-floating btn-large #69f0ae green accent-2">
                                                            <i className="material-icons large white-text">chevron_right</i>
                                                            </button>
                                                        }
                                </div>
                            </div>


                            <Carousel
                                carouselId="Carousel-2"
                                images={
                                    [
                                        this.state.images.map(function(item,index)
                                        {
                                            return require('../Admin/OPERATIONS/uploads/'+item)
                                        })
                                    ]
                                }
                                className="imake"
                                options={{
                                   marginTop:'30%',
                                   width:'100%',
                                    indicators: true
                                  }}
                                />

                    </div>
        }
        </React.Fragment>
                )
            
        }

    }
}
