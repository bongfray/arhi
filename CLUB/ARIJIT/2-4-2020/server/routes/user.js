const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const User = require('../database/models/FAC/user')
const Sponsor = require('../database/models/FAC/sponsor')
const Team = require('../database/models/FAC/team')
const VDetails = require('../database/models/FAC/vehicleDetails')
const passport = require('../passport/fac_pass')
const crypto = require('crypto');
const multer = require("multer");
const path = require("path");
var nodemailer = require('nodemailer')


const storage = multer.diskStorage({
  destination: "../src/components/Admin/OPERATIONS/uploads/",
  filename: function(req, file, cb){
     cb(null,"IMAGE-" + Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({
  storage: storage,
  limits:{fileSize: 1000000},
}).single("myImage");

router.get('/', (req, res, next) => {
  if (req.user) {
    res.json({user: req.user})
  } else {
      res.json({ user: null })
  }
})

router.post(
  '/signin',(req, res, next) => {
        passport.authenticate('local', function(err, user, info) {
          if (err) {
            return next(err);
          }
          if (!user) {
              res.status(401);
              res.send(info.message);
              return;
          }
          req.logIn(user, function(err) {
            if (err) {
              return next(err);
            }
            var userInfo = {
                username: req.user.username,
            };
            res.send(userInfo);
          });
        })(req, res, next);
      }
)

router.post('/logout', (req, res) => {
  if (req.user) {
      req.logout()
      res.send('done')
  } else {
      res.send('no')
  }
})


router.post('/forgo', (req, res) => {
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'noreplyauv@gmail.com',
           pass: 'avmpcbqultjqadkv'
       }
   });
   User.findOne(
     {$and:[{mailid: req.body.data.mailid},{username:req.body.data.username}]
     },
   ).then(user => {
     if(user === null)
     {
       var nodata ={
         nodata:'INVALID USER !!'
       }
       res.send(nodata);
     }
     else{
       const token = crypto.randomBytes(20).toString('hex');
       // console.log(token);
       User.updateOne({$and:[{mailid: req.body.data.mailid},{username:req.body.data.username}]},
         { $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 1800000,}} ,(err, user) => {
       })
       const mailOptions = {
        from: 'noreplyauv@gmail.com', // sender address
        to: req.body.data.mailid, // list of receivers
        subject: 'Reset Password:  Team AUV', // Subject line
        html: '<p>You are receiving this message because you have requested for reset password in AUV</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 30min from now.</p>'
      };
      // console.log("Way to enter.......");
      transporter.sendMail(mailOptions, function (err, info) {
        if(err)
        {
          res.status(500).send('error');
        }
        else{
          console.log(info)
          res.send('done')
        }
     });
     }
   })
})
    router.get('/reset_password', function(req, res) {
      User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
        if (!user) {
          // console.log("Expired");
          var expire ={
            expire:"Link is Expired"
          }
          res.send(expire);
        }
        else if(user){
          res.send(user.resetPasswordToken)
        }

      });
    });

router.post('/signup', (req, res) => {
  User.findOne({username: req.body.data.username}, (err, user) => {
      if (err)
      { 
         res.status(500).send('Error!!');
      }
      else if (user)
      {
          res.send('have');
      }
      else {
        const newUser = new User(req.body.data)
          newUser.save((err, savedUser) => {
            if(err)
            {
              res.status(500).send('Error Occoured');
            }
            else if(savedUser)
            {
              res.send('done');
            }
          })
      }
  })
})


router.get('/dash2', function(req, res) {
  const { username} = req.user;
  if(!username)
  {
    var nologin = {
      nologin:'no'
    }
    res.send(nologin);
  }
  else if(username)
  {
  User.findOne({ username: username }, function(err, objs){
    if(err)
    {
      return;
    }
      else if (objs)
      {
          res.send(objs);
      }
  });
}
});


router.post('/newd', (req, res) => {
  const {up_username} = req.body;
  User.findOne({ username: req.user.username }, (err, objs) => {
      if (err)
      {
          return;
      }
      else if(objs)
      {
       if(req.body.new_password) {
        if(bcrypt.compareSync(req.body.current_password, objs.password)) {
          let hash = bcrypt.hashSync(req.body.new_password, 10);
          User.updateOne({username:req.user.username},{ $set: 
            { mailid: up_username,password: hash}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "fail"
          };
          res.send(fail);
        }
      }
      else if(!req.body.new_password)
      {
        User.updateOne({username: req.user.username},
          { $set: { mailid: up_username}} ,(err, user) => {
          var succ= {
            succ: "Datas are Updated",
          };
          res.send(succ);
        })
      }
    }
  })
}
)

router.post('/setSignup', function(req, res) {
  Team.findOne({action:req.body.action},(err,found)=>{
    if(err)
    {
      res.status(500).send('error');
    }
    else if(found)
    {
      console.log(req.body)
      Team.updateOne({action:req.body.action},{status:req.body.status},(error,done)=>{
        if(error)
        {

        }
        else{
          console.log(done)
          res.send('done')
        }
      })
    }
    else{
      var newdata= new Team(req.body);
      newdata.save((error1,saved)=>{
        if(error1)
        {
          res.status(500).send('error');
        }
        else if(saved)
        {
          res.send('done')
        }
      })
    }
  })
});

router.post('/admin_action', function(req, res) {
  Team.findOne({action:req.body.action},(err,found)=>{
    if(err)
    {
      res.status(500).send('error');
    }
    else if(found)
    {
      res.send(found);
    }
    else{
      res.send('no');
    }
  })
});


function mail_send(mailid,content,res,id)
{
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'noreplyauv@gmail.com',
           pass: 'uolqdatwxgbhqals'
       }
   });
       const mailOptions = {
        from: 'noreplyauv@gmail.com', // sender address
        to: mailid, // list of receivers
        subject: 'eWork - A message from eWork Team', // Subject line
        html: '<p>Dear User,</p><br /><p>'+content+'</p><br />Thank You.'
      };
      transporter.sendMail(mailOptions, function (err, info) {
        if(err)
        {

        }
        else
        {
          res.send('ok');
        }
     });
}


router.post('/sponsor_details', (req, res) => {
  upload(req, res, (err) => {
    // console.log("Request file ---", req.file);//Here you get file.
    // console.log("Request file ---", req.file.filename);
     console.log("Request file ---", req.body.serial);
    /*Now do where ever you want to do*/
    if(err)
    {
      return res.status(500).send('Upload Not Completed');
    }
    else
    {
      Sponsor.findOne({name:req.body.spons_name},(error,found)=>{
        if(error)
        {

        }
        else if(found)
        {
          console.log('Fouund Here')
          if(found.name === req.body.spons_name)
          {
            console.log('As')
            if(!(req.body.serial === undefined))
            {
              Sponsor.updateOne({serial:req.body.serial},{$set:{photo:req.file.filename}},(error,found)=>{
                res.send('done')
              })
              // console.log('Updae')
            }
            else
            {
              res.send('have')
            }
          }
          else
          {
            console.log('AT')
            if(req.file)
            {
              console.log('agss')
              Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name,photo:req.file.filename}},(error,found)=>{
                res.send('done')
              })
            }
            else
            {
              console.log('Dagss')
              Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name}},(error,found)=>{
                res.send('done')
              })
            }

            console.log('Updae')
          }
        }
        else
        {
          console.log('Not Fouund Here')
          console.log(req.body.serial)
          if(!(req.body.serial === 'undefined'))
          {
            if(req.file)
            {
              console.log('agss')
              Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name,photo:req.file.filename}},(error,found)=>{
                res.send('done')
              })
            }
            else
            {
              console.log('Dagss')
              Sponsor.updateOne({serial:req.body.serial},{$set:{name:req.body.spons_name}},(error,found)=>{
                res.send('done')
              })
            }
          }
          else
          {
            const newSponser = new Sponsor({
              name:req.body.spons_name,
              photo:req.file.filename,
              })
            newSponser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
          }
        }
      })
    }   
 });
})


router.post('/fetch_sponsor', (req, res) => {
  Sponsor.find({},(error,found)=>{
    if(error)
    {

    }
    else{
      res.send(found)
    }
  })
})

router.post('/fetch_to_edit_sponsor',function(req,res) {
  Sponsor.findOne({$and: [{serial: req.body.id}]}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/del_sponsor',function(req,res) {
  Sponsor.deleteOne({$and: [{serial:  req.body.serial }]},function(err,succ){
    res.send("OK")
  });
});

/*team-alumni- Handle*/

router.post('/team_details', (req, res) => {
  upload(req, res, (err) => {
    if(err)
    {
      return res.status(500).send('Upload Not Completed');
    }
    else
    {
      Team.findOne({name:req.body.name},(error,found)=>{
        if(error)
        {

        }
        else if(found)
        {
          console.log('Fouund Here')
          if(found.name === req.body.name)
          {
            console.log('As')
            if(!(req.body.serial === undefined))
            {
              if(req.file)
              {
                console.log('agss')
                Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,photo:req.file.filename,
                  action:req.body.action,
                  dept:req.body.dept,
                  about_member: req.body.about_member,
                  alumni_journey: req.body.alumni_journey,
                  linkedin: req.body.linkedin,
                  gmail: req.body.gmail,
                  insta: req.body.insta,
                  regno: req.body.regno}},(error,found)=>{
                  res.send('done')
                })
              }
              else
              {
                console.log('Dagss')
                Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,
                  action:req.body.action,
                  dept:req.body.dept,
                  about_member: req.body.about_member,
                  alumni_journey: req.body.alumni_journey,
                  linkedin: req.body.linkedin,
                  gmail: req.body.gmail,
                  insta: req.body.insta,
                  regno: req.body.regno}},(error,found)=>{
                  res.send('done')
                })
              }
        
            }
            else
            {
              res.send('have')
            }
          }
          else
          {
            console.log('AT')
            if(req.file)
            {
              console.log('agss')
              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,photo:req.file.filename,
                action:req.body.action,
                dept:req.body.dept,
                about_member: req.body.about_member,
                alumni_journey: req.body.alumni_journey,
                linkedin: req.body.linkedin,
                gmail: req.body.gmail,
                insta: req.body.insta,
                regno: req.body.regno}},(error,found)=>{
                res.send('done')
              })
            }
            else
            {
              console.log('Dagss')
              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,
                action:req.body.action,
                dept:req.body.dept,
                about_member: req.body.about_member,
                alumni_journey: req.body.alumni_journey,
                linkedin: req.body.linkedin,
                gmail: req.body.gmail,
                insta: req.body.insta,
                regno: req.body.regno}},(error,found)=>{
                res.send('done')
              })
            }

            console.log('Updae')
          }
        }
        else
        {
          console.log('Not Fouund Here')
          console.log(req.body.serial)
          if(!(req.body.serial === 'undefined'))
          {
            if(req.file)
            {
              console.log('agss')
              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,photo:req.file.filename,
                action:req.body.action,
                dept:req.body.dept,
                about_member: req.body.about_member,
                alumni_journey: req.body.alumni_journey,
                linkedin: req.body.linkedin,
                gmail: req.body.gmail,
                insta: req.body.insta,
                regno: req.body.regno}},(error,found)=>{
                res.send('done')
              })
            }
            else
            {
              console.log('Dagss')
              console.log(req.body.serial)
              Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{name:req.body.name,
                dept:req.body.dept,
                about_member: req.body.about_member,
                alumni_journey: req.body.alumni_journey,
                linkedin: req.body.linkedin,
                gmail: req.body.gmail,
                insta: req.body.insta,
                regno: req.body.regno,}},(err1,done)=>{
                if(err1)
                {
                  console.log('err')
                }
                else if(done)
                {
                  res.send('done')
                }
                else{
                  console.log('no')
                }
              })
            }
          }
          else
          {
            const newSponser = new Team({
              action:req.body.action,
              name:req.body.name,
              photo:req.file.filename,
              dept:req.body.dept,
              about_member: req.body.about_member,
              alumni_journey: req.body.alumni_journey,
              linkedin: req.body.linkedin,
              gmail: req.body.gmail,
              insta: req.body.insta,
              regno: req.body.regno,
              })
            newSponser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
          }
        }
      })
    }   
 });
})

router.post('/fetch_team', (req, res) => {
  Team.find({action:req.body.action},(error,found)=>{
    if(error)
    {

    }
    else{
      res.send(found)
    }
  })
})

router.post('/fetch_to_edit_team',function(req,res) {
  Team.findOne({$and: [{serial: req.body.id}]}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/del_team',function(req,res) {
  Team.deleteOne({$and: [{serial:  req.body.serial }]},function(err,succ){
    res.send("OK")
  });
});


router.post('/save_gallery_photo', (req, res) => {
  upload(req, res, (err) => {
    if(err)
    {
      return res.status(500).send('Upload Not Completed');
    }
    else
    {
      console.log(req.body.serial)
            if(!(req.body.serial === 'undefined'))
            {
              if(req.file)
              {
                console.log('agss')
                Team.updateOne({$and:[{serial:req.body.serial},{action:req.body.action}]},{$set:{photo:req.file.filename}},(error,found)=>{
                  res.send('done')
                })
              }
              else
              {
                res.send('no');
              }
            }
            else
            {
              const newSponser = new Team({
                action:req.body.action,
                photo:req.file.filename,
                })
              newSponser.save((err, savedUser) => {
                var succ= {
                  succ: "Successful SignedUP"
                };
                res.send(succ);
              })
            }
    }   
 });
})


router.post('/achievment_details', function(req,res) {
  console.log(req.body)
  var trans = new Team(req.body.data);
  trans.save((err, savedUser) => {
    if(err)
    {
      res.status(500).send('Error');
    }
    else if(savedUser)
    {
      res.send('done')
    }
  })
});


router.post('/edit_achievment', function(req,res) {
  console.log(req.body.data)
  Team.updateOne({$and: [{ serial: req.body.data.serial},{action: req.body.data.action}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })
});


router.post('/add_details_of_vehicle', function(req,res) {
  //console.log(req.body)
  var trans = new VDetails(req.body.data);
  trans.save((err, savedUser) => {
    if(err)
    {
      res.status(500).send('Error');
    }
    else if(savedUser)
    {
      res.send('done')
    }
  })
});


router.post('/edit_vehicle_details', function(req,res) {
 // console.log(req.body.data)
 VDetails.updateOne({$and: [{ serial: req.body.data.serial},{action: req.body.data.action}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Successful SignedUP"
    };
    res.send(succ);
  })
});

router.post('/fetch_to_edit_vehicle_details',function(req,res) {
  VDetails.findOne({$and: [{serial: req.body.id}]}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
 });
})

router.post('/fetch_vdetails', (req, res) => {
  console.log(req.body)
  VDetails.find({$and:[{type:req.body.type},{action:req.body.action}]},(error,found)=>{
    if(error)
    {

    }
    else{
      res.send(found)
    }
  })
})


router.post('/del_vehicle_details',function(req,res) {
  VDetails.deleteOne({$and: [{serial:  req.body.serial }]},function(err,succ){
    res.send("OK")
  });
});





module.exports = router
