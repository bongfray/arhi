const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');
//Schema
const teamschema = new Schema({
  name:{ type: String, unique: false, required: false },
  type: { type: String, unique: false, required: false },
  data: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
})


teamschema.plugin(autoIncrement.plugin, { model: 'VehicleDetails', field: 'serial', startAt: 1,incrementBy: 1 });

const VehicleDetails = mongoose.model('VehicleDetails',teamschema)
module.exports = VehicleDetails

