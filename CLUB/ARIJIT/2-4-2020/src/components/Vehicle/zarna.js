import React, { Component } from 'react'
import ZImg from './zarnaimg.jpg'

export default class Zarna extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="row">
                    
                    <h1 className="center tag-head-v col s12"><span className="z-head">ZARNA</span> <br/><span className="team-header1">A</span>utonomous <span className="team-header2">U</span>nderwater <span className="team-header4">V</span>ehicle </h1><br/>
                    <div className="col s1"></div>
                    <div className="col s10">
                    
                        <img src={ZImg} className="col s12" alt="" width="100%"/>
                        <div className=" col s6">
                            <h4 className="dim-h4">Vehicle Dimensions</h4>
                            <p className="dim-p">Length : 850mm <br/> Breadth : 320mm <br/> Height : 350mm</p>
                            <h4 className="dim-h4">Mass of vehicle</h4>
                            <p className="dim-p">34 kg in air</p>
                            <h4 className="dim-h4">Top Speed</h4>
                            <p className="dim-p">1 m/s</p>
                            <h4 className="dim-h4">Degrees of Freedom</h4>
                            <p className="dim-p">6 (Roll, Pitch,Yaw)(x,y,z)</p>
                            
                            <h4 className="dim-h4">Propulsion System</h4>
                            <p className="dim-p">8 Bluerobotics Thrusters <br/>Depth Rating – 150m in fresh water<br/>Maximum thrust forward @16V is 5.1 kgf <br/>Maximum thrust reverse @16V is 4.1 kgf <br/> Rotational speed of 300-3800 rev/min</p>
                            
                        </div>

                        <div className=" col s6">
                            
                            <h4 className="dim-h4">Navigational Components</h4>
                            <p className="dim-p">Sparton based AHRS-8 </p>
                            <h4 className="dim-h4">Single Board Computer Model</h4>
                            <p className="dim-p">Nvidia Jetson Tx1</p>
                            <h4 className="dim-h4">Camera Sensors:</h4>
                            <p className="dim-p">IDS Camera <br/> Logitech Web Cam</p>
                            <h4 className="dim-h4">Software Architecture</h4>
                            <p className="dim-p">ROS Melodic(Robotic Operating System) <br/> Ubuntu 18.04</p>
                            <h4 className="dim-h4">Power System</h4>
                            <p className="dim-p">LiPo Battery <br/> Battery Capacity: 10Ah, 4S  <br/>Config: 4S/14.8V/4 <br/> Cell Constant Discharge: 30C <br/> Peak Discharge: 10C <br/> Total Weight: 2496g <br/> Battery Volume: 150mm*43mm*27mm</p>
                        </div>
                    </div>

                    <div className="col s1"></div>
                    
                </div>
            </React.Fragment>
            
        )
    }
}
