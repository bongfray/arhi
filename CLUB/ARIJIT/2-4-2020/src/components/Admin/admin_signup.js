import React, { Component } from 'react';
import { Link,Redirect } from 'react-router-dom';
import M from 'materialize-css';
import Axios from 'axios';

export default class AdminSignup extends Component {
    constructor()
    {
        super()
        this.state={
            username:'',
            password:'',
            mailid:'',
            signup:false,
            redirectTo:null,
        }
        this.componentDidMount =this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        M.AutoInit();
        this.getUser();
    }
    getUser=()=>{
        Axios.get('/ework/user/')
        .then(res=>{
            if(res.data.user === null)
            {

            }
            else{
                window.M.toast({html: 'Already Logged In !!',classes:'green darken-1 rounded'});
                this.setState({redirectTo:'/admin_panel'})
            }
        })
    }
    signIn=()=>{
        window.M.toast({html: 'Signing In !!',classes:'orange rounded'});
        Axios.post('/ework/user/signin',{username:this.state.username,password:this.state.password})
        .then(res=>{
            this.setState({redirectTo:'/admin_panel'})
            window.M.toast({html: 'Signed In !!',classes:'green rounded'});
        })
        .catch(error=>{
            this.setState({block_click:false})
            if(error.response.status === 401)
            {
                window.M.toast({html: error.response.data,classes:'red rounded'});
            }
            else
            {
                window.M.toast({html: 'Something Went Wrong !!',classes:'red rounded'});
            }
        })
    }

    sendMail=(e)=>{
        e.preventDefault();
        if(!this.state.username || !this.state.mailid)
        {
            window.M.toast({html: 'Enter all the details !!',classes:'red rounded'});
        }
        else
        {
            window.M.toast({html: 'Sending Mail...!!',classes:'green darken-1 rounded'});
            Axios.post('/ework/user/forgo',{data:this.state})
            .then(res=>{
                window.M.toast({html: 'Check your Mail Id !!',classes:'green darken-1 rounded'});
            })
            .catch(err=>{
                window.M.toast({html: 'Something Went Wrong  !!',classes:'red rounded'});
            })
        }
    }
    render()
    {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
        return(
            <React.Fragment>
                  <div id="modal1" className="modal">
                    <div className="modal-content">
                    <h4 className="center">Reset Password</h4>
                    <br />
                     <div className="input-field ">
                        <input id="mailid" type="email" 
                        onChange={(e)=>this.setState({mailid:e.target.value})} className="validate" required />
                        <label htmlFor="mailid">Mail ID</label>
                     </div>
                     <br />
                     <div className="input-field">
                        <input id="username" type="text" 
                        onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                        <label htmlFor="username">Username</label>
                     </div>
                     <br />
                     <button className="btn btn-small right col l4" onClick={this.sendMail}>Send Mail</button>
                    </div>
                    <div className="modal-footer">
                    <a href="#!" className="modal-close waves-effect waves-green btn-flat">Close</a>
                    </div>
                </div>

                {this.state.signup ? <Signup over={()=>this.setState({signup:false})} /> :
               <div className="row">
                   <div className="col l4 xl4 m4"/>
                    <div className="col l4 xl4 m4 s12">
                        <div className="card hoverable" style={{borderRadius:'10px',marginTop:'120px'}}>
                            <div className="card-content">
                                <span className="center card-title">SIGN IN</span>
                                <div className="row">
                                <div className="input-field col l12 xl12 s12 m12">
                                    <input id="susername" type="text" onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                                    <label htmlFor="susername">Username</label>
                                    </div>
                                    <div className="input-field col l12 xl12 s12 m12">
                                    <input id="spassword" type="password" onChange={(e)=>this.setState({password:e.target.value})} className="validate" required />
                                    <label htmlFor="spassword">Password</label>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col l6 xl6 s6 m6">
                                         <button style={{width:'100%'}} onClick={()=>this.setState({signup:true})} className="left btn btn-small pink">SIGN UP</button>
                                    </div>
                                    <div className="col l6 xl6 s6 m6">
                                     <button style={{width:'100%'}} type="submit" onClick={this.signIn} disabled={(this.state.username && this.state.password) ? false :true}
                                      className="btn btn-small pink right">SIGN IN</button>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action center">
                                    <a className="red-text modal-trigger" href="#modal1">Forgot Password</a>
                            </div>
                        </div>
                    </div>
                   <div className="col l4 xl4 m4"/>
               </div> 
               }
            </React.Fragment>
        )
            }
    }
}

class Signup extends Component {
    constructor()
    {
        super()
        this.state={
            username:'',
            password:'',
            mailid:'',
            renderStat:true,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchState();
    }
    fetchState=()=>{
        Axios.post('/ework/user/admin_action',{action:'signup_stat'})
        .then(res=>{
            if(!(res.data==='no'))
            {
                this.setState({renderStat:res.data.status,loading:false})
            }
        })
    }
    submit=(e)=>{
        e.preventDefault();
        if(!this.state.username || !this.state.password || !this.state.mailid)
        {
            window.M.toast({html: 'Enter all the details !!',classes:'red rounded'});
        }
        else{
            window.M.toast({html: 'Signing Up !!',classes:'orange rounded'});
            Axios.post('/ework/user/signup',{data:this.state})
            .then(res=>{
                if(res.data === 'have')
                {
                    window.M.toast({html: 'User already there !!',classes:'red rounded'});
                }
                else
                {
                    window.M.toast({html: 'Signed Up !!',classes:'green darken-1 rounded'});
                }
            })
            .catch(err=>{
                window.M.toast({html: 'Something Went Wrong !!',classes:'red rounded'});
            })
        }
    }
    render()
    {
        return(
            <div className="row">
            <div className="col l4"/>
             <div className="col l4" >
                 <div className="card hoverable" style={{borderRadius:'10px',marginTop:'120px'}}>
                     <div className="card-content">
                         {this.state.renderStat ?
                         <React.Fragment>
                            <span className="center card-title">SIGN UP</span>
                            <div className="row">
                                <div className="input-field col l12 xl12 s12 m12">
                                    <input id="username" type="text" onChange={(e)=>this.setState({username:e.target.value})} className="validate" required />
                                    <label htmlFor="username">Username</label>
                                </div>
                                <div className="input-field col l12 xl12 s12 m12">
                                    <input id="mailid" type="email" onChange={(e)=>this.setState({mailid:e.target.value})} className="validate" required />
                                    <label htmlFor="mailid">Email Id</label>
                                </div>
                                <div className="input-field col l12 xl12 s12 m12">
                                <input id="password" type="password" onChange={(e)=>this.setState({password:e.target.value})} className="validate" required />
                                <label htmlFor="password">Password</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col l6 xl6 s6 m6">
                                    <div style={{width:'100%'}} onClick={this.props.over} className="left go">Sign In</div>
                                </div>
                                <div className="col l6 xl6 s6 m6">
                                <button style={{width:'100%'}} type="submit" onClick={this.submit}
                                className="btn btn-small pink right">SIGN UP</button>
                                </div>
                            </div>
                        </React.Fragment>
                     :
                     <React.Fragment>
                         <h6 className="center">We are not acceping signup requests !!</h6>
                         <br />
                         <Link to="/"><button className="btn btn-small pink center">HOME</button></Link>
                     </React.Fragment>
                     }
                    </div>
                 </div>
             </div>
            <div className="col l4"/>
        </div> 
        )
    }
}