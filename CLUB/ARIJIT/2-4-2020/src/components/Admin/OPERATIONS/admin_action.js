import React, { Component } from 'react'
import Axios from 'axios';



export default class SSSS extends Component {
    constructor()
    {
        super()
        this.state={
            status:false,
            loading:true
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchStat();
    }
    fetchStat=()=>{
        Axios.post('/ework/user/admin_action',{action:'signup_stat'})
        .then(res=>{
            if(!(res.data==='no'))
            {
                this.setState({status:res.data.status,loading:false})
            }
        })
    }
    setSignup=(e)=>{
        this.setState({action:e.target.value,status:!this.state.status})
        Axios.post('/ework/user/setSignup',{action:e.target.value,status:!this.state.status})
        .then(res=>{
            
        })
    }
    render()
    {
        return(
            <div className="row">
                <div className="col l4 card">
                    Render Signup Page - 
                        <p>
                            <label>
                                <input checked={this.state.status} type="checkbox" value="signup_stat" 
                                onChange={this.setSignup}/>
                                <span>Status</span>
                            </label>
                        </p>
                </div>

            </div>
        )
    }
}