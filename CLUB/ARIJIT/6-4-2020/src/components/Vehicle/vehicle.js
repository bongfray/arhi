import React, { Component } from 'react';
import Axios from 'axios';
import ShowDetails from './zarna';

export default class Vehicle extends Component {
    constructor()
    {
        super()
        this.state={
            newone:'',
            showDetails:false,
            vehicle:'',
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        Axios.post('/ework/user/fetchRecent',{action:'Vehicle'})
        .then(res=>{
            console.log(res.data)
            this.setState({newone:res.data,loading:false})
            
        })
        .catch(err=>{

        })
    }
    render() {
        if(this.state.loading)
        {
            return(
                <div className="center">Loading...</div>
            )
        }
        else{
                return (
                    <React.Fragment>
                        {this.state.showDetails && 
                         <div className="notinoti">
                            <div class="btn-floating btn-small red" style={{margin:'5px'}}>
                              <i className="material-icons right" onClick={()=>this.setState({showDetails:false})}>close</i>
                            </div>
                            <ShowDetails vehicle={this.state.vehicle} />
                         </div>
                        }
                        <div className="vehicle">
                            <div className="row">
                                <div className="col l3"></div>
                                <div className="col s12 l6">
                                <div className="floating center">
                                    <span className="zarna-name">{this.state.newone.length === 0 ? this.state.newone.name : 'ZARNA'}</span><br/>
                                <p className="float fade"><span className="team-header4">A</span>utonomous <span className="team-header1">U</span>nderwater <span className="team-header3">V</span>ehicle 
                                </p>
                                </div>
                                </div>
                                <div className="col l3"></div>
                            </div>
                                <div className="row">
                                    <div className="col l4"></div>
                                    <div className="col l4 s12 center">
                                    <div onClick={()=>this.setState({showDetails:true,vehicle: this.state.newone.name})} className="go v-find fade">
                                         Find out more about our latest AUV here <span className="fa fa-arrow-right small"></span>
                                    </div>
                                    </div>
                                    <div className="col l4"></div>
                                </div>
                            </div>
                            
                        
                    </React.Fragment>
                        
                )
      }
    }
}
