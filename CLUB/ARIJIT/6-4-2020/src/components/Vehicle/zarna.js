import React, { Component } from 'react';
import ZImg from './zarnaimg.jpg';
import Axios from 'axios';

export default class Zarna extends Component {
    constructor()
    {
        super()
        this.state={
            details:false,
            image:'',
            vehicle_dimension:[],
            mass_vehicle:[],
            top_speed:[],
            dof:[],
            propulation:[],
            navigational:[],
            computer_model:[],
            cam_sensor:[],
            soft_arch:[],
            power_system:[],
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchVehicleInfo();
    }
    fetchVehicleInfo=()=>{
        Axios.post('/ework/user/vehiclePhoto',{vehicle:this.props.vehicle})
        .then(res=>{
                this.setState({image:res.data.photo})
        })
        .catch(err=>{

        })
        this.fetchDetails();
    }
    fetchDetails=()=>{
        Axios.post('/ework/user/detail_info',{vehicle:this.props.vehicle})
        .then(res=>{
            var vehicle_dimension =res.data.filter(item=>item.type === "Vehicle Dimensions");
            var mass_vehicle =res.data.filter(item=>item.type === "Mass of vehicle");
            var top_speed =res.data.filter(item=>item.type === "Top Speed");
            var dof =res.data.filter(item=>item.type === "Degrees of Freedom");
            var propulation =res.data.filter(item=>item.type === "Propulsion System");
            var navigational =res.data.filter(item=>item.type === "Navigational Components");
            var computer_model=res.data.filter(item=>item.type === "Single Board Computer Model");
            var cam_sensor =res.data.filter(item=>item.type === "Camera Sensors");
            var soft_arch =res.data.filter(item=>item.type === "Software Architecture");
            var power_system =res.data.filter(item=>item.type === "Power System");
            this.setState({
                vehicle_dimension:vehicle_dimension,
                mass_vehicle:mass_vehicle,
                top_speed:top_speed,
                dof:dof,
                propulation:propulation,
                navigational:navigational,
                computer_model:computer_model,
                cam_sensor:cam_sensor,
                soft_arch:soft_arch,
                power_system:power_system,
                loading:false
            })
        })
        .catch(err=>{
            this.setState({loading:false})
        })
        
    }
    render() {
        if(this.state.loading)
        {
            return(
                <div className="center">Loading ....</div>
            )
        }
        else{
            return (
                <React.Fragment>
                    <div className="row">
                        
                        <h1 className="center tag-head-v col s12"><span className="z-head">{this.props.vehicle ? this.props.vehicle : 'ZARNA'}</span> <br/><span className="team-header1">A</span>utonomous <span className="team-header2">U</span>nderwater <span className="team-header4">V</span>ehicle </h1><br/>
                        <div className="col s1"></div>
                        <div className="col s10">
                        
                            <img src={this.state.image ? require('../Admin/OPERATIONS/uploads/'+this.state.image) : ZImg } 
                            className="col s12" alt="" width="100%"/>
                            <div className=" col s6">
                                <h4 className="dim-h4">Vehicle Dimensions</h4>
                                {this.state.vehicle_dimension.map((content,index)=>{
                                   return(
                                    <p className="dim-p" key={index} style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                   )
                                })}
                                {/* <p className="dim-p">Length : 850mm <br/> Breadth : 320mm <br/> Height : 350mm</p> */}
                                <h4 className="dim-h4">Mass of vehicle</h4>
                                {this.state.mass_vehicle.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.mass_vehicle.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }
                                

                                <h4 className="dim-h4">Top Speed</h4>

                                {this.state.top_speed.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.top_speed.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }


                                <h4 className="dim-h4">Degrees of Freedom</h4>
                                {this.state.dof.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.dof.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }

                                {/* <p className="dim-p">6 (Roll, Pitch,Yaw)(x,y,z)</p> */}
                                

                                <h4 className="dim-h4">Propulsion System</h4>

                                {this.state.propulation.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.propulation.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }

                                {/* <p className="dim-p">8 Bluerobotics Thrusters <br/>Depth Rating – 150m in fresh water<br/>Maximum thrust forward @16V is 5.1 kgf <br/>Maximum thrust reverse @16V is 4.1 kgf <br/> Rotational speed of 300-3800 rev/min</p> */}
                                
                            </div>
    
                            <div className=" col s6">
                                
                                <h4 className="dim-h4">Navigational Components</h4>

                                {this.state.navigational.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.navigational.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }
                                
                                {/* <p className="dim-p">Sparton based AHRS-8 </p> */}


                                <h4 className="dim-h4">Single Board Computer Model</h4>
                                {this.state.computer_model.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.computer_model.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }
                                {/* <p className="dim-p">Nvidia Jetson Tx1</p> */}


                                <h4 className="dim-h4">Camera Sensors</h4>
                                {this.state.cam_sensor.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.cam_sensor.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }

                                {/* <p className="dim-p">IDS Camera <br/> Logitech Web Cam</p> */}


                                <h4 className="dim-h4">Software Architecture</h4>
                                {this.state.soft_arch.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.soft_arch.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }
                                {/* <p className="dim-p">ROS Melodic(Robotic Operating System) <br/> Ubuntu 18.04</p> */}

                                <h4 className="dim-h4">Power System</h4>
                                {this.state.power_system.length>0 ? 
                                    <React.Fragment>
                                                                {this.state.power_system.map((content,index)=>{
                                                                    return(
                                                                     <p className="dim-p" key={index}
                                                                      style={{whiteSpace: 'pre-line'}}>{content.data}</p>
                                                                    )
                                                                 })}
                                    </React.Fragment>
                                  :
                                  <p className="dim-p">No Data</p>
                                }

                                {/* <p className="dim-p">LiPo Battery <br/> Battery Capacity: 10Ah, 4S  <br/>Config: 4S/14.8V/4 <br/> Cell Constant Discharge: 30C <br/> Peak Discharge: 10C <br/> Total Weight: 2496g <br/> Battery Volume: 150mm*43mm*27mm</p> */}
                            </div>
                        </div>
    
                        <div className="col s1"></div>
                        
                    </div>
                </React.Fragment>
                
            )
        }
    }
}
