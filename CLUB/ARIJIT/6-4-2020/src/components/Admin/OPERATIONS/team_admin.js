import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';
import M from 'materialize-css';


export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      loading:false,
      uploading:false,
      loaded_percent:0,
      username:'',
      redirectTo:'',
      option:'',
      file:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }
componentDidMount(){
  M.AutoInit();
}


   onCreate = (e,index) => {

     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;

       apiUrl = '/ework/user/team_details';
     
     const formData = new FormData();
     formData.append('myImage',data.file);
     formData.append('name',data.name);
     formData.append('domain',data.domain);
     formData.append('r_type',data.r_type);
     formData.append('about_member',data.about_member);
     formData.append('alumni_journey',data.alumni_journey);
     formData.append('linkedin',data.linkedin);
     formData.append('gmail',data.gmail);
     formData.append('github',data.github);
     formData.append('action',data.action);
     formData.append('serial',data.serial);
     window.M.toast({html: 'Uploading ....!!',classes:'orange rounded'});
     axios.post(apiUrl,formData,{
         onUploadProgress:ProgressEvent=>{
              var loaded = Math.round(ProgressEvent.loaded/ProgressEvent.total*100);
              console.log(loaded)
              this.setState({uploading:true,loaded_percent:loaded})
         }
     })
         .then(response => {
          if(response.data === 'have')
          {
           window.M.toast({html: 'Already There !!',classes:'pink rounded'});
           this.setState({disable_btn:false})
          }
          else{
           this.setState({
            uploading:false,
            loaded_percent:0,
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
          }
         })
    
   }

   editProduct = (productId,index)=> {
     axios.post('/ework/user/fetch_to_edit_team',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

  render()
  {
    let productForm;
    var data = {
      fielddata: [
            {
              header: "Member Name",
              name: "name",
              placeholder: "Enter the Member Name",
              type: "text",
              size:false,
              alumni:true,
            },
            { 
              header: "Responsibility Type",
              name: "r_type",
              placeholder: "Enter the Responsibility Type",
              type: "text",
              select:true,
              alumni:true,
            },
            {
            header: "Domain",
            name: "domain",
            placeholder: "Enter the Domain Type",
            type: "text",
            domain_type:true,
          },
          {
            header: "About The Member/Alumni/Faculty",
            name: "about_member",
            placeholder: "Brief about the team member",
            type: "text",
            size:true,
            alumni:true,
          },
          {
            header: "Their Journey",
            name: "alumni_journey",
            placeholder: "Brief about Journey",
            type: "text",
            size:true,
            alumni:true,
          },
          {
            header: "LinkedIn Account",
            name: "linkedin",
            placeholder: "Enter LinkedIn Account",
            type: "text",
            size:false,
            alumni:true,
          },
        {
          header: "Gmail Account",
          name: "gmail",
          placeholder: "Enter Gmail Account",
          type: "text",
          size:false,
          alumni:true,
        },
        {
          header: "GitHub Account",
          name: "github",
          placeholder: "Enter GitHub Account",
          type: "text",
          size:false,
        },
        {
            header: "Uploaded Photo",
            name: "photo",
            placeholder: "none",
            type: "text",
          },
      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct) {
    productForm = <AddProduct loaded_percent={this.state.loaded_percent} cancel={this.updateState} username={this.state.username}
     action={"Team Member"} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading ===  true)
    {
      return(
         <div>Loading...</div>
      );
    }
    else{
         if(this.state.uploading)
         {
             return(
                 <div className="center">Loading  {this.state.loaded_percent}</div>
             )
         }
         else{
                return(
                <React.Fragment>
                    <h5 className="center prof-heading black-text">Manage Team Member</h5>
                    <br />
                    <div className="sponsor">
                        <div>
                        {!this.state.isAddProduct && 
                            <ProductList image={true} username={this.props.username} title={this.state.option}  
                            action={"Team Member"} data={data}  editProduct={this.editProduct}/>
                        }<br />
                        {!this.state.isAddProduct &&
                        <React.Fragment>
                        <div className="row">
                            <div className="col l6 s12 xl6 m12 left" />
                            <div className="col l6 s6 xl6 m6">
                                    <button className="btn right blue-grey darken-2 sup subm" 
                                    onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                </div>
                            </div>
                        </React.Fragment>
                        }
                        { productForm }
                        <br/>
                        </div>
                    </div>
                </React.Fragment>
                )
          }
        }
}
  }
}



