import React, { Component } from 'react'
import $ from 'jquery'
import Axios from 'axios';

export default class Contact extends Component {
    constructor()
    {
        super()
        this.state={
            faculty:'',
            h_team:'',
            lead:'',
            member:'',
            alumni:'',
            fetching:true,
            moving_icon:'',
            active:'',
            clicked:false,
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchTeam();
    }

    settedClass=(num)=>{
        if(this.state.active === num && this.state.clicked === num)
        {      
            return 'mc-active';
        }
        else{
            return '';
        }
    }
    setSpin=(ins)=>{
            this.setState({clicked:ins,active:ins})
    }

    moveIcon=(incom)=>{
        if((this.state.active === incom))
        {
            if(this.state.clicked === incom)
            {
                return 'fa-arrow-left';
            }
            else{
                return 'fa-bars';
            }
          
        }
        else
        {
            return 'fa-bars';
        }
    }

    fetchTeam=()=>{
        Axios.post('/ework/user/fetch_team',{
            action: 'Team Member',
          })
          .then(response =>{
              var faculty = response.data.filter(item=>item.r_type === 'Faculty');
              var h_team = response.data.filter(item=>item.r_type === 'Head of Team');
              var lead = response.data.filter(item=>item.r_type === 'Lead');
              let member = response.data.filter(item=>item.r_type === 'Member');
              let alumni = response.data.filter(item=>item.r_type === 'Alumni');
            this.setState({
             faculty:faculty,
             h_team:h_team,
             lead:lead,
             member:member,
             alumni:alumni,
             fetching:false,
           })
          })
    }

    render() {
 if(this.state.fetching)
 {
     return(<div></div>)
 }
 else{
        return (
            <React.Fragment>
                <div class="gallery">
                        <div class="gallery-image">
                            <img src={require('./team.jpg')} style={{height:'100%',width:'100%'}} />
                                <div class="gallery-text">
                                    <h3>Meet Our Team</h3>
                                </div>
   
                        </div>  
                    </div>


           
            <section className="team-details">
              <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                <h5>Faculty Member</h5>
                <div class="line"></div>
              </div>
                <div className="row active-with-click" style={{paddingTop:'50px'}}>
                    {this.state.faculty.length === 0 ? 
                    <div className="center">No Data</div>
                    :
                    <React.Fragment>
                        {this.state.faculty.map((content,index)=>{
                            return(
                                    <div className="col l3 s12" key={index}>

                                            <article className={"material-card Red "+this.settedClass(content.serial)}>
                                                    <h2>
                                                        <span>{content.name}</span>
                                                        <strong>
                                                            The Deer Hunter
                                                        </strong>
                                                    </h2>
                                                    <div className="mc-content">
                                                        <div className="img-container">
                                                            <img alt="team" className="img-responsive"
                                                             src={require('../Admin/OPERATIONS/uploads/'+content.photo)}/>
                                                        </div>
                                                        <div className="mc-description">
                                                           {content.about_member}
                                                        </div>
                                                    </div>
                                                    <a className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                    <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                    </a>
                                                    <div className="mc-footer">
                                                        <h4>
                                                            Social Accounts
                                                        </h4>
                                                        <div className="center">
                                                        <a href={content.github} target="_blank" className="fa fa-fw fa-github"></a>
                                                        <a href={content.linkedin} target="_blank" className="fa fa-fw fa-linkedin"></a>
                                                        <a href={content.gmail} target="_blank" className="fa fa-fw fa-google"></a>
                                                        </div></div>
                                                </article>    
                                    </div>
                            )
                        })}
                    </React.Fragment>
                    } 
                    </div>

                    <br />

                    <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Alumni</h5>
                        <div class="line"></div>
                    </div>
                    <div className="row active-with-click" style={{paddingTop:'50px'}}>
                        {this.state.alumni.length === 0 ? 
                        <div className="center">No Data</div>
                        :
                        <React.Fragment>
                            {this.state.alumni.map((content,index)=>{
                                return(
                                    <div className="col l3 s12" key={index}>

                                            <article className={"material-card Red "+this.settedClass(content.serial)}>
                                                    <h2>
                                                        <span>{content.name}</span>
                                                        <strong>
                                                         {content.about_member}
                                                        </strong>
                                                    </h2>
                                                    <div className="mc-content">
                                                        <div className="img-container">
                                                            <img alt="team" className="img-responsive"
                                                             src={require('../Admin/OPERATIONS/uploads/'+content.photo)}/>
                                                        </div>
                                                        <div className="mc-description">
                                                           {content.alumni_journey}
                                                        </div>
                                                    </div>
                                                    <a className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                    <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                    </a>
                                                    <div className="mc-footer">
                                                        <h4>
                                                            Social Accounts
                                                        </h4>
                                                        <div className="center">
                                                        <a href={content.github} target="_blank" className="fa fa-fw fa-github"></a>
                                                        <a href={content.linkedin} target="_blank" className="fa fa-fw fa-linkedin"></a>
                                                        <a href={content.gmail} target="_blank" className="fa fa-fw fa-google"></a>
                                                        </div></div>
                                                </article>    
                                    </div>
                                )
                            })}
                        </React.Fragment>
                        } 
                        </div>

                    <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Team Head</h5>
                        <div class="line"></div>
                    </div>
                    <div className="row active-with-click" style={{paddingTop:'50px'}}>
                        {this.state.h_team.length === 0 ? 
                        <div className="center">No Data</div>
                        :
                        <React.Fragment>
                            {this.state.h_team.map((content,index)=>{
                                return(
                                    <div className="col l3 s12" key={index}>
                                            <article className={"material-card Red "+this.settedClass(content.serial)}>
                                                    <h2>
                                                        <span>{content.name}</span>
                                                        <strong>
                                                            {content.r_type} of {content.domain}
                                                        </strong>
                                                    </h2>
                                                    <div className="mc-content">
                                                        <div className="img-container">
                                                            <img alt="team" className="img-responsive"
                                                            src={require('../Admin/OPERATIONS/uploads/'+content.photo)}/>
                                                        </div>
                                                        <div className="mc-description">
                                                         {content.about_member}
                                                        </div>
                                                    </div>
                                                    <a className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                    <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i>
                                                    </a>
                                                    <div className="mc-footer">
                                                        <h4>
                                                            Social Accounts
                                                        </h4>
                                                        <div className="center">
                                                        <a href={content.github} target="_blank" className="fa fa-fw fa-github"></a>
                                                        <a href={content.linkedin} target="_blank" className="fa fa-fw fa-linkedin"></a>
                                                        <a href={content.gmail} target="_blank" className="fa fa-fw fa-google"></a>
                                                        </div></div>
                                                </article>    
                                    </div>
                                )
                            })}
                        </React.Fragment>
                        } 
                        </div>

                    <div style={{paddingLeft:'15px',textAlign: 'left'}}>
                        <h5>Members</h5>
                        <div class="line"></div>
                    </div>
                    <div className="row active-with-click" style={{paddingTop:'50px'}}>
                        {this.state.member.length === 0 ? 
                        <div className="center">No Data</div>
                        :
                        <React.Fragment>
                            {this.state.member.map((content,index)=>{
                                return(
                                    <div className="col l3 s12" key={index}>
                                            <article className={"material-card Red "+this.settedClass(content.serial)}>
                                                    <h2>
                                                        <span>{content.name}</span>
                                                        <strong>
                                                            {content.r_type} of {content.domain}
                                                        </strong>
                                                    </h2>
                                                    <div className="mc-content">
                                                        <div className="img-container">
                                                            <img alt="team" className="img-responsive"
                                                            src={require('../Admin/OPERATIONS/uploads/'+content.photo)}/>
                                                        </div>
                                                        <div className="mc-description">
                                                        {content.about_member}
                                                        </div>
                                                    </div>
                                                    <a className="mc-btn-action" onClick={()=>this.setSpin(content.serial)}>
                                                      <i className={"fa small "+this.moveIcon(content.serial)} aria-hidden="true"></i> 
                                                    </a>
                                                    <div className="mc-footer">
                                                        <h4>
                                                            Social Accounts
                                                        </h4>
                                                        <div className="center">
                                                        <a href={content.github} target="_blank" className="fa fa-fw fa-github"></a>
                                                        <a href={content.linkedin} target="_blank" className="fa fa-fw fa-linkedin"></a>
                                                        <a href={content.gmail} target="_blank" className="fa fa-fw fa-google"></a>
                                                        </div></div>
                                                </article>    
                                    </div>
                                )
                            })}
                        </React.Fragment>
                        } 
                        </div>
</section>
</React.Fragment>
        )
                    }
                    
    }
}
