import React, { Component } from 'react'
import M from 'materialize-css';
import axios from 'axios';

import S1 from './alind.jpg'
import S2 from './anekonnect.png'
import S3 from './blue.png'
import S4 from './ids.png'
import S5 from './navicom.png'
import S6 from './niot.jpg'
import S7 from './nvidia.png'
import S8 from './pcbpower.png'
import S9 from './rje1.png'
import S10 from './siic.jpg'
import S11 from './spartan.png'
import S12 from './solidworks.png'

export default class Sponsor extends Component {
    constructor()
    {
        super()
        this.state={
            sponsors:[],
            loading:true,
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }
    componentDidMount(){
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.carousel');
            M.Carousel.init(elems, {
                duration: 100,
                indicators: true,
                shift:1,
            });
          });
          this.fetchSponsors();
    }
    fetchSponsors=()=>{
        axios.post('/ework/user/fetch_sponsor')
          .then(response =>{
              console.log(response.data)
            this.setState({
             sponsors: response.data,
             loading:false,
           })
          })
          .catch(err=>{
            window.M.Toast({html: 'Something Went Wrong !!',classes:'red rounded'})
          })
    }
    render() {
        console.log(this.state.sponsors)
        if(this.state.loading)
        {
            return(
                <div className="center">Loading...</div>
            )
        }
        else{
            if(this.state.sponsors.length===0)
            {
                return(
                    <div className="center">No Data</div>
                )
            }
            else{
                return (
                    <React.Fragment>
                    <h1 className="center tag-head fade"> <span className="team-header1">S</span>ponsorship <span className="team-header2">D</span>etails</h1><br/>
                            <div className="row s-cont fade">
                                {this.state.sponsors.map((content,index)=>{
                                    return(
                                    <div className="col l3 m3 center" key={index}>
                                        <div className="box">
                                            <img src={require('../Admin/OPERATIONS/uploads/'+content.photo)}
                                             style={{width:'100%',height:'100%'}} title={content.name} alt=""/>
                                        </div>
                                    </div>
                                    )
                                })}   
                            </div>
                    </React.Fragment>
    
                )
            }
        }

    }
}
