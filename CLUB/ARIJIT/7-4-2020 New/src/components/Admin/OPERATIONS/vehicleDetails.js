import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import M from 'materialize-css';
import AddProduct from './CRUD/add';
import ProductList from './CRUD/show';



export default class Section extends Component {
    constructor()
    {
        super()
        this.state={type:''}
        this.componentDidMount  = this.componentDidMount.bind(this);
    }
    componentDidMount()
    {
        M.AutoInit();
    }
    setType=(e)=>{
        this.setState({type:e.target.value})
    }
    render()
    {
        return(
              <React.Fragment>
                  <div className="notinoti">
                    <div class="btn-floating btn-small red" style={{margin:'5px'}}>
                        <i className="material-icons right" onClick={this.props.close}>close</i>
                    </div>
                      <h5 className="center">Vehicle Name - {this.props.vehicle_name}</h5>
                            <div className="row">
                                <div style={{marginLeft:'10px'}} className="input-field col l3">
                                    <select value={this.state.type} onChange={this.setType}>
                                        <option value="" disabled defaultValue>Choose your option</option>
                                        <option value="Vehicle Dimensions">Vehicle Dimensions</option>
                                        <option value="Mass of vehicle">Mass of vehicle</option>
                                        <option value="Top Speed">Top Speed</option>
                                        <option value="Degrees of Freedom">Degrees of Freedom</option>
                                        <option value="Propulsion System">Propulsion System</option>
                                        <option value="Single Board Computer Model">Single Board Computer Models</option>
                                        <option value="Camera Sensors">Camera Sensors</option>
                                        <option value="Software Architecture">Software Architecture</option>
                                        <option value="Power System">Power System</option>
                                    </select>
                                </div>
                             </div>
                            <div className="row" style={{margin:'5px'}}>
                              {this.state.type && <AddDetails type={this.state.type}  vehicle_name={this.props.vehicle_name}/>}
                            </div>
                            
                            
                  </div>
              </React.Fragment>
        )
    }
}

class AddDetails extends Component {
    constructor(props)
    {
      super(props)
      this.state ={
        isAddProduct: false,
        response: {},
        product: {},
        isEditProduct: false,
        loading:false,
        uploading:false,
        loaded_percent:0,
        username:'',
        redirectTo:'',
        option:'',
        file:'',
      }
      this.componentDidMount = this.componentDidMount.bind(this)
       this.onFormSubmit = this.onFormSubmit.bind(this);
    }
  componentDidMount(){
    M.AutoInit();
  }
     onCreate = (e,index) => {
  
       this.setState({ isAddProduct: true ,product: {}});
     }
     onFormSubmit(data) {
         //console.log(data)
       let apiUrl;
       if(this.state.isEditProduct){
         apiUrl = '/ework/user/edit_vehicle_details';
       } else {
         apiUrl = '/ework/user/add_details_of_vehicle';
       }
       axios.post(apiUrl,{data})
           .then(response => {
             this.setState({
               response: response.data,
               isAddProduct: false,
               isEditProduct: false
             })
           })
      
     }
  
     editProduct = (productId,index)=> {
       axios.post('/ework/user/fetch_to_edit_vehicle_details',{
         id: productId,
         username: this.props.username,
       })
           .then(response => {
             this.setState({
               product: response.data,
               isEditProduct: true,
               isAddProduct: true,
             });
           })
  
    }
    updateState = () =>{
      this.setState({
        isAddProduct:false,
        isEditProduct:false,
      })
    }
  
    render()
    {
      let productForm;
      var data = {
        fielddata: [
          {
            header: "Data",
            name: "data",
            placeholder: "Put the Info",
            type: "text",
            size:true,
          },
        ],
      };
      if(this.state.isAddProduct || this.state.isEditProduct) {
      productForm = <AddProduct type={this.props.type} cancel={this.updateState} username={this.state.username}
       action={this.props.vehicle_name} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
      }
  
      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {
      if(this.state.loading ===  true)
      {
        return(
           <div className="center">Loading...</div>
        );
      }
      else{
                  return(
                  <React.Fragment>
                      <br />
                      <div className="sponsor">
                          <div>
                          {!this.state.isAddProduct && 
                              <ProductList image={false} username={this.props.username} title={this.state.option}  
                              action={this.props.vehicle_name} type={this.props.type} data={data}  editProduct={this.editProduct}/>
                          }<br />
                          {!this.state.isAddProduct &&
                          <React.Fragment>
                          <div className="row">
                              <div className="col l6 s12 xl6 m12 left" />
                              <div className="col l6 s6 xl6 m6">
                                      <button className="btn right blue-grey darken-2 sup subm" 
                                      onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</button>
                                  </div>
                              </div>
                          </React.Fragment>
                          }
                          { productForm }
                          <br/>
                          </div>
                      </div>
                  </React.Fragment>
                  )
          }
  }
    }
}


