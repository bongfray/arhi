import React, { Component } from 'react';
import Axios from 'axios';
import ShowDetails from './zarna';

export default class Vehicle extends Component {
    constructor()
    {
        super()
        this.state={
            newone:'',
            showDetails:false,
            vehicle:'',
            loading:true,
            visible: 1,
            start:0,
            restore_start:0,
            restore_next:0,
        }
        this.componentDidMount  = this.componentDidMount.bind(this)
    }

    
    loadMore=()=> {
        this.setState((prev) => {
            return {restore_start:prev.start,restore_next:prev.visible,start:prev.visible,
                visible: prev.visible + 1};
          });
  }
  loadPrev=()=> {
    this.setState((prev) => {
        return {start:this.state.restore_start,visible: this.state.restore_next};
      });
  }
    componentDidMount()
    {
        Axios.post('/ework/user/fetchRecent',{action:'Vehicle'})
        .then(res=>{
            console.log(res.data)
            this.setState({newone:res.data,loading:false})
            
        })
        .catch(err=>{

        })
    }
    render() {
        if(this.state.loading)
        {
            return(
                <div className="center">Loading...</div>
            )
        }
        else{
                return (
                    <React.Fragment>
                        {this.state.showDetails && 
                         <div className="notinoti">
                            <div class="btn-floating btn-small red" style={{margin:'5px'}}>
                              <i className="material-icons right" onClick={()=>this.setState({showDetails:false})}>close</i>
                            </div>
                            <ShowDetails vehicle={this.state.vehicle} />
                         </div>
                        }
                        <div className="vehicle">
                            {this.state.newone.length>0 ? 
                              <React.Fragment>
                                  {this.state.newone.slice(this.state.start, this.state.visible).map((content,index)=>{
                                      return(
                                          <React.Fragment key={index}>
                                              <div className="row">
                                                  <div className="col l1 s2 xl1 m1">
                                                    {this.state.start>0 &&
                                                        <button onClick={this.loadPrev} style={{marginTop:'350%'}} type="button" 
                                                        className=" btn btn-floating btn-large #69f0ae green accent-2">
                                                        <i className="material-icons large black-text">chevron_left</i>
                                                        </button>
                                                    } 
                                                  </div>
                                                  <div className="col l10 xl10 s8 m10">
                                                <div className="row" >
                                                    <div className="col l3">
                                                    </div>
                                                    <div className="col s12 l6">
                                                    <div className="floating center fade_in_up">
                                                        <span className="zarna-name">{content.name}</span><br/>
                                                    <p className="float fade"><span className="team-header4">A</span>utonomous <span className="team-header1">U</span>nderwater <span className="team-header3">V</span>ehicle 
                                                    </p>
                                                    </div>
                                                    </div>
                                                    <div className="col l3"></div>
                                                </div>
                                                <div className="row">
                                                    <div className="col l4"></div>
                                                    <div className="col l4 s12 center">
                                                    <div onClick={()=>this.setState({showDetails:true,vehicle: content.name})} className="go v-find fade">
                                                        Find out more about this AUV here
                                                    </div>
                                                    </div>
                                                    <div className="col l4">
                                                    </div>
                                                </div>
                                             </div>
                                             <div className="col l1 xl1 s2 m1">
                                                        {this.state.visible < this.state.newone.length &&
                                                            <button onClick={(this.loadMore)} type="button"  style={{marginTop:'350%'}}
                                                            className="btn btn-floating btn-large #69f0ae green accent-2">
                                                            <i className="material-icons large black-text">chevron_right</i>
                                                            </button>
                                                        }
                                             </div>
                                            </div>
                                          </React.Fragment>
                                      )
                                  })}
                              </React.Fragment>
                             :
                             <React.Fragment>
                            <div className="row">
                                <div className="col l3"></div>
                                <div className="col s12 l6">
                                <div className="floating center">
                                    <span className="zarna-name">{'ZARNA'}</span><br/>
                                <p className="float fade"><span className="team-header4">A</span>utonomous <span className="team-header1">U</span>nderwater <span className="team-header3">V</span>ehicle 
                                </p>
                                </div>
                                </div>
                                <div className="col l3"></div>
                            </div>
                                <div className="row">
                                    <div className="col l4"></div>
                                    <div className="col l4 s12 center">
                                    <div onClick={()=>this.setState({showDetails:true,vehicle:'ZARNA'})} className="go v-find fade">
                                         Find out more about our latest AUV here <span className="fa fa-arrow-right small"></span>
                                    </div>
                                    </div>
                                    <div className="col l4"></div>
                                </div>
                             </React.Fragment>
                            }
                                
                            </div>
                            
                        
                    </React.Fragment>
                        
                )
      }
    }
}
