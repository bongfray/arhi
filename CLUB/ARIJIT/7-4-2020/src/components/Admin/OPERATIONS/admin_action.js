import React, { Component } from 'react'
import Axios from 'axios';
import M from 'materialize-css'; 



export default class SSSS extends Component {
    constructor()
    {
        super()
        this.state={
            status:false,
            loading:true,
            sponsor:0,
            team:0,
            alumni:0,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount()
    {
        this.fetchStat();
        this.fetchSponsor();
    }
    fetchStat=()=>{
        Axios.post('/ework/user/admin_action',{action:'signup_stat'})
        .then(res=>{
            if(!(res.data==='no'))
            {
                this.setState({status:res.data.status})
            }
        })
    }
    setSignup=(e)=>{
        this.setState({action:e.target.value,status:!this.state.status})
        Axios.post('/ework/user/setSignup',{action:e.target.value,status:!this.state.status})
        .then(res=>{
            
        })
    }

    fetchSponsor=()=>{
        Axios.post('/ework/user/fetch_sponsor',{action:'Sponsors'})
        .then(res=>{
            this.setState({sponsor:res.data.length})
            this.fetchTeam();
        })
        .catch(err=>{
            this.setState({loading:false})
            window.M.Toast({html:'Something went wrong !!'})
        })
        
    }
    fetchTeam=()=>{
        Axios.post('/ework/user/fetch_team',{action:'Team Member'})
        .then(res=>{
            this.setState({team:res.data.length})
            this.fetchAlumni();
        })
        .catch(err=>{
            this.setState({loading:false})
            window.M.Toast({html:'Something went wrong !!'})
        })
        
    }
    fetchAlumni=()=>{
        Axios.post('/ework/user/fetch_team',{action:'Alumni Details'})
        .then(res=>{
            this.setState({alumni:res.data.length})
        })
        .catch(err=>{
            window.M.Toast({html:'Something went wrong !!'})
        })
        this.setState({loading:false})
    }

    render()
    {
        if(this.state.loading)
        {
            return(
                <div className="center">
                      <div className="preloader-wrapper active">
                        <div className="spinner-layer spinner-red-only">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div><div className="gap-patch">
                            <div className="circle"></div>
                        </div><div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                        </div>
                    </div>
                </div>
            )
        }
        else{
        return(
            <React.Fragment>
                <div className="row">
                    <div className="col l4 card">
                        Render Signup Page - 
                            <p>
                                <label>
                                    <input checked={this.state.status} type="checkbox" value="signup_stat" 
                                    onChange={this.setSignup}/>
                                    <span>Status</span>
                                </label>
                            </p>
                    </div>
                </div>
                <div className="row"><h5 className="">Details of the System</h5></div>
                <div className="row">
                    <div className="col l4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Sponsors</span>
                                <h4 className="center">{this.state.sponsor}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col l4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Team Members</span>
                                <h4 className="center">{this.state.team}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col l4">
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title center">Total Alumnis</span>
                                <h4 className="center">{this.state.alumni}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
      }
    }
}