import React, { Component } from 'react'
import Logo from '../Home/auv.png'

export default class Footer extends Component {
    render() {
        return (
            <React.Fragment>
           
                <div class="footer" style={{display:'none'}}>
                    <div id="button"></div>
                    <div id="container">
                        <div className="row center">
                            <div className="col l4 s4 m4">
                                <div className="row">
                                    <div className="l12 m12 s12">
                                        <img src={Logo} alt="" className="footer-logo"/>
                                    </div>
                                    <div className="col l12 s12 m12">
                                        <h3 className="footer-lname">SRM AUV</h3>
                                        <p className="footer-lp">SRM IST's Official AUV Team</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col l4 s4 m4 footer-list">
                                <h5 className="center footer-h4">Explore</h5>
                                <ul class="wrap-nav">
                                <li><a href="#">Home</a></li>
                                <li><a href="#vehicle">Vehicle</a></li>
                                <li><a href="#team">Team</a></li>
                                <li><a href="#sponsor">Sponsors</a></li>
                                </ul>
                            </div>

                            <div className="col s4 m4">
                            <h5 className="center footer-h4">Follow Us</h5>
                                
                            <div class="rounded-social-buttons">
                                <a class="social-button facebook" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                <a class="social-button twitter" href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a class="social-button linkedin" href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                <a class="social-button youtube" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
                                <a class="social-button instagram" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                            </div>
                            <hr/>
                            <p className="copyright">© Made with <i class="fa fa-heart"></i> by ArHi &#128526; 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
