import React, { Component } from 'react'
import Slider from 'infinite-react-carousel';


import Mech from './mech.jpg'
import Soft from './soft.jpg'
import Elect from './elect.jpg'
import Corp from './corp.jpg'

export default class Domain extends Component {
    render() {
        const settings =  {
            arrows: false,
            arrowsBlock: false,
            // autoplay: true,
            dots: true
          };
        return (
            
            <React.Fragment>
                <div className="domain">
                <h1 className="center tag-head dom-cont fade"> <span className="team-header1">D</span>omains <span className="team-header2">I</span>n <span className="team-header3">SRM</span> <span className="team-header4">AUV</span> </h1><br/>

                <div className="row">
                    <div className="col s1"></div>
                    <div class="col s10 card product fade">
                        <div class="ribbon-wrapper">
                            <div class="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">MECHANICAL</h4>
                            <p className="d-content">•Responsible for designing frame work & designing AUV. 
                                        <br/>•Designing a neutrally or positively buoyant & leak proof vehicle.
                                        <br/>•Designing and fabrication battery pod, camera pod & casing for various electronic components.
                                        <br/>•Done using Solid Works, Catia & Ansys.
                                    </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                <div className="row">
                    <div className="col s1"></div>
                    <div class="col s10 card product fade">
                        <div class="ribbon-wrapper">
                            <div class="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">Software</h4>
                            <p className="d-content">•Software domain ensures movement of vehicle through image processing.
                                <br/>•Designing algorithms to give input to on board computer JETSON TX1.
                                <br/>•The software also look upon vehicles navigation and sensor fusion.
                                <br/>•Web Development & Robotics are being considered software domain. 
                            </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                <div className="row">
                    <div className="col s1"></div>
                    <div class="col s10 card product fade">
                        <div class="ribbon-wrapper">
                            <div class="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">Electrical</h4>
                            <p className="d-content">•The electrical and electronic components of the vehicle are incorporated safely on backplane within the hull of the vehicle.
                                <br/>•Electrtical and Electronics  infrastructure consists of power management system of the vehicle, Acoustic signal processing, and various payload electronics.
                            </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                <div className="row">
                    <div className="col s1"></div>
                    <div class="col s10 card product fade">
                        <div class="ribbon-wrapper">
                            <div class="ribbon">DOMAIN</div>
                        </div>
                        <div>
                            <h4 className="d-head center">Corporate</h4>
                            <p className="d-content">•Non technical domain of SRM AUV, Corporate domain forms a critical aspect of the operations of the team.
                                <br/>•The challenges range from finance, availability of the tech, sponsorship, public relationship and social media awareness.
                                <br/>•Also known as the face of the company in colloquial terms.
                            </p>
                            </div>
                    </div>
                    <div className="col s1"></div>
                </div>

                

                

                <div class="containerr fade">
                        
                            
                        <div class="box fade">
                            <div class="imgBx">
                                <img src={Mech} />
                            </div>
                            <div class="content">
                            <h2 className="dim-h4">Mechanical</h2>
                            <p className="content-p">•Responsible for designing frame work & designing AUV. 
                                <br/>•Designing a neutrally or positively buoyant & leak proof vehicle.
                                <br/>•Designing and fabrication battery pod, camera pod & casing for various electronic components.
                                <br/>•Done using Solid Works, Catia & Ansys.
                            </p>
                            </div>
                        </div>
                    

                    
                        <div class="box fade">
                            <div class="imgBx">
                                <img src={Soft} />
                            </div>
                            <div class="content">
                            <h2 className="dim-h4">Software</h2>
                            <p className="content-p">•Software domain ensures movement of vehicle through image processing.
                                <br/>•Designing algorithms to give input to on board computer JETSON TX1.
                                <br/>•The software also look upon vehicles navigation and sensor fusion.
                                <br/>•Web Development & Robotics are being considered software domain. 

                            </p>
                            </div>
                        </div>
                    

                    
                        <div class="box fade">
                            <div class="imgBx">
                                <img src={Elect} />
                            </div>
                            <div class="content">
                            <h2 className="dim-h4">Electrical</h2>
                            <p className="content-p">•The electrical and electronic components of the vehicle are incorporated safely on backplane within the hull of the vehicle.
                                <br/>•Electrtical and Electronics  infrastructure consists of power management system of the vehicle, Acoustic signal processing, and various payload electronics.
                            </p>
                            </div>
                        </div>
                    

                    
                        <div class="box fade">
                            <div class="imgBx">
                                <img src={Corp} />
                            </div>
                            <div class="content">
                            <h2 className="dim-h4">Corporate</h2>
                            <p className="content-p">•Non technical domain of SRM AUV, Corporate domain forms a critical aspect of the operations of the team.
                                <br/>•The challenges range from finance, availability of the tech, sponsorship, public relationship and social media awareness.
                                <br/>•Also known as the face of the company in colloquial terms.
                                

                            </p>
                            </div>
                        </div>
                    
                </div>

                </div>
                                        
            </React.Fragment>

            )
    }
}
