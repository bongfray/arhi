import React, { Component } from 'react'
import $ from 'jquery'

export default class ContactUs extends Component {
    componentDidMount(){
        $(document).ready(function(){
            //button click event
            $("#btn").click(function(){
              // pushed form down
              $(".form").css("margin-top", "6px");
              // folds down envelope top
               $(".top").css({
                "transform": "rotatex(0deg)",
                "z-index": "3"
              });
              // rotates envelope
              $(".envelope").css("transform", "rotatey(180deg)");
              // stores first name text
              var ipt = document.getElementById('first').value;
              // enters text in to div
              $( "#name" ).html( ipt );
              // adds box shadow to front of envelope
              $(".envelope__bottom--front").css("box-shadow", "0 0 30px black");
            });
          });
    }
    render() {
        return (
            <React.Fragment>

            <div class="row loading2 wave2 fade">
                Contact Us
            </div>

            <div className="row mob-c">
                <div className="col s12 fade">
                        <ul class="social-media-list">
                            
                                <li><a href="#" target="_blank" class="contact-icon">
                                <i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>
                            
                            
                                <li><a href="#" target="_blank" class="contact-icon">
                                <i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                            
                            
                                <li><a href="#" target="_blank" class="contact-icon">
                                <i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                            
                            
                                <li><a href="#" target="_blank" class="contact-icon">
                                <i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </li>
                            
                            
                                <li><a href="#" target="_blank" class="contact-icon">
                                <i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                            
                        </ul>
                        
                </div>
                
                <div className="col s12 fade">
                    <form class="letter fade">
                                <div className="input-field col s12">
                                    <input id="mname" type="text" className="validate" required />
                                    <label htmlFor="mname" className="label-c">Name</label>
                                </div>
                                <div className="input-field col s12">
                                    <input id="memail" type="text" className="validate" required />
                                    <label htmlFor="memail" className="label-c">E-Mail</label>
                                </div>
                                <div className="input-field col s12">
                                    <textarea id="mmessage" cols="60" class="materialize-textarea txt-c"></textarea>
                                    <label for="mmessage" className="label-c">Message</label>    
                                </div>
                                

                                <input type="submit" className="send-c" value="Send"/>
                    </form>
                </div>

                <div className="col s1"></div>
                <div className="col s10 fade">
                    
                <ul class="contact-list">
                        <li class="list-item"><i class="fa fa-map-marker fa-2x"><span class="contact-text place">SRM IST</span></i></li>
                        
                        <li class="list-item"><i class="fa fa-phone fa-2x"><span class="contact-text phone"><a href="tel:1-212-555-5555" title="Give me a call">9988998899</a></span></i></li>
                        
                        <li class="list-item"><i class="fa fa-envelope fa-2x"><span class="contact-text gmail"><a href="mailto:#" title="Send me an email">me@gmail.com</a></span></i></li>
                    </ul>
                </div>
                <div className="col s1"></div>
            </div>
            
            <div className="row desk-c">
                <div className="col l3 fade">
                    <ul class="social-media-list fade">
                        <div className="col l12">
                            <li><a href="#" target="_blank" class="contact-icon">
                            <i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                        </div>
                        <div className="col l12">
                            <li><a href="#" target="_blank" class="contact-icon">
                            <i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </li>
                        </div>
                        <div className="col l12">
                            <li><a href="#" target="_blank" class="contact-icon">
                            <i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                        </div>
                        <div className="col l12">
                            <li><a href="#" target="_blank" class="contact-icon">
                            <i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            </li>
                        </div>
                        <div className="col l12">
                            <li><a href="#" target="_blank" class="contact-icon">
                            <i class="fa fa-youtube" aria-hidden="true"></i></a>
                            </li>
                        </div>
                    </ul>
                </div>

                <div className="col l6 fade">
                    <div class="envelope fade" title="">
                        <div class="back"></div>
                            <form class="letter">
                                <div className="input-field col s12">
                                    <input id="name" type="text" className="validate" required />
                                    <label htmlFor="name" className="label-c">Name</label>
                                </div>
                                <div className="input-field col s12">
                                    <input id="email" type="text" className="validate" required />
                                    <label htmlFor="email" className="label-c">E-Mail</label>
                                </div>
                                <div className="input-field col s12">
                                    <textarea id="message" cols="60" class="materialize-textarea txt-c"></textarea>
                                    <label for="message" className="label-c">Message</label>    
                                </div>
                               
                                <input type="submit" className="send-c" value="Send"/>
                            </form>
                    </div>
                </div>

                <div className="col l3 fade">
                    <ul class="contact-list fade">
                        <li class="list-item"><i class="fa fa-map-marker fa-2x"><span class="contact-text place">SRM IST</span></i></li>
                        
                        <li class="list-item"><i class="fa fa-phone fa-2x"><span class="contact-text phone"><a href="tel:1-212-555-5555" title="Give me a call">9988998899</a></span></i></li>
                        
                        <li class="list-item"><i class="fa fa-envelope fa-2x"><span class="contact-text gmail"><a href="mailto:#" title="Send me an email">me@gmail.com</a></span></i></li>
                    </ul>
                
                    
                </div>

                
            </div>

            </React.Fragment>
        )
    }
}
