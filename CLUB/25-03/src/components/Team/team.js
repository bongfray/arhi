import React, { Component } from 'react'
import $ from 'jquery'

export default class Team extends Component {
    componentDidMount(){
        $("header").append("<div class='glitch-window'></div>");
//fill div with clone of real header
$( "h1.glitched" ).clone().appendTo( ".glitch-window" );
    }
    render() {
        return (
            <React.Fragment>
                <div className="team">

                    
                    <header class="header row fade">
                        <h1 class="glitchede fade">MEET THE TEAM</h1>
                    </header>

                    {/* <div className="row">
                        <h4 className="center glitchede">MEET THE TEAM</h4>
                    </div> */}

                    <div className="row te-link hide-on-small-only">
                        <div className="col s1 l4"></div>
                        <div className="col s10 l4 center fade"><a href="/teamdetails" className="fade t-link">Give a peek behind the curtains <span className="fa fa-arrow-right small"></span> </a></div>
                    </div>
                    <div className="row hide-on-med-and-up fade">
                        <div className="col l3"></div>
                        <div className="col s12 l6">
                        <div class="floating-team center fade">
                            <span className="fade">MEET THE TEAM</span><br/>
                        </div>
                        </div>
                        <div className="col l3"></div>
                    </div>
                        <div className="row hide-on-med-and-up">
                            <div className="col l4"></div>
                            <div className="col l4 s12 center fade">
                            <a href="/teamdetails" className="t-link fade"> Give a peek behind the curtains <span className="fa fa-arrow-right small"></span></a>
                            </div>
                            <div className="col l4"></div>
                        </div>

                
                </div>
            </React.Fragment>
            
        )
    }
}
