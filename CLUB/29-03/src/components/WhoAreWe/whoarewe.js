import React, { Component } from 'react'

export default class Whoarewe extends Component {
    componentDidMount(){
        var TxtRotate = function(el, toRotate, period) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 2000;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
          };
          
          TxtRotate.prototype.tick = function() {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];
          
            if (this.isDeleting) {
              this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
              this.txt = fullTxt.substring(0, this.txt.length + 1);
            }
          
            this.el.innerHTML = '<span className="wrap">'+this.txt+'</span>';
          
            var that = this;
            var delta = 300 - Math.random() * 100;
          
            if (this.isDeleting) { delta /= 2; }
          
            if (!this.isDeleting && this.txt === fullTxt) {
              delta = this.period;
              this.isDeleting = true;
            } else if (this.isDeleting && this.txt === '') {
              this.isDeleting = false;
              this.loopNum++;
              delta = 500;
            }
          
            setTimeout(function() {
              that.tick();
            }, delta);
          };
          
          window.onload = function() {
            var elements = document.getElementsByClassName('txt-rotate');
            for (var i=0; i<elements.length; i++) {
              var toRotate = elements[i].getAttribute('data-rotate');
              var period = elements[i].getAttribute('data-period');
              if (toRotate) {
                new TxtRotate(elements[i], JSON.parse(toRotate), period);
              }
            }
            // INJECT CSS
            var css = document.createElement("style");
            css.type = "text/css";
            css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
            document.body.appendChild(css);
          };
    }
    render() {
        return (
            <React.Fragment>

                <div className="whoarewe">
                    <div className="row w-h" >
                        <h1 className="center tag-head white-text"> <span className="team-header1">W</span>ho <span className="team-header2">A</span>re <span className="team-header1">W</span>e?</h1><br/>
                    </div>

                    <div className="row">
                        <div className="col l1" />
                        <div className="col s12 l10 mob-wp white-text">
                            <p className="w-p"><span className="team-header4">SRM</span> <span className="team-header3">AUV</span> is an undergraduate student run team of SRMIST, Kattankulatur founded in the year 2013. We  the team of underwater robotics enthusiasts share our common interest in designing of AUVs(Autonomous Underwater Vehicles). Our team comprises of members of several domains starting from Computer Science,  Electrical and Mechatronics. The team works around the year to publish papers on underwater robotics and prototyping our new vehicle. The team is currently working on its fifth iteration of the vehicle ZARNA 2.0.   
                            <br/>  The team participates in various national and internal competitions such as  <b><span
                                className="txt-rotate"
                                data-period="500"
                                data-rotate='[ "SAVe NIOT,", "SAUVC,", "SAUC-E,", "Robosub." ]'></span></b> <br/>The team’s internal structure is divided into five Domains:
                            </p>
                        </div>
                        <div className="col l1"/>
                    </div>

                    
        
                    <div className="row white-text w-marg-mob">
                            <div className="center col s3 mob-col"><p className="w-dom">MECHANICAL</p></div>
                            <div className="center col s3 mob-col"><p className="w-dom">SOFTWARE</p></div>
                            <div className="center col s3 mob-col"><p className="w-dom">ELECTRICAL</p></div>
                            <div className="center col s3 mob-col"><p className="w-dom">CORPORATE</p></div>
                    </div>

                </div>
            </React.Fragment>
                
                
        )
    }
}
