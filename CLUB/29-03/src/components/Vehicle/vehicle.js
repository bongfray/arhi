import React, { Component } from 'react'

export default class Vehicle extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="vehicle">
                    <div className="row">
                        <div className="col l3"></div>
                        <div className="col s12 l6">
                        <div className="floating center">
                            <span className="zarna-name">ZARNA</span><br/>
                        <p className="float fade"><span className="team-header4">A</span>utonomous <span className="team-header1">U</span>nderwater <span className="team-header3">V</span>ehicle 
                        </p>
                        </div>
                        </div>
                        <div className="col l3"></div>
                    </div>
                        <div className="row">
                            <div className="col l4"></div>
                            <div className="col l4 s12 center">
                            <a href="/zarna" className="v-find fade"> Find out more about our latest AUV here <span className="fa fa-arrow-right small"></span></a>
                            </div>
                            <div className="col l4"></div>
                        </div>
                    </div>
                    
                
            </React.Fragment>
                
        )
    }
}
