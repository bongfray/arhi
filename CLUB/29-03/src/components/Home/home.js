import React, { Component } from 'react';
import {} from 'materialize-css'
import $ from 'jquery'

import Logo from './auv.png'

// import NL from './logo.png'

import Vid from './back.mp4';

import Sponsor from '../Sponsors/sponsor'

import Whoarewe from '../WhoAreWe/whoarewe'

import Domain from '../Domains/domain'

import Team from '../Team/team'

import Achievement from '../Achievement/achievement'

import Vehicle from '../Vehicle/vehicle'

import ContactUs from '../ContactUs/contactus'

import Footer from '../Footer/footer'





export default class Home extends Component {
  componentDidMount(){

    /* FadeIn Scroll */
$(document).ready(function() {
    
  /* Every time the window is scrolled ... */
  $(window).scroll( function(){
  
      /* Check the location of each desired element */
      $('.fade').each( function(i){
          
          var bottom_of_object = $(this).position().top + $(this).outerHeight();
          var bottom_of_window = $(window).scrollTop() + $(window).height();
          
          /* If the object is completely visible in the window, fade it it */
          if( bottom_of_window > bottom_of_object ){
              
              $(this).animate({'opacity':'1'},1500);
                  
          }
          
      }); 
  
  });
  
});

    //-------top button----
    var btn = $('#top-button');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 100) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });
    
    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '100');
    });
    //-------End of top button----
    
    
  // ------------NAV--------------
  $(document).ready(function(){
    $(".button a").click(function(e){
      e.preventDefault();
        $(".overlay").fadeToggle(400);
       $(this).toggleClass('btn-open').toggleClass('btn-close');
    });
});
$('.overlay').on('click', function(){
    $(".overlay").fadeToggle(400);   
    $(".button a").toggleClass('btn-open').toggleClass('btn-close');
});


//-----------END NAV-------------
  // To display none and block on scrolling
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
      if (scroll >= 735) {
          $("#tabs").addClass("tabs-fixed z-depth-0");
          $(".tabs").addClass("#00838f cyan darken-3 tabs-transparent"); 
      }
      else {
          $("#tabs").removeClass("tabs-fixed z-depth-0");
      }

      if(scroll>=250){
        $('.type').addClass("pos_fix");
      }
      else{
        $('.type').removeClass("pos_fix");

      }
      if(scroll>=525){
        $('.arrows').addClass("nav-l");
      }
      else{
        $('.arrows').removeClass("nav-l");

      }
      if(scroll>=735){
        $('.type').removeClass("pos_fix");
        $('.logo-auv').addClass("pos_abs");
        $('.nav').addClass("nav_fix");        
        $('.nav-logo').removeClass("nav-l");

      }
      else{
        $('.logo-auv').removeClass("pos_abs");
        $('.nav').removeClass("nav_fix"); 
        $('.nav-logo').addClass("nav-l");


      }
      if(scroll>=800){
        $('.menu').addClass("nav_fix");
      }
      else{
        $('.menu').removeClass("nav_fix");

      }
      if(scroll>=900){
        $('.footer').addClass("footer_show")
      }
      else{
        $('.footer').removeClass("footer_show")
      }

      
  });

  //END SCrolling

  }
  render() {
    
    return (
      <div>
        <svg className="arrows">
							<path className="a1" d="M0 0 L30 32 L60 0"></path>
							<path className="a2" d="M0 20 L30 52 L60 20"></path>
							<path className="a3" d="M0 40 L30 72 L60 40"></path>
						</svg>
            <section className="top-video" style={{backgroundColor:'#1d2024'}}>

              <span><img src={Logo} className="logo-auv" alt="" height='50px'/></span>

              
              <span> <p className="a-head">SRM AUV</p> </span>
            
          
              <span className="type">

              <span
                                className="txt-rotate t-h"
                                data-period="300"
                                data-rotate='[ "SRMIST&apos;s Official AUV Team", "Diving in to the depths", "Exploring the Deep Blue" ]'></span>

                                
                          {/* <Typewriter
                options={{
                  strings: ["SRMIST's Official AUV Team", "Diving in to the depths", "Exploring the Deep Blue"],
                  autoStart: true,
                  loop: true,
                }}
              /> */}
              </span>

              <video
                  autoPlay
                  muted
                  loop
                  style={{
                  position: "relative",
                  width: "100%",
                  left: 0,
                  top: 0,
                  bottom: 80,
                  }}
              >
                <source src={Vid} type="video/mp4" />
              </video>


            </section>

            <section>
                    <div className="button">
                      <a className="btn-open" href="/" title="Menu"><i></i></a>
                    </div>
                    
                    <div className="overlay">
                      
                      <div className="wrap">
                        <ul className="wrap-nav">
                          <li><a href="/">Home</a></li>
                          <li><a href="/#about">About</a></li>
                          <li><a href="/#vehicle">Vehicle</a></li>
                          <li><a href="/#domain">Domains</a></li>
                          <li><a href="/#team">Team</a></li>
                          <li><a href="/#achievement">Achievements</a></li>
                          <li><a href="/#sponsor">Sponsors</a></li>
                          <li><a href="/#contact">Contact Us</a></li>
                        </ul>
                        <div className="row">
                          <div className="col s1"></div>
                          <a href="/" className="col s2 social-ic fa fa-twitter small"><i></i></a>
                          <a href="/" className="col s2 social-ic fa fa-instagram small"><i></i></a>
                          <a href="/" className="col s2 social-ic fa fa-facebook small"><i></i></a>
                          <a href="/" className="col s2 social-ic fa fa-linkedin small"><i></i></a>
                          <a href="/" className="col s2 social-ic fa fa-youtube small"><i></i></a>
                          <div className="col s1"></div>
                        </div>
                        </div>
                        <div className="mmenu">
                    <div className="label">Follow Us</div>
                    <div className="spacer"></div>
                    <a href="/"><div className="item"><span>Twitter</span></div></a>
                    <a href="/"><div className="item"><span>Instagram</span></div></a>
                    <a href="/"><div className="item"><span>Facebook</span></div></a>
                    <a href="/"><div className="item"><span>Linkedin</span></div></a>
                    <a href="/"><div className="item"><span>YouTube</span></div></a>
                  </div>
                    </div>
                                    
                 <a href="/" id="top-button"><i></i></a>
                
                <div id="about">
                  <Whoarewe/>
                </div>

                <div id="vehicle">
                  <Vehicle/>
                </div>

                <div id="domain">
                  <Domain/>
                </div>

                <div id="team">
                  <Team/>
                </div>

                <div id="achievement">
                  <Achievement/>
                </div>

                <div id="sponsor"> 
                  <Sponsor/> 
                </div>   

                <div id="contact">
                  <ContactUs/>
                </div>

                <div id="footer">
                  <Footer/>
                </div>

            </section>
</div>
    );
  }
}