const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Define collection and schema for Business
let Business = new Schema({
  email: {
    type: String
  },
  name: {
    type: String
  },
  faculty_id: {
    type: String
  },
  password: {
    type: String
  },
  cpassword: {
    type: String
  },
  campus: {
    type: String
  },
  designation: {
    type: String
  }
},{
    collection: 'login'
});
module.exports = mongoose.model('Business', Business);
