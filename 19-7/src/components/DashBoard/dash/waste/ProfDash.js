import React, { Component } from 'react';
import Sunburst from './global-data/Sunburst';
import data from './global-data/data';

class ProfDash extends Component{
    render(){
        return(
            <div>

            <Sunburst data={data}
            width="700"
            height="700"
            count_member="size"
            labelFunc={(node)=>node.data.name}
            _debug={true} />
            </div>
        )
    }
}

export default ProfDash;
