import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'


 class CheckDisp extends Component{
   render(){
     if(this.props.check === true)
     {
       return(
         <div>
         <input />
         </div>
       );
     }
     else
     {
       return(
         <div>

         </div>
       );
     }
   }
 }


class Check extends React.Component{
  constructor() {
    super();
    this.state = {isChecked: false};
    this.handleChecked = this.handleChecked.bind(this);
  }

  handleChecked () {
    this.setState({isChecked: true});
  }

  render(){
    return <div>
            <p>
              <label>
                <input type="checkbox" defaultChecked={this.state.checked} onChange={ this.handleChecked } />
                <span style={{color:'red'}}><b>Problem With  Class Completion</b></span>
              </label>
           </p>
       <p><CheckDisp check={this.state.isChecked} /></p>
    </div>
  }
}




class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <div className="curr_date">{new Date().toDateString()}</div>
      <p className="curr_time">{this.state.time}</p>
      </div>
    );
  }
}

class TableDisplay extends Component {
  render() {

                if (this.props.selectValue === "allot") {
                  return(
                    <div className="">
                    <h5 className="center">B2</h5>
                    <span style={{color:'red'}}>Reminder: </span><span>This slot is booked at the beginig of Semester.You are not allowed to edit it.</span>
                    <p>
                      <label>
                        <input type="checkbox" />
                        <span style={{color:'green'}}><b>Class Completed</b></span>
                      </label>
                   </p>
                   <Check />
                   <div className="right btn-of-submit-at-time"><a className="btn" href="#">SUBMIT</a></div>

                    </div>
                  );

                } else if (this.props.selectValue === "free") {
                  return(
                    <div><h6 className="free-head">Free Slot is for different Works. Kindly fill the Details..</h6><br />
                    <span className="quali">Plaese Mention on which area you are going to work :</span><br />
                              <div className="input-field inline">
                                <input id="email_inline" type="email" required/>
                                <label for="email_inline">Fill it correctly</label>
                              </div>
                    <label className="pure-material-textfield-outlined alignfull">
                      <textarea
                        className="area"
                        type="text"
                        placeholder=" "
                        min="10"
                        max="60"
                      />
                      <span>Detail About Your work</span>
                    </label>
                    <div className="right btn-of-submit-at-time"><a className="btn" href="#">SUBMIT</a></div>
                    </div>
                  );

                }
                else if (this.props.selectValue === "slot_cancel") {
                 return(
                   <div>
                   <label className="pure-material-textfield-outlined alignfull">
                     <textarea
                       className="area"
                       type="text"
                       placeholder=" "
                       min="10"
                       max="60"
                     />
                     <span>Enter the reason</span>
                   </label><br />
                   <p className="need">
                    <label>
                      <input type="checkbox" />
                      <span style={{color:'black'}}>Would you like to Get a extra slot in this week ? </span>
                    </label>
                  </p>
                  <div className="right btn-of-submit-at-time"><a className="btn" href="#">SUBMIT</a>
                  </div>
                   </div>
                 );

               }
                else{
                  return(
                    <div className="inter-drop center">
                      Choose from above DropDown
                    </div>
                  );
                }
  }
}
class Head extends Component{
  render(){
    return(
      <div className="time_head">
      <div className="row">
      <div className="col l2">
          <span className="col s2">
          <b className="center day-order-day">DETAILS</b>
          </span>
          </div>
          <div className="col l10">
          <span className="col s4">
          <div className="center day-order-day">8:00 - 8:50 A.M.</div>
          </span>
          <span className="col s4">
          <div className="center day-order-day">8:50 - 9:40 A.M.</div>
          </span>
          <span className="col s4">
          <div className="center day-order-day">9:45 - 10:25 A.M.</div>
          </span>
          </div>
          </div>
      </div>
    );

  }
}
class Head2 extends Component{
  render(){
    return(
      <div className="row time_head">
          <div className="col l12">
          <span className="col s4">
          <div className="center day-order-day">8:00 - 8:50 A.M.</div>
          </span>
          <span className="col s4">
          <div className=" center day-order-day">8:50 - 9:40 A.M.</div>
          </span>
          <span className="col s4">
          <div className=" center day-order-day">9:45 - 10:25 A.M.</div>
          </span>
          </div>
      </div>
    );

  }
}
class Head3 extends Component{
  render(){
    return(
      <div className="row time_head">
          <div className="col l12">
          <span className="col s4">
          <div className="center day-order-day">8:00 - 8:50 A.M.</div>
          </span>
          <span className="col s4">
          <div className=" center day-order-day">8:50 - 9:40 A.M.</div>
          </span>
          <span className="col s4">
          <div className=" center day-order-day">9:45 - 10:25 A.M.</div>
          </span>
          </div>
      </div>
    );

  }
}


class Day_Order extends Component{

  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    var sec = this.getTimeString();
    this.state = {
      day_order:'1',
      date: sec
    }
  }
  getTimeString() {
    var dat = new Date().getMinutes();
    return dat;
  }

  getNext(next){
    if((this.state.date)===next){
      this.setState({
        day_order: '2'
      });
    }
  }

  componentDidMount() {
    const nextDay = (new Date().getMinutes())+1;
    const _this = this;
    this.timer = setInterval(function(){
      var dat = _this.getTimeString();
      _this.getNext(nextDay);
      _this.setState({
        date: dat
      })
    },1000);

  }

  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.state.day_order}</b></div>
    );
  }
}



export default class Simple extends Component{
  constructor() {
    super();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      opendir: ''
    }
  }
  componentDidMount(){
    M.AutoInit();
  }


  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };
  render(){
    // if(this.props.loggedIn)
    // {
    return(
      <div className="time-div">
      <Head />
      <div className="row">
      <div className="col l2">
        <div className="date_pick">
          <Clock />
        </div>
        <br />
      <div className="dynamic_dayorder">
          <Day_Order />
      </div>
        <br />
      <div className="pick_curent">
            <select value={this.state.opendir}  onChange={this.handledir}>
              <option value="" disabled selected>Choose</option>
              <option value="r_class">Regular Class</option>
              <option value="own_ab">Absent</option>
              <option value="class_cancel">Class Cancelled</option>
              <option value="college_cancel">College Cancelled</option>
            </select>
        </div>
    </div>
    <div className="col l10">
      <Content opendir={this.state.opendir}/>
      <Head2 />
      <Content opendir={this.state.opendir}/>
      <Head3 />
      <Content opendir={this.state.opendir}/>
    </div>
    </div>
      </div>
    );
  // }
  //   else{
  //     return(
  //       <div className="error-div">
  //        <div className="row">
  //        <div className="col s4">
  //        <i className="large material-icons right error-icon">warning</i>
  //        </div>
  //        <div className="col s6">
  //        <h3 className="left error-msg">Sorry !! You are not logged in</h3>
  //        </div>
  //        <div className="col s2">
  //        </div>
  //        </div>
  //        <div className="row">
  //        <h5 className="reminder">Please Log In to Access this Site</h5>
  //        </div>
  //       </div>
  //     );
  //
  //   }
  }
}


class Content extends Component{
  render(){
    if(this.props.opendir==="r_class")
    {
      return(
        <div>
         <TimeSlot />
        </div>
      );
    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Kindly Provide Valid Reason for Your Absence</span>
        </label>
        </div>
      );
    }
    else if(this.props.opendir === "class_cancel")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Kindly Provide Valid Reason of Class Cancellation</span>
        </label>
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select Something from the DropDown</div>
      );
    }

  }
}

 class TimeSlot extends React.Component {
  constructor() {
    super();
    this.state = {
      selectValue1: '',
      selectValue2: '',
      selectValue3: '',
    };
    this.componentDidMount= this.componentDidMount.bind(this);
  }
  componentDidMount(){
    M.AutoInit();
  }


  handle1 = (e) => {
          this.setState({
              selectValue1: e.target.value
          });
      };
      handle2 = (e) => {
              this.setState({
                  selectValue2: e.target.value
              });
          };
          handle3 = (e) => {
                  this.setState({
                      selectValue3: e.target.value
                  });
              };


  render() {
    return (
            <div className="root-of-time">
                <div className="col l4 card hoverable ">
                  <div>
                    <select value={this.state.selectValue1}  onChange={this.handle1}>
                        <option value="" disabled selected>Choose</option>
                        <option value="allot">Alloted Class</option>
                        <option value="free">Free Slot</option>
                        <option value="slot_cancel">Slot Cancelled</option>
                    </select>
                  </div>
                  <span className="drop">
                    <TableDisplay
                      selectValue={this.state.selectValue1}
                    />
                  </span>
                </div>
                <div className="col l4 card hoverable ">
                <div>
                  <select value={this.state.selectValue2}  onChange={this.handle2}>
                      <option value="" disabled selected>Choose</option>
                      <option value="allot">Alloted Class</option>
                      <option value="free">Free Slot</option>
                      <option value="slot_cancel">Slot Cancelled</option>
                  </select>
                </div>
                <span className="drop">
                  <TableDisplay
                    selectValue={this.state.selectValue2}
                  />
                </span>
                </div>

                <div className="col l4 card hoverable">
                <div>
                  <select value={this.state.selectValue3}  onChange={this.handle3}>
                      <option value="" disabled selected>Choose</option>
                      <option value="allot">Alloted Class</option>
                      <option value="free">Free Slot</option>
                      <option value="slot_cancel">Slot Cancelled</option>
                  </select>
                </div>
                <span className="drop">
                  <TableDisplay
                    selectValue={this.state.selectValue3}
                  />
                </span>
                </div>
              </div>
          );
  }
}
