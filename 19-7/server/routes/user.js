const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs');
const User = require('../database/models/user')
const Prof = require('../database/models/profile1')
const passport = require('../passport')

router.post('/', (req, res) => {
    //console.log('user signup');

    const { username, password,title,name,id,phone,campus,dept,desgn,dob,count} = req.body
    User.findOne({ username: username }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            msg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
              username: username,
              password: password,
              title: title,
              name: name,
              id: id,
              phone: phone,
              campus: campus,
              dept: dept,
              desgn: desgn,
              dob: dob,
              count: count
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})



router.post('/newd', (req, res) => {
    //console.log('user signup');

    const {id,up_username,up_phone,up_name,up_dob} = req.body;
    // var password = req.body.new_password;
    User.findOne({ id: id }, (err, objs) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if(req.body.new_password) {
          // let exist_password = req.body.current_password;
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            console.log("Successful Authen");
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            console.log(hash);
            User.updateOne({id:id},{ $set: { username: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              console.log("Successful Hash");
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "failed"
            };
            res.send(fail);
          }


        }
        else if(!req.body.new_password)
        {
          User.updateOne({id:id},{ $set: { username: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated"
            };
            res.send(succ);

          })
        }
    })
}
)





/*End of Signup------------------------------------------------------------------------------ */
router.post('/profile1', (req, res) => {

    const { username, id , ug_clg_name,pg_clg_name,phd_clg_name,ug_start_year,pg_start_year,phd_start_year,ug_end_year,pg_end_year,phd_end_year,marks_ug,marks_pg,marks_phd,other_degree,other_degree_info,certificate,honors_awards} = req.body
    User.findOne({ username: username }, (err, objs) => {
        if (err)
        {
          var errormsg = {
            errormsg: "Kindly Priovide Correct Mail Id & Id",
          };
            res.send(errormsg);
        }
        else if (objs.count==="2") {
          var emsg = {
            msg: "Datas are already there",
          };
            res.send(emsg);
        }
        else if ((objs.count === "1") || (obj.count === "0"))
        {
            const profData = new Prof({
              username: username,
              id: id,
              ug_clg_name:ug_clg_name,
              pg_clg_name:pg_clg_name,
              phd_clg_name:phd_clg_name,
              ug_end_year:ug_end_year,
              pg_end_year:pg_end_year,
              phd_end_year:phd_end_year,
              ug_start_year:ug_start_year,
              pg_start_year:pg_start_year,
              phd_start_year:phd_start_year,
              marks_pg:marks_pg,
              marks_ug:marks_ug,
              marks_phd:marks_phd,
              other_degree:other_degree,
              other_degree_info:other_degree_info,
              certificate:certificate,
              honors_awards:honors_awards
            })
            profData.save((err, savedUser) => {
              var succ= {
                succ: "Successful Submitted"
              };
              res.send(succ);
            })
        }
    })
})




/*End of PROFILE1-------------------------------------------------------------------------------- */



router.post(
    '/login',(req, res, next) => {

      const { username, password} = req.body

      User.findOne({ username: username }, function(err, objs){

          if (objs.count === "0")
          {
              var counter = 0;
              User.updateOne({ username: username }, {count: counter+1},(err, user) => {

              })
              console.log(counter); // this prints "Renato", as it should

          }
          else if (objs.count === "1")
          {
              var coun = 0;
              User.updateOne({ username: username }, {count: coun+2},(err, user) => {

              })
              console.log(counter); // this prints "Renato", as it should

          }
      });


      // User.findOne({ username: username },(err, res) => {
      //   if(res.count === 0)
      //   {
      //     var count=0;
      //     User.updateOne({ username: username }, {count: count+1},(err, user) => {
      //
      //     })
      //   }
      //   else if(res.count>=2)
      //   {
      //     var count=0;
      //     User.updateOne({ username: username }, {count: count+3},(err, user) => {
      //
      //     })
      //   }
      // })

        console.log('routes/user.js, login, req.body: ');
        console.log(req.body)
        next()
    },
    passport.authenticate('local'),
    (req, res) => {
        //console.log('logged in', req.user);
        var userInfo = {
            use: req.user.desgn,
            username: req.user.username,
            count: req.user.count
        };
        res.send(userInfo);
    },


)

router.get('/', (req, res, next) => {
    //console.log('===== user!!======')

    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})


router.get('/login', function(req, res) {

    var name = '';
    const { username} = req.user;
    User.findOne({ username: username }, function(err, objs){

        if (objs.count)
        {
            name = objs.count;
            //console.log(name); // this prints "Renato", as it should
            res.send(objs.count);
        }
    });


        // res.render('index', {
        //     title: name,
        //     year: date.getFullYear()
        // });
    });











// router.get('/login', (req, res) => {
//
//
//   //console.log("Hello");
//   //console.log(req.user);
//   const { username} = req.user;
//   User.findOne({ username: username }, (err, res) => {
//     console.log(res.count);
//
//       // console.log(data);
//       // res.json({dir: 1});
//     })
//
//
//
// // res.send(emsg);
// })



router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })



})

router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out' })
    } else {
        res.send({ msg: 'no user to log out' })
    }
})






router.get('/dash2', function(req, res) {
    const { username} = req.user;
    User.findOne({ username: username }, function(err, objs){

        if (objs)
        {
            console.log(objs);
            //console.log(name); // this prints "Renato", as it should
            res.send(objs);
        }
    });


        // res.render('index', {
        //     title: name,
        //     year: date.getFullYear()
        // });
    });

module.exports = router
