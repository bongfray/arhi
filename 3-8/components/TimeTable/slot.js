import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'



{/*---------------------------------------------------------Code for regular classes time table------------------------------------ */}
export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',

    };
    this.componentDidMount= this.componentDidMount.bind(this);
  }
  componentDidMount(){
    M.AutoInit();
  }


  handle1 = (e, index) => {
    // alert(e.target.value);
    // alert(e.target.name);
    const key = e.target.name;
    const value = e.target.value;
    this.setState({ rit: value, index ,day_order: key, index});

  };


  render() {
    // alert(this.state.day_order);
const {datt} = this.props.datt;
    return (
            <div className="root-of-time" >


             {this.props.datt.map((content,index) => {
               return(
              <div className="row" key={index}>
                   <div className="col l12 card hoverable ">
                   <div className="card-title">
                   <span className="">
                   <div className="center day-order-day">{content.head}</div>
                   </span>
                   </div><br /><br />
                     <div>
                       <select name={content.con} onChange={e => this.handle1(e, index)}>
                           <option defaultValue="" disabled selected>Choose</option>
                           <option value="allot">Alloted Class</option>
                           <option value="free">Free Slot</option>
                           <option value="slot_cancel">Slot Cancelled</option>
                       </select>
                     </div>
                     <span className="drop">

                       <TableDisplay
                       selectValue={
                         index == this.state.index && this.state.rit} day_order={this.state.day_order} usern={this.props.usern}
                       />
                     </span>
                   </div>


                   </div>
             );
             })}


</div>
          );
  }
}



{/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */}


class TableDisplay extends Component {
  constructor() {
    super()
    this.state = {
      allot:'',
        count: 0,
        selected:'',
        problem_conduct:'',
        freeslot:'',
        freefield:'',
        c_cancelled: '',
        compensation:'false'

    }
    this.handleAlloted = this.handleAlloted.bind(this)
    this.handleFreeSlot = this.handleFreeSlot.bind(this)
    this.handleFreeVal = this.handleFreeVal.bind(this)
    this.handleFreeField = this.handleFreeField.bind(this)
    this.handleMissComp = this.handleMissComp.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.updateAllotV = this.updateAllotV.bind(this)
    }
    handleAlloted =(e) =>{
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      // alert(this.props.usern)
      axios.post('/user/timeallot', {
        usern: this.props.usern,
        day_slot_time: this.props.day_order,
        selected: this.state.selected,
        order:this.props.day_order+'Alloted',
  		})
  			.then(response => {
  				if(response.status===200){
  					if(response.data.msg)
  					{
              alert(response.data.msg);
  				  }
  					else if(response.data.succ)
  					{
              alert('Success');
  					}
  				}
  			}).catch(error => {
  				window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
  			})
        this.setState({
          selected: '',
      })
    }


    handleFreeField =(e) =>{
      this.setState({
        freefield: e.target.value,
      })

    }

    handleFreeVal =(e) =>{
      this.setState({
        freeslot: e.target.value,
      })

    }


    handleFreeSlot =(e) =>{
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      // alert(this.props.usern)

      axios.post('/user/timefree', {
        usern: this.props.usern,
        order: this.props.day_order+'Free',
        day_slot_time: this.props.day_order,
        freefield: this.state.freefield,
        freeslot: this.state.freeslot,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.msg)
            {
              alert(response.data.msg);
            }
            else if(response.data.succ)
            {
              alert('Success');
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          selected: '',
      })


    }


    handleCancel =(e) =>{
      // alert(e.target.value)
      this.setState({
          c_cancelled: e.target.value,
      })

    }
    handleMissComp = (e) =>{
      // alert(e.target.value)
      this.setState({
        compensation: e.target.value,
      })
    }

    handleCancelSlot =(e) =>{
      // alert(this.props.day_order)
      // alert(this.state.selected)
      e.preventDefault()
      // alert(this.state.problem_conduct)
      alert(this.state.compensation)
      alert(this.state.c_cancelled)

      axios.post('/user/timecancel', {
        usern: this.props.usern,
        order: this.props.day_order+'Cancel',
        day_slot_time: this.props.day_order,
        compensation_status: this.state.compensation,
        c_cancelled: this.state.c_cancelled,
      })
        .then(response => {
          if(response.status===200){
            if(response.data.msg)
            {
              alert(response.data.msg);
            }
            else if(response.data.succ)
            {
              alert('Success');
            }
          }
        }).catch(error => {
          window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
        })
        this.setState({
          selected: '',
      })


    }


    updateAllotV (userObject) {
      // alert(userObject)
      this.setState(userObject)
    }
  render() {


                if (this.props.selectValue === "allot") {
                  return(
                    <div className="">
                    <h5 className="center">B2</h5>
                    <span style={{color:'red'}}>Reminder: </span><span>This slot is booked at the beginig of Semester.You are not allowed to edit it.</span>
                    <p>
                      <label>
                      <input type='radio' id='radio-1' name='myRadio' value='Class_completed' onChange={(e) => this.setState({ selected: e.target.value })} />
                        <span style={{color:'green'}}><b>Class Completed</b></span>
                      </label>
                   </p>
                   <Check updateAllotV={this.updateAllotV} />
                   <div className="right btn-of-submit-at-time"><Link to ="#" className="btn" onClick={this.handleAlloted}>SUBMIT</Link></div>

                    </div>
                  );

                } else if (this.props.selectValue === "free") {
                  return(
                    <div><h6 className="free-head">Free Slot is for different Works. Kindly fill the Details..</h6><br />
                    <span className="quali">Plaese Mention on which area you are going to work :</span><br />
                              <div className="input-field inline">
                                <input id="email_inline" type="text" value={this.state.freefield} onChange={this.handleFreeField} required/>
                                <label for="email_inline">Fill it correctly</label>
                              </div>
                    <label className="pure-material-textfield-outlined alignfull">
                      <textarea
                        className="area"
                        type="text"
                        placeholder=" "
                        min="10"
                        max="60"
                        value={this.state.freeslot}
                        onChange={this.handleFreeVal}
                      />
                      <span>Detail About Your work</span>
                    </label>
                    <div className="right btn-of-submit-at-time"><Link to ="#" className="btn" onClick={this.handleFreeSlot}>SUBMIT</Link></div>
                    </div>
                  );

                }
                else if (this.props.selectValue === "slot_cancel") {
                 return(
                   <div>
                   <label className="pure-material-textfield-outlined alignfull">
                     <textarea
                       className="area"
                       type="text"
                       placeholder=" "
                       min="10"
                       max="60"
                       value={this.state.c_cancelled}
                       onChange={this.handleCancel}
                     />
                     <span>Enter the reason</span>
                   </label><br />
                   <p className="need">
                    <label>
                      <input type="checkbox" value="true" onChange={this.handleMissComp} />
                      <span style={{color:'black'}}>Would you like to Get a extra slot in this week ? </span>
                    </label>
                  </p>
                  <div className="right btn-of-submit-at-time"><Link className="btn" to="#" onClick={this.handleCancelSlot} >SUBMIT</Link>
                  </div>
                   </div>
                 );

               }
                else{
                  return(
                    <div className="inter-drop center">
                      Choose from above DropDown
                    </div>
                  );
                }
  }
}





{/*-------for check radio button----------------------------------- */}

 class CheckDisp extends Component{
   constructor(){
     super();
     this.state = ({
       problem_conduct:'',
     });
     this.componentDidMount = this.componentDidMount.bind(this);
   }
   componentDidMount(){

   }
   handle1Res= (e) =>{
     this.setState({
       problem_conduct:e.target.value,
     });
   }

   render(){
     if(this.props.check === true)
     {
       return(
         <div>
         <input value={this.state.problem_conduct} onChange={this.handle1Res}  />
         </div>
       );
     }
     else if(this.props.check === false)
     {
       return(
         <div>

         </div>
       );
     }
   }
 }

{/*-----------------------------------------------------------Check box--------------------------- */}




class Check extends React.Component{

  constructor() {
    super();
    this.state = {isChecked: false,};
    this.handleChecked = this.handleChecked.bind(this);
  }


  handleChecked =(e) =>{
    // alert(this)
    this.setState({isChecked: true});
    this.props.updateAllotV({
      selected: e.target.value,
    });
  }

  render(){
    return <div>
            <p>
              <label>
              <input type='radio' id='radio-2' name='myRadio' value='problem_conduct' checked={this.state.checked} onChange={this.handleChecked} />
                <span style={{color:'red'}}><b>Problem With  Class Completion</b></span>
              </label>
           </p>
       <p><CheckDisp check={this.state.isChecked}/></p>
    </div>
  }
}

const TableRow = ({ children }) => (
  <div className="row">{children}</div>
);
