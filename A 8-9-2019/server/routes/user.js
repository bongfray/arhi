const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs');
const Nav = require('../database/models/FAC/nav_stat')
const User = require('../database/models/FAC/user')
const Admin = require('../database/models/FAC/admin')
const Prof = require('../database/models/FAC/profile1')
const DayOrder = require('../database/models/FAC/dayorder')
const DayHis = require('../database/models/FAC/dayorderhistory')
const Timetable = require('../database/models/FAC/time_table')
const BluePrint = require('../database/models/FAC/blueprint')
const Request = require('../database/models/FAC/req')
const AdminOrder = require('../database/models/FAC/admin_instruction')
const passport = require('../passport/fac_pass')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')
var db = require('../database')
var autoIncrement = require("mongodb-autoincrement")



// Date.prototype.getWeek = function () {
//     var onejan = new Date(this.getFullYear(), 0, 1);
//     return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
// };
//
// var myDate = new Date.today();
// var dd =myDate.getWeek();
// console.log(dd)






function logout()
{
  const url = 'mongodb://localhost:27017/eWork'
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    var cursor = client.collection('sessions').remove( { } );
  });
}

/*------------------------------------------------------------------------Changing Day Orders------------------- */
let timer;

function nextGo(time)
  {
    timer = setTimeout(() => {
    myFunc()
  },time);
}
clearInterval(timer);

DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
  if(user)
  {
  console.log("Server Started!!!");
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour+minutes+second;
  // let max = (86398000-total);
  let max= 3000;
  console.log(max);
  // console.log(Date.today().equals(Date.parse("today")));
  nextGo(max);
  }
else
{
  Date.prototype.getWeek = function () {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
  };

  var myDate = new Date.today();
  var week =myDate.getWeek();

  var day = Date.today().toString("dd");
  var month = Date.today().toString("M");
  var year = Date.today().toString("yyyy");
  let hour = (new Date().getHours())*3600000;
  let minutes = (new Date().getMinutes())*60000;
  let second =(new Date().getSeconds())*1000;
  let total = hour + minutes + second;
  let time = (86398000-total);
  var day_order;
  var day_order_status;
  if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
  {
    day = day;
    month=  month;
    year=  year;
    day_order = 0;
    day_order_status = "weekend";
  }
  else
  {
    day_order = 1;
    day_order_status= "online";
  }
  const dayyy = new DayOrder({
    day_order: day_order,
    time: time,
    time_now: total,
    week: week,
  });
  dayyy.save((err, savedUser) => {
  });

  DayHis.findOne({day_order : {"$exists" : true}}, function(err, user){
    const dayhistory = new DayHis({
      day_order: day_order,
      day:day,
      month:month,
      year:year,
      day_order_status:day_order_status,
      week:week,
    });
    dayhistory.save((err, savedUser) => {
    });
  })
}
})




function myFunc()
{
    console.log("Entered")
      DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
        if(err)
        {

        }
        else if(user)
        {
          var day = Date.today().toString("dd");
          var month = Date.today().toString("M");
          var year = Date.today().toString("yyyy");
          Date.prototype.getWeek = function () {
              var onejan = new Date(this.getFullYear(), 0, 1);
              return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
          };

          var myDate = new Date.today();
          var week =myDate.getWeek();
          // var arr1 = new Array();
          if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true)
          {
            day_order =0;
            day_order_status='weekend'
            var arr1 = [day_order,day,month,year,week,day_order_status]
            DayHis.findOne({day_order : {"$exists" : true}}, function(err, user){
              const dayhistory = new DayHis({
                date:arr1,
              });
              dayhistory.save((err, savedUser) => {
              });
            })
              nextGo(86398000);
          }
          else
          {
            var yesday = Date.parse("yesterday").toString("dd");
            var day = Date.today().toString("dd");
            var month = Date.today().toString("M");
            var year = Date.today().toString("yyyy");
            let prev = user.day_order;
            let recent = prev + 1;
            let next;
            let day_order_status ="online"
            let hour = (new Date().getHours())*3600000;
            let minutes = (new Date().getMinutes())*60000;
            let second =(new Date().getSeconds())*1000;
            let total = hour + minutes + second;
            let max = (86398000-total);
            let no=0;
            if(prev === 5)
            {
              next=1;
            }
            else
            {
              next = recent;
            }
              DayHis.findOne({$and: [{day:yesday},{month:month},{year:year}]}, function(err, canceluser){
                if(err)
                {

                }
                else if(!canceluser)
                {
                    updatedy(prev,next,max,week)
                }
                else if(canceluser)
                {
                 if(canceluser.day_order_status === "weekend")
                {
                  weekend(prev,canceluser.day,canceluser.month,canceluser.year,max,week)
                }

                else if(canceluser.day_order_status==="cancel")
                {
                  updatedaycancel(prev,canceluser.day_order,max,week);
                }
                else
                {
                  updatedy(prev,next,max,week)
                }
              }

              })
            }
        }

      });

}



function weekend(prev,day,month,year,max,week)
{
  let day = day -1;
  DayHis.findOne({$and: [{day:day},{month:month},{year:year}]}, function(err, canceluser){

    if(canceluser.day_order_status === "weekend")
    {
      weekend(prev,canceluser.day,canceluser.month,canceluser.year,week);
    }
    else if(canceluser.day_order_status === "cancel")
    {
      updatedaycancel(prev,canceluser.day_order,max,week);
    }

  })
}


function updatedaycancel(prev,day_order,max)
{
  DayOrder.updateOne({day_order: prev},{ $set: { day_order: day_order, time: max}} ,(err, newdata) => {
    inserthistory(day_order)
    nextcall();
  })
}

 function updatedy(prev,day_order,max)
 {
   DayOrder.updateOne({day_order: prev},{ $set: { day_order: day_order, time: max}} ,(err, newdata) => {
     inserthistory(day_order)
     nextcall();
   })
 }

 function inserthistory(day_order,day,month,year,day_order_status,week)
 {
   var day = Date.today().toString("dd");
   var month = Date.today().toString("M");
   var year = Date.today().toString("yyyy");
   var day_order_status = "online";

   DayHis.findOne({day_order : {"$exists" : true}}, function(err, user){
     const dayhistory = new DayHis({
       day_order: day_order,
       day:day,
       month:month,
       year:year,
       day_order_status:day_order_status,
       week:week,
     });
     dayhistory.save((err, savedUser) => {
     });
     return;
   })
 }

function nextcall()
{
  logout();
  DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
    nextGo(user.time);
  })
}







/* -------------------------------------- College Cancellation Handle----------------------------------------------------------*/

router.post('/college_cancel', function(req, res) {
  const {reason,day,month,year,day_order} = req.body;
  DayHis.findOne({$and: [{day:day},{month:month},{year:year},{username:req.user.username}]}, function(err, user){
    if(user)
    {
      res.send("have");
      return;
    }
    else{
    const dayhistory = new DayHis({
      day_order_status:'cancel',
      username: req.user.username,
      day_order: day_order,
      day:day,
      month:month,
      year:year,
      reason_of_cancel:reason,
    });
    dayhistory.save((err, savedUser) => {
    });

    res.send("ok")
  }
  })
})





/* -----------------------------------------Fetching Request From Nav Bar ------------------------*/

  router.get('/fetchnav',function(req,res) {
    Nav.find({ }, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })


/*--------------------------------------To Set the color of the 10 Cards------------------ */

router.post('/fetchfrom', function(req, res) {
  console.log(req.body)
  const {slot_time,day,month,year} = req.body;
  Timetable.findOne({$and: [{day_slot_time : slot_time},{usern: req.user.username},{date:day},{month:month},{year:year}]}, function(err, passed){
    res.send(passed);
  })
  });

/*------------------------To Render Slots in 10 Cards in Timetable---------------------------*/
  router.post('/fetchme', function(req, res) {
    BluePrint.findOne({$and: [{timing : req.body.slot_time},{username: req.user.username}]}, function(err, passed){
      res.send(passed);
    })
    });




/*---------------------------------------------------Day Order Fetch----------------------------------- */
  router.get('/fetchdayorder', function(req, res) {
    if(!req.user)
    {
      res.send("Not")
    }
    else
    {
      var day = Date.parse("yesterday").toString("dd");
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
      DayHis.findOne({$and: [{username: req.user.username},{day:day},{month:month},{year:year},{day_order_status:"cancel"}]}, function(err, user){
        if(err)
        {

        }
        else if(user)
        {
          var cancel ={
            cancel_day: user.day_order,
          }
          res.send(cancel);
        }
        else
        {
          DayOrder.findOne({day_order : {"$exists" : true}}, function(err, user){
            if(err){

            }
            else if(user)
            {
                    var day_order ={
                      day_order: user.day_order,
                    }
                    res.send(day_order);
                    console.log(day_order)
            }
          });
        }

        })
    }
    });





    router.post('/fetchfromtimetable', function(req, res) {
      if(!req.user)
      {
        res.send("Not")
      }
      else
      {
      const {day_slot_time} = req.body;
                  BluePrint.findOne({$and: [{timing: day_slot_time},{username: req.user.username}]}, function(err, passed){
                    if(passed)
                    {
                      var alloted_slots ={
                        alloted_slots: passed.alloted_slots,
                      }
                      res.send(alloted_slots);
                    }
                    else{
                      var message ={
                        message: "There is no Saved Data for This Slot",
                      }
                      res.send(message);
                    }
                  })
          }
      });


router.post('/', (req, res) => {

    const { username, password,title,name,mailid,phone,campus,dept,desgn,dob,count} = req.body
    User.findOne({$and: [{username: req.body.username},{mailid:req.body.phone}]}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
            const newUser = new User({
              username: username,
              password: password,
              title: title,
              name: name,
              mailid: mailid,
              phone: phone,
              campus: campus,
              dept: dept,
              desgn: desgn,
              dob: dob,
              count: count,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              blue_allot:0,
              total_adminispercentage:req.body.total_adminispercentage,
              total_academicpercentage:req.body.total_academicpercentage,
              total_researchpercentage:req.body.total_researchpercentage,
              ad_total_role_res:req.body.ad_total_role_res,
              ad_total_clerical:req.body.ad_total_clerical,
              ad_total_planning:req.body.ad_total_planning,
              academic_total_curricular:req.body.academic_total_curricular,
              academic_total_cocurricular:req.body.academic_total_cocurricular,
              academic_total_extracurricular:req.body.academic_total_extracurricular,
              academic_total_evaluation_placementwork:req.body.academic_total_evaluation_placementwork,
              research_total_publication:req.body.research_total_publication,
              research_total_ipr_patents:req.body.research_total_ipr_patents,
              research_total_funded_sponsored_projectes:req.body.research_total_funded_sponsored_projectes,
              research_total_tech_dev_consultancy:req.body.research_total_tech_dev_consultancy,
              research_total_product_development:req.body.research_total_product_development,
              research_total_research_center_establish:req.body.research_total_research_center_establish,
              research_total_research_guidnce:req.body.research_total_research_guidnce,
              ad_complete_role_res:0,
              ad_complete_clerical:0,
              ad_complete_planning:0,
              academic_complete_curricular:0,
              academic_complete_cocurricular:0,
              academic_complete_extracurricular:0,
              academic_complete_evaluation_placementwork:0,
              research_complete_publication:0,
              research_complete_ipr_patents:0,
              research_complete_funded_sponsored_projectes:0,
              research_complete_tech_dev_consultancy:0,
              research_complete_product_development:0,
              research_complete_research_center_establish:0,
              research_complete_research_guidnce:0,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})



router.post('/newd', (req, res) => {

    const {id,up_username,up_phone,up_name,up_dob} = req.body;
    console.log(req.body)
    User.findOne({ username: req.user.username }, (err, objs) => {
        if (err)
        {
            return;
        }
        else if(objs)
        {
         if(req.body.new_password) {
          // let exist_password = req.body.current_password;
          if(bcrypt.compareSync(req.body.current_password, objs.password)) {
            // console.log("Successful Authen");
            let hash = bcrypt.hashSync(req.body.new_password, 10);
            // console.log(hash);
            User.updateOne({username:req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name,password: hash}} ,(err, user) => {
              // console.log("Successful Hash");
              var succ= {
                succ: "Datas are Updated"
              };
              res.send(succ);

            })
          } else {
            var fail= {
              fail: "fail"
            };
            res.send(fail);
          }
        }
        else if(!req.body.new_password)
        {
          User.updateOne({username: req.user.username},{ $set: { mailid: up_username, phone: up_phone , dob: up_dob , name: up_name}} ,(err, user) => {
            var succ= {
              succ: "Datas are Updated",
            };
            res.send(succ);
          })
        }
      }
    })
}
)







/*---------------------------------------------reseting password by mail ------------------------------- */
router.post('/reset_from_mail', (req, res) => {
  //console.log('user signup');

  const {token_come,password} = req.body;
  User.findOne({ resetPasswordToken: token_come }, (err, user) => {
      if (err)
      {
          //console.log('User.js post error: ', err)
      }
      else if(user) {
        console.log(user)
        let hash = bcrypt.hashSync(password, 10);
          User.updateOne({resetPasswordToken: token_come},{password: hash} ,(err, user) => {
            console.log("Successful Hash");
            var succ= {
              succ: "Password Updated"
            };
            res.send(succ);

          })
        } else {
          var fail= {
            fail: "failed"
          };
          res.send(fail);
        }
      })
    })


/*End of Signup------------------------------------------------------------------------------ */



/* --------------------Degree & Certificates--------------------------------------------------------------*/
router.post('/profile1', (req, res) => {
  console.log(req.body)
          var tran = new Prof(req.body.data);
          tran.save((err, savedUser) => {
            var succ= {
              succ: "Successfully Submitted"
            };
            res.send(succ);
          })
})



router.post('/editprofile1', function(req,res) {
  Prof.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
    var succ= {
      succ: "Edited"
    };
    res.send(succ);
  })

});


router.post('/fetchprofile1',function(req,res) {
Prof.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/fetchtoedit',function(req,res) {
Prof.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
// console.log(docs)
if(err)
{

}
else
{
res.send(docs)
}
});
})

router.post('/profile1del',function(req,res) {
  Prof.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
})




/*End of PROFILE1-------------------------------------------------------------------------------- */



router.post(
    '/faclogin',(req, res, next) => {
      const { username, password} = req.body;
      User.findOne({ username: username }, function(err, objs){
        if(err)
        {
          console.log(err)
        }
        else if(objs)
        {
         if (objs.count === 0)
          {
              User.updateOne({ username: username }, { $inc: { count: 1 }},(err, user) => {
              })

          }
          else if (objs.count === 1)
          {
              User.updateOne({ username: username }, { count: 2 },(err, user) => {

              })
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
      console.log(res)
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)

router.get('/', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})


router.get('/knowcount', function(req, res) {
    const { username} = req.user;
    if(req.user)
    {
    User.findOne({ username: username }, function(err, objs){

        if (objs)
        {
            res.send(objs);
        }
    });
  }
  else{
    return;
  }
    });



router.get('/Dash', (req, res, next) => {
  const {user} = req.user

    console.log(req.user)
      User.findOne({ username: user }, (err, user) => {
        if(err)
        {

        }
        else if(user)
        {
          var dir= {
              logins: req.user.desgn,
          };
          res.send(dir);
        }

      })



})

router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out'})
    } else {
        res.send({ msg: 'no user to log out' })
    }
})



router.get('/dash2', function(req, res) {
    const { username} = req.user;
    if(!username)
    {
      var nologin = {
        nologin:'no'
      }
      res.send(nologin);
    }
    else if(username)
    {
    User.findOne({ username: username }, function(err, objs){
      if(err)
      {
        return;
      }
        else if (objs)
        {
            res.send(objs);
        }
    });
  }
});




    router.post('/timeallot', (req, res) => {
        const { day_slot_time, selected,day_order, usern,order, saved_slots,problem_statement,cday,cmonth,cyear,compdayorder,compslot,covered,date,month,year,week} = req.body;
              Timetable.findOne({$and: [{day_slot_time: day_slot_time},{usern: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
                  if (err)
                  {
                      return;
                  }
                  else if(objs)
                  {

                  }
                  else if (!objs) {
                            const newTime = new Timetable({
                              date: date,
                              month: month,
                              year: year,
                              usern: req.user.username,
                              day_order: day_order,
                              day_slot_time:day_slot_time,
                              order: order,
                              selected:selected,
                              problem_statement: req.body.problem_statement,
                              problem_compensation_date: req.body.compday,
                              problem_compensation_month: req.body.compmonth,
                              problem_compensation_year: req.body.compyear,
                              problem_compensation_dayorder: req.body.compdayorder,
                              problem_compensation_slot: req.body.compslot,
                              covered: req.body.covered,
                              week:week,
                            })
                            newTime.save((err, savedUser) => {
                              var succ= {
                                succ: "Success"
                              };
                              res.send(succ);
                            });
                            User.findOne({username:req.user.username}, (err, user) => {
                              User.updateOne({ username: req.user.username }, {academic_complete_curricular:user.academic_complete_curricular+1},(err, user) => {
                              })
                           })
                          }
            });

  })


    router.post('/timefree', (req, res) => {
        const { day_slot_time,freeslot,freefield, order,date,month,year,week } = req.body
        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{usern: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
            if (err)
            {

            }
            else if (objs)
            {

            }
            else if(!objs) {
                if((req.body.freeparts==="ad_total_clerical"))
                {

                  User.findOne({$and: [{username: req.user.username},{ad_total_clerical : {"$exists" : true}}]}, (err, user) => {

                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.ad_complete_clerical===user.ad_total_clerical))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { ad_complete_clerical: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ ad_complete_clerical: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })
                }
                else if(req.body.freeparts==="ad_total_role_res")
                {
                  User.findOne({$and: [{username: req.user.username},{ad_total_clerical : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.ad_complete_role_res===user.ad_total_role_res))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { ad_complete_role_res: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ ad_complete_role_res: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })
                }
                else if(req.body.freeparts==="ad_total_planning")
                {
                  User.findOne({$and: [{username: req.user.username},{ad_total_clerical : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {

                      if(user.week === req.body.week)
                      {
                      if((user.ad_complete_planning===user.ad_total_planning))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { ad_complete_planning: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ ad_complete_planning: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })
                }
                else if(req.body.freeparts==="academic_total_cocurricular")
                {
                  User.findOne({$and: [{username: req.user.username},{academic_total_cocurricular : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.academic_complete_cocurricular===user.academic_total_cocurricular))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { academic_complete_cocurricular: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ academic_complete_cocurricular: 1 },(err, user) => {
                     })
                     updateweek()
                   }


                   }
                  })

                }
                else if(req.body.freeparts==="academic_total_extracurricular")
                {
                  User.findOne({$and: [{username: req.user.username},{academic_total_extracurricular : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.academic_total_extracurricular===user.academic_complete_extracurricular))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { academic_complete_extracurricular: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ academic_complete_extracurricular: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })

                }
                else if(req.body.freeparts==="academic_total_evaluation_placementwork")
                {
                  User.findOne({$and: [{username: req.user.username},{academic_total_evaluation_placementwork : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.academic_complete_evaluation_placementwork===user.academic_total_evaluation_placementwork))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { academic_complete_evaluation_placementwork: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ academic_complete_evaluation_placementwork: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })
                }
                else if(req.body.freeparts==="research_total_publication")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_publication : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_total_publication===user.research_complete_publication))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_publication: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_publication: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })
                }
                else if(req.body.freeparts==="research_total_ipr_patents")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_ipr_patents : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {


                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_ipr_patents===user.research_total_ipr_patents))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_ipr_patents: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_ipr_patents: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })

                }
                else if(req.body.freeparts==="research_total_funded_sponsored_projectes")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_funded_sponsored_projectes : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {


                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_funded_sponsored_projectes===user.research_total_funded_sponsored_projectes))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_funded_sponsored_projectes: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_funded_sponsored_projectes: 1 },(err, user) => {
                     })
                     updateweek()
                   }


                   }
                  })

                }
                else if(req.body.freeparts==="research_total_tech_dev_consultancy")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_tech_dev_consultancy : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_tech_dev_consultancy===user.research_total_tech_dev_consultancy))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_tech_dev_consultancy: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_tech_dev_consultancy: 1 },(err, user) => {
                     })
                     updateweek()
                   }

                   }
                  })

                }
                else if(req.body.freeparts==="research_total_product_development")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_product_development : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_total_product_development===user.research_complete_product_development))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_product_development: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username }, { research_complete_product_development: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })
                }
                else if(req.body.freeparts==="research_total_research_center_establish")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_research_center_establish: {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                      if(user.week === req.body.week)
                      {
                      if((user.research_complete_research_center_establish===user.research_total_research_center_establish))
                      {
                        var suc= {
                          suc: "Field,you are going to submit is already reached to it's max!!"
                        };
                        res.send(suc);
                      }
                      else{
                        send()
                        User.updateOne({ username: req.user.username },{ $inc: { research_complete_research_center_establish: 1 }},(err, user) => {
                        })
                      }
                    }
                   else
                   {
                     send()
                     User.updateOne({ username: req.user.username },{ research_complete_research_center_establish: 1 },(err, user) => {
                     })
                     updateweek()
                   }
                   }
                  })

                }
                else if(req.body.freeparts==="research_total_research_guidnce")
                {
                  User.findOne({$and: [{username: req.user.username},{research_total_research_guidnce : {"$exists" : true}}]}, (err, user) => {
                    if(err)
                    {

                    }
                    else if(!user)
                    {
                      ressend()
                    }
                    else if(user)
                    {
                     if(user.week === req.body.week)
                      {
                        if((user.research_complete_research_guidnce===user.research_total_research_guidnce))
                        {

                          var suc= {
                            suc: "Field,you are going to submit is already reached to it's max!!"
                          };
                          res.send(suc);
                        }
                        else{
                          send()
                          User.updateOne({ username: req.user.username },{ $inc: { research_complete_research_guidnce: 1 }},(err, user) => {
                          })
                        }
                     }
                    else
                    {
                      send()
                      User.updateOne({ username: req.user.username },{ research_complete_research_guidnce: 1 },(err, user) => {
                      })
                    }
                   }
                  })

                }

                function updateweek()
                {
                  User.updateOne({ username: req.user.username },{ week: req.body.week },(err, user) => {
                  })
                }
                function ressend()
                {
                  var not ={
                    not:"This Field is not alloted for you. Kindly Check Dashboard !!",
                  };
                  res.send(not);
                }
                function send()
                {
                const newTime = new Timetable({
                  date: date,
                  month: month,
                  year: year,
                  usern: req.user.username,
                  day_slot_time:day_slot_time,
                  order: order,
                  freefield: freefield,
                  freeslot: freeslot,
                  freeparts: req.body.freeparts,
                  week:week,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                });
              }


            }
        })
    })



    router.post('/timecancel', (req, res) => {
        const { day_slot_time,usern, order, compensation_status,c_cancelled,date,month,year,week } = req.body
        Timetable.findOne({$and: [{day_slot_time: day_slot_time},{usern: req.user.username},{date: date},{month: month},{year: year},{week:req.body.week}]}, (err, objs) => {
            if (err)
            {

            }
            else if(objs)
            {

            }
            else if(!objs)
            {
                const newTime = new Timetable({
                  date: date,
                  month: month,
                  year: year,
                  usern: req.user.username,
                  day_slot_time:day_slot_time,
                  order: order,
                  c_cancelled: c_cancelled,
                  compensation_status: compensation_status,
                  week:week,
                })
                newTime.save((err, savedUser) => {
                  var succ= {
                    succ: "Success"
                  };
                  res.send(succ);
                })
          }
      })
    })




    router.post('/forgo', (req, res) => {
      const {mailid} = req.body
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'ework.care@gmail.com',
               pass: 'uolqdatwxgbhqals'
           }
       });
       User.findOne(
         {username: mailid,
         },
       ).then(user => {
         if(user === null)
         {
           var nodata ={
             nodata:'Invalid User'
           }
           res.send(nodata);
         }
         else{
           const token = crypto.randomBytes(20).toString('hex');
           // console.log(token);
           User.updateOne({username:mailid},{ $set: {  resetPasswordToken: ":"+token,resetPasswordExpires: Date.now() + 1800000,}} ,(err, user) => {
           })
           const mailOptions = {
            from: 'ework.care@gmail.com', // sender address
            to: mailid, // list of receivers
            subject: 'Reset Password:  eWorks', // Subject line
            html: '<p>You are receiving this message because you have requested for reset password in eWork portal</p><br />'+'<a href="http://localhost:3000/reset_password/:'+token+'">Reset you password here</a><br /><p>A genral reminder : This link of reseting password will be valid upto 30min</p>'
          };
          // console.log("Way to enter.......");
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            {

            }
            else{
              // console.log(info);
              var success = {
                success: "Check you Mail Id to Reset Password",
              };
              res.send(success);

            }
         });
         }
       })

        })

        router.get('/reset_password', function(req, res) {
          // console.log(req.query);
          User.findOne({ resetPasswordToken: req.query.resetPasswordToken,resetPasswordExpires: { $gt: Date.now() } }, function(err,user) {
            if (!user) {
              console.log("Expired");
              var expire ={
                expire:"Link is Expired"
              }
              res.send(expire);
            }
            else if(user){
              // console.log("Tested:  "+user.resetPasswordToken)
              res.send(user.resetPasswordToken)
            }

          });
        });





/*------------------------Blue Print Send-------------------------------------------------- */
router.post('/send_blue_print', (req, res) => {
    const {alloted_slots,timing} = req.body
    User.findOne({ username: req.user.username}, (err, user) => {
      if(err)
      {

      }
      else if(user)
      {
        if(user.blue_allot === user.academic_total_curricular)
        {
          var exceed = {
            exceed: "No of the maximum input reached!!",
          };
            res.send(exceed);
        }
        else
        {

        BluePrint.findOne({$and: [{timing: timing},{username: req.user.username}]}, (err, objs) => {
          if(err)
          {

          }
          else if(objs)
          {
            var emsg = {
              emsg: "We have datas saved on the following Time",
            };
              res.send(emsg);
          }
          else{
            const allottime = new BluePrint({
              alloted_slots: alloted_slots,
              timing: timing,
              username: req.user.username,

            })
            allottime.save((err, savedUser) => {
              var succ= {
                succ: "Success"
              };
              res.send(succ);
            })
            User.updateOne({ username: req.user.username},{blue_allot: user.blue_allot+1}, (err, user) => {
            })
          }
        })
      }
      }
    })
  })


/*--------------------------------Getting Percentages of Individual ------------------------------------------------- */
router.get('/getPercentage', (req, res) => {

   User.findOne({username:req.user.username}, (err, user) => {
     if(err)
     {

     }
     else if(user)
     {
       res.send(user);
     }
   })
})





/*----------------------------------------------------------Master Table------------------------------------------ */


router.post('/fetchmaster', function(req, res) {
  const {slot_time} = req.body;
  Timetable.findOne({$and: [{day_slot_time : slot_time},{usern: req.user.username},{week:req.body.week}]}, function(err, passed){
    res.send(passed);
  })
  });








/*--------------------------------------------CRUD CODE--------------------------------------------- */




  router.post('/addmm', function(req,res) {
            var trans = new Admin(req.body.data);
            trans.save((err, savedUser) => {
              var succ= {
                succ: "Done"
              };
              res.send(succ);
            })
  });



function resetAdmincount()
{
  Admin.nextCount(function(err, count) {

  Admin.resetCount(function(err, nextCount) {
    return;
  });

});
}


    router.post('/editt', function(req,res) {
      Admin.updateOne({$and: [{ serial: req.body.data.serial},{username: req.user.username}]},req.body.data, (err, user) => {
        var succ= {
          succ: "Successful SignedUP"
        };
        res.send(succ);
      })

    });


router.post('/fetchall',function(req,res) {
  Admin.find({$and: [{action:req.body.action},{username: req.user.username}]}, function(err, docs){
  if(err)
  {

  }
  else
  {
    res.send(docs)
  }
});
})

router.post('/fetcheditdata',function(req,res) {
  Admin.findOne({$and: [{serial: req.body.id},{username: req.user.username}]}, function(err, docs){
    // console.log(docs)
  if(err)
  {

  }
  else
  {
    res.send(docs)
  }
});
})



router.post('/del',function(req,res) {
  Admin.remove({$and: [{serial:  req.body.serial },{username: req.user.username}]},function(err,succ){
    res.send("OK")
  });
})




router.post('/reset',function(req,res) {
  Admin.findOne({serial : {"$exists" : true}}, function(err, passed){
    if(err)
    {

    }
    else if(passed)
    {
      var notallowed ={
        notallowed:'Sorry Already Fields are there!!'
      }
      res.send(notallowed)
    }
    else if(!passed)
    {
      resetAdmincount()
      var succ={
        succ:'RESET DONE'
      }
      res.send(succ)
    }
  })

})


/*------------------------------------------------Code For Manage Page------------------------------- */
router.post('/request_for_entry',function(req,res) {
  DayHis.findOne({$and: [{day:req.body.day},{month: req.body.month},{year: req.body.year},{day_order:req.body.day_order},{day_order_status: 'online'}]}, function(err, present){
    if(err)
    {

    }
    else if(present)
    {
      Request.findOne({$and: [{day:req.body.day},{month: req.body.month},{year: req.body.year}]}, function(err, passed){
        if(err)
        {

        }
        else if(passed)
        {
          var handled;
          if(passed.action === "approved")
          {
          handled= {
            handled: "Request Already Approved !!"
          };
          res.send(handled);
          }
          else
          {
            handled= {
              handled: "Admin Has Not Approved Till Now!!"
            };
            res.send(handled);
          }
        }
        else{
          var requests = new Request(req.body);
          requests.save((err, savedUser) => {
            var succ= {
              succ: "We have sent your request!!"
            };
            res.send(succ);
          })
        }
      })
    }
    else{
      var noday={
        noday:"Requested DayOrder Doesn't Match !!"
      }
      res.send(noday)
    }
  })
})

router.get('/fetch_requested_list',function(req,res) {
  Request.find({username: req.user.username}, function(err, docs){
  if(err)
  {

  }
  else
  {
    res.send(docs)
  }
});
})


router.post('/fetch_requested_status',function(req,res) {
  Request.findOne({$and:[{username: req.user.username},{serial:req.body.serial}]}, function(err, docs){
  if(err)
  {

  }
  else if(docs)
  {
    if(docs.action ==="approved")
    {
      res.send("approved")
    }
    else if(docs.action ==="denied")
    {
      res.send("denied")
    }
    else if(docs.action ==="false")
    {
      res.send("pending")
    }
  }
  else{

  }
});
})








/*----------------------------------------------------------Start of Admin Operation----------------------------- */

/*----------------------------------------------Admin Operation--------------------------------------------------------*/
router.post('/auth', (req, res) => {
  const {id,username,password} = req.body;
  console.log(id)
   if(req.body.id === "2019")
   {
     User.findOne({username:username}, (err, user) => {
       if(user)
       {
         var existadmin ={
           existadmin:'You already have an account !!',
         }
         res.send(existadmin);
       }
       else
       {
         const newadmin = new User({
           username:username,
           password: password,
           count: 5,
           resetPasswordExpires:'',
           resetPasswordToken:'',
         })
         newadmin.save((err, savedUser) => {
           var succ= {
             succ: "Successful SignedUP"
           };
           res.send(succ);
         })
       }
     })
  }
  else
  {
    var wrong ={
      wrong:'Passcode is Wrong !!'
    }
    res.send(wrong)
  }
  })







    router.post('/addnav', function(req,res) {
              var trans = new Nav(req.body.data);
              trans.save((err, savedUser) => {
                var succ= {
                  succ: "Done"
                };
                res.send(succ);
              })
    });


      router.post('/editnav', function(req,res) {
        Nav.updateOne({ serial: req.body.data.serial},req.body.data, (err, user) => {
          var succ= {
            succ: "Updated"
          };
          res.send(succ);
        })

      });


  router.post('/fetchnav',function(req,res) {
    Nav.find({action: req.body.action}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })


  router.post('/edit_existing_nav',function(req,res) {
    Nav.findOne({serial: req.body.id}, function(err, docs){
      // console.log(docs)
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

  router.post('/delnav',function(req,res) {
    Nav.remove({serial:  req.body.serial },function(err,succ){
      res.send("OK")
    });
  })



  router.get('/getrequest',function(req,res) {
    Request.find({action: "false"}, function(err, docs){
    if(err)
    {

    }
    else
    {
      res.send(docs)
    }
  });
  })

    router.get('/fetch_denied_list',function(req,res) {
      Request.find({action: "denied"}, function(err, docs){
      if(err)
      {

      }
      else
      {
        res.send(docs)
      }
    });
    })


  router.post('/approve_request',function(req,res) {
    console.log(req.body)
    Timetable.findOne({$and:[{usern:req.body.username},{date:req.body.day},{month: req.body.month},{year: req.body.year}]}, function(err, docs){
    if(err)
    {

    }
    else if(docs)
    {

      res.send("yes")
    }
    else
    {
      res.send("no")
    }
  });
  })



    router.post('/denyrequest',function(req,res) {
      Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},{action: "denied"},function(err,done){
          res.send("Done")
        })
      }
    });
    })



    router.post('/approverequest',function(req,res) {

    Request.findOne({$and:[{username: req.body.username,serial: req.body.serial}]}, function(err, docs){
      if(err)
      {
      }
      else if(docs)
      {
        Request.updateOne({$and:[{username:docs.username},{serial: req.body.serial}]},{action: "approved"},function(err,done){
          res.send("Done")
        })
      }
    });
    })


    /*----------------------------------Admin Operation for opening a set of dayorder availbale to everyone----------------- */

    router.post('/show_a_span_of_dayorder_to_all_user',function(req,res) {
      console.log(req.body.startday)
      console.log(req.body.endday)
      var arr1 = new Array();
      DayHis.find({$and: [{$and:[{ day: { $gt: req.body.startday} },{ day: { $lt: req.body.endday}}]},{day_order_status:"online"}]},function(err,matched){
        if(err)
        {
          console.log("NO")
        }
        else if(matched)
        {
          console.log(matched)
          res.send("Can't Proceed With this Request!! Because requested DayOrder Range Having DayOrder Saved in it!! We can't take this request !! Kindly verify and put datas accordingly")
        }
        else if(matched!==0)
        {
          console.log("YUP")
          var trans = new AdminOrder(req.body);
          trans.save((err, savedUser) => {
            res.send("Order has been registered !!");
          })
        }
      })
    })





module.exports = router
