const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise


const userSchema = new Schema({

username: { type: String, unique: false, required: false },
password: { type: String, unique: false, required: false },
title: { type: String, unique: false, required: false },
name: { type: String, unique: false, required: false },
mailid: { type: String, unique: false, required: false },
phone: { type: String, unique: false, required: false },
cnf_pswd: { type: String, unique: false, required: false },
campus: { type: String, unique: false, required: false },
dept: { type: String, unique: false, required: false },
desgn: { type: String, unique: false, required: false },
dob: { type: String, unique: false, required: false },
count: { type: Number, unique: false, required: false },
day_order: { type: String, unique: false, required: false },
resetPasswordToken: { type: String, unique: false, required: false },
resetPasswordExpires: { type: String, unique: false, required: false },
day_order: { type: Number, unique: false, required: false },
blue_allot: { type: Number, unique: false, required: false },
week: { type: Number, unique: false, required: false },

total_adminispercentage: { type: Number, unique: false, required: false },
total_academicpercentage: { type: Number, unique: false, required: false },
total_researchpercentage: { type: Number, unique: false, required: false },
complete_adminispercentage: { type: Number, unique: false, required: false },
complete_researchpercentage: { type: Number, unique: false, required: false },
complete_academicpercentage: { type: Number, unique: false, required: false },

ad_total_role_res: { type: Number, unique: false, required: false },
ad_total_clerical: { type: Number, unique: false, required: false },
ad_total_planning: { type: Number, unique: false, required: false },
ad_total_account_finace: { type: Number, unique: false, required: false },
ad_total_dataentry_analysis: { type: Number, unique: false, required: false },


ad_complete_role_res: { type: Number, unique: false, required: false },
ad_complete_clerical: { type: Number, unique: false, required: false },
ad_complete_planning: { type: Number, unique: false, required: false },
ad_complete_account_finace: { type: Number, unique: false, required: false },
ad_complete_dataentry_analysis: { type: Number, unique: false, required: false },



academic_total_curricular: { type: Number, unique: false, required: false },
academic_total_cocurricular: { type: Number, unique: false, required: false },
academic_total_extracurricular: { type: Number, unique: false, required: false },
academic_total_evaluation_placementwork: { type: Number, unique: false, required: false },

academic_complete_curricular: { type: Number, unique: false, required: false },
academic_complete_cocurricular: { type: Number, unique: false, required: false },
academic_complete_extracurricular: { type: Number, unique: false, required: false },
academic_complete_evaluation_placementwork: { type: Number, unique: false, required: false },


research_total_publication: { type: Number, unique: false, required: false },
research_total_ipr_patents: { type: Number, unique: false, required: false },
research_total_funded_sponsored_projectes: { type: Number, unique: false, required: false },
research_total_tech_dev_consultancy: { type: Number, unique: false, required: false },
research_total_product_development: { type: Number, unique: false, required: false },
research_total_research_center_establish: { type: Number, unique: false, required: false },
research_total_research_guidnce: { type: Number, unique: false, required: false },

research_complete_publication: { type: Number, unique: false, required: false },
research_complete_ipr_patents: { type: Number, unique: false, required: false },
research_complete_funded_sponsored_projectes: { type: Number, unique: false, required: false },
research_complete_tech_dev_consultancy: { type: Number, unique: false, required: false },
research_complete_product_development: { type: Number, unique: false, required: false },
research_complete_research_center_establish: { type: Number, unique: false, required: false },
research_complete_research_guidnce: { type: Number, unique: false, required: false },
})


userSchema.methods = {
	checkPassword: function (inputPassword) {
		return bcrypt.compareSync(inputPassword, this.password)
	},
	hashPassword: plainTextPassword => {
		return bcrypt.hashSync(plainTextPassword, 10)
	}
}



userSchema.pre('save', function (next) {
	if (!this.password) {
		console.log('models/user.js =======NO PASSWORD PROVIDED=======')
		next()
	} else {
		console.log('models/user.js hashPassword in pre save');

		this.password = this.hashPassword(this.password)
		next()
	}
})



const User = mongoose.model('login_db', userSchema)
module.exports = User
