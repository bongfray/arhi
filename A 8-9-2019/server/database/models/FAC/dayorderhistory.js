const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const dayorderhis = new Schema({
  day_order: { type: Number, unique: false, required: false },
  day_order_status: { type: String, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  day: { type: Number, unique: false, required: false },
  month: { type: Number, unique: false, required: false },
  year: { type: Number, unique: false, required: false },
  username: { type: String, unique: false, required: false },
  reason_of_cancel: { type: String, unique: false, required: false },
  date:{ type: Array, unique: false, required: false}
})


const DayHis = mongoose.model('dayorder_history', dayorderhis)
module.exports = DayHis
