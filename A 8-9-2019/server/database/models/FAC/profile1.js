const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');

//Schema
const profSchema = new Schema({

  username: { type: String, unique: false, required: false },
  action: { type: String, unique: false, required: false },
  ug_clg_name: { type: String, unique: false, required: false },
  pg_clg_name: { type: String, unique: false, required: false },
  phd_clg_name: { type: String, unique: false, required: false },
  ug_start_year: { type: String, unique: false, required: false },
  pg_start_year: { type: String, unique: false, required: false },
  phd_start_year: { type: String, unique: false, required: false },
  ug_end_year: { type: String, unique: false, required: false },
  pg_end_year: { type: String, unique: false, required: false },
  phd_end_year: { type: String, unique: false, required: false },
  marks_ug: { type: String, unique: false, required: false },
  marks_pg: { type: String, unique: false, required: false },
  marks_phd: { type: String, unique: false, required: false },
  other_degree_name:{ type: String, unique: false, required: false },
  other_degree_info:{ type: String, unique: false, required: false },
  other_degree_duration:{ type: String, unique: false, required: false },
  other_degree_grade:{ type: String, unique: false, required: false },
  certificate: { type: String, unique: false, required: false },
  honors_awards: { type: String, unique: false, required: false },
})





profSchema.plugin(autoIncrement.plugin, { model: 'prof_db', field: 'serial', startAt: 1,incrementBy: 1 });



var Prof = mongoose.model('prof_db', profSchema);

module.exports = Prof
