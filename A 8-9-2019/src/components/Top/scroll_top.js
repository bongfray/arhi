import React, { Component } from 'react'
import axios from 'axios'
import {Link } from 'react-router-dom'
import '../style.css'
import M from 'materialize-css'


class ScrollButton extends React.Component {
  constructor() {
    super();

    this.state = {
        intervalId: 0
    };
  }

  scrollStep() {
    if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
  }

  scrollToTop() {
    let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
    this.setState({ intervalId: intervalId });
  }

  render () {
      return(
        <div>
        <div className='up' onClick={ () => { this.scrollToTop();}}> <i class="material-icons aroo">arrow_upward</i>
       </div>
        </div>
      );
   }
}

export default class ScrollApp extends React.Component {
  render () {
    return <div>
              <ScrollButton scrollStepInPx="30" delayInMs="26.66"/>
           </div>
  }
}
