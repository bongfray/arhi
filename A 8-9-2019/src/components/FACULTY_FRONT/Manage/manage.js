import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import Nav from '../dynnav'
import SingleManage from './single_user_manage'
import GlobalAccess from './global_manage'
import Insturction from './instra.'

export default class Manage extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      modal:false,
      redirectTo:'',
      option:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      nav_route: '/user/fetchnav',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  getCount =() =>{
    axios.get('/user/knowcount', {
    })
    .then(response =>{
      if(response.data.count ===1)
      {
          this.selectModal();
      }
    })
  }

  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
}

handleOption = (e) =>{
  this.setState({option:e.target.value})
}
componentDidMount(){
  M.AutoInit()
  this.getCount()
  axios.get('/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/faculty',
       });
       window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
     }
   })
}


  render()
  {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <Insturction
          displayModal={this.state.modal}
          closeModal={this.selectModal}
      />
      <div className="row">
      <div className="col l2" />
      <div className="col l8 form-signup">
          <div className="row">
            <div className="col l8">
            <h5 className="">Kindly Choose From RightHandleSide</h5>
            </div>
            <div className="col l4">
            <select name="title" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled selected>Select Here...</option>
            <option value="one">Request For a Entry</option>
            <option value="every">Entry Activity For EveryUser</option>
            </select>
            </div>
          </div>
      </div>
      <div className="col l2" />
      </div>
      <Navigate username={this.state.username} selected={this.state.option} />
      </React.Fragment>

    )
  }
  }
}


class Navigate extends Component{
  constructor()
  {
    super()
    this.state ={

    }
  }
  render()
  {
    let object;
    if(this.props.selected ==="one")
    {
      object = <SingleManage username={this.props.username} />
    }
    else if(this.props.selected === "every")
    {
      object = <GlobalAccess />
    }
    return(
      <div>
       {object}
      </div>
    )
  }
}
