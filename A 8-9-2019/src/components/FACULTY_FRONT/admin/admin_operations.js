import React, { Component,Fragment } from 'react'
import { } from 'react-router-dom'
import {Link } from 'react-router-dom'
import axios from 'axios'
import {Dropdown} from 'react-materialize'
import '../style.css'
import Nav from '../dynnav'
import M from 'materialize-css'
import NavAdd from './NavControl/NavHandle'

export default class Admin extends Component{
constructor(){
    super();
    this.state ={
        isChecked: false,
        selected: '',
        home:'/admin_panel',
        logout:'/user/logout',
        get:'/user/',
        nav_route: '/user/fetchnav',
    }
}
handleChecked =(e)=>{
    this.setState({
        isChecked: !this.state.isChecked,
        selected: e.target.value
    });
}
render()
{
    return(
        <React.Fragment>
<Nav home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
            <div className="row">

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-1' name='myRadio' value='insert' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Insert</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-2' name='myRadio' value='update' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Update</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-3' name='myRadio' value='super' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Super</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-4' name='myRadio' value='delete' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Delete</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-5' name='myRadio' value='modify' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Modify</b></span>
            </label>
            </p>
            </div>
            </div>

            <div className="col s2">
            <div className="col s1"></div>
            <div className="col s10 card">
            <p>
            <label>
            <input type='radio' id='radio-6' name='myRadio' value='suspend' onChange={this.handleChecked} />
            <span style={{color:'green'}}><b>Suspension</b></span>
            </label>
            </p>
            </div>
            </div>

            </div>

            <div className="row">
                <div className="col l12">
            <Display selected={this.state.selected}/>
            </div>
            </div>
        </React.Fragment>
    );
}
}


class Display extends Component{
    constructor(){
        super();
        this.state = {
            isChecked: false,
            select: ''
        }
    }
    handleCheck = (e) => {
        this.setState({
            isChecked: true,
            select: e.target.value
        })

    }
    render(){
        if(this.props.selected === 'insert')
        {
            return(
              <React.Fragment>
                    <div className="card col l2 s12" style={{marginRight: '1px solid'}}>
                        <p>
                        <label>
                        <input type='radio' id='radio-7' name='myRadio' value='insertadmin' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Department Admin</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-8' name='myRadio' value='insert_in_nav' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Insert Into NavBar</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="card col l10 s12">
                            <InsertUser select={this.state.select} />
                    </div>
                    </React.Fragment>
            );
        }
        else if(this.props.selected === 'update')
        {
            return(
                <div className="row card">
                <div className="col s12">
                    <div className="col s3 brd-r">
                        <p>
                        <label>
                        <input type='radio' id='radio-apwd' name='myRadio' value='adminpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Admin Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-fpsw' name='myRadio' value='facpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Faculty Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-spsw' name='myRadio' value='studpwd' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Reset Student Password</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-spsw' name='myRadio' value='all_user_dayorder_entry' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Add DayOrder</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col s9">
                            <UpdateUser select={this.state.select} />
                    </div>


                </div>
                </div>

            );
        }
        else if(this.props.selected === 'super')
        {
            return(

                <div className="row">
                <div className="col l12">
                    <div className="col l2 card">
                        <p>
                        <label>
                        <input type='radio' id='radio-reg' name='myRadio' value='register' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Handle Registration</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-frep' name='myRadio' value='facprofile' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Faculty Profile</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-srep' name='myRadio' value='studprofile' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Student Profile</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-frep' name='myRadio' value='facreport' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Faculty Report</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-srep' name='myRadio' value='studreport' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>View Student Report</b></span>
                        </label>
                        </p>
                        <p>
                        <label>
                        <input type='radio' id='radio-req' name='myRadio' value='approve_single_faculty_req' onChange={this.handleCheck} />
                        <span style={{color:'green'}}><b>Approve Request</b></span>
                        </label>
                        </p>
                    </div>
                    <div className="col l10">
                            <SuperUser select={this.state.select} />
                    </div>
                </div>
                </div>



            );
        }
        else if(this.props.selected === 'delete')
        {
            return(
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-20' name='myRadio' value='deleteadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-21' name='myRadio' value='deletefac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-22' name='myRadio' value='deletestu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Delete Student</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <DeleteUser select={this.state.select} />
                </div>
                </div>
                </div>

            );
        }
        else if(this.props.selected === 'modify')
            {
            return(
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-18' name='myRadio' value='start-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Start Semester</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-19' name='myRadio' value='end-semester' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>End Semester</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-13' name='myRadio' value='modifytoday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Modify Today's Day Order</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-17' name='myRadio' value='holiday' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Declare a Holiday</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <ModifyUser select={this.state.select} />
                </div>
                </div>
                </div>
            )
        }
        else if(this.props.selected === 'suspend')
        {
            return(
                <div className="row card">
                <div className="col s12">
                <div className="col s3 brd-r">
                <p>
                <label>
                <input type='radio' id='radio-14' name='myRadio' value='suspendadmin' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Admin</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-15' name='myRadio' value='suspendfac' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Faculty</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-16' name='myRadio' value='suspendstu' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Student</b></span>
                </label>
                </p>
                <p>
                <label>
                <input type='radio' id='radio-17' name='myRadio' value='suspend_request' onChange={this.handleCheck} />
                <span style={{color:'green'}}><b>Suspend Request</b></span>
                </label>
                </p>
                </div>
                <div className="col s9">
                    <SuspendUser select={this.state.select} />
                </div>
                </div>
                </div>

            )
        }

        else{
            return(
                <div></div>
            )
        }
    }
}

/*------------Insert------------ */

class InsertUser extends Component{
    constructor(){
        super();
        this.state = {
            username: '',
            pswd: ''
        }
    }
    handleInsert = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleSubmit(event) {
      event.preventDefault()

    }
    render(){
        if(this.props.select==='insertadmin')
        {
            return(
                <div className="row">
                <div className="col l3"/>
                <div className="col l6">
                <div className="input-field col s6">
								<input id="username" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleInsert} required />
								<label htmlFor="username">Username</label>
								</div>

								<div className="input-field col s6">
								<input id="password" type="password" className="validate" name="pswd" value={this.state.pswd} onChange={this.handleInsert} required />
								<label htmlFor="password">Password</label>
								</div>
								<button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Insert Admin</button>
                </div>
                <div className="col l3"/>
                </div>
                )
        }
        if(this.props.select==='insert_in_nav')
        {
            return(
                  <NavAdd />
                )
        }

        else{
            return(
                <div></div>
            )
        }
    }
}

/*-------------------End of Insert--------------- */

/**-----------------Update------------------------ */

class UpdateUser extends Component{
    constructor(){
        super();
        this.initialState={
            username:'',
            password:'',
            startday:'',
            endday:'',
            startmonth:'',
            endmonth:'',
            startyear:'',
            endyear:'',
            value:'',
            response:'',
        }
        this.state = this.initialState;
    }

handleAddAccess_To_DayOrder = (e) =>{
  this.setState({
    [e.target.name]:e.target.value,
  })
}



  handleAllUser_DayOrder_Add = (e) =>{
    e.preventDefault();
    axios.post('/user/show_a_span_of_dayorder_to_all_user',this.state)
    .then(response=>{
      this.setState({response: response.data})
    })
  }


    handleUpdate = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleAdminUpdate = (e) =>{
      e.preventDefault()
      alert("Admin Here")
      this.setState({username:'',pswd:''})
    }
    handleStudentUpdate = (e) =>{
      e.preventDefault()
      alert("Student here")
      this.setState({username:'',pswd:''})
    }
    render(){
      var button;
        if(this.props.select === 'adminpwd'){
          button =
          <div>
          <div className="input-field col s6">
          <input id="uusername" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleUpdate} required />
          <label htmlFor="uusername">Username</label>
          </div>

          <div className="input-field col s6">
          <input id="upassword" type="password" className="validate" name="pswd" value={this.state.pswd} onChange={this.handleUpdate} required />
          <label htmlFor="upassword">Password</label>
          </div>
          <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleAdminUpdate}>Admin Reset</button>
          </div>
        }
        else if(this.props.select === 'facpwd'){
          button =
          <div>
          <div className="input-field col s6">
          <input id="uusername" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleUpdate} required />
          <label htmlFor="uusername">Username</label>
          </div>

          <div className="input-field col s6">
          <input id="upassword" type="password" className="validate" name="pswd" value={this.state.pswd} onChange={this.handleUpdate} required />
          <label htmlFor="upassword">Password</label>
          </div>
          <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleAdminUpdate}>Faculty Reset</button>
          </div>
        }
        else if(this.props.select === 'studpwd'){
          button =
          <div>
          <div className="input-field col s6">
          <input id="uusername" type="text" className="validate" name="username" value={this.state.username} onChange={this.handleUpdate} required />
          <label htmlFor="uusername">Username</label>
          </div>

          <div className="input-field col s6">
          <input id="upassword" type="password" className="validate" name="pswd" value={this.state.pswd} onChange={this.handleUpdate} required />
          <label htmlFor="upassword">Password</label>
          </div>
          <button className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleStudentUpdate}>Student Reset</button>
          </div>
        }
        else if(this.props.select === 'all_user_dayorder_entry')
        {
          button =
          <div>
           <div className="row">
             <div className="col l5 form-signup">
             Start Date
               <div className="row">
                <div className="col l4 input-field">
                <input type="text" id="sday" name="startday" value={this.state.startday} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="sday">Day(dd)</label>
                </div>
                <div className="col l4 input-field">
                 <input type="text" id="smonth" name="startmonth" value={this.state.startmonth} onChange={this.handleAddAccess_To_DayOrder}/>
                 <label htmlFor="smonth">Month(mm)</label>
                </div>
                <div className="col l4 input-field">
                <input type="text" id="syear" name="startyear" value={this.state.startyear} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="syear">Year(yyyy)</label>
                 </div>
               </div>
             </div>
             <div className="col l1" />
             <div className="col l6 form-signup">
             End Date
               <div className="row">
                <div className="col l4 input-field">
                <input type="text" id="eday" name="endday" value={this.state.endday} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="eday">Day(dd)</label>
                </div>
                <div className="col l4 input-field">
                 <input type="text" id="emonth" name="endmonth" value={this.state.endmonth} onChange={this.handleAddAccess_To_DayOrder}/>
                 <label htmlFor="emonth">Month(mm)</label>
                </div>
                <div className="col l4 input-field">
                <input type="text" id="eyear" name="endyear" value={this.state.endyear} onChange={this.handleAddAccess_To_DayOrder}/>
                <label htmlFor="eyear">Year(yyyy)</label>
                 </div>
               </div>
              </div>
           </div>
           <button className="btn col l2 right blue-grey darken-2 sup" style={{marginBottom:'5px'}} onClick={this.handleAllUser_DayOrder_Add}>SUBMIT</button>
<br />
              <div className="center">{this.state.response}</div>
          </div>
        }
        else{
            return(
                <div></div>
            )
        }
        return(
            <div className="center">

            {button}
            </div>
        )
    }
}

/**------------------Super----------------------- */

class SuperUser extends Component{
    constructor(){
        super();
        this.state={
            susername:'',
            username:'',
            index_id:'',
            request:[],
            viewdata:'',
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    getRequest =() =>{
      axios.get('/user/getrequest')
          .then(response => {
            this.setState({request: response.data})
          })
    }
    view_Status_of_Request = (content,index) =>{
      this.setState({
        username:content.username,
        index_id:index,
      })
      axios.post('/user/approve_request',content)
          .then(response => {
            if(response.data)
            {
              this.setState({viewdata: response.data})
            }
          })
    }
    componentDidMount()
    {
      this.getRequest();
    }
    handleRequestModify =(object) =>{
      console.log(object)
      this.setState(object)
    }
    render(){
      var viewbutton;
        if(this.props.select === 'register'){
            return(
                <div className="row" style={{padding:'15px'}}>
                <div className=" col s12" >

                <div className="row">
                <div className="col l8">
                  Stop Faculty Registration
                </div>
                <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
                </div>
                </div>

                <div className="row">
                <div className="col l8">
                  Stop Student Registration
                </div>
                <div className="col l4">
                <div className="switch">
                  <label>
                    Off
                    <input type="checkbox" />
                    <span className="lever"></span>
                    On
                  </label>
                </div>
                </div>
                </div>
                </div>
                </div>
            )
        }
        else if(this.props.select === 'facprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studprofile'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Studnet Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'facreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Faculty Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === 'studreport'){
          viewbutton =
          <React.Fragment>
          <div className="input-field col s12">
            <input id="username" type="text" className="validate" value={this.state.susername} onChange={this.handleUser} required />
            <label htmlFor="username">Username</label>
          </div>
          <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleView}>View Student Profile</Link>
          </React.Fragment>
        }
        else if(this.props.select === "approve_single_faculty_req")
        {
          viewbutton =
          <React.Fragment>
          <div className="row">
            <div className="col l1 center"><b>Serial No.</b></div>
            <div className="col l1 center"><b>Day Order</b></div>
            <div className="col l2 center"><b>Official ID</b></div>
            <div className="col l3 center"><b>Requested Date(dd/mm/yyyy)</b></div>
            <div className="col l3 center"><b>Reason</b></div>
            <div className="col l2 center"><b>Action</b></div>
          </div><hr />
          {this.state.request.map((content,index)=>(
            <React.Fragment key={index}>
            <div className="row">
            <div>
            <div className=" col l1 center">{index+1}</div>
            <div className=" col l1 center">{content.day_order}</div>
            <div className=" col l2 center">{content.username}</div>
            <div className=" col l3 center">{content.day}/{content.month}/{content.year}</div>
            <div className=" col l3 center">{content.req_reason}</div>
            </div>
            <button className="btn col l2 blue-grey darken-2 sup" onClick={() => this.view_Status_of_Request(content,content.serial)}>View Status</button>
            </div>
            </React.Fragment>
          ))}
          <ViewApprove handleRequestModify={this.handleRequestModify} request={this.state.request} view_details={this.state.viewdata} username={this.state.username} index_id={this.state.index_id}/>
          </React.Fragment>
        }
        else{
          viewbutton =
          <div></div>
        }
        return(
            <div className="center">
            {viewbutton}
            </div>
        )
    }
}

class ViewApprove extends Component{
  constructor(props)
  {
    super(props)
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  handleDeny =(id,username) =>{
    axios.post('/user/denyrequest',{
      serial: id,
      username:username,
    }).then(res=>{
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  handleApprove = (id,username) =>{
    axios.post('/user/approverequest',{
      serial: id,
      username:username,
    }).then(res=>{
      var request = this.props.request;
      this.props.handleRequestModify({
        request: request.filter(request => request.serial !== id),
        viewdata:'',
      })
    })
  }
  render()
  {
    var dat;
    if(this.props.view_details ==="yes")
    {
      dat= <React.Fragment>
      <h6>Request is not acceptable !! As this request having datas saved in DB !! As per the policy of eWork, kindly deny request !!</h6>
      <div className="row">
      <button className="btn col l2 right red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      </div>
      </React.Fragment>
    }
    else if(this.props.view_details ==="no")
    {
      dat= <React.Fragment>
      <h6>We can't able to find any data for this request !! Kindly take decison manually !!</h6>
      <div className="row">
      <div className="col l7" />
      <div className="col l5">
      <div className="row">
      <div className="col l5" />
      <button className="btn col l3 sup red" onClick={() => this.handleDeny(this.props.index_id,this.props.username)}>DENY</button>
      <div className="col l1" />
      <button className="btn col l3 blue-grey darken-2 sup" onClick={() => this.handleApprove(this.props.index_id,this.props.username)}>APPROVE</button>
      </div>
      </div>
      </div>
      </React.Fragment>
    }
    else{
      dat =<div></div>
    }
    return(
      <div className="form-signup">
      {dat}
      </div>
    )
  }
}
/**-----------------End of Super----------------- */

/*-------------------Delete--------------------- */

class DeleteUser extends Component{
    constructor(){
        super();
        this.state={
            username: ''
        }
    }
    handleDelete = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handleSubmit(event){

    }
    render(){
      let deleteoperation;
        if(this.props.select === 'deleteadmin'){
          deleteoperation =	<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Admin</Link>
        }
        else if(this.props.select === 'deletefac'){
          deleteoperation = <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Faculty</Link>
        }
        else if(this.props.select === 'deletestu'){
           deleteoperation =   <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Delete Student</Link>
        }
        else{
            return(
                <div></div>
            )
        }
        return(
            <div className="center">
            <div className="input-field col s12">
              <input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleDelete} required />
              <label htmlFor="username">Enter Username to Delete</label>
            </div>
            {deleteoperation}
            </div>

            )
    }

}

/*---------------End of Delete---------------- */

/*-------------------Modify-------------------- */

class ModifyUser extends Component{
    constructor(){
        super();
        this.state={
            dayorder: '',
            day: '',
            month: '',
            year: '',
            sday: '',
            smonth: '',
            syear: '',
            eday: '',
            emonth: '',
            eyear: ''
        }
    }
    handleDayorder = (e) =>{
        this.setState({
            dayorder: e.target.value
        })
    }
    handleDay = (e) =>{
        this.setState({
            day: e.target.value
        })
    }
    handleMonth = (e) =>{
        this.setState({
            month: e.target.value
        })
    }
    handleYear = (e) =>{
        this.setState({
            year: e.target.value
        })
    }
    handleSday = (e) =>{
        this.setState({
            sday: e.target.value
        })
    }
    handleSmonth = (e) =>{
        this.setState({
            smonth: e.target.value
        })
    }
    handleSyear = (e) =>{
        this.setState({
            syear: e.target.value
        })
    }
    handleEday = (e) =>{
        this.setState({
            eday: e.target.value
        })
    }
    handleEmonth = (e) =>{
        this.setState({
            emonth: e.target.value
        })
    }
    handlEyear = (e) =>{
        this.setState({
            eyear: e.target.value
        })
    }
    handleSubmit(event){

    }
    handleHoliday(event){

    }
    handleStart(event){

    }
    handleEnd(event){

    }

    render(){
        if(this.props.select === 'start-semester'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="sday" type="text" className="validate" value={this.state.sday} onChange={this.handleSday} required />
								<label htmlFor="sday">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="smonth" type="text" className="validate" value={this.state.smonth} onChange={this.handleSmonth} required />
								<label htmlFor="smonth">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="syear" type="text" className="validate" value={this.state.syear} onChange={this.handleSyear} required />
								<label htmlFor="syear">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleStart}>Start Semester</Link>
                </div>

            )
        }
        else if(this.props.select === 'end-semester'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="eday" type="text" className="validate" value={this.state.eday} onChange={this.handleEday} required />
								<label htmlFor="eday">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="emonth" type="text" className="validate" value={this.state.emonth} onChange={this.handleEmonth} required />
								<label htmlFor="emonth">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="eyear" type="text" className="validate" value={this.state.eyear} onChange={this.handleEyear} required />
								<label htmlFor="eyear">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleEnd}>End Semester</Link>
                </div>

            )
        }
        else if(this.props.select === 'modifytoday'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="dayorder" type="text" className="validate" value={this.state.dayorder} onChange={this.handleDayorder} required />
								<label htmlFor="dayorder">Reset Today's Day Order to:</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleSubmit}>Reset Day Order</Link>
                </div>

            )
        }
        else if(this.props.select === 'holiday'){
            return(
                <div className="center">

                                <div className="input-field col s4">
								<input id="day" type="text" className="validate" value={this.state.day} onChange={this.handleDay} required />
								<label htmlFor="day">Day (dd)</label>
								</div>
                                <div className="input-field col s4">
								<input id="month" type="text" className="validate" value={this.state.month} onChange={this.handleMonth} required />
								<label htmlFor="month">Month (mm)</label>
								</div>
                                <div className="input-field col s4">
								<input id="year" type="text" className="validate" value={this.state.year} onChange={this.handleYear} required />
								<label htmlFor="year">Year (yyyy)</label>
								</div>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s6 offset-s3" onClick={this.handleHoliday}>Declare a Holiday</Link>
                </div>

            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}

/*-------------------End of Modify-------------*/

/*------Suspend -----*/

class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            username: '',
            deniedlist:[],
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount()
    {
      this.fetch_denied_list()
    }

    fetch_denied_list =() =>{
      axios.get('/user/fetch_denied_list')
      .then(res=>{
        this.setState({deniedlist: res.data})
      })
    }
    handleApprove = (id,username) =>{
      axios.post('/user/approverequest',{
        serial: id,
        username:username,
      }).then(res=>{
        var request = this.state.deniedlist;
        this.setState({
          deniedlist: request.filter(request => request.serial !== id),
        })
      })
    }


    handleUsername = (e) =>{
        this.setState({
            username: e.target.value
        })
    }
    handleSuspend(event){

    }
    handleRemove(event){

    }
    render(){
        if(this.props.select === 'suspendadmin'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Admin</Link>
                </div>

            )
        }
        else if(this.props.select === 'suspendfac'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Faculty</Link>
                </div>

            )
        }
        else if(this.props.select === 'suspendstu'){
            return(
                <div className="center">

                                <div className="input-field col s12">
								<input id="username" type="text" className="validate" value={this.state.username} onChange={this.handleUsername} required />
								<label htmlFor="username">Username</label>
								</div>
                                <Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5" onClick={this.handleRemove}>Remove Suspension</Link>
								<Link to="#" className="waves-effect btn #37474f blue-grey darken-3 col s5 offset-s2" onClick={this.handleSuspend}>Suspend Student</Link>
                </div>

            )
        }
        else if(this.props.select === 'suspend_request'){
            return(
                <div>
                {this.state.deniedlist.map((content,index)=>(
                  <React.Fragment key={index}>
                  <div className="row">
                  <div>
                  <div className=" col l2 center">{content.username}</div>
                  <div className=" col l1 center">{content.day}</div>
                  <div className=" col l1 center">{content.month}</div>
                  <div className=" col l2 center">{content.year}</div>
                  <div className=" col l1 center">{content.day_order}</div>
                  <div className=" col l3 center">{content.req_reason}</div>
                  </div>
                  <button className="btn col l2 " onClick={() => this.handleApprove(content.serial,content.username)}>Approve</button>
                  </div>
                  </React.Fragment>
                ))}
                </div>
            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}

/*------------End of Suspend------------ */
