import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import {API_URL} from './utils/apiUrl'
import Signup from './components/sign-up'
import Ssignup from './components/ssign-up'
import LoginForm from './components/login-form'
import Navbar from './components/navbar'
import PartB from './components/Sections/par'
import PartA from './components/Sections/PartA'
import Home from './components/Home'
import Dash from './components/DashBoard/dash/global-data/Dashboard'
import Profile from './components/Profile/profile'
import Con from './components/online-offline/Connection'
import DashProf from './components/Profile/DashProf'
import Home2 from './components/Home2'
import Scroll from './components/Top/scroll_top'
import TimeTab from './components/TimeTable/time'
import DashProf2 from './components/Profile/NewDash'
import Dash3 from './components/Profile/exdash'
import TimeTable from './components/Schedule/Timetable'

import ResetPass from './components/Reset/reset_password'

import './style.css'
require('disable-react-devtools');

class App extends Component {
  intervalID;
  intervalID1;

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      day_order: '',
      time: '',

    }
    // this.getCurrent = this.getCurrent.bind(this)
    // this.getDatas = this.getDatas.bind(this)
    this.getUser = this.getUser.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }


// getCurrent(){
//   axios.get('/user/fetchdayorder', {
//   })
//   .then(response =>{
//     if(response.data.time)
//     {
//       // let hour = (new Date().getHours())*3600000;
//       // let minutes = (new Date().getMinutes())*60000;
//       // let second =(new Date().getSeconds())*1000;
//       // let time_ren = hour + minutes + second;
// // time_ren-response.data.time
//       this.intervalID = setTimeout((this.getDatas.bind(this)),(response.data.time));
//     }
//   });
// }
//
//
//
// getDatas(){
//   axios.get('/user/updatedayorder', {
//   })
//   .then(response => {
//     if(response.status === 200)
//     {
//       if(response.data)
//       {
//         this.getCurrent();
//       }
//     }
//
//
//   })
// }






  updateUser (userObject) {
    this.setState(userObject)
  }



  getUser() {
// this.getCurrent();
    axios.get('/user/').then(response => {
      console.log('Get user response: ')
      console.log(response.data)
      if (response.data.user) {
        console.log('Get User: There is a user saved in the server session: ')
        console.log(response.data.user);
        this.setState({
          loggedIn: true,
          username: response.data.user.username,

        })
      } else {
        console.log('Get user: no user');
        this.setState({
          loggedIn: false,
          username: null,

        })
      }
    })
  }


  componentDidMount() {
     this.getUser();
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }


  render() {
    return (
       <div className="App">

        <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn} logins={this.state.logins}/>


        {/*<Con />*/} {/*This is for checking the connectivity */}


        <Route path="/login" render={() =>
          <LoginForm
         updateUser={this.updateUser}
         />}
       />

       <Route path="/signup" component={Signup} />
       <Route path="/ssignup" component={Ssignup} />



      <Route path="/partB" render={() =>
        <PartB
       updateUser={this.updateUser} loggedIn={this.state.loggedIn}
       />} />
       <Route path="/partA" render={() =>
         <PartA loggedIn={this.state.loggedIn}
        />} />
       <Route exact path="/" component={Home} />

       <Route path="/profile1" render={() =>
         <Profile username={this.state.username} loggedIn={this.state.loggedIn}
        />} />

       <Route path="/dashprof" component={DashProf} />
       <Route path="/dash2" render={() =>
         <DashProf2 loggedIn={this.state.loggedIn}
        />} />
       <Route path="/home" component={Home2} />
       <Route path="/Dash" render={() =>
         <Dash
        updateUser={this.updateUser} loggedIn={this.state.loggedIn}
        />} />

         <Route path="/newd" component={Dash3} />
         <Route path="/blue_print" component={TimeTable} />
         <Route path="/reset_password/:token" component={ResetPass} />

         <Route path="/time" render={() =>
           <TimeTab loggedIn={this.state.loggedIn} username={this.state.username}
          />} />
         {/*<Scroll/>*/}
      </div>
    );
  }
}

export default App;
