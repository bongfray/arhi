import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from './forget'
import axios from 'axios'
import './style.css'
import M from 'materialize-css';
import { } from 'materialize-css';

var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redir: '',
            redirectTo: null
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)
        this.getDir = this.getDir.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'Enter all the Details', outDuration:'1000', inDuration:'1000', displayLength:'4000'});
          this.setState({
              cnf_pswd:''
          })
          return false;
        }
        else{
        axios.post('/user/login', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                console.log('login response: ')
                console.log(response)
                if (response.status === 200) {
                  this.props.updateUser({
                    loggedIn: true,
                    username: response.data.username
                  });
                  console.log(response.data.count);

                if(response.data.count==="0")
                {
                  this.setState({
                      redirectTo: '/profile1'
                  })
                }
                else if(response.data.count >= "1")
                {
                  this.setState({
                      redirectTo: '/'
                  })
                }


                }
            }).catch(error => {

                window.M.toast({html: 'Check your Login Credentials',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});

            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }
    notifi = () => window.M.toast({html: 'Enter Details', outDuration:'900', inDuration:'900', displayLength:'1700'});

        componentDidMount() {
            this.notifi();
            this.getDir();

            M.AutoInit();
          let elems = document.querySelectorAll(".modal-trigger");
          M.Modal.init(elems, {
          inDuration: 300,
          outDuration: 225,
          hover: true,
          belowOrigin: true
        });
        }
        getDir(){
          axios.get('/user/login').then(response => {
            if(response.status === 200)
            {
              console.log(response.data);
              this.setState({
                redir: response.data
              });
            }
          })
        }
    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

            return (
              <div className="row">

             <div className="col s4">
             </div>

             <div className="col l4 s12 m12 form-login">
                 <div className="ew center">
                     <Link to ="/"><h5 className="blue-grey darken-2 highlight">eWORK</h5></Link>
                     <h5 className="logi"><b>LOG IN</b></h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="email" type="text" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <label htmlFor="email">Email</label>
                     </div>
                     <div className="input-field inline col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <label htmlFor="password">Password</label>
                     </div>
                     </div>
                     <br/>
                     <div className="row">
                     <a href="#modal2" className=" btn blue-grey darken-2 col s5 sup modal-trigger">Register</a>
                     <div id="modal2" className="modal modal-fixed z-depth-5 modaln">
                    <div className="modal-content">
                    <div className="mcont"  style={{marginBottom:'35px'}}>
                      <Link to="/signup" className="col s5 waves-effect btn #311b92 deep-purple darken-4">Faculty</Link>
                      <div className="col s2"></div>
                      <Link to="/ssignup" className="col s5 waves-effect btn #00c853 green accent-4">Student</Link>
                    </div>
                    </div>
               
                    </div>
                     <div className="col s2"></div>
                     <button className=" btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <br/>
                     <hr/>
                     <div className="row">
                     <Modal />
                     </div>

                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
           );
         }

    }
}

export default LoginForm
