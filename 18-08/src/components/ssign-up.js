import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import './style.css'

var empty = require('is-empty');



class SSignup extends Component {
	constructor() {
    super()
    this.state = {
			redirectTo: null,
        name: '',
        mailid: '',
        regid: '',
        phone: '',
        password: '',
        cnf_pswd: '',
        campus: '',
		dept: '',
		degree: '',
		st_year: '',
		fn_year: '',
		dob:'',
		count: 0,
    }
    this.handleName = this.handleName.bind(this)
    this.handleId = this.handleId.bind(this)
    this.handleEmail = this.handleEmail.bind(this)
    this.handlePhone = this.handlePhone.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.handleConfirmPassword = this.handleConfirmPassword.bind(this)
    this.handleCampus = this.handleCampus.bind(this)
    this.handleDept = this.handleDept.bind(this)
	this.handleDOB = this.handleDOB.bind(this)
	this.handleSubmit = this.handleSubmit.bind(this)
	this.handleDegree = this.handleDegree.bind(this)
	this.handleStYear = this.handleStYear.bind(this)
    this.handleFnYear = this.handleFnYear.bind(this)



    }
		handleDOB =(e) =>{
			this.setState({
				dob: e.target.value
			});
		}

	    
	    handleName = (e) => {
	            this.setState({
	                name: e.target.value
	            });
	        };
	    handleId = (e) => {
	            this.setState({
	                regid: e.target.value
	            });
	        };
	    handleEmail = (e) => {
	            this.setState({
	                mailid: e.target.value
	            });
	        };
	    handlePhone = (e) => {
	            this.setState({
	                phone: e.target.value
	            });
	        };
	    handlePasswordChange = (e) => {
	            this.setState({
	              password: e.target.value
	            });
	        };
	    handleConfirmPassword = (e) => {
	              this.setState({
	                  cnf_pswd: e.target.value
	                })
	        };
	    handleCampus = (e) => {
	            this.setState({
	                campus: e.target.value
	            });
	        };
	    handleDept = (e) => {
	            this.setState({
	                dept: e.target.value
	            });
			};
		handleDegree = (e) => {
			this.setState({
				degree: e.target.value
			});
		};
		handleStYear = (e) => {
			this.setState({
				st_year: e.target.value
			});
		};
		handleFnYear = (e) => {
			this.setState({
				fn_year: e.target.value
			});
		};
	    
	handleSubmit(event) {		
		var verify = this.state.regid;
		var vermail = this.state.mailid;
		console.log('sign-up handleSubmit, username: ')
		console.log(this.state.username)
		event.preventDefault()
		if(empty(this.state.fn_year)||empty(this.state.st_year)||empty(this.state.degree)||empty(this.state.name)||empty(this.state.mailid)||empty(this.state.regid)||empty(this.state.phone)||empty(this.state.password)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.dept))
		{
			window.M.toast({html: 'Enter all the Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
      return false;
		}
		else if(this.state.password !==this.state.cnf_pswd)
		{
	     window.M.toast({html: 'Password does not match', outDuration:'850', inDuration:'800', displayLength:'1500'});
	     return false;

	  }
	  else if(!verify.includes('RA'))
	  {
		window.M.toast({html: 'Enter Registration Number starting with RA', outDuration:'850', inDuration:'800', displayLength:'1500'});
		this.setState({
			regid: ''
		})
		return false;
	  }
	  else if(verify.length!==15)
	  {
		window.M.toast({html: 'Registration Number should be of 15 digit', outDuration:'850', inDuration:'800', displayLength:'1500'});
		
		return false;
	  }
	  else if((!vermail.includes('srmuniv.edu.in'))&&(!vermail.includes('srmist.edu.in')))
	  {
		window.M.toast({html: 'Enter official SRM Mail Id', outDuration:'850', inDuration:'800', displayLength:'1500'});
		this.setState({
			mailid: ''
		})
		return false;
	  }
	  
		else if ( ((this.state.password).match(/\d/)) && ((this.state.password).match(/[A-Z]/)) && ((this.state.password).match(/[A-z]/)))
		{
			window.M.toast({html: 'Password Pattern {A-a-1}', outDuration:'850', inDuration:'800', displayLength:'1500'});

		}
		else if ((this.state.phone).length!==10)
		{
			window.M.toast({html: 'Enter correct format of Phone no', outDuration:'850', inDuration:'800', displayLength:'1500'});
			this.setState({
					phone:''
			})
			return false;
		}
		else{



		axios.post('/user/student', {
			regid: this.state.regid,
			password: this.state.password,
      		name: this.state.name,
      		mailid: this.state.mailid,
			phone: this.state.phone,
			campus: this.state.campus,
	    	dept: this.state.dept,
			dob: this.state.dob,
			count: this.state.count,
			degree: this.state.degree,
			st_year: this.state.st_year,
			fn_year: this.state.fn_year
		})
			.then(response => {
				console.log(response)
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, outDuration:'9000', classes:'rounded #ba68c8 purple lighten-2'});
				  }
					else if(response.data.succ)
					{
						alert(response.data.succ);
						this.setState({
								redirectTo: '/login'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
			})
			this.setState({
	    name: '',
	    mailid: '',
	    regid: '',
	    phone: '',
	    password: '',
	    cnf_pswd: '',
	    campus: '',
	    dept: '',
			dob:''
		})
	}
}
	notifi = () => window.M.toast({html: 'Enter Details', outDuration:'1000', inDuration:'900', displayLength:'1800'});

    componentDidMount() {
        this.notifi();
		M.AutoInit();
		
    }

render() {
	if (this.state.redirectTo) {
			 return <Redirect to={{ pathname: this.state.redirectTo }} />
	 } else {
	return (
		<div className="row">

		<div className="col s2">
		</div>

		<div className="col l8 s12 m12 form-signup">
				<div className="ew center">
						<h5 className="reg">Student Registration</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">

								<div className="input-field col s6">
								<input id="name" type="text" className="validate" value={this.state.name} onChange={this.handleName} required />
								<label htmlFor="name">Name</label>
								</div>

								<div className="input-field col s6">
								<input id="stud_id" type="text" className="validate" value={this.state.regid} onChange={this.handleId} required />
								<label htmlFor="stud_id">Registration Number</label>
								</div>

						</div>

						

						<div className="input-field row">

								<div className="input-field col s5">
								<input id="email" type="email" className="validate" value={this.state.mailid} onChange={this.handleEmail} required />
								<label htmlFor="email">Email</label>
								</div>

								<div className="input-field col s4">
								<input id="ph_num" type="text" className="validate" value={this.state.phone} onChange={this.handlePhone} required />
								<label htmlFor="ph_num">Phone Number</label>
								</div>
								<div className="input-field col s3">
								<input id="dob" type="text" className="validate" value={this.state.dob} onChange={this.handleDOB} required />
								<label htmlFor="dob">Date of Birth (dd-mm-yyyy)</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s6">
								<input onChange={this.handlePasswordChange} id="pswd" value={this.state.password} type="password" className="validate" required />
								<label htmlFor="pswd">Password</label>
								</div>

								<div className="input-field col s6">
								<input onChange={this.handleConfirmPassword} id="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
								<label htmlFor="cnf_pswd">Confirm Password</label>
								</div>

						</div>

						<div className="input-field row">

								<div className="input-field col s4">
										<select defaultValue={this.state.degree} onChange={this.handleDegree}>
										<option value="" disabled selected>Degree</option>
										<option value="btech">B.Tech</option>
										<option value="mtech">M.Tech</option>
										<option value="bca">BCA</option>
										<option value="mca">MCA</option>
										</select>
								</div>
								
								<div className="input-field col s4">
								<input id="st_year" type="text" className="validate" value={this.state.styear} onChange={this.handleStYear} required />
								<label htmlFor="st_year">Starting Year (yyyy)</label>
								</div>

								<div className="input-field col s4">
								<input id="fn_year" type="text" className="validate" value={this.state.fnyear} onChange={this.handleFnYear} required />
								<label htmlFor="fn_year">Completion Year (yyyy)</label>
								</div>

						</div>
						<div className="input-field row">

								<div className="input-field col s4">
										<select defaultValue={this.state.campus} onChange={this.handleCampus}>
										<option value="" disabled selected>Campus</option>
										<option value="Kattankulathur Campus">Kattankulathur Campus</option>
										<option value="Ramapuram Campus">Ramapuram Campus</option>
										<option value="Vadapalani Campus">Vadapalani Campus</option>
										<option value="NCR Campus">NCR Campus</option>
										</select>
								</div>
								<div className="input-field col l4 s4 m4">
										<select defaultValue={this.state.dept} onChange={this.handleDept}>
										<option value="" disabled selected>Department</option>
										<option value="Computer Science">Computer Science</option>
										<option value="Information Technology">Information Technology</option>
										<option value="Software Engineering">Software</option>
										<option value="Mechanical Engineering">Mechanical</option>
										</select>
								</div>
						</div>
						<br/>
						<div className="row"><div className="col l6 m12 s12 left">
						<Link to='/login' className="log"> <b> Login Instead ?</b></Link></div>

						<div className="col l6 s12 m12 right">
						<Link to="#" className="waves-effect btn blue-grey darken-2 sup" onClick={this.handleSubmit}>Submit</Link>
						</div>
						</div>
				</form>
		</div>
		</div>

	);
}
}
}

export default SSignup;
