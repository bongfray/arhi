const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const profSchema = new Schema({

  username: { type: String, unique: false, required: false },
  id: { type: String, unique: false, required: false },
  ug_clg_name: { type: String, unique: false, required: false },
  pg_clg_name: { type: String, unique: false, required: false },
  phd_clg_name: { type: String, unique: false, required: false },
  ug_start_year: { type: String, unique: false, required: false },
  pg_start_year: { type: String, unique: false, required: false },
  phd_start_year: { type: String, unique: false, required: false },
  ug_end_year: { type: String, unique: false, required: false },
  pg_end_year: { type: String, unique: false, required: false },
  phd_end_year: { type: String, unique: false, required: false },
  marks_ug: { type: String, unique: false, required: false },
  marks_pg: { type: String, unique: false, required: false },
  marks_phd: { type: String, unique: false, required: false },
  other_degree: { type: String, unique: false, required: false },
  other_degree_info: { type: String, unique: false, required: false },
  certificate: { type: String, unique: false, required: false },
  honors_awards: { type: String, unique: false, required: false },
})


const Prof = mongoose.model('prof_db', profSchema)
module.exports = Prof
