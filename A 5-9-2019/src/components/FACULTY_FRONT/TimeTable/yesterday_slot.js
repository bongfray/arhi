import React,{ Component, Fragment } from 'react'
import { } from 'react-router-dom'
import {Link , Redirect} from 'react-router-dom'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import ColorRep from './colordiv.js'
import Nav from '../dynnav'
import '../style.css'
require("datejs")


{/*-------------------------------------------------Clock for Current Date and time------------------- */}

class Clock extends React.Component {
  constructor(props) {
    super(props);
    var date= this.getTimeString();
    this.state= {
      time: date
    }
  }
  getTimeString() {
    const date = new Date(Date.now()).toLocaleTimeString();
    return date;
  }
  componentDidMount() {
    const _this = this;
    this.timer = setInterval(function(){
      var date = _this.getTimeString();
      _this.setState({
        time: date
      })
    },1000)
  }
  componentWillUnmount() {
      clearInterval(this.timer);
  }
  render() {
    return(
      <div className="date">
      <span>{new Date().toDateString()}</span>
      <p>{this.state.time}</p>
      </div>
    );
  }
}



{/*-------------------------------------------------------Automatically Chanege the day order------------------------------------------------*/}

class Day_Order extends Component{
  render(){
    return(
      <div className="day_order"><b>DAY&emsp;{this.props.day_order}</b></div>
    );
  }
}



/*-------------------------------------------------------This is the class which we are rendenring to the client side-------------------------------------------- */


export default class YesSimple extends Component{

  constructor(props) {
    super(props);
    this.getDayOrder = this.getDayOrder.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.state = {
      opendir: '',
      display:'block',
      saved_dayorder:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/fprofile',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
    }
  }

  getDayOrder = () =>{
    axios.get('/user/fetchdayorder', {
    })
    .then(response =>{
      if(response.data === "Not"){
        this.setState({
          redirectTo:'/',
        });
        window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
      }
      else if(( (Date.today().is().saturday()) || (Date.today().is().sunday()) )===true){
        this.setState({
          display:'none',
          saved_dayorder:'NO',
        });
      }
      else if(response.data.day_order===1){
        this.setState({
          saved_dayorder: 5,
        });
      }
      else if(response.data.day_order){
        this.setState({
          saved_dayorder: response.data.day_order-1,
        });
      }
    });
  }



  componentDidMount(){
    M.AutoInit();
    this.getDayOrder();
  }

  handledir = (e) => {
          this.setState({
              opendir: e.target.value
          });
      };
  render(){
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav home={this.state.home} content={this.state.content} get={this.state.get} logout={this.state.logout}/>
      <div className="time-div">
      <div className="row">
      <div className="col l2">
        <Clock />
      </div>
      <div className="col l10">
        <Link to="time_new" className="right" style={{color:'black'}}>Go to Current DayOrder</Link><br />
        <p className="center day-order-day" style={{fontSize:'20px'}}>Yesterday's DayOrder</p>
      </div>
      </div>
      <div className="row">
      <div className="col l2">
          <Day_Order day_order={this.state.saved_dayorder}/>
        <br />
        <div className="status_of_day" style={{display:this.state.display}}>
              <select value={this.state.opendir}  onChange={this.handledir}>
                <option value="" disabled selected>Select Here</option>
                <option value="r_class">Regular Class</option>
                <option value="own_ab">Absent</option>
                <option value="college_cancel">College Cancelled</option>
              </select>
          </div>
    </div>
    <div className="col l10">
      <Content opendir={this.state.opendir} day_order={this.state.saved_dayorder} usern={this.props.username}/>
    </div>
    </div>
      </div>
      </React.Fragment>
    );
  }
  }
}




{/*-----------------------------------------------This class is for dynamically allocating div ------------- */}

class Content extends Component{
  constructor(){
    super();
    this.state={
  }
      this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }

  render(){
    if(this.props.opendir==="r_class")
    {
      var day = Date.parse("yesterday").toString("dd")
      if(( (Date.today().is().monday()) )===true)
      {
        day= day-2
      }
      else
      {
        day=day
      }
      var month = Date.today().toString("M");
      var year = Date.today().toString("yyyy");
          return(
            <React.Fragment>
             <ColorRep usern={this.props.usern} day={day} month={month} year={year} day_order={this.props.day_order}/>
            </React.Fragment>
          );

    }
    else if(this.props.opendir ==="own_ab")
    {
      return(
        <div>
         <Switch />
        </div>
      );
    }
    else if(this.props.opendir === "college_cancel")
    {
      return(
        <div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Kindly Provide the Reason of College Cancellation</span>
        </label>
        <a href="#" className="waves-effect btn col l2 s4 blue-grey darken-2 sup right">Submit</a>
        </div>
      );
    }
    else{
      return(
        <div className="def-reg center">Please Select from the DropDown</div>
      );
    }

  }
}

class Switch extends React.Component {

    constructor ( props ) {
        super( props );

		this.state = {
			isChecked: false,
      history:'',
		}
    }
    handleComp = (e) =>{
      this.setState({
        isChecked: !this.state.isChecked,
        history: e.target.value,
      })
    }
    render () {

        return(
          <div>

            <div className="switch center">
                <label style={{color:'red',fontSize:'15px'}}>
                    Want to compensate on other day ?
                    <input  checked={ this.state.isChecked } value="compense" onChange={ this.handleComp} type="checkbox" />
                    <span className="lever"></span>
                    Want to Handover this class to Other Faculty?
                </label>
            </div>
            <br />
            <br /><br />
            <InputValue datas={this.state.isChecked}/>
          </div>
        );
    }

}

class InputValue extends Component{
  render(){
    if(this.props.datas === true)
    {
      return(
        <div>
        <div className="input-field">
           <input id="fac" type="text" />
           <label for="fac">Name of the Faculty</label>
        </div>
        <label className="pure-material-textfield-outlined alignfull">
          <textarea
            className="area"
            type="text"
            placeholder=" "
            min="10"
            max="60"
          />
          <span>Provide the Details of the Classes here</span>
        </label>
          <a className="btn">Submit</a>
        </div>
      );
    }
    else{
      return(
        <div>
        <div className="input-field">
           <input id="fac1" type="text" />
           <label for="fac1">Mention the Date</label>
        </div>
        <div className="input-field">
           <input id="fac2" type="text" />
           <label for="fac2">Mention the DayOrder where you want to compensate this</label>
        </div>
        <div className="input-field">
           <input id="fac3" type="text" />
           <label for="fac3">Mention the slot</label>
        </div>
          <a className="btn">Submit</a>
        </div>
      );
    }
  }
}
