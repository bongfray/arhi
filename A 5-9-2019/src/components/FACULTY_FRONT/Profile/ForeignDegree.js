import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import AddProduct from './add';
import ProductList from './prod'
import Nav from '../dynnav'
import "../style.css";
var empty = require('is-empty');

 export default class Gg extends Component{
  constructor(props) {
     super(props);
     this.state = {
       redirectTo: null,
       logout:'/user/logout',
       home:'/faculty',
       get:'/user/',
       content:[
         {
           val:'Profile',
           link:'/fprofile',
         },
         {
           val:'DashBoard',
           link:'/dash',
         },
         {
           val:'Sections',
           link:'/partA',
         },
         {
           val:'TimeTable',
           link:'/time_new',
         },
         {
           val:'Master TimeTable',
           link:'/master',
         }
       ],
       username:'',
       isAddProduct: false,
       response: {},
       product: {},
       isEditProduct: false,
       action:this.props.options,

     }
     this.componentWillMount = this.componentWillMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
   }
componentWillMount()
{
  axios.get('/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/faculty',
       });
       window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
     }
   })
}

   onCreate = (e,index) => {

     this.setState({ isAddProduct: e.target.name ,product: {}});
   }

   onFormSubmit(data) {
     let apiUrl;

     if(empty(data))
     {
       window.M.toast({html: 'Enter All the Details !!',outDuration:'9000', classes:'rounded  red lighten-1'});
       return false;
     }
     else
     {
     if(this.state.isEditProduct){
       apiUrl = '/user/editprofile1';
     } else {
       apiUrl = '/user/profile1';
     }
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false
           })
         })
    }
   }

   editProduct = (productId,index)=> {
     axios.post('/user/fetchtoedit',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: index,
             isAddProduct: index,
           });
         })

  }
   render() {
     let productForm,title,description;

             var data = {
               index:'UG',
               title :'U.G Degree',
               fielddata: [
                 {
                   header: "Title of Completed Thesis/Project",
                   name: "ug_clg_name",
                   placeholder: "Enter the Collage Name",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Description of Thesis/Project",
                   name: "ug_start_year",
                   placeholder: "Starting Year of UG",
                   type: "text",
                   grid: 2
                 },
                 {
                   header: "Credits",
                   name: "ug_end_year",
                   type: "text",
                   grid: 2,
                   placeholder: "Ending Year of UG"
                 },
                 {
                   header: "Date of Completion",
                   name: "marks_ug",
                   type: "text",
                   grid: 2,
                   placeholder: "Enter Your Grade/ Mark"
                 },

               ],
             };
  var data1 = {
    index:'PG',
      title :'P.G Degree',
      fielddata: [
        {
          header: "Title of Completed Thesis/Project",
          name: "pg_clg_name",
          placeholder: "Enter the Collage Name",
          type: "text",
          grid: 2
        },
        {
          header: "Description of Thesis/Project",
          name: "pg_start_year",
          placeholder: "Starting Year of PG",
          type: "text",
          grid: 2
        },
        {
          header: "Credits",
          name: "pg_end_year",
          type: "text",
          grid: 2,
          placeholder: "Ending Year of PG"
        },
        {
          header: "Date of Completion",
          name: "marks_pg",
          type: "text",
          grid: 2,
          placeholder: "Enter Your Grade/ Mark"
        },

      ],
  };
  var data2 = {
    index:'PHD',
      title :'PHD',
      fielddata: [
        {
          header: "Title of Completed Thesis/Project",
          name: "pg_clg_name",
          placeholder: "Enter the Collage Name",
          type: "text",
          grid: 2
        },
        {
          header: "Description of Thesis/Project",
          name: "pg_start_year",
          placeholder: "Starting Year of PG",
          type: "text",
          grid: 2
        },
        {
          header: "Credits",
          name: "pg_end_year",
          type: "text",
          grid: 2,
          placeholder: "Ending Year of PG"
        },
        {
          header: "Date of Completion",
          name: "marks_pg",
          type: "text",
          grid: 2,
          placeholder: "Enter Your Grade/ Mark"
        },

      ],
  };
  var data3 = {
    index:'Doctorate',
      title :'Dr.',
      fielddata: [
        {
          header: "Title of Completed Thesis/Project",
          name: "pg_clg_name",
          placeholder: "Enter the Collage Name",
          type: "text",
          grid: 2
        },
        {
          header: "Description of Thesis/Project",
          name: "pg_start_year",
          placeholder: "Starting Year of PG",
          type: "text",
          grid: 2
        },
        {
          header: "Credits",
          name: "pg_end_year",
          type: "text",
          grid: 2,
          placeholder: "Ending Year of PG"
        },
        {
          header: "Date of Completion",
          name: "marks_pg",
          type: "text",
          grid: 2,
          placeholder: "Enter Your Grade/ Mark"
        },

      ],
  };
  var data4 = {
    index:'Other',
      title :'Other',
      fielddata: [
        {
          name: "other_degree_name",
          type: "text",
          grid: 2,
          placeholder: "Name of the Degree"
        },
        {
          name: "other_degree_info",
          placeholder: "Other Details",
          type: "text",
          grid: 2
        },
        {
          name: "other_degree_duration",
          placeholder: "Enter the Duration of the Degree",
          type: "text",
          grid: 2
        },
        {
          name: "marks_other",
          type: "text",
          grid: 2,
          placeholder: "Enter Your Grade/ Mark"
        },
      ],
  };
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   } else {
return (
  <React.Fragment>
  <div className="row">
  <div className="col l4" />
  <div className="col l4" style={{marginTop:'10px'}}>
    <Link to="/"><h5 className="prof-heading center black-text">YOUR DEGREES</h5></Link>
  </div>
  <div className="col l4"/>
  </div>
          <div className="row">
            <div className="col l1 s1 left profinfohead"><b>Degree</b></div>
            <div className="col l1 s1 center profinfohead"><b>No.</b></div>
            <div className="col l2  s2 center profinfohead"><b>College Name</b></div>
            <div className="col l2 s2 center profinfohead"><b>Year of Starting</b></div>
            <div className="col l2 s2 center profinfohead"><b>Year of Completion</b></div>
            <div className="col l2 s2 center profinfohead"><b>Percentage/CGPA</b></div>
            <div className="col l2 s2  center profinfohead"><b>Actions</b></div>
          </div>

      <hr />

          <div className="row">
            {!this.state.isAddProduct && <ProductList username={this.state.username} title={title} title={data.title} action={data.title} data={data}  editProduct={this.editProduct}/>}
            {!this.state.isAddProduct &&
             <React.Fragment>
               <button className="btn right blue-grey darken-2 sup subm" name="U.G Degree" onClick={(e) => this.onCreate(e,data)}>Add Degree</button>
            </React.Fragment>
          }
          {(this.state.isAddProduct==="U.G Degree" || this.state.isEditProduct==="U.G Degree") &&
            <AddProduct username={this.state.username} action={data.title} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
          }
          </div>

      <hr />

          <div className="row">
            {!this.state.isAddProduct && <ProductList username={this.state.username} title={title} title={data1.title} action={data1.title} data={data1}  editProduct={this.editProduct}/>}
            {!this.state.isAddProduct &&
             <React.Fragment>
               <button className="btn right blue-grey darken-2 sup subm" name="P.G Degree" onClick={(e) => this.onCreate(e,data1)}>Add Degree</button>
            </React.Fragment>
          }
          {(this.state.isAddProduct==="P.G Degree" || this.state.isEditProduct==="P.G Degree") &&
            <AddProduct  username={this.state.username} action={data1.title} data={data1} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
          }
          </div>

      <hr />
      <div className="row">
        {!this.state.isAddProduct && <ProductList username={this.state.username} title={title} title={data2.title} action={data2.title} data={data2}  editProduct={this.editProduct}/>}
        {!this.state.isAddProduct &&
         <React.Fragment>
           <button className="btn right blue-grey darken-2 sup subm" name="PHD" onClick={(e) => this.onCreate(e,data2)}>Add Degree</button>
        </React.Fragment>
      }
      {(this.state.isAddProduct==="PHD" || this.state.isEditProduct==="PHD") &&
        <AddProduct  username={this.state.username} action={data2.title} data={data2} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
      }
      </div>

  <hr />
  <div className="row">
    {!this.state.isAddProduct && <ProductList username={this.state.username} title={title} title={data3.title} action={data3.title} data={data3}  editProduct={this.editProduct}/>}
    {!this.state.isAddProduct &&
     <React.Fragment>
       <button className="btn right blue-grey darken-2 sup subm" name="Dr." onClick={(e) => this.onCreate(e,data3)}>Add Degree</button>
    </React.Fragment>
  }
  {(this.state.isAddProduct==="Dr." || this.state.isEditProduct==="Dr.") &&
    <AddProduct  username={this.state.username} action={data3.title} data={data3} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
  }
  </div>

<hr />
<div className="row">
  {!this.state.isAddProduct && <ProductList username={this.state.username} title={title} title={data4.title} action={data4.title} data={data4}  editProduct={this.editProduct}/>}
  {!this.state.isAddProduct &&
   <React.Fragment>
     <button className="btn right blue-grey darken-2 sup subm center" name="Other" onClick={(e) => this.onCreate(e,data4)}>Add Degree</button>
  </React.Fragment>
}
{(this.state.isAddProduct==="Other" || this.state.isEditProduct==="Other") &&
  <AddProduct  username={this.state.username} action={data4.title} data={data4} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
}
</div>

<hr />
</React.Fragment>
);
}
}
}
