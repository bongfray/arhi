import React from 'react';
import axios from 'axios'
export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      action:'',
    }
    this.fetch = this.fetch.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }

  componentWillMount(){

      this.fetch()
  }

  componentDidUpdate =(prevProps) => {
    if (prevProps.action !== this.props.action) {
      this.fetch();
    }
  }


  deleteProduct = (productId,index) => {
  const { products } = this.state;
  axios.post('/user/del',{
    serial: productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
          response: response,
          products: products.filter(product => product.serial !== productId)
       })
      })
}


fetch =() =>{
  axios.post('/user/fetchall',{
    action: this.props.action,
    username: this.props.username,
  })
  .then(response =>{
    this.setState({
     products: response.data,
   })
  })
}
  render() {

    const { error, products} = this.state;
      return(
        <div>
        <div className="row">
          <div className="col s1">
            <h5 className="right td ft">A .</h5>
          </div>
          <div className="col s11 td">
            <h5 className="ft">
            {this.props.description}
            </h5>
          </div>
        </div>
        <br />
          <table>
            <thead className="col s12">
            <tr>
            <th className="col s1">Serial No</th>
            {this.props.data.fielddata.map((content,index)=>(
              <th className="col s2 center" key={index}>{content.header}</th>
            ))}
            <th className="col s1 center"> Action</th>
            </tr>
            </thead>
            <tbody>
              {products.map((product,index) => (
                <tr key={product.serial}>
                  <td className="center">{index+1}</td>
                  {this.props.data.fielddata.map((content,index)=>(
                    <td className="center" key={index}>{product[content.name]}</td>
                  ))}
                    <td className="center"><button className="btn-small btnalign green" onClick={() => this.props.editProduct(product.serial,this.props.action)}>Edit</button>
                    &nbsp;<button className="btn-small btnalign red" onClick={() => this.deleteProduct(product.serial,this.props.action)}>Delete</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )

  }
}
