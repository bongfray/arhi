import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import M from 'materialize-css';
import {} from 'materialize-css'
import {Collapsible, CollapsibleItem} from 'react-materialize'
import Collap from './Gg'
import Nav from '../dynnav'
export default class Section extends Component {

  constructor(props)
  {
    super(props)
    this.state ={
      username:'',
      redirectTo:'',
      option:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/fprofile',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

handleOption = (e) =>{
  this.setState({option: e.target.value})
}

componentDidMount(){
  M.AutoInit()
  axios.get('/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
     }
     else{
       this.setState({
         redirectTo:'/faculty',
       });
       window.M.toast({html: 'You are not Logged In',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
     }
   })
}


  render()
  {
    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return(
      <React.Fragment>
      <Nav home={this.state.home} content={this.state.content} get={this.state.get} logout={this.state.logout}/>
      <h5 className="center prof-heading black-text">This  Part  is  For  Enlisting  Your  Achivements</h5>
      <div className="row">
      <div className="col l6">
        <p className="day-order-day">Kindly Select On Which Area You Are Going to Upload You Datas</p>
      </div>
        <div className="col l6">
            <select name="title" value={this.state.option} onChange={this.handleOption}>
            <option value="" disabled selected>Select Here...</option>
            <option value="thesis_projects_supervised">Thesis & Projects Supervised</option>
            <option value="training_workshop_attended">Training or Workshop or Seminer or Conference Attended</option>
            <option value="training_workshop_held">Training or Workshop or Seminer or Conference Held</option>
            <option value="paper_published_accepted">Paper Published & Accepted</option>
            <option value="research_projects_done_by_you">Research or Projects Done by You</option>
            <option value="contribution_to_university">Contribution to University</option>
            <option value="department_activities">Department Activities</option>
            <option value="department_committe_membership">Department Committes Membership</option>
            <option value="advising_counselling_details">Advising and Counselling Details</option>
            </select>
        </div>
        </div>
          <Collap options={this.state.option} username={this.state.username}/>
      </React.Fragment>

    )
  }
  }
}
