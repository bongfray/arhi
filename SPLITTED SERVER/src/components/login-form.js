import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Modal from './forget'
import axios from 'axios'
import './style.css'
import M from 'materialize-css';
import { } from 'materialize-css';

var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redir: '',
            redirectTo: null
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        //console.log('handleSubmit')
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'Username and Password cannot be empty !!',outDuration:'1000', classes:'rounded #f44336 red'});
          this.setState({
              cnf_pswd:''
          })
          return false;
        }
        else{
        axios.post('/user/faclogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                console.log(response)
                if (response.status === 200) {
                  this.props.updateUser({
                    loggedIn: true,
                    username: response.data.username
                  });
                if(response.data.count==="0")
                {
                  this.setState({
                      redirectTo: '/profile1'
                  })
                }
                else if(response.data.count >= "1")
                {
                  this.setState({
                      redirectTo: '/'
                  })
                }
                }
            }).catch(error => {
                window.M.toast({html: 'Check your Login Credentials',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});

            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }



    getUser = () => {
      axios.get('/user/').then(response => {
        if (response.data.user) {
            this.setState({redirectTo:'/'});
            window.M.toast({html: 'Sorry already loggedIn !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
          })
        }



        componentDidMount() {
            this.getUser();
            M.AutoInit();

        }

    render() {

      if (this.state.redirectTo) {
           return <Redirect to={{ pathname: this.state.redirectTo }} />
       } else {

            return (
              <div className="row">

             <div className="col s4">
             </div>

             <div className="col l4 s12 m12 form-login">
                 <div className="ew center">
                     <Link to ="/"><h5 className="blue-grey darken-2 highlight">eWORK</h5></Link>
                     <h5 className="logi"><b>LOG IN</b></h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="email" type="email" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <label htmlFor="email">Email</label>
                     </div>
                     <div className="input-field inline col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <label htmlFor="password">Password</label>
                     </div>
                     </div>
                     <br/>
                     <div className="row">
                     <Link to="/signup" className=" btn blue-grey darken-2 col s5 sup">Register</Link>
                     <div className="col s2"></div>
                     <button className=" btn blue-grey darken-2 col s5 sup" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <br/>
                     <hr/>
                     <div className="row">
                     <Modal />
                     </div>

                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
           );
         }

    }
}

export default LoginForm
