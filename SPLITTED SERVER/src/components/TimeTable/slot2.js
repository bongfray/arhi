import React, { Component } from 'react'
import { } from 'react-router-dom'
import {Link,Redirect } from 'react-router-dom'
import {Button, Modal} from 'react-materialize'
import M from 'materialize-css'
import axios from 'axios'
import Allot from './Allot'
import Free from './Free'
import Cancel from './Cancel'
import Modal2 from './Modal'

{/*---------------------------------------------------------Code for regular classes time table------------------------------------ */}



export default class TimeSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rit:'',
      day_order:'',
      modal: false,

    };

  }
  selectModal = (info) => {
  this.setState({modal: !this.state.modal}) // true/false toggle
}
  componentDidMount(){
    M.AutoInit();
  }


  handle1 = (e, index) => {
    // alert(e.target.value);
    // alert(e.target.name);
    const key = e.target.name;
    const value = e.target.value;
    this.setState({ rit: value, index ,day_order: key, index});

  };


  render()
  {


    if (this.state.redirectTo) {
      return <Redirect to={{ pathname: this.state.redirectTo }} />
  } else {
      return(
        <div className="root-of-time" >
        <div className="node">
        <div style={{backgroundColor:'white'}} onClick={ this.selectModal } className="card hoverable each_time">
        <p className="center">{this.props.time}</p>
        <p className="go center"
        ><b>{this.props.slots}</b></p>
        </div>
   <Modal2
   displayModal={this.state.modal}
   closeModal={this.selectModal}
   day_slot_time={this.props.day_slot_time}
   usern={this.props.usern}
   day_order={this.props.day_order}
   color={this.props.color}
   />
   </div>
   </div>
      )
  }
  }
}



{/*----------------------------------------------------For blueprint of content handleConfirmPassword Displaying the regular content  values -------------------------------------- */}
