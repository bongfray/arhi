import React from "react";

export default class Table extends React.Component {
  state = {
    data: [
      1
    ]
  };
  appendChild = () => {
    let { data } = this.state;
    data.push(data.length)
    this.setState({data});
  };
  render() {
    return (
      <table>
        <thead>
          <th>Id of the Faculty</th>
          <th>Slot Name</th>
          <th>Slot Timing</th>
        </thead>
        <tbody>
          {this.state.data.map(id => (
            <Row id = {id} append={this.appendChild}/>
          ))}
          <a className="btn" onClick={this.appendChild}>Add</a>
        </tbody>
      </table>
    );
  }
}

const Row = ({ id}) => (
  <tr>
    <td>
      <input type="text" id={`select-type-${id}`} placeholder="Enter Details" />
    </td>
    <td>
      <input type="text" id={`select-position-${id}`} placeholder="Enter Details" />
    </td>
    <td>
      <input type="text" id={`select-position-${id}`} placeholder="Enter Details" />
    </td>
  </tr>
);
