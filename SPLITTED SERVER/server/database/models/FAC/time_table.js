const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const timeSchema = new Schema({
  usern: { type: String, unique: false, required: false },
  day_slot_time: { type: String, unique: false, required: false },
  order: { type: String, unique: false, required: false },
  selected: { type: String, unique: false, required: false },
  freefield: { type: String, unique: false, required: false },
  freeslot: { type: String, unique: false, required: false },
  compensation_status: { type: String, unique: false, required: false },
  c_cancelled: { type: String, unique: false, required: false },
  problem_statement: { type: String, unique: false, required: false },
  covered: { type: String, unique: false, required: false },

})
const Time_table = mongoose.model('alloted_class_db', timeSchema)
module.exports = Time_table
