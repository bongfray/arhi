const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const express = require('express')
const route = express.Router()
const bcrypt = require('bcryptjs');
const Suser = require('../database/models/STUD/suser')
const passport = require('../passport/stud_pass')
const crypto = require('crypto')
require("datejs")
var nodemailer = require('nodemailer')

console.log("Hi Student Here")

route.post('/ssignup', (req, res) => {
console.log("Stud REG");
    const { regid, password,name,mailid,phone,campus,dept,dob,count,degree,st_year,fn_year} = req.body
    Suser.findOne({ username: regid }, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          console.log("Way to signup")
            const newUser = new Suser({
              username: regid,
              password: password,
              name: name,
              mailid: mailid,
              phone: phone,
              campus: campus,
              dept: dept,
              dob: dob,
              degree: degree,
              st_year: st_year,
              fn_year: fn_year,
              resetPasswordExpires:'',
              resetPasswordToken:'',
              count: count,
            })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})


route.post(
    '/slogin',(req, res, next) => {

console.log("In Student dB");
      const { username, password} = req.body;


      Suser.findOne({ username: username }, function(err, objs){
        if(err)
        {
          return;
        }
        else if(objs)
        {
         if (objs.count === "0")
          {
              var counter = 0;
              Suser.updateOne({ username: username }, {count: counter+1},(err, user) => {

              })

          }
          else if (objs.count === "1")
          {
              var coun = 0;
              Suser.updateOne({ username: username }, {count: coun+2},(err, user) => {

              })
          }
        }
      });
        next()

    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            count: req.user.count,
        };
        res.send(userInfo);
    },
)


route.get('/getstudent', (req, res, next) => {
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})



route.get('/', (req, res, next) => {
  console.log("Hello From Student"+req.user);
    if (req.user) {
      res.json({user: req.user})
    } else {
        res.json({ user: null })
    }
})



module.exports = route
