import React, { Component } from 'react';
import Slogo from './Slogo.png'

class Navbar extends Component{
    render(){
        return(
            <nav>
                <div className="nav-wrapper blue-grey darken-2">
                    <div className="brand-logo"><img src={Slogo} className="srm" alt="SRM Institute of Science and Technology" title="SRM Institute of Science and Technology"/></div>
                    <div className="nav-wrapper center nav-cen">SRM Centre for Applied Research in Education</div>
                </div>
            </nav>
        )
    }
}

export default Navbar;