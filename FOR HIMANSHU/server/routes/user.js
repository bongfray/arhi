const express = require('express')
const router = express.Router()
const User = require('../database/models/FAC/user')
const bcrypt = require('bcryptjs');
const Sess =  require('../database/models/FAC/sess')


console.log('Connected to Server !!')


router.get('/get', (req, res) => {
  console.log(req.session.username)
  sess =req.session;
  if(sess.username)
  {
    res.send(req.session);
  }
  else{
    res.send('no')
  }
})

router.post('/logout', (req, res) => {
  req.session.destroy((err)=>{
    if(err)
    {
      res.send('login error!!')
    }
    else{
      res.send('ok')
    }
  })
})

router.post(
    '/faclogin',(req, res) => {
      const { username} = req.body;
      User.findOne({ username: username }, function(err, objs){
        if(err)
        {
          // console.log(err)
        }
        else if(!objs)
        {
          res.send('U P')
        }
        else if(objs){
          if(bcrypt.compareSync(req.body.password, objs.password)) 
          {
            sess = req.session;
            sess.username = req.body.username;
            sess.password = objs.password;
            res.send('ok')
          }
          else{
            res.send('P P')
          }
        }
      });

    }
)


router.post('/', (req, res) => {
    User.findOne({username: req.body.username}, (err, user) => {
        if (err)
        {
            //console.log('User.js post error: ', err)
        }
        else if (user)
        {
          var emsg = {
            emsg: "User already taken",
          };
            res.send(emsg);
        }
        else {
          const newUser = new User({
              username: req.body.username,
              password: req.body.password,
              })
            newUser.save((err, savedUser) => {
              var succ= {
                succ: "Successful SignedUP"
              };
              res.send(succ);
            })
        }
    })
})



module.exports = router
