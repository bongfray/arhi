import React, { Component } from 'react'
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
import {} from 'materialize-css'

var empty = require('is-empty');

export default class Signup extends Component {
	constructor() {
    super()
    this.initialState = {
			redirectTo: null,
			username: '',
			password: '',
			cnf_pswd: '',
	}
	 this.state = this.initialState;
    this.componentDidMount = this.componentDidMount.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    }
		componentDidMount(){
			
		}




		handleField = (e) =>{
				this.setState({
					[e.target.name] : e.target.value,
				})
		}
	handleSubmit(event) {
		event.preventDefault()
		if(!(this.state.username) && !(this.state.password))
		{
			window.M.toast({html: 'Enter all the details !!', outDuration:'4000', classes:'rounded red'});
		}
		else if(this.state.password !== this.state.cnf_pswd)
		{
			window.M.toast({html: 'Password Does not Match !!', outDuration:'4000', classes:'rounded pink'});
		}
		else{
		window.M.toast({html: 'Signing UP....',outDuration:'3000', classes:'rounded violet'});
		axios.post('/user/', {
			username: this.state.username,
			password: this.state.password,
		})
			.then(response => {
				if(response.status===200){
					if(response.data.emsg)
					{
					window.M.toast({html: response.data.emsg, outDuration:'4000', classes:'rounded #ba68c8 purple lighten-2'});
				    }
					else if(response.data.succ)
					{
						window.M.toast({html: response.data.succ, outDuration:'2000', classes:'rounded #ba68c8 purple lighten-2'});
						this.setState({
								redirectTo: '/'
						})
					}
				}
			}).catch(error => {
				window.M.toast({html: 'Internal Error',outDuration:'2000', classes:'rounded red'});
			})
			
		}
}
render() {
	return (
		<div className="row">
		<div className="col s2 l4 m2 xl4" />
		<div className="col l4 xl4 s12 m12 form-signup">
				<div className="center">
						<h5 className="reg">SIGN UP</h5>
				</div>
				<form className="row form-con">
						<div className="input-field row">
								<div className="input-field col s6 l12 xl12 m6">
								<input id="fac_id" name="username" type="text" className="validate" value={this.state.username} onChange={this.handleField} required />
								<span className="helper-text" data-error="Please enter data!!" data-success=""></span>
								<label htmlFor="fac_id">Username</label>
								</div>

						</div>
						<div className="input-field row">

								<div className="input-field col s6 xl12 l12 m6">
									<input  onChange={this.handleField}  name="password" id="pswd" value={this.state.password} type="password" className="validate" required />
									<label htmlFor="pswd">Password</label>
								</div>
								</div>
								<div className="input-field row">
								<div className="input-field col s6 xl12 m6 l12">
									<input  onChange={this.handleField} id="cnf_pswd" name="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
									<label htmlFor="cnf_pswd">Confirm Password</label>
									<span className="helper-text" data-error="Please enter data!!" data-success=""></span>
								</div>

						</div>

						<br/>
						<div className="row">
							<button className="blue waves-effect btn" style={{width:'100%'}} onClick={this.handleSubmit}>Submit</button><br /><br />
							<Link to="/" className="btn pink" style={{width:'100%'}}>BACK TO LOGIN</Link>
						</div>
				</form>
		</div>
		</div>

	);
						}

}
