import React, { Component } from 'react';
import M from 'materialize-css';
import {} from 'materialize-css'
import { render } from "react-dom";
import { css } from '@emotion/core';
import Nav from '../dynnav'
import Daa from './DAA'

// Can be a string as well. Need to ensure each key-value pair ends with ;
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./AnimatedProgressProvider";
import ChangingProgressProvider from "./ChangingProgressProvider";
import RadialSeparators from "./RadialSeparators";

const academics = 66;
const research = 76;
const administrative = 55;
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
export default class Dash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/newd',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],

    }
  }

render(){
  return (
    <React.Fragment>
    <Nav content={this.state.content} get={this.state.get} logout={this.state.logout}/>
    <div className="row">
    <div className="col l4">
    <Example label="Academics">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={academics}
        duration={1.4}
        easingFunction={easeQuadInOut}

        // repeat
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              /* This is important to include, because if you're fully managing the
        animation yourself, you'll want to disable the CSS animation. */
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    <div className="col l4">
    <Example label="Administrative">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={administrative}
        duration={1.4}
        easingFunction={easeQuadInOut}

        // repeat
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              /* This is important to include, because if you're fully managing the
        animation yourself, you'll want to disable the CSS animation. */
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>

    <div className="col l4">
    <Example label="Research">
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={research}
        duration={1.4}
        easingFunction={easeQuadInOut}

        // repeat
      >
        {value => {
          const roundedValue = Math.round(value);
          return (
            <CircularProgressbar
              value={value}
              text={`${roundedValue}%`}
              /* This is important to include, because if you're fully managing the
        animation yourself, you'll want to disable the CSS animation. */
              styles={buildStyles({ pathTransition: "none" })}
            />
          );
        }}
      </AnimatedProgressProvider>
    </Example>
    </div>
    </div>
    <div>
     <Daa />
    </div>
    </React.Fragment>
    );
    }

  }
  function Example(props) {
  return (
  <div className="card hoverable" style={{paddingTop: '10px', paddingBottom:'10px'}}>
      <div style={{marginLeft:'26%',width:'200px'}}>{props.children}</div>
  </div>
  );
  }
