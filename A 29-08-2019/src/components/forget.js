import React, { Component } from 'react'
import {  Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom';
import axios from 'axios'
import M from 'materialize-css';
import { } from 'materialize-css';
import Loader from './loader'
import Loader2 from './loader2'
import './style.css'
var empty = require('is-empty');


class Modal extends Component {
  constructor() {
      super()
      this.state = {
          mailid: '',
          redirectTo: null,
          sending: '',
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleForgo= this.handleForgo.bind(this)

  }
  handleForgo(e) {
      this.setState({
          mailid: e.target.value
      })
  }
  handleSubmit(event) {
      event.preventDefault()
      if(empty(this.state.mailid)){
        window.M.toast({html: 'Mailid Cannot Be Empty',classes:'rounded #f44336 red'});
        return false;
      }
      else{
      axios.post('/user/forgo', {
              mailid: this.state.mailid,
          },
          this.setState({
            sending: 'Sending Mail .....',
          })
        )

          .then(response => {
              console.log(response)
              if (response.status === 200) {
                      if(response.data.success)
                        {
                          this.setState({
                           loading: false,
                           redirectTo: '/'
                          })
                         window.M.toast({html: 'Check your mail to reset password !!',outDuration:'9000', classes:'rounded #ffeb3b yellow black-text text-darken-2'});
                        }
                      else if(response.data.nodata)
                        {
                          alert(response.data.nodata);
                          window.location.assign('/');
                        }
              }

          }).catch(error => {
            window.M.toast({html: 'Check Whether this mail is exist or not !!!'});
            window.location.assign('/');

          })
        }
  }
  componentDidMount() {
    const options ={
      inDuration: 250,
      outDuration: 250,
      opacity: 0.5,
      dismissable: false,
      startingTop: "94%",
      endingTop: "%"
    };
    M.Modal.init(this.Modal,options);
}
    render()
    {
      if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    } else {
        return(
          <div>
          <a className="col s6 offset-s3 but N/A transparent modal-trigger go" data-target="modal1">
          Forget password?
          </a>
          <div ref={Modal => {
            this.Modal =Modal;
          }}
           id="modal1" className="modal">
      <div className="modal-content">
      <h4 className="forgohead">Forget Password</h4>
      <p className="ppo">Please enter your email id :</p>
      <div className="row">
      <div className="input-field col s12">
        <input id="mail" type="email" value={this.state.mailid} onChange={this.handleForgo} className="validate" required="true"/>
        <label htmlFor="mail">Mail Id</label>
      </div>
      <Link to="#" className="waves-effect btn blue-grey darken-2 col s4 sup right" onClick={this.handleSubmit}>Send Mail</Link>
      </div>
      <br />
      <div className="center sup" style={{fontSize:'20px',color:'purple'}}>{this.state.sending}</div>
     </div>
     <div className="modal-footer">
      <a className="modal-close waves-effect waves-green btn-flat">Close</a>
      </div>
     </div>
     </div>
        )
    }
    }
}

export default Modal;
