import React, { Component } from 'react';
import { Route} from 'react-router-dom'
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';


import Signup from './components/FACULTY_FRONT/sign-up'
import Login from './components/FACULTY_FRONT/login-form'

class App extends Component {

  constructor() {
    super()

    this.state = {
      loggedIn: false,
      username: '',
      modal: false,

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }




  componentDidMount() {
  }




  render() {
    return (
       <div className="App">

        <Route exact path="/" component={Login} />

        <Route path="/signup" render={() =>
          <Signup
         />}
       />
      </div>
    );
  }
}

export default App;
