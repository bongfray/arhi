import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import axios from 'axios'
import M from 'materialize-css';
import { } from 'materialize-css';

var empty = require('is-empty');



class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            error:'',
            username: '',
            password: '',
            redir: '',
            redirectTo: null,
        }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUser= this.handleUser.bind(this)
        this.handlePass= this.handlePass.bind(this)

    }

    handleUser(e) {
        this.setState({
            username: e.target.value
        })
    }
    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        if(empty(this.state.username)||empty(this.state.password)){
          window.M.toast({html: 'First Enter Details!!',outDuration:'1000',activationPercent:0.999, classes:'rounded #f44336 red'});
          return false;
        }
        else{
        axios.post('/user/faclogin', {
                username: this.state.username,
                password: this.state.password
            })
            .then(response => {
                if (response.status === 200) {
                  if(response.data === 'block')
                  {
                    window.M.toast({html: 'Secondary User Not Allowed !!',outDuration:'1000',activationPercent:0.999, classes:'rounded red darken-2'});
                  }
                    else if(response.data === 'U P')
                    {
                        window.M.toast({html: 'User Doesnot Match!!',outDuration:'1000',activationPercent:0.999, classes:'rounded red darken-2'});

                    }
                    else if(response.data === 'P P')
                    {
                        window.M.toast({html: 'Password Doesnot Match!!',outDuration:'1000',activationPercent:0.999, classes:'rounded red darken-2'});
                    }
                    else{
                        this.setState({user:response.data})
                        window.M.toast({html: 'Success!!',outDuration:'1000',activationPercent:0.999, classes:'rounded green darken-2'});
                    }
     
                }
            }).catch(error => {
                window.M.toast({html: 'WRONG CREDENTIALS!!',outDuration:'1000',activationPercent:0.999, classes:'rounded #f44336 red'});
            })
            this.setState({
               username: '',
               password: '',
             })

          }
    }






        componentDidMount() {
            M.AutoInit();
            this.fetchUser();
        }
        fetchUser =()=>{
            axios.get('/user/get')
            .then(res=>{
                if(res.data === 'no')
                {
                    this.setState({user:null})
                
               }
               else{
                this.setState({user:res.data.username})
               }
            })
        }

        Welcome =() =>{
          this.setState({
            display_nav:!this.state.display_nav,
            display_welcomenote:!this.state.display_welcomenote,
          })
        }
        logout = () =>{
            axios.post('/user/logout')
            .then(res=>{
                    if(res.data === 'ok')
                    {
                        this.setState({user:null})
                    }
                   
                
            })
        }
    render() {
        if(this.state.user)
        {
            return(
                <div>
        <h4 className="center">LOGGED {this.state.user}</h4>
                    <button onClick={this.logout} className="btn">LOG OUT</button>
                </div>
            )
        }
        else{
            return (
              <React.Fragment>
              <div className="row">

             <div className="col l4 xl4">
             </div>

             <div className="col l4 s12 m12 form-signup notinoti">
                 <div className="ew center">
                     <h5 className="highlight black-text">LOG IN</h5>
                 </div>
                 <form className="form-con">
                     <div className="row">
                     <div className="input-field col s12">
                         <input id="email" type="text" className="validate" value={this.state.username} onChange={this.handleUser} required />
                         <label htmlFor="email">Username</label>
                         <span className="helper-text" data-error="Please enter username !!" data-success=""></span>
                     </div>
                     <div className="input-field col s12">
                         <input id="password" type="password" className="validate" value={this.state.password} onChange={this.handlePass}required/>
                         <label htmlFor="password">Password</label>
                         <span className="helper-text" data-error="Please enter password !!" data-success=""></span>
                     </div>
                     </div>
                     <div className="row">
                     <button style={{width:'100%'}} className="btn pink" onClick={this.handleSubmit}>Login</button>
                     </div>
                     <hr/>
                    <Link to="/signup" style={{width:'100%'}} className="blue btn white-text">Register</Link>
                     <div className="row">
                     </div>
                 </form>
             </div>

             <div className="col s4">
             </div>

             </div>
             </React.Fragment>
           );
            }
         

    }
}

export default LoginForm
