const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs');
mongoose.promise = Promise

//Schema
const dayorder = new Schema({
  day_order: { type: Number, unique: false, required: false },
  day_order_status: { type: String, unique: false, required: false },
  week: { type: Number, unique: false, required: false },
  time: { type: Number, unique: false, required: false },
  day: { type: String, unique: false, required: false },
})


const DayOrder = mongoose.model('common_dayorder', dayorder)
module.exports = DayOrder
