const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const uri = 'mongodb://localhost:27017/eWork_stud'

mongoose.connect(uri,{ useNewUrlParser: true }).then(
    () => {

        console.log('Connected Student DB');

    },
    err => {

         console.log('error connecting to Mongo: ')
         console.log(err);

        }
  );
module.exports = mongoose.connection
