 import React, { Component } from 'react'
 import axios from 'axios'
 import {Link } from 'react-router-dom'
 import '../style.css'
 import {Collapsible, CollapsibleItem} from 'react-materialize'
 import M from 'materialize-css'
import Nav from '../dynnav'
import Loader from '../loader'

 var empty = require('is-empty');

export default class ActualDash extends Component {

  constructor(props) {
		super(props);
    this.state = {
      loading: true,
      sending:'SUBMIT',
      id:'',
      up_username:'',
      up_name:'',
      up_phone:'',
      campus:'',
      dept:'',
      desgn:'',
      up_dob: '',
      new_password:'',
      current_password:'',
      up_confirm_password:'',
      home:'/faculty',
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/fprofile',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
    }
      this.componentDidMount = this.componentDidMount.bind(this)
      this.handleSub = this.handleSub.bind(this)
      this.handleNewP = this.handleNewP.bind(this)
      this.handleConfirmPassword = this.handleConfirmPassword.bind(this)
      this.handleCurrentPass = this.handleCurrentPass.bind(this)
	}
  componentDidMount() {
    axios.get('/user/dash2',
    this.setState({
      loading: true,
    })
  )
    .then(response => {
      if(response.status === 200)
      {
        this.setState({
          loading:false,
        })
        if(response.data.nologin)
        {
          window.M.toast({html: 'You are not loggedIn',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
        else
        {
        this.setState({
          id: response.data.id,
          up_username: response.data.username,
          up_name: response.data.name,
          up_phone: response.data.phone,
          dept: response.data.dept,
          campus: response.data.campus,
          desgn: response.data.desgn,
          up_dob: response.data.dob
        })
      }
      }
    })

    M.AutoInit()
}
handleSub(event) {
  event.preventDefault()
  if((this.state.up_confirm_password) || (this.state.new_password) || (this.state.current_password))
  {
    if((this.state.up_confirm_password) && (this.state.new_password) && (this.state.current_password))
    {
      if(this.state.up_confirm_password === this.state.new_password)
      {
        if((this.state.up_phone).length!==10){
          window.M.toast({html: 'Phone no is not of 10 digit !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
        }
        else{
    // window.M.toast({html: 'Procc', outDuration:'850', inDuration:'800', displayLength:'1500'});
   axios.post('/user/newd', {
     id: this.state.id,
     up_username: this.state.up_username,
     up_name: this.state.up_name,
     up_phone: this.state.up_phone,
     up_dob: this.state.up_dob,
     new_password: this.state.new_password,
     current_password: this.state.current_password,
   },this.setState({
     sending: 'UPDATING...',
   }))
    .then(response => {
      console.log(response)
      if(response.status===200){
        if(response.data.succ)
         {
          window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded #ec407a green lighten-1'});
           this.setState({
              redirectTo: '/newd'
          })
        }
        else if(response.data.fail)
        {
          window.M.toast({html: 'Kindly Match Your Current Password', outDuration:'4200', inDuration:'500', displayLength:'1500'});
          return false;
        }
      }
     }).catch(error => {
       window.M.toast({html: 'Error !! Check if you are loggedIn or not !!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
     })
    this.setState({
      up_name: this.state.up_name,
      id: this.state.id,
      up_username: this.state.up_username,
      up_phone: this.state.up_phone,
      campus: this.state.campus,
      dept: this.state.dept,
      up_dob:this.state.up_dob,
      desgn: this.state.desgn,
      up_confirm_password: '',
      current_password: '',
      new_password: '',
      })
    }
    }else{
      window.M.toast({html: 'New Password Does not Match', outDuration:'3850', inDuration:'900', displayLength:'1500'});
      this.setState({
        up_confirm_password: '',
        new_password: '',
        })
      return false;

    }
  }
    else{
      window.M.toast({html: 'Fill all the fields', outDuration:'1850', inDuration:'900', displayLength:'1500'});
      return false;
    }
  }
  else
  {
    if((this.state.up_phone).length!==10){
      window.M.toast({html: 'Phone no is not of 10 digit!!',outDuration:'9000', classes:'rounded #ec407a pink lighten-1'});
    }
    else{
    axios.post('/user/newd', {
      id: this.state.id,
      up_username: this.state.up_username,
      up_name: this.state.up_name,
      up_phone: this.state.up_phone,
      up_dob: this.state.up_dob,
      new_password: this.state.new_password,
      up_confirm_password: this.state.up_confirm_password,
      current_password: this.state.current_password
    })
     .then(response => {
       console.log(response)
       if(response.status===200){
         if(response.data.succ)
          {
           window.M.toast({html: response.data.succ,outDuration:'9000', classes:'rounded #ec407a green lighten-1'});
            this.setState({
               redirectTo: '/newd'
           })
         }
       }
      }).catch(error => {
        window.M.toast({html: 'Internal Error', outDuration:'1200', inDuration:'1200', displayLength:'1500'});
      })
     this.setState({
      up_name: this.state.up_name,
      id: this.state.id,
      up_username: this.state.up_username,
      up_phone: this.state.up_phone,
      campus: this.state.campus,
      dept: this.state.dept,
      up_dob:this.state.up_dob,
      desgn: this.state.desgn,
      up_confirm_password: '',
      current_password: '',
      new_password: '',
       })
     }
}
}
handleNewP= (e) => {
        this.setState({
            new_password: e.target.value
        });

    };
    handleConfirmPassword= (e) =>{
            this.setState({
                up_confirm_password: e.target.value
            });
            // alert(this.state.up_confirm_password);
        };
        handleCurrentPass = (e) =>{
                this.setState({
                    current_password: e.target.value
                });
            };


  render() {
    if(this.state.loading === true)
    {
      return(
        <Loader />
      );
    }
    else if(this.state.loading === false){
		return (
      <React.Fragment>
      <Nav home={this.state.home} content={this.state.content} get={this.state.get} logout={this.state.logout}/>
      <div className="profile-root">
              <div className="hoverable card">
              <div className="">
                <Link to="/"><h5 className="prof-heading center black-text">PROFILE DATA</h5></Link>
              </div><br />
                    <div>
                     {this.renderCells()}
                    </div>
              </div>
      </div>
      </React.Fragment>
      );
    }
  }

  renderCells() {
    return(
      <div className="profile">

        <div name="phone" className="row">
            <div className="col l6 s12">
            <div className="row">
              <div className="col l4 m6 s6">
              <span className="tcr">Official ID</span>
              </div>
              <div className="col l8">
              <Cell value={ this.state.id }  />
              </div>
            </div>
            </div>
             <div className="col l6">
             <div className="row">
                 <div className="col l4">
                   <span className="tcr">Official Mail:</span>
                 </div>
                 <div className="col l8">
                   <Cell value={ this.state.up_username } name="mail_id" onChange={value => this.setState({up_username: value})} />
                 </div>
             </div>
            </div>
         </div>

        <br />



        <div className="row">
            <div className="col l6">
            <div className="row">
              <div className="col l4">
              <span className="tcr">Name:</span>
              </div>
              <div className="col l8">
              <Cell value={ this.state.up_name } name="name"  onChange={value => this.setState({up_name: value})} />
              </div>
            </div>
            </div>
             <div className="col l6">
             <div className="row">
                 <div className="col l4">
                   <span className="tcr">Mobile No:</span>
                 </div>
                 <div className="col l8">
                   <Cell value={ this.state.up_phone } name="phone" onChange={value => this.setState({up_phone: value})} />
                 </div>
             </div>
            </div>
         </div>

         <br />

         <div className="row">
             <div className="col l6">
             <div className="row">
               <div className="col l4">
               <span className="tcr">Campus: </span>
               </div>
               <div className="col l8">
               <Cell value={ this.state.campus }  onChange={value => this.setState({campus: value})} />
               </div>
             </div>
             </div>
              <div className="col l6">
              <div className="row">
                  <div className="col l4">
                    <span className="tcr">Designation: </span>
                  </div>
                  <div className="col l8">
                    <Cell value={ this.state.desgn } onChange={value => this.setState({desgn: value})} />
                  </div>
              </div>
             </div>
          </div>

          <br />
          <div className="row">
              <div className="col l6">
              <div className="row">
                <div className="col l4">
                <span className="tcr">Department:</span>
                </div>
                <div className="col l8">
                <Cell value={ this.state.dept }  onChange={value => this.setState({dept: value})} />
                </div>
              </div>
              </div>
               <div className="col l6">
               <div className="row">
                   <div className="col l4">
                     <span className="tcr">DOB:</span>
                   </div>
                   <div className="col l8">
                     <Cell value={ this.state.up_dob } name="phone" onChange={value => this.setState({up_dob: value})} />
                   </div>
               </div>
              </div>
           </div>
           <br />
           <div className="row div-reset">
               <div className="input-field col l4">
                 <input id="curpass" type="password" className="validate" value={this.state.current_password} onChange={this.handleCurrentPass} required />
                 <label htmlFor="curpass">Current Password</label>
               </div>
                <div className="input-field col l4">
                    <input id="newpass" type="password" className="validate" value={this.state.new_password} onChange={this.handleNewP} required />
                    <label htmlFor="newpass">New Password</label>
               </div>
               <div className="input-field col l4">
                   <input id="conpass" type="password" className="validate" value={this.state.up_confirm_password} onChange={this.handleConfirmPassword} required />
                   <label htmlFor="conpass">Confirm Password</label>
              </div>
            </div>







<div className="row">

      <button className="waves-effect btn blue-grey darken-2 col s3 prof-submit right sup" onClick={this.handleSub}>{this.state.sending}</button>
      </div>
      </div>
		);

	}

	componentDidUpdate() {
		const { description, hours, rate, amount } = this.state;
		//console.log(`New State: ${description} - ${hours} - ${rate} - ${amount}`);
	}
}

class Cell extends Component {

	constructor(props) {
		super(props);
		this.state = { editing: false };
	}

	render() {
    const name = this.props.name;
		const { value, onChange } = this.props;
    if(this.state.editing)
    {
      return(
        <div>
        <div className="col l8"><input className="values-to-show" ref='input' value={value} onChange={e => onChange(e.target.value)} onBlur={ e => this.onBlur()} /></div>
        <span><a className="btn left waves-effect btn  editable-save pink" href="#" onBlur={ e => this.onBlur()}>Save</a></span>
        </div>

      );
    }
    else{
      if((name === "phone") || (name=== "name") || (name==="mail_id"))
      {
      return(
        <div>
        <div className="col l8">
        <span className="values-to-show">{value}</span>
        </div>
        <div className="col l4">
        <a href="#" className="editable-edit waves-effect btn blue-grey darken-2 " onClick={() => this.onFocus()}>Edit</a>
        </div>
        </div>
      );
    }
    else{
      return(
        <div>
        <div className="col l6">
        <span className="values-to-show">{value}</span>
        </div>
        </div>
      );
    }
    }


	}

	onFocus() {
		this.setState({ editing: true }, () => this.refs.input.focus());
	}

	onBlur() {
		this.setState({ editing: false });
	}
}


{/*---------------------------------------------------------****Profile */}
