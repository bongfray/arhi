import React, { Component } from "react";
import axios from "axios";
import { Link, Redirect} from "react-router-dom";
import M from "materialize-css";
import Nav from '../dynnav'
import "../style.css";
var empty = require("is-empty");

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      redir: "",
      id: "",
      username: "",
      ug_clg_name: "",
      pg_clg_name:"",
      phd_clg_name: "",
      ug_start_year:"",
      pg_start_year:"",
      phd_start_year:"",
      ug_end_year:"",
      pg_end_year:"",
      phd_end_year:"",
      marks_ug:"",
      marks_pg:"",
      marks_phd:"",
      other_degree:"",
      other_degree_info:"",
      certificate:"",
      honors_awards:"",
      redirectTo: null,
      logout:'/user/logout',
      get:'/user/',
      content:[
        {
          val:'Profile',
          link:'/newd',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Sections',
          link:'/partA',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
    };
    this.getProf =this.getProf.bind(this);
    this.handleId = this.handleId.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUGMarks = this.handleUGMarks.bind(this);
    this.handlePGMarks = this.handlePGMarks.bind(this);
    this.handlePHDMarks = this.handlePHDMarks.bind(this);
    this.handleUGStYear = this.handleUGStYear.bind(this);
    this.handleUGFnYear = this.handleUGFnYear.bind(this);
    this.handlePHDFnYear = this.handlePHDFnYear.bind(this);
    this.handlePHDStYear = this.handlePHDStYear.bind(this);
    this.handlePGStYear = this.handlePGStYear.bind(this);
    this.handlePGFnYear = this.handlePGFnYear.bind(this);
    this.handleUGClgn = this.handleUGClgn.bind(this);
    this.handlePGClgn = this.handlePGClgn.bind(this);
    this.handlePHDClgn = this.handlePHDClgn.bind(this);
    this.handleOtherDegree =this.handleOtherDegree.bind(this);
    this.handleOtrDeg_info =this.handleOtrDeg_info.bind(this);
    this.handleCerti =this.handleCerti.bind(this);
    this.handleAwards =this.handleAwards.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this)
  }



  handleId = e => {
    this.setState({
      id: e.target.value
    });
  };
  handleEmail = e => {
    this.setState({
      username: e.target.value
    });
  };
  handleUGClgn = e => {
    this.setState({
      ug_clg_name: e.target.value
    });
  };
  handleUGStYear = e => {
    this.setState({
      ug_start_year: e.target.value
    });
  };
  handleUGFnYear = e => {
    this.setState({
      ug_end_year: e.target.value
    });
  };
  handleUGMarks = e => {
    this.setState({
      marks_ug: e.target.value
    });
  };
  handlePGClgn = e => {
    this.setState({
      pg_clg_name: e.target.value
    });
  };
  handlePGStYear = e => {
    this.setState({
      pg_start_year: e.target.value
    });
  };
  handlePGFnYear = e => {
    this.setState({
      pg_end_year: e.target.value
    });
  };
  handlePGMarks = e => {
    this.setState({
      marks_pg: e.target.value
    });
  };
  handlePHDClgn = e => {
    this.setState({
      phd_clg_name: e.target.value
    });
  };
  handlePHDStYear = e => {
    this.setState({
      phd_start_year: e.target.value
    });
  };
  handlePHDFnYear = e => {
    this.setState({
      phd_end_year: e.target.value
    });
  };
  handlePHDMarks = e => {
    this.setState({
      marks_phd: e.target.value
    });
  };
  handleOtherDegree = e => {
    this.setState({
      other_degree: e.target.value
    });
  };
  handleOtrDeg_info = e => {
    this.setState({
      other_degree_info: e.target.value
    });
  };
  handleCerti= e => {
    this.setState({
      certificate: e.target.value
    });
  };
  handleAwards= e => {
    this.setState({
      honors_awards: e.target.value
    });
  };
  handleSubmit(event) {

    event.preventDefault();
    if (
      empty(this.state.pg_clg_name) ||
      empty(this.state.ug_clg_name) ||
      empty(this.state.phd_clg_name) ||
      empty(this.state.ug_start_year) ||
      empty(this.state.pg_start_year) ||
      empty(this.state.phd_start_year) ||
      empty(this.state.ug_end_year) ||
      empty(this.state.pg_end_year) ||
      empty(this.state.phd_end_year) ||
      empty(this.state.marks_pg) ||
      empty(this.state.marks_ug) ||
      empty(this.state.marks_phd) ||
      empty(this.state.id) ||
      empty(this.state.username) ||
      empty(this.state.other_degree) ||
      empty(this.state.other_degree_info) ||
      empty(this.state.certificate) ||
      empty(this.state.honors_awards)
    ) {
      window.M.toast({
        html: "Please fill up all the Datas to proceed forward",
        outDuration: "3850",
        inDuration: "400",
        displayLength: "1500"
      });

      return false;
    }else {
      axios.post('/user/profile1', {
  			username: this.state.username,
        id: this.state.id,
        ug_clg_name:this.state.ug_clg_name,
        pg_clg_name:this.state.pg_clg_name,
        phd_clg_name:this.state.phd_clg_name,
        ug_start_year:this.state.ug_start_year,
        pg_start_year:this.state.pg_start_year,
        phd_start_year:this.state.phd_start_year,
        ug_end_year:this.state.ug_end_year,
        pg_end_year:this.state.pg_end_year,
        phd_end_year:this.state.phd_end_year,
        marks_pg: this.state.marks_pg,
        marks_ug: this.state.marks_ug,
        marks_phd: this.state.marks_phd,
        other_degree:this.state.other_degree,
        other_degree_info: this.state.other_degree_info,
        certificate:this.state.certificate,
        honors_awards:this.state.honors_awards
  		})
  			.then(response => {
  				console.log(response)
  				if(response.status===200){
  					if(response.data.msg)
  					{
              this.setState({
                  redirectTo: '/'
              })
  					window.M.toast({html: 'Datas are already saved!!',outDuration:'1000', classes:'rounded #f44336 red'});
  				  }
  					else if(response.data.succ)
  					{
  						window.M.toast({html: 'Successfully Submitted !!',outDuration:'1000', classes:'rounded green'});
              this.setState({
                  redirectTo: '/partA'
              })
  					}
  				}
  			}).catch(error => {
  				window.M.toast({html: 'Kindly Priovide Correct Mail Id & Id ', outDuration:'6200', inDuration:'1200', displayLength:'1500'});
  			})
  			this.setState({
          username:'',
          id:'',
          ug_clg_name:'',
          pg_clg_name:'',
          phd_clg_name:'',
          ug_start_year:'',
          pg_start_year:'',
          phd_start_year:'',
          ug_end_year:'',
          pg_end_year:'',
          phd_end_year:'',
          marks_pg: '',
          marks_ug:'',
          marks_phd: '',
          other_degree:'',
          other_degree_info: '',
          certificate:'',
          honors_awards:''
  		})
    }
  }
  notifi = () =>
    window.M.toast({
      html: "Enter Details",
      outDuration: "850",
      inDuration: "800",
      displayLength: "1500"
    });

  componentDidMount() {
    this.notifi();
    M.AutoInit();
    this.getProf();
  }

  getProf(){
    axios.get('/user/login').then(response => {
      if(response.status === 200)
      {
        console.log(response.data);
        this.setState({
          redir: response.data
        });
      }
    })
  }

  render() {
    // alert(this.state.redir);

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    return (
      <React.Fragment>
      <Nav content={this.state.content} get={this.state.get} logout={this.state.logout}/>
      <div className="row">
<div className="col s1" />

        <div className="col s12 l10 m12 prof-1-dash">

          <div className="blue-grey darken-2">
            <Link to="/"><h5 className="prof-heading center">PROFILE DATA</h5></Link>
          </div>
          <form className="row form-con">
            <div className="input-field row">
              <div className="input-field col l6 s12 m12">
              <input
                id="email"
                type="email"
                className="validate"
                value={this.state.username}
                onChange={this.handleEmail}
                required
              />
              <label htmlFor="email">Official Email</label>
              </div>

              <div className="input-field col l6 m12 s12">
                <input
                  id="fac_id"
                  type="text"
                  className="validate"
                  value={this.state.id}
                  onChange={this.handleId}
                  required
                />
                <label htmlFor="fac_id">ID</label>
              </div>
            </div>

<br />

            <div className="row">
            <div className=" hide-on-med-and-down col l2 left profinfohead"><b>Personal Info</b></div>
            <div className="col l3 hide-on-med-and-down center profinfohead"><b>College Name</b></div>
            <div className="col l2 hide-on-med-and-down center profinfohead"><b>Year of Starting</b></div>
            <div className="col l3 hide-on-med-and-down center profinfohead"><b>Year of Completion</b></div>
            <div className="col l2 hide-on-med-and-down center profinfohead"><b>Percentage/CGPA</b></div>
            </div>

<br/>


{/*-------------------------------------------------Starting of Ug --------------------------------------------------- */}

            <div className="row">
              <div className="col l2 s12 m12 proftitle">
                U.G Degree:
              </div>
              <div className="col l3 s12 m12">
              <label className="pure-material-textfield-outlined alignfull">
                <textarea
                  className="area"
                  type="text"
                  placeholder=" "
                  min="10"
                  max="60"
                  value={this.state.ug_clg_name}
                  onChange={this.handleUGClgn}
                />
                <span>College Name</span>
              </label>
              </div>

              <div className="col l2 s6 m12 center">
              <label className="pure-material-textfield-outlined alignfull">
                <textarea
                  className="area"
                  type="text"
                  placeholder=" "
                  min="10"
                  max="60"
                  value={this.state.ug_start_year}
                  onChange={this.handleUGStYear}
                />
                <span>Starting Year</span>
              </label>
              </div>

              <div className="col l3 s6 m12">
              <label className="pure-material-textfield-outlined alignfull">
                <textarea
                  className="area"
                  type="text"
                  placeholder=" "
                  min="10"
                  max="60"
                  value={this.state.ug_end_year}
                  onChange={this.handleUGFnYear}
                />
                <span>Completion Year</span>
              </label>
              </div>
              <div className="col l2 s12 m12">
              <label className="pure-material-textfield-outlined alignfull">
                <textarea
                  className="area"
                  type="text"
                  placeholder=" "
                  min="10"
                  max="60"
                  value={this.state.marks_ug}
                  onChange={this.handleUGMarks}
                />
                <span>Result</span>
              </label>
              </div>
        </div>
{/*----------------------End of UG Degree------------------------------------------------------------------------------------- */}

    {/*--------------------------------------------Starting of PG Degree-------------------------------------------------------------*/}

<div className="row">
<div className="col l2 proftitle">
  P.G Degree:
</div>
<div className="col l3 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.pg_clg_name}
    onChange={this.handlePGClgn}
  />
  <span>Enter your College Name</span>
</label>
</div>

<div className="col l2 s6 m12 center">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.pg_start_year}
    onChange={this.handlePGStYear}
  />
  <span>Starting Year</span>
</label>
</div>

<div className="col l3 s6 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.pg_end_year}
    onChange={this.handlePGFnYear}
  />
  <span>Completion Year</span>
</label>
</div>
<div className="col l2 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.marks_pg}
    onChange={this.handlePGMarks}
  />
  <span>Result</span>
</label>
</div>
</div>



{/* ---------------------------------------------------Ending of PG Degree-------------------------------------------*/}


{/*------------------------------------------------PHD Degree -----------------------------------------------------  */}


<div className="row">
<div className="col l2 proftitle">
  PHD Degree:
</div>
<div className="col l3 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.phd_clg_name}
    onChange={this.handlePHDClgn}
  />
  <span>Enter your College Name</span>
</label>
</div>

<div className="col l2 s6  m12 center">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.phd_start_year}
    onChange={this.handlePHDStYear}
  />
  <span>Starting Year</span>
</label>
</div>

<div className="col l3 s6 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.phd_end_year}
    onChange={this.handlePHDFnYear}
  />
  <span>Completion Year</span>
</label>
</div>
<div className="col l2 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.marks_phd}
    onChange={this.handlePHDMarks}
  />
  <span>Result</span>
</label>
</div>
</div>

{/*----------------------------------------------------------------------------------------------Ending of PHD  */}

{/* Starting of Others Degree---------------------------------------------------------------------------------------*/}


<div className="row">
<div className="col l2 s12 m12 proftitle">
  Others Degree:
</div>
<div className="col l4 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.other_degree}
    onChange={this.handleOtherDegree}
  />
  <span>Mentions the Degree Name</span>
</label>
</div>

<div className="col l6 s12 m12 center">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.other_degree_info}
    onChange={this.handleOtrDeg_info}
  />
  <span>Fill the whole Degree info</span>
</label>
</div>
</div>
<hr />
<br/>

{/*---------------------------------------------------------------------Ending of Others Degree */}



{/*---------------------------------------------Cerificates Holder--------------------------------------------------------- */}


<div className="row">
<div className="col l2 s12 m12 proftitle">
  Cerificates :
</div>
<div className="col l10 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.certificate}
    onChange={this.handleCerti}
  />
  <span>Input the Certificates one by one</span>
</label>
</div>
</div>

{/*---------------------------------------------------------Ending of Certificate-------------------------- */}


{/* Starting  of Honors & Rewards---------------------------------------------------------------------------*/}

<div className="row">
<div className="col l2 s12 m12 proftitle">
  Honors/Rewards :
</div>
<div className="col l10 s12 m12">
<label className="pure-material-textfield-outlined alignfull">
  <textarea
    className="area"
    type="text"
    placeholder=" "
    min="10"
    max="60"
    value={this.state.honors_awards}
    onChange={this.handleAwards}
  />
  <span>Input the Awards & Honors You Achived</span>
</label>
</div>
</div>
{/*-------------------------------------------------------------End of Awards---------------------- */}

            <div>
              <div className="col s3" />
              <div className="col s5" />
              <Link
                to="#"
                className="waves-effect waves-light btn blue-grey darken-2 col s4 sup"
                onClick={this.handleSubmit}
              >
                Submit
              </Link>
            </div>
          </form>
        </div>
      </div>
      </React.Fragment>
    );
  }

}



}

export default Signup;
