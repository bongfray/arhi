import React, { Component } from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import {Link } from 'react-router-dom'
import Nav from '../dynnav'
import M from 'materialize-css'
import Fhome from '../fhome2.png'

export default class Arhi extends Component{
  constructor()
  {
    super();
    this.state ={
      display:'',
      logout:'/user2/slogout',
      get:'/user2/getstudent',
      home:'/student',
      content:[
        {
          val:'Profile',
          link:'/sprofile',
        },
        {
          val:'DashBoard',
          link:'/dash',
        },
        {
          val:'Resume',
          link:'/resume',
        },
        {
          val:'TimeTable',
          link:'/time_new',
        },
        {
          val:'Master TimeTable',
          link:'/master',
        }
      ],
    };
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    return(
      <React.Fragment>
      <Nav home={this.state.home} content={this.state.content} get={this.state.get} logout={this.state.logout}/>
      <h4 className="center">Welcome !!</h4>
      </React.Fragment>
    )
  }
}
