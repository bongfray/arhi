import React, { Component } from 'react';
import axios from 'axios'
import { Route, Link } from 'react-router-dom'
import NaVM from './nav-modal'
import './style.css'

class Nstart extends Component {

  constructor() {
    super()
    this.state = {
      modal: false,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

openModal =(info) =>
{
    this.setState({modal: !this.state.modal})
}

  componentDidMount() {
    this.openModal();
  }



  render() {
    return (
       <div className="Navigator">

       <NaVM
           displayModal={this.state.modal}
           closeModal={this.openModal}
       />
       </div>
    );
  }
}

export default Nstart;
