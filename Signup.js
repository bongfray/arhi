import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import M from "../node_modules/materialize-css";
import '../node_modules/materialize-css/dist/css/materialize.min.css';
var empty = require('is-empty');

class Signup extends Component{
    state = {
        title: '',
        name: '',
        id: '',
        email: '',
        phone: '',
        pswd: '',
        cnf_pswd: '',
        campus: '',
        dept: '',
        desgn: ''


    }
    handleTitle = (e) => {
        this.setState({
            title: e.target.value
        });
    };
    handleName = (e) => {
        this.setState({
            name: e.target.value
        });
    };
    handleId = (e) => {
        this.setState({
            id: e.target.value
        });
    };
    handleEmail = (e) => {
        this.setState({
            email: e.target.value
        });
    };
    handlePhone = (e) => {
        this.setState({
            phone: e.target.value
        });
    };
    handlePasswordChange = (e) => {
        this.setState({
          pswd: e.target.value
        });
    };
    handleConfirmPassword = (e) => {
          this.setState({
              cnf_pswd: e.target.value
            })
    };
    handleCampus = (e) => {
        this.setState({
            campus: e.target.value
        });
    };
    handleDept = (e) => {
        this.setState({
            dept: e.target.value
        });
    };
    handleDesgn = (e) => {
        this.setState({
            desgn: e.target.value
        });
    };
    handleSubmit = (e) => {
        // const { pswd, cnf_pswd } = this.state;
        e.preventDefault();
        if(empty(this.state.title)||empty(this.state.name)||empty(this.state.id)||empty(this.state.email)||empty(this.state.phone)||empty(this.state.pswd)||empty(this.state.cnf_pswd)||empty(this.state.campus)||empty(this.state.dept)||empty(this.state.desgn))
        {
            window.M.toast({html: 'Enter all the Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
            this.setState({
                cnf_pswd:''
            })
            return false;
        }
        else if(this.state.pswd !==this.state.cnf_pswd){
           window.M.toast({html: 'Password does not match', outDuration:'850', inDuration:'800', displayLength:'1500'});
           this.setState({
               cnf_pswd:''
           })
           return false;
             // The form won't submit
        }
        else {
            
            window.M.toast({html: 'Successful', outDuration:'850', inDuration:'800', displayLength:'1500'});
            return true;
            
        } // The form will submit
        this.setState({
            title: '',
            name: '',
            id: '',
            email: '',
            phone: '',
            pswd: '',
            cnf_pswd: '',
            campus: '',
            dept: '',
            desgn: ''

        })
     }

    notifi = () => window.M.toast({html: 'Enter Details', outDuration:'850', inDuration:'800', displayLength:'1500'});
    
    componentDidMount() {
        this.notifi();
        M.AutoInit();
    }
    render() {

        return(
            <div className="row">

            <div className="col s2">
            </div>

            <div className="col s8 form-signup">
                <div className="ew center">
                    <h5>REGISTRATION</h5>
                </div>
                <form className="row form-con">
                    <div className="input-field row">

                        <div className="input-field col s2">
                            <select value={this.state.title} onChange={this.handleTitle}>
                            <option value="" disabled selected>Title</option>
                            <option value="Mr.">Mr.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Miss.">Miss.</option>
                            <option value="Dr.">Dr.</option>
                            </select>
                        </div>
                        
                        <div className="input-field col s6">
                        <input id="name" type="text" className="validate" value={this.state.name} onChange={this.handleName} required />
                        <label htmlFor="name">Name</label>
                        </div>

                        <div className="input-field col s4">
                        <input id="fac_id" type="text" className="validate" value={this.state.id} onChange={this.handleId} required />
                        <label htmlFor="fac_id">ID</label>
                        </div>

                    </div>

                    <div className="input-field row">

                        <div className="input-field col s8">
                        <input id="email" type="email" className="validate" value={this.state.email} onChange={this.handleEmail} required />
                        <label htmlFor="email">Email</label>
                        </div>

                        <div className="input-field col s4">
                        <input id="ph_num" type="text" className="validate" value={this.state.phone} onChange={this.handlePhone} required />
                        <label htmlFor="ph_num">Phone Number</label>
                        </div>

                    </div>

                    <div className="input-field row">

                        <div className="input-field col s6">
                        <input onChange={this.handlePasswordChange} id="pswd" value={this.state.pswd} type="password" className="validate" required />
                        <label htmlFor="pswd">Password</label>
                        </div>

                        <div className="input-field col s6">
                        <input onChange={this.handleConfirmPassword} id="cnf_pswd" value={this.state.cnf_pswd} type="password" className="validate" required />
                        <label htmlFor="cnf_pswd">Confirm Password</label>
                        </div>

                    </div>
                    <div className="input-field row">
                        
                        <div className="input-field col s4">
                            <select value={this.state.campus} onChange={this.handleCampus}>
                            <option value="" disabled selected>Campus</option>
                            <option value="ktr">Kattankulathur Campus</option>
                            <option value="rpr">Ramapuram Campus</option>
                            <option value="vpl">Vadapalani Campus</option>
                            <option value="ncr">NCR Campus</option>
                            </select>
                        </div>
                        <div className="input-field col s4">
                            <select value={this.state.dept} onChange={this.handleDept}>
                            <option value="" disabled selected>Department</option>
                            <option value="cse">Computer Science</option>
                            <option value="it">Information Technology</option>
                            <option value="swe">Software</option>
                            <option value="mech">Mechanical</option>
                            </select>
                        </div>
                        <div className="input-field col s4">
                            <select value={this.state.desgn} onChange={this.handleDesgn}>
                            <option value="" disabled selected>Designation</option>
                            <option value="hod">HOD</option>
                            <option value="prf">Professor</option>
                            <option value="aprf">Associate Professor</option>
                            <option value="asprf">Assistant Professor</option>
                            
                            </select>
                        </div>


                    </div>
                    <br/>
                    <div>
                    <Link to='/' className="col s5 log"> <b> Login Instead ?</b></Link>
                    
                    <div className="col s3"></div>
                    <a className="waves-effect waves-light btn blue-grey darken-2 col s4" onClick={ this.handleSubmit}>Submit</a>
                    </div>
                </form>
            </div>

            

            </div>
    
        )
    }
}

export default Signup;