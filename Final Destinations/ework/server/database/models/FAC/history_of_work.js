const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.promise = Promise
var autoIncrement = require('mongoose-auto-increment');



const five = new Schema({
week: { type: String, unique: false, required: false },
month: { type: String, unique: false, required: false },
year: { type: String, unique: false, required: false },
update_day: { type: Date ,default : Date.now() },
username: { type: String, unique: false, required: false },
term: { type: String, unique: false, required: false },
action: { type: String, unique: false, required: false },
weekly_percentage:{ type: Number, unique: false, required: false },
monthly_percentage:{ type: Number, unique: false, required: false },
})

five.plugin(autoIncrement.plugin, { model: 'History-Of-Work', field: 'serial', startAt: 1,incrementBy: 1 });



var fivep = mongoose.model('History-Of-Work', five);




module.exports = fivep
