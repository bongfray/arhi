import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import {Grid,Grow} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

export default class Nstart extends Component {
  render() {
    const checked = true;
    return (
      <Grid container>
         <Grid item xs={1} sm={1} lg={3} xl={3} md={2}/>
         <Grid item xs={10} sm={10} lg={6} xl={6} md={8}>
           <Grow
              in={true}
              style={{ transformOrigin: '0 0 0' }}
              {...(checked ? { timeout: 1000 } : {})}
            >
             <Paper elevation={3} style={{padding:'10px',borderRadius:'10px',marginTop:'170px'}}>
              <div>
                <Typography variant="h4" align="center" gutterBottom>
                  eWork
                </Typography><br />
                <Grid container style={{marginBottom:'35px'}}>
                  <Grid item xs={5} sm={5}>
                    <Link to="/ework/flogin" style={{textDecoration:'none'}}>
                        <Paper style={{color:'white',padding:'10px',textAlign:'center',background:'linear-gradient(to bottom left, #ff0066 0%, #00ff99 100%)'}}>
                          Faculty
                        </Paper>
                    </Link>

                  </Grid>
                  <Grid item xs={2} sm={2}/>
                  <Grid item xs={5} sm={5}>
                      <Link to="/ework/slogin" style={{textDecoration:'none'}}>
                        <Paper style={{color:'black',padding:'10px',textAlign:'center',background: 'linear-gradient(to bottom left, #66ffff 0%, #ffff99 100%)'}}>
                          Student
                        </Paper>
                      </Link>
                  </Grid>
                </Grid>
              </div>
            </Paper>
          </Grow>
         </Grid>
         <Grid item xs={1} sm={1} lg={3} xl={3} md={2}/>
      </Grid>
    );

  }
}
