import React, { Component} from 'react';
import axios from 'axios'
import { Link, Redirect} from 'react-router-dom';
import Nav from '../../dynnav'
import ContentLoader from "react-content-loader"
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export default class BluePrint extends Component {
	constructor() {
    super();
    this.state = {
			loading: true,
			datas: [
				{
					day_order:'Day Order 1',dayor:'1',
					day:[
						{id:"1.1"},{id:"1.2"},{id:"1.3"},{id:"1.4"},{id:"1.5"},{id:"1.6"},{id:"1.7"},{id:"1.8"},{id:"1.9"},{id:"1.10"}
					]

				},
				{
					day_order:'Day Order 2',dayor:'2',
					day:[
						{id:"2.1"},{id:"2.2"},{id:"2.3"},{id:"2.4"},{id:"2.5"},{id:"2.6"},{id:"2.7"},{id:"2.8"},{id:"2.9"},{id:"2.10"}
					]
				},
				{
					day_order:'Day Order 3',dayor:'3',
					day:[
						{id:"3.1"},{id:"3.2"},{id:"3.3"},{id:"3.4"},{id:"3.5"},{id:"3.6"},{id:"3.7"},{id:"3.8"},{id:"3.9"},{id:"3.10"}
					]
				},
				{
					day_order:'Day Order 4',dayor:'4',
					day:[
						{id:"4.1"},{id:"4.2"},{id:"4.3"},{id:"4.4"},{id:"4.5"},{id:"4.6"},{id:"4.7"},{id:"4.8"},{id:"4.9"},{id:"4.10"}
					]
				},
				{
					day_order:'Day Order 5',dayor:'5',
					day:[
						{id:"5.1"},{id:"5.2"},{id:"5.3"},{id:"5.4"},{id:"5.5"},{id:"5.6"},{id:"5.7"},{id:"5.8"},{id:"5.9"},{id:"5.10"}
					]
				}
			],
			username:'',
			batch:'',
			year:'',
			dept:'',
			sem:'',
			logout:'/ework/user2/slogout',
      get:'/ework/user2/getstudent',
      home:'/ework/student',
      nav_route:'/ework/user2/fetch_snav',
			noti_route:false,
			display:'',
    };

		this.componentDidMount = this.componentDidMount.bind(this);

    }
		fetchlogin = () =>{
			axios.get('/ework/user2/getstudent').then(response => {
	      if (response.data.user)
	      {
					//console.log(response.data.user)
	          this.setState({username:response.data.user.username,
						batch:response.data.user.batch,sem:response.data.user.sem,
						year:response.data.user.year,dept:response.data.user.dept});
						this.fetchRenderStatus();
	      }
				else {
					this.setState({loading:false})
				}
	        })
    }

    componentDidMount() {
			if(this.props.props_to_refer)
			{
				this.setState({username:this.props.props_to_refer.username,loading:false})
			}
			else
			{
				 this.fetchlogin();
			}

    }


		fetchRenderStatus=()=>{
			axios.post('/ework/user2/stud_status',{userEnable:'verify'})
					.then(response => {
						this.setState({loading: false})
						if(response.data === 'no')
						{
							this.setState({display:'none'})
						}
						else if(response.data === 'ok')
						{
							this.setState({display:'block'})
						}
						else
						{
							var fac_blueprint_status = response.data.filter(item => ((item.usertype==="stud") && (item.order==="BLUE PRINT STATUS")));
							if((fac_blueprint_status.length)>0)
							{
								if(fac_blueprint_status[0].status === true)
								{
									this.setState({display:'block'})
								}
								else {
									this.setState({display:'none'})
								}
							}
							else {
								this.setState({status:'block'})
							}
						}
					})
		}



render() {
	const MyLoader = () => (
		<ContentLoader
		  height={160}
		  width={400}
		  speed={2}
		  primaryColor="#f3f3f3"
		  secondaryColor="#c0c0c0"
		>
		  <rect x="24" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="35" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="35" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="55" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="55" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="75" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="75" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="95" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="95" rx="0" ry="0" width="38" height="7" />

		  <rect x="24" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="63" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="102" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="141" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="180" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="219" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="258" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="297" y="115" rx="0" ry="0" width="38" height="7" />
		  <rect x="336" y="115" rx="0" ry="0" width="38" height="7" />

		</ContentLoader>
	  )
	if(this.state.loading=== true)
	{
		return(
			<MyLoader />
		);
	}
	else{
  if (this.state.redirectTo) {
       return <Redirect to={{ pathname: this.state.redirectTo }} />
   }
	  else
		 {
			 if(this.state.display === 'none')
			 {
				 return(
				 <Grid container>
						<Grid item xs={3} sm={3}/>
						<Grid item xs={6} sm={6}>
									<Card style={{marginTop:'60px'}}>
									<CardContent style={{textAlign:'center'}}>
									<Typography gutterBottom variant="h4" style={{color:'red'}} component="h2">
										Oops !!
									</Typography>
									 <Typography variant="button" display="block" gutterBottom>
										 SUBMISSION OF ANY DATA IN THIS PAGE HAS BEEN CLOSED BY ADMIN
									 </Typography>
									 <Typography variant="overline" display="block" gutterBottom>
										 TO RE-RENDER THIS PAGE CONTACT WITH YOUR FACULTY ADVISOR.
									 </Typography>
									</CardContent>
									<CardActions>
									 <Button size="small" color="primary" variant="outlined" style={{float:'right'}}>
										 <Link style={{textDecoration:'none'}} to="/ework/student">HOME</Link>
									 </Button>
									</CardActions>
									</Card>
						</Grid>
						<Grid item xs={3} sm={3}/>
				 </Grid>
			  );
			 }
			 else
			 {
					return (
						<React.Fragment>
						{!(this.props.props_to_refer) &&
						<Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
					  }

						<TableContainer component={Paper}>
	 					<Table aria-label="simple table">
	 					<TableHead>
	 						<TableRow>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">Hour /Day Order</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>1</b></span>
	 								<br />08:00 - 08:50
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>2</b></span>
	 								<br />08:50 - 09:40
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>3</b></span>
	 								<br />09:45 - 10:35
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>4</b></span>
	 								<br />10:40 - 11:30
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>5</b></span>
	 								<br />11:35 - 12:25
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>6</b></span>
	 								<br />12:30 - 01:20
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>7</b></span>
	 								<br />01:25 - 02:15
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>8</b></span>
	 								<br />02:20 - 03:10
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>9</b></span>
	 								<br />03:15 - 04:05
	 							</TableCell>
	 							<TableCell  style={{borderRight: '1px solid'}} align="center">
	 								<span style={{color:'red'}}><b>10</b></span>
	 								<br />04:05 - 04:55
	 							</TableCell>
	 						</TableRow>
	 					</TableHead>
	 						<TableBody>
	 							{this.state.datas.map((content,index)=> (
	 								<TableRow key={index}>
	 										<Stat content={content} stateData={this.state}
											dayor={content.dayor} username = {this.state.username}/>
	 								</TableRow>
	 							))}
	 						</TableBody>
	 					</Table>
	 				</TableContainer>
				</React.Fragment>
				);
		}
}
}
}
}


class Stat extends Component{
	constructor()
	{
		super()
		this.state={
			isAddProduct: false,
			response: {},
			action:'',
			product: {},
			isEditProduct: false,
			added:'',
			disabled:'block',
			editing:'',
			popOver:false,
			popId:'',
			snack_open:false,
			alert_type:'',
			snack_msg:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}

	componentDidMount()
	{
	}

	    onCreate = (e,action) => {
	      this.setState({ isAddProduct: action ,product: {},disabled:'none'});
	    }
	    onFormSubmit =(data) => {
	      let apiUrl;
				if(this.state.isEditProduct)
	      {
	        apiUrl = '/ework/user2/edit_faculty';
	      }
	       else
	       {
	        apiUrl = '/ework/user2/add_faculty';
	       }
	      axios.post(apiUrl,data)
	          .then(response => {
							if(response.data.exceed)
							{
								this.setState({
									snack_open:true,
									snack_msg:response.data.exceed,
									alert_type:'error',
								})
							}
							else if(response.data.succ)
							{
								this.setState({
									snack_open:true,
									snack_msg:response.data.succ,
									alert_type:'success',
								})
							}
							else if(response.data.emsg)
							{
								this.setState({
									snack_open:true,
									snack_msg:response.data.emsg,
									alert_type:'error',
								})
							}
							if(this.state.isEditProduct)
							{
							 this.setState({added:data.serial})
							}
							 else
							 {
								this.setState({added:this.state.isAddProduct})
							 }

							this.setState({
								response: response.data,
								isEditProduct: false,
								isAddProduct: false,
								disabled:'block',
								editing:'',
							})
	          })
	    }

	    editProduct = (productId,index)=> {
				 this.setState({editing:productId})
	      axios.post('/ework/user2/fetcheditdata_facultylist',{
	        id: productId,
	      })
	          .then(response => {
	            this.setState({
	              product: response.data,
	              isEditProduct: index,
	              isAddProduct: index,
	            });
	          })
	   }
		 close=()=>{
			 this.setState({editing:'',isAddProduct:'',isEditProduct:''})
		 }


		 displayPops=(id)=>{
		 	this.setState({popOver:true,popId:id})
		 }
		 displayPopsOff=(id)=>{
			this.setState({popOver:false,popId:id})
		 }

	render()
	{
		return(
			<React.Fragment>

			<Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} maxSnack={5}
			open={this.state.snack_open} autoHideDuration={2000}
			onClose={()=>this.setState({snack_open:false})}>
				<Alert onClose={()=>this.setState({snack_open:false})}
				severity={this.state.alert_type}>
					{this.state.snack_msg}
				</Alert>
			</Snackbar>



			<TableCell  style={{borderRight: '1px solid'}}  align="center">
				 {this.props.content.day_order}{this.state.week}
			</TableCell>
				 {this.props.content.day.map((content,ind)=>{
						 return(
							 <TableCell style={{borderRight: '1px solid'}} align="center" onMouseLeave={()=>this.displayPopsOff(content.id)} onMouseEnter={()=>this.displayPops(content.id)} key={content.id}>
									 <ShowDetails
									 stateData={this.props.stateData}
									 editing={this.state.editing} creating={this.state.isAddProduct}
									 popOver={this.state.popOver} popId={this.state.popId}
									 create={this.onCreate} added={this.state.added} data={content}
									 username={this.props.username}  action={content.id}
									 editProduct={this.editProduct}/>
									 {((this.state.isAddProduct === content.id) || (this.state.isEditProduct === content.id)) &&
									 <Add data={content} stateData={this.props.stateData} close={this.close} action={content.id}
									 username={this.props.username} onFormSubmit={this.onFormSubmit}
									 product={this.state.product} />
									 }
							 </TableCell>
						 );
					 })}
			</React.Fragment>
		);
	}
}




class Add extends Component {
	constructor(props) {
    super(props);
    this.initialState = {
			alloted_slots:'',
			timing:'',
      value:'',
      username:'',
      serial:'',
      action:'',
      content:'',
			faculty_id:'',
			hour:'',
			snack_open:false,
			alert_type:'',
			snack_msg:'',
    }
    if(this.props.product){
      this.state = this.props.product
    } else {
      this.state = this.initialState;
    }
    this.componentDidMount = this.componentDidMount.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }


	handleD = (e,index) =>{
    this.setState({
      [e.target.name]: e.target.value.toUpperCase(),
    })
  }

	handleF = (e,index) =>{
		this.setState({
			faculty_id: e,
		})
	}


componentDidMount(){
  this.setState({
    action:this.props.data.id,
		timing:this.props.data.id,
    username: this.props.username,
		batch:this.props.stateData.batch,
		year:this.props.stateData.year,
		sem:this.props.stateData.sem,
		dept:this.props.stateData.dept,
    verified:false,
  })
}



  handleSubmit(event,index) {
		event.preventDefault();
		if(!(this.state.alloted_slots) || !(this.state.subject_code) || !(this.state.faculty_id))
		{
			this.setState({
				snack_open:true,
				snack_msg:'Enter all the details !!',
				alert_type:'warning',
			})
		}
		else{
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
		}
  }

  render() {
    return(
      <React.Fragment>
			<Dialog open={true} fullWidth>
					<DialogTitle id="alert-dialog-title" align="center">Enter the slot Details</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							After entering the slot name and subject code, list of the faculty will appear.
						</DialogContentText>
								<TextField
				          id="filled-textarea1"
				          label="Enter Slot Name"
				          variant="outlined"
									type="text"
									name="alloted_slots"
									value={this.state.alloted_slots}
									onChange={e => this.handleD(e,this.props.data.id)}
									fullWidth
				        />
								<br /><br />
								<TextField
									id="filled-textarea"
									label="Enter Subject Code"
									variant="outlined"
									type="text"
									name="subject_code"
									value={this.state.subject_code}
									onChange={e => this.handleD(e,this.props.data.id)}
									fullWidth
								/>
								<br /><br />
								{(this.state.subject_code && this.state.alloted_slots) &&
									<FacultyListFetch subject_code={this.state.subject_code} handleF={this.handleF}
									slot={this.state.alloted_slots} id={this.props.data.id} />
								}
					</DialogContent>
					 <br />
					 <DialogActions>
						<Button onClick={this.props.close} color="primary">
							CANCEL
						</Button>
						<Button onClick={this.handleSubmit} color="primary" autoFocus>
							SUBMIT
						</Button>
					</DialogActions>

			</Dialog>
      </React.Fragment>
    )
  }
}


class FacultyListFetch extends Component{
	constructor()
	{
		super()
		this.state={
			facultyList:'',
			fetching:true,
		}
		this.componentDidMount = this.componentDidMount.bind(this)
	}

	componentDidMount()
	{
		this.fetchPossibilty();
	}
	componentDidUpdate=(prevProps)=>{
		if(prevProps!==this.props)
		{
			this.fetchPossibilty()
		}
	}

	fetchPossibilty=()=>{
		axios.post('/ework/user/fetch_possible_list',{props:this.props})
		.then( res => {
			//console.log(res.data)
				this.setState({
					facultyList:res.data,
					fetching:false,
				})
		});
	}

	setDomain=(e)=>{
		if(e === null)
		{

		}
		else{
			let value = e.username;
			this.props.handleF(value,this.props.id)
		}
	}
	render()
	{
		if(this.state.fetching)
		{
			return(
				<div>Fetching...</div>
			)
		}
		else {
			const options = this.state.facultyList.map(option => {
				const firstLetter = option.username[0].toUpperCase();
				return {
					firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
					...option,
				};
			});
			return(
				<div>
				  {this.state.facultyList.length>0 ?
						<React.Fragment>
							<Autocomplete
								id="grouped-demo"
								options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
								groupBy={option => option.firstLetter}
								getOptionLabel={option => option.username+' - '+option.faculty_name}
								onChange={(event, value) => this.setDomain(value)}
								style={{ width: '100%' }}
								renderInput={params => (
									<TextField {...params} label="Choose Faculty(Faculty Id - Faculty Name)" variant="filled" fullWidth />
								)}
							/>
						</React.Fragment>
						:
						<div style={{color:'red',textAlign:'center'}}>Ooops!! No result found...!! Check the details again..!!</div>
					}
				</div>
			)
		}
	}
}




class ShowDetails extends Component {
	constructor(props) {
    super(props);
    this.state = {
      username:'',
      error: null,
      products: [],
      action:'',
			loading:true,
			popOver:false,
			render_confirm:false,
			productId:0,
    }
    this.fetch = this.fetch.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
      this.fetch();
  }

  componentDidUpdate =(prevProps) => {
    if ((prevProps.action!==this.props.action) || (prevProps.added!==this.props.added)) {
      this.fetch();
    }
  }

 deleteProduct=(productId,index)=>{
	 this.setState({render_confirm:!this.state.render_confirm,productId:productId});
 }

  confirm = (productId,index) => {
  const { products } = this.state;
  axios.post('/ework/user2/del_faculty_list',{
    serial: this.state.productId,
    username: this.props.username,
  })
      .then(response => {
        this.setState({
					render_confirm:false,
					productId:0,
          response: response,
          products: products.filter(product => product.serial !== this.state.productId)
       })
      })
}


fetch =(e) =>{
	let route;
	if(e)
	{
		route = e;
	}
	else
	{
	route =this.props.action;
 }
  axios.post('/ework/user2/fetchall_faculty',{
    action: route,
		username:this.props.username,
  })
  .then(response =>{
    this.setState({
			loading:false,
      products: response.data,
   })
  })
}



  render() {
      return(
        <React.Fragment>
				<Dialog
          open={this.state.render_confirm}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Confirmation of Deleting the Data</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
						  This Action will permanently delete you data
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button style={{float:'left'}} onClick={this.deleteProduct} color="primary">
              Disagree
            </Button>
            <Button style={{float:'right'}} onClick={this.confirm} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>

				{this.state.loading === false ?
						<React.Fragment>
									{this.state.products.length!==0 ?
										<React.Fragment>
											        {this.state.products.map((content,index)=>(
																<React.Fragment key={index}>
																{((this.props.popOver) && (this.props.popId === this.props.action)) &&
                                  <React.Fragment>
                                    {(content.verified === false) &&
                                      <div className="popup_over right" >
                                        <EditIcon onClick={() => this.props.editProduct(content.serial,this.props.action)} />
                                        <DeleteIcon onClick={() => this.deleteProduct(content.serial,this.props.action)} />
                                     </div>
                                    }
                                  </React.Fragment>
															 }

																	{!(this.props.editing === content.serial) &&
												          <Grid container spacing={1} style={{display:this.props.disabled}}>
												              <Grid item xs={12} sm={12}>
																			  <span style={{color:'red'}}> Slot - </span>{content.alloted_slots} ({content.subject_code})<br />
																				 <span style={{color:'red'}}>Faculty - </span>{content.faculty_id}
																			</Grid>
												          </Grid>
																  }
																	</React.Fragment>
											        ))}
											</React.Fragment>
									 :<div>
									{!(this.props.creating === this.props.data.id) &&
										<Fab color="primary" size="small" aria-label="add" onClick={(e) => this.props.create(e,this.props.data.id)}>
											<AddIcon  />
										</Fab>
									 }
									</div>
								}
						</React.Fragment>
			:
			<CircularProgress color="secondary" />
		}
			</React.Fragment>
      )
  }
}
