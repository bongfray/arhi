import React, { Component} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import ViewSubject from './setupForView';

require("datejs")




export default class Types extends React.Component{
  constructor()
  {
    super()
    this.state={
      modal:true,
      setday:'',
      setmonth:'',
      setyear:'',
      setday2:'',
      setmonth2:'',
      setyear2:'',
      renderFetch:false,
      onlyDownload:false,
      username:'',
    }
  }

  selectModal = (info) => {
  this.setState({modal: !this.state.modal})
  }
  closeModal = (info) => {
  this.setState({renderFetch:true,onlyDownload:true,})
  }
  handleChecked=(e)=>{
    this.setState({type:e.target.value})
  }
  setDatas=(object)=>{
    this.setState(object);
  }

  render()
  {
    return(
      <React.Fragment>
             {this.state.modal &&
                 <Dialog open={this.props.display}>
                 <DialogTitle id="alert-dialog-title" align="center">{"Kindly Select the Correct Format of Date"}</DialogTitle>
                         <div style={{padding:'15px'}}>
                            <FormControl component="fieldset">
                              <RadioGroup aria-label="gender" name="gender1" value={this.state.type} onChange={this.handleChecked}>
                              <Grid container spacing={1}>
                                 <Grid item xs>
                                   <FormControlLabel value="single" control={<Radio />} label="Single" />
                                 </Grid>
                                 <Grid item xs>
                                   <FormControlLabel value="monthly" control={<Radio />} label="Monthly" />
                                 </Grid>
                                 <Grid item xs>
                                   <FormControlLabel value="yearly" control={<Radio />} label="Yearly" />
                                 </Grid>
                                 <Grid item xs>
                                   <FormControlLabel value="custom" control={<Radio />} label="Custom" />
                                 </Grid>
                              </Grid>
                              </RadioGroup>
                            </FormControl>
                            <br /><br />
                            {!(this.state.type) &&
                              <div>
                                <Button variant="outlined" style={{float:'right'}} onClick={this.closeModal} color="secondary">CLOSE</Button>
                             </div>
                            }
                            <Grid container>
                                 <Decision
                                 closeModal={this.selectModal}
                                 setDatas={this.setDatas}
                                 type={this.state.type} />
                            </Grid>
                         </div>
             </Dialog>
        }
          {this.state.renderFetch &&
                <React.Fragment>
                  <ViewSubject actionType={this.props.actionType} closeDecider={this.props.closeDecider}
                  admin_action={this.props.admin_action} closeModal={this.selectModal}
                  onlyDownload={this.state.onlyDownload} username={this.props.username}
                  datas={this.state} setDatas={this.setDatas} />
                </React.Fragment>
           }
      </React.Fragment>
    );
  }
}


class Decision extends Component {
  constructor()
  {
    super()
    this.state={

    }
  }

  render() {
    if(this.props.type === 'single')
    {
      return (
        <Single closeModal={this.props.closeModal} setDatas={this.props.setDatas}/>
      );
    }
    else if(this.props.type === 'monthly')
    {
      return (
        <Monthly closeModal={this.props.closeModal} setDatas={this.props.setDatas}/>
      );
    }
    else if(this.props.type === 'yearly')
    {
      return (
        <Yearly closeModal={this.props.closeModal} setDatas={this.props.setDatas}/>
      );
    }
    else if(this.props.type === 'custom')
    {
      return (
        <Custom closeDecider={this.props.closeDecider} setDatas={this.props.setDatas}/>
      );
    }
    else{
      return(
        <div></div>
      )
    }
  }
}


class Single extends Component {
  constructor()
  {
    super()
    this.state={
      setmonth:'',
      setyear:'',
      setday:'',
      visible_day:true,
      day_set:[],
    }
  }


setField=(e)=>{

  this.setState({[e.target.name]:e.target.value})
if(e.target.name === "setyear")
{
  if(this.leapYear(e.target.value)=== true)
  {
    this.setState({
      day_set: [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
    {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
  ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
    })
  }
  else
  {
    this.setState({
      day_set: [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
    {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
  ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
    })
  }
  if(e.target.value)
  {
    this.setState({visible_day:false})
  }
}

}



leapYear(year)
{
 return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}

viewIt =()=>{
  this.props.setDatas({
    setmonth:this.state.setmonth,
    setyear:this.state.setyear,
    setday:this.state.setday,
    renderFetch:true,
    modal:false,
  })

}

  render() {
    var year = parseInt(Date.today().toString("yyyy"));
    var years =[];
    var days =[];
    for(var i=2019;i<=year;i++)
    {
      years.push(i);
    }
   if(this.state.setmonth)
   {
       const day_data= this.state.day_set.filter(item => item.value===this.state.setmonth);
       //console.log(day_data[0].day)
       for(var j=1;j<=day_data[0].day;j++)
       {
         days.push(j);
       }
   }


  const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


    return (
      <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={12}>
           <Paper variant="outlined" square style={{padding:'5px'}}>
                <Grid container spacing={1}>
                    <Grid item xs={4} sm={4}>
                        <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                          <InputLabel id="year">Year</InputLabel>
                          <Select
                            labelId="year"
                            id="year"
                            name="setyear"
                            value={this.state.setyear}
                            onChange={this.setField}
                          >
                          {years.map((content,index)=>{
                                  return(
                                    <MenuItem key={index} value={content}>{content}</MenuItem>
                                  )
                            })}
                          </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={4} sm={4}>
                        <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                          <InputLabel id="month">Month</InputLabel>
                          <Select
                            labelId="month"
                            id="month"
                            name="setmonth"
                            value={this.state.setmonth}
                            onChange={this.setField}
                          >
                          {this.state.day_set.map((content,index)=>{
                                  return(
                                    <MenuItem key={index} value={content.value}>{content.name}</MenuItem>
                                  )
                            })}
                          </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={4} sm={4}>
                        <FormControl style={{width:'100%'}} className={useStyles.formControl} disabled={this.state.visible_day}>
                          <InputLabel id="day">Day</InputLabel>
                          <Select
                            labelId="day"
                            id="day"
                            name="setday"
                            value={this.state.setday}
                            onChange={this.setField}
                          >
                          {days.map((content,index)=>{
                                  return(
                                    <MenuItem key={index} value={content}>{content}</MenuItem>
                                  )
                            })}
                          </Select>
                        </FormControl>
                    </Grid>
                  </Grid>
              </Paper>
          </Grid>
          </Grid>
      <br />
             {((this.state.setmonth) && (this.state.setyear)&&(this.state.setday)) ?
                <Button style={{float:'right'}} variant="contained" color="secondary" onClick={this.viewIt}>View</Button>
                :
                <Button style={{float:'right'}} disabled variant="contained" color="secondary">View</Button>
            }

      </React.Fragment>
    );
  }
}




class Monthly extends Component {
  constructor()
  {
    super()
    this.state={
      month:[{name:'January',value:1},{name:'February',value:2},{name:'March',value:3},{name:'April',value:4},
    {name:'May',value:5},{name:'June',value:6},{name:'July',value:7},{name:'August',value:8},{name:'September',value:9}
  ,{name:'October',value:10},{name:'November',value:11},{name:'December',value:12}],
  setmonth:'',
    }
  }
  viewIt =()=>{
    var year = parseInt(Date.today().toString("yyyy"));
    this.props.setDatas({
      setmonth:this.state.setmonth,
      setyear:year,
      renderFetch:true,
      modal:false,
    })
  }

  setMonth=(e)=>{
    this.setState({setmonth:e.target.value})
  }

  render() {


    const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
var year = parseInt(Date.today().toString("yyyy"));
    return (
      <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={12}>
           <Paper variant="outlined" square style={{padding:'5px'}}>
                <Grid container spacing={1}>
                      <Grid item xs={6} sm={6}>
                        <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                          <InputLabel id="month">Month</InputLabel>
                          <Select
                            labelId="month"
                            id="month"
                            value={this.state.setmonth}
                            onChange={this.setMonth}
                          >
                          {this.state.month.map((content,index)=>{
                                  return(
                                    <MenuItem key={index} value={content.value}>{content.name}</MenuItem>
                                  )
                            })}
                          </Select>
                        </FormControl>
                      </Grid>
                    <Grid item xs={6} sm={6}>
                        Year : {year}
                    </Grid>
                  </Grid>
                  <br />
                     {((this.state.setmonth)) ?
                        <Button style={{float:'right'}} variant="contained" color="secondary" onClick={this.viewIt}>View</Button>
                        :
                        <Button style={{float:'right'}} disabled variant="contained" color="secondary">View</Button>
                    }

                </Paper>
             </Grid>
          </Grid>
      </React.Fragment>
    );
  }
}


class Yearly extends Component {
  constructor()
  {
    super()
    this.state={
      month:[{name:'January',value:1},{name:'February',value:2},{name:'March',value:3},{name:'April',value:4},
    {name:'May',value:5},{name:'June',value:6},{name:'July',value:7},{name:'August',value:8},{name:'September',value:9}
  ,{name:'October',value:10},{name:'November',value:11},{name:'December',value:12}],
      setmonth:'',
      setyear:'',
    }
  }

setMonth=(e)=>{
  this.setState({setmonth:e.target.value})
}

setYear=(e)=>{
  this.setState({setyear:e.target.value})
}

viewIt =()=>{
  this.props.setDatas({
    setmonth:this.state.setmonth,
    setyear:this.state.setyear,
    renderFetch:true,
    modal:false,
  })

}

  render() {
    //var day = parseInt(Date.today().toString("dd"));
    var year = parseInt(Date.today().toString("yyyy"));
    var years =[];
    for(var i=2019;i<=year;i++)
    {
      years.push(i);
    }

    const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


    return (
      <React.Fragment>
      <Grid container>
        <Grid item xs={12} sm={12}>
           <Paper variant="outlined" square style={{padding:'5px'}}>
              <Grid container spacing={1}>
                <Grid item xs={6} sm={6}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                      <InputLabel id="month">Month</InputLabel>
                      <Select
                        labelId="month"
                        id="month"
                        value={this.state.setmonth}
                        onChange={this.setMonth}
                      >
                      {this.state.month.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content.value}>{content.name}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6} sm={6}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                      <InputLabel id="year">Year</InputLabel>
                      <Select
                        labelId="year"
                        id="year"
                        name="setyear"
                        value={this.state.setyear}
                        onChange={this.setYear}
                      >
                      {years.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content}>{content}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
              </Grid>
              <br />

                 {((this.state.setmonth) && (this.state.setyear))?
                   <Button style={{float:'right'}} variant="contained" color="secondary" onClick={this.viewIt}>View</Button>
                   :
                   <Button style={{float:'right'}} disabled variant="contained" color="secondary">View</Button>
                }
               </Paper>
            </Grid>
          </Grid>
      </React.Fragment>
    );
  }
}



class Custom extends Component {
  constructor()
  {
    super()
    this.state={
      setmonth:'',
      setyear:'',
      setday:'',
      setmonth2:'',
      setyear2:'',
      setday2:'',
      visible_day:true,
      visible_day2:true,
      day_set:[],
      day_set2:[]
    }
  }

setMonth=(e)=>{
  this.setState({setmonth:e.target.value})
  if(e.target.value && this.state.setyear)
  {
    this.setState({visible_day:false})
  }
}

setField=(e)=>{

  this.setState({[e.target.name]:e.target.value})
//  console.log(this.leapYear(e.target.value));
if(e.target.name === "setyear")
{
  if(this.leapYear(e.target.value)=== true)
  {
    this.setState({
      day_set: [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
    {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
  ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
    })
  }
  else
  {
    this.setState({
      day_set: [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
    {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
  ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
    })
  }
  if(e.target.value)
  {
    this.setState({visible_day:false})
  }
}
else if(e.target.name === "setyear2")
  {
    if(this.leapYear(e.target.value)=== true)
    {
      this.setState({
        day_set2: [{name:'January',day:31,value:1},{name:'February',day:29,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
      {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
    ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
      })
    }
    else
    {
      this.setState({
        day_set2: [{name:'January',day:31,value:1},{name:'February',day:28,value:2},{name:'March',day:31,value:3},{name:'April',day:'30',value:4},
      {name:'May',day:31,value:5},{name:'June',day:30,value:6},{name:'July',day:31,value:7},{name:'August',day:31,value:8},{name:'September',day:30,value:9}
    ,{name:'October',day:31,value:10},{name:'November',day:30,value:11},{name:'December',day:31,value:12}]
      })
    }
    if(e.target.value)
    {
      this.setState({visible_day2:false})
    }
  }

}



leapYear(year)
{
 return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}

viewIt =()=>{
  this.props.setDatas({
    setmonth:this.state.setmonth,
    setyear:this.state.setyear,
    setday:this.state.setday,
    setmonth2:this.state.setmonth2,
    setyear2:this.state.setyear2,
    setday2:this.state.setday2,
    renderFetch:true,
    modal:false,
  })

}

  render() {
    var year = parseInt(Date.today().toString("yyyy"));
    var years =[];
    var days =[];
    var days2 =[];
    for(var i=2019;i<=year;i++)
    {
      years.push(i);
    }
   if(this.state.setmonth)
   {
       const day_data= this.state.day_set.filter(item => item.value===this.state.setmonth);
       //console.log(day_data[0].day)
       for(var j=1;j<=day_data[0].day;j++)
       {
         days.push(j);
       }
   }
   if(this.state.setmonth2)
   {
       const day_data2= this.state.day_set2.filter(item => item.value===this.state.setmonth2);
       //console.log(day_data[0].day)
       for(var k=1;k<=day_data2[0].day;k++)
       {
         days2.push(k);
       }
   }


  const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


    return (
      <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={6} sm={6}>
          <Paper variant="outlined" square style={{padding:'5px'}}>
              <Grid container spacing={1}>
                 <Grid item xs={4} sm={4}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                      <InputLabel id="year">Year</InputLabel>
                      <Select
                        labelId="year"
                        id="year"
                        name="setyear"
                        value={this.state.setyear}
                        onChange={this.setField}
                      >
                      {years.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content}>{content}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                      <InputLabel id="month">Month</InputLabel>
                      <Select
                        labelId="month"
                        id="month"
                        name="setmonth"
                        value={this.state.setmonth}
                        onChange={this.setField}
                      >
                      {this.state.day_set.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content.value}>{content.name}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl} disabled={this.state.visible_day}>
                      <InputLabel id="day">Day</InputLabel>
                      <Select
                        labelId="day"
                        id="day"
                        name="setday"
                        value={this.state.setday}
                        onChange={this.setField}
                      >
                      {days.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content}>{content}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={6} sm={6}>
          <Paper variant="outlined" square style={{padding:'5px'}}>
                <Grid container spacing={1}>
                <Grid item xs={4} sm={4}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                      <InputLabel id="year">Year</InputLabel>
                      <Select
                        labelId="year"
                        id="year"
                        name="setyear2"
                        value={this.state.setyear2}
                        onChange={this.setField}
                      >
                      {years.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content}>{content}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl}>
                      <InputLabel id="month">Month</InputLabel>
                      <Select
                        labelId="month"
                        id="month"
                        name="setmonth2"
                        value={this.state.setmonth2}
                        onChange={this.setField}
                      >
                      {this.state.day_set2.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content.value}>{content.name}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <FormControl style={{width:'100%'}} className={useStyles.formControl} disabled={this.state.visible_day2}>
                      <InputLabel id="day">Day</InputLabel>
                      <Select
                        labelId="day"
                        id="day"
                        name="setday2"
                        value={this.state.setday2}
                        onChange={this.setField}
                      >
                      {days2.map((content,index)=>{
                              return(
                                <MenuItem key={index} value={content}>{content}</MenuItem>
                              )
                        })}
                      </Select>
                    </FormControl>
                </Grid>
                </Grid>
                </Paper>
            </Grid>
          </Grid>
          <br />
             {((this.state.setmonth) && (this.state.setyear)&&(this.state.setmonth2) && (this.state.setyear2)&&(this.state.setday) && (this.state.setday2))?
               <Button style={{float:'right'}} variant="contained" color="secondary" onClick={this.viewIt}>View</Button>
               :
               <Button style={{float:'right'}} disabled variant="contained" color="secondary">View</Button>
            }
      </React.Fragment>
    );
  }
}
