import React, { Component } from 'react';
import Logo from './Logo.png';
import Doc from './DocService';
import Axios from 'axios';
import PdfContainer from './PdfContainer';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import {TableContainer,Hidden} from '@material-ui/core';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

var time = new Date();

class Print extends Component {


  constructor(){
    super();
    this.state={
      loading:true,
      username:'',
      content: "",
      name: '',
      id: '',
      desgn:'',
      dept: '',
      campus: '',
      mob: '',
      mail: '',
      date:time.format(),
      profile_data:[],
      degree_data:[],
      administrative_data:[],
      section:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.setUser();
  }


  fetchOption = () =>{
    this.profileFetch()
  }

  profileFetch = () =>{
    Axios.post('/ework/user/dash2',{username:this.props.username})
    .then(res=>{
      this.degreeFetch();
      this.setState({profile_data:res.data})
    })
  }
  degreeFetch = () =>{
    Axios.post('/ework/user/fetch_degree_for_pdf',{username:this.props.username})
    .then(res=>{
      this.fetchSection();
      this.setState({degree_data:res.data})
    })
  }
  fetchSection=()=>{
    let action;
    console.log(this.props.onlyDownload);
    if(!this.props.onlyDownload)
    {
      action = {datas:this.props.referDatas,
        username:this.props.username}
    }
    else {
      action = {action:'all',username:this.props.username}
    }
    Axios.post('/ework/user/fetch_section_for_view',action)
    .then(res=>{
      this.administrativeFetch();
      this.setState({section:res.data})
    })
  }
  administrativeFetch =()=>{
    let action;
    console.log(this.props.onlyDownload);
    if(!this.props.onlyDownload)
    {
      action = {datas:this.props.referDatas,
        username:this.props.username}
    }
    else {
      action = {action:'all',username:this.props.username}
    }
    Axios.post('/ework/user/fetch_adminis_for_view',action).then(res => {
    //  console.log(res.data);
      this.setState({
        administrative_data: res.data
      });
      this.researchFetch();
    })
  }

  researchFetch =()=>{
    let action;
    if(!this.props.onlyDownload)
    {
      action = {datas:this.props.referDatas,
        username:this.props.username}
    }
    else {
      action = {action:'all',username:this.props.username}
    }
    Axios.post('/ework/user/fetch_research_for_view',action).then(res => {
      this.setState({
        research_data: res.data
      });
    })
    this.acdemicFetch();
  }

  acdemicFetch =()=>{
    let action;
    if(!this.props.onlyDownload)
    {
      action = {datas:this.props.referDatas,
        username:this.props.username}
    }
    else {
      action = {action:'all',username:this.props.username}
    }
    Axios.post('/ework/user/fetch_academic_for_view',action).then(res => {
      this.setState({
        academic_data: res.data,
      });
      this.fiveFetch();
    })
  }

  fiveFetch =()=>{
    let action;
    if(!this.props.onlyDownload)
    {
      action = {datas:this.props.referDatas,
        username:this.props.username}
    }
    else {
      action = {action:'all',username:this.props.username}
    }
    Axios.post('/ework/user/fetch_five_for_view',action).then(res => {
      this.setState({
        five_data: res.data,loading:false
      });
    })
  }


  setUser = () =>{
    Axios.get('/ework/user/')
    .then(res=>{
      if(res.data.user)
      {
        this.setState({username:res.data.user.mailid})
        this.profileFetch()
      }
    })
  }


  createPdf = (html) => Doc.createPdf(html);

  render() {
    let content,section,administrative_data,research_data,five_data;

    if(this.state.degree_data){
      content=
      <Grid container spacing={1}>

        <Grid container justify="center" item xs={12} sm={12}>
          <p className="resume-mhead"><b>Qualifications</b></p>
        </Grid>

        <TableContainer >
          <Table aria-label="simple table" className="resume-table">
            <TableHead>
              <TableRow>
                <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
                <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Degree Name</b></TableCell>
                <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Institute Name</b></TableCell>
                <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Start Date</b></TableCell>
                <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>End Date</b></TableCell>
                <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Grade/CGPA</b></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.degree_data.map((content,index)=>{
                    return(
                      <TableRow key={index}>
                        <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                        <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.action}</TableCell>
                        <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.College_Name}</TableCell>
                        <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.Start_Year}</TableCell>
                        <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.End_Year}</TableCell>
                        <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.Marks_Grade}</TableCell>
                      </TableRow>
                    )
                })}
            </TableBody>
          </Table>
        </TableContainer>

      </Grid>
    }
    if(this.state.section)
    {
      section =
      <Grid container spacing={1}>

      <Grid container justify="center" item xs={12} sm={12}>
        <p className="resume-mhead"><b>Section Data</b></p>
      </Grid>

      <TableContainer >
        <Table aria-label="simple table" className="resume-table">
          <TableHead>
            <TableRow>
              <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
              <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Description</b></TableCell>
              <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Role</b></TableCell>
              <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Achievement (s)</b></TableCell>
              <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Start Date</b></TableCell>
              <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>End Date</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.section.map((content,index)=>{
                  return(
                    <TableRow key={index}>
                      <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                      <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.action}</TableCell>
                      <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.role}</TableCell>
                      <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.achivements}</TableCell>
                      <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.date_of_starting}</TableCell>
                      <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.date_of_complete}</TableCell>
                    </TableRow>
                  )
              })}
          </TableBody>
        </Table>
      </TableContainer>

      </Grid>
   }

   if(this.state.administrative_data)
   {
     section =
     <Grid container spacing={1}>

     <Grid container justify="center" item xs={12} sm={12}>
       <p className="resume-mhead"><b>Administrative Data</b></p>
     </Grid>

     <TableContainer >
       <Table aria-label="simple table" className="resume-table">
         <TableHead>
           <TableRow>
             <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
             <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Dayorder-Hour-Week</b></TableCell>
             <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Date</b></TableCell>
             <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Responsibility Type</b></TableCell>
             <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Covered Subject</b></TableCell>
             <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Verification Status</b></TableCell>
           </TableRow>
         </TableHead>
         <TableBody>
           {this.state.administrative_data.map((content,index)=>{
                 return(
                   <TableRow key={index}>
                     <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                     <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.day_order} - {content.hour} - {content.week}</TableCell>
                     <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.date} / {content.month} / {content.year}</TableCell>
                     <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.freeparts}</TableCell>
                     <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.covered}</TableCell>
                     <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.verified ? "Verified" : "Not Verified"}</TableCell>
                   </TableRow>
                 )
             })}
         </TableBody>
       </Table>
     </TableContainer>

     </Grid>
  }

 if(this.state.research_data)
 {
   research_data =
   <Grid container spacing={1}>

   <Grid container justify="center" item xs={12} sm={12}>
     <p className="resume-mhead"><b>Research Data</b></p>
   </Grid>

   <TableContainer >
     <Table aria-label="simple table" className="resume-table">
       <TableHead>
         <TableRow>
           <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
           <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Dayorder-Hour-Week</b></TableCell>
           <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Date</b></TableCell>
           <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Responsibility Type</b></TableCell>
           <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Covered Subject</b></TableCell>
           <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Verification Status</b></TableCell>
         </TableRow>
       </TableHead>
       <TableBody>
         {this.state.research_data.map((content,index)=>{
               return(
                 <TableRow key={index}>
                   <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                   <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.day_order} - {content.hour} - {content.week}</TableCell>
                   <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.date} / {content.month} / {content.year}</TableCell>
                   <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.freeparts}</TableCell>
                   <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.covered}</TableCell>
                   <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.verified ? "Verified" : "Not Verified"}</TableCell>
                 </TableRow>
               )
           })}
       </TableBody>
     </Table>
   </TableContainer>

   </Grid>
}

if(this.state.five_data)
{
  five_data =
  <Grid container spacing={1}>

  <Grid container justify="center" item xs={12} sm={12}>
    <p className="resume-mhead"><b>Five Year Data</b></p>
  </Grid>

  <TableContainer >
    <Table aria-label="simple table" className="resume-table">
      <TableHead>
        <TableRow>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>No.</b></TableCell>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Date</b></TableCell>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Responsibility Type</b></TableCell>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Plan</b></TableCell>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Completion Status</b></TableCell>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Expired Status</b></TableCell>
          <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}><b>Verification Status</b></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {this.state.five_data.map((content,index)=>{
              return(
                <TableRow key={index}>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{index+1}</TableCell>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.date} / {content.month} / {content.year}</TableCell>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.action}</TableCell>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.content}</TableCell>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.completed ? "Completed" : "Not Completed"}</TableCell>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.expired ? "Expired" : "Not Expired"}</TableCell>
                  <TableCell align="center" padding={'none'} style={{fontSize:'10px'}}>{content.verified ? "Verified" : "Not Verified"}</TableCell>
                </TableRow>
              )
          })}
      </TableBody>
    </Table>
  </TableContainer>

  </Grid>
}


    return (
      <React.Fragment>

      <Grid container>
        <Hidden xsDown><Grid item sm={3} /></Hidden>
            <Grid item xs={12} sm={6}>

              <PdfContainer loading={this.state.loading} createPdf={this.createPdf}>

                <React.Fragment>

                  <section className="resum-pdf-body" style={{padding:'20px'}}>

                    <Grid container spacing={1}>

                      <Grid item xs={2} sm={2}>
                        <img alt="prof" src={Logo} className="resume-logo"/>
                      </Grid>
                      <Grid item xs={8} sm={8}>
                          <Typography variant="h2" style={{  fontSize: '16px'}} align="center">SRM INSTITUTE OF SCIENCE AND TECHNOLOGY</Typography><br />
                          <p className="resume-p" style={{textAlign:'center',marginTop:'-3%', marginBottom:'-3%'}}>Profile Report (For Faculty Members)</p>
                      </Grid>
                      <Grid item xs={2} sm={2}/>

                      </Grid>

                      <br/>

                      <Grid container spacing={1}>

                        <Grid item xs={3} sm={3}>
                          <p className="resume-gen"><b>Name<br/>Faculty Id<br/>Department<br/>Designation<br/> Mobile No.<br/>Campus<br/></b> </p>
                        </Grid>
                        <Grid item xs={9} sm={9}>
                          <p className="resume-gen">: {this.state.profile_data.name} <br/>: {this.state.profile_data.username} <br/>: {this.state.profile_data.dept} <br/>: {this.state.profile_data.desgn} <br/>: {this.state.profile_data.phone} <br/>: {this.state.profile_data.campus}  </p>
                        </Grid>

                      </Grid>

                      {this.state.degree_data && content}

                      {this.state.section && section}

                      {this.state.administrative_data && administrative_data}

                      {this.state.research_data && research_data}

                      {this.state.five_data && five_data}

                      <Grid container direction="row" justify="flex-end">
                        <p className="resume-date">{this.state.date}</p>
                      </Grid>

                  </section>
                </React.Fragment>
            </PdfContainer>
            </Grid>
              <Hidden xsDown><Grid item sm={3} /></Hidden>
      </Grid>

      </React.Fragment>
    );

  }
}

export default Print;
