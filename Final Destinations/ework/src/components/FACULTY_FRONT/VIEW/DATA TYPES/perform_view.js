import React, { Component} from 'react'
import axios from 'axios'
import {Backdrop,Typography} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

export default class Five extends Component {
  constructor() {
    super()
    this.state={
      loader:true,
      admins:[],
      academic:[],
      research:[],
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount()
  {
    axios.post('/ework/user/fetch_perform_for_view',{datas:this.props.referDatas,action:null,
      username:this.props.username}).then(res => {
        let research = res.data.filter(item=>item.term  === 'Research');
        let academic = res.data.filter(item=>item.term === 'Academic');
        let admins = res.data.filter(item=>item.term  === 'Administrative');
      this.setState({
        admins:admins,
        research:research,
        academic:academic,
        loader:false,
      });
    }).catch(error => {
      this.setState({
        error: true,
        loader:false,
      });
    });
  }

  render()
  {
    function formatDate(string){
      var dateString =string.toString();
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(dateString).toLocaleDateString([],options);
    }

    let five_dat;
    if(this.state.loader === true)
    {
      five_dat = <Backdrop  open={true} style={{zIndex:'2040'}}>
                  <CircularProgress color="secondary" />&emsp;
                  <div style={{color:'yellow'}}>Processing Your Request....</div>
                </Backdrop>
    }
    else
    {
      five_dat =
       <div style={{padding:'0% 8% 0% 8%'}}>

         <React.Fragment>
           {this.state.academic.length>0 ?
              <React.Fragment>
                <Typography variant="h5" color="secondary">Academic Performance</Typography>

                    <TableContainer component={Paper}>
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell align="center">Responsibility</TableCell>
                            <TableCell align="center">Week - Month - Year</TableCell>
                            <TableCell align="center">Weekly Percentage</TableCell>
                            <TableCell align="center">Monthly Percentage</TableCell>
                            <TableCell align="center">Updated At</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {this.state.academic.map((row,index) => (
                            <TableRow key={index}>
                              <TableCell align="center">
                                {row.term}
                              </TableCell>
                              <TableCell align="center">{row.week} - {row.month} - {row.year}</TableCell>
                              <TableCell align="center">{row.weekly_percentage}</TableCell>
                              <TableCell align="center">{row.monthly_percentage}</TableCell>
                              <TableCell align="center">{formatDate(row.update_day)}</TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                </React.Fragment>
                :
                <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
              }
                <br />
        {this.state.research.length>0 ?
          <React.Fragment>
            <Typography variant="h5" color="secondary">Research Performance</Typography>
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Responsibility</TableCell>
                    <TableCell align="center">Week - Month - Year</TableCell>
                    <TableCell align="center">Weekly Percentage</TableCell>
                    <TableCell align="center">Monthly Percentage</TableCell>
                    <TableCell align="center">Updated At</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.research.map((row,index) => (
                    <TableRow key={index}>
                      <TableCell align="center">
                        {row.term}
                      </TableCell>
                      <TableCell align="center">{row.week} - {row.month} - {row.year}</TableCell>
                      <TableCell align="center">{row.weekly_percentage}</TableCell>
                      <TableCell align="center">{row.monthly_percentage}</TableCell>
                      <TableCell align="center">{formatDate(row.update_day)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

            </React.Fragment>
            :
            <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
          }
            <br />

            {this.state.admins.length>0 ?
              <React.Fragment>
            <Typography variant="h5" color="secondary">Administrative Performance</Typography>

            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Responsibility</TableCell>
                    <TableCell align="center">Week - Month - Year</TableCell>
                    <TableCell align="center">Weekly Percentage</TableCell>
                    <TableCell align="center">Monthly Percentage</TableCell>
                    <TableCell align="center">Updated At</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.admins.map((row,index) => (
                    <TableRow key={index}>
                      <TableCell align="center">
                        {row.term}
                      </TableCell>
                      <TableCell align="center">{row.week} - {row.month} - {row.year}</TableCell>
                      <TableCell align="center">{row.weekly_percentage}</TableCell>
                      <TableCell align="center">{row.monthly_percentage}</TableCell>
                      <TableCell align="center">{formatDate(row.update_day)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            </React.Fragment>
            :
            <div style={{textAlign:'center',color:'red'}}>No Data Found !!</div>
          }

         </React.Fragment>

        </div>
    }
    return (
      <React.Fragment>
       {five_dat}
        </React.Fragment>
    );
  }
}
