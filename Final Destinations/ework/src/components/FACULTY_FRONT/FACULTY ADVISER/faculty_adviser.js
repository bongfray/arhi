import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {Grid,Typography} from '@material-ui/core';

import Nav from '../../dynnav'

import StudentList from './studentList'
import ValidateUser from './validateUser'
import Manage from '../admin/Operations/SUPER_OPERATIONS/handleRerender_bluePrint'



export default class DepartmentAdmin extends Component {
  constructor()
  {
    super()
    this.state={
      choosed:0,
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      get:'/ework/user/',
      nav_route: '/ework/user/fetchnav',
      login:'/ework/flogin',
      noti_route:true,
      loader:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchlogin();
  }

  fetchlogin = () =>{
      axios.get('/ework/user/'
    )
      .then(response =>{
        if(response.data.user === null){
          this.setState({
            redirectTo:'/ework/',
          });
          window.M.toast({html: 'You are not Logged In', classes:'rounded red'});
        }
        else {
          this.setState({user:response.data.user,loader:false})
        }
      })
    }


  render() {
    if (this.state.redirectTo) {
        return <Redirect to={{ pathname: this.state.redirectTo }} />
    }
    else
    {

       if(this.state.loader)
       {
         return(
           <div className="load-me" style={{textAlign:'center'}}>
             <div class="spinner-box">
                   <div class="blue-orbit leo">
                   </div>

                   <div class="green-orbit leo">
                   </div>

                   <div class="red-orbit leo">
                   </div>
                   <div class="white-orbit w1 leo">
                   </div><div class="white-orbit w2 leo">
                   </div><div class="white-orbit w3 leo">
                   </div>
             </div>
             <Typography style={{color:'white'}} variant="h5" align="center">Processing Your Request....</Typography>
           </div>
         )
       }
       else{
          return(
              <React.Fragment>
              <Nav noti_route={this.state.noti_route} home={this.state.home} login={this.state.login} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>

              <AppBar position="static" color="default">
                 <Tabs
                   value={this.state.choosed}
                   indicatorColor="secondary"
                   textColor="secondary"
                   variant="fullWidth"
                   aria-label="full width tabs example"
                 >
                   <Tab label="Validate User" onClick={(e)=>this.setState({choosed:0})}  />
                   <Tab label="Student Under You" onClick={(e)=>this.setState({choosed:1})} />
                   <Tab label="Re-render BluePrint" onClick={(e)=>this.setState({choosed:2})}  />
                 </Tabs>
               </AppBar>

                  <Grid container>
                     <Display user={this.state.user} choosed={this.state.choosed}/>
                  </Grid>


                  <div className="row">

                  </div>
              </React.Fragment>
          );
        }
}
  }
}



class Display extends Component {
  constructor() {
    super()
    this.state={

    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {

  }
  render()
  {
    let content;
    if(this.props.choosed === 1)
    {
      content = <StudentList user={this.props.user} />
    }
    else if(this.props.choosed === 0)
    {
      content =<ValidateUser user={this.props.user} />
    }
    else if(this.props.choosed === 2)
    {
      content =<Manage choice={"student"} user={this.props.user} student={true} />
    }
    else{
      content = <div></div>
    }
    return(
      <React.Fragment>
       {content}
      </React.Fragment>
    );
  }
}
