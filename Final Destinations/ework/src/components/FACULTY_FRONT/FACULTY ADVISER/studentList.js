import React, { Component } from 'react';
import axios from 'axios'
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import {Paper} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import MuiAlert from '@material-ui/lab/Alert';
 import {Snackbar,Typography,Grid,TextField,Button} from '@material-ui/core';
 import VisibilityIcon from '@material-ui/icons/Visibility';
 import SendIcon from '@material-ui/icons/Send';
 import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
 import Dialog from '@material-ui/core/Dialog';
 import DialogActions from '@material-ui/core/DialogActions';
 import DialogContent from '@material-ui/core/DialogContent';
 import DialogContentText from '@material-ui/core/DialogContentText';
 import DialogTitle from '@material-ui/core/DialogTitle';

 import FormControl from '@material-ui/core/FormControl';
 import InputLabel from '@material-ui/core/InputLabel';
 import MenuItem from '@material-ui/core/MenuItem';
 import Select from '@material-ui/core/Select';

import ViewStudentData from './view_single_student';
import SEngine from './search_engine';

 function Alert(props) {
   return <MuiAlert elevation={6} variant="filled" {...props} />;
 }



export default class StundetList extends Component{
  constructor()
  {
    super()
    this.state={
      students:'',
      loading:true,
      end_report:false,
      value_for_search:'',
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.fetchStudentList();
  }

  fetchStudentList=()=>{
    axios.post('/ework/user2/fetch_student_list_under_faculty_adviser',{user:this.props.user})
  .then( res => {
      if(res.data)
      {
        this.setState({students:res.data,loading:false})
      }
      else {
          this.setState({loading:false})
      }
  })
  .catch( err => {
      this.setState({loading:false})
  });
  }


  searchEngine =(e)=>{
    this.setState({value_for_search:e.target.value})
  }

  render()
  {
    if(this.state.loading)
    {
      return(
        <Backdrop open={true} style={{zIndex:'2024'}}>
          <CircularProgress style={{color:'yellow'}} />&emsp;
          <div style={{color:'yellow',textAlign:'center'}}>Loading....</div>
        </Backdrop>
      )
    }
    else {
      if(this.state.students.length === 0)
      {
        return(
          <Grid container>
            <Grid item xs={12} sm={12}>
                <Typography align="center" variant="h6">No Request Found !!</Typography>
            </Grid>
          </Grid>
        )
      }
      else {
        const {students} = this.state;
        var libraries = students,
        searchString = this.state.value_for_search.trim().toLowerCase();

            libraries = libraries.filter(function(i) {
              return i.username.toLowerCase().match( searchString );
            })
        return(
          <React.Fragment>
              <div style={{padding:'30px',width:'100%'}}>
               <SEngine  value_for_search={this.state.value_for_search} searchEngine={this.searchEngine}  />
             </div>
            <EnhancedTable request={libraries} end_sem={this.state.end_report} user_session={this.props.user} />
          </React.Fragment>
        )
      }
    }
  }
}



function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}


const headCells = [
  { id: 'Reg ID', numeric: false, disablePadding: false, label: 'Reg ID' },
  { id: 'Mail Id', numeric: true, disablePadding: false, label: 'Mail Id' },
  { id: 'Year - Sem - Batch', numeric: true, disablePadding: false, label: 'Year - Sem - Batch' },
  { id: 'Campus - Department', numeric: true, disablePadding: false, label: 'Campus - Department' },
  { id: 'View', numeric: true, disablePadding: true, label: 'View' },
  { id: 'Send Message', numeric: true, disablePadding: true, label: 'Send Message' },
  { id: 'Action', numeric: true, disablePadding: true, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>
               {(((headCell.label === 'Information') || (headCell.label === 'Action')) && (props.incoming_action===false)) ?
                 <React.Fragment></React.Fragment>
                   :
                 <React.Fragment>{headCell.label}</React.Fragment>
               }
              </b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [submit,setSubmit] = React.useState(false)
  const [data,setData] = React.useState({
    details_view:false,
    data:'',
    snack_open:false,
    snack_msg:'',
    alert_type:'',
  });
  const [state,setState] = React.useState({
    year:'',
    sem:'',
    batch_ref:'',
    batch:'',
  });
  const [mwindow,setMwindow] = React.useState({
      open_popup: false,
      open_verify:false,
      userDetails: '',
    });
    const [message, setMessage] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };



  const closeView=()=>{
    setData({details_view:false})
  }

  const showDetails=(content)=>{
    setData({data:content,details_view:true})
  }

  const sendMessage=()=>{
    var data;
    if(!message)
    {
      setData({
        snack_open:true,
        snack_msg:'Enter all the Details !!',
        alert_type:'warning',
      })
    }
    else{
        data = {
          mail_send:true,
          user:[{mailid:mwindow.userDetails.mailid}],
          message:message,
          user_session:props.user_session,
        }
        setData({
          snack_open:true,
          snack_msg:'Sending Mail....',
          alert_type:'info',
        })
        setSubmit(true)
      axios.post('/ework/user2/sendMessage_from_UserSide',data)
      .then( res => {
        setSubmit(false)
          if(res.data === 'ok')
          {
            setData({
              snack_open:true,
              snack_msg:'Done !!',
              alert_type:'success',
            })

            message('')
            setMwindow({
              open_popup:false,
            })
          }
          else if(res.data === 'no') {
            setData({
              snack_open:true,
              snack_msg:'Failed To Send !!',
              alert_type:'error',
            })
          }
            setSubmit(false)
      })
      .catch( err => {
        setSubmit(false)
      });
   }
  }

  const reActive=()=>{
     if(!state.year || !state.batch || !state.sem)
     {
       setData({
         snack_open:true,
         snack_msg:'Enter all the details !!',
         alert_type:'warning',
       })
     }
     else{
      setSubmit(true)
      axios.post('/ework/user2/updatestudent',{username:mwindow.userDetails.username,data:state})
      .then( res => {
        setSubmit(false)
          if(res.data === 'ok')
          {
            setData({
              snack_open:true,
              snack_msg:'Done !!',
              alert_type:'success',
            })
            setMwindow({
              open_popup:false,
            })
          }
          else if(res.data === 'no') {
            setData({
              snack_open:true,
              snack_msg:'Failed To Update !!',
              alert_type:'error',
            })
            setMwindow({
              open_popup:false,
            })
          }
            setSubmit(false)
      })
      .catch( err => {
        setSubmit(false)
      });
    }
  }
const handleChange=(evt)=>{
  const value = evt.target.value;
setState({
...state,
[evt.target.name]: value
});
}
  const number=[{id:'A'},{id:'B'},{id:'C'},{id:'D'},{id:'E'},{id:'F'},{id:'G'},{id:'H'},{id:'I'},{id:'J'},{id:'K'},{id:'L'},{id:'M'}
,{id:'N'},{id:'O'},{id:'P'},{id:'Q'},{id:'R'},{id:'S'},{id:'T'},{id:'U'},{id:'V'},{id:'W'},{id:'X'},{id:'Y'},{id:'Z'}]
  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.request.length - page * rowsPerPage);

if(submit)
{
  return(
    <Backdrop open={true} style={{zIndex:'2024'}}>
      <CircularProgress style={{color:'yellow'}} />&emsp;
      <div style={{color:'yellow',textAlign:'center'}}>Sending Your Data....</div>
    </Backdrop>
  )
}
else{
  return (
    <div className={classes.root}>
    {data.details_view  &&
        <ViewStudentData details_view={data.details_view}  closeView={closeView} data={data} />
    }

    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
    open={data.snack_open} autoHideDuration={2000}
    onClose={()=>setData({snack_open:false})}>
      <Alert onClose={()=>setData({snack_open:false})}
      severity={data.alert_type}>
        {data.snack_msg}
      </Alert>
    </Snackbar>

    {mwindow.open_popup &&
        <Dialog open={mwindow.open_popup} onClose={()=>setMwindow({open_popup:false,userDetails:''})} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">
           {!(mwindow.open_popup && mwindow.open_verify) ?
             "Send Message"
             :
             "Re-Active Student Account"
           }
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
             {!(mwindow.open_popup && mwindow.open_verify) &&
                "This message will be forwarded to the particular student."
             }
            </DialogContentText>
              {mwindow.open_popup && mwindow.open_verify ?
                <React.Fragment>
                <FormControl style={{width:'100%'}}>
                  <InputLabel id="sel_year">Year</InputLabel>
                    <Select
                      labelId="sel_year"
                      id="sel_year"
                      name="year"
                      value={state.year}
                      onChange={handleChange}
                    >
                      <MenuItem value="" disabled defaultValue>Year</MenuItem>
                      <MenuItem value="1">First Year</MenuItem>
                      <MenuItem value="2">Second Year</MenuItem>
                      <MenuItem value="3">Third Year</MenuItem>
                      <MenuItem value="4">Fourth Year</MenuItem>
                      <MenuItem value="5">Fifth Year</MenuItem>
                    </Select>
                 </FormControl>
                 <br />
                <FormControl style={{width:'100%'}}>
                  <InputLabel id="sel_sem">Semester</InputLabel>
                    <Select
                      labelId="sel_sem"
                      id="sel_sem"
                      name="sem"
                      value={state.sem}
                      onChange={handleChange}
                    >
                      <MenuItem value="" disabled defaultValue>Year</MenuItem>
                      <MenuItem value="1">1</MenuItem>
                      <MenuItem value="2">2</MenuItem>
                      <MenuItem value="3">3</MenuItem>
                      <MenuItem value="4">4</MenuItem>
                      <MenuItem value="5">5</MenuItem>
                      <MenuItem value="6">6</MenuItem>
                      <MenuItem value="7">7</MenuItem>
                      <MenuItem value="8">8</MenuItem>
                      <MenuItem value="9">9</MenuItem>
                      <MenuItem value="10">10</MenuItem>
                    </Select>
                 </FormControl>
                 <br />
                  <FormControl style={{width:'100%'}}>
                    <InputLabel id="sel_batch">Batch Type</InputLabel>
                      <Select
                        labelId="sel_batch"
                        id="sel_batch"
                        name="batch_ref"
                        value={state.batch_ref}
                        onChange={handleChange}
                      >
                      {number.map((content,index)=>{
                        return(
                          <MenuItem value={content.id}>{content.id}</MenuItem>
                        )
                      })}
                      </Select>
                   </FormControl>
                   <br />
                  <FormControl style={{width:'100%'}}>
                    <InputLabel id="sel_batch">Select Batch</InputLabel>
                      <Select
                        labelId="sel_batch"
                        id="sel_batch"
                        name="batch"
                        disabled={state.batch_ref ? false:true}
                        value={state.batch}
                        onChange={handleChange}
                      >
                      {Array.apply(null, {length: 10}).map((i, index)=>{
                        return(
                          <MenuItem value={state.batch_ref+(index+1)}>{state.batch_ref}{index+1}</MenuItem>
                        )
                      })}
                      </Select>
                   </FormControl>
                </React.Fragment>
                :
                <TextField
                    required
                    id="filled-required"
                    label="Enter the message"
                    value={message}
                    onChange={(e)=>setMessage(e.target.value)}
                    variant="filled"
                    fullWidth
                    multiline
                  />
              }
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setMwindow({open_popup:false,userDetails:''})} color="primary">
              Cancel
            </Button>
            {mwindow.open_popup && mwindow.open_verify ?
              <Button color="primary" onClick={reActive}>
                Re-Active
              </Button>
              :
              <Button disabled={message ? false: true} color="primary" onClick={sendMessage}>
                Send Message
              </Button>
            }
          </DialogActions>
         </Dialog>
    }




      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              incoming_action={props.admin_action}
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.request.length}
            />
            <TableBody>
              {stableSort(props.request, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row._id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                    >
                     <TableCell>
                     {row.username}
                     </TableCell>
                     <TableCell align="center">
                       {row.mailid}
                      </TableCell>

                      <TableCell align="center">
                        {row.year} - {row.sem} - {row.batch}
                      </TableCell>
                      <TableCell align="center">
                        {row.campus} - {row.dept}
                      </TableCell>
                      <TableCell align="center">
                        <Tooltip title="View Details">
                         <VisibilityIcon onClick={()=>showDetails(row)} />
                        </Tooltip>
                       </TableCell>
                       <TableCell align="center">
                         <Tooltip title="Send Message To This Student">
                          <SendIcon onClick={()=>setMwindow({
                            open_popup:true,
                            userDetails:row,
                          })} />
                         </Tooltip>
                       </TableCell>
                       <TableCell align="center">
                         {(row.sem_break === true)  ?
                           <Button variant="contained" color="primary" onClick={()=>setMwindow({
                             open_popup:true,
                             userDetails:row,
                             open_verify:true,
                           })}>Verify</Button>
                           :
                           <Typography color="secondary">Not Availbale</Typography>
                         }
                       </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.request.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}


}
