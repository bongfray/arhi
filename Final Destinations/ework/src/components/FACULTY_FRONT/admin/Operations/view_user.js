import React, { Component} from 'react'
import { } from 'react-router-dom'
import axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Hidden,Paper,Button,Typography,Tooltip,TextField,Backdrop,CircularProgress} from '@material-ui/core';
import AddAlertIcon from '@material-ui/icons/AddAlert';
import VisibilityIcon from '@material-ui/icons/Visibility';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


import ViewUserBYList from '../../VIEW/FACULTY DATA/SUPER VIEW/view_super'
import Student from '../../VIEW/studentSearchView';
import DetailView from '../department_admin_panel'


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default class SuspendUser extends Component{
    constructor(){
        super();
        this.state={
            success:'',
            modal:false,
            disabled:'',
            allowed:false,
        }
    }


    render(){
      let view_content;
      if(this.props.select === 'dept_admin_profile'){
            view_content=
                <React.Fragment>
                   <DeptAdminData />
                </React.Fragment>
        }
        else if(this.props.select === 'facprofile'){
            view_content =
            <ViewUserBYList admin_action={true} />
        }
        else if(this.props.select === 'studprofile'){
            view_content =
                <React.Fragment>
                 <Student />
                </React.Fragment>
        }
        else{
            view_content=
                <div></div>
        }
        return(
          <React.Fragment>
            {view_content}
          </React.Fragment>
        );
    }
}



class DeptAdminData extends Component {
  constructor()
  {
    super()
    this.state={
      departAdmin:[],
      user:[],
      render_detail:false
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.data()
  }
  data(){
    axios.get('/ework/user/know_no_of_user')
    .then( res => {
      const d_user = (res.data.filter(item => item.h_order=== 0.5));
        this.setState({departAdmin:d_user})
    });
  }
  showDetails =(user)=>{
    this.setState({
      render_detail:!this.state.render_detail,
      user:user,
    })
  }

  render() {
    return (
      <React.Fragment>
          <ShowDetails show_div={this.state.render_detail} detail={this.showDetails} user={this.state.user} />

          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Official ID</TableCell>
                  <TableCell align="center">Department</TableCell>
                  <TableCell align="center">Campus</TableCell>
                  <TableCell align="center">Mail ID</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.departAdmin.map(row => (
                  <TableRow key={row.username}>
                    <TableCell align="center">
                      {row.username}
                    </TableCell>
                    <TableCell align="center">{row.dept}</TableCell>
                    <TableCell align="center">{row.campus}</TableCell>
                    <TableCell align="center">{row.mailid}</TableCell>
                    <TableCell align="center">
                       <Button variant="outlined" color="secondary" onClick={ () => this.showDetails(row)}>View History</Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
      </React.Fragment>
    );
  }
}


class ShowDetails extends Component {
  constructor()
  {
    super()
    this.state={
      total_accepted:0,
      total_request:0,
      pending:0,
      message:'',
      mailid:'',
      show_page:false,
      snack_open:false,
      alert_type:'',
      snack_msg:'',
      fetching:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount()
  {
    this.total_details();
  }
  componentDidUpdate=(prevProps)=>{
    if(prevProps.user.username!==this.props.user.username)
    {
      this.setState({fetching:true})
      this.total_details();
    }
  }
  total_details = () =>{
    axios.post('/ework/user/fetch_request',{user:this.props.user})
    .then(res=>{
        this.setState({
          total_request:res.data.length,
          total_accepted:(res.data.filter(item=>(item.active === true && item.activated_by === this.props.user.username))).length,
          pending:(res.data.filter(item=>item.active === false)).length,
          fetching:false,
        })
    })
  }

  retriveMessage =(e)=>{
    this.setState({message:e.target.value})
  }
  sendMessage = (mailid) =>{
    this.setState({fetching:true})
    axios.post('/ework/user/send_message',{username:this.props.user.username,message:this.state.message})
    .then( res => {
        if(res.data)
        {
          this.setState({
            snack_open:true,
            snack_msg:'Sent !!',
            alert_type:'success',
            message:'',
            fetching:false,
          });
        }
    })
    .catch( err => {
        this.setState({fetching:false})
    });
    }

  ping =()=>{
    this.setState({fetching:true})
    axios.post('/ework/user/ping',{username:this.props.user.username})
    .then( res => {
      this.setState({
        snack_open:true,
        snack_msg:'Pinged !!',
        alert_type:'success',
        fetching:false,
      })
    })
    .catch( err => {
        this.setState({fetching:false})
    });
  }

  render() {
    if(this.state.fetching)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Processing Your Request.....</div>
        </Backdrop>
      );
    }
    else{
    return (
      <React.Fragment>

      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      open={this.state.snack_open} autoHideDuration={2000}
      onClose={()=>this.setState({snack_open:false})}>
        <Alert onClose={()=>this.setState({snack_open:false})}
        severity={this.state.alert_type}>
          {this.state.snack_msg}
        </Alert>
      </Snackbar>

      <Dialog fullScreen open={this.state.show_page} onClose={()=>this.setState({show_page:false})} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2} sm={2}>
                  <IconButton edge="start" color="inherit" onClick={()=>this.setState({show_page:false})} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8} sm={8}>
                 <div style={{textAlign:'center',fontSize:'20px'}}>Profile Details Of {this.props.user.username}</div>
               </Grid>
               <Grid item xs={2} sm={2}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
             <div style={{marginTop:'56px'}}>
                <DetailView user={this.props.user} />
             </div>
          </List>
      </Dialog>



      <Dialog fullScreen open={this.props.show_div} onClose={this.props.detail} TransitionComponent={Transition}>
          <AppBar>
            <Toolbar>
             <Grid style={{marginTop:'55'}}container spacing={1}>
               <Grid item xs={2} sm={2}>
                  <IconButton edge="start" color="inherit" onClick={this.props.detail} aria-label="close">
                    <CloseIcon />
                  </IconButton>
               </Grid>
               <Grid item xs={8} sm={8}>
                 <div style={{textAlign:'center',fontSize:'20px'}}>Profile Details Of {this.props.user.username}</div>
               </Grid>
               <Grid item xs={2} sm={2}/>
             </Grid>
            </Toolbar>
          </AppBar>
          <List>
          <Grid container spacing={1} style={{marginTop:'56px'}}>
              <Grid item xs={1} sm={1} lg={3} xl={3} md={2}/>
              <Grid item xs={10} sm={10} lg={6} xl={6} md={8} container spacing={1}  style={{marginTop:'20px'}}>
                <Grid item xs={4} sm={4}>
                   <Paper elevation={3} style={{height:'90px'}}>
                   <div style={{textAlign:'center',backgroundColor:'#ff4081',color:'white'}}>TOTAL REQUEST</div>
                   <div style={{margin:'10px 0px 5px 0px'}}>
                    <Typography variant="h5" align="center">{this.state.total_request}</Typography>
                   </div>
                   </Paper>
                </Grid>
                <Grid item xs={4} sm={4}>
                   <Paper elevation={3} style={{height:'90px'}}>
                     <div style={{textAlign:'center',backgroundColor:'#ff4081',color:'white'}}>ACCEPTED BY THIS USER</div>
                     <div style={{margin:'10px 0px 5px 0px'}}>
                      <Typography variant="h5" align="center">{this.state.total_accepted}</Typography>
                     </div>
                   </Paper>
                </Grid>
                <Grid item xs={4} sm={4}>
                   <Paper elevation={3} style={{height:'90px'}}>
                      <div style={{textAlign:'center',backgroundColor:'#ff4081',color:'white'}}>TOTAL PENDING</div>
                       <div style={{margin:'10px 0px 5px 0px'}}>
                        <Typography variant="h5" align="center">{this.state.pending}&emsp;
                              <IconButton aria-label="delete" disabled={(this.state.pending)>0 ? false :true}>
                                <Tooltip title="Ping To Accept Request">
                                    <AddAlertIcon color="secondary" fontSize="small" onClick={this.ping}  />
                                </Tooltip>
                              </IconButton>
                          </Typography>
                       </div>
                   </Paper>
                </Grid>
              </Grid>
              <Grid item xs={1} sm={1} lg={3} xl={3} md={2} />
          </Grid>
<br /><br />

                <Grid container spacing={1}>
                    <Hidden xsDown><Grid item  lg={2} xl={2} md={1}/></Hidden>
                        <Grid  item xs={12} sm={12} lg={8} xl={8} md={10}>
                              <Paper elevation={3} style={{padding:"10px 10px 50px 10px"}}>
                                <Grid container spacing={1}>
                                  <Grid item xs={1} sm={2}/>
                                  <Grid item xs={10} sm={8}>
                                     <Typography align="center" variant="h6">Leave A Message</Typography>
                                  </Grid>
                                  <Grid item xs={1} sm={2}>
                                    <IconButton style={{float:'right'}} aria-label="delete">
                                      <Tooltip title="View The Users Page">
                                          <VisibilityIcon color="secondary" fontSize="small" onClick={()=>this.setState({show_page:true})} />
                                      </Tooltip>
                                    </IconButton>
                                  </Grid>
                                </Grid>
                                  <br />
                                  <FormControl fullWidth variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-amount">Username</InputLabel>
                                    <OutlinedInput
                                      id="outlined-adornment-amount"
                                      value={this.props.user.username}
                                      readOnly
                                      startAdornment={<InputAdornment position="start">To</InputAdornment>}
                                      labelWidth={80}
                                    />
                                  </FormControl>
                                  <br /><br />
                                  <TextField
                                    id="outlined-multiline-static"
                                    label="Enter the Message"
                                    multiline
                                    rows="4"
                                    fullWidth
                                    value={this.state.message}
                                    onChange={this.retriveMessage}
                                    variant="filled"
                                  />
                                  <br /><br />
                                  <Button style={{float:'right'}} variant="contained" color="secondary"
                                  onClick={this.sendMessage} disabled={this.state.message ?  false :true} >SEND A MESSAGE</Button>
                                  </Paper>
                        </Grid>
                    <Hidden xsDown><Grid item lg={2} xl={2} md={1}/></Hidden>
                </Grid>

          </List>
        </Dialog>
        </React.Fragment>
    );
   }
  }
}
