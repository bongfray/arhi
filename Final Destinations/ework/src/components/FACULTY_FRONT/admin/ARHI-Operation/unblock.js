import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import {Paper,TextField,Typography} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import PropTypes from 'prop-types';
import {  makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import {Backdrop} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogTitle from '@material-ui/core/DialogTitle';
import InfoIcon from '@material-ui/icons/Info';



 export default class Action extends Component {
  constructor(){
    super()
    this.state={
      blocked_list:'',
      id:'',
      found:false,
      loading:false,
    }
  }
  checkUser=()=>{
    this.setState({loading:true})
    axios.post('/ework/user2/fetch_blocklist',{id:this.state.id})
    .then( res => {
        this.setState({found:true,blocked_list:res.data,loading:false})
    })
    .catch( err => {
        this.setState({loading:false})
    });
  }

  render() {
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Loading....</div>
        </Backdrop>
      )
    }
    else{
    return (
      <React.Fragment>
      <Grid container>
        <Grid item xs={1} sm={1} lg={4} xl={4} md={3}/>
        <Grid item xs={1} sm={1} lg={4} xl={4} md={6}>
           <Paper elevation={3} style={{padding:'15px 15px 40px 15px'}}>
             <TextField id="outlined-basic1" label="Enter Registration Id"
                type="text" name="username" value={this.state.id} onChange={(e)=>this.setState({id:e.target.value})}
                variant="outlined" fullWidth />
                <br/><br />
                <Button variant="contained" color="secondary" style={{float:'right'}}
                 onClick={this.checkUser}>See Details</Button>
           </Paper>
        </Grid>
        <Grid item xs={1} sm={1} lg={4} xl={4} md={3}/>
      </Grid>
      <br />
      {this.state.found &&
       <React.Fragment>
        {this.state.blocked_list.length> 0 ?
          <div style={{padding:'10px'}}>
          <Typography variant="h6" color="secondary">Courses Blocked</Typography>
            <EnhancedTable data={this.state.blocked_list} user={this.props.user}
            admin_action={this.props.admin_action} fetchUser={this.checkUser} />
          </div>
          :
          <Typography variant="h6" align="center" color="secondary">No Data Availbale !!</Typography>
        }
      </React.Fragment>
     }
    </React.Fragment>
    );
   }
  }
}



function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}


const headCells = [
  { id: 'Student Id', numeric: false, disablePadding: false, label: 'Student Id' },
  { id: 'Skill Name', numeric: true, disablePadding: false, label: 'Skill Name' },
  { id: 'Blocked Count', numeric: true, disablePadding: false, label: 'Blocked Count' },
  { id: 'Recent Status', numeric: true, disablePadding: false, label: 'Recent Status' },
  { id: 'Blocked Time', numeric: true, disablePadding: false, label: 'Blocked Time' },
  { id: 'Unblocked By', numeric: true, disablePadding: false, label: 'Unblocked By' },
  { id: 'Action', numeric: true, disablePadding: true, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy} = props;


  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
          >

              <b>
               {(((headCell.label === 'Information') || (headCell.label === 'Action')) && (props.incoming_action===false)) ?
                 <React.Fragment></React.Fragment>
                   :
                 <React.Fragment>{headCell.label}</React.Fragment>
               }
              </b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

 function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [submit, setSubmit] = React.useState(false);
  const [dense] = React.useState(false);
  const [info,openInfo] = React.useState({
    info:false,
    info_data:'',

  });

  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const unblock=(data)=>{
    setSubmit(true)
    axios.post('/ework/user2/unblock_skill',{data:data,user_session:props.user})
    .then( res => {
      setSubmit(false)
      props.fetchUser();
    })
    .catch( err => {
      setSubmit(false)
    });
  }

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.data.length - page * rowsPerPage);


  function formatDate(string){
    var dateString =string.toString();
      var options = { year: 'numeric', month: 'long', day: 'numeric' };
      return new Date(dateString).toLocaleDateString([],options);
  }

if(submit)
{
  return(
    <Backdrop  open={true} style={{zIndex:'2040'}}>
      <CircularProgress style={{color:'yellow'}}/>&emsp;
      <div style={{color:'yellow'}}>Processing Your Request...</div>
    </Backdrop>
  )
}
else{
  return (
    <div className={classes.root}>


      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.data.length}
            />
            <TableBody>
              {stableSort(props.data, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row._id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                    >
                     <TableCell>
                       {row.username}
                     </TableCell>
                     <TableCell align="center">
                         {row.name}
                      </TableCell>

                      <TableCell align="center">
                        {row.count}
                      </TableCell>
                      <TableCell align="center">
                        {row.status ?
                          <div style={{color:'red'}}>Blocked</div>
                          :
                          <div style={{color:'green'}}>Opened</div>
                        }
                      </TableCell>
                      <TableCell align="center">
                          {formatDate(row.received)}
                      </TableCell>
                      <TableCell align="center">
                        {props.admin_action ?
                          <React.Fragment>
                               <InfoIcon onClick={()=>openInfo({
                                 info:true,
                                 info_data:row.unblocked_by,
                               })}/>
                         </React.Fragment>
                         :
                         "User Mismatch"
                        }
                      </TableCell>
                      <TableCell align="center">
                        <Button align="center" variant="contained" color="primary" disabled={!row.status}
                         onClick={()=>unblock(row)}>Unblock</Button>
                      </TableCell>

                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>

      {info.info &&
        <Dialog
          open={info.info}
          fullWidth
          keepMounted
          onClose={()=>openInfo({
            info:false,
          })}
           aria-labelledby="scroll-dialog-title"
           aria-describedby="scroll-dialog-description"
         >
           <DialogTitle id="scroll-dialog-title" align="center">Unblocked By</DialogTitle>
           <DialogContent >
             <DialogContentText
               id="scroll-dialog-description"
               tabIndex={-1}
             >
             <div style={{padding:'10px'}}>
                 <Grid container>
                   <Grid item xs={4} sm={4} style={{textAlign:'center',color:'red'}}>Index</Grid>
                   <Grid item xs={4} sm={4} style={{textAlign:'center',color:'red'}}>Blocked By</Grid>
                   <Grid item xs={4} sm={4} style={{textAlign:'center',color:'red'}}>Date</Grid>
                 </Grid>
                 {info.info_data.map((content,index)=>{
                   return(
                     <React.Fragment key={index}>
                       <Grid container>
                         <Grid item xs={4} sm={4} style={{textAlign:'center',color:'black'}}>{index+1}</Grid>
                         <Grid item xs={4} sm={4} style={{textAlign:'center',color:'black'}}>{content.user}</Grid>
                         <Grid item xs={4} sm={4} style={{textAlign:'center',color:'black'}}>{content.time}</Grid>
                       </Grid><br />
                     </React.Fragment>
                   )
                 })}
             </div>
             </DialogContentText>
           </DialogContent>
           <DialogActions>
             <Button onClick={()=>openInfo({
               info:false,
             })} color="primary">
               CLOSE
             </Button>
           </DialogActions>
         </Dialog>
      }
    </div>
  );
 }
}
