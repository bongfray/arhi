import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import AddIcon from '@material-ui/icons/Add';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import axios from 'axios';





function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}


const headCells = [
  { id: 'Faculty Id', numeric: false, disablePadding: false, label: 'Faculty Id' },
  { id: 'Faculty Name', numeric: true, disablePadding: false, label: 'Faculty Name' },
  { id: 'Designation', numeric: true, disablePadding: false, label: 'Designation' },
  { id: 'Mail Id', numeric: true, disablePadding: false, label: 'Mail Id' },
  { id: 'Phone number', numeric: true, disablePadding: false, label: 'Phone number' },
  { id: 'SUPRIMITY', numeric: true, disablePadding: false, label: 'SUPRIMITY' },
  { id: 'Faculty Adviser', numeric: true, disablePadding: false, label: 'Faculty Adviser' },
  { id: 'Suspension Status', numeric: true, disablePadding: false, label: 'Suspension Status' },
  { id: 'Action', numeric: true, disablePadding: false, label: 'Action' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <b>
              {headCell.label}
              </b>
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

 export default function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [adviser, adiviserChange] = React.useState({
    modalon_adviser:false,
    username:'',
  });
  const [year, handleInput] = React.useState('')


  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };


  const handleModal=()=>{
    axios.post('/ework/user/faculty_adviser_status_modify',{updated:true,
      username:adviser.username,year:year})
    .then( res => {
        adiviserChange({
          modalon_adviser:false,
        })
          window.M.toast({html: 'Done !!', classes:'rounded green darken-1'});
    });
  }

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.list_of_user.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
    {adviser.modalon_adviser &&
          <Dialog
            open={adviser.modalon_adviser}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">In Which Year ?</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                  <FormControl style={{width:'100%'}}>
                    <InputLabel id="sel_year">Year</InputLabel>
                      <Select
                        labelId="sel_year"
                        id="sel_year"
                        name="year"
                        value={year}
                        onChange={(e)=>handleInput(e.target.value)}
                      >
                        <MenuItem value="" disabled defaultValue>Year</MenuItem>
                        <MenuItem value="1">First Year</MenuItem>
                        <MenuItem value="2">Second Year</MenuItem>
                        <MenuItem value="3">Third Year</MenuItem>
                        <MenuItem value="4">Fourth Year</MenuItem>
                        <MenuItem value="5">Fifth Year</MenuItem>
                      </Select>
                   </FormControl>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={()=>adiviserChange({
              modalon_adviser:false,
            })} color="primary" autoFocus>
              CLOSE
            </Button>
              <Button onClick={handleModal} color="primary" autoFocus>
                SUBMIT
              </Button>
            </DialogActions>
          </Dialog>
    }
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              incoming_action={props.admin_action}
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.list_of_user.length}
            />
            <TableBody>
              {stableSort(props.list_of_user, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row._id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                    >
                     <TableCell>
                     {row.username}
                     </TableCell>
                       <TableCell align="center">
                       {row.name}
                       </TableCell>
                      <TableCell align="center">
                       {row.desgn}
                      </TableCell>
                      <TableCell align="center">
                       {row.mailid}
                      </TableCell>
                      <TableCell align="center">
                       {row.phone}
                      </TableCell>
                      <TableCell align="center">
                       {row.suprimity ?
                          <React.Fragment>
                            Supreme Action - {row.task}
                          </React.Fragment>
                          :
                          <div style={{color:'red'}}>
                            NULL
                          </div>
                       }
                      </TableCell>
                      <TableCell align="center">
                      {row.faculty_adviser ?
                         <React.Fragment>
                           Faculty Adviser For Year - {row.faculty_adviser_year}
                         </React.Fragment>
                         :
                         <div style={{color:'red'}}>
                           NULL
                         </div>
                      }
                      </TableCell>
                      <TableCell align="center">
                       {row.suspension_status ?
                         <div style={{color:'red'}}>
                           SUSPENDED
                         </div>
                         :
                         <React.Fragment>
                           NULL
                         </React.Fragment>
                       }
                      </TableCell>
                      <TableCell>
                        <IconButton  disabled={row.faculty_adviser ? true : false}  aria-label="delete" className={classes.margin}>
                          <AddIcon onClick={()=>adiviserChange({
                            modalon_adviser:true,
                            username:row.username,
                          })} />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.list_of_user.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
