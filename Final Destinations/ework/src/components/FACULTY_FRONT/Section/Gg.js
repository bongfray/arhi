import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Backdrop,CircularProgress} from '@material-ui/core';
import Nav from '../../dynnav'
import AddProduct from './add';
import ProductList from './prod'

export default class Section extends Component {
  constructor(props)
  {
    super(props)
    this.state ={
      isAddProduct: false,
      response: {},
      product: {},
      isEditProduct: false,
      SectionProps:[],
      loading:true,
      username:'',
      redirectTo:'',
      option:'',
      home:'/ework/faculty',
      logout:'/ework/user/logout',
      login:'/ework/flogin',
      get:'/ework/user/',
      noti_route:true,
      nav_route: '/ework/user/fetchnav',
      isExpanded:false,
      disabled_expanse:true,
    }
    this.componentDidMount = this.componentDidMount.bind(this)
     this.onFormSubmit = this.onFormSubmit.bind(this);
  }

handleOption = (e) =>{
  this.setState({option: e.target.value,isExpanded:true,disabled_expanse:false})
}
getUser()
{
  axios.get('/ework/user/')
   .then(response =>{
     if(response.data.user)
     {
       this.setState({username: response.data.user.username})
       this.getSectionData()
     }
     else{
      this.setState({loading: false})
       this.setState({
         redirectTo:'/ework/faculty',
       });
     }
   })
}
getSectionData()
{
  axios.post('/ework/user/fetch_section',{action:'Section'}).then(response =>{
    this.setState({loading: false,
     SectionProps: response.data,
   })
  })
}

componentDidMount(){
  this.getUser()
}
   onCreate = (e,index) => {
     this.setState({ isAddProduct: true ,product: {}});
   }
   onFormSubmit(data) {
     let apiUrl;
     if(this.state.isEditProduct){
       apiUrl = '/ework/user/editt';
     } else {
       apiUrl = '/ework/user/addmm';
     }
     this.setState({loading:true})
     axios.post(apiUrl, {data})
         .then(response => {
           this.setState({
             response: response.data,
             isAddProduct: false,
             isEditProduct: false,
             loading:false,
           })
         })
    }

   editProduct = (productId,index)=> {
     this.setState({loading:true})
     axios.post('/ework/user/fetcheditdata',{
       id: productId,
       username: this.props.username,
     })
         .then(response => {
           this.setState({
             product: response.data,
             isEditProduct: true,
             isAddProduct: true,
             loading:false,
           });
         })

  }
  updateState = () =>{
    this.setState({
      isAddProduct:false,
      isEditProduct:false,
    })
  }

handleExpand =(e) => {
  this.setState({isExpanded:!this.state.isExpanded});
};

  render()
  {
    let productForm;
    var data = {
      fielddata: [
        {
          header: "Description of Your Work",
          name: "description",
          placeholder: "Description of your work",
          type: "text",
          grid: 3
        },
        {
          header: "Your Role",
          name: "role",
          placeholder: "Enter Your Role",
          type: "text",
          grid:2
        },

        {
          header: "Your Achievements",
          name: "achivements",
          type: "text",
          grid:3,
          placeholder: "Enter Details about your Achievements"
        },
        {
          header: "Date of Starting",
          name: "date_of_starting",
          type: "date",
          grid:1,
          placeholder: "Enter the Date of Starting"
        },
        {
          header: "Date of Completion",
          name: "date_of_complete",
          type: "date",
          grid:1,
          placeholder: "Enter the Date of Completion"
        },
        {
          header: "Verification Link",
          name: "verification_link",
          type: "text",
          grid:1,
          link:true,
          placeholder:"Put a Google Drive Link",
        },

      ],
    };
    if(this.state.isAddProduct || this.state.isEditProduct)
    {
     productForm = <AddProduct cancel={this.updateState} username={this.state.username} action={this.state.option} data={data} onFormSubmit={this.onFormSubmit}  product={this.state.product} />
    }
    let section_datas;
    if(this.state.SectionProps)
    {
      section_datas =
      <FormControl style={{width:'100%'}}>
         <InputLabel id="free_type">Select Here</InputLabel>
         <Select
           labelId="free_type"
           id="free_type"
           value={this.state.option}
           onChange={this.handleOption}
         >
         {this.state.SectionProps.map((content,index)=>{
                 return(
                <MenuItem key={index} value={content.section_data_name}>{content.section_data_name}</MenuItem>                 )
           })}
         </Select>
       </FormControl>
    }
    else
    {
      section_datas =
      <FormControl style={{width:'100%'}}>
         <InputLabel id="free_type1">Select Here</InputLabel>
         <Select
           labelId="free_type1"
           id="free_type1"
         >
          <MenuItem disabled>Fetching....</MenuItem>                 )
         </Select>
       </FormControl>
    }

    if (this.state.redirectTo) {
         return <Redirect to={{ pathname: this.state.redirectTo }} />
     } else {
    if(this.state.loading)
    {
      return(
        <Backdrop  open={true} style={{zIndex:'2040'}}>
          <CircularProgress style={{color:'yellow'}}/>&emsp;
          <div style={{color:'yellow'}}>Processing Your Request.....</div>
        </Backdrop>
      );
    }
    else
    {
    return(
      <React.Fragment>
      <Nav noti_route={this.state.noti_route} home={this.state.home} nav_route={this.state.nav_route} get={this.state.get} logout={this.state.logout}/>
      <Grid container>
            <Hidden xsDown>
              <Grid item sm={1}/>
            </Hidden>
               <Grid item xs={12} sm={10}>
                 <Paper variant="outlined" square style={{borderRadius:'15px',padding:'12px',marginTop:'10px'}}>
                   <Grid container spacing={1}>
                     <Grid item xs={12} sm={7}>
                       <Typography variant="h6" align="center" color="secondary" gutterBottom>
                        Kindly Select On Which Area You Are Going to Upload You Datas
                       </Typography>
                     </Grid>
                     <Grid item xs={12} sm={5}>
                      {section_datas}
                     </Grid>
                   </Grid>
                  </Paper>
                </Grid>
              <Hidden xsDown>
                <Grid item sm={1} />
              </Hidden>
        </Grid>

        <div className="section-data" style={{padding:'15px'}}>
                <ExpansionPanel
                expanded={this.state.isExpanded}
                style={{borderRadius:'20px'}} onChange={this.handleExpand}
                disabled={this.state.disabled_expanse}>
                  <ExpansionPanelSummary
                  style={{borderRadius:'20px'}}
                    expandIcon={<ExpandMoreIcon style={{color:'yellow'}} />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                    className="mine-collap"
                  >
                    <div style={{color:'yellow',fontSize:'25px'}}>{this.state.option}</div>
                          </ExpansionPanelSummary>
                              <div style={{padding:'15px'}}>
                                {!this.state.isAddProduct &&
                                     <ProductList username={this.props.username} title={this.state.option}  action={this.state.option} data={data}  editProduct={this.editProduct}/>
                                }
                                <br />
                                {!this.state.isAddProduct &&
                                 <React.Fragment>
                                   <Grid container spacing={1}>
                                     <Grid item xs={12} sm={6} />
                                     <Grid item xs={6} sm={6}>
                                      {this.state.option &&
                                        <Button style={{float:'right'}} variant="contained" color="secondary"
                                        onClick={(e) => this.onCreate(e,this.props.options)}>Add Data</Button>}
                                     </Grid>
                                   </Grid>
                                </React.Fragment>
                              }
                                { productForm }
                                <br/>
                              </div>
                        </ExpansionPanel>
            </div>
      </React.Fragment>
    )
}
}
  }
}
